
/* -----Wow Js Config-------- */
new WOW().init();

/* ----Maps------ */

// google.maps.event.addDomListener(window, 'load', initialize);
//
// function initialize() {
//   var mapCanvas = document.getElementById('map');
//   var mapOptions = {
//     center: new google.maps.LatLng(-5.3823236, 105.2568212),
//     zoom: 18,
//     mapTypeId: google.maps.MapTypeId.ROADMAP
//   }
//   var map = new google.maps.Map(mapCanvas, mapOptions);
// }


/* ------Scroll Top----- */
$(window).scroll(function () {
    if ($(this).scrollTop() > 100) {
        $('.scroll-up').fadeIn();
    } else {
        $('.scroll-up').fadeOut();
    }
});

$('.scroll-up').click(function () {
    $("html, body").animate({
        scrollTop: 0
    }, 600);
    return false;
});


/*Side Bar Accordion*/
function toggleChevron(e) {
    $(e.target)
        .prev('.panel-heading')
        .find("i.indicator")
        .toggleClass('glyphicon-chevron-down glyphicon-chevron-up');
}
$('#accordion').on('hidden.bs.collapse', toggleChevron);
$('#accordion').on('shown.bs.collapse', toggleChevron);




/*Header Freeze*/
$(document).ready(function () {

    $('#nav').affix({
        offset: {
            top: 50
        }
    });

    $('#sidebar').affix({
        offset: {
            top: 17
        }
    });

});




/*DateTimePicker*/


$(document).ready(function () {

    // if ($.isFunction($.datepicker)) {

      $(".date-picker").datepicker();
      // adding todays date as the value to the datepickers.
      var d = new Date();
      var curr_day = d.getDate();
      var curr_month = d.getMonth() + 1; //Months are zero based
      var curr_year = d.getFullYear();
      var eutoday = curr_day + "-" + curr_month + "-" + curr_year;
      var ustoday = curr_month + "-" + curr_day + "-" + curr_year;
      $("div.datepicker input").attr('value', eutoday);
      $("div.usdatepicker input").attr('value', ustoday);

      //calling the datepicker for bootstrap plugin
      // https://github.com/eternicode/bootstrap-datepicker
      // http://eternicode.github.io/bootstrap-datepicker/
      $('.datepicker').datepicker({
        autoclose: true,
        startDate: new Date(1960, 1, 1)
      });
    // }
});

$(document).ready(function () {
    $('.selectpicker').selectpicker({

    });
});



$(document).on('change', '.btn-file :file', function () {
    var input = $(this),
        numFiles = input.get(0).files ? input.get(0).files.length : 1,
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
    input.trigger('fileselect', [numFiles, label]);
});

$(document).ready(function () {
    $('.btn-file :file').on('fileselect', function (event, numFiles, label) {

        var input = $(this).parents('.input-group').find(':text'),
            log = numFiles > 1 ? numFiles + ' files selected' : label;

        if (input.length) {
            input.val(log);
        } else {
            if (log) alert(log);
        }

    });
});



/*Modal Dialog*/

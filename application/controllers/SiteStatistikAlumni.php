<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Methods: GET, OPTIONS");

class SiteStatistikAlumni extends MY_Controller {

  var $pagesId = 99916;

  private function getRowOtherBanner() {

    $rowOtherBanner = $this->callApi('OtherBanner/detailData', array(
      'id' => $this->pagesId
    ));

    if ($rowOtherBanner)
    if ($rowOtherBanner['publishStatusId'] == 2)
    return $rowOtherBanner;
  }

  public function index() {

    $rowSeo = $this->callApi('Seo/detailMeta', array(
      'id' => $this->pagesId
    ));

    $this->setSettings(array(
      'meta' => $rowSeo,
      'subTitle' => 'Statistik Alumni',
      'scoopy' => 'StatistikAlumni',
    ));

    
    $this->load->model('Kelulusan', 'miklat');

    $result = $this->miklat->readDataStatistikAlumni();
    $rsAlumni = $result['data'];


    $this->load->model('JenisDiklat', 'mJenisDiklat');
    $result = $this->mJenisDiklat->readData(array(
    		'orderBy' => 'position',
    		'reverse' => 1,
    ));
    $rsJenisDiklat = $result['data'];

    $this->render('StatistikAlumni', array(
      'rowOtherBanner' => $this->getRowOtherBanner(),
      'rsAlumni' => $rsAlumni,
    	'rsJenisDiklat' => $rsJenisDiklat,
    ));
  }

  
}

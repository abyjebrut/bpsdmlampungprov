<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Methods: GET, OPTIONS");
class SiteInfoPeserta extends MY_Controller {

  var $pagesId = 99915;

  private function getRowOtherBanner() {

    $rowOtherBanner = $this->callApi('OtherBanner/detailData', array(
      'id' => $this->pagesId
    ));

    if ($rowOtherBanner)
    if ($rowOtherBanner['publishStatusId'] == 2)
    return $rowOtherBanner;
  }

  public function index() {

    $rowSeo = $this->callApi('Seo/detailMeta', array(
      'id' => $this->pagesId
    ));

    $this->setSettings(array(
      'meta' => $rowSeo,
      'subTitle' => 'Info Peserta',
      'scoopy' => 'InfoPeserta',
    ));

    
    $this->load->model('PendaftaranDiklat', 'miklat');

    $result = $this->miklat->readDataChartAllAnggota();
    $rsAllAnggota = $result['data'];

    $result = $this->miklat->readDataChartJenisKelaminPria();
    $rsJenisKelaminPria = $result['data'];

    $result = $this->miklat->readDataChartJenisKelaminWanita();
    $rsJenisKelaminWanita = $result['data'];

    $result = $this->miklat->readDataChartLatsar();
    $rsLatsar = $result['data'];

    $result = $this->miklat->readDataChartPka();
    $rsPka = $result['data'];
    
    $result = $this->miklat->readDataChartPkp();
    $rsPkp = $result['data'];

    $result = $this->miklat->readDataChartPrajab();
    $rsPrajab = $result['data'];

    $result = $this->miklat->readDataChartPktuf();
    $rsPktuf = $result['data'];

    $result = $this->miklat->readDataChartPkti();
    $rsPkti = $result['data'];

    $result = $this->miklat->readDataChartD1D2();
    $rsD1D2 = $result['data'];
    
    $result = $this->miklat->readDataChartD3();
    $rsD3 = $result['data'];
    
    $result = $this->miklat->readDataChartD4S1();
    $rsD4S1 = $result['data'];
    
    $result = $this->miklat->readDataChartS2();
    $rsS2 = $result['data'];

    $result = $this->miklat->readDataChartS3();
    $rsS3 = $result['data'];
    
    $result = $this->miklat->readDataChartSmpSma();
    $rsSmpSma = $result['data'];

    $result = $this->miklat->readDataChart1830();
    $rs1830 = $result['data'];
    
    $result = $this->miklat->readDataChart3140();
    $rs3140 = $result['data'];
    
    $result = $this->miklat->readDataChart4150();
    $rs4150 = $result['data'];
    
    $result = $this->miklat->readDataChart51();
    $rs51 = $result['data'];

    $this->load->model('JenisDiklat', 'mJenisDiklat');
    $result = $this->mJenisDiklat->readData(array(
    		'orderBy' => 'position',
    		'reverse' => 1,
    ));
    $rsJenisDiklat = $result['data'];

    $this->render('InfoPeserta', array(
      'rowOtherBanner' => $this->getRowOtherBanner(),
      'jmlAllAnggota' => count($rsAllAnggota),
      'jmlPria' => count($rsJenisKelaminPria),
      'jmlWanita' => count($rsJenisKelaminWanita),
      'jmlLatsar' => count($rsLatsar),
      'jmlPka' => count($rsPka),
      'jmlPkp' => count($rsPkp),
      'jmlPrajab' => count($rsPrajab),
      'jmlPktuf' => count($rsPktuf),
      'jmlPkti' => count($rsPkti),
      'jmlD1D2' => count($rsD1D2),
      'jmlD3' => count($rsD3),
      'jmlD4S1' => count($rsD4S1),
      'jmlS2' => count($rsS2),
      'jmlS3' => count($rsS3),
      'jmlSmpSma' => count($rsSmpSma),
      'jml1830' => count($rs1830),
      'jml3140' => count($rs3140),
      'jml4150' => count($rs4150),
      'jml51' => count($rs51),
    	'rsJenisDiklat' => $rsJenisDiklat,
    ));
  }

  
}

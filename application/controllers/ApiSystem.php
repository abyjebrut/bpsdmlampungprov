<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ApiSystem extends MY_Controller {

  public function readConfig() {

    $rowLanguage = $this->callApi('Language/detailData');

    $data = array(
      'lang' => 'en',
      'primaryLang' => $rowLanguage['primaryCode'],
      'secondaryLang' => $rowLanguage['secondaryCode'],
      'extendedLang' => $rowLanguage['extendedCode'],
      'apiKey' => $this->config->item('apiKey'),
      'urlSite' => base_url(),
      'urlApi' => site_url().'/api/post',
      'urlTmp' => base_url().'asset/tmp',
      'urlArchive' => base_url().'asset/archive',
    );

    $this->json($this->success('DATA_READ', $data));
  }

  public function uploadImage() {

    if(!empty($_FILES)) {

      $fileType = $_FILES[0]['type'];

      switch($fileType) {
        case 'image/png':
        case 'image/jpeg':
        case 'image/gif':

          $fileName = $this->encrypt();
          $splitFileType = explode('/', $fileType);

          $fileName .= '.'.$splitFileType[1];

          $filePath = 'asset/tmp/'.$fileName;
          $fileURL = base_url().'asset/tmp/'.$fileName;

          $fileOnClient = $_FILES[0]['tmp_name'];

          move_uploaded_file($fileOnClient, $filePath);

          $this->json($this->success('FILE_UPLOADED', $fileURL));
          break;
        default:
          $this->json($this->failed('FILE_TYPE_DENIED'));
      }
    }
    else
      $this->json($this->failed('FILE_UPLOAD_FAILED'));
  }

  public function uploadPdf() {

    if(!empty($_FILES)) {

      $fileType = $_FILES[0]['type'];

      switch($fileType) {
        case 'application/pdf':

          $fileName = $this->encrypt();
          $splitFileType = explode('/', $fileType);

          $fileName .= '.'.$splitFileType[1];

          $filePath = 'asset/tmp/'.$fileName;
          $fileURL = base_url().'asset/tmp/'.$fileName;

          $fileOnClient = $_FILES[0]['tmp_name'];

          move_uploaded_file($fileOnClient, $filePath);

          $this->json($this->success('FILE_UPLOADED', $fileURL));
          break;
        default:
          $this->json($this->failed('FILE_TYPE_DENIED'));
      }
    }
    else
      $this->json($this->failed('FILE_UPLOAD_FAILED'));
  }

  public function uploadFile() {

    if(!empty($_FILES)) {

      $fileType = $_FILES[0]['type'];

      switch($fileType) {
        // case 'image/png':
        // case 'image/jpeg':
        // case 'image/gif':
        default:
          $fileName = $this->encrypt();

          $name = $_FILES[0]["name"];
          $ext = end((explode(".", $name)));

          $fileName .= '.'.$ext;

          $filePath = 'asset/tmp/'.$fileName;
          $fileURL = base_url().'asset/tmp/'.$fileName;

          $fileOnClient = $_FILES[0]['tmp_name'];

          move_uploaded_file($fileOnClient, $filePath);

          $this->json($this->success('FILE_UPLOADED', $fileURL));
        break;
        // default:
        //    $this->json($this->failed('FILE_TYPE_DENIED'));
      }
    }
    else
      $this->json($this->failed('FILE_UPLOAD_FAILED'));
  }

  public function deleteFile() {

    $file = $this->input->post('file');

    $tmpFile = "asset/tmp/$file";
    $archiveFile = "asset/archive/$file";

    $deletedFile = '';

    if(!is_dir($tmpFile) && file_exists($tmpFile)) {
      unlink($tmpFile);
      $deletedFile = $tmpFile;
    }

    if(!is_dir($tmpFile) && file_exists($archiveFile)) {
      unlink($archiveFile);
      $deletedFile = $archiveFile;
    }

    $this->json($this->success('FILE_DELETED', $tmpFile));
  }
}

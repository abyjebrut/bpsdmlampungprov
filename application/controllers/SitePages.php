<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SitePages extends MY_Controller {

	public function index() {
		$this->pageDetail(1);
	}

	private function getRowOtherBanner($id) {

		$rowOtherBanner = $this->callApi('OtherBanner/detailData', array(
			'id' => $id,
		));

		if ($rowOtherBanner)
			if ($rowOtherBanner['publishStatusId'] == 2)
				return $rowOtherBanner;
	}

	private function getRowPages($id) {
		$rowPages = $this->callApi('Pages/detailData', array(
			'id' => $id,
			'publishStatusId' => 2,
		));

		return $rowPages;
	}

	private function getRowSubPages($id) {
		$rowSubPages = $this->callApi('SubPages/detailData', array(
			'id' => $id,
		));

		return $rowSubPages;
	}

	private function getRowSeo($id) {
		$rowSeo = $this->callApi('Seo/detailMeta', array(
			'id' => $id,
		));

		return $rowSeo;
	}

	public function pageDetail($id)
	{
		$rowOtherBanner = $this->getRowOtherBanner($id);
		$rowPages = $this->getRowPages($id);
		$rowSeo = $this->getRowSeo($id);

		$this->setSettings(array(
			'subTitle' => $rowPages['title'],
			'meta' => $rowSeo,
			'scoopy' => '',
			'pagesId' => $id,
		));

		// $rsSubPages = $this->callApi('SubPages/readData', array(
		// 	'pagesId' => $id,
		// 	'publishStatusId' => 2,
		// ));

		$rsPages = $this->callApi('Pages/readData', array(
			'orderBy' => 'position',
			'publishStatusId' => 2,
			'reverse' => 1,
		));

		if (isset($_SESSION['id'])){
	    	$idAnggota =$_SESSION['id'];
	    }else{
	    	$idAnggota =0;
	    }

		$this->load->model('Anggota', 'mAnggota');
		$result = $this->mAnggota->detailData(array(
				'id' => $idAnggota,
		));

		$rowAnggota = $result['data'];

		$this->render('PageDetail', array(
			'rowAnggota' => $rowAnggota,
			'rowOtherBanner' => $rowOtherBanner,
			'rowPages' => $rowPages,
			'rsPages' => $rsPages,
			'activeId' => $id,
			// 'rsSubPages' => $rsSubPages,
		));
	}

	// public function subPageDetail($pagesId, $id) {
	// 	$rowOtherBanner = $this->getRowOtherBanner($pagesId);
	// 	$rowPages = $this->getRowPages($pagesId);
	// 	$rowSubPages = $this->getRowSubPages($id);
	// 	$rowSeo = $this->getRowSeo($id);
	//
	// 	$this->setSettings(array(
	// 		'subTitle' => $rowSubPages['title'],
	// 		'meta' => $rowSeo,
	// 		'scoopy' => '',
	// 		'pagesId' => $id,
	// 	));
	//
	// 	$rsSubPages = $this->callApi('SubPages/readData', array(
	// 		'pagesId' => $pagesId,
	// 		'publishStatusId' => 2,
	// 	));
	//
	// 	$this->render('SubPageDetail', array(
	// 		'rowOtherBanner' => $rowOtherBanner,
	// 		'rowPages' => $rowPages,
	// 		'rowSubPages' => $rowSubPages,
	// 		'rsSubPages' => $rsSubPages,
	// 	));
	// }

}

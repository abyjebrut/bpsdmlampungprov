<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Methods: GET, OPTIONS");
class SiteInfoAjar extends MY_Controller {

  var $pagesId = 99914;

  private function getRowOtherBanner() {

    $rowOtherBanner = $this->callApi('OtherBanner/detailData', array(
      'id' => $this->pagesId
    ));

    if ($rowOtherBanner)
    if ($rowOtherBanner['publishStatusId'] == 2)
    return $rowOtherBanner;
  }

  public function index() {

    $rowSeo = $this->callApi('Seo/detailMeta', array(
      'id' => $this->pagesId
    ));

    $this->setSettings(array(
      'meta' => $rowSeo,
      'subTitle' => 'Info Ajar',
      'scoopy' => 'InfoAjar',
    ));

    
    $this->load->model('Diklat', 'miklat');
    $result = $this->miklat->readDataJadwalDiklat();
    $rsJadwal = $result['data'];
    $this->load->model('JenisDiklat', 'mJenisDiklat');
    $result = $this->mJenisDiklat->readData(array(
    		'orderBy' => 'position',
    		'reverse' => 1,
    ));
    $rsJenisDiklat = $result['data'];

    $this->render('InfoAjar', array(
      'rowOtherBanner' => $this->getRowOtherBanner(),
      'rsJadwal' => $rsJadwal,
    	'rsJenisDiklat' => $rsJenisDiklat,
    ));
  }

  
}

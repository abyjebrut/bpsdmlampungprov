<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SiteFaq extends MY_Controller {

  var $pagesId = 9995;
  var $limit = 5;

  private function getRowOtherBanner() {

		$rowOtherBanner = $this->callApi('OtherBanner/detailData', array(
      'id' => $this->pagesId
    ));

		if ($rowOtherBanner)
			if ($rowOtherBanner['publishStatusId'] == 2)
				return $rowOtherBanner;
	}

  public function index() {
  	$this->page(1);
  }
  
  public function page($pageActive) {
  	 
  	$rowSeo = $this->callApi('Seo/detailMeta', array(
  			'id' => $this->pagesId
  	));
  	 
  	$this->setSettings(array(
      'meta' => $rowSeo,
      'subTitle' => 'FAQ',
      'scoopy' => 'Faq',
    ));
  
  	$this->load->model('Faq', 'mFaq');
  	$result = $this->mFaq->readData(array(
  			'orderBy' => 'position',
  			'reverse' => 1,
  			'publishStatusId' => 2,
  			'page' => $pageActive,
  			'count' => $this->limit,
  	));
  	 
  	$rsFaq = $result['data'];
  	$pageCount = $result['extra']['pageCount'];
  	 
  	$this->render('Faq', array(
  			'rowOtherBanner' => $this->getRowOtherBanner(),
  			'rsFaq' => $rsFaq,
  			'pageCount' => $pageCount,
  			'pageActive' => $pageActive,
  	));
  }

}

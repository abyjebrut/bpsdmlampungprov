<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SiteWidyaiswara extends MY_Controller {

  var $pagesId = 9991;
  var $limit = 6;

  private function getRowOtherBanner() {

		$rowOtherBanner = $this->callApi('OtherBanner/detailData', array(
      'id' => $this->pagesId
    ));

		if ($rowOtherBanner)
			if ($rowOtherBanner['publishStatusId'] == 2)
				return $rowOtherBanner;
	}

  public function index() {
  	$this->page(1);
  }
  
  public function page($pageActive) {
  	
  	$rowSeo = $this->callApi('Seo/detailMeta', array(
  			'id' => $this->pagesId
  	));
  	
  	$this->setSettings(array(
  			'meta' => $rowSeo,
  			'subTitle' => 'Widyaiswara',
  			'scoopy' => 'Widyaiswara',
  	));
  	
  if (isset($_SESSION['id'])){
    	$idAnggota =$_SESSION['id'];
    }else{
    	$idAnggota =0;
    }
  	
  	$this->load->model('Anggota', 'mAnggota');
  	$result = $this->mAnggota->detailData(array(
  			'id' => $idAnggota,
  	));
  	
  	$rowAnggota = $result['data'];

  	$this->load->model('Widyaiswara', 'mWidyaiswara');
  	$result = $this->mWidyaiswara->detailData();
  	$rowWidyaiswara = $result['data'];
  	
  	$this->load->model('User', 'mPengajarDiklat');
  	$result = $this->mPengajarDiklat->readDataPengajar(array(
			'page' => $pageActive,
			'count' => $this->limit,
	));	
  	
  	$rsPengajarDiklat = $result['data'];
  	$pageCount = $result['extra']['pageCount'];
  	
  	$this->render('Widyaiswara', array(
  			'rowAnggota' => $rowAnggota,
  			'rowOtherBanner' => $this->getRowOtherBanner(),
  			'rowWidyaiswara' => $rowWidyaiswara,
  			'rsPengajarDiklat' => $rsPengajarDiklat,
  			'pageCount' => $pageCount,
  			'pageActive' => $pageActive,
  	));
  }

}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SitePendaftaranDiklat extends MY_Controller {

  var $pagesId = 9997;

  private function getRowOtherBanner() {

		$rowOtherBanner = $this->callApi('OtherBanner/detailData', array(
      'id' => $this->pagesId
    ));

		if ($rowOtherBanner)
			if ($rowOtherBanner['publishStatusId'] == 2)
				return $rowOtherBanner;
	}

  public function daftarDiklat($diklatId = '') {

    if (!isset($_SESSION['id'])) {
      header("location:".base_url());
      exit;
    }

    if ($_SESSION['tipeAkunId'] == 2) {
      header("location:".base_url());
      exit;
    }

    $rowSeo = $this->callApi('Seo/detailMeta', array(
      'id' => $this->pagesId
    ));

    $this->setSettings(array(
      'meta' => $rowSeo,
      'subTitle' => 'Pendaftaran Diklat',
      'scoopy' => 'PendaftaranDiklat',
      'vars' => array(
        'AKUN_ID' => $_SESSION['id'],
      ),
    ));

    if($diklatId){
      $this->load->model('Diklat', 'mDiklat');
      $result = $this->mDiklat->detailData(array(
          'id' => $diklatId,
      ));

      $this->load->model('PendaftaranDiklat', 'mPendaftaranDiklat');
      $result1 = $this->mPendaftaranDiklat->cariAnggota(array(
          'akunId' => $_SESSION['id'],
          'diklatId' => $diklatId,
      ));
      $dataAnggota =$result1['data'];
      if($dataAnggota)
      $rowDiklat1 ='';
      else
      $rowDiklat1 = $result['data'];
    }else{
      $rowDiklat1='';  
    }
    

    $rowAnggota = $this->callApi('Anggota/detailData', array(
    		'id' => $_SESSION['id'],
    ));


    $rsPangkatGolongan = $this->callApi('PangkatGolongan/readData', array(
    		'orderBy' => 'id',
    ));

    $rsUnitKerja = $this->callApi('UnitKerja/readData', array(
    		'orderBy' => 'id',
    ));

    $rsInstansi = $this->callApi('Instansi/readData', array(
    		'orderBy' => 'instansi',
    ));

    $rsPendidikanTerakhir = $this->callApi('PendidikanTerakhir/readData', array(
    		'orderBy' => 'id',
    ));

    $rsGender = $this->callApi('Gender/readData', array(
    		'orderBy' => 'id',
    ));

    $rsAgama = $this->callApi('Agama/readData', array(
    		'orderBy' => 'id',
    ));

    
    if($rowDiklat1){
        $this->render('PendaftaranDiklat', array(
          'diklatId' => $diklatId,
          'rowDiklat1' => $rowDiklat1,
          'rowOtherBanner' => $this->getRowOtherBanner(),
          'rowAnggota' => $rowAnggota,
          'rsPangkatGolongan' => $rsPangkatGolongan,
          'rsUnitKerja' => $rsUnitKerja,
          'rsInstansi' => $rsInstansi,
          'rsPendidikanTerakhir' => $rsPendidikanTerakhir,
          'rsGender' => $rsGender,
          'rsAgama' => $rsAgama,
      ));
    }else{
      $this->render('PendaftaranDiklat', array(
          'rowDiklat1' => $rowDiklat1,
          'rowOtherBanner' => $this->getRowOtherBanner(),
      ));
    }
    
  }

}

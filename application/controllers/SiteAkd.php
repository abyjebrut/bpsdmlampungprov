<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SiteAkd extends MY_Controller {

  var $pagesId = 99910;

  private function getRowOtherBanner() {

    $rowOtherBanner = $this->callApi('OtherBanner/detailData', array(
      'id' => $this->pagesId
    ));

    if ($rowOtherBanner)
    if ($rowOtherBanner['publishStatusId'] == 2)
      return $rowOtherBanner;
  }

  public function pageList() {

    if (!isset($_SESSION['id'])) {
      header("location:".base_url());
      exit;
    }

    if ($_SESSION['tipeAkunId'] == 1) {
      header("location:".base_url());
      exit;
    }

    $rowSeo = $this->callApi('Seo/detailMeta', array(
      'id' => $this->pagesId
    ));

    $this->setSettings(array(
      'meta' => $rowSeo,
      'subTitle' => 'AKD',
      'scoopy' => 'SoalAkd',
      'vars' => array(
        'AKUN_ID' => $_SESSION['id'],
      ),
    ));

    // Extract Data

    $rowAnggota = $this->callApi('Anggota/detailData', array(
      'id' => $_SESSION['id'],
    ));

    $rsSoalAkd = $this->callApi('SoalAkd/readData', array(
      'akunId' => $_SESSION['id'],
      'orderBy' => '_tanggalPembuatan',
      'reverse' => 1,
    ));

    $rsSoalAkdJawaban = $this->callApi('SoalAkd/readDataJawaban', array(
      'akunId' => $_SESSION['id'],
      'orderBy' => '_tanggalPengisian',
      'reverse' => 1,
    ));

    $this->render('ListAkd', array(
      'rowOtherBanner' => $this->getRowOtherBanner(),
      'rowAnggota' => $rowAnggota,
      'rsSoalAkd' => $rsSoalAkd,
      'rsSoalAkdJawaban' => $rsSoalAkdJawaban,
    ));
  }

  public function pageDetail($soalAkdId = '') {

    if (!isset($_SESSION['id'])) {
      header("location:".base_url());
      exit;
    }

    $rowSeo = $this->callApi('Seo/detailMeta', array(
    'id' => $this->pagesId
    ));

    // Extract Data

    $rowAnggota = $this->callApi('Anggota/detailData', array(
    'id' => $_SESSION['id'],
    ));

    $rsTingkatKesanggupan = $this->callApi('TingkatKesanggupan/readData', array(
    'orderBy' => 'id',
    ));

    $rowSoalAkd = $this->callApi('SoalAkd/detailData', array(
    'id' => $soalAkdId,
    ));

    $this->setSettings(array(
      'meta' => $rowSeo,
      'subTitle' => 'Soal AKD',
      'scoopy' => 'SoalAkd',
      'vars' => array(
        'AKUN_ID' => $_SESSION['id'],
        'SOAL_AKD_ID' => $soalAkdId,
        'JUMLAH_SOAL' => count($rowSoalAkd['links']),
      ),
    ));

    $this->render('SoalAkd', array(
    'rowOtherBanner' => $this->getRowOtherBanner(),
    'rowAnggota' => $rowAnggota,
    'rsTingkatKesanggupan' => $rsTingkatKesanggupan,
    'rowSoalAkd' => $rowSoalAkd,
    ));
  }

}

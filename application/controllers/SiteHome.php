<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SiteHome extends MY_Controller {
	var $pagesId = 9990;

	public function index()
	{
		$rowSeo = $this->callApi('Seo/detailMeta', array(
			'id' => $this->pagesId,
		));

		$this->setSettings(array(
			'subTitle' => 'Home',
			'meta' => $rowSeo,
			'scoopy' => 'Home',
		));

		// Extract Banner Content
		$rsBanner = $this->callApi('Banner/readData', array(
			'orderBy' => 'position',
      'publishStatusId' => 2,
    ));
		
		if (isset($_SESSION['id'])){
	    	$idAnggota =$_SESSION['id'];
	    }else{
	    	$idAnggota =0;
	    }
		
		$this->load->model('Anggota', 'mAnggota');
		$result = $this->mAnggota->detailData(array(
				'id' => $idAnggota,
		));
		
		$rowAnggota = $result['data'];
		
		// Extract Social Network
		$rowSocialNetwork = $this->callApi('SocialNetwork/detailData', array(
			'id' => 1,
		));
		
		$rowFootContent = $this->callApi('FootContent/detailData', array(
				'id' => 1,
		));
		
		// Extract About Us Content
		$rowAboutUs = $this->callApi('Pages/detailData', array(
			'id' => 1,
		));
		$rsSubAboutUs = $this->callApi('SubPages/readData', array(
			'pagesId' => 1,
		));

		// Extract Our Goal Content
		$rowOurGoal = $this->callApi('Pages/detailData', array(
			'id' => 2,
		));

		// Extract Products Content
		$rsProduct = $this->callApi('Product/readData', array(
      'publishStatusId' => 2,
      'orderBy' => 'id',
      'reverse' => 1,
      'page' => 1,
      'count' => 5,
    ));

		// Extract Services Content
		$rowService = $this->callApi('Pages/detailData', array(
			'id' => 3,
		));

		// Extract Contact/Profile Content
		$rowContactProfile = $this->callApi('ContactProfile/detailData');
		

		// Output
		$this->render('Home', array(
			'rsBanner' => $rsBanner,
			'rowAnggota' => $rowAnggota,
			'rowSocialNetwork' => $rowSocialNetwork,
			'rowFootContent' => $rowFootContent,
			'rowAboutUs' => $rowAboutUs,
			'rowOurGoal' => $rowOurGoal,
			'rsSubAboutUs' => $rsSubAboutUs,
			'rsProduct' => $rsProduct,
			'rowService' => $rowService,
			'rowContactProfile' => $rowContactProfile,
		));
	}

}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SiteRekapitulasiJp extends MY_Controller {

  var $pagesId = 99913;

  private function getRowOtherBanner() {

		$rowOtherBanner = $this->callApi('OtherBanner/detailData', array(
      'id' => $this->pagesId
    ));

		if ($rowOtherBanner)
			if ($rowOtherBanner['publishStatusId'] == 2)
				return $rowOtherBanner;
	}

  public function index() {

    $rowSeo = $this->callApi('Seo/detailMeta', array(
      'id' => $this->pagesId
    ));

    $this->setSettings(array(
      'meta' => $rowSeo,
      'subTitle' => 'Rekapitulasi JP',
      'scoopy' => 'RekapitulasiJp',
    ));

    
    $this->load->model('Diklat', 'mDiklat');
    $result = $this->mDiklat->readDataRekapitulasiJp(array(
      'orderBy' => 'namaPengajar',
      'reverse' => 2,
  ));
    $rsDiklat = $result['data'];
    
    $this->load->model('JenisDiklat', 'mJenisDiklat');
    $result = $this->mJenisDiklat->readData(array(
    		'orderBy' => 'position',
    		'reverse' => 1,
    ));
    $rsJenisDiklat = $result['data'];

    if (isset($_SESSION['id'])){
    	$idAnggota =$_SESSION['id'];
    }else{
    	$idAnggota =0;
    }
  	
  	$this->load->model('Anggota', 'mAnggota');
  	$result = $this->mAnggota->detailData(array(
  			'id' => $idAnggota,
  	));
  	
  	$rowAnggota = $result['data'];
    
    $this->render('RekapitulasiJp', array(
      'rowAnggota' => $rowAnggota,
      'rowOtherBanner' => $this->getRowOtherBanner(),
    	'rsDiklat' => $rsDiklat,
    	'rsJenisDiklat' => $rsJenisDiklat,
    ));
  }

}

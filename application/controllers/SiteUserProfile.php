<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SiteUserProfile extends MY_Controller {

  var $pagesId = 9998;

  private function getRowOtherBanner() {

		$rowOtherBanner = $this->callApi('OtherBanner/detailData', array(
      'id' => $this->pagesId
    ));

		if ($rowOtherBanner)
			if ($rowOtherBanner['publishStatusId'] == 2)
				return $rowOtherBanner;
	}

  public function index() {

    if (!isset($_SESSION['id'])) {
      header("location:".base_url());
      exit;
    }

    $rowSeo = $this->callApi('Seo/detailMeta', array(
      'id' => $this->pagesId
    ));

    $this->setSettings(array(
      'meta' => $rowSeo,
      'subTitle' => 'User Profile',
      'scoopy' => 'UserProfile',
      'vars' => array(
        'AKUN_ID' => $_SESSION['id'],
      ),
    ));

	  if (isset($_SESSION['id'])){
	    	$idAnggota =$_SESSION['id'];
	    }else{
	    	$idAnggota =0;
	    }

    $this->load->model('Anggota', 'mAnggota');
    $result = $this->mAnggota->detailData(array(
    		'id' => $idAnggota,
    ));

    $rowAnggota = $result['data'];

    $this->load->model('PendaftaranDiklat', 'mPendaftaranDiklat');
    $result = $this->mPendaftaranDiklat->detailDataAnggota(array(
    		'id' => $idAnggota,
    ));
    $rsPendaftaranDiklat = $result['data'];

    $this->render('UserProfile', array(
      'rowOtherBanner' => $this->getRowOtherBanner(),
    	'rowAnggota' => $rowAnggota,
    	'rsPendaftaranDiklat' => $rsPendaftaranDiklat,
    ));
  }

  public function editProfile() {

    if (!isset($_SESSION['id'])) {
      header("location:".base_url());
      exit;
    }

    $rowSeo = $this->callApi('Seo/detailMeta', array(
      'id' => $this->pagesId
    ));

    $this->setSettings(array(
      'meta' => $rowSeo,
      'subTitle' => 'Edit Profile',
      'scoopy' => 'EditProfile',
      'vars' => array(
        'AKUN_ID' => $_SESSION['id'],
      ),
    ));

    if (isset($_SESSION['id'])){
      $idAnggota =$_SESSION['id'];
    }else{
      $idAnggota =0;
    }

    $this->load->model('Anggota', 'mAnggota');
    $result = $this->mAnggota->detailData(array(
    	'id' => $idAnggota,
    ));

    $rowAnggota = $result['data'];

    $this->load->model('PendaftaranDiklat', 'mPendaftaranDiklat');
    $result = $this->mPendaftaranDiklat->detailDataAnggota(array(
    	'id' => $idAnggota,
    ));
    $rsPendaftaranDiklat = $result['data'];

    $this->load->model('PendaftaranDiklat', 'mPendaftaranDiklat');
    $result1 = $this->mPendaftaranDiklat->detailDiklatAnggota(array(
    	'id' => $idAnggota,
    ));
    $rowDiklat1 = $result1['data'];

    $rsPangkatGolongan = $this->callApi('PangkatGolongan/readData', array(
    		'orderBy' => 'id',
    ));

    $rsUnitKerja = $this->callApi('UnitKerja/readData', array(
    		'orderBy' => 'id',
    ));

    $rsInstansi = $this->callApi('Instansi/readData', array(
    		'orderBy' => 'instansi',
    ));

    $rsPendidikanTerakhir = $this->callApi('PendidikanTerakhir/readData', array(
    		'orderBy' => 'id',
    ));

    $rsGender = $this->callApi('Gender/readData', array(
    		'orderBy' => 'id',
    ));

    $rsAgama = $this->callApi('Agama/readData', array(
    		'orderBy' => 'id',
    ));

    $this->render('EditProfile', array(
    		'rsPangkatGolongan' => $rsPangkatGolongan,
    		'rowOtherBanner' => $this->getRowOtherBanner(),
    		'rowAnggota' => $rowAnggota,
    		'rsPendaftaranDiklat' => $rsPendaftaranDiklat,
    		'rowDiklat1' => $rowDiklat1,
    		'rsUnitKerja' => $rsUnitKerja,
    		'rsInstansi' => $rsInstansi,
    		'rsPendidikanTerakhir' => $rsPendidikanTerakhir,
    		'rsGender' => $rsGender,
    		'rsAgama' => $rsAgama,
    ));
  }

}

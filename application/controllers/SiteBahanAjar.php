<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SiteBahanAjar extends MY_Controller {

  var $pagesId = 9993;

  private function getRowOtherBanner() {

		$rowOtherBanner = $this->callApi('OtherBanner/detailData', array(
      'id' => $this->pagesId
    ));

		if ($rowOtherBanner)
			if ($rowOtherBanner['publishStatusId'] == 2)
				return $rowOtherBanner;
	}

  public function index() {

    $rowSeo = $this->callApi('Seo/detailMeta', array(
      'id' => $this->pagesId
    ));

    $this->setSettings(array(
      'meta' => $rowSeo,
      'subTitle' => 'Bahan Ajar',
      'scoopy' => 'BahanAjar',
    ));

    $this->load->model('BahanAjar', 'mBahanAjar');
    $result = $this->mBahanAjar->detailData();
    $rowBahanAjar = $result['data'];
    
    $this->load->model('ArsipMateri', 'mArsipMateri');
    $result = $this->mArsipMateri->readData(array(
    		'orderBy' => 'mataDiklat',
    		'publishStatusId' => 2,
    		'reverse' => 1,

    ));
    
    $rsArsipMateri = $result['data'];
    
    $this->load->model('JenisDiklat', 'mJenisDiklat');
    $result = $this->mJenisDiklat->readData(array(
    		'orderBy' => 'position',
    		'reverse' => 1,
    ));
    $rsJenisDiklat = $result['data'];
    
    if (isset($_SESSION['id'])){
    	$idAnggota =$_SESSION['id'];
    }else{
    	$idAnggota =0;
    } 
    
	    $this->load->model('Anggota', 'mAnggota');
	    $result = $this->mAnggota->detailData(array(
	    		'id' => $idAnggota,
	    ));
    
    $rowAnggota = $result['data'];
    
    
    $this->render('BahanAjar', array(
    		'rowAnggota' => $rowAnggota,
      		'rowOtherBanner' => $this->getRowOtherBanner(),
    		'rowBahanAjar' => $rowBahanAjar,
    		'rsArsipMateri' => $rsArsipMateri,
    		'rsJenisDiklat' => $rsJenisDiklat,
    ));
  }

}

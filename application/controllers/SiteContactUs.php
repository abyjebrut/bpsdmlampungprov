<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SiteContactUs extends MY_Controller {

  var $pagesId = 9996;

  private function getRowOtherBanner() {

		$rowOtherBanner = $this->callApi('OtherBanner/detailData', array(
      'id' => $this->pagesId
    ));

		if ($rowOtherBanner)
			if ($rowOtherBanner['publishStatusId'] == 2)
				return $rowOtherBanner;
	}

  public function index() {

    $rowSeo = $this->callApi('Seo/detailMeta', array(
      'id' => $this->pagesId
    ));


    // Extract data

    $rowContactProfile = $this->callApi('ContactProfile/detailData');
    $this->setSettings(array(
      'meta' => $rowSeo,
      'subTitle' => 'Contact Us',
      'scoopy' => 'ContactUs',
      'vars' => array(
        'LONGITUDE' => $rowContactProfile['longitude'],
        'LATITUDE' => $rowContactProfile['latitude'],
      ),
    ));

    // Output

    $this->render('ContactUs', array(
      'rowOtherBanner' => $this->getRowOtherBanner(),
      'rowContactProfile' => $rowContactProfile,
    ));
  }

}

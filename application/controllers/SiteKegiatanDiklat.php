<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SiteKegiatanDiklat extends MY_Controller {

  var $pagesId = 9992;

  private function getRowOtherBanner() {

		$rowOtherBanner = $this->callApi('OtherBanner/detailData', array(
      'id' => $this->pagesId
    ));

		if ($rowOtherBanner)
			if ($rowOtherBanner['publishStatusId'] == 2)
				return $rowOtherBanner;
	}

  public function index() {


    $rowSeo = $this->callApi('Seo/detailMeta', array(
      'id' => $this->pagesId
    ));

    if (isset($_SESSION['id'])){
      $idAnggota =$_SESSION['id'];
    }else{
      $idAnggota =0;
    }

    $this->setSettings(array(
      'meta' => $rowSeo,
      'subTitle' => 'Kegiatan Diklat',
      'scoopy' => 'KegiatanDiklat',
      'vars' => array(
        'AKUN_ID' => $idAnggota,
      ),
    ));

    $this->load->model('KegiatanDiklat', 'mKegiatanDiklat');
    $result = $this->mKegiatanDiklat->detailData();
    $rowKegiatanDiklat = $result['data'];
    
    $this->load->model('Diklat', 'mDiklat');
    $result = $this->mDiklat->readData(array(
    		'orderBy' => '_pelaksanaanMulai',
    		'publishStatusId' => 2,
    		'reverse' => 1,
    ));
    
    $rsDiklat = $result['data'];
    
    $this->load->model('JenisDiklat', 'mJenisDiklat');
    $result = $this->mJenisDiklat->readData(array(
    		'orderBy' => 'position',
    		'reverse' => 1,
    ));
    $rsJenisDiklat = $result['data'];
    
	  if (isset($_SESSION['id'])){
	    	$idAnggota =$_SESSION['id'];
	    }else{
	    	$idAnggota =0;
	    }
    
    $this->load->model('Anggota', 'mAnggota');
    $result = $this->mAnggota->detailData(array(
    		'id' => $idAnggota,
    ));
    
    $rowAnggota = $result['data'];
    
    
    $this->render('KegiatanDiklat', array(
    	'rowAnggota' => $rowAnggota,
      'rowOtherBanner' => $this->getRowOtherBanner(),
    	'rowKegiatanDiklat' => $rowKegiatanDiklat,
    	'rsDiklat' => $rsDiklat,
    	'rsJenisDiklat' => $rsJenisDiklat,
    ));
  }

}

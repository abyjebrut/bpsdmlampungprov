<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends MY_Controller {

	public function post($className, $methodName)
	{
    $this->load->model($className, 'mApi');

    $this->mApi->setApiRequest(true);
    $result = $this->mApi->$methodName();

    $this->json($result);
	}
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SiteVerifikasi extends MY_Controller {

  var $pagesId = 9999;

  private function getRowOtherBanner() {

		$rowOtherBanner = $this->callApi('OtherBanner/detailData', array(
      'id' => $this->pagesId
    ));

		if ($rowOtherBanner)
			if ($rowOtherBanner['publishStatusId'] == 2)
				return $rowOtherBanner;
	}

  public function resetPassword($token) {

    $this->load->model('Anggota');
    $tokenAvailable = $this->Anggota->checkForgotToken($token);

    if (!$tokenAvailable)
      header("location:../../");

    // Output

    $this->setSettings(array(
      'subTitle' => 'Reset Kata Sandi',
      'scoopy' => 'ResetPassword',
      'vars' => array(
        'TOKEN' => $token,
      ),
    ));

    $this->render('ResetPassword', array(
      'rowOtherBanner' => $this->getRowOtherBanner(),
    ));
  }

  public function resetBerhasil() {

    // Output

    $this->setSettings(array(
      'subTitle' => 'Reset Kata Sandi Berhasil',
      'scoopy' => '',
    ));

    $this->render('ResetBerhasil', array(
      'rowOtherBanner' => $this->getRowOtherBanner(),
    ));
  }

  public function aktifasi($token) {

    $this->load->model('Anggota');
    $_verifikasi = $this->Anggota->verifikasiToken(array(
      'token' => $token,
    ));

    if ($_verifikasi['status'] == 'success')
      header("location:../berhasil");
    else
      die('Verifikasi akun gagal!');
  }

  public function verifikasiBerhasil() {

    // Output
    if (isset($_SESSION['id'])){
      $idAnggota =$_SESSION['id'];
    }else{
      $idAnggota =0;
    }

    $this->load->model('Anggota', 'mAnggota');
		$result = $this->mAnggota->detailData(array(
				'id' => $idAnggota,
		));
		
		$rowAnggota = $result['data'];

    $this->setSettings(array(
      'subTitle' => 'Verifikasi Akun Berhasil',
      'scoopy' => '',
    ));

    $this->render('VerifikasiBerhasil', array(
			'rowAnggota' => $rowAnggota,
      'rowOtherBanner' => $this->getRowOtherBanner(),
    ));
  }

}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SiteSttplAlumni extends MY_Controller {

  var $pagesId = 9994;

  private function getRowOtherBanner() {

    $rowOtherBanner = $this->callApi('OtherBanner/detailData', array(
      'id' => $this->pagesId
    ));

    if ($rowOtherBanner)
    if ($rowOtherBanner['publishStatusId'] == 2)
    return $rowOtherBanner;
  }

  public function index() {

    $rowSeo = $this->callApi('Seo/detailMeta', array(
      'id' => $this->pagesId
    ));

    $this->setSettings(array(
      'meta' => $rowSeo,
      'subTitle' => 'STTPL/Alumni',
      'scoopy' => 'SttplAlumni',
    ));

    $this->load->model('SttplAlumni', 'mSttplAlumni');
    $result = $this->mSttplAlumni->detailData();
    $rowSttplAlumni = $result['data'];

    $rsKelulusan = $this->callApi('Diklat/readDataAlumni', array(
    		'orderBy' => 'namaDiklatKode',
    		'reverse' => 1,
    ));

    $rsJenisDiklat = $this->callApi('JenisDiklat/readData', array(
    		'orderBy' => 'position',
      		'reverse' => 1,
    ));

    if (isset($_SESSION['id'])){
      $idAnggota =$_SESSION['id'];
    }else{
      $idAnggota =0;
    }

    $rowAnggota = $this->callApi('Anggota/detailData', array(
      'id' => $idAnggota,
    ));

    $this->render('SttplAlumni', array(
      'rowAnggota' => $rowAnggota,
      'rowOtherBanner' => $this->getRowOtherBanner(),
      'rowSttplAlumni' => $rowSttplAlumni,
      'rsKelulusan' => $rsKelulusan,
      'rsJenisDiklat' => $rsJenisDiklat,
    ));
  }

  public function detailAlumni($diklatId) {
    $rowSeo = $this->callApi('Seo/detailMeta', array(
      'id' => $this->pagesId
    ));

    if (isset($_SESSION['id'])){
      $idAnggota =$_SESSION['id'];
    }else{
      $idAnggota =0;
    }

    $rowAnggota = $this->callApi('Anggota/detailData', array(
      'id' => $idAnggota,
    ));

    $rsKelulusan = $this->callApi('Kelulusan/readDataAlumni', array(
    		'diklatId' => $diklatId,
    ));

    $rowDiklat = $this->callApi('Diklat/detailData', array(
    		'id' => $diklatId,
    ));

    $this->setSettings(array(
      'meta' => $rowSeo,
      'subTitle' => 'Detail Alumni',
      'scoopy' => 'DetailAlumni',
      'vars' => array(
        'DIKLAT_ID' => $diklatId,
      ),
    ));

    $this->render('DetailAlumni', array(
	    'rowDiklat' => $rowDiklat,
	    'rowAnggota' => $rowAnggota,
	    'rsKelulusan' => $rsKelulusan,
      'rowOtherBanner' => $this->getRowOtherBanner(),
      // 'rowSttplAlumni' => $rowSttplAlumni,
      // 'rsKelulusan' => $rsKelulusan,
      // 'rsJenisDiklat' => $rsJenisDiklat,
    ));
  }
}

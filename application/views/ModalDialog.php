<div data-scope="Signup" id="buatAkun" class="modal">
  <div class="modal-dialog ezCustTrans">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h4 class="modal-title">Pendaftaran Akun Baru</h4>
      </div>
      <div class="modal-body">
        <div class="input-group">
          <span class="input-group-addon"><i class="fa fa-user"></i></span>
          <input data-ref="nip" type="text" class="form-control" name="NIP" placeholder="NIP">
        </div>
        <div class="input-group">
          <span class="input-group-addon"><i class="fa fa-user"></i></span>
          <input data-ref="ulangiNip" type="text" class="form-control" name="NIP" placeholder="Ulangi NIP">
        </div>
        <div class="input-group">
          <span class="input-group-addon"><i class="fa fa-user"></i></span>
          <input data-ref="nama" type="text" class="form-control" name="Nama" placeholder="Nama">
        </div>
        <div class="input-group">
          <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
          <input data-ref="email" type="text" class="form-control" name="Email" placeholder="Email">
        </div>
        <div class="input-group">
          <span class="input-group-addon"><i class="fa fa-lock"></i></span>
          <input data-ref="kataSandi" type="password" class="form-control" name="password" placeholder="Kata Sandi">
        </div>
        <div class="input-group">
          <span class="input-group-addon"><i class="fa fa-lock"></i></span>
          <input data-ref="ulangiKataSandi" type="password" class="form-control" name="password" placeholder="Ulangi Kata Sandi">
        </div>
        <div class="modal-footer">
          <button data-ref="btnKirim" type="button" class="btn btn-primary btn-login">Kirim</button>
          <button data-ref="btnBatal" type="cancel" class="btn btn-default btn-login" data-dismiss="modal">Batal</button>
        </div>
      </div>
    </div>
  </div>
</div>


<div data-scope="ForgotPassword" id="lupaSandi" class="modal">
  <div class="modal-dialog ezCustTrans">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h4 class="modal-title">Konfirmasi Lupa Kata Sandi</h4>
      </div>
      <div class="modal-body">
        <div class="input-group">
          <span class="input-group-addon"><i class="fa fa-user"></i></span>
          <input data-ref="nip" type="text" class="form-control" name="NIP" placeholder="NIP">
        </div>
        <div class="input-group">
          <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
          <input data-ref="email" type="text" class="form-control" name="Email" placeholder="Email">
        </div>
        <br>
        <span class="help-block">**Konfirmasi persetujuan kata sandi akan dikirim melalui e-mail yang sah terkait akun, setelah ini silahkan cek e-mail Anda untuk proses berikutnya.</span>
        <div class="modal-footer">
          <button data-ref="btnKirim" type="button" class="btn btn-primary btn-login">Kirim</button>
          <button data-ref="btnBatal" type="cancel" class="btn btn-default btn-login" data-dismiss="modal">Batal</button>
        </div>
      </div>
    </div>
  </div>
</div>

<div data-scope="ReEmail" id="reEmail" class="modal">
  <div class="modal-dialog ezCustTrans">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h4 class="modal-title">Verifikasi Ulang Email</h4>
      </div>
      <div class="modal-body">
        <div class="input-group">
          <span class="input-group-addon"><i class="fa fa-user"></i></span>
          <input data-ref="nip" type="text" class="form-control" name="NIP" placeholder="NIP">
        </div>
        <div class="input-group">
          <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
          <input data-ref="email" type="text" class="form-control" name="Email" placeholder="Email">
        </div>
        <br>
        <span class="help-block">**Konfirmasi persetujuan kata sandi akan dikirim melalui e-mail. Silahkan cek kotak masuk Email Anda untuk proses berikutnya.</span>
        <div class="modal-footer">
          <button data-ref="btnKirim" type="button" class="btn btn-primary btn-login">Kirim</button>
          <button data-ref="btnBatal" type="cancel" class="btn btn-default btn-login" data-dismiss="modal">Batal</button>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- <div data-scope="AlerLogin" id="alertLogin" class="modal">
  <div class="modal-dialog ezCustTrans">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h4 class="modal-title">Informasi</h4>
      </div>
      <div class="modal-body">
        <span class="help-block">Login / Registrasi Dahulu Baru anda bisa Daftar</span>
        <div class="modal-footer">
          <button data-ref="btnBatal" type="cancel" class="btn btn-default btn-login" data-dismiss="modal">Ok</button>
        </div>
      </div>
    </div>
  </div>
</div> -->

<div data-scope="AlerLogin">    
      <div class="modal fade" id="alertLogin" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-box">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <div class="modal-body">
                        <div class="icon"><i class="fa fa-check"></i></div>
                        <p class="description">Anda harus <a class="button-daftar-modal" href="#" data-toggle="modal" data-target="#buatAkun" data-dismiss="modal">Buat Akun</a> terlebih dahulu untuk mendaftar atau <a class="button-daftar-modal" href="">Login</a> untuk melanjutkan
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Judul Laporan -->
<div data-scope="judulLaporan" id="judulLaporan" class="modal">
    <div class="modal-dialog ezCustTrans">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h4 class="modal-title">Judul Laporan</h4>
            </div>
            <div class="modal-body">
                <div class="input-group" style='display:none'>
                <!-- <div class="input-group"> -->
                  <span class="input-group-addon"><i class="fa fa-user"></i></span>
                  <input data-ref="pendaftaranId" type="text" class="form-control" name="DiklatId" placeholder="Diklat ID">
                </div>
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-book"></i></span>
                    <!-- <input type="text" class="form-control" name="Judul" placeholder="ketik judul disini ..."> -->
                    <textarea class="form-control" data-ref="judul" name="Judul" placeholder="ketik judul disini ..." rows="4"></textarea>
                </div>
                <br>
                <div class="modal-footer">
                    <button data-ref="btnLaporan" type="button" class="btn btn-primary btn-login">Simpan</button>
                    <button data-ref="btnBatalLaporan" type="cancel" class="btn btn-default btn-login" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Judul Laporan -->
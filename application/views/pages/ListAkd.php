<!-- Banner -->
<?php if($rowOtherBanner): ?>
  <div class="container-fluid">
    <div class="row section-pages-banner">
      <div class="col-md-12">
        <div class="banner-box">
          <div class="image-item">
            <img
            src="<?= base_url().'asset/archive/'.$rowOtherBanner['banner'] ?>"
            alt="<?= $rowOtherBanner['title'] ?>"
            class="img-responsive image-bg">
            <div class="overlay"></div>
          </div>
          <div class="banner-title">
            <h2 class="wow fadeIn" data-wow-delay=".4"><?= $rowOtherBanner['title'] ?></h2>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>
<!-- End Banner -->

<div class="container-fluid">
  <div class="row section-otp-content">
    <div class="col-xs-12 rata">
      <div class="col-xs-12 col-sm-4 reset-col main-left-col">
        <div class="col-md-12 reset-col side-bar-tp">
          <?php $this->load->view('Login') ?>
          <div class="clearfix"></div>
          <?php $this->load->view('GuestBook') ?>
        </div>
      </div>
      <div class="col-xs-12 col-sm-8 reset-col main-right-col">
        <div class="col-md-12 wrap-side-content wow fadeIn" data-wow-delay=".4">
          <div class="col-md-12 side-content">

            <div class="col-md-12 reset-col">
              <div class="breadcrumb-title">
                <ul>
                  <li><a href="index.html"><i class="fa fa-home"></i></a></li>
                  <li><i class="fa fa-angle-right"></i><i class="fa fa-angle-right"></i></li>
                  <li><a href="<?= base_url() ?>"> Beranda</a></li>
                  <li><i class="fa fa-angle-right"></i><i class="fa fa-angle-right"></i></li>
                  <li class="active"><?= $settings['subTitle'] ?></li>
                </ul>
              </div>
            </div>

            <?php if(count($rsSoalAkd) > 0): ?>
            <div class="col-md-12 reset-col wrap-ac-brief">
              <h4>Anda memiliki <?= count($rsSoalAkd) ?> soal belum terjawab;</h4>
              <div id="no-more-tables">
                <table class="col-md-12 reset-col table-striped table-condensed cf">
                  <thead class="cf">
                    <tr>
                      <th class="numbers">NO</th>
                      <th class="numbers">TANGGAL PEMBUATAN</th>
                      <th class="numbers">JAWAB</th>
                    </tr>
                  </thead>
                  <tbody data-ref="listData">
                  <?php
                  $num = 0;
                  foreach ($rsSoalAkd as $rowSoalAkd):
                    $num++;
                  ?>
                    <tr>
                      <td data-title="No" class="numbers"><?= $num ?>.</td>
                      <td data-title="Tanggal Pembuatan"><?= $rowSoalAkd['tanggalPembuatan'] ?></td>
                      <td data-title="Tanggal Pengisian" class="numbers">
                        <a data-ref="linkCetak" data-id="<?= $rowSoalAkd['id'] ?>" href="<?= base_url() ?>akd/<?= $rowSoalAkd['id'] ?>">
                          Buka Soal
                        </a>
                      </td>
                    </tr>
                  <?php endforeach; ?>
                  </tbody>
                </table>
              </div>
            </div>
          <?php endif; ?>

          <?php if(count($rsSoalAkdJawaban) > 0): ?>
          <div class="col-md-12 reset-col wrap-ac-brief">
            <h4>Anda memiliki <?= count($rsSoalAkdJawaban) ?> soal yg sudah dijawab;</h4>
            <div id="no-more-tables">
              <table class="col-md-12 reset-col table-striped table-condensed cf">
                <thead class="cf">
                  <tr>
                    <th class="numbers">NO</th>
                    <th class="numbers">TANGGAL PEMBUATAN</th>
                    <th class="numbers">TANGGAL PENGISIAN</th>
                    <th class="numbers">PDF</th>
                  </tr>
                </thead>
                <tbody data-ref="listData">
                <?php
                $num = 0;
                foreach ($rsSoalAkdJawaban as $rowSoalAkdJawaban):
                  $num++;
                ?>
                  <tr>
                    <td data-title="No" class="numbers"><?= $num ?>.</td>
                    <td data-title="Tanggal Pembuatan"><?= $rowSoalAkdJawaban['tanggalPembuatan'] ?></td>
                    <td data-title="Tanggal Pengisian"><?= $rowSoalAkdJawaban['tanggalPengisian'] ?></td>
                    <td data-title="PDF" class="numbers">
                      <a data-ref="btnPdf" data-id="<?= $rowSoalAkdJawaban['id'] ?>" href="javascript:">
                        <i class="fa fa-file-pdf-o" aria-hidden="true"></i>
                      </a>
                    </td>
                  </tr>
                <?php endforeach; ?>
                </tbody>
              </table>
            </div>
          </div>
        <?php endif; ?>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>

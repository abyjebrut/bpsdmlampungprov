<!-- Banner -->
<?php if($rowOtherBanner): ?>
  <div class="container-fluid">
    <div class="row section-pages-banner">
      <div class="col-md-12">
        <div class="banner-box">
          <div class="image-item">
            <img
            src="<?= base_url().'asset/archive/'.$rowOtherBanner['banner'] ?>"
            alt="<?= $rowOtherBanner['title'] ?>"
            class="img-responsive image-bg">
            <div class="overlay"></div>
          </div>
          <div class="banner-title">
            <h2 class="wow fadeIn" data-wow-delay=".4"><?= $rowOtherBanner['title'] ?></h2>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>
<!-- End Banner -->


<div class="container-fluid">
  <div class="row section-otp-content">
    <div class="col-xs-12 rata">
      <div class="col-xs-12 col-sm-4 reset-col main-left-col">
        <div class="col-md-12 reset-col side-bar-tp">
          <?php $this->load->view('Login') ?>
          <div class="clearfix"></div>
          <?php $this->load->view('GuestBook') ?>
        </div>
      </div>
      <div class="col-xs-12 col-sm-8 reset-col main-right-col">
        <div class="col-md-12 wrap-side-content wow fadeIn" data-wow-delay=".4">
          <div class="col-md-12 side-content">

            <div class="col-md-12 reset-col">
              <div class="breadcrumb-title">
                <ul>
                  <li><a href="index.html"><i class="fa fa-home"></i></a></li>
                  <li><i class="fa fa-angle-right"></i><i class="fa fa-angle-right"></i></li>
                  <li><a href="<?= base_url() ?>"> Beranda</a></li>
                  <li><i class="fa fa-angle-right"></i><i class="fa fa-angle-right"></i></li>
                  <li class="active"><?= $settings['subTitle'] ?></li>
                </ul>
              </div>
            </div>

            <div class="title-custom">
              <div class="four">
                  <h1><span>REKAPITULASI</span> Jam <em>Ajar</em> Widyaiswara</h1>
              </div>
          </div>
          <div class="col-md-12 reset-col wrap-ac-brief">
            <div class="col-md-12 reset-col combo-pencarian">

              <div>
                <h5>Pencarian Data:</h5>
              </div>
              <div class="col-md-7">
                <select class="form-control" data-ref="jenisDiklat">
                  <option value="-1" data-hidden="true">--Jenis Diklat--</option>
                	<?php foreach ($rsJenisDiklat as $rowJenisDiklat): ?>
                    <option value="<?= $rowJenisDiklat['id'] ?>"><?= $rowJenisDiklat['jenisDiklat'] ?></option>
                  <?php endforeach; ?>
                </select>
              </div>

              <span class="input-group col-md-5">
                <span class="input-group-addon"><i class="fa fa-search"></i></span>
                <input data-ref="keyword" class="form-control" placeholder="Nama Widyaiswara" type="text">
              </span>
            </div>
            <div id="no-more-tables" class="tb-rekapitulasi">
              <table class="col-md-12 reset-col table-striped table-condensed cf">
                <thead class="cf">
                <tr>
                    <th class="numbers">NO</th>
                    <th class="numbers">WIDYAISWARA</th>
                    <th class="numbers">Jumlah JP</th>
                </tr>
                </thead>
                <tbody data-ref="listData">
                <?php
                $num = 0;
                foreach ($rsDiklat as $rowDiklat):
                  $num++;
                ?>
                  <tr>
                    <td data-title="No" class="numbers"><?= $num ?>.</td>
                    <td data-title="Widyaiswara">
                        <img class="img-tb-rekap" src="<?= base_url().'asset/archive/'.$rowDiklat['fotoPengajar'] ?>" alt="">
                        <span><?= $rowDiklat['namaPengajar'] ?></span>
                    </td>
                    <td data-title="Jumlah JP" class="numbers"><?= $rowDiklat['jumlahJp'] ?></td>

                  </tr>
                  <?php endforeach; ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>

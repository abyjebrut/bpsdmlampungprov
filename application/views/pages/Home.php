<div class="container-fluid">
  <div class="row section-pages-banner">
    <div class="col-md-12">
      <div class="banner-box">
        <div class="image-item">
          <img src="asset/diklat/img/banner-bg.png" alt="" class="img-responsive image-bg">
          <div class="overlay"></div>
        </div>
        <div class="banner-title">
          <h2 class="wow fadeIn" data-wow-delay=".4">BERANDA</h2>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid">
  <div class="row section-otp-content">
    <div class="col-xs-12 rata">
      <div class="col-xs-12 col-sm-4 reset-col main-left-col">
        <div class="col-md-12 reset-col side-bar-tp">
          <?php $this->load->view('Login') ?>

          <div class="clearfix"></div>
          <div class="panel panel-default">
            <div class="panel-heading single">
              <a class="active" href="#">Menu Utama</a>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading single">
              <a href="#">Persyaratan1</a>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading single">
              <a href="#">Cara Mendaftar</a>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading single">
              <a href="form-pendaftaran.html">Daftar</a>
            </div>
          </div>
          <div class="clearfix"></div>

          <?php $this->load->view('GuestBook') ?>
        </div>
      </div>
      <div class="col-xs-12 col-sm-8 reset-col main-right-col">
        <div class="col-md-12 wrap-side-content wow fadeIn" data-wow-delay=".4">
          <div class="col-md-12 side-content">
            <?php $this->load->view('Breadcrumb') ?>

            <div class="tinymce-title">
              <h2>MUKADIMAH</h2>
            </div>
            <br>
            <p>Selamat datang calon peserta diklat. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur eius numquam dolor unde alias voluptas esse non consectetur libero, sunt id natus in, modi odit asperiores nisi deserunt quidem iure!</p>
            <p>Selamat datang calon peserta diklat. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur eius numquam dolor unde alias voluptas esse non consectetur libero, sunt id natus in, modi odit asperiores nisi deserunt quidem iure! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ratione odit iusto itaque. Temporibus molestiae magni rem error amet voluptates officia quo minus at fugit, rerum soluta adipisci, consequuntur, voluptatibus. Ea.</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Banner -->
<?php if($rowOtherBanner): ?>
  <div class="container-fluid">
    <div class="row section-pages-banner">
      <div class="col-md-12">
        <div class="banner-box">
          <div class="image-item">
            <img
            src="<?= base_url().'asset/archive/'.$rowOtherBanner['banner'] ?>"
            alt="<?= $rowOtherBanner['title'] ?>"
            class="img-responsive image-bg">
            <div class="overlay"></div>
          </div>
          <div class="banner-title">
            <h2 class="wow fadeIn" data-wow-delay=".4"><?= $rowOtherBanner['title'] ?></h2>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>
<!-- End Banner -->

<div class="container-fluid">
  <div class="row section-contact-us-content section-contact">
    <div class="col-lg-12 wrap-side-content reset-col wow fadeIn" data-wow-delay=".4">
      <div class="col-md-12" style="padding-left:30px; padding-right:35px;">

        <div class="col-md-12 reset-col">
          <div class="breadcrumb-title">
            <ul>
              <li><a href="index.html"><i class="fa fa-home"></i></a></li>
              <li><i class="fa fa-angle-right"></i><i class="fa fa-angle-right"></i></li>
              <li><a href="<?= base_url() ?>"> Beranda</a></li>
              <li><i class="fa fa-angle-right"></i><i class="fa fa-angle-right"></i></li>
              <li class="active"><?= $settings['subTitle'] ?></li>
            </ul>
          </div>
        </div>

      </div>
      <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 left-content">
        <div class="col-md-12 title-left">
          <hr>
          <h4 class="wow fadeIn" data-wow-delay=".4">INFO</h4>
          <h3 class="wow fadeIn" data-wow-delay=".4">CONTACT US</h3>
        </div>
        <div class="col-md-12 detail-contact wow fadeIn" data-wow-delay=".3">
          <hr>
          <h4>ALAMAT</h4>
          <p>
	          <?php foreach ($rowContactProfile['address'] as $address): ?>
		        <?= $address['caption'] ?>: <?= $address['address'] ?><br>
		      <?php endforeach; ?>
          </p>
        </div>
        <div class="col-md-12 detail-contact wow fadeIn" data-wow-delay=".4">
          <hr>
          <h4>HUBUNGI KAMI</h4>
          <p>
            <?php foreach ($rowContactProfile['phoneNumbers'] as $phoneNumbers): ?>
	        <?= $phoneNumbers['caption'] ?>:
	        <a href="tel:<?= $phoneNumbers['phoneNumber'] ?>"><?= $phoneNumbers['phoneNumber'] ?></a>
	        <br>
	      <?php endforeach; ?>
          </p>
        </div>
        <div class="col-md-12 detail-contact wow fadeIn" data-wow-delay=".2">
          <hr>
          <h4>EMAIL</h4>
          <p>
            <?php foreach ($rowContactProfile['emails'] as $emails): ?>
		        <?= $emails['caption'] ?>:
		        <a href="mailto:<?= $emails['email'] ?>"><?= $emails['email'] ?></a>
		        <br>
	      	<?php endforeach; ?>
          </p>
        </div>
      </div>
      <div class="col-lg-1 col-md-1"></div>
      <div data-scope="GuestBook" class="col-lg-5 col-md-11 col-sm-11 col-xs-11">
        <div class="col-md-12 title-right">
          <hr>
          <h4 class="wow fadeIn" data-wow-delay=".4">CONTACT</h4>
          <h3 class="wow fadeIn" data-wow-delay=".4">FORM</h3>
        </div>
        <div class="col-md-12 form-contact wow fadeIn" data-wow-delay=".4">
          <div class="form-group">
            <label class="sr-only" for="c_name">Nama</label>
            <input data-ref="name" type="text" id="c_name" class="form-control" name="c_name" placeholder="Nama">
          </div>
          <div class="form-group">
            <label class="sr-only" for="c_email">Email</label>
            <input data-ref="email" type="email" id="c_email" class="form-control" name="c_email" placeholder="E-mail">
          </div>
          <div class="form-group">
            <label class="sr-only" for="c_subject">Judul</label>
            <input data-ref="subject" type="subject" id="c_subject" class="form-control" name="c_subject" placeholder="Subjek">
          </div>
          <div class="form-group">
            <textarea
            data-ref="message"
            class="form-control"
            id="c_message"
            name="c_message"
            rows="7"
            placeholder="Pesan"></textarea>
          </div>
          <button data-ref="btnSendContactUs" type="button" class="btn btn-default" data-wow-delay=".4s">
            KIRIM <i class="wow fadeIn fa fa-paper-plane"></i>
          </button>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
  <div id="location" class="row maps">
    <img class="maps_image" src="asset/diklat/img/index/map.png">
    <div id="map"><?= $rowContactProfile['latitude'] ?><?= $rowContactProfile['longitude'] ?></div>
  </div>
</div>

<!-- Banner -->
<?php if($rowOtherBanner): ?>
  <div class="container-fluid">
    <div class="row section-pages-banner">
      <div class="col-md-12">
        <div class="banner-box">
          <div class="image-item">
            <img
            src="<?= base_url().'asset/archive/'.$rowOtherBanner['banner'] ?>"
            alt="<?= $rowOtherBanner['title'] ?>"
            class="img-responsive image-bg">
            <div class="overlay"></div>
          </div>
          <div class="banner-title">
            <h2 class="wow fadeIn" data-wow-delay=".4"><?= $rowOtherBanner['title'] ?></h2>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>
<!-- End Banner -->


<div class="container-fluid">
  <div class="row section-otp-content">
    <div class="col-xs-12 rata">
      <div class="col-xs-12 col-sm-4 reset-col main-left-col">
        <div class="col-md-12 reset-col side-bar-tp">
          <?php $this->load->view('Login') ?>
          <div class="clearfix"></div>
          <?php $this->load->view('GuestBook') ?>
        </div>
      </div>
      <div class="col-xs-12 col-sm-8 reset-col main-right-col">
        <div class="col-md-12 wrap-side-content wow fadeIn" data-wow-delay=".4">
          <div class="col-md-12 side-content">

            <div class="col-md-12 reset-col">
              <div class="breadcrumb-title">
                <ul>
                  <li><a href="index.html"><i class="fa fa-home"></i></a></li>
                  <li><i class="fa fa-angle-right"></i><i class="fa fa-angle-right"></i></li>
                  <li><a href="<?= base_url() ?>"> Beranda</a></li>
                  <li><i class="fa fa-angle-right"></i><i class="fa fa-angle-right"></i></li>
                  <li class="active"><?= $settings['subTitle'] ?></li>
                </ul>
              </div>
            </div>

          <div class="tinymce-ac-brief-title">
            <h2><?= $rowKegiatanDiklat['judul'] ?></h2>
          </div>
          <div>
			<?= $rowKegiatanDiklat['pengantar'] ?>
          </div>
          <div class="col-md-12 reset-col wrap-ac-brief">

           <div class="col-md-12 reset-col combo-pencarian">
              <div>
                  <h5>Pencarian Data:</h5>
              </div>
              <div class="col-md-7 cb-filter">
                <select class="form-control" data-ref="jenisDiklat">
                    <option value="-1" data-hidden="true">--Jenis Diklat--</option>
                    <?php foreach ($rsJenisDiklat as $rowJenisDiklat): ?>
                      <option value="<?= $rowJenisDiklat['id'] ?>"><?= $rowJenisDiklat['jenisDiklat'] ?></option>
                    <?php endforeach; ?>
                  </select>
              </div>
              <div class="col-md-5 input-group tb-filter">
                <span class="input-group col-md-12">
                  <span class="input-group-addon"><i class="fa fa-search"></i></span>
                  <input data-ref="keyword" class="form-control" placeholder="Kode / Nama Diklat" type="text">
                </span>
              </div>
            </div>

            <div id="no-more-tables">
              <?php // print_r($_SESSION) ?>
              <table class="col-md-12 reset-col table-striped table-condensed cf">
                <thead class="cf">
                  <tr>
                    <th class="numbers">NO</th>
                    <th class="numbers">KODE</th>
                    <th class="numbers">NAMA DIKLAT</th>
                    <th class="numbers">PELAKSANAAN</th>
                    <th class="numbers">KUOTA</th>
                    <th class="numbers">STATUS</th>
                    <th class="numbers">DAFTAR</th>

                  </tr>
                </thead>
                <tbody data-ref="listData">
                <?php
                $num = 0;
                foreach ($rsDiklat as $rowDiklat):
                  $num++;
                ?>
                  <tr>
                    <td data-title="No" class="numbers"><?= $num ?>.</td>
                    <td data-title="Kode"><?= $rowDiklat['kode'] ?></td>
                    <td data-title="Nama Diklat"><?= $rowDiklat['namaDiklat'] ?></td>
                    <td data-title="Pelaksanaan"><?= $rowDiklat['pelaksanaanMulai'] ?> s.d. <?= $rowDiklat['pelaksanaanSelesai'] ?></td>
                    <td data-title="Kuota"><?= $rowDiklat['kuota'] ?></td>
                    <td data-title="Status"><?= $rowDiklat['statusDiklat'] ?></td>

                    <td data-title="Daftar" class="numbers">
                          <?php if($rowDiklat['statusDiklatId'] == 1 && $rowDiklat['kuotaDiklat'] < $rowDiklat['_kuota']  ): ?>
                            <?php if (isset($_SESSION['id'])): ?>
                              <a href="<?= base_url() ?>pendaftaran_diklat/<?= $rowDiklat['id'] ?>">
                                <i class="fa fa-cloud-upload" aria-hidden="true"></i>
                              </a>
                              <?php else: ?>
                                <a href="javascript:" data-toggle="modal" data-target="#alertLogin">
                                  <i class="fa fa-cloud-upload" aria-hidden="true"></i>
                                </a>
                            <?php endif; ?>
                          
                          <?php else: ?>
                          -
                        <?php endif; ?>
                    </td>
                    

                  </tr>
                  <?php endforeach; ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>

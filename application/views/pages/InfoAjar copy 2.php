<!-- Banner -->
<?php if($rowOtherBanner): ?>
  <div class="container-fluid">
    <div class="row section-pages-banner">
      <div class="col-md-12">
        <div class="banner-box">
          <div class="image-item">
            <img
            src="<?= base_url().'asset/archive/'.$rowOtherBanner['banner'] ?>"
            alt="<?= $rowOtherBanner['title'] ?>"
            class="img-responsive image-bg">
            <div class="overlay"></div>
          </div>
          <div class="banner-title">
            <h2 class="wow fadeIn" data-wow-delay=".4"><?= $rowOtherBanner['title'] ?></h2>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>
<!-- End Banner -->

<div class="container-fluid">
  <div class="row section-contact-us-content section-contact">
    <div class="col-lg-12 wrap-side-content reset-col wow fadeIn" data-wow-delay=".4">
      <div class="col-md-12" style="padding-left:30px; padding-right:45px;">

        <div class="col-md-12 reset-col">
          <div class="breadcrumb-title">
            <ul>
              <li><a href="index.html"><i class="fa fa-home"></i></a></li>
              <li><i class="fa fa-angle-right"></i><i class="fa fa-angle-right"></i></li>
              <li><a href="<?= base_url() ?>"> Beranda</a></li>
              <li><i class="fa fa-angle-right"></i><i class="fa fa-angle-right"></i></li>
              <li class="active"><?= $settings['subTitle'] ?></li>
            </ul>
          </div>
        </div>

      </div>
      <div class="col-md-12 content">

      <?php  //print_r($data) ?>
      <div>
          <h2 style="text-align:center;">AGENDA </h2>
          <h4 style="text-align:center;">Info Ajar Widyaiswara</h4>
          <br>
          <br>
      </div>
      <div class="col-md-12 reset-col wrap-ac-brief">
            <div class="col-md-12 reset-col combo-pencarian">

              <div>
                <h5>Pencarian Data:</h5>
              </div>
              
              <?php
                // $tgl =date("Y-m-d");
                $thn =date("Y");
                $bln =date("m");
                $hari =date("d");
                $tgl = $hari.'/'.$bln.'/'.$thn
              ?>
                <div class="col-md-4">
                <!-- <h5>Tanggal Dari:</h5> -->
                <input value="<?= $tgl ?>" data-ref="tanggalMulai" type="text" class="form-control datepicker" id="datepicker" data-date-format="dd/mm/yyyy" />
                <!-- s/d -->
                </div>
                <div class="col-md-8">
                <input value="<?= $tgl ?>" data-ref="tanggalSampai" type="text" class="form-control datepicker" id="datepicker" data-date-format="dd/mm/yyyy" />
              </div>
              <br>
              <br>
              <div class="col-md-4">
                <select class="form-control" data-ref="jenisDiklat">
                  <option value="-1" data-hidden="true">--Jenis Diklat--</option>
                	<?php foreach ($rsJenisDiklat as $rowJenisDiklat): ?>
                    <option value="<?= $rowJenisDiklat['id'] ?>"><?= $rowJenisDiklat['jenisDiklat'] ?></option>
                  <?php endforeach; ?>
                </select>
              </div>

              <span class="input-group col-md-8">
                <span class="input-group-addon"><i class="fa fa-search"></i></span>
                <input data-ref="keyword" class="form-control" placeholder="Search" type="text">
              </span>
            </div>
            <div id="no-more-tables" class="tb-rekapitulasi">
              <table class="col-md-12 reset-col table-striped table-condensed cf">
                <thead class="cf">
                <tr>
                    <th class="numbers">Kegiatan</th>
                </tr>
                </thead>
                <tbody data-ref="listData">
                <?php
                foreach ($rsJadwal as $rowDiklat):
                ?>
                  <tr>
                    <td data-title="Widyaiswara">
                        <span><?= $rowDiklat['mataPelajaran'] ?></span>
                    </td>
                  </tr>
                  <?php endforeach; ?>
                </tbody>
              </table>
            </div>
          </div>
      <br>
      
    </div>
  </div>
</div>
</div>


<!-- Banner -->
<?php if($rowOtherBanner): ?>
  <div class="container-fluid">
    <div class="row section-pages-banner">
      <div class="col-md-12">
        <div class="banner-box">
          <div class="image-item">
            <img
            src="<?= base_url().'asset/archive/'.$rowOtherBanner['banner'] ?>"
            alt="<?= $rowOtherBanner['title'] ?>"
            class="img-responsive image-bg">
            <div class="overlay"></div>
          </div>
          <div class="banner-title">
            <h2 class="wow fadeIn" data-wow-delay=".4"><?= $rowOtherBanner['title'] ?></h2>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>
<!-- End Banner -->

<div class="container-fluid">
  <div class="row section-contact-us-content section-contact">
    <div class="col-lg-12 wrap-side-content reset-col wow fadeIn" data-wow-delay=".4">
      <div class="col-md-12" style="padding-left:30px; padding-right:45px;">

        <div class="col-md-12 reset-col">
          <div class="breadcrumb-title">
            <ul>
              <li><a href="index.html"><i class="fa fa-home"></i></a></li>
              <li><i class="fa fa-angle-right"></i><i class="fa fa-angle-right"></i></li>
              <li><a href="<?= base_url() ?>"> Beranda</a></li>
              <li><i class="fa fa-angle-right"></i><i class="fa fa-angle-right"></i></li>
              <li class="active"><?= $settings['subTitle'] ?></li>
            </ul>
          </div>
        </div>

      </div>
      <div class="col-md-12 content">

      <?php // print_r($rsJadwal) ?>
      <div>
          <h2 style="text-align:center;">AGENDA </h2>
          <h4 style="text-align:center;">Info Ajar Widyaiswara</h4>
          <br>
          <br>
      </div>
      <div data-ref="calendar" id='calendar' class="col-xs-12"></div>
      <br>
</div>
</div>

<!-- place -->
                        <div id="calendar"></div>
                            <div class="modal fade" id="create_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                  <span aria-hidden="true">&times;</span>
                                              </button>
                                              <h4 class="modal-title" id="myModalLabel">Detail Jadwal</h4>
                                              <br>
                                              <center>
                                            <table class="table">
                                              <tbody>
                                              <tr>
                                                <th>
                                                  Nama Diklat
                                                </th>  
                                                <td id="titleAsik">
                                                  
                                                </td>  
                                              </tr> 
                                              <tr>
                                                <th>
                                                  Jenis Diklat
                                                </th>  
                                                <td id="jenisDiklat">
                                                  
                                                </td>  
                                              </tr>  
                                              <tr>
                                                <th>
                                                Peserta
                                                </th>  
                                                <td id="peserta">
                                                  
                                                </td>  
                                              </tr>  
                                              <tr>
                                                <th>
                                                Tempat
                                                </th>  
                                                <td id="ruang">
                                                  
                                                </td>  
                                              </tr>  
                                              <tr>
                                                <th>
                                                Tanggal 
                                                </th>  
                                                <td id="allTanggal">
                                                  
                                                </td>  
                                              </tr> 
                                              <tr>
                                                <th>
                                                Waktu 
                                                </th>  
                                                <td id="allWaktu">
                                                  
                                                </td>  
                                              </tr>  
                                              <tr>
                                                <th>
                                                Mata Pelajaran 
                                                </th>  
                                                <td id="mataPelajaran">
                                                  
                                                </td>  
                                              </tr>  
                                              <tr>
                                                <th>
                                                Pengajar 
                                                </th>  
                                                <td id="allPengajar">
                                                  
                                                </td>  
                                              </tr>  
                                              
                                            </tbody>  

                                            </table>
                                            
                                          </center>
                                          </div>
                                          
                                    </div>
                            </div>
                        </div>
                    </div>
                    <!-- end place -->

<script type="text/javascript">

    var events = <?php echo json_encode($rsJadwal) ?>;
    var date = new Date()
    var d    = date.getDate(),
        m    = date.getMonth(),
        y    = date.getFullYear()

      $(document).ready(function() {
        $('#calendar').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'basicDay,basicWeek,month'
            },
            defaultDate: moment().format('YYYY-MM-DD'),
            editable: true,
                eventLimit: true, // allow "more" link when too many events
                selectable: true,
                selectHelper: true,
                eventDrop: function(event, delta, revertFunc) { // si changement de position
                    editDropResize(event);
                },
                eventResize: function(event,dayDelta,minuteDelta,revertFunc) { // si changement de longueur
                    editDropResize(event);
                },
                eventClick: function(event, element)
                {
                    deteil(event);
                },
                events: JSON.parse(events)
            });
    });

    function deteil(event)
    {
        $('#create_modal input[name=calendar_id]').val(event.id);
        $('#create_modal input[name=start_date]').val(moment(event.start).format('YYYY-MM-DD'));
        $('#create_modal input[name=end_date]').val(moment(event.end).format('YYYY-MM-DD'));
        $('#create_modal table > tbody > tr > td[id=titleAsik]').text(event.namaDiklat);
        $('#create_modal table > tbody > tr > td[id=jenisDiklat]').text(event.jenisDiklat);
        $('#create_modal table > tbody > tr > td[id=peserta]').text(event.kuotaDiklat +' Orang');
        $('#create_modal table > tbody > tr > td[id=ruang]').text(event.ruang);
        $('#create_modal table > tbody > tr > td[id=allTanggal]').text(event.allTanggal);
        $('#create_modal table > tbody > tr > td[id=allWaktu]').text(event.allWaktu);
        $('#create_modal table > tbody > tr > td[id=mataPelajaran]').text(event.mataPelajaran);
        $('#create_modal table > tbody > tr > td[id=allPengajar]').text(event.allPengajar);
        $('#create_modal').modal('show');
    }

</script>


<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<!-- Banner -->
<?php if($rowOtherBanner): ?>
  <div class="container-fluid">
    <div class="row section-pages-banner">
      <div class="col-md-12">
        <div class="banner-box">
          <div class="image-item">
            <img
            src="<?= base_url().'asset/archive/'.$rowOtherBanner['banner'] ?>"
            alt="<?= $rowOtherBanner['title'] ?>"
            class="img-responsive image-bg">
            <div class="overlay"></div>
          </div>
          <div class="banner-title">
            <h2 class="wow fadeIn" data-wow-delay=".4"><?= $rowOtherBanner['title'] ?></h2>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>
<!-- End Banner -->

<div class="container-fluid">
  <div class="row section-contact-us-content section-contact">
    <div class="col-lg-12 wrap-side-content reset-col wow fadeIn" data-wow-delay=".4">
      <div class="col-md-12" style="padding-left:30px; padding-right:45px;">

        <div class="col-md-12 reset-col">
          <div class="breadcrumb-title">
            <ul>
              <li><a href="index.html"><i class="fa fa-home"></i></a></li>
              <li><i class="fa fa-angle-right"></i><i class="fa fa-angle-right"></i></li>
              <li><a href="<?= base_url() ?>"> Beranda</a></li>
              <li><i class="fa fa-angle-right"></i><i class="fa fa-angle-right"></i></li>
              <li class="active"><?= $settings['subTitle'] ?></li>
            </ul>
          </div>
        </div>

      </div>
</div>
<div class="col-md-12 content">
    <div class="col-md-12" style="padding-right: 30px;">
        <div class="wrimagecard wrimagecard-topimage">
            <div class="wrimagecard-topimage_title">
                <h4><i class="fa fa-bar-chart"></i> Statistik Alumni Diklat </h4>
            </div>
            <div class="wrimagecard-topimage_header">
                <div id="ChartStatistikAlumni" class="chart"></div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

        google.charts.load('current', {
            packages: ['corechart', 'bar']
        });
        google.charts.setOnLoadCallback(drawMaterial);

        function drawMaterial() {

            var data = google.visualization.arrayToDataTable([
                ['Tahun', 'Latsar', 'Prajab', 'PKA', 'PKP', 'PKTUF', 'PKTI'],
                <?php
                foreach ($rsAlumni as $val) :{
                  $tahun = $val['tahun'];
                  if ($val['jenisDiklatId'] == 6) {
                    $latsar = $val['jumlahOrang'];
                  }else{
                    $latsar = 0;
                  }
                  if ($val['jenisDiklatId'] == 10) {
                    $prajab = $val['jumlahOrang'];
                  }else{
                    $prajab =0;
                  }
                  if ($val['jenisDiklatId'] == 8) {
                    $pka = $val['jumlahOrang'];
                  }else{
                    $pka =0;
                  }
                  if ($val['jenisDiklatId'] == 9) {
                    $pkp = $val['jumlahOrang'];
                  }else{
                    $pkp =0;
                  }
                  if ($val['jenisDiklatId'] == 11) {
                    $PKTUF = $val['jumlahOrang'];
                  }else{
                    $PKTUF =0;
                  }
                  if ($val['jenisDiklatId'] == 12) {
                    $PKTI = $val['jumlahOrang'];
                  }else{
                    $PKTI =0;
                  }
                }
                ?> 
                <?= "['".$tahun."',".$latsar.",".$prajab.",".$pka.",".$pkp.",".$PKTUF.",".$PKTI."]," ?>
                <?php endforeach; ?>
            ]);

            var materialOptions = {
                hAxis: {
                    title: 'Total Population',
                    minValue: 0,
                },
                vAxis: {
                    title: 'City'
                },
                bars: 'vertical'
            };
            var materialChart = new google.charts.Bar(document.getElementById('ChartStatistikAlumni'));
            materialChart.draw(data, materialOptions);
        }
    </script>



<!-- Banner -->
<?php if($rowOtherBanner): ?>
  <div class="container-fluid">
    <div class="row section-pages-banner">
      <div class="col-md-12">
        <div class="banner-box">
          <div class="image-item">
            <img
            src="<?= base_url().'asset/archive/'.$rowOtherBanner['banner'] ?>"
            alt="<?= $rowOtherBanner['title'] ?>"
            class="img-responsive image-bg">
            <div class="overlay"></div>
          </div>
          <div class="banner-title">
            <h2 class="wow fadeIn" data-wow-delay=".4"><?= $rowOtherBanner['title'] ?></h2>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>
<!-- End Banner -->

<div class="container-fluid">
  <div class="row section-otp-content">
    <div class="col-xs-12 rata">
      <div class="col-xs-12 col-sm-4 reset-col main-left-col">
        <div class="col-md-12 reset-col side-bar-tp">
          <?php $this->load->view('Login') ?>
          <div class="clearfix"></div>
          <?php $this->load->view('GuestBook') ?>
        </div>
      </div>
      <div class="col-xs-12 col-sm-8 reset-col main-right-col">
        <div class="col-md-12 wrap-side-content wow fadeIn" data-wow-delay=".4">
          <div class="col-md-12 side-content">

            <div class="col-md-12 reset-col">
              <div class="breadcrumb-title">
                <ul>
                  <li><a href="index.html"><i class="fa fa-home"></i></a></li>
                  <li><i class="fa fa-angle-right"></i><i class="fa fa-angle-right"></i></li>
                  <li><a href="<?= base_url() ?>"> Beranda</a></li>
                  <li><i class="fa fa-angle-right"></i><i class="fa fa-angle-right"></i></li>
                  <li><a href="<?= base_url() ?>akd"> AKD</a></li>
                  <li><i class="fa fa-angle-right"></i><i class="fa fa-angle-right"></i></li>
                  <li class="active"><?= $settings['subTitle'] ?></li>
                </ul>
              </div>

              <div class="tinymce-akd-title">
                <h2>ISIAN KOMPETENSI</h2>
                <h2>ANALISIS KEBUTUHAN DIKLAT (AKD)</h2>
              </div>
              <table class="col-md-12 reset-col akd-table">
                <tbody>
                  <tr>
                    <td>JABATAN</td>
                    <td class="marks">:</td>
                    <td><?= $rowSoalAkd['jabatan'] ?></td>
                  </tr>
                  <tr>
                    <td>UNIT KERJA</td>
                    <td class="marks">:</td>
                    <td><?= $rowSoalAkd['unitKerja'] ?></td>
                  </tr>
                  <tr>
                    <td>INSTANSI</td>
                    <td class="marks">:</td>
                    <td><?= $rowSoalAkd['instansi'] ?></td>
                  </tr>
                  <tr>
                    <td>
                      <h5 class="responden">RESPONDEN</h5>
                    </td>
                  </tr>
                  <tr>
                    <td>NIP</td>
                    <td class="marks">:</td>
                    <td><?= $rowSoalAkd['nip'] ?></td>
                  </tr>
                  <tr>
                    <td>NAMA</td>
                    <td class="marks">:</td>
                    <td><?= $rowSoalAkd['nama'] ?></td>
                  </tr>
                  <tr>
                    <td>KEDUDUKAN</td>
                    <td class="marks">:</td>
                    <td><?= $rowSoalAkd['kedudukan'] ?></td>
                  </tr>
                </tbody>
              </table>
              &nbsp;
              <hr>
              <div class="col-md-12 reset-col questioner">
                <h4>RUMUSAN KOMPETENSI &amp; KRITERIA PENILAIAN</h4>
                <div class="list-questions">

                  <ol>

                    <?php
                    $i = 0;
                    foreach ($rowSoalAkd['links'] as $rowRumusanKompetensi): ?>
                      <li>
                        <?= $rowRumusanKompetensi['rumusanKompetensi'] ?>
                        <form role="form">

                          <?php

                          $i++;
                          foreach ($rsTingkatKesanggupan as $rowTingkatKesanggupan):
                          ?>
                            <label class="radio-inline">
                              <input
                              type="radio"
                              name="jawaban_<?= $i ?>"
                              data-ref="jawaban_<?= $i ?>"
                              value="<?= $rowTingkatKesanggupan['id'] ?>"
                              data-rumusan-kompetensi="<?= $rowRumusanKompetensi['rumusanKompetensi'] ?>"
                              >
                              <?= $rowTingkatKesanggupan['tingkatKesanggupan'] ?>
                            </label>
                          <?php endforeach; ?>

                        </form>
                      </li>
                    <?php endforeach; ?>

                  </ol>

                  <div class="col-md-12 reset-col content-bottom">
                    <button data-ref="btnSubmit" class="btn btn-lg btn-primary btn-submit" type="button">SUBMIT</button>
                  </div>
                  <p>Catatan:
                    <br> Dalam pengisian kuisioner isian kompetensi diharapkan kepada responden penilai untuk melakukan penilaian sesuai kondisi sebenarnya.</p>
                  </div>
                </div>

              </div>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

<!-- Banner -->
<?php if($rowOtherBanner): ?>
  <div class="container-fluid">
    <div class="row section-pages-banner">
      <div class="col-md-12">
        <div class="banner-box">
          <div class="image-item">
            <img
            src="<?= base_url().'asset/archive/'.$rowOtherBanner['banner'] ?>"
            alt="<?= $rowOtherBanner['title'] ?>"
            class="img-responsive image-bg">
            <div class="overlay"></div>
          </div>
          <div class="banner-title">
            <h2 class="wow fadeIn" data-wow-delay=".4"><?= $rowOtherBanner['title'] ?></h2>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>
<!-- End Banner -->

<div class="container-fluid">
  <div class="row section-otp-content">
    <div class="col-xs-12 rata">
      <div class="col-xs-12 col-sm-4 reset-col main-left-col">
        <div class="col-md-12 reset-col side-bar-tp">
          <?php $this->load->view('Login') ?>
          <div class="clearfix"></div>
          <?php $this->load->view('GuestBook') ?>
        </div>
      </div>
      <div class="col-xs-12 col-sm-8 reset-col main-right-col">
        <div class="col-md-12 wrap-side-content wow fadeIn" data-wow-delay=".4">
          <div class="col-md-12 side-content">

            <div class="col-md-12 reset-col">
              <div class="breadcrumb-title">
                <ul>
                  <li><a href="index.html"><i class="fa fa-home"></i></a></li>
                  <li><i class="fa fa-angle-right"></i><i class="fa fa-angle-right"></i></li>
                  <li><a href="<?= base_url() ?>"> Beranda</a></li>
                  <li><i class="fa fa-angle-right"></i><i class="fa fa-angle-right"></i></li>
                  <li class="active"><?= $settings['subTitle'] ?></li>
                </ul>
              </div>
            </div>

            <div class="col-md-12 reset-col wrap-user">
              <div class="col-md-12 reset-col toppad">

              </div>
              <div class="col-md-12 reset-col toppad">
                <div class="panel panel-info">
                  <div class="panel-heading">
                    <h3 class="panel-title">User Profile</h3>
                  </div>
                  <div class="panel-body">
                    <div class="row">
                      <div class="col-md-3 col-lg-3 " align="center">
                        <img alt="User Pic" src="<?= base_url().'asset/archive/'.$rowAnggota['foto'] ?>" class="img-circle img-responsive">
                      </div>
                      <div class=" col-md-9 col-lg-9 ">
                        <table class="table table-user-information">
                          <tbody>
                            <tr>
                              <td>NIP</td>
                              <td>: <?= $rowAnggota['nip'] ?></td>
                            </tr>
                            <tr>
                              <td>Nama</td>
                              <td>: <?= $rowAnggota['nama'] ?></td>
                            </tr>
                            <tr>
                              <td>Instansi</td>
                                <?php if ($rowAnggota['instansiId'] == -1):?>
                              	<td>: <?= $rowAnggota['instansiLainnya'] ?></td>
                              	<?php else: ?>
                              	<td>: <?= $rowAnggota['instansi'] ?></td>
                              	<?php endif; ?>
                            </tr>
                            <tr>
                              <td>Email</td>
                              <td><a href="mailto:info@support.com">: <?= $rowAnggota['email'] ?></a></td>
                            </tr>
                            <tr>
                              <td>Aktif dari</td>
                              <td>: <?= $rowAnggota['registeredOn'] ?></td>
                            </tr>
                            <tr>
                              <td></td>
                              <td><a href="<?= base_url().'user_profile/edit_profile' ?>" class="btn btn-default btn-sm btn-sidebar pull-right">Edit Profile</a></td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                  <div class="panel-footer user-foot" style="font-size:10px; text-align:right; padding:7px;">
                    <!-- <span><b>Login : 11 Maret 2016 <i class="fa fa-clock-o"></i> 14:07 WIB </b></span> -->
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-12 reset-col wrap-ac-brief" >
              <h4>Tabel Data Diklat</h4>
              <div id="no-more-tables">
                <table class="col-md-12 reset-col table-striped table-condensed cf">
                  <thead class="cf">
                    <tr>
                      <th class="numbers">NO</th>
                      <th class="numbers">KODE PESERTA</th>
                      <th class="numbers">NAMA DIKLAT</th>
                      <th class="numbers">PELAKSANAAN</th>
                      <!-- <th class="numbers">EDIT</th> -->
                      <th class="numbers">CETAK FORM</th>
                      <th class="numbers">Jadwal</th>
                      <th class="numbers">Judul Laporan</th>
                    </tr>
                  </thead>
                  <tbody data-ref="listData">
                  <?php
                  $num = 0;
                  foreach ($rsPendaftaranDiklat as $rowPendaftaranDiklat):
                    $num++;
                  ?>
                    <tr>
                      <td data-title="No" class="numbers"><?= $num ?>.</td>
                      <td data-title="Kode"><?= $rowPendaftaranDiklat['kodePeserta'] ?></td>
                      <td data-title="Nama Diklat"><?= $rowPendaftaranDiklat['namaDiklat'] ?></td>
                      <td data-title="Pelaksanaan"><?= $rowPendaftaranDiklat['pelaksanaanMulai'] ?> s.d. <?= $rowPendaftaranDiklat['pelaksanaanSelesai'] ?></td>
                      <!-- <td data-title="Edit" class="numbers"><a href="edit-pendaftaran.html"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></td> -->
                      <td data-title="Cetak Form" class="numbers">
                        <a id="load-button" data-ref="linkCetak" data-id="<?= $rowPendaftaranDiklat['id'] ?>" href="javascript:">
                          <i class="fa fa-print" aria-hidden="true"></i>
                        </a>
                      </td>
                      <td data-title="Jadwal" class="numbers">
                      <a id="load-button-d" data-ref="linkDownload" data-id="<?= $rowPendaftaranDiklat['diklatId'] ?>" href="javascript:">
                          <i class="fa fa-file-excel-o" aria-hidden="true"></i>
                        </a>
                      </td>
                      <td data-title="Judul Laporan" class="numbers">
                          <a href="#" data-toggle="modal" data-ref="judulLaporan" data-id="<?= $rowPendaftaranDiklat['id'] ?>" data-target="#judulLaporan" data-dismiss="modal"><i class="fa fa-book" aria-hidden="true"></i></a>
                      </td>
                    </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Banner -->
<?php if($rowOtherBanner): ?>
  <div class="container-fluid">
    <div class="row section-pages-banner">
      <div class="col-md-12">
        <div class="banner-box">
          <div class="image-item">
            <img
            src="<?= base_url().'asset/archive/'.$rowOtherBanner['banner'] ?>"
            alt="<?= $rowOtherBanner['title'] ?>"
            class="img-responsive image-bg">
            <div class="overlay"></div>
          </div>
          <div class="banner-title">
            <h2 class="wow fadeIn" data-wow-delay=".4"><?= $rowOtherBanner['title'] ?></h2>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>
<!-- End Banner -->

<div class="container-fluid">
  <div class="row section-contact-us-content section-contact">
    <div class="col-lg-12 wrap-side-content reset-col wow fadeIn" data-wow-delay=".4">
      <div class="col-md-12" style="padding-left:30px; padding-right:45px;">

        <div class="col-md-12 reset-col">
          <div class="breadcrumb-title">
            <ul>
              <li><a href="index.html"><i class="fa fa-home"></i></a></li>
              <li><i class="fa fa-angle-right"></i><i class="fa fa-angle-right"></i></li>
              <li><a href="<?= base_url() ?>"> Beranda</a></li>
              <li><i class="fa fa-angle-right"></i><i class="fa fa-angle-right"></i></li>
              <li class="active"><?= $settings['subTitle'] ?></li>
            </ul>
          </div>
        </div>

      </div>
      <div class="col-md-12 content">

        <div class="col-md-6 content-kiri">
          <h3>Seputar Pertanyaan</h3>
          <div class="detail-content-ask">
            <ul>
            <?php foreach ($rsFaq as $rowFaq): ?>
              <li><a href="faq-detail.html"><?= $rowFaq['pertanyaan'] ?></a></li>
             <?php endforeach; ?>
            </ul>
          </div>
          <div class="col-md-12" style="text-align:center;">
              <ul class="pagination">
                  <?php
			        for ($i = 1; $i <= $pageCount; $i++):
			          if ($i == $pageActive):
			        ?>
			          <li class="active"><a href="javascript:"><?= $i ?></a></li>
			        <?php else: ?>
			          <li><a href="<?= base_url().'faq/'.$i ?>"><?= $i ?></a></li>
			        <?php
			          endif;
			        endfor;
			        ?>
              </ul>
              <br>
              <br>
              <br>
              <br>
          </div>
      </div>
      <div data-scope="Pertanyaan" class="col-lg-6 content-kanan">
        <h3>Kirim Pertanyaan</h3>
        <div class="col-md-12 reset-col form-contact">
          <div class="form-group">
            <label class="sr-only" for="c_nama">Nama</label>
            <input data-ref="nama" type="text" id="c_nama" class="form-control" name="c_nama" placeholder="Nama">
          </div>
          <div class="form-group">
            <label class="sr-only" for="c_email">Email</label>
            <input data-ref="email" type="email" id="c_email" class="form-control" name="c_email" placeholder="E-mail">
          </div>
          <div class="form-group">
            <textarea data-ref="pertanyaan" class="form-control" id="c_message" name="c_message" rows="7" placeholder="Pesan pertanyaan diklat"></textarea>
          </div>
          <button data-ref="btnSend" type="button" class="btn btn-default" data-wow-delay=".4s">
            KIRIM <i class="wow fadeIn fa fa-paper-plane"></i>
          </button>
        </div>
      </div>

    </div>
  </div>
</div>
</div>

<!-- Banner -->
<?php if($rowOtherBanner): ?>
  <div class="container-fluid">
    <div class="row section-pages-banner">
      <div class="col-md-12">
        <div class="banner-box">
          <div class="image-item">
            <img
            src="<?= base_url().'asset/archive/'.$rowOtherBanner['banner'] ?>"
            alt="<?= $rowOtherBanner['title'] ?>"
            class="img-responsive image-bg">
            <div class="overlay"></div>
          </div>
          <div class="banner-title">
            <h2 class="wow fadeIn" data-wow-delay=".4"><?= $rowOtherBanner['title'] ?></h2>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>
<!-- End Banner -->

<div class="container-fluid">
  <div class="row section-otp-content">
    <div class="col-xs-12 rata">
      <div class="col-xs-12 col-sm-4 reset-col main-left-col">
        <div class="col-md-12 reset-col side-bar-tp">
          <?php $this->load->view('Login') ?>
          <div class="clearfix"></div>
          <?php $this->load->view('GuestBook') ?>
        </div>
      </div>
      <div class="col-xs-12 col-sm-8 reset-col main-right-col">
        <div class="col-md-12 wrap-side-content wow fadeIn" data-wow-delay=".4">
          <div class="col-md-12 side-content">

            <div class="col-md-12 reset-col">
              <div class="breadcrumb-title">
                <ul>
                  <li><a href="index.html"><i class="fa fa-home"></i></a></li>
                  <li><i class="fa fa-angle-right"></i><i class="fa fa-angle-right"></i></li>
                  <li><a href="<?= base_url() ?>"> Beranda</a></li>
                  <li><i class="fa fa-angle-right"></i><i class="fa fa-angle-right"></i></li>
                  <li class="active"><?= $settings['subTitle'] ?></li>
                </ul>
              </div>
            </div>

            <div class="tinymce-ac-brief-title">
              <h2><?= $rowSttplAlumni['judul'] ?></h2>
            </div>
            <div>
              <?= $rowSttplAlumni['pengantar'] ?>
            </div>
            <br>
            <h5>Memulai pencarian (sortir data berdasarkan) :</h5>
            <br>
            <div class="contact-group col-xs-12 reset-col">

              <div class="col-sm-12 reset-col">
                <div class="col-md-2 col-sm-3 col-xs-4 reset-col">
                  <label>Jenis Diklat :</label>
                </div>
                <div class="col-md-10 col-sm-9 col-xs-8 reset-col">
                  <select data-ref="jenisDiklat" class="selectpicker col-xs-12 reset-col">
                    <option data-hidden="true" value="">--Jenis Diklat--</option>
                    <?php foreach ($rsJenisDiklat as $rowJenisDiklat): ?>
                      <option value="<?= $rowJenisDiklat['id'] ?>">
                        <?= $rowJenisDiklat['jenisDiklat'] ?>
                      </option>
                    <?php endforeach; ?>
                  </select>
                </div>
              </div>

              <div class="col-sm-12 reset-col">
                <div class="col-md-2 col-sm-3 col-xs-4 reset-col">
                  <label>Tahun :</label>
                </div>
                <div class="col-md-10 col-sm-9 col-xs-8 reset-col">
                  <select data-ref="tahun" class="selectpicker col-xs-12 reset-col">
                    <option data-hidden="true" value="">--Tahun--</option>
                    <?php for ($i = 2000; $i <= 2030; $i++): ?>
                      <option value="<?= $i ?>"><?= $i ?></option>
                    <?php endfor; ?>
                  </select>
                </div>
              </div>

              <div class="col-sm-12 reset-col">
                <div class="col-md-2 col-sm-3 col-xs-4 reset-col">
                </div>
                <div class="col-md-10 col-sm-9 col-xs-8 reset-col">
                  <button data-ref="btnCari" type="button" class="btn btn-default btn-cari pull-right"><i class="fa fa-search"></i>Cari Data</button>
                </div>
              </div>

            </div>
            <div class="clearfix"></div>
            <div class="col-md-12 reset-col wrap-ac-brief">
              <br>
              <br>

              <div class="col-sm-12 reset-col">
                <div class="col-md-2 col-sm-3 col-xs-4 reset-col">
                  <label>Filter :</label>
                </div>
                <div class="col-md-10 col-sm-9 col-xs-8 reset-col">
                  <input data-ref="namaDiklat" type="text" class="form-control" placeholder="Program Diklat">
                </div>
              </div>
              <br>&nbsp;

              <div id="no-more-tables">
                <table class="col-md-12 reset-col table-striped table-condensed cf">
                  <thead class="cf">
                    <tr>
                      <th class="numbers">NO</th>
                      <th class="numbers">PROGRAM DIKLAT</th>
                    </tr>
                  </thead>
                  <tbody data-ref="listData">
                     <?php
	                  $num = 0;
	                  foreach ($rsKelulusan as $rowKelulusan):
	                    $num++;
	                  ?>
                      <tr>
                        <td data-title="No" class="numbers"><?= $num ?>.</td>
                        <td data-title="Program Diklat">
                          <a href="<?= base_url() ?>sttpl_alumni/detail_alumni/<?= $rowKelulusan['id'] ?>">
                            <?= $rowKelulusan['namaDiklatKode'] ?>
                          </a>
                        </td>
                      </tr>
                    <?php endforeach; ?>
                    <script>
                      var RS_DIKLAT = <?= json_encode($rsKelulusan) ?>;
                    </script>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

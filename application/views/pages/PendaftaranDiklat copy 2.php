<!-- Banner -->

<?php if($rowOtherBanner): ?>
  <div class="container-fluid">
    <div class="row section-pages-banner">
      <div class="col-md-12">
        <div class="banner-box">
          <div class="image-item">
            <img
            src="<?= base_url().'asset/archive/'.$rowOtherBanner['banner'] ?>"
            alt="<?= $rowOtherBanner['title'] ?>"
            class="img-responsive image-bg">
            <div class="overlay"></div>
          </div>
          <div class="banner-title">
            <h2 class="wow fadeIn" data-wow-delay=".4">FORM PENDAFTARAN</h2>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>
<!-- End Banner -->

<div class="col-md-12" style="padding-left:11.5%; padding-right:11.5%">

  <div class="col-md-12 reset-col">
    <div class="breadcrumb-title">
      <ul>
        <li><a href="index.html"><i class="fa fa-home"></i></a></li>
        <li><i class="fa fa-angle-right"></i><i class="fa fa-angle-right"></i></li>
        <li><a href="<?= base_url() ?>"> Beranda</a></li>
        <li><i class="fa fa-angle-right"></i><i class="fa fa-angle-right"></i></li>
        <li class="active"><?= $settings['subTitle'] ?></li>
      </ul>
    </div>
  </div>

</div>

<?php if(isset($rowDiklat1['kode'])): ?>
<div class="container-fluid">
  <div class="row section-pendaftaran-content">
    <div class="col-lg-12 wrap-side-content reset-col">
      <div class="col-xs-12 col-sm-12 col-md-8 reset-col">
        <div class="col-md-12 side-content">
          <div class="col-md-12 wrap-diploma section-reg">
            <div class="col-md-12 form-contact">
              <div class="form-group">
              <table class="col-md-12 reset-col akd-table">
                      <tbody>
                          <tr>
                              <td>KODE</td>
                              <td class="marks">:</td>
                              <td><?= $rowDiklat1['kode'] ?></td>
                          </tr>
                          <tr>
                              <td>NAMA DIKLAT</td>
                              <td class="marks">:</td>
                              <td><?= $rowDiklat1['namaDiklat'] ?></td>
                          </tr>
                          <tr>
                              <td>TGL PELAKSANAAN</td>
                              <td class="marks">:</td>
                              <td><?= $rowDiklat1['pelaksanaanMulai'] ?>  s.d. <?= $rowDiklat1['pelaksanaanSelesai'] ?></td>
                          </tr>
                          <tr>
                              <td>LOKASI PELAKSANAAN</td>
                              <td class="marks">:</td>
                              <td><?= $rowDiklat1['lokasi'] ?></td>
                          </tr>
                      </tbody>
                  </table>
                  <div class="clearfix"></div>
                    <div class="col-md-12 reset-col catatan-pendaftaran">
                        <h5>Catatan :</h5>
                        <ol>
                            <li>Harus diisi sesuai SK PNS</li>
                            <li>Form cetak & berkas pendukung dibawa saat registrasi ulang (2 rangkap)</li>
                        </ol>
                    </div>
                <div style="display: none" class="contact-group col-xs-12 reset-col">
                  <div class="col-sm-3 col-xs-12 reset-col">
                    <label>Kode Peserta</label>
                  </div>
                  <div class="col-sm-9 col-xs-12 reset-col">
                    <input data-ref="kodePeserta" type="text" class="form-control" name="" placeholder="Automatis" disabled>
                    <input data-ref="idDiklat" value="<?= $rowDiklat1['id'] ?>" type="text" class="form-control" name="" placeholder="Automatis" disabled>
                    <input value="<?= $rowDiklat1['jenisDiklatId'] ?>" data-ref="jenisDiklatId" type="text" class="form-control" name="" disabled>
                </div>
                </div>
                
                <div class="clearfix"></div>
                <div class="contact-group col-xs-12 reset-col">
                    <div class="col-sm-3 col-xs-12 reset-col">
                        <label>NIP</label>
                    </div>
                    <div class="col-md-4 col-sm-9 col-xs-12 reset-col">
                        <input value="<?= $rowAnggota['nip'] ?>" data-ref="email" type="text" class="form-control" name="" placeholder="Automatis Dari User Login" disabled>
                    </div>
                </div>
                <div class="clearfix"></div>
                      <div class="contact-group col-xs-12 reset-col">
                        <div class="col-sm-3 col-xs-12 reset-col">
                          <label>Email</label>
                        </div>
                        <div class="col-sm-9 col-xs-12 reset-col">
                          <input value="<?= $rowAnggota['email'] ?>" disabled data-ref="email" type="text" class="form-control" name="" placeholder="">
                        </div>
                      </div>
                      <div class="clearfix"></div>
                      <div class="contact-group col-xs-12 reset-col">
                        <div class="col-sm-3 col-xs-12 reset-col">
                          <label>Nama Lengkap</label>
                        </div>
                        <div class="col-sm-9 col-xs-12 reset-col">
                          <input value="<?= $rowAnggota['nama'] ?>" data-ref="namaLengkap" type="text" class="form-control" name="" placeholder="">
                          <span class="help-block">Nama Lengkap &amp Gelar</span>
                        </div>
                      </div>
                      <div class="clearfix"></div>
                      <div class="contact-group col-xs-12 reset-col">
                        <div class="col-sm-3 col-xs-12 reset-col">
                          <label>Alamat Rumah</label>
                        </div>
                        <div class="col-sm-9 col-xs-12 reset-col">
                          <textarea data-ref="alamatLengkap" class="form-control" name="" rows="7" placeholder=""><?= $rowAnggota['alamat'] ?></textarea>
                        </div>
                      </div>
                      <div class="clearfix"></div>
                      <div class="contact-group col-xs-12 reset-col">
                        <div class="col-sm-3 reset-col">
                          <label>Tempat Lahir</label>
                        </div>
                        <div class="col-sm-9 reset-col">
                          <input value="<?= $rowAnggota['tempatLahir'] ?>" data-ref="tempatLahir" type="text" class="form-control" name="" placeholder="">
                        </div>
                      </div>
                      <div class="clearfix"></div>
                      <div class="contact-group col-xs-12 reset-col">
                        <div class="col-sm-3 reset-col">
                          <label>Tanggal Lahir</label>
                        </div>
                        <div class="col-sm-9 reset-col">
                          <div class="form-horizontal">
                            <div class="input-group">
                              <input value="<?= $rowAnggota['tanggalLahir'] ?>" data-ref="tanggalLahir" type="text" class="form-control datepicker" id="datepicker" data-date-format="dd/mm/yyyy" /S>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="clearfix"></div>
                      <div class="contact-group col-xs-12 reset-col">
                        <div class="col-sm-3 col-xs-12 reset-col">
                          <label>Agama</label>
                        </div>
                        <div class="col-sm-9 col-xs-12 reset-col">
                          <select data-ref="agama" class="selectpicker" data-width="100%">
                            <option value="" data-hidden="true">--Pilih Agama--</option>
                            <?php foreach ($rsAgama as $rowAgama): ?>
                              <?php if ($rowAgama['id'] == $rowAnggota['agamaId']): ?>
                                <option selected value="<?= $rowAgama['id'] ?>">
                                  <?= $rowAgama['agama'] ?>
                                </option>
                              <?php else: ?>
                                <option value="<?= $rowAgama['id'] ?>">
                                  <?= $rowAgama['agama'] ?>
                                </option>
                              <?php endif; ?>
                            <?php endforeach; ?>
                          </select>
                        </div>
                      </div>
                      <div class="clearfix"></div>
                      <div class="contact-group col-xs-12 reset-col">
                      <div class="col-sm-3 col-xs-12 reset-col">
                        <label>Jenis Kelamin</label>
                      </div>
                      <div class="col-sm-9 col-xs-12 radio-wrap">
                        <?php foreach ($rsGender as $rowGender): ?>
                          <?php if ($rowGender['id'] == $rowAnggota['jenisKelaminId']): ?>
                            <label class="radio-inline">
                              <input checked data-ref="jenisKelamin" type="radio" name="jeniskelamin" value="<?= $rowGender['id'] ?>">
                              <?= $rowGender['gender'] ?>
                            </label>
                          <?php else: ?>
                            <label class="radio-inline">
                              <input data-ref="jenisKelamin" type="radio" name="jeniskelamin" value="<?= $rowGender['id'] ?>">
                              <?= $rowGender['gender'] ?>
                            </label>
                          <?php endif; ?>
                        <?php endforeach; ?>
                        </div>
                      </div>
                      
                      <div class="clearfix"></div>
                      <div class="contact-group col-xs-12 reset-col">
                        <div class="col-sm-3 col-xs-12 reset-col">
                          <label>Pangkat/Golongan</label>
                        </div>
                        <div class="col-sm-9 col-xs-12 reset-col">
                          <select data-ref="pangkatGolongan" class="selectpicker" data-live-search="true" data-width="100%">
                            <option value="" data-hidden="true">--Pilih Pangkat/ Golongan--</option>
                            <?php foreach ($rsPangkatGolongan as $rowPangkatGolongan): ?>
                              <?php if ($rowPangkatGolongan['id'] == $rowAnggota['pangkatGolonganId']): ?>
                                <option selected value="<?= $rowPangkatGolongan['id'] ?>"><?= $rowPangkatGolongan['pangkatGolongan'] ?></option>
                              <?php else: ?>
                                <option value="<?= $rowPangkatGolongan['id'] ?>"><?= $rowPangkatGolongan['pangkatGolongan'] ?></option>
                              <?php endif; ?>
                            <?php endforeach; ?>
                          </select>
                        </div>
                      </div>
                      <div class="clearfix"></div>
                      <div class="contact-group col-xs-12 reset-col">
                        <div class="col-sm-3 col-xs-12 reset-col">
                          <label>Jabatan/Eselon</label>
                        </div>
                        <div class="col-sm-9 col-xs-12 reset-col">
                          <input value="<?= $rowAnggota['jabatanEselon'] ?>" data-ref="jabatanEselon" type="text" class="form-control" name="" placeholder="">
                        </div>
                      </div>
                      <div class="clearfix"></div>
                      <div class="contact-group col-xs-12 reset-col">
                        <div class="col-sm-3 col-xs-12 reset-col">
                          <label>Unit Kerja</label>
                        </div>
                        <div class="col-sm-9 col-xs-12 reset-col">
                          <input value="<?= $rowAnggota['unitKerja'] ?>" data-ref="unitKerja" type="text" class="form-control" name="" placeholder="">
                        </div>
                      </div>
                      <div class="clearfix"></div>
                      <div class="contact-group col-xs-12 reset-col">
                        <div class="col-sm-3 col-xs-12 reset-col">
                          <label>Instansi</label>
                        </div>
                        <div class="col-sm-9 col-xs-12 reset-col">
                          <select data-ref="instansi" data-ref="instansi" class="selectpicker" data-live-search="true" data-width="100%">
                            <option value="" data-hidden="true">--Pilih Instansi--</option>
                            <?php foreach ($rsInstansi as $rowInstansi): ?>
                              <?php if ($rowInstansi['id'] == $rowAnggota['instansiId']): ?>
                                <option selected value="<?= $rowInstansi['id'] ?>"><?= $rowInstansi['instansi'] ?></option>
                              <?php else: ?>
                                <option value="<?= $rowInstansi['id'] ?>"><?= $rowInstansi['instansi'] ?></option>
                              <?php endif; ?>
                            <?php endforeach; ?>

                            <?php
                            if ($rowAnggota['instansiId'] == -1):
                            $displayStyle = '';
                            ?>
                            <option value="-1" selected="">Instansi Lainnya</option>
                            <?php
                            else:
                            $displayStyle = 'style="display: none"';
                            ?>
                            <option value="-1">Instansi Lainnya</option>
                          <?php endif; ?>
                        </select>
                        <input value="<?= $rowAnggota['instansiLainnya'] ?>" <?= $displayStyle ?> data-ref="namaInstansi" type="text" class="form-control" name="" placeholder="--Ketik Nama Instansi--">
                      </div>
                    </div>
                    <div class="clearfix"></div>
              <div class="contact-group col-xs-12 reset-col">
                <div class="col-sm-3 col-xs-12 reset-col">
                  <label>Pendidikan Terakhir</label>
                </div>
                <div class="col-sm-9 col-xs-12 reset-col">
                  <select data-ref="pendidikanTerakhir" class="selectpicker" data-width="100%">
                    <option data-hidden="true" value="">--Pilih Pendidikan Terakhir--</option>
                    <?php foreach ($rsPendidikanTerakhir as $rowPendidikanTerakhir): ?>
                      <?php if ($rowPendidikanTerakhir['id'] == $rowAnggota['pendidikanTerakhirId']): ?>
                        <option selected value="<?= $rowPendidikanTerakhir['id'] ?>">
                          <?= $rowPendidikanTerakhir['pendidikanTerakhir'] ?>
                        </option>
                      <?php else: ?>
                        <option value="<?= $rowPendidikanTerakhir['id'] ?>">
                          <?= $rowPendidikanTerakhir['pendidikanTerakhir'] ?>
                        </option>
                      <?php endif; ?>
                    <?php endforeach; ?>
                  </select>
                </div>
              </div>
                    <div class="clearfix"></div>
                      <div class="contact-group col-xs-12 reset-col">
                        <div class="col-sm-3 col-xs-12 reset-col">
                          <label>Alamat Kantor</label>
                        </div>
                        <div class="col-sm-9 col-xs-12 reset-col">
                          <textarea data-ref="alamatKantor" class="form-control" name="" rows="7" placeholder=""><?= $rowAnggota['alamatKantor'] ?></textarea>
                        </div>
                      </div>
                    <div class="clearfix"></div>
                    
                      <div class="contact-group col-xs-12 reset-col">
                        <div class="col-sm-3 col-xs-12 reset-col">
                          <label>No. Telepon / HP</label>
                        </div>
                        <div class="col-sm-9 col-xs-12 reset-col">
                          <input value="<?= $rowAnggota['telpHp'] ?>" data-ref="telpHp" type="text" class="form-control" name="" placeholder="">
                        </div>
                      </div>
                      <div class="clearfix"></div>
                      <div class="contact-group col-xs-12 reset-col">
                        <div class="col-sm-3 col-xs-12 reset-col">
                          <label>Pasfoto</label>
                        </div>
                        <div class="col-sm-9 col-xs-12 reset-col">
                          <img data-ref="fotoPreview" alt="" class="img-responsive" src="<?= base_url() ?>asset/archive/<?= $rowAnggota['foto'] ?>">
                          <div class="input-group">
                            <span class="input-group-btn">
                              <span class="btn btn-warning btn-file">
                                <span data-ref="uploadIndicator">Pilih File</span> <input data-value="<?= $rowAnggota['foto'] ?>" type="file" accept="image/*" data-ref="foto">
                              </span>
                            </span>
                            <input type="text" class="form-control" readonly>
                          </div>
                          <span class="help-block">Berkas format *.jpg Ukuran 4x6 berlatar warna Merah</span>
                        </div>
                      </div>
                      
                        <div class="clearfix"></div>
                        <div class="contact-group col-xs-12 reset-col">
                          <div class="col-sm-3 col-xs-12 reset-col">
                            <label>Surat Perintah Tugas</label>
                          </div>
                          <div class="col-sm-9 col-xs-12 reset-col">
                            <iframe data-ref="suratPerintahTugasPreview" alt="" src="<?= base_url() ?>asset/archive/<?= $rowAnggota['suratPerintahTugas'] ?>" 
                            style="width:600px; height:500px;" frameborder="0"></iframe>
                            <div class="input-group">
                              <span class="input-group-btn">
                                <span class="btn btn-warning btn-file">
                                  <span data-ref="uploadIndicatorSuratPerintahTugas">Pilih File</span> <input data-value="<?= $rowAnggota['suratPerintahTugas'] ?>" type="file" accept="application/pdf" data-ref="suratPerintahTugas">
                                </span>
                              </span>
                              <input type="text" class="form-control" readonly>
                            </div>
                            <span class="help-block">Berkas format *.pdf maksimal 3Mb</span>
                          </div>
                        </div>

                      <?php if($rowDiklat1['jenisDiklatId'] != 11 && $rowDiklat1['jenisDiklatId'] != 12): ?>
                      <div class="clearfix"></div>
                      <div class="contact-group col-xs-12 reset-col">
                        <div class="col-sm-3 col-xs-12 reset-col">
                          <label>Surat Pernyataan Bebas Tugas Kedinasan</label>
                        </div>
                        <div class="col-sm-9 col-xs-12 reset-col">
                        <iframe data-ref="suratPernyataanBebasTugasKedinasanPreview" alt="" src="<?= base_url() ?>asset/archive/<?= $rowAnggota['suratPernyataanBebasTugasKedinasan'] ?>" 
                           style="width:600px; height:500px;" frameborder="0"></iframe>
                          <div class="input-group">
                            <span class="input-group-btn">
                              <span class="btn btn-warning btn-file">
                                <span data-ref="uploadIndicatorSuratPernyataanBebasTugasKedinasan">Pilih File</span> <input data-value="<?= $rowAnggota['suratPernyataanBebasTugasKedinasan'] ?>" type="file" accept="application/pdf" data-ref="suratPernyataanBebasTugasKedinasan">
                              </span>
                            </span>
                            <input type="text" class="form-control" readonly>
                          </div>
                          <span class="help-block">Berkas format *.pdf maksimal 3Mb</span>
                        </div>
                      </div>
                      <?php endif; ?>

                      <?php if($rowDiklat1['jenisDiklatId'] == 6 && $rowDiklat1['jenisDiklatId'] == 10): ?>
                      <div class="clearfix"></div>
                      <div class="contact-group col-xs-12 reset-col">
                        <div class="col-sm-3 col-xs-12 reset-col">
                          <label>Surat Keputusan CPNS</label>
                        </div>
                        <div class="col-sm-9 col-xs-12 reset-col">
                        <iframe data-ref="suratKeputusanCPNSPreview" alt="" src="<?= base_url() ?>asset/archive/<?= $rowAnggota['suratKeputusanCPNS'] ?>" 
                           style="width:600px; height:500px;" frameborder="0"></iframe>
                          <div class="input-group">
                            <span class="input-group-btn">
                              <span class="btn btn-warning btn-file">
                                <span data-ref="uploadIndicatorSuratKeputusanCPNS">Pilih File</span> <input data-value="<?= $rowAnggota['suratKeputusanCPNS'] ?>" type="file" accept="application/pdf" data-ref="suratKeputusanCPNS">
                              </span>
                            </span>
                            <input type="text" class="form-control" readonly>
                          </div>
                          <span class="help-block">Berkas format *.pdf maksimal 3Mb</span>
                        </div>
                      </div>
                      <?php endif; ?>
                      <?php if($rowDiklat1['jenisDiklatId'] == 8 && $rowDiklat1['jenisDiklatId'] == 9): ?>
                      <div class="clearfix"></div>
                      <div class="contact-group col-xs-12 reset-col">
                        <div class="col-sm-3 col-xs-12 reset-col">
                          <label>Surat Keputusan Pangkat</label>
                        </div>
                        <div class="col-sm-9 col-xs-12 reset-col">
                        <iframe data-ref="suratKeputusanPangkatPreview" alt="" src="<?= base_url() ?>asset/archive/<?= $rowAnggota['suratKeputusanPangkat'] ?>" 
                           style="width:600px; height:500px;" frameborder="0"></iframe>
                        <div class="input-group">
                            <span class="input-group-btn">
                              <span class="btn btn-warning btn-file">
                                <span data-ref="uploadIndicatorSuratKeputusanPangkat">Pilih File</span> <input data-value="<?= $rowAnggota['suratKeputusanPangkat'] ?>" type="file" accept="application/pdf" data-ref="suratKeputusanPangkat">
                              </span>
                            </span>
                            <input type="text" class="form-control" readonly>
                          </div>
                          <span class="help-block">Berkas format *.pdf maksimal 3Mb</span>
                        </div>
                      </div>
                      <?php endif; ?>
                      <?php if($rowDiklat1['jenisDiklatId'] == 8 && $rowDiklat1['jenisDiklatId'] == 9): ?>
                      <div class="clearfix"></div>
                      <div class="contact-group col-xs-12 reset-col">
                        <div class="col-sm-3 col-xs-12 reset-col">
                          <label>Surat Keputusan Jabatan Terakhir</label>
                        </div>
                        <div class="col-sm-9 col-xs-12 reset-col">
                        <iframe data-ref="suratKeputusanJabatanTerakhirPreview" alt="" src="<?= base_url() ?>asset/archive/<?= $rowAnggota['suratKeputusanJabatanTerakhir'] ?>" 
                           style="width:600px; height:500px;" frameborder="0"></iframe>
                        <div class="input-group">
                            <span class="input-group-btn">
                              <span class="btn btn-warning btn-file">
                                <span data-ref="uploadIndicatorSuratKeputusanJabatanTerakhir">Pilih File</span> <input data-value="<?= $rowAnggota['suratKeputusanJabatanTerakhir'] ?>" type="file" accept="application/pdf" data-ref="suratKeputusanJabatanTerakhir">
                              </span>
                            </span>
                            <input type="text" class="form-control" readonly>
                          </div>
                          <span class="help-block">Berkas format *.pdf maksimal 3Mb</span>
                        </div>
                      </div>
                      <?php endif; ?>
                      <?php if($rowDiklat1['jenisDiklatId'] != 11 && $rowDiklat1['jenisDiklatId'] != 12 ): ?>
                      <div class="clearfix"></div>
                      <div class="contact-group col-xs-12 reset-col">
                        <div class="col-sm-3 col-xs-12 reset-col">
                          <label>Ijazah Terakhir</label>
                        </div>
                        <div class="col-sm-9 col-xs-12 reset-col">
                        <iframe data-ref="ijazahTerakhirPreview" alt="" src="<?= base_url() ?>asset/archive/<?= $rowAnggota['ijazahTerakhir'] ?>" 
                           style="width:600px; height:500px;" frameborder="0"></iframe>
                        <div class="input-group">
                            <span class="input-group-btn">
                              <span class="btn btn-warning btn-file">
                                <span data-ref="uploadIndicatorIjazahTerakhir">Pilih File</span> <input data-value="<?= $rowAnggota['ijazahTerakhir'] ?>" type="file" accept="application/pdf" data-ref="ijazahTerakhir">
                              </span>
                            </span>
                            <input type="text" class="form-control" readonly>
                          </div>
                          <span class="help-block">Berkas format *.pdf maksimal 3Mb</span>
                        </div>
                      </div>
                      <?php endif; ?>
                      
                      <div class="clearfix"></div>
                      <div class="contact-group col-xs-12 reset-col">
                        <div class="col-sm-3 col-xs-12 reset-col">
                          <label>Surat Keterangan Dokter dari Rumah Sakit Pemerintah</label>
                        </div>
                        <div class="col-sm-9 col-xs-12 reset-col">
                        <iframe data-ref="suratKeteranganDokterdariRumahSakitPemerintahPreview" alt="" src="<?= base_url() ?>asset/archive/<?= $rowAnggota['suratKeteranganDokterdariRumahSakitPemerintah'] ?>" 
                           style="width:600px; height:500px;" frameborder="0"></iframe>
                        <div class="input-group">
                            <span class="input-group-btn">
                              <span class="btn btn-warning btn-file">
                                <span data-ref="uploadIndicatorSuratKeteranganDokterdariRumahSakitPemerintah">Pilih File</span> <input data-value="<?= $rowAnggota['suratKeteranganDokterdariRumahSakitPemerintah'] ?>" type="file" accept="application/pdf" data-ref="suratKeteranganDokterdariRumahSakitPemerintah">
                              </span>
                            </span>
                            <input type="text" class="form-control" readonly>
                          </div>
                          <span class="help-block">Berkas format *.pdf maksimal 3Mb</span>
                        </div>
                      </div>
                      <div class="clearfix"></div>
                      <div class="contact-group col-xs-12 reset-col">
                        <div class="col-sm-3 col-xs-12 reset-col">
                          <label>Surat Kesehatan Covid 19</label>
                        </div>
                        <div class="col-sm-9 col-xs-12 reset-col">
                        <iframe data-ref="suratKesehatanPreview" alt="" src="<?= base_url() ?>asset/archive/<?= $rowAnggota['suratKesehatan'] ?>" 
                           style="width:600px; height:500px;" frameborder="0"></iframe>
                        <div class="input-group">
                            <span class="input-group-btn">
                              <span class="btn btn-warning btn-file">
                                <span data-ref="uploadIndicatorSuratKesehatan">Pilih File</span> <input data-value="<?= $rowAnggota['suratKesehatan'] ?>" type="file" accept="application/pdf" data-ref="suratKesehatan">
                              </span>
                            </span>
                            <input type="text" class="form-control" readonly>
                          </div>
                          <span class="help-block">Berkas format *.pdf maksimal 3Mb</span>
                        </div>
                      </div>
                      <?php if($rowDiklat1['jenisDiklatId'] != 11 && $rowDiklat1['jenisDiklatId'] != 12 ): ?>
                      
                      <div class="clearfix"></div>
                      <div class="contact-group col-xs-12 reset-col">
                        <div class="col-sm-3 col-xs-12 reset-col">
                          <label>Askes, BPJS atau KIS</label>
                        </div>
                        <div class="col-sm-9 col-xs-12 reset-col">
                        <iframe data-ref="askesBPJSatauKISPreview" alt="" src="<?= base_url() ?>asset/archive/<?= $rowAnggota['askesBPJSatauKIS'] ?>" 
                           style="width:600px; height:500px;" frameborder="0"></iframe>
                        <div class="input-group">
                            <span class="input-group-btn">
                              <span class="btn btn-warning btn-file">
                                <span data-ref="uploadIndicatorAskesBPJSatauKIS">Pilih File</span> <input data-value="<?= $rowAnggota['askesBPJSatauKIS'] ?>" type="file" accept="application/pdf" data-ref="askesBPJSatauKIS">
                              </span>
                            </span>
                            <input type="text" class="form-control" readonly>
                          </div>
                          <span class="help-block">Berkas format *.pdf maksimal 3Mb</span>
                        </div>
                      </div>
                      
                      <?php endif; ?>
                                <div class="clearfix"></div>
                                  <div class="col-xs-12 reset-col button-daftar">
                                          <button data-ref="btnDaftar" type="button" class="btn btn-default btn-sm btn-reg ">Simpan 
                                          <?php if($rowDiklat1['jenisDiklatId'] != 11) : ?>
                                          & Cetak
                                          <?php endif; ?>
                                          </button>
                                          <button data-ref="btnBatal" type="cancel" class="btn btn-default btn-sm btn-reg ">Batal</button>
                                      </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <?php else: ?>
                    <h3>ANDA SUDAH MENDAFTAR</h3>
<?php endif; ?>

<!-- Banner -->
<?php if($rowOtherBanner): ?>
  <div class="container-fluid">
    <div class="row section-pages-banner">
      <div class="col-md-12">
        <div class="banner-box">
          <div class="image-item">
            <img
            src="<?= base_url().'asset/archive/'.$rowOtherBanner['banner'] ?>"
            alt="<?= $rowOtherBanner['title'] ?>"
            class="img-responsive image-bg">
            <div class="overlay"></div>
          </div>
          <div class="banner-title">
            <h2 class="wow fadeIn" data-wow-delay=".4"><?= $rowOtherBanner['title'] ?></h2>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>
<!-- End Banner -->

<div class="container-fluid">
  <div class="row section-otp-content">
    <div class="col-xs-12 rata">
      <div class="col-xs-12 col-sm-4 reset-col main-left-col">
        <div class="col-md-12 reset-col side-bar-tp">
          <?php $this->load->view('Login') ?>
          <div class="clearfix"></div>
          <?php $this->load->view('GuestBook') ?>
        </div>
      </div>
      <div class="col-xs-12 col-sm-8 reset-col main-right-col">
        <div class="col-md-12 wrap-side-content wow fadeIn" data-wow-delay=".4">
          <div class="col-md-12 side-content">
            <div class="col-md-12 reset-col">
              <div class="breadcrumb-title">
                <ul>
                  <li><a href="index.html"><i class="fa fa-home"></i></a></li>
                  <li><i class="fa fa-angle-right"></i><i class="fa fa-angle-right"></i></li>
                  <li><a href="<?= base_url() ?>"> Beranda</a></li>
                  <li><i class="fa fa-angle-right"></i><i class="fa fa-angle-right"></i></li>
                  <li class="active"><?= $settings['subTitle'] ?></li>
                </ul>
              </div>
            </div>

            <div class="tinymce-title">
              <h2><?= $rowWidyaiswara['judul'] ?></h2>
            </div>
            <br>
            <div>
            <?= $rowWidyaiswara['pengantar'] ?>
            </div>

	  		<?php foreach ($rsPengajarDiklat as $rowPengajarDiklat): ?>
            <div class="clearfix"></div>
            <div class="col-md-12 reset-col wrap-directors">
              <img src="<?= base_url().'asset/archive/'.$rowPengajarDiklat['photo'] ?>" alt="" class="img-circle">
              <h4><?= $rowPengajarDiklat['name'] ?></h4>
              <div>
              <?= $rowPengajarDiklat['pangkatGolongan'] ?>
              </div>
            </div>
        	<?php endforeach; ?>
            <div class="clearfix"></div>
            <div class="col-md-12" style="text-align:center;">
                <ul class="pagination">
	                <?php
			        for ($i = 1; $i <= $pageCount; $i++):
			          if ($i == $pageActive):
			        ?>
			          <li class="active"><a href="javascript:"><?= $i ?></a></li>
			        <?php else: ?>
			          <li><a href="<?= base_url().'widyaiswara/'.$i ?>"><?= $i ?></a></li>
			        <?php
			          endif;
			        endfor;
			        ?>
                </ul>
                <br>
                <br>
                <br>
                <br>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

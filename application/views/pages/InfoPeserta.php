<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">

var jmlPria = <?php echo $jmlPria ?>;
var jmlWanita = <?php echo $jmlWanita ?>;
        
        google.charts.load('current', {
            'packages': ['corechart']
        });
        google.charts.setOnLoadCallback(drawChart);

        function drawChart() {
          
            var data = google.visualization.arrayToDataTable([
                ['Chart', 'Peserta'],
                ['Pria', jmlPria],
                ['Wanita', jmlWanita]
            ]);
            var options = {
                'title': '',
                is3D: true,
            };
            var chart = new google.visualization.PieChart(document.getElementById('ChartJenisKelamin'));
            chart.draw(data, options);
        }
    </script>
    <script type="text/javascript">

        var jmlAllAnggota = <?php echo $jmlAllAnggota ?>;
        var jmlD1D2 = <?php echo $jmlD1D2 ?>;
        var jmlD3 = <?php echo $jmlD3 ?>;
        var jmlD4S1 = <?php echo $jmlD4S1 ?>;
        var jmlS2 = <?php echo $jmlS2 ?>;
        var jmlS3 = <?php echo $jmlS3 ?>;
        var jmlSmpSma = <?php echo $jmlSmpSma ?>;

        google.charts.load('current', {
            'packages': ['corechart']
        });
        google.charts.setOnLoadCallback(drawChart);

        function drawChart() {
            var data = google.visualization.arrayToDataTable([
                ['Chart', 'Peserta'],
                ['D1/D2', jmlD1D2],
                ['D3', jmlD3],
                ['D4/S1', jmlD4S1],
                ['S2', jmlS2],
                ['S3', jmlS3],
                ['Other', jmlSmpSma]
            ]);
            var options = {
                'title': 'Jumlah Seluruh Peserta ' +  jmlAllAnggota,
            };
            var chart = new google.visualization.PieChart(document.getElementById('ChartPendidikan'));
            chart.draw(data, options);
        }
    </script>
    <script type="text/javascript">
var jmlAllAnggota = <?php echo $jmlAllAnggota ?>;
    
var jmlLatsar = <?php echo $jmlLatsar ?>;
var jmlPka = <?php echo $jmlPka ?>;
var jmlPkp = <?php echo $jmlPkp ?>;
var jmlPrajab = <?php echo $jmlPrajab ?>;
var jmlPktuf = <?php echo $jmlPktuf ?>;
var jmlPkti = <?php echo $jmlPkti ?>;

        google.charts.load('current', {
            'packages': ['corechart']
        });
        google.charts.setOnLoadCallback(drawChart);

        function drawChart() {
            var data = google.visualization.arrayToDataTable([
                ['Chart', 'Peserta'],
                ['Latsar', jmlLatsar],
                ['PKA', jmlPka],
                ['PKP', jmlPkp],
                ['Prajab', jmlPrajab],
                ['PKTI', jmlPktuf],
                ['PKTUF', jmlPkti]
            ]);
            var options = {
                'title': 'Jumlah Seluruh Peserta ' +  jmlAllAnggota,
                is3D: true,
            };
            var chart = new google.visualization.PieChart(document.getElementById('ChartJenisDiklat'));
            chart.draw(data, options);
        }
    </script>
    <script type="text/javascript">
    var jmlAllAnggota = <?php echo $jmlAllAnggota ?>;
    var jml1830 = <?php echo $jml1830 ?>;
    var jml3140 = <?php echo $jml3140 ?>;
    var jml4150 = <?php echo $jml4150 ?>;
    var jml51 = <?php echo $jml51 ?>;

        google.charts.load('current', {
            'packages': ['corechart']
        });
        google.charts.setOnLoadCallback(drawChart);

        function drawChart() {
            var data = google.visualization.arrayToDataTable([
                ['Chart', 'Peserta'],
                ['Usia 18 - 30', jml1830],
                ['Usia 31 - 40', jml3140],
                ['Usia 41 - 50', jml3140],
                ['Usia 51 >', jml51]
            ]);
            var options = {
                'title': 'Jumlah Seluruh Peserta  ' +  jmlAllAnggota,
            };
            var chart = new google.visualization.PieChart(document.getElementById('ChartUsia'));
            chart.draw(data, options);
        }
    </script>

<!-- Banner -->
<?php if($rowOtherBanner): ?>
  <div class="container-fluid">
    <div class="row section-pages-banner">
      <div class="col-md-12">
        <div class="banner-box">
          <div class="image-item">
            <img
            src="<?= base_url().'asset/archive/'.$rowOtherBanner['banner'] ?>"
            alt="<?= $rowOtherBanner['title'] ?>"
            class="img-responsive image-bg">
            <div class="overlay"></div>
          </div>
          <div class="banner-title">
            <h2 class="wow fadeIn" data-wow-delay=".4"><?= $rowOtherBanner['title'] ?></h2>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>
<!-- End Banner -->

<div class="container-fluid">
  <div class="row section-contact-us-content section-contact">
    <div class="col-lg-12 wrap-side-content reset-col wow fadeIn" data-wow-delay=".4">
      <div class="col-md-12" style="padding-left:30px; padding-right:45px;">

        <div class="col-md-12 reset-col">
          <div class="breadcrumb-title">
            <ul>
              <li><a href="index.html"><i class="fa fa-home"></i></a></li>
              <li><i class="fa fa-angle-right"></i><i class="fa fa-angle-right"></i></li>
              <li><a href="<?= base_url() ?>"> Beranda</a></li>
              <li><i class="fa fa-angle-right"></i><i class="fa fa-angle-right"></i></li>
              <li class="active"><?= $settings['subTitle'] ?></li>
            </ul>
          </div>
        </div>

      </div>
      <div class="col-md-12 content">

</div>
</div>
<div class="col-md-12 content">
                        <div class="col-md-6">
                            <div class="wrimagecard wrimagecard-topimage">
                                <div class="wrimagecard-topimage_title">
                                    <h4><i class="fa fa-bar-chart"></i> Peserta Menurut Jenis Kelamin
                                        <div class="pull-right badge"><span><?= $jmlAllAnggota ?></span> peserta</div>
                                    </h4>
                                </div>
                                <div class="wrimagecard-topimage_header">

                                    <div id="ChartJenisKelamin" class="chart"></div>

                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="wrimagecard wrimagecard-topimage">
                                <div class="wrimagecard-topimage_title">
                                    <h4>
                                        <i class="fa fa-bar-chart"></i> Peserta Menurut Pendidikan
                                    </h4>
                                </div>
                                <div class="wrimagecard-topimage_header">
                                    <div id="ChartPendidikan" class="chart"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="wrimagecard wrimagecard-topimage">
                                <div class="wrimagecard-topimage_title">
                                    <h4><i class="fa fa-bar-chart"></i> Peserta Menurut Jenis Diklat
                                        <div class="pull-right badge"><span><?= $jmlAllAnggota ?></span> peserta</div>
                                    </h4>
                                </div>
                                <div class="wrimagecard-topimage_header">
                                    <div id="ChartJenisDiklat" class="chart"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="wrimagecard wrimagecard-topimage">
                                <div class="wrimagecard-topimage_title">
                                    <h4><i class="fa fa-bar-chart"></i> Peserta Menurut Usia
                                        <div class="pull-right badge"><span><?= $jmlAllAnggota ?></span> peserta</div>
                                    </h4>
                                </div>
                                <div class="wrimagecard-topimage_header">
                                    <div id="ChartUsia" class="chart"></div>
                                </div>
                            </div>
                        </div>
                    </div>



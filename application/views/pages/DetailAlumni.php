<!-- Banner -->
<?php if($rowOtherBanner): ?>
  <div class="container-fluid">
    <div class="row section-pages-banner">
      <div class="col-md-12">
        <div class="banner-box">
          <div class="image-item">
            <img
            src="<?= base_url().'asset/archive/'.$rowOtherBanner['banner'] ?>"
            alt="<?= $rowOtherBanner['title'] ?>"
            class="img-responsive image-bg">
            <div class="overlay"></div>
          </div>
          <div class="banner-title">
            <h2 class="wow fadeIn" data-wow-delay=".4"><?= $rowOtherBanner['title'] ?></h2>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>
<!-- End Banner -->

<div class="container-fluid">
  <div class="row section-otp-content">
    <div class="col-xs-12 rata">
      <div class="col-xs-12 col-sm-4 reset-col main-left-col">
        <div class="col-md-12 reset-col side-bar-tp">
          <?php $this->load->view('Login') ?>
          <div class="clearfix"></div>
          <?php $this->load->view('GuestBook') ?>
        </div>
      </div>
      <div class="col-xs-12 col-sm-8 reset-col main-right-col">
        <div class="col-md-12 wrap-side-content wow fadeIn" data-wow-delay=".4">
          <div class="col-md-12 side-content">
            <div class="col-md-12 reset-col">
              <div class="breadcrumb-title">
                <ul>
                  <li><a href="index.html"><i class="fa fa-home"></i></a></li>
                  <li><i class="fa fa-angle-right"></i><i class="fa fa-angle-right"></i></li>
                  <li><a href="<?= base_url() ?>"> Beranda</a></li>
                  <li><i class="fa fa-angle-right"></i><i class="fa fa-angle-right"></i></li>
                  <li><a href="<?= base_url() ?>sttpl_alumni"> STTPL/Alumni</a></li>
                  <li><i class="fa fa-angle-right"></i><i class="fa fa-angle-right"></i></li>
                  <li class="active"><?= $settings['subTitle'] ?></li>
                </ul>
              </div>
            </div>

            <div class="tinymce-ac-brief-title">
              <h2>
                Daftar Alumni Badan Diklat Daerah Provinsi Lampung
                <a style="float: right" data-ref="btnPdf" href="javascript:"><i style="font-size: 32px" class="fa fa-file-pdf-o" aria-hidden="true"></i></a>
              </h2>
            </div>
            
            <br>
            <br>

            <div class="col-md-12 reset-col">
              <h5 class="col-xs-6 reset-col left-sub">Nama Program :</h5>
              <h5 class="col-xs-6 reset-col right-sub"><?= $rowDiklat['namaDiklat'] ?></h5>
            </div>
            <div class="col-md-12 reset-col">
              <h5 class="col-xs-6 reset-col left-sub">Waktu Pelaksanaan :</h5>
              <h5 class="col-xs-6 reset-col right-sub"><?= $rowDiklat['pelaksanaanMulai'] ?> - <?= $rowDiklat['pelaksanaanSelesai'] ?></h5>
            </div>

            <div class="col-md-12 reset-col wrap-ac-brief">
              <br>
              <br>
              <div id="no-more-tables">
                <table class="col-md-12 reset-col table-striped table-condensed cf">
                  <thead class="cf">
                    <tr>
                      <th class="numbers">NO</th>
                      <th class="numbers">NIP</th>
                      <th class="numbers">NAMA</th>
                      <th class="numbers">NOMOR REGISTRASI</th>
                    </tr>
                  </thead>
                  <tbody>
                   <?php foreach ($rsKelulusan as $rowKelulusan): ?>
                    <tr>
                      <td data-title="No" class="numbers">1</td>
                      <td data-title="NIP" class="numbers"><?= $rowKelulusan['nip'] ?></td>
                      <td data-title="Nama"><?= $rowKelulusan['nama'] ?></td>
                      <td data-title="No. Registrasi"><?= $rowKelulusan['noRegistrasi'] ?></td>
                    </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

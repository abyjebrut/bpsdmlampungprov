<!-- Banner -->
<?php if($rowOtherBanner): ?>
  <div class="container-fluid">
    <div class="row section-pages-banner">
      <div class="col-md-12">
        <div class="banner-box">
          <div class="image-item">
            <img
            src="<?= base_url().'asset/archive/'.$rowOtherBanner['banner'] ?>"
            alt="<?= $rowOtherBanner['title'] ?>"
            class="img-responsive image-bg">
            <div class="overlay"></div>
          </div>
          <div class="banner-title">
            <h2 class="wow fadeIn" data-wow-delay=".4"><?= $rowOtherBanner['title'] ?></h2>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>
<!-- End Banner -->

<div class="container-fluid">
  <div class="row section-otp-content">
    <div class="col-xs-12 rata">
      <div class="col-xs-12 col-sm-4 reset-col main-left-col">
        <div class="col-md-12 reset-col side-bar-tp">
          <?php $this->load->view('Login') ?>

          <div class="clearfix"></div>

          <?php foreach ($rsPages as $_rowPages): ?>
            <div class="panel panel-default">
              <div class="panel-heading single">
                <a
                class="<?php if ($activeId == $_rowPages['id']) echo 'active'; ?>"
                href="<?= base_url().'page/'.$_rowPages['id'] ?>"><?= $_rowPages['title'] ?></a>
              </div>
            </div>
          <?php endforeach ?>

          <div class="clearfix"></div>

          <?php $this->load->view('GuestBook') ?>
        </div>
      </div>
      <div class="col-xs-12 col-sm-8 reset-col main-right-col">
        <div class="col-md-12 wrap-side-content wow fadeIn" data-wow-delay=".4">
          <div class="col-md-12 side-content">

            <div class="col-md-12 reset-col">
              <div class="breadcrumb-title">
                <ul>
                  <li><a href="index.html"><i class="fa fa-home"></i></a></li>
                  <li><i class="fa fa-angle-right"></i><i class="fa fa-angle-right"></i></li>
                  <li><a href="<?= base_url() ?>"> Beranda</a></li>
                  <li><i class="fa fa-angle-right"></i><i class="fa fa-angle-right"></i></li>
                  <li class="active"><?= $settings['subTitle'] ?></li>
                </ul>
              </div>
            </div>

            <div class="tinymce-title">
              <h2><?= $rowPages['title'] ?></h2>
            </div>
            <br>
            <div class="page-content">
            <?php
			    if ($rowPages['mediaTypeId'] == 1):
			      // Image full width
			      if ($rowPages['contentLayoutId'] == 1):
			        ?>
			        <img src="<?= base_url().'asset/archive/'.$rowPages['image'] ?>" style="width: 100%">

			        <!-- Image on left -->
			      <?php elseif ($rowPages['contentLayoutId'] == 2): ?>
			        <img src="<?= base_url().'asset/archive/'.$rowPages['image'] ?>" align="left" style="width: 300px">

			        <!-- Image on right -->
			      <?php elseif ($rowPages['contentLayoutId'] == 3): ?>
			        <img src="<?= base_url().'asset/archive/'.$rowPages['image'] ?>" align="right" style="width: 300px">

			      <?php endif; ?>

			      <!-- Type Video -->
			    <?php elseif ($rowPages['mediaTypeId'] == 2): ?>
			      <div class="embed-responsive embed-responsive-16by9">
			        <?= $rowPages['videoEmbed'] ?>
			      </div>

			      <?php
			    endif;
			    ?>

			    <?= $rowPages['description'] ?>
    			<?= $rowPages['moreDescription'] ?>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Banner -->
<?php if($rowOtherBanner): ?>
  <div class="container-fluid">
    <div class="row section-pages-banner">
      <div class="col-md-12">
        <div class="banner-box">
          <div class="image-item">
            <img
            src="<?= base_url().'asset/archive/'.$rowOtherBanner['banner'] ?>"
            alt="<?= $rowOtherBanner['title'] ?>"
            class="img-responsive image-bg">
            <div class="overlay"></div>
          </div>
          <div class="banner-title">
            <h2 class="wow fadeIn" data-wow-delay=".4"><?= $rowOtherBanner['title'] ?></h2>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>
<!-- End Banner -->


<div class="container-fluid">
  <div class="row section-otp-content">
    <div class="col-xs-12 rata">
      <div class="col-xs-12 col-sm-4 reset-col main-left-col">
        <div class="col-md-12 reset-col side-bar-tp">
          <?php $this->load->view('Login') ?>
          <div class="clearfix"></div>
          <?php $this->load->view('GuestBook') ?>
        </div>
      </div>
      <div class="col-xs-12 col-sm-8 reset-col main-right-col">
        <div class="col-md-12 wrap-side-content wow fadeIn" data-wow-delay=".4">
          <div class="col-md-12 side-content">

            <div class="col-md-12 reset-col">
              <div class="breadcrumb-title">
                <ul>
                  <li><a href="index.html"><i class="fa fa-home"></i></a></li>
                  <li><i class="fa fa-angle-right"></i><i class="fa fa-angle-right"></i></li>
                  <li><a href="<?= base_url() ?>"> Beranda</a></li>
                  <li><i class="fa fa-angle-right"></i><i class="fa fa-angle-right"></i></li>
                  <li class="active"><?= $settings['subTitle'] ?></li>
                </ul>
              </div>
            </div>

          <div class="col-md-12 reset-col wrap-ac-brief">
            <div class="col-md-12 reset-col combo-pencarian">
              <div>
                <h5>Pencarian Data:</h5>
              </div>
              <div class="col-md-12 input-group tb-filter">
                  <span class="input-group-addon"><i class="fa fa-search"></i></span>
                  <input data-ref="keyword" class="form-control" placeholder="Masukkan kata kunci ..." type="text">
              </div>
            </div>
            <div id="no-more-tables">
              <table class="col-md-12 reset-col table-striped table-condensed cf">
                  <thead class="cf">
                      <tr>
                          <th class="numbers">NO</th>
                          <th class="numbers">Tahun</th>
                          <th class="numbers">Target Peserta</th>
                          <th class="numbers">Realisasi Peserta</th>
                      </tr>
                  </thead>
                  <tbody data-ref="listData">
                <?php
              $num = 0;
              foreach ($rsDiklat as $rowDiklat):
                $num++;
              ?>
              <tr>
                      <td data-title="No" class="numbers"><?= $num ?>.</td>
                      <td data-title="Tahun" class="numbers"><?= $rowDiklat['tahunDiklat'] ?></td>
                      <td data-title="Target Peserta" class="numbers"><?= $rowDiklat['_kuota'] ?> Orang</td>
                      <td data-title="Widyaiswara" class="numbers"><?= $rowDiklat['kuotaDiklat'] ?> Orang</td>
                  </tr>
                  
                  <?php endforeach; ?>
              </tbody>
          </table>
      </div>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>

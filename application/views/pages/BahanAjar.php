<!-- Banner -->
<?php if($rowOtherBanner): ?>
  <div class="container-fluid">
    <div class="row section-pages-banner">
      <div class="col-md-12">
        <div class="banner-box">
          <div class="image-item">
            <img
            src="<?= base_url().'asset/archive/'.$rowOtherBanner['banner'] ?>"
            alt="<?= $rowOtherBanner['title'] ?>"
            class="img-responsive image-bg">
            <div class="overlay"></div>
          </div>
          <div class="banner-title">
            <h2 class="wow fadeIn" data-wow-delay=".4"><?= $rowOtherBanner['title'] ?></h2>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>
<!-- End Banner -->

<div class="container-fluid">
  <div class="row section-otp-content">
    <div class="col-xs-12 rata">
      <div class="col-xs-12 col-sm-4 reset-col main-left-col">
        <div class="col-md-12 reset-col side-bar-tp">
          <?php $this->load->view('Login') ?>
          <div class="clearfix"></div>
          <?php $this->load->view('GuestBook') ?>
        </div>
      </div>
      <div class="col-xs-12 col-sm-8 reset-col main-right-col">
        <div class="col-md-12 wrap-side-content wow fadeIn" data-wow-delay=".4">
          <div class="col-md-12 side-content">

            <div class="col-md-12 reset-col">
              <div class="breadcrumb-title">
                <ul>
                  <li><a href="index.html"><i class="fa fa-home"></i></a></li>
                  <li><i class="fa fa-angle-right"></i><i class="fa fa-angle-right"></i></li>
                  <li><a href="<?= base_url() ?>"> Beranda</a></li>
                  <li><i class="fa fa-angle-right"></i><i class="fa fa-angle-right"></i></li>
                  <li class="active"><?= $settings['subTitle'] ?></li>
                </ul>
              </div>
            </div>

          <div class="tinymce-ac-brief-title">
            <h2><?= $rowBahanAjar['judul'] ?></h2>
          </div>
          <div>
			<?= $rowBahanAjar['pengantar'] ?>
          </div>
          <div class="col-md-12 reset-col wrap-bahan-ajar">
            <div class="col-md-12 reset-col combo-pencarian">
              <h5>Filter Data Berdasarkan :</h5>
              <select data-ref="jenisDiklat" class="selectpicker col-sm-4 col-xs-12 reset-col">
                <option data-hidden="true" value="-1">--Jenis Diklat--</option>
                <?php foreach ($rsJenisDiklat as $rowJenisDiklat): ?>
                <option value="<?= $rowJenisDiklat['id'] ?>"><?= $rowJenisDiklat['jenisDiklat'] ?></option>
                <?php endforeach; ?>
              </select>

              <!-- <select data-ref="namaDiklat" class="selectpicker col-sm-7 col-xs-12 reset-col"> -->

              <select style="margin-top: 8px" data-ref="namaDiklat" class="selectpicker col-sm-12 col-xs-12 reset-col">
                <option data-hidden="true" value="-1">--Nama Diklat--</option>
                <?php foreach ($rsArsipMateri as $rowArsipMateri): ?>
                <option value="<?= $rowArsipMateri['id'] ?>">
                  <?= $rowArsipMateri['namaDiklat'] ?>
                </option>
                <?php endforeach; ?>
              </select>

              <!-- <select data-ref="namaDiklat" class="selectpicker col-sm-7 col-xs-12 reset-col" data-live-search="true">
                <option data-hidden="true" value="-1">--Nama Diklat--</option>
                <?php foreach ($rsArsipMateri as $rowArsipMateri): ?>
                <option value="<?= $rowArsipMateri['id'] ?>">
                  <?= $rowArsipMateri['namaDiklat'] ?>
                </option>
                <?php endforeach; ?>
              </select> -->
            </div>
            <div id="no-more-tables">
              <table class="col-md-12 reset-col table-striped table-condensed cf">
                <thead class="cf">
                  <tr>
                    <th class="numbers">NO</th>
                    <th class="numbers">MATA DIKLAT</th>
                    <th class="numbers">FILE</th>
                    <th class="numbers">WIDYAISWARA</th>
                  </tr>
                </thead>
                <tbody data-ref="listData">
                	<?php
                  $num = 0;
                  foreach ($rsArsipMateri as $rowArsipMateri):
                    $num++;
                  ?>
                  <tr>
                    <td data-title="No" class="numbers"><?= $num ?>.</td>
                    <td data-title="Mata Diklat"><?= $rowArsipMateri['mataDiklat'] ?></td>
                    <td data-title="File" class="numbers">
                <?php if ($rowArsipMateri['fileWord']): ?>
                    <a href="<?= base_url().'asset/archive/'.$rowArsipMateri['fileWord'] ?>"><i class="fa fa-file-word-o" aria-hidden="true"></i></a>
                <?php endif; ?>
	              <?php if ($rowArsipMateri['fileExcel']): ?>
				            <a href="<?= base_url().'asset/archive/'.$rowArsipMateri['fileExcel'] ?>"><i class="fa fa-file-excel-o" aria-hidden="true"></i></a>
				        <?php endif; ?>
				        <?php if ($rowArsipMateri['filePowerPoint']): ?>
				            <a href="<?= base_url().'asset/archive/'.$rowArsipMateri['filePowerPoint'] ?>"><i class="fa fa-file-powerpoint-o" aria-hidden="true"></i></a>
				        <?php endif; ?>
				        <?php if ($rowArsipMateri['filePdf']): ?>
				            <a href="<?= base_url().'asset/archive/'.$rowArsipMateri['filePdf'] ?>"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a>
				        <?php endif; ?>
                    </td>
                    <td data-title="Widyaiswara">
                    <?php
	                $pengajars = explode(',', $rowArsipMateri['pengajar']);
	                foreach ($pengajars as $pengajar):
	                ?>
                      <br><?= $pengajar?>
                      <?php endforeach; ?>
                    </td>
                  </tr>
                  <?php endforeach; ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>

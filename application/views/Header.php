<div class="container-fluid">
  <div class="header" style="z-index: 999999999;">
    <header class="masthead hidden-xs">
      <div class="col-md-12 reset-col">
        <div class="header-top">
          <ul>
            <li>
              <a href="<?= base_url() ?>">
                <img src="<?= base_url() ?>asset/diklat/img/logo-text.png" alt="Logo" class="wow fadeInLeft" data-wow-delay=".2">

                <div style="width:800px;" class="wow fadeIn" data-wow-delay=".2">
                  <h3>SISTEM INFORMASI APLIKASI DIKLAT</h3>
                  <h3>BADAN PENGEMBANGAN SUMBER DAYA MANUSIA DAERAH</h3>
                  <h3>PROVINSI LAMPUNG</h3>
                </div>
              </a>
              <div class="clearfix"></div>
            </li>

            <li class="pull-right hidden-sm wow fadeIn" data-wow-delay=".2">
              <div class="social">
                <a target="_blank" class="facebookBtn smGlobalBtn" href="<?= $rowSocialNetwork['facebookPage'] ?>"></a>
                <a target="_blank" class="twitterBtn smGlobalBtn" href="<?= $rowSocialNetwork['twitterPage'] ?>"></a>
                <a target="_blank" class="googleplusBtn smGlobalBtn" href="<?= $rowSocialNetwork['googlePlusPage'] ?>"></a>
                <a target="_blank" class="linkedinBtn smGlobalBtn" href="<?= $rowSocialNetwork['instagramPage'] ?>"></a>
              </div>
            </li>
          </ul>
        </div>
      </div>
      <div class="clearfix"></div>
    </header>
    <div class="col-md-12 reset-col">
      <div class="header-menu">
        <div id="nav">
          <div class="navbar navbar-default navbar-static">
            <div class="container-fluid">
              <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                </button>
                <div class="title-xs visible-xs">
                  <img src="<?= base_url() ?>asset/diklat/img/logo-xs.png" alt="logo">
                  <h3>SISTEM INFORMASI APLIKASI DIKLAT</h3>
                  <h3>BADAN PENDIDIKAN DAN LATIHAN</h3>
                  <h3>DAERAH PROVINSI LAMPUNG</h3>
                </div>
                <div class="clearfix"></div>
              </div>
              <div data-scope="MenuBar" class="navbar-collapse collapse" id="navbar">
                <ul class="nav navbar-nav navbar-left">
                  <!-- Menu -->
                  <?php
                  foreach($rsMenu as $rowMenu):
                    if ($rowMenu['menuBehaviorId'] == 1):
                      ?>

                      <?php
                      if ($rowMenu['onlyMemberId'] == 1):
                        if (isset($_SESSION['id']) && $_SESSION['tipeAkunId'] == 1):
                      ?>
                        <li><a href="<?= base_url().$rowMenu['uri'] ?>"><?= $rowMenu['menuCaption'] ?></a></li>
                      <?php
                        endif;
                      else:
                      ?>
                        <li><a href="<?= base_url().$rowMenu['uri'] ?>"><?= $rowMenu['menuCaption'] ?></a></li>
                      <?php endif; ?>

                    <?php elseif ($rowMenu['menuBehaviorId'] == 2): ?>
                      <li><a href="<?= $rowMenu['url'] ?>"><?= $rowMenu['menuCaption'] ?></a></li>

                    <?php elseif ($rowMenu['menuBehaviorId'] == 3): ?>
                      <li class="dropdown">
                        <a href="javascript:" class="dropdown-toggle" data-toggle="dropdown">
                          <?= $rowMenu['menuCaption'] ?> <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu">
                          <?php
                          foreach ($rowMenu['subMenu'] as $rowSubMenu):
                            if ($rowSubMenu['subMenuBehaviorId'] == 1):
                              ?>
                              <li class="menu-item"><a href="<?= base_url().$rowSubMenu['uri'] ?>"><?= $rowSubMenu['subMenuCaption'] ?></a></li>
                            <?php elseif ($rowSubMenu['subMenuBehaviorId'] == 2): ?>
                              <li class="menu-item">
                                <a href="<?= $rowSubMenu['url'] ?>" <?php if($rowSubMenu['openUrlOnNewTabId'] == 1) echo 'target="_blank"' ?>>
                                  <?= $rowSubMenu['subMenuCaption'] ?>
                                </a>
                              </li>
                              <?php
                            endif;
                          endforeach;
                          ?>
                        </ul>
                      </li>

                    <?php else: ?>
                      <li><a href="javascript:"><?= $rowMenu['menuCaption'] ?></a></li>

                      <?php
                    endif;
                  endforeach;
                  ?>
                  <!-- End Menu -->

                  <?php if (isset($_SESSION['tipeAkunId'])): ?>
                    <?php if ($_SESSION['tipeAkunId']==2): ?>
                      <li><a href="<?= base_url() ?>user_profile">User Profile</a></li>
                      <li><a href="<?= base_url() ?>akd">AKD</a></li>
                    <?php endif; ?>
                  <?php endif; ?>

                  <?php if (isset($_SESSION['id'])): ?>
                    <li><a data-ref="linkLogout" href="javascript:">Logout</a></li>
                  <?php endif; ?>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="clearfix"></div>

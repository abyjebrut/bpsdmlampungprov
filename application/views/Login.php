<?php if (!isset($_SESSION['id'])): ?>
  <div data-scope="Login" class="col-xs-12 reset-col login-sidebar">
    <h5>Login atau <a href="#" data-toggle="modal" data-target="#buatAkun">Buat Akun</a></h5>
    <div class="col-xs-12">
      <div class="input-group">
        <span class="input-group-addon"><i class="fa fa-user"></i></span>
        <input data-ref="nip" type="text" class="form-control" name="NIP" placeholder="NIP">
      </div>
      <div class="input-group">
        <span class="input-group-addon"><i class="fa fa-lock"></i></span>
        <input data-ref="kataSandi" type="password" class="form-control" name="password" placeholder="Kata Sandi">
      </div>
      <button data-ref="btnLogin" class="btn btn-lg btn-primary btn-login pull-left" type="button">Login</button>
      
      <a href="javascript:" class="pull-right" data-toggle="modal" data-target="#lupaSandi">Lupa Kata Sandi ?   </a>
       <br>
      
    </div>
    
    <div class="col-xs-12" style="border-top:2px solid #e7e7e7; margin-top:20px; padding-top:15px;">
    <h5 style="font-color:#000; font-size:12px;"> Jika email Anda tidak terkirim, anda bisa mengulang untuk mengirim kembali dengan klik tautan ini
    <a href="javascript:" class="" data-toggle="modal" data-target="#reEmail"> <i>Verifikasi Email</i></a>
    </h5>
  </div>
    
    </div>
    
   
<?php else: ?>
  <div class="col-xs-12 reset-col login-sidebar">
    <div class="col-xs-12" align="center">
      <a href="user-page.html">
      <?php if ($rowAnggota['foto']): ?>
        <img alt="User Pic" src="<?= base_url().'asset/archive/'.$rowAnggota['foto'] ?>" class="img-circle img-responsive img-login">
      <?php endif; ?>
      </a>
      <br>
      <span><b>NIP : <span class="isi-nip"><?= $rowAnggota['nip'] ?></span></b>
    </span>
    <br>
    <span><b>Nama : <span class="isi-nama"><?= $rowAnggota['nama'] ?></span></b>
  </span>
</div>
</div>
<?php endif; ?>

<div data-scope="Subscriber">
  <form role="form">
    <div class="form-group">
      <input data-ref="email" type="text" class="form-control" placeholder="Email">
    </div>
    <button data-ref="btnSubscribe" type="button" class="btn btn-primary">Subscribe</button>
  </form>
</div>

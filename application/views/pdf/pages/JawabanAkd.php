<table>
	<tr >
        <td style="width: 526px; border: 1px solid black;
    padding: 5px; text-align:center;">
						<h2>
							<?= $this->string('ISIAN KOMPETENSI') ?><br>
	            <?= $this->string('ANALISIS KEBUTUHAN DIKLAT (AKD)') ?>
						</h2>
        </td>
    </tr>
    <br>&nbsp;

    <tr>
        <td style="width: 100px;">
            <?= $this->string('JABATAN') ?>
        </td>
        <td style="width: 400px;">
            : <?= $row['jabatan'] ?>
        </td>
    </tr>

    <tr>
        <td style="width: 100px;">
            <?= $this->string('UNIT_KERJA') ?>
        </td>
        <td style="width: 400px;">
            : <?= $row['unitKerja'] ?>
        </td>
    </tr>

    <tr>
        <td style="width: 100px;">
            <?= $this->string('INSTANSI') ?>
        </td>
        <td style="width: 400px;">
            : <?= $row['instansi'] ?>
        </td>
    </tr>
    <br>&nbsp;
    <tr >
        <td style="width: 100px; border: 1px solid black;
    padding: 5px; text-align:center;">
            <?= $this->string('RESPONDEN') ?>
        </td>
    </tr>
    <br>&nbsp;
    <tr>
        <td style="width: 100px;">
            <?= $this->string('NIP') ?>
        </td>
        <td style="width: 400px;">
            : <?= $row['nip'] ?>
        </td>
    </tr>

    <tr>
        <td style="width: 100px;">
            <?= $this->string('NAMA') ?>
        </td>
        <td style="width: 400px;">
            : <?= $row['nama'] ?>
        </td>
    </tr>

    <tr>
        <td style="width: 100px;">
            <?= $this->string('KEDUDUKAN') ?>
        </td>
        <td style="width: 400px;">
            : <?= $row['kedudukan'] ?>
        </td>
    </tr>

</table>
<br>
<br>

<table class="table-list">
    <tr class="thead">
        <th style="text-align:center;" class="num"><?= $this->string('NUM') ?></th>
        <th style="width: 390px; text-align:center;"><?= $this->string('RUMUSAN KOMPETENSI') ?></th>
        <th style="width: 120px; text-align:center;"><?= $this->string('KRITERIA PENILAIAN') ?></th>
    </tr>

    <?php
    $num = 0;
    foreach($rsItems as $row1):
        $num++;
    ?>
    <tr>
        <td ><?= $num ?>.</td>
        <td ><?= $row1['rumusanKompetensi'] ?></td>
        <td><?= $row1['tingkatKesanggupan'] ?></td>
    </tr>
    <?php endforeach; ?>
</table>

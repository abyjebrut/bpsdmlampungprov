<table>
	<?php if ($rs): ?>
    <tr>
        <td style="width: 100px;">
            <?= $this->string('Nomor') ?>
        </td>
        <td style="width: 400px;">
            : <?= $rs['nomor'] ?> 
        </td>
    </tr>
    <tr >
        <td style="width: 526px; text-align:center;">
						<h2>
							<?= $rs['namaDiklat'] ?> 
						</h2>
        </td>
    </tr>
    
    <tr>
        <td style="width: 400px; text-align:right;">
             <?= $rs['angkatan'] ?> 
        </td>
    </tr>
    <?php endif; ?>
</table>
<br>
<br>

<table class="table-list">
    <tr class="thead">
        <th class="num"><?= $this->string('NUM') ?></th>
        <th style="width: 65px"><?= $this->string('Tanggal') ?></th>
        <th style="width: 65px"><?= $this->string('Waktu') ?></th>
        <th style="width: 50"><?= $this->string('JLH Jam') ?></th>
        <th style="width: 100px"><?= $this->string('Mata Pelajaran') ?></th>
        <th style="width: 90px"><?= $this->string('Tempat') ?></th>
        <th style="width: 70px"><?= $this->string('Lokasi') ?></th>
        <th style="width: 90px"><?= $this->string('Widyaiswara/Penatar/Instruktur') ?></th>
    </tr>

    <?php
    $num = 0;
    foreach($rsItems as $row):
        $num++;
    ?>
    <tr>
        <td><?= $num ?>.</td>
        <td><?= $row['_tanggal'] ?></td>
        <td><?= $row['jamMulai'] ?>-<?= $row['jamSelesai'] ?></td>
        <td><?= $row['jumlahJam'] ?></td>
        <td><?= $row['mataPelajaran'] ?></td>
        <td><?= $row['tempat'] ?></td>
        <td><?= $row['lokasi'] ?></td>
        <td><?= $row['namaPengajar'] ?></td>
    </tr>
    <?php endforeach; ?>
</table>

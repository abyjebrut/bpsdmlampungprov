<table class="table-detail">
    <tr>
        <th style="width: 100px"><?= $this->string('NAME') ?></th>
        <td style="width: 430px">: <?= $name ?></td>
    </tr>
    <tr>
        <th><?= $this->string('EMAIL') ?></th>
        <td>: <?= $email ?></td>
    </tr>
    <tr>
        <th><?= $this->string('GENDER') ?></th>
        <td>: <?= $gender ?></td>
    </tr>
    <tr>
        <th><?= $this->string('DATE_OF_BIRTH') ?></th>
        <td>: <?= $dateOfBirth ?></td>
    </tr>
    <tr>
        <th><?= $this->string('MOBILE_NUMBER') ?></th>
        <td>: <?= $mobileNumber ?></td>
    </tr>
    <tr>
        <th><?= $this->string('BRANCH_NAME') ?></th>
        <td>: <?= $branchName ?></td>
    </tr>
    <tr>
        <th><?= $this->string('ADDRESS') ?></th>
        <td>: <?= $address ?></td>
    </tr>
    
</table>
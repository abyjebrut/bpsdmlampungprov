<h4><u>BPSDM PROVINSI LAMPUNG</u></h4>
<table class="table-detail">
    <tr>
        <th></th>
        <td rowspan="3" width="100"> <img
            src="<?= base_url().'asset/archive/'.$foto ?>"
            ></td>
    </tr>
</table>

<table class="table-detail">
	<tr>
        <th style="width: 140px"><?= $this->string('KODE_PESERTA') ?>1</th>
        <td style="width: 390px">: <?= $kodePeserta ?></td>
    </tr>
    <tr>
        <th style="width: 140px"><?= $this->string('NIP') ?></th>
        <td style="width: 390px">: <?= $nip ?></td>
    </tr>
    <tr>
        <th><?= $this->string('NAMA') ?></th>
        <td>: <?= $nama ?></td>
    </tr>
    <tr>
        <th><?= $this->string('EMAIL') ?></th>
        <td>: <?= $email ?></td>
    </tr>
    <tr>
        <th><?= $this->string('JENIS_DIKLAT') ?></th>
        <td>: <?= $jenisDiklat ?></td>
    </tr>
    <tr>
        <th><?= $this->string('KODE') ?></th>
        <td>: <?= $kode ?></td>
    </tr>
    <tr>
        <th><?= $this->string('NAMA_DIKLAT') ?></th>
        <td>: <?= $namaDiklat ?></td>
    </tr>
    <tr>
        <th><?= $this->string('PELAKSANAAN_MULAI') ?></th>
        <td>: <?= $pelaksanaanMulai ?></td>
    </tr>
    <tr>
        <th><?= $this->string('PELAKSANAAN_SELESAI') ?></th>
        <td>: <?= $pelaksanaanSelesai ?></td>
    </tr>
    <tr>
        <th><?= $this->string('PANGKAT_GOLONGAN') ?></th>
        <td>: <?= $pangkatGolongan ?></td>
    </tr>
    <tr>
        <th><?= $this->string('JABATAN_ESELON') ?></th>
        <td>: <?= $jabatanEselon ?></td>
    </tr>
    <tr>
        <th><?= $this->string('UNIT_KERJA') ?></th>
        <td>: <?= $unitKerja ?></td>
    </tr>
    <tr>
        <th><?= $this->string('INSTANSI') ?></th>
        <td>: <?= $instansi ?></td>
    </tr>
    <tr>
        <th><?= $this->string('INSTANSI_LAINNYA') ?></th>
        <td>: <?= $instansiLainnya ?></td>
    </tr>
    <tr>
        <th><?= $this->string('PENDIDIKAN_TERAKHIR') ?></th>
        <td>: <?= $pendidikanTerakhir ?></td>
    </tr>
    <tr>
        <th><?= $this->string('GENDER') ?></th>
        <td>: <?= $jenisKelamin ?></td>
    </tr>
    <tr>
        <th><?= $this->string('TEMPAT_LAHIR') ?></th>
        <td>: <?= $tempatLahir ?></td>
    </tr>
    <tr>
        <th><?= $this->string('DATE_OF_BIRTH') ?></th>
        <td>: <?= $tanggalLahir ?></td>
    </tr>
    <tr>
        <th><?= $this->string('AGAMA') ?></th>
        <td>: <?= $agama ?></td>
    </tr>
    <tr>
        <th><?= $this->string('ALAMAT') ?></th>
        <td>: <?= $alamat ?></td>
    </tr>
    <tr>
        <th><?= $this->string('TELP_HP') ?></th>
        <td>: <?= $telpHp ?></td>
    </tr>
    
    
</table>
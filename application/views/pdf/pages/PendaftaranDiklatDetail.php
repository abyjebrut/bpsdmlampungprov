<!-- <u>BPSDM PROVINSI LAMPUNG</u>
<table class="table-detail">
    <tr>
        <th></th>
        <td rowspan="3" width="100"> <img
            src="<?= base_url().'asset/archive/'.$foto ?>"
            ></td>
    </tr>
</table>

<table class="table-detail">
	<tr>
        <th style="width: 140px"><?= $this->string('KODE_PESERTA') ?>1</th>
        <td style="width: 390px">: <?= $kodePeserta ?></td>
    </tr>
    <tr>
        <th style="width: 140px"><?= $this->string('NIP') ?></th>
        <td style="width: 390px">: <?= $nip ?></td>
    </tr>
    <tr>
        <th><?= $this->string('NAMA') ?></th>
        <td>: <?= $nama ?></td>
    </tr>
    <tr>
        <th><?= $this->string('EMAIL') ?></th>
        <td>: <?= $email ?></td>
    </tr>
    <tr>
        <th><?= $this->string('JENIS_DIKLAT') ?></th>
        <td>: <?= $jenisDiklat ?></td>
    </tr>
    <tr>
        <th><?= $this->string('KODE') ?></th>
        <td>: <?= $kode ?></td>
    </tr>
    <tr>
        <th><?= $this->string('NAMA_DIKLAT') ?></th>
        <td>: <?= $namaDiklat ?></td>
    </tr>
    <tr>
        <th><?= $this->string('PELAKSANAAN_MULAI') ?></th>
        <td>: <?= $pelaksanaanMulai ?></td>
    </tr>
    <tr>
        <th><?= $this->string('PELAKSANAAN_SELESAI') ?></th>
        <td>: <?= $pelaksanaanSelesai ?></td>
    </tr>
    <tr>
        <th><?= $this->string('PANGKAT_GOLONGAN') ?></th>
        <td>: <?= $pangkatGolongan ?></td>
    </tr>
    <tr>
        <th><?= $this->string('JABATAN_ESELON') ?></th>
        <td>: <?= $jabatanEselon ?></td>
    </tr>
    <tr>
        <th><?= $this->string('UNIT_KERJA') ?></th>
        <td>: <?= $unitKerja ?></td>
    </tr>
    <tr>
        <th><?= $this->string('INSTANSI') ?></th>
        <td>: <?= $instansi ?></td>
    </tr>
    <tr>
        <th><?= $this->string('INSTANSI_LAINNYA') ?></th>
        <td>: <?= $instansiLainnya ?></td>
    </tr>
    <tr>
        <th><?= $this->string('PENDIDIKAN_TERAKHIR') ?></th>
        <td>: <?= $pendidikanTerakhir ?></td>
    </tr>
    <tr>
        <th><?= $this->string('GENDER') ?></th>
        <td>: <?= $jenisKelamin ?></td>
    </tr>
    <tr>
        <th><?= $this->string('TEMPAT_LAHIR') ?></th>
        <td>: <?= $tempatLahir ?></td>
    </tr>
    <tr>
        <th><?= $this->string('DATE_OF_BIRTH') ?></th>
        <td>: <?= $tanggalLahir ?></td>
    </tr>
    <tr>
        <th><?= $this->string('AGAMA') ?></th>
        <td>: <?= $agama ?></td>
    </tr>
    <tr>
        <th><?= $this->string('ALAMAT') ?></th>
        <td>: <?= $alamat ?></td>
    </tr>
    <tr>
        <th><?= $this->string('TELP_HP') ?></th>
        <td>: <?= $telpHp ?></td>
    </tr>        
</table> -->

<div id="wrapper">
        <div class="container form-cetak-pendaftaran">
            <div class="row">
                <div class="content-fcp">
                    <div class="col-100">
                        <h5 class="fcp-title"> BPSDM PROVINSI LAMPUNG</h5>
                    </div>
                    <div class="cp1">
                        <div class="cp1-1">.</div>
                        <div class="cp1-2">
                            
                        </div>
                        <div class="cp1-3">
                            <table>
                                <tr>
                                    <td>Nomor Absensi</td>
                                    <td>:</td>
                                    <td><span></span></td>
                                </tr>
                                <tr>
                                    <td>Angkatan</td>
                                    <td>:</td>
                                    <td><span></span></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="cp2">
                        <div class="cp100">
                            <h5 class="biodata-title">BIODATA</h5>
                        </div>
                        <div class="cp1-1">
                            <table>
                                <tr>
                                    <td class="tbcol-1 col-wrap">Nama Diklat</td>
                                    <td class="tbcol-2 col-wrap">:</td>
                                    <td class="tbcol-3 col-wrap">DIKLAT PIM TK.III ANGKATAN I BAGI APARATUR DI LINGKUNGAN PEMERINTAH KABUPATEN/KOTA SE-PROVINSI LAMPUNG TAHUN ANGGARAN 2019</td>
                                </tr>
                                <tr>
                                    <td class="tbcol-1">Tanggal Pelaksanaan</td>
                                    <td class="tbcol-2">:</td>
                                    <td class="tbcol-3">24 Maret 2019 s.d. 12 Juli 2019</td>
                                </tr>
                                <tr>
                                    <td class="tbcol-1">Tempat Penyelenggaraan</td>
                                    <td class="tbcol-2">:</td>
                                    <td class="tbcol-3">BPSDM PROVINSI LAMPUNG</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="cp2">
                        <div class="cp1-1">
                            <table>
                                <tr>
                                    <td class="tbcol-1">01. Nama Lengkap</td>
                                    <td class="tbcol-2">:</td>
                                    <td class="tbcol-3"></td>
                                </tr>
                                <tr>
                                    <td class="tbcol-1">02. NIP</td>
                                    <td class="tbcol-2">:</td>
                                    <td class="tbcol-3"></td>
                                </tr>
                                <tr>
                                    <td class="tbcol-1">03. Alamat Rumah</td>
                                    <td class="tbcol-2">:</td>
                                    <td class="tbcol-3"></td>
                                </tr>
                                <tr>
                                    <td class="tbcol-1">04. Tempat Lahir</td>
                                    <td class="tbcol-2">:</td>
                                    <td class="tbcol-3"></td>
                                </tr>
                                <tr>
                                    <td class="tbcol-1">05. Tanggal Lahir</td>
                                    <td class="tbcol-2">:</td>
                                    <td class="tbcol-3"></td>
                                </tr>
                                <tr>
                                    <td class="tbcol-1">06. Agama</td>
                                    <td class="tbcol-2">:</td>
                                    <td class="tbcol-3"></td>
                                </tr>
                                <tr>
                                    <td class="tbcol-1">07. Jenis Kelamin</td>
                                    <td class="tbcol-2">:</td>
                                    <td class="tbcol-3"></td>
                                </tr>
                                <tr>
                                    <td class="tbcol-1">08. Pangkat/ Golongan</td>
                                    <td class="tbcol-2">:</td>
                                    <td class="tbcol-3"></td>
                                </tr>
                                <tr>
                                    <td class="tbcol-1">09. Jabatan</td>
                                    <td class="tbcol-2">:</td>
                                    <td class="tbcol-3"></td>
                                </tr>
                                <tr>
                                    <td class="tbcol-1">10. Unit Kerja</td>
                                    <td class="tbcol-2">:</td>
                                    <td class="tbcol-3"></td>
                                </tr>
                                <tr>
                                    <td class="tbcol-1">11. Instansi</td>
                                    <td class="tbcol-2">:</td>
                                    <td class="tbcol-3"></td>
                                </tr>
                                <tr>
                                    <td class="tbcol-1">12. Alamat Kantor</td>
                                    <td class="tbcol-2">:</td>
                                    <td class="tbcol-3"></td>
                                </tr>
                                <tr>
                                    <td class="tbcol-1">13. E-Mail</td>
                                    <td class="tbcol-2">:</td>
                                    <td class="tbcol-3"></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="cp3">
                        <div class="cp100">
                            <h5 class="title-syarat">Persyaratan Berkas :</h5>
                        </div>
                        <div class="cp100">
                            <form>
                                <div class="form-group">
                                    <input type="checkbox" id="SuratPerintahTugas">
                                    <label for="SuratPerintahTugas">Surat Perintah Tugas</label>
                                </div>
                                <div class="form-group">
                                    <input type="checkbox" id="SuratPernyataanBebasTugasKedinasan">
                                    <label for="SuratPernyataanBebasTugasKedinasan">Surat Pernyataan Bebas Tugas Kedinasan</label>
                                </div>
                                <div class="form-group">
                                    <input type="checkbox" id="SuratKeputusanCPNS">
                                    <label for="SuratKeputusanCPNS">Surat Keputusan CPNS</label>
                                </div>
                                <div class="form-group">
                                    <input type="checkbox" id="SuratKeputusanPangkat">
                                    <label for="SuratKeputusanPangkat">Surat Keputusan Pangkat</label>
                                </div>
                                <div class="form-group">
                                    <input type="checkbox" id="SuratKeputusanJabatanTerakhir">
                                    <label for="SuratKeputusanJabatanTerakhir">Surat Keputusan Jabatan Terakhir</label>
                                </div>
                                <div class="form-group">
                                    <input type="checkbox" id="Ijazah">
                                    <label for="Ijazah">Ijazah Terakhir</label>
                                </div>
                                <div class="form-group">
                                    <input type="checkbox" id="SuratDokter">
                                    <label for="SuratDokter">Surat Keterangan Dokter dari Rumah Sakit Pemerintah</label>
                                </div>
                                <div class="form-group">
                                    <input type="checkbox" id="Askes">
                                    <label for="Askes">Askes, BPJS atau KIS</label>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="cp4">
                        <div class="cp1-1">.</div>
                        <div class="cp1-2">
                            <h5 class="tgl-cetak">Bandar Lampung, 12 Juni 2019</h5>
                            <h5 class="caption-peserta">PESERTA,</h5>
                            <h5 class="caption-nama">Nama Lengkap</h5>
                            <h5 class="caption-nip">NIP. 12345678</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
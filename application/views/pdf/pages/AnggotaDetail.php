<table class="table-detail">
    <tr>
        <th style="width: 100px"><?= $this->string('NAME') ?></th>
        <td style="width: 430px">: <?= $nama ?></td>
    </tr>
    <tr>
        <th><?= $this->string('EMAIL') ?></th>
        <td>: <?= $email ?></td>
    </tr>
    <tr>
        <th><?= $this->string('GENDER') ?></th>
        <td>: <?= $jenisKelamin ?></td>
    </tr>
    <tr>
        <th><?= $this->string('DATE_OF_BIRTH') ?></th>
        <td>: <?= $tanggalLahir ?></td>
    </tr>
    <tr>
        <th><?= $this->string('MOBILE_NUMBER') ?></th>
        <td>: <?= $telpHp ?></td>
    </tr>
    <tr>
        <th><?= $this->string('ADDRESS') ?></th>
        <td>: <?= $alamat ?></td>
    </tr>
    
</table>
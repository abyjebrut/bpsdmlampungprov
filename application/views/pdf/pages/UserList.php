<table class="table-list">
    <tr class="thead">
        <th class="num"><?= $this->string('NUM') ?></th>
        <th style="width: 200px"><?= $this->string('NAME') ?></th>
        <th style="width: 150px"><?= $this->string('EMAIL') ?></th>
        <th style="width: 150px"><?= $this->string('MOBILE_NUMBER') ?></th>
    </tr>

    <?php 
    $num = 0;
    foreach($rs as $row): 
        $num++;
    ?>
    <tr>
        <td><?= $num ?>.</td>
        <td><?= $row['name'] ?></td>
        <td><?= $row['email'] ?></td>
        <td><?= $row['mobileNumber'] ?></td>
    </tr>
    <?php endforeach; ?>
</table>
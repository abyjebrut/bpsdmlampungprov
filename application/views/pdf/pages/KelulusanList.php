<table>
	<?php if ($rowKop): ?>
    <tr>
        <td style="width: 150px;">
            <?= $this->string('NAMA_PROGRAM') ?>
        </td>
        <td style="width: 400px;">
            : <?= $rowKop['namaDiklat'] ?> 
        </td>
    </tr>

    <tr>
        <td style="width: 150px;">
            <?= $this->string('WAKTU_PELAKSANAAN') ?> :
        </td>
        <td style="width: 200px;">
            :  <?= $rowKop['pelaksanaanMulai'] ?> - <?= $rowKop['pelaksanaanSelesai'] ?> 
        </td>
    </tr>
    <?php endif; ?>
</table>
<br>
<br>

<table class="table-list">
    <tr class="thead">
        <th class="num"><?= $this->string('NUM') ?></th>
        <th style="width: 100px"><?= $this->string('NIP') ?></th>
        <th style="width: 200px"><?= $this->string('NAMA') ?></th>
        <th style="width: 200px"><?= $this->string('NO_REGISTRASI') ?></th>
    </tr>

    <?php
    $num = 0;
    foreach($rs as $row):
        $num++;
    ?>
    <tr>
        <td><?= $num ?>.</td>
        <td><?= $row['nip'] ?></td>
        <td><?= $row['nama'] ?></td>
        <td><?= $row['noRegistrasi'] ?></td>
    </tr>
    <?php endforeach; ?>
</table>

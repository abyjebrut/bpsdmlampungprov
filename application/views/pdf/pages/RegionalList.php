<table class="table-list">
    <tr class="thead">
        <th class="num"><?= $this->string('NUM') ?></th>
        <th style="width: 500px"><?= $this->string('REGIONAL') ?></th>
    </tr>

    <?php 
    $num = 0;
    foreach($rs as $row): 
        $num++;
    ?>
    <tr>
        <td><?= $num ?>.</td>
        <td><?= $row['regional'] ?></td>
    </tr>
    <?php endforeach; ?>
</table>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <style>
        .table-list {
            padding: 4px;
        }
        
        .table-list .thead {
            background-color: #F5F5F5;
            font-weight: bold;
        }
        
        .table-list th, .table-list td {
            border: solid black 1px;
            margin: 4px;
        }
        
        .table-list .num {
            width: 30px;
        }
        
        .table-detail {
            padding: 4px;
        }
        
        .table-detail th {
            font-weight: bold;
        }
        
        .table-detail td {
        }
    </style>
</head>
<body>
    
    <h1><?= $this->string($title) ?></h1>
    <div>
        <?php $this->view($page, $viewData) ?>
    </div>

</body>
</html>
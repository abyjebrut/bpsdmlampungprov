<?php
defined('BASEPATH') OR exit('No direct script access allowed');
extract($settings);

?><!DOCTYPE html>
<html lang="en">

<head>
  <title><?= $title ?> | <?= $subTitle ?></title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes">

  <meta name="author" content="BPSDM Development Team">

  <?php
  if (isset($meta)):
    if (count($meta) > 0):
      ?>
      <meta name="keywords" content="<?= $meta['keywords'] ?>">
      <meta name="description" content="<?= $meta['description'] ?>">
      <?php
    endif;
  endif;
  ?>

  <link rel="icon" href="<?= base_url() ?>asset/diklat/img/favicon.png">
  <link rel="stylesheet" href="<?= base_url() ?>asset/diklat/libraries/bootstrap/css/bootstrap.min.css" type="text/css">
  <link rel="stylesheet" href="<?= base_url() ?>asset/diklat/libraries/font-awesome/css/font-awesome.min.css" type="text/css">
  <link rel="stylesheet" href="<?= base_url() ?>asset/diklat/libraries/bootstrapselect/dist/css/bootstrap-select.css" type="text/css">
  <link rel="stylesheet" href="<?= base_url() ?>asset/diklat/libraries/css/animate.min.css" type="text/css">
  <link rel="stylesheet" href="<?= base_url() ?>asset/diklat/css/style.css" type="text/css">

  <link rel="stylesheet" href="<?= base_url() ?>asset/diklat/libraries/css/fullcalendar.min.css" type="text/css">

  <?php if ($settings['scoopy'] == 'PendaftaranDiklat' || $settings['scoopy'] == 'EditProfile'): ?>
    <link rel="stylesheet" href="<?= base_url() ?>asset/diklat/libraries/datepicker/datepicker.css" type="text/css">
  <?php endif; ?>

  <script src="<?= base_url() ?>asset/diklat/libraries/js/jquery-1.11.3.min.js"></script>
</head>

<body>
  <div id="wrapper">

    <header data-scope="HeadBar">
      <?php $this->load->view('Header', $headData) ?>
    </header>

    <section data-scope="Content">
        <?php $this->load->view($page, $viewData) ?>
    </section>

  </div>

  <footer data-scope="FootBar">
    <?php $this->load->view('Footer', $footData) ?>
  </footer>
  
  <?php $this->load->view('ModalDialog') ?>

  <div class="scroll-up">
    <a href="#wrapper"><i class="fa fa-angle-up"></i></a>
  </div>
  <script src="<?= base_url() ?>asset/diklat/libraries/bootstrap/js/bootstrap.min.js"></script>
  <script src="<?= base_url() ?>asset/diklat/libraries/bootstrapselect/js/bootstrap-select.js"></script>

  <?php if ($settings['scoopy'] == 'InfoAjar' || $settings['scoopy'] == 'PendaftaranDiklat' || $settings['scoopy'] == 'EditProfile'): ?>
  <?php endif; ?>
    <script src="<?= base_url() ?>asset/diklat/libraries/datepicker/bootstrap-datepicker.js"></script>

  <?php if ($settings['scoopy'] == 'ContactUs'): ?>
    <!-- <script src="https://maps.googleapis.com/maps/api/js"></script> -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCIoLGyS9ZgcKJNdCEHoi_fA483R-oQv-Y&callback"></script>
    <!-- <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBBtF5qkjY_dqGUYM8xkY6CVOdZ-RkHmmg&callback"></script> -->
  <?php endif; ?>

  <script src="<?= base_url() ?>asset/diklat/libraries/js/wow.min.js"></script>
  <script src="<?= base_url() ?>asset/diklat/libraries/js/jquery.touchSwipe.min.js"></script>
  <script src="<?= base_url() ?>asset/diklat/libraries/jquery-mobile-custom/jquery.mobile.custom.min.js"></script>
  <script src="<?= base_url() ?>asset/diklat/js/main.js"></script>
  
  <!-- <script src="<?= base_url() ?>asset/diklat/libraries/js/2jquery.min.js"></script> -->
  <script src="<?= base_url() ?>asset/diklat/libraries/js/moment.min.js"></script>
  <script src="<?= base_url() ?>asset/diklat/libraries/js/fullcalendar.min.js"></script>
    
  <?php if (isset($vars)): ?>
    <script type="text/javascript">
      <?php foreach ($vars as $key => $value): ?>
        var <?= $key ?> = '<?= $value ?>';
      <?php endforeach; ?>
    </script>
  <?php endif; ?>

  <script>
  var CI_BASE_URL = '<?= base_url() ?>';
  var baseUrl = '<?= base_url() ?>scoopy';
  </script>
  <script src="<?= base_url() ?>scoopy/scoopy.js" data-controller="<?= $scoopy ?>"></script>

  <div class="loading-overlay">
        <span class="spinner" role="status" aria-hidden="true"></span>
    </div>
    <script>
        
    </script>

<!-- <script type="text/javascript">
    /* Your existing code + store entry in LocalStorage */
    setTimeout(function() {
        $('#data-popup-box-container').fadeOut('fast');

        // Add entry in localstorage that Popup displayed once :)
        localStorage.setItem("popupDisplayed", "yes");
    }, 1000);   

    /* On reload check if localstorage value is yes :) */
    $(document).ready(function() {
        var popupDisplayed = localStorage.getItem("popupDisplayed");
        /* If local storage value is yes - remove element directly from dom */
        if(popupDisplayed && popupDisplayed == 'yes') {
           $('#data-popup-box-container').remove();
        }
    });

</script> -->
    
</body>

</html>

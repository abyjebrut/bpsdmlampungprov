<div data-scope="GuestBook" class="col-md-12 hidden-xs reset-col widget-sidebar">
  <div class="col-md-12 reset-col contact-sidebar">
    <h2>CONTACT US</h2>
    <div class="form-group form-contact">
      <input data-ref="name" class="form-control" type="text" placeholder="Nama">
      <input data-ref="email" class="form-control" type="text" placeholder="Email">
      <input data-ref="subject" class="form-control" type="text" placeholder="Judul">
      <textarea data-ref="message" class="form-control" rows="9" placeholder="Pesan"></textarea>
      <button data-ref="btnSend" type="button" class="btn btn-default btn-sm btn-sidebar">
        KIRIM <i class="wow fadeIn fa fa-paper-plane"></i>
      </button>
    </div>
  </div>
</div>

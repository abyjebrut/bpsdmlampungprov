<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'SitePages';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

// API Router

$route['api/System/(:any)'] = 'ApiSystem/$1';
$route['api/(:any)/(:any)'] = 'Api/post/$1/$2';

// End API Router

$route['page/(:num)'] = 'SitePages/pageDetail/$1';
$route['page/(:num)/(:num)'] = 'SitePages/subPageDetail/$1/$2';

$route['widyaiswara'] = 'SiteWidyaiswara/index';
$route['widyaiswara/(:num)'] = 'SiteWidyaiswara/page/$1';

$route['rekapitulasi_jp'] = 'SiteRekapitulasiJp/index';
$route['rekapitulasi_peserta'] = 'SiteRekapitulasiPeserta/index';
$route['kegiatan_diklat'] = 'SiteKegiatanDiklat/index';
$route['info_ajar'] = 'SiteInfoAjar/index';
$route['info_peserta'] = 'SiteInfoPeserta/index';
$route['statistik_alumni'] = 'SiteStatistikAlumni/index';
$route['bahan_ajar'] = 'SiteBahanAjar/index';
$route['sttpl_alumni'] = 'SiteSttplAlumni/index';
$route['sttpl_alumni/detail_alumni/(:num)'] = 'SiteSttplAlumni/detailAlumni/$1';

$route['agenda'] = 'SiteAgenda/index';
$route['agenda/detail_alumni/(:num)'] = 'SiteAgenda/detailAlumni/$1';

$route['soal_akd'] = 'SiteSoalAkd/index';

$route['faq'] = 'SiteFaq/index';
$route['faq/(:num)'] = 'SiteFaq/page/$1';

$route['contact_us'] = 'SiteContactUs/index';
$route['pendaftaran_diklat'] = 'SitePendaftaranDiklat/daftarDiklat';
$route['pendaftaran_diklat/(:num)'] = 'SitePendaftaranDiklat/daftarDiklat/$1';
$route['user_profile'] = 'SiteUserProfile/index';
$route['user_profile/edit_profile'] = 'SiteUserProfile/editProfile';

$route['verifikasi/aktifasi/(:any)'] = 'SiteVerifikasi/aktifasi/$1';
$route['verifikasi/berhasil'] = 'SiteVerifikasi/verifikasiBerhasil';
$route['verifikasi/reset_password/(:any)'] = 'SiteVerifikasi/resetPassword/$1';
$route['verifikasi/reset_berhasil'] = 'SiteVerifikasi/resetBerhasil';

$route['list_akd'] = 'SiteAkd/pageList';
$route['list_akd/(:num)'] = 'SiteAkd/pageDetail/$1';

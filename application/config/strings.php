<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['strings'] = array(
  'en' => array(

    // Char A

    'ACCOUNT_NOT_ACTIVE' => 'Your Account is still not active',
    'ACCOUNT_SUSPENDED' => 'Your Account is suspended',

    'ACCESS_DENIED' => 'Access is denied',
    'ADDRESS' => 'Address',
    'ADDRESS_REQUIRED' => 'Address required',
    'AUTHOR_REQUIRED' => 'Author required',

    // Char B

    'BANNER_REQUIRED' => 'Banner required',
    'BRANCH_NAME' => 'Branch Name',
    'BRANCH_NAME_REQUIRED' => 'Branch Name required',

    // Char C

    'CATEGORY_REQUIRED' => 'Category required',
    'CATEGORY_IS_REG' => 'Category already registered',
    'CITY_REQUIRED' => 'City required',
    'CITY_IS_REG' => 'City already registered',
    'COMMENT_REQUIRED' => 'Comment required',

    // Char D

    'DATA_CREATED' => 'Data created',
    'DATA_READ' => 'Data read',
    'DATA_UPDATED' => 'Data updated',
    'DATA_DELETED' => 'Data deleted',
    'DATA_NOT_FOUND' => 'Data not found',
    'DATE_OF_BIRTH' => 'Date Of Birth',
    'DISTRICT_CITY_DETAIL' => 'District/City Detail',
    'DISTRICT_CITY_IS_REG' => 'District/City already registered',
    'DISTRICT_CITY_LIST' => 'District/City List',
    'DISTRICT_CITY' => 'District/City',
    'DISTRICT_CITY_REQUIRED' => 'District/City required',
    'DESCRIPTION_REQUIRED' => 'Description required',
    'DESCRIPTION_IS_REG' => 'Description already registered',

    // Char E

    'EMAIL' => 'Email',
    'EMAIL_REQUIRED' => 'Email required',
    'EMAIL_WRONG' => 'Wrong Email',
    'EMAIL_NOT_REG' => 'Email not registered',
    'EMAIL_IS_REG' => 'Email already registered',

    // Char F

    'FILE_CREATED' => 'File created',
    'FILE_DELETED' => 'File deleted',
    'FILE_UPLOADED' => 'File uploaded',
    'FILE_TYPE_DENIED' => 'File type is denied',
    'FILE_UPLOAD_FAILED' => 'Upload file failed',
    'FILE_CREATE_FAILED' => 'Create file failed',
    'FIRST_NAME_REQUIRED' => 'First Name required',

    // Char G

    'GENDER' => 'Gender',

    // Char I

    'INVALID_NAME' => 'Invalid Name format',
    'INVALID_EMAIL' => 'Invalid Email format',
    'INVALID_PASSWORD' => 'Invalid Password',
    'INVALID_OLD_PASSWORD' => 'Invalid Old Password',
    'ID_REQUIRED' => 'ID required',
    'IMAGE_REQUIRED' => 'Image required',

  	// Char J

  	'JENIS_DIKLAT_REQUIRED' => 'Jenis Diklat required',
  	'JENIS_DIKLAT_IS_REG' => 'Jenis Diklat already registered',
  		'JAWABAN_REQUIRED' => 'Jawaban required',

    // Char L

    'LOGIN_SUCCESS' => 'Login success',
    'LOGOUT_SUCCESS' => 'Logout success',
    'LOGIN_FAILED' => 'Login failed',

    // Char M

    'MOBILE_NUMBER' => 'Mobile Number',
    'MOBILE_NUMBER_REQUIRED' => 'Phone Number required',
    'MENU_CAPTION_REQUIRED' => 'Menu Caption required',
    'MENU_CAPTION_IS_REG' => 'Menu Caption already registered',
    'MESSAGE_REQUIRED' => 'Message required',

    // Char N

    'NAME' => 'Name',
    'NAME_REQUIRED' => 'Name required',
    'NEW_PASSWORD_REQUIRED' => 'New Password required',
    'NUM' => 'No.',
    'NIP_REQUIRED' => 'NIP required',
    'NIP_WRONG' => 'Wrong NIP',
  		'NOTIF_SEND_EMAIL' => 'Notip send email',
  		
    // Char O

    'OLD_PASSWORD_REQUIRED' => 'Old Password required',
    'OLD_PASSWORD_INVALID' => 'Old Password invalid',

    // Char P

    'PASSWORD_CHANGED' => 'Password changed',
    'PASSWORD_NOT_MATCH' => 'Retype Password not match',
    'PASSWORD_THAN_SIX' => 'Password must be more than six character',
    'PAGES_REQUIRED' => 'Pages required',
    'PRIVILEGES_SAVED' => 'Privileges saved',
    'PLEASE_CORRECT' => 'Please correct following data',
    'PHOTO_REQUIRED' => 'Photo required',
    'PHONE_NUMBER_REQUIRED' => 'Phone Number required',
    'PAYMENT_REQUIRED' => 'Payment required',
    'PUBLISH_STATUS_REQUIRED' => 'Publish Status required',
    'PASSWORD_REQUIRED' => 'Password required',
    'PRIVILEGES_REQUIRED' => 'Privileges required',
    'PRODUCT_NAME_REQUIRED' => 'Product Name required',
    'PRODUCT_NAME_IS_REG' => 'Product Name already registered',
  		'PASSWORD_SEND_TO_MEMBER_EMAIL' => 'Password send to member email',

    // Char Q

    'QUESTION_UNCOMPLETED' => 'Question Uncompleted',

    // Char R

    'REGIONAL' => 'Regional',
    'REGIONAL_LIST' => 'Regional List',
    'REGIONAL_REQUIRED' => 'Regional required',
    'REGIONAL_IS_REG' => 'Regional already registered',
    'RETYPE_PASSWORD_REQUIRED' => 'Retype Password required',
    'RUNNING_TEXT_REQUIRED' => 'Running Text required',

    // Char S

    'SUB_TITLE_REQUIRED' => 'Sub Title required',
    'SUBJECT_REQUIRED' => 'Subject required',
    'SEND_EMAIL' => 'Reset Password',

    // Char T

    'TITLE_REQUIRED' => 'Title required',
    'TITLE_IS_REG' => 'Title already registered',

    // Char U

    'USER_DETAIL' => 'User Detail',
    'USER_LIST' => 'User List',
    'USER_NOT_FOUND' => 'User not found',
    'ULANGI_NIP_REQUIRED' => 'Retype NIP required',
    'ULANGI_NIP_INVALID' => 'Retype NIP invalid',


    ),

  'id' => array(
    // Char A

    'ACCOUNT_NOT_ACTIVE' => 'Akun Anda belum diaktifkan, silakan cek email Anda, untuk mengaktifkan ikuti instruksi di dalam email tersebut. Jika Anda tidak mendapatkan email dari kami silakan hubungi Administrator Diklatlampung.info',
    'ACCOUNT_SUSPENDED' => 'Akun Anda telah di suspend oleh Administrator, silakan hubungi Administrator untuk mengaktifkan kembali',

    'ACCESS_DENIED' => 'Akses tidak diizinkan',
    'ADDRESS' => 'Alamat',
  	'ALAMAT' => 'Alamat',
  	'ALAMAT_REQUIRED' => 'Alamat belum diisi',
  	'AGAMA' => 'Agama',
  	'AGAMA_REQUIRED' => 'Agama belum diisi',
    'ADDRESS_REQUIRED' => 'Alamat belum diisi',
    'AUTHOR_REQUIRED' => 'Penulis belum diisi',

    // Char B

    'BANNER_REQUIRED' => 'Banner belum diisi',
    'BRANCH_NAME' => 'Nama Cabang',
    'BRANCH_NAME_REQUIRED' => 'Nama Cabang belum diisi',

    // Char C

    'CATEGORY_REQUIRED' => 'Kategori belum diisi',
    'CATEGORY_IS_REG' => 'Kategori sudah terdaftar sebelumnya',
    'CITY_REQUIRED' => 'Kota belum diisi',
    'CITY_IS_REG' => 'Kota sudah terdaftar sebelumnya',
    'COMMENT_REQUIRED' => 'Komentar belum diisi',

    // Char D

    'DATA_CREATED' => 'Data telah dibuat',
    'DATA_READ' => 'Data terbaca',
    'DATA_UPDATED' => 'Data telah diubah',
    'DATA_DELETED' => 'Data telah dihapus',
    'DATA_NOT_FOUND' => 'Data tidak ditemukan',
    'DATE_OF_BIRTH' => 'Tanggal Lahir',
    'DISTRICT_CITY_DETAIL' => 'Detail Kabupaten/Kota',
    'DISTRICT_CITY_IS_REG' => 'Kabupaten/Kota telah terdaftar sebelumnya',
    'DISTRICT_CITY_LIST' => 'List Kabupaten/Kota',
    'DISTRICT_CITY' => 'Kabupaten/Kota',
    'DISTRICT_CITY_REQUIRED' => 'Kabupaten/Kota belum diisi',
    'DESCRIPTION_REQUIRED' => 'Deskripsi belum diisi',
    'DESCRIPTION_IS_REG' => 'Deskripsi sudah terdaftar sebelumnya',

    // Char E

    'EMAIL' => 'Email',
    'EMAIL_REQUIRED' => 'Email belum diisi',
    'EMAIL_WRONG' => 'Email salah',
    'EMAIL_NOT_REG' => 'Email tidah terdaftar',
    'EMAIL_IS_REG' => 'Email sudah terdaftar sebelumnya',

    // Char F

    'FOTO' => 'Foto',
  	'FOTO_REQUIRED' => 'Foto belum diisi',
  	'FILE_CREATED' => 'Berkas telah dibuat',
    'FILE_DELETED' => 'Berkas telah dihapus',
    'FILE_UPLOADED' => 'Berkas telah diunggah',
    'FILE_TYPE_DENIED' => 'Tipe berkas tidak diizinkan',
    'FILE_UPLOAD_FAILED' => 'Gagal mengunggah berkas',
    'FILE_CREATE_FAILED' => 'Gagal membuat berkas',
    'FIRST_NAME_REQUIRED' => 'Nama Depan belum diisi',

    // Char G

    'GENDER' => 'Jenis Kelamin',
  	'GENDER_REQUIRED' => 'Jenis Kelamin belum diisi',

    // Char I

    'INSTANSI' => 'Instansi',
  	'IS_REG' => 'Anda Sudah Pernah Daftar',
  	'INSTANSI_REQUIRED' => 'Instansi belum diisi',
  	'INSTANSI_LAINNYA' => 'Instansi Lainnya',
  	'INSTANSI_LAINNYA_REQUIRED' => 'Instansi Lainnya belum diisi',
  	'INVALID_NAME' => 'Format Nama salah',
    'INVALID_EMAIL' => 'Format Email salah',
    'INVALID_PASSWORD' => 'Kata Sandi salah',
    'INVALID_OLD_PASSWORD' => 'Kata Sandi Lama salah',
  	'INSTANSI_REQUIRED' => 'Instansi belum diisi',
  	'INSTANSI_IS_REG' => 'Instansi sudah terdaftar sebelumnya',
    'ID_REQUIRED' => 'ID belum diisi',
    'IMAGE_REQUIRED' => 'Gambar belum diisi',

  	// Char J

  	'JENIS_DIKLAT' => 'Jenis Diklat',
  	'JAWABAN_REQUIRED' => 'Jawaban belum diisi',
  	'JENIS_DIKLAT_REQUIRED' => 'Jenis Diklat belum diisi',
  	'JENIS_DIKLAT_IS_REG' => 'Jenis Diklat sudah terdaftar sebelumnya',
  	'JUDUL_REQUIRED' => 'Judul belum diisi',
  	'JABATAN_ESELON' => 'Jabatan Eselon',
  	'JABATAN_ESELON_REQUIRED' => 'Jabatan Eselon belum diisi',
  	'JABATAN' => 'Jabatan',
  		'JAWABAN_REQUIRED' => 'Jawaban belum diisi',

	// Char K

  	'KELULUSAN_LIST' => 'List Kelulusan',
  	'KODE' => 'Kode',
  	'KODE_PESERTA' => 'Kode Peserta',
  	'KUOTA_FULL' => 'Kuota sudah penuh',
  	'KODE_REQUIRED' => 'Kode belum diisi',
  	'KEDUDUKAN' => 'Kedudukan',
  	'KRITERIA_PENILAIAN' => 'Kriteria Penilaian',

    // Char L

    'LOGIN_SUCCESS' => 'Berhasil masuk',
    'LOGOUT_SUCCESS' => 'Berhasil keluar',
    'LOGIN_FAILED' => 'Gagal masuk',

    // Char M

  	'MATA_DIKLAT_REQUIRED' => 'Mata Diklat belum diisi',
    'MOBILE_NUMBER' => 'No. HP',
    'MOBILE_NUMBER_REQUIRED' => 'No. HP belum diisi',
    'MENU_CAPTION_REQUIRED' => 'Label Menu belum diisi',
    'MENU_CAPTION_IS_REG' => 'Label Menu sudah terdaftar sebelumnya',
    'MESSAGE_REQUIRED' => 'Pesan belum diisi',

    // Char N

  	'NAMA' => 'Nama',
  	'NAMA_DIKLAT' => 'Nama Diklat',
  	'NAMA_PROGRAM' => 'Nama Program',
  	'NIP' => 'NIP',
  	'NAMA_ALUMNI_REQUIRED' => 'Nama Alumni belum diisi',
  	'NAMA_REQUIRED' => 'Nama belum diisi',
  	'NAMA_DIKLAT_REQUIRED' => 'Nama Diklat belum diisi',
    'NAME' => 'Nama',
    'NAME_REQUIRED' => 'Nama belum diisi',
    'NEW_PASSWORD_REQUIRED' => 'Kata Sandi baru belum diisi',
  	'NO_REGISTRASI' => 'No Registrasi',
  	'NO_REGISTRASI_REQUIRED' => 'No Registrasi belum diisi',
    'NUM' => 'No.',
    'NIP_REQUIRED' => 'NIP belum diisi',
    'NIP_WRONG' => 'NIP salah',
  		'NOTIF_SEND_EMAIL' => 'Pemberitahuan telah terkirim keemail anggota',

    // Char O

    'OLD_PASSWORD_REQUIRED' => 'Kata Sandi Lama belum diisi',
    'OLD_PASSWORD_INVALID' => 'Kata Sandi Lama salah',

    // Char P

    'PELAKSANAAN_MULAI' => 'Pelaksanaan Mulai',
  	'PELAKSANAAN_MULAI_REQUIRED' => 'Pelaksanaan Mulai belum diisi',
  	'PELAKSANAAN_SELESAI' => 'Pelaksanaan Selesai',
  	'PELAKSANAAN_SELESAI_REQUIRED' => 'Pelaksanaan Selesai belum diisi',
  	'PANGKAT_GOLONGAN' => 'Pangkat Golongan',
  	'PANGKAT_GOLONGAN_REQUIRED' => 'Pangkat Golongan belum diisi',
  	'PENDIDIKAN_TERAKHIR' => 'Pendidikan Terakhir',
  	'PAGES_REQUIRED' => 'Halaman belum diisi',
    'PASSWORD_CHANGED' => 'Kata Sandi telah diubah',
    'PASSWORD_NOT_MATCH' => 'Pengulangan Kata Sandi tidak sama',
    'PASSWORD_THAN_SIX' => 'Kata Sandi harus lebih dari 6 karakter',
  	'PANGKAT_GOLONGAN_REQUIRED' => 'Pangkat / Golongan belum diisi',
  	'PANGKAT_GOLONGAN_IS_REG' => 'Pangkat / Golongan sudah terdaftar sebelumnya',
  	'PENDAFTARAN_DIKLAT_DETAIL' => 'Pendaftaran Diklat Detail',
  	'PENDIDIKAN_TERAKHIR_REQUIRED' => 'Pendidikan Terakhir belum diisi',
  	'PENDIDIKAN_TERAKHIR_IS_REG' => 'Pendidikan Terakhir sudah terdaftar sebelumnya',
  	'PERTANYAAN_REQUIRED' => 'Pertanyaan belum diisi',
    'PRIVILEGES_SAVED' => 'Hak Akses telah tersimpan',
    'PLEASE_CORRECT' => 'Silakan perbaiki data berikut;',
    'PHOTO_REQUIRED' => 'Foto belum diisi',
    'PHONE_NUMBER_REQUIRED' => 'No. Telp belum diisi',
    'PAYMENT_REQUIRED' => 'Pembayaran belum diisi',
    'PUBLISH_STATUS_REQUIRED' => 'Status Publikasi belum diisi',
    'PASSWORD_REQUIRED' => 'Kata Sandi belum diisi',
    'PRIVILEGES_REQUIRED' => 'Hak Akses belum diisi',
    'PRODUCT_NAME_REQUIRED' => 'Nama Product belum diisi',
    'PRODUCT_NAME_IS_REG' => 'Nama Product telah terdaftar sebelumnya',
	'RUMUSAN_KOMPETENSI' => 'Rumusan Kompetensi',
  		'PASSWORD_SEND_TO_MEMBER_EMAIL' => 'Password terkirim ke email anggota',

    // Char Q

    'QUESTION_UNCOMPLETED' => 'Lengkapi Dulu Seluruh Jawaban',

    // Char R

    'REGIONAL' => 'Wilayah',
    'REGIONAL_LIST' => 'List Wilayah',
    'REGIONAL_REQUIRED' => 'Wilayah belum diisi',
    'REGIONAL_IS_REG' => 'Wilayah telah terdaftar sebelumnya',
    'RETYPE_PASSWORD_REQUIRED' => 'Pengulangan Kata Sandi belum diisi',
    'RUNNING_TEXT_REQUIRED' => 'Teks berjalan belum diisi',

    // Char S

    'SUB_TITLE_REQUIRED' => 'Sub Judul belum diisi',
    'SUBJECT_REQUIRED' => 'Subyek belum diisi',
    'SEND_EMAIL' => 'Reset Kata Sandi',

    // Char T

    'TELP_HP' => 'Telp Hp',
  	'TELP_HP_REQUIRED' => 'Telp Hp belum diisi',
  	'TEMPAT_LAHIR' => 'Tempat Lahir',
  	'TEMPAT_LAHIR_REQUIRED' => 'Tempat Lahir belum diisi',
  	'TANGGAL_LAHIR_REQUIRED' => 'Tanggal Lahir belum diisi',
  	'TITLE_REQUIRED' => 'Judul belum diisi',
    'TITLE_IS_REG' => 'Judul sudah terdaftar sebelumnya',

    // Char U

    'UNIT_KERJA' => 'Unit Kerja',
  	'UNIT_KERJA_REQUIRED' => 'Unit Kerja belum diisi',
  	'UNIT_KERJA_IS_REG' => 'Unit Kerja sudah terdaftar sebelumnya',
  	'USER_DETAIL' => 'Detail Pengguna',
    'USER_LIST' => 'List Pengguna',
    'USER_NOT_FOUND' => 'Pengguna tidak ditemukan',
    'ULANGI_NIP_REQUIRED' => 'NIP belum diulangi',
    'ULANGI_NIP_INVALID' => 'NIP yg diulangi tidak sama',

  	//	W
  	'WAKTU_PELAKSANAAN' => 'Waktu Pelaksanaan',

    ),
    );

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SocialNetwork extends MY_Model {
    protected $table = 'tb_social_network';
    
    public function detailData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();
        
		$id = 1;
        
        // Query
        
		$rs = $this->find($id);

        if($rs)
            return $this->success('DATA_READ', $rs);
        else
            return $this->failed('DATA_NOT_FOUND');
	}

    private function checkRequest() {

		$error = array();

        return $error;
    }

	public function saveData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();
        
		$error = $this->checkRequest();

        if(count($error) > 0)
			return $this->invalid('PLEASE_CORRECT', $error);

        $id = 1;
		$facebookPage = $this->request('facebookPage');
        $twitterPage = $this->request('twitterPage');
        $googlePlusPage = $this->request('googlePlusPage');
        $instagramPage = $this->request('instagramPage');

		$rs = array(
			'facebookPage' => $facebookPage,
			'twitterPage' => $twitterPage,
			'googlePlusPage' => $googlePlusPage,
			'instagramPage' => $instagramPage,
		);

        $rsFound = $this->find($id);

        if($rsFound) {

            $this->update($id, $rs);
            return $this->success('DATA_UPDATED');
        }
        else {

            $rs['id'] = $id;

            $this->insert($rs);
            return $this->success('DATA_CREATED');
        }

	}

}

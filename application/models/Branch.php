<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Branch extends MY_Model {
    protected $table = 'tb_branch';
    protected $view = 'vi_branch';
    
    public function readData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();

        // order

        $orderBy = $this->request('orderBy');
        $reverse = $this->request('reverse');

        $orderStmt = '';

        if (!empty($orderBy)) {
            $orderStmt = "order by $orderBy";

            if (!empty($reverse)) {
                if ($reverse == 1)
                    $orderStmt .= ' desc';
            }
        }

        // Limit

        $page = $this->request('page');
		$count = $this->request('count');

        $limitStmt = '';

        $rowsCount = $this->getRowsCount();
        $pageCount = 1;

        if (!empty($page) && !empty($count)) {
            $row = ($page * $count) - $count;
            $limitStmt = "limit $row, $count";

            $pageCount = ceil($rowsCount / $count);

            if($pageCount < 1)
                $pageCount = 1;
        }

        // query

		$rs = $this->select($orderStmt.' '.$limitStmt);
        $extra = array(
            'rowsCount' => $rowsCount,
            'pageCount' => $pageCount,
        );

		return $this->success('DATA_READ', $rs, $extra);
	}

    public function filterData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();
        
        // query

        $field = $this->request('field');
        $keyword = $this->request('keyword');

        $queryStmt = "where $field like ?";
        $rowsCount = $this->getRowsCount($queryStmt, array("$keyword%"));

        // Limit

        $page = $this->request('page');
		$count = $this->request('count');

        $limitStmt = '';
        $pageCount = 1;

        if (!empty($page) && !empty($count)) {
            $row = ($page * $count) - $count;
            $limitStmt = "limit $row, $count";

            $pageCount = ceil($rowsCount / $count);

            if($pageCount < 1)
                $pageCount = 1;
        }

        $rs = $this->select($queryStmt, array("$keyword%"));

        $extra = array(
            'rowsCount' => $rowsCount,
            'pageCount' => $pageCount,
        );

		return $this->success('DATA_READ', $rs, $extra);
	}

	public function detailData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();

		$id = $this->request('id');

        // Query

		$rs = $this->find($id);

        if($rs)
            return $this->success('DATA_READ', $rs);
        else
            return $this->failed('DATA_NOT_FOUND');
	}

    private function checkRequest() {
        $branchName = $this->request('branchName');
        $districtCityId = $this->request('districtCityId');

		$error = array();

		if(empty($branchName))
			$error['branchName'] = $this->string('BRANCH_NAME_REQUIRED');

		if(empty($districtCityId))
			$error['districtCityId'] = $this->string('DISTRICT_CITY_REQUIRED');

        return $error;
    }

	public function createData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

		$error = $this->checkRequest();

        $branchName = $this->request('branchName');
        $districtCityId = $this->request('districtCityId');
        $address = $this->request('address');
        $latitude = $this->request('latitude');
        $longitude = $this->request('longitude');
        $phoneNumber = $this->request('phoneNumber');
        $faxNumber = $this->request('faxNumber');
        $email = $this->request('email');
        $facebookPage = $this->request('facebookPage');
        $twitterPage = $this->request('twitterPage');
        $mainBranchId = $this->request('mainBranchId');

        $rs = $this->find('branchName', $branchName);

		if($rs)
            $error['branchName'] = $this->string('BRANCH_NAME_IS_REG');

		if(count($error) > 0)
			return $this->invalid('PLEASE_CORRECT', $error);

        if ($mainBranchId == 1)
            $this->update('mainBranchId', 1, array('mainBranchId' => 2));

		$rs = array(
            'branchName' => $branchName,
			'districtCityId' => $districtCityId,
            'address' => $address,
            'latitude' => $latitude,
            'longitude' => $longitude,
            'phoneNumber' => $phoneNumber,
            'faxNumber' => $faxNumber,
            'email' => $email,
            'facebookPage' => $facebookPage,
            'twitterPage' => $twitterPage,
            'mainBranchId' => $mainBranchId,
        );

		$this->insert($rs);

        // position
        $lastId = $this->getLastInsertId();
        $this->update($lastId, array('position' => $lastId));

		return $this->success('DATA_CREATED');
	}

	public function updateData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

        $id = $this->request('id');
        $branchName = $this->request('branchName');
        $districtCityId = $this->request('districtCityId');
        $address = $this->request('address');
        $latitude = $this->request('latitude');
        $longitude = $this->request('longitude');
        $phoneNumber = $this->request('phoneNumber');
        $faxNumber = $this->request('faxNumber');
        $email = $this->request('email');
        $facebookPage = $this->request('facebookPage');
        $twitterPage = $this->request('twitterPage');
        $mainBranchId = $this->request('mainBranchId');

		$error = $this->checkRequest();

        $rs = $this->find($id);
        if(!$rs)
            return $this->failed('DATA_NOT_FOUND');

        $last_branchName = $rs['branchName'];

        if($branchName != $last_branchName) {
            $rs = $this->find('branchName', $branchName);

            if($rs)
                $error['branchName'] = $this->string('BRANCH_NAME_IS_REG');
        }

		if(count($error) > 0)
			return $this->invalid('PLEASE_CORRECT', $error);

		if ($mainBranchId == 1)
            $this->update('mainBranchId', 1, array('mainBranchId' => 2));

		$rs = array(
            'branchName' => $branchName,
			'districtCityId' => $districtCityId,
            'address' => $address,
            'latitude' => $latitude,
            'longitude' => $longitude,
            'phoneNumber' => $phoneNumber,
            'faxNumber' => $faxNumber,
            'email' => $email,
            'facebookPage' => $facebookPage,
            'twitterPage' => $twitterPage,
            'mainBranchId' => $mainBranchId,
        );

		$this->update($id, $rs);
		return $this->success('DATA_UPDATED');
	}

    public function moveData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

		$idFrom = $this->request('idFrom');
		$idTo = $this->request('idTo');

        $rsFrom = $this->find($idFrom);
        $rsTo = $this->find($idTo);

        $positionFrom = $rsFrom['position'];
        $positionTo = $rsTo['position'];

		$this->update($idFrom, array('position' => $positionTo));
        $this->update($idTo, array('position' => $positionFrom));

		return $this->success('DATA_UPDATED');
	}

	public function deleteData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

		$id = $this->request('id');
        $rs = $this->find($id);

		$this->delete($id);
		return $this->success('DATA_DELETED');
	}

	public function multipleDeleteData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

		$multipleId = $this->request('multipleId');

		foreach($multipleId as $id)
            $this->delete($id);

		return $this->success('DATA_DELETED');
	}
    
}
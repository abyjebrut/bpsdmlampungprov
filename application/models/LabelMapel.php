<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LabelMapel extends MY_Model {
    protected $table = 'tb_label_mapel';
    
    public function readData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();

        // Order

        $orderBy = $this->request('orderBy');
        $reverse = $this->request('reverse');

        $orderStmt = '';

        if (!empty($orderBy)) {
            $orderStmt = "order by $orderBy";

            if (!empty($reverse)) {
                if ($reverse == 1)
                    $orderStmt .= ' desc';
            }
        }

        // Limit
        $page = $this->request('page');
		$count = $this->request('count');

        $limitStmt = '';

      	$rowsCount = $this->getRowsCount($orderStmt);
        
      	$pageCount = 1;

        if (!empty($page) && !empty($count)) {
            $row = ($page * $count) - $count;
            $limitStmt = "limit $row, $count";

            $pageCount = ceil($rowsCount / $count);

            if($pageCount < 1)
                $pageCount = 1;
        }

        // Query

	      $rs = $this->select($orderStmt.' '.$limitStmt);
    
        $extra = array(
            'rowsCount' => $rowsCount,
            'pageCount' => $pageCount,
        );

		return $this->success('DATA_READ', $rs, $extra);
	}

    public function filterData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();
        
        // Query

        $field = $this->request('field');
        $keyword = $this->request('keyword');

        $queryStmt = "where $field like ?";
        $rowsCount = $this->getRowsCount($queryStmt, array("$keyword%"));

        // Limit

        $page = $this->request('page');
		$count = $this->request('count');

        $limitStmt = '';
        $pageCount = 1;

        if (!empty($page) && !empty($count)) {
            $row = ($page * $count) - $count;
            $limitStmt = "limit $row, $count";

            $pageCount = ceil($rowsCount / $count);

            if($pageCount < 1)
                $pageCount = 1;
        }

        $rs = $this->select($queryStmt, array("$keyword%"));

        $extra = array(
            'rowsCount' => $rowsCount,
            'pageCount' => $pageCount,
        );

		return $this->success('DATA_READ', $rs, $extra);
	}

	public function detailData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();

		$id = $this->request('id');
        
        // Query
        
		$rs = $this->find($id);

        if($rs)
            return $this->success('DATA_READ', $rs);
        else
            return $this->failed('DATA_NOT_FOUND');
	}

    private function checkRequest() {
        $labelMapel = $this->request('labelMapel');

		$error = array();
		
		if(empty($labelMapel))
			$error['labelMapel'] = $this->string('Judul Mata Pelajaran diperlukan');

        return $error;
    }

	public function createData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

		$error = $this->checkRequest();

		$labelMapel = $this->request('labelMapel');

        $rs = $this->find('labelMapel', $labelMapel);

		if($rs)
            $error['labelMapel'] = $this->string('Judul Mata Pelajaran sudah tersedia');

		if(count($error) > 0)
			return $this->invalid('PLEASE_CORRECT', $error);

		$rs = array(
            'labelMapel' => $labelMapel,
        );

		$this->insert($rs);
		return $this->success('DATA_CREATED');
	}

	public function updateData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

        $id = $this->request('id');
        $labelMapel = $this->request('labelMapel');
		
        $error = $this->checkRequest();

        $rs = $this->find($id);
        
        if(!$rs)
            return $this->failed('DATA_NOT_FOUND');

        $last_labelMapel = $rs['labelMapel'];

        if($labelMapel != $last_labelMapel) {
            $rs = $this->find('labelMapel', $labelMapel);

            if($rs)
                $error['labelMapel'] = $this->string('Judul Mata Pelajaran sudah tersedia');
        }

		if(count($error) > 0)
			return $this->invalid('PLEASE_CORRECT', $error);
		
		$rs = array(
            'labelMapel' => $labelMapel,
		);

		$this->update($id, $rs);
		return $this->success('DATA_UPDATED');
	}
	
	public function deleteData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

		$id = $this->request('id');
        $rs = $this->find($id);

		$this->delete($id);
		return $this->success('DATA_DELETED');
	}

	public function multipleDeleteData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

		$multipleId = $this->request('multipleId');
        $mItems = $this->model('tb_mata_pelajaran');
        $error = array();
        $i = 0;

		foreach($multipleId as $id){
            $rs = $this->find($id);
            $i++;

            $rsItems = $mItems->select("where labelMapelId = ?", array($id));
            if($rsItems){
                if(count($error) == 0)
                  $error['0'] = 'Data dibawah ini tidak bisa di hapus karena terkait dengan Data lain';
              
              $rs = $this->find($id);
              $error[$i] = $rs['labelMapel'];        
            }

            if(!$rsItems ){
                $this->delete($id);
            }
        }

        if(count($error) > 0){
            return $this->failed('PLEASE_CORRECT', $error);
            exit;
          }else{
            return $this->success('DATA_DELETED');
          }
	}
    
    
}
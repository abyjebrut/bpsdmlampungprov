<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Language extends MY_Model {
  protected $table = 'tb_language';
  protected $view = 'vi_language';

  public function readData($params = array())
  {
    if(!$this->setSettings(array(
      'authen' => 'free',
      'requestSource' => $params,
  )))
      return $this->denied();

    // Query

    $row = $this->single();
    $rs[] = array(
      'id' => $row['primaryCode'],
      'language' => $row['primaryLanguage'],
      'flag' => $row['primaryFlag'],
    );

    if ($row['secondaryEnabledId'] == 1)
      $rs[] = array(
        'id' => $row['secondaryCode'],
        'language' => $row['secondaryLanguage'],
        'flag' => $row['secondaryFlag'],
      );

    if ($row['extendedEnabledId'] == 1)
      $rs[] = array(
        'id' => $row['extendedCode'],
        'language' => $row['extendedLanguage'],
        'flag' => $row['extendedFlag'],
      );

    return $this->success('DATA_READ', $rs);
  }

  public function detailData($params = array())
  {
    if(!$this->setSettings(array(
      'authen' => 'free',
      'requestSource' => $params,
    )))
      return $this->denied();

    $id = 1;

    // Query

  	$row = $this->find($id);

    return $this->success('DATA_READ', $row);
  }

}

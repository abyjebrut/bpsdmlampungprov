<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class TipeWaktu extends MY_Model {
    protected $table = 'tb_tipe_waktu';
    
    public function readData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();
        
        // Order By
        
        $orderBy = $this->input->post('orderBy');
        $reverse = $this->input->post('reverse');
        
        $orderStmt = '';
        
        if (!empty($orderBy)) {
            $orderStmt = "order by $orderBy";

            if (!empty($reverse)) {
                if ($reverse == 1)
                    $orderStmt .= ' desc';
            }
        }

        // Query

		$rs = $this->select($orderStmt);
        $rsCount = count($rs);

		return $this->success('DATA_READ', $rs, $rsCount);
    }

}
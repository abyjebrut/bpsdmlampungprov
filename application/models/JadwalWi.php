<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class JadwalWi extends MY_Model {
    protected $view = 'vi_bahan_jadwal_diklat_pengajar_detail';
    
    public function readData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();

        // Order

        $orderBy = $this->request('orderBy');
        $reverse = $this->request('reverse');

        $orderStmt = '';

        if (!empty($orderBy)) {
            $orderStmt = "order by $orderBy";

            if (!empty($reverse)) {
                if ($reverse == 1)
                    $orderStmt .= ' desc';
            }
        }

        // Limit
        $page = $this->request('page');
		$count = $this->request('count');
        $pengajarDiklatId = $this->request('pengajarDiklatId');
        $jenisDiklatId = $this->request('jenisDiklatId');
        $tanggalMulai = $this->request('tanggalMulai');
        $tanggalSelesai = $this->request('tanggalSelesai');

        $limitStmt = '';
     
        if (!empty($jenisDiklatId))
	      $rowsCount = $this->getRowsCount("where pengajarDiklatId = ? and jenisDiklatId = ? and tanggal between ? and ? ", array($pengajarDiklatId,$jenisDiklatId,$tanggalMulai,$tanggalSelesai));
	    else
        $rowsCount = $this->getRowsCount("where pengajarDiklatId = ?  and tanggal between ? and ? ", array($pengajarDiklatId,$tanggalMulai,$tanggalSelesai));
        
      	$pageCount = 1;

        if (!empty($page) && !empty($count)) {
            $row = ($page * $count) - $count;
            $limitStmt = "limit $row, $count";

            $pageCount = ceil($rowsCount / $count);

            if($pageCount < 1)
                $pageCount = 1;
        }

        // Query
        
        
        $mItems = $this->model('vi_bahan_jadwal_diklat');
        if (!empty($jenisDiklatId)){
			$rs = $this->select("where pengajarDiklatId = ? and jenisDiklatId = ? and tanggal between ? and ?  ".$orderStmt.' '.$limitStmt, array($pengajarDiklatId,$jenisDiklatId,$tanggalMulai,$tanggalSelesai));
            $rsJadwal = $mItems->select("where  jenisDiklatId = ? and tanggal between ? and ?  ".$orderStmt.' '.$limitStmt, array($jenisDiklatId,$tanggalMulai,$tanggalSelesai));
        }else{
			$rs = $this->select("where pengajarDiklatId = ?  and tanggal between ? and ?  ".$orderStmt.' '.$limitStmt, array($pengajarDiklatId,$tanggalMulai,$tanggalSelesai));
            $rsJadwal = $mItems->select("where  tanggal between ? and ?  ".$orderStmt.' '.$limitStmt, array($tanggalMulai,$tanggalSelesai));
        }
			
        $extra = array(
            'rowsCount' => $rowsCount,
            'pageCount' => $pageCount,
            'rsJadwal' => $rsJadwal,
        );

		return $this->success('DATA_READ', $rs, $extra);
    }

    
    public function createExcel($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();
       
        // Excel Settings
        
        $this->load->library('excel');

        $pengajarDiklatId = $this->request('pengajarDiklatId');
        $jenisDiklatId = $this->request('jenisDiklatId');
        $jenisDiklat = $this->request('jenisDiklat');
        $tanggalMulai = $this->request('tanggalMulai');
        $tanggalSelesai = $this->request('tanggalSelesai');
        $mItems = $this->model('vi_bahan_jadwal_diklat');

        if (!empty($jenisDiklatId)){
			$rs = $this->select("where pengajarDiklatId = ? and jenisDiklatId = ? and tanggal between ? and ?  order by  tanggal,jamMulai asc", array($pengajarDiklatId,$jenisDiklatId,$tanggalMulai,$tanggalSelesai));
        }else{
			$rs = $this->select("where pengajarDiklatId = ?  and tanggal between ? and ? order by  tanggal,jamMulai asc", array($pengajarDiklatId,$tanggalMulai,$tanggalSelesai));
        }
            $mUser = $this->model('vi_user');
            $rowPengajar = $mUser->find($pengajarDiklatId);
            $render = 'renderDetailWi';
            $nameFile='JadwalWi';
        // Output
        
        if (count($rs) > 0) {
            $fileUrl = $this->$render(
                $nameFile,
                array(
                    'rs' => $rs,
                    'jenisDiklat' => $jenisDiklat,
                    'tanggalMulai' => $tanggalMulai,
                    'tanggalSelesai' => $tanggalSelesai,
                    'rowPengajar' => $rowPengajar,
                    )
            );
            return $this->success('FILE_CREATED', $fileUrl);
        }
        else
            return $this->failed('FILE_CREATE_FAILED');
    }

    private function renderDetailWi($template, $data) {

        $rs = $data['rs'];
        $tanggalMulai = $data['tanggalMulai'];
        $tanggalSelesai = $data['tanggalSelesai'];
        $jenisDiklat = $data['jenisDiklat'];
        $rowPengajar = $data['rowPengajar'];

        $reader = PHPExcel_IOFactory::createReader('Excel5');
        $templateUrl = "application/views/excel/$template.xls";

        $excel = $reader->load($templateUrl);
        $sheet = $excel->setActiveSheetIndex(0);

        $i = 0;
        $x =4;
        $styleArray = array(
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				)
			)
        );
        $sheet->setCellValue("A2", 'Jadwal Ajar dari Tanggal: '.$this->indonesian_date($tanggalMulai ,"l, j F Y").' s/d '.$this->indonesian_date($tanggalSelesai ,"l, j F Y"));
       if($jenisDiklat){
           $sheet->setCellValue("A3", 'Jenis Diklat: '.$jenisDiklat);
       }
       

       foreach($rs as $row) {
        $x++;
        $i++;
        if ($row['jumlahJp'] == 0)
            $allJam = '-';
        else
            $allJam = $row['jumlahJp'];

        if ($row['tipeWaktuId'] == 1) {
            $allTanggal = $this->indonesian_date1($row['tanggal'] ,"l j F y");
            $allWaktu = $row['jamMulai'].' - '.$row['jamSelesai'];
        }else{
            $allTanggal = $this->indonesian_date1($row['tanggal'] ,"l j F y").' s/d '.$this->indonesian_date1($row['tanggalSelesai'] ,"l j F y");
            $allWaktu = $row['jumlahHari'].' hari';
        }
        $mBahan = $this->model('vi_bahan_jadwal_diklat');
        $rowBahan = $mBahan->find($row['bahanJadwalDiklatId']);

        $sheet->setCellValue("A$x", $i);
        $sheet->setCellValue("B$x", $row['namaDiklat']);
        $sheet->setCellValue("C$x", $allTanggal);
        $sheet->setCellValue("D$x", $allWaktu);
        $sheet->setCellValue("E$x", $row['mataPelajaran']);
        $sheet->setCellValue("F$x", $rowBahan['lokasi']);
        $sheet->setCellValue("G$x", $allJam);
        $sheet->getStyle("A$x")->applyFromArray($styleArray);
        $sheet->getStyle("B$x")->applyFromArray($styleArray);
        $sheet->getStyle("C$x")->applyFromArray($styleArray);
        $sheet->getStyle("D$x")->applyFromArray($styleArray);
        $sheet->getStyle("E$x")->applyFromArray($styleArray);
        $sheet->getStyle("F$x")->applyFromArray($styleArray);
        $sheet->getStyle("G$x")->applyFromArray($styleArray);
    }

        $fileName = $this->encrypt().'.xls';
        $filePath = "asset/tmp/$fileName";

        $writer = PHPExcel_IOFactory::createWriter($excel, 'Excel5');
        $writer->save($filePath);

        $fileUrl = base_url().$filePath;

        return $fileUrl;
    }

    
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class GuestBook extends MY_Model {
    protected $table = 'tb_guest_book';
    protected $view = 'vi_guest_book';

    public function readData($params = array())
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();

        // order

        $orderBy = $this->request('orderBy');
        $reverse = $this->request('reverse');

        $orderStmt = '';

        if (!empty($orderBy)) {
            $orderStmt = "order by $orderBy";

            if (!empty($reverse)) {
                if ($reverse == 1)
                    $orderStmt .= ' desc';
            }
        }

        // Limit

        $page = $this->request('page');
		$count = $this->request('count');

        $limitStmt = '';

        $rowsCount = $this->getRowsCount();
        $pageCount = 1;

        if (!empty($page) && !empty($count)) {
            $row = ($page * $count) - $count;
            $limitStmt = "limit $row, $count";

            $pageCount = ceil($rowsCount / $count);

            if($pageCount < 1)
                $pageCount = 1;
        }

        // query

		$rs = $this->select($orderStmt.' '.$limitStmt);
        $extra = array(
            'rowsCount' => $rowsCount,
            'pageCount' => $pageCount,
        );

		return $this->success('DATA_READ', $rs, $extra);
	}

    public function filterData($params = array())
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();
        // query

        $field = $this->request('field');
        $keyword = $this->request('keyword');

        $queryStmt = "where $field like ?";
        $rowsCount = $this->getRowsCount($queryStmt, array("$keyword%"));

        // Limit

        $page = $this->request('page');
		$count = $this->request('count');

        $limitStmt = '';
        $pageCount = 1;

        if (!empty($page) && !empty($count)) {
            $row = ($page * $count) - $count;
            $limitStmt = "limit $row, $count";

            $pageCount = ceil($rowsCount / $count);

            if($pageCount < 1)
                $pageCount = 1;
        }

        $rs = $this->select($queryStmt, array("$keyword%"));

        $extra = array(
            'rowsCount' => $rowsCount,
            'pageCount' => $pageCount,
        );

		return $this->success('DATA_READ', $rs, $extra);
	}

	public function detailData($params = array())
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();

		$id = $this->request('id');

        // Query

		$rs = $this->find($id);

        if($rs)
            return $this->success('DATA_READ', $rs);
        else
            return $this->failed('DATA_NOT_FOUND');
	}

    private function checkRequest() {
        $name = $this->request('name');
		$email = $this->request('email');
		$subject = $this->request('subject');
		$message = $this->request('message');

		$error = array();

		if(empty($name))
			$error['name'] = $this->string('NAME_REQUIRED');
		else {
            // if(!$this->valid->name($name))
            //     $error['name'] = $this->string('INVALID_NAME');
        }

		if(empty($email))
			$error['email'] = $this->string('EMAIL_REQUIRED');
		else {
            // if(!$this->valid->email($email))
            //     $error['email'] = $this->string('INVALID_EMAIL');
        }
        
        if(empty($subject))
        	$error['subject'] = $this->string('SUBJECT_REQUIRED');
        
        if(empty($message))
        	$error['message'] = $this->string('MESSAGE_REQUIRED');

        return $error;
    }

	public function createData($params = array())
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();

		$error = $this->checkRequest();

        $name = $this->request('name');
		$email = $this->request('email');
        $subject = $this->request('subject');
		$message = $this->request('message');

		if(count($error) > 0)
			return $this->invalid('PLEASE_CORRECT', $error);

		$rs = array(
            'name' => $name,
            'email' => $email,
            'subject' => $subject,
            'message' => $message,
			'postedOn' => date('Y-m-d H:i:s'),
        );

		$this->insert($rs);
		return $this->success('DATA_CREATED');
	}

	public function updateData($params = array())
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

        $id = $this->request('id');
        $name = $this->request('name');
		$email = $this->request('email');
		$subject = $this->request('subject');
		$message = $this->request('message');

		$error = $this->checkRequest();

        $rs = $this->find($id);
        if(!$rs)
            return $this->failed('DATA_NOT_FOUND');

		if(count($error) > 0)
			return $this->invalid('PLEASE_CORRECT', $error);

		$rs = array(
				'name' => $name,
				'email' => $email,
				'subject' => $subject,
				'message' => $message,
		);

		$this->update($id, $rs);
		return $this->success('DATA_UPDATED');
	}

	public function deleteData($params = array())
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

		$id = $this->request('id');

        // Query

        $rs = $this->find($id);

		$this->delete($id);
		return $this->success('DATA_DELETED');
	}

	public function multipleDeleteData($params = array())
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

		$multipleId = $this->request('multipleId');

		foreach($multipleId as $id)
            $this->delete($id);

		return $this->success('DATA_DELETED');
	}
}

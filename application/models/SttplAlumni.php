<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SttplAlumni extends MY_Model {
    protected $table = 'tb_sttpl_alumni';
    
    public function detailData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();
        
		$id = 1;
        
        // Query
        
		$rs = $this->find($id);
		
        if($rs)
            return $this->success('DATA_READ', $rs);
        else
            return $this->failed('DATA_NOT_FOUND');
	}

    private function checkRequest() {
    	$judul = $this->request('judul');
    	
		$error = array();

		if(empty($judul))
			$error['judul'] = $this->string('JUDUL_REQUIRED');
		
        return $error;
    }

	public function saveData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();
        
		$error = $this->checkRequest();

        if(count($error) > 0)
			return $this->invalid('PLEASE_CORRECT', $error);

        $id = 1;
        $judul = $this->request('judul');
        $pengantar = $this->request('pengantar');

		$rs = array(
				'judul' => $judul,
				'pengantar' => $pengantar,
		);

        $rsFound = $this->find($id);

        if($rsFound) {

            $this->update($id, $rs);
            return $this->success('DATA_UPDATED');
        }
        else {
            $rs['id'] = $id;
            $this->insert($rs);
            return $this->success('DATA_CREATED');
        }

	}

}

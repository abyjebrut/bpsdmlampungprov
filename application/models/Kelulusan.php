<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kelulusan extends MY_Model {
    protected $table = 'tb_alumni';
    protected $view = 'vi_alumni';

    public function readData($params = array())
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();

        // Order

        $orderBy = $this->request('orderBy');
        $reverse = $this->request('reverse');

        $orderStmt = '';

        if (!empty($orderBy)) {
            $orderStmt = "order by $orderBy";

            if (!empty($reverse)) {
                if ($reverse == 1)
                    $orderStmt .= ' desc';
            }
        }

        // Limit
        $page = $this->request('page');
		$count = $this->request('count');
		$jenisDiklatId = $this->request('jenisDiklatId');
		$namaDiklat = $this->request('namaDiklat');
		$tahun = $this->request('tahun');
		$bulan = $this->request('bulan');

        $limitStmt = '';

        if (!empty($jenisDiklatId) && !empty($namaDiklat) && !empty($bulan) && !empty($tahun))
	      $rowsCount = $this->getRowsCount("where jenisDiklatId = ? and diklatId = ? and bulan = ? and tahun = ? ", array($jenisDiklatId, $namaDiklat,$bulan,$tahun));
        elseif (!empty($jenisDiklatId) && !empty($namaDiklat))
        	$rowsCount = $this->getRowsCount("where jenisDiklatId = ? and diklatId = ? ", array($jenisDiklatId, $namaDiklat));
        elseif (!empty($jenisDiklatId) && !empty($bulan) && !empty($tahun))
        	$rowsCount = $this->getRowsCount("where jenisDiklatId = ?  and bulan = ? and tahun = ? ", array($jenisDiklatId, $bulan,$tahun));
        elseif (!empty($namaDiklat) && !empty($bulan) && !empty($tahun))
        	$rowsCount = $this->getRowsCount("where diklatId = ? and bulan = ? and tahun = ? ", array($namaDiklat,$bulan,$tahun));
	    elseif (!empty($jenisDiklatId))
	      $rowsCount = $this->getRowsCount("where jenisDiklatId = ? ", array($jenisDiklatId));
	    elseif (!empty($namaDiklat))
	      $rowsCount = $this->getRowsCount("where diklatId = ? ", array($namaDiklat));
	    elseif (!empty($bulan) && !empty($tahun))
	    	$rowsCount = $this->getRowsCount("where bulan = ? and tahun = ? ", array($bulan,$tahun));
	    else
      		$rowsCount = $this->getRowsCount($orderStmt);

      	$pageCount = 1;

        if (!empty($page) && !empty($count)) {
            $row = ($page * $count) - $count;
            $limitStmt = "limit $row, $count";

            $pageCount = ceil($rowsCount / $count);

            if($pageCount < 1)
                $pageCount = 1;
        }

        // Query

        if (!empty($jenisDiklatId) && !empty($namaDiklat) && !empty($bulan) && !empty($tahun))
	      $rs = $this->select("where jenisDiklatId = ? and diklatId = ? and bulan = ? and tahun = ? ".$orderStmt.' '.$limitStmt, array($jenisDiklatId, $namaDiklat , $bulan , $tahun));
        elseif (!empty($jenisDiklatId) && !empty($namaDiklat))
        	$rs = $this->select("where jenisDiklatId = ? and diklatId = ? ".$orderStmt.' '.$limitStmt, array($jenisDiklatId, $namaDiklat));
        elseif (!empty($jenisDiklatId) && !empty($bulan) && !empty($tahun))
	        $rs = $this->select("where jenisDiklatId = ?  and bulan = ? and tahun = ? ".$orderStmt.' '.$limitStmt, array($jenisDiklatId, $bulan , $tahun));
        elseif (!empty($jenisDiklatId) && !empty($tahun))
        	$rs = $this->select("where jenisDiklatId = ?  and tahun = ? ".$orderStmt.' '.$limitStmt, array($jenisDiklatId, $tahun));
        elseif (!empty($namaDiklat) && !empty($bulan) && !empty($tahun))
    	    $rs = $this->select("where diklatId = ? and bulan = ? and tahun = ? ".$orderStmt.' '.$limitStmt, array($namaDiklat , $bulan , $tahun));
        elseif (!empty($namaDiklat) && !empty($tahun))
        	$rs = $this->select("where diklatId = ? and tahun = ? ".$orderStmt.' '.$limitStmt, array($namaDiklat , $tahun));
	    elseif (!empty($jenisDiklatId))
	      $rs = $this->select("where jenisDiklatId = ? ".$orderStmt.' '.$limitStmt, array($jenisDiklatId));
	    elseif (!empty($namaDiklat))
	      $rs = $this->select("where diklatId = ? ".$orderStmt.' '.$limitStmt, array($namaDiklat));
	    elseif (!empty($bulan) && !empty($tahun))
	    	$rs = $this->select("where bulan = ? and tahun = ? ".$orderStmt.' '.$limitStmt, array($bulan , $tahun));
	    elseif (!empty($tahun))
	    	$rs = $this->select("where tahun = ? ".$orderStmt.' '.$limitStmt, array($tahun));
	    else
	      	$rs = $this->select($orderStmt.' '.$limitStmt);

        $extra = array(
            'rowsCount' => $rowsCount,
            'pageCount' => $pageCount,
        );

		return $this->success('DATA_READ', $rs, $extra);
	}

	public function readDataAlumni($params = array())
	{
		if(!$this->setSettings(array(
				'authen' => 'free',
				'requestSource' => $params,
		)))
			return $this->denied();

		// Order

		$orderBy = $this->request('orderBy');
		$reverse = $this->request('reverse');

		$orderStmt = '';

		if (!empty($orderBy)) {
			$orderStmt = "order by $orderBy";

			if (!empty($reverse)) {
				if ($reverse == 1)
					$orderStmt .= ' desc';
			}
		}

		// Limit
		$page = $this->request('page');
		$count = $this->request('count');
		$diklatId = $this->request('diklatId');

		$limitStmt = '';

		if (!empty($diklatId))
			$rowsCount = $this->getRowsCount("where diklatId = ? ", array($diklatId));
		else
			$rowsCount = $this->getRowsCount($orderStmt);

		$pageCount = 1;

		if (!empty($page) && !empty($count)) {
			$row = ($page * $count) - $count;
			$limitStmt = "limit $row, $count";

			$pageCount = ceil($rowsCount / $count);

			if($pageCount < 1)
				$pageCount = 1;
		}

		// Query

		if (!empty($diklatId))
			$rs = $this->select("where diklatId = ? ".$orderStmt.' '.$limitStmt, array($diklatId));
		else
			$rs = $this->select($orderStmt.' '.$limitStmt);

		$extra = array(
				'rowsCount' => $rowsCount,
				'pageCount' => $pageCount,
		);

		return $this->success('DATA_READ', $rs, $extra);
	}

	public function readDataStatistikAlumni($params = array())
	{
		if(!$this->setSettings(array(
				'authen' => 'free',
				'requestSource' => $params,
		)))
			return $this->denied();

		// Order

		$orderBy = $this->request('orderBy');
		$reverse = $this->request('reverse');

		$orderStmt = '';

		if (!empty($orderBy)) {
			$orderStmt = "order by $orderBy";

			if (!empty($reverse)) {
				if ($reverse == 1)
					$orderStmt .= ' desc';
			}
		}

		// Limit
		$page = $this->request('page');
		$count = $this->request('count');

		$limitStmt = '';

			$rowsCount = $this->getRowsCount($orderStmt);

		$pageCount = 1;

		if (!empty($page) && !empty($count)) {
			$row = ($page * $count) - $count;
			$limitStmt = "limit $row, $count";

			$pageCount = ceil($rowsCount / $count);

			if($pageCount < 1)
				$pageCount = 1;
		}

		// Query
		$mItems = $this->model('vi_statistik_alumni');
		$rs = $mItems->select();
		
		$calendar = array();
		foreach ($rs as $key => $val) 
		{
		if ($val['jenisDiklatId'] == 6) {
			$latsar = $val['jumlahOrang'];
		}else{
			$latsar = 0;
		}
		if ($val['jenisDiklatId'] == 10) {
			$prajab = $val['jumlahOrang'];
		}else{
			$prajab =0;
		}
		if ($val['jenisDiklatId'] == 8) {
			$pka = $val['jumlahOrang'];
		}else{
			$pka =0;
		}
		if ($val['jenisDiklatId'] == 9) {
			$pkp = $val['jumlahOrang'];
		}else{
			$pkp =0;
		}
		if ($val['jenisDiklatId'] == 11) {
			$PKTUF = $val['jumlahOrang'];
		}else{
			$PKTUF =0;
		}
		if ($val['jenisDiklatId'] == 12) {
			$PKTI = $val['jumlahOrang'];
		}else{
			$PKTI =0;
		}
		$calendar[] = array(
			'a' => $val['tahun'], 
			'b' => $latsar,
			'c' => $prajab,
			'd' => $pka,
			'e' => $pkp,
			'f' => $PKTUF,
			'g' => $PKTI,
		   );
		}
		
		$data = array();
		$data   = $rs;

		$extra = array(
				'rowsCount' => $rowsCount,
				'pageCount' => $pageCount,
		);

		return $this->success('DATA_READ', $data, $extra);
	}


    public function filterData($params = array())
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();

        // Query

        $field = $this->request('field');
        $keyword = $this->request('keyword');

        $queryStmt = "where $field like ?";
        $rowsCount = $this->getRowsCount($queryStmt, array("$keyword%"));

        // Limit

        $page = $this->request('page');
		$count = $this->request('count');

        $limitStmt = '';
        $pageCount = 1;

        if (!empty($page) && !empty($count)) {
            $row = ($page * $count) - $count;
            $limitStmt = "limit $row, $count";

            $pageCount = ceil($rowsCount / $count);

            if($pageCount < 1)
                $pageCount = 1;
        }

        $rs = $this->select($queryStmt, array("$keyword%"));

        $extra = array(
            'rowsCount' => $rowsCount,
            'pageCount' => $pageCount,
        );

		return $this->success('DATA_READ', $rs, $extra);
	}

	public function detailData($params = array())
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();

		$id = $this->request('id');

        // Query

		$rs = $this->find($id);

        if($rs)
            return $this->success('DATA_READ', $rs);
        else
            return $this->failed('DATA_NOT_FOUND');
	}

    private function checkRequest() {
        $diklatId = $this->request('diklatId');
        $akunId = $this->request('akunId');
        $noRegistrasi = $this->request('noRegistrasi');

		$error = array();

		if(empty($diklatId))
			$error['diklatId'] = $this->string('NAMA_DIKLAT_REQUIRED');

		if(empty($akunId))
			$error['akunId'] = $this->string('NAMA_ALUMNI_REQUIRED');

		if(empty($noRegistrasi))
			$error['noRegistrasi'] = $this->string('NO_REGISTRASI_REQUIRED');

        return $error;
    }

	public function createData($params = array())
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

		$error = $this->checkRequest();

		$diklatId = $this->request('diklatId');
		$akunId = $this->request('akunId');
		$noRegistrasi = $this->request('noRegistrasi');

		if(count($error) > 0)
			return $this->invalid('PLEASE_CORRECT', $error);

		$rs = array(
            'diklatId' => $diklatId,
			'akunId' => $akunId,
			'noRegistrasi' => $noRegistrasi,
        );

		$this->insert($rs);

		return $this->success('DATA_CREATED');
	}

	public function updateData($params = array())
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

        $id = $this->request('id');
        $diklatId = $this->request('diklatId');
		$akunId = $this->request('akunId');
		$noRegistrasi = $this->request('noRegistrasi');

		$error = $this->checkRequest();

        $rs = $this->find($id);

        if(!$rs)
            return $this->failed('DATA_NOT_FOUND');

		if(count($error) > 0)
			return $this->invalid('PLEASE_CORRECT', $error);

		$rs = array(
			'diklatId' => $diklatId,
			'akunId' => $akunId,
			'noRegistrasi' => $noRegistrasi,
		);

		$this->update($id, $rs);
		return $this->success('DATA_UPDATED');
	}

	public function deleteData($params = array())
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

		$id = $this->request('id');
        $rs = $this->find($id);

		$this->delete($id);
		return $this->success('DATA_DELETED');
	}

	public function multipleDeleteData($params = array())
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

		$multipleId = $this->request('multipleId');

		foreach($multipleId as $id)
            $this->delete($id);

		return $this->success('DATA_DELETED');
	}

	public function createPDFList($params = array())
	{
		if(!$this->setSettings(array(
				'authen' => 'user',
				'requestSource' => $params,
		)))
			return $this->denied();

		// PDF Settings

		$this->load->library('pdf');
		$this->pdf->setSettings(array(
				'strings' => $this->config->item('strings'),
				'title' => 'KELULUSAN_LIST',
				'pageOrientation' => 'P',
		));

		// Query

		$jenisDiklatId = $this->request('jenisDiklatId');
		$tahun = $this->request('tahun');
		$namaDiklatId = $this->request('namaDiklatId');
		$search = $this->request('id');
		$rowKop='';
		
		if(!empty($jenisDiklatId) && !empty($tahun) && !empty($namaDiklatId) && !empty($search))
        	$rs = $this->select("where jenisDiklatId = ? and tahun = ? and diklatId = ? and (nipNama LIKE ? or noRegistrasi LIKE ? or namaDiklat LIKE ?) ", array($jenisDiklatId, $tahun, $namaDiklatId, "%$search%", "%$search%", "%$search%"));
		elseif(!empty($jenisDiklatId) && !empty($tahun) && !empty($namaDiklatId))
        	$rs = $this->select("where jenisDiklatId = ? and tahun = ? and diklatId = ? ", array($jenisDiklatId, $tahun, $namaDiklatId));
		elseif(!empty($jenisDiklatId) && !empty($tahun) && !empty($search))
        	$rs = $this->select("where jenisDiklatId = ? and tahun = ?  and (nipNama LIKE ? or noRegistrasi LIKE ? or namaDiklat LIKE ?) ", array($jenisDiklatId, $tahun, "%$search%", "%$search%", "%$search%"));
		elseif(!empty($jenisDiklatId) && !empty($namaDiklatId) && !empty($search))
			$rs = $this->select("where jenisDiklatId = ? and diklatId = ?  and (nipNama LIKE ? or noRegistrasi LIKE ? or namaDiklat LIKE ?) ", array($jenisDiklatId, $namaDiklatId, "%$search%", "%$search%", "%$search%"));
		elseif(!empty($tahun) && !empty($namaDiklatId) && !empty($search))
			$rs = $this->select("where tahun = ? and diklatId = ?  and (nipNama LIKE ? or noRegistrasi LIKE ? or namaDiklat LIKE ?) ", array($tahun, $namaDiklatId, "%$search%", "%$search%", "%$search%"));
		elseif(!empty($jenisDiklatId) && !empty($tahun))
			$rs = $this->select("where jenisDiklatId = ? and tahun = ?  ", array($jenisDiklatId, $tahun));
		elseif(!empty($jenisDiklatId) && !empty($namaDiklatId))
			$rs = $this->select("where jenisDiklatId = ? and diklatId = ? ", array($jenisDiklatId, $namaDiklatId));
		elseif(!empty($tahun) && !empty($namaDiklatId))
			$rs = $this->select("where tahun = ? and diklatId = ? ", array($tahun, $namaDiklatId));
		elseif(!empty($jenisDiklatId) && !empty($search))
			$rs = $this->select("where jenisDiklatId = ?  and (nipNama LIKE ? or noRegistrasi LIKE ? or namaDiklat LIKE ?) ", array($jenisDiklatId, "%$search%", "%$search%", "%$search%"));
		elseif(!empty($namaDiklatId) && !empty($search))
			$rs = $this->select("where diklatId = ?  and (nipNama LIKE ? or noRegistrasi LIKE ? or namaDiklat LIKE ?) ", array($namaDiklatId, "%$search%", "%$search%", "%$search%"));
		elseif(!empty($tahun) && !empty($search))
			$rs = $this->select("where tahun = ?  and (nipNama LIKE ? or noRegistrasi LIKE ? or namaDiklat LIKE ?) ", array($tahun, "%$search%", "%$search%", "%$search%"));
		elseif(!empty($jenisDiklatId))
			$rs = $this->select("where jenisDiklatId = ? ", array($jenisDiklatId));
		elseif(!empty($namaDiklatId)){
			$rs = $this->select("where diklatId = ?  ", array($namaDiklatId));
			$rowKop = $this->find('diklatId',$namaDiklatId);
		}elseif(!empty($tahun))
			$rs = $this->select("where tahun = ? ", array($tahun));
		elseif(!empty($search))
			$rs = $this->select("where (nipNama LIKE ? or noRegistrasi LIKE ? or namaDiklat LIKE ?) ", array("%$search%", "%$search%", "%$search%"));
        else
        	$rs = $this->select();

		// Output
        

		if (count($rs) > 0) {
			$fileUrl = $this->pdf->render(
					'KelulusanList',
					array(
            'rs' => $rs,
            'rowKop' => $rowKop,
          )
			);

			return $this->success('FILE_CREATED', $fileUrl);
		}
		else
			return $this->failed('FILE_CREATE_FAILED');
	}

}

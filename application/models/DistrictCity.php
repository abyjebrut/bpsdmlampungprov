<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DistrictCity extends MY_Model {
    protected $table = 'tb_district_city';
    protected $view = 'vi_district_city';
    
    public function readData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();

        // order

        $orderBy = $this->request('orderBy');
        $reverse = $this->request('reverse');

        $orderStmt = '';

        if (!empty($orderBy)) {
            $orderStmt = "order by $orderBy";

            if (!empty($reverse)) {
                if ($reverse == 1)
                    $orderStmt .= ' desc';
            }
        }

        // Limit

        $page = $this->request('page');
		$count = $this->request('count');

        $limitStmt = '';

        $rowsCount = $this->getRowsCount();
        $pageCount = 1;

        if (!empty($page) && !empty($count)) {
            $row = ($page * $count) - $count;
            $limitStmt = "limit $row, $count";

            $pageCount = ceil($rowsCount / $count);

            if($pageCount < 1)
                $pageCount = 1;
        }

        // query

        $regionalId = $this->request('regionalId');

        if(!empty($regionalId))
            $rs = $this->select("where regionalId = ? ".$orderStmt.' '.$limitStmt, array($regionalId));
        else
            $rs = $this->select($orderStmt.' '.$limitStmt);

		$extra = array(
            'rowsCount' => $rowsCount,
            'pageCount' => $pageCount,
        );

		return $this->success('DATA_READ', $rs, $extra);
	}

    public function filterData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();
        // query

        $field = $this->request('field');
        $keyword = $this->request('keyword');

        $queryStmt = "where $field like ?";
        $rowsCount = $this->getRowsCount($queryStmt, array("$keyword%"));

        // Limit

        $page = $this->request('page');
		$count = $this->request('count');

        $limitStmt = '';
        $pageCount = 1;

        if (!empty($page) && !empty($count)) {
            $row = ($page * $count) - $count;
            $limitStmt = "limit $row, $count";

            $pageCount = ceil($rowsCount / $count);

            if($pageCount < 1)
                $pageCount = 1;
        }

        $rs = $this->select($queryStmt, array("$keyword%"));

        $extra = array(
            'rowsCount' => $rowsCount,
            'pageCount' => $pageCount,
        );

		return $this->success('DATA_READ', $rs, $extra);
	}

	public function detailData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();

		$id = $this->request('id');
        
        // Query
        
		$rs = $this->find($id);

        if($rs)
            return $this->success('DATA_READ', $rs);
        else
            return $this->failed('DATA_NOT_FOUND');
	}

    private function checkRequest() {
        $regionalId = $this->request('regionalId');
        $districtCity = $this->request('districtCity');

		$error = array();

        if(empty($regionalId))
			$error['regionalId'] = $this->string('REGIONAL_REQUIRED');

		if(empty($districtCity))
			$error['districtCity'] = $this->string('DISTRICT_CITY_REQUIRED');

        return $error;
    }

	public function createData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

		$error = $this->checkRequest();

        $regionalId = $this->request('regionalId');
        $districtCity = $this->request('districtCity');

        $rs = $this->single('where regionalId = ? and districtCity = ?', array(
            $regionalId,
            $districtCity,
        ));

		if($rs)
            $error['districtCity'] = $this->string('DISTRICT_CITY_IS_REG');

		if(count($error) > 0)
			return $this->invalid('PLEASE_CORRECT', $error);

		$rs = array(
            'regionalId' => $regionalId,
            'districtCity' => $districtCity,
        );

		$this->insert($rs);
		return $this->success('DATA_CREATED');
	}

	public function updateData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

        $id = $this->request('id');
        $regionalId = $this->request('regionalId');
        $districtCity = $this->request('districtCity');

		$error = $this->checkRequest();
        
        $rs = $this->find($id);
        if(!$rs)
            return $this->failed('DATA_NOT_FOUND');

        $last_districtCity = $rs['districtCity'];
        $last_regionalId = $rs['regionalId'];

        if($districtCity != $last_districtCity || $regionalId != $last_regionalId) {
            $rs = $this->single('where regionalId = ? and districtCity = ?', array(
            $regionalId,
            $districtCity,
        ));

            if($rs)
                $error['districtCity'] = $this->string('DISTRICT_CITY_IS_REG');
        }

		if(count($error) > 0)
			return $this->invalid('PLEASE_CORRECT', $error);

		$rs = array(
				'regionalId' => $regionalId,
                'districtCity' => $districtCity,
		);

		$this->update($id, $rs);
		return $this->success('DATA_UPDATED');
	}

	public function deleteData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

		$id = $this->request('id');
        $rs = $this->find($id);

		$this->delete($id);
		return $this->success('DATA_DELETED');
	}

	public function multipleDeleteData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

		$multipleId = $this->request('multipleId');

		foreach($multipleId as $id)
            $this->delete($id);

		return $this->success('DATA_DELETED');
	}
    
    public function createPDFList($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();
        
        // PDF Settings
        
        $this->load->library('pdf');
        $this->pdf->setSettings(array(
            'strings' => $this->config->item('strings'),
            'title' => 'DISTRICT_CITY_LIST',
            'pageOrientation' => 'P',
        ));
        
        // Query
        
        $rs = $this->select();
        
        // Output
        
        if (count($rs) > 0) {
            $fileUrl = $this->pdf->render(
                'DistrictCityList',
                array('rs' => $rs)
            );

            return $this->success('FILE_CREATED', $fileUrl);
        }
        else
            return $this->failed('FILE_CREATE_FAILED');
    }
    
    public function createPDFDetail($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();
        
        // PDF Settings
        
        $this->load->library('pdf');
        $this->pdf->setSettings(array(
            'strings' => $this->config->item('strings'),
            'title' => 'DISTRICT_CITY_DETAIL',
            'pageOrientation' => 'P',
        ));
        
        // Request
        
        $id = $this->request('id');
        
        // Query
        
        $row = $this->find($id);
        
        // Output
        
        if (!empty($row)) {
            $fileUrl = $this->pdf->render(
                'DistrictCityDetail',
                $row
            );

            return $this->success('FILE_CREATED', $fileUrl);
        }
        else
            return $this->failed('FILE_CREATE_FAILED');
    }
    
    public function createExcelList($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();
        
        // Excel Settings
        
        $this->load->library('excel');
//        $this->excel->setSettings(array(
//            'strings' => $this->config->item('strings'),
//            'title' => 'REGIONAL_LIST',
//            'pageOrientation' => 'P',
//        ));
        
        // Query
        
        $rs = $this->select();
        
        // Output
        
        if (count($rs) > 0) {
            $fileUrl = $this->excel->renderList(
                'DistrictCityList',
                array(
                    'dataSource' => $rs,
                    'showFields' => array(
                        'districtCity',
                    ),
                )
            );

            return $this->success('FILE_CREATED', $fileUrl);
        }
        else
            return $this->failed('FILE_CREATE_FAILED');
    }
    
    public function createExcelDetail($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();
        
        // Request
        
        $id = $this->request('id');
        
        // Excel Settings
        
        $this->load->library('excel');
//        $this->excel->setSettings(array(
//            'strings' => $this->config->item('strings'),
//            'title' => 'REGIONAL_LIST',
//            'pageOrientation' => 'P',
//        ));
        
        // Query
        
        $row = $this->find($id);
        
        // Output
        
        if (!empty($row)) {
            $fileUrl = $this->excel->renderDetail(
                'DistrictCityDetail',
                array(
                    'dataSource' => $row,
                    'showFields' => array(
                        'regional',
                        'districtCity',
                    ),
                )
            );

            return $this->success('FILE_CREATED', $fileUrl);
        }
        else
            return $this->failed('FILE_CREATE_FAILED');
    }

}
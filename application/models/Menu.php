<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends MY_Model {
    protected $table = 'tb_menu';
    protected $view = 'vi_menu';

    public function readData($params = array())
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();

        // Order

        $orderBy = $this->request('orderBy');
        $reverse = $this->request('reverse');

        $orderStmt = '';

        if (!empty($orderBy)) {
            $orderStmt = "order by $orderBy";

            if (!empty($reverse)) {
                if ($reverse == 1)
                    $orderStmt .= ' desc';
            }
        }

        // Limit

        $page = $this->request('page');
		$count = $this->request('count');
		$publishStatusId = $this->request('publishStatusId');

        $limitStmt = '';

        if (!empty($publishStatusId))
	      $rowsCount = $this->getRowsCount("where publishStatusId = ? ", array($publishStatusId));
	    else
	      $rowsCount = $this->getRowsCount($orderStmt);

        $pageCount = 1;

        if (!empty($page) && !empty($count)) {
            $row = ($page * $count) - $count;
            $limitStmt = "limit $row, $count";

            $pageCount = ceil($rowsCount / $count);

            if($pageCount < 1)
                $pageCount = 1;
        }

        // Query

		if (!empty($publishStatusId))
	      $rs = $this->select("where publishStatusId = ? ".$orderStmt.' '.$limitStmt, array($publishStatusId));
	    else
	      $rs = $this->select($orderStmt.' '.$limitStmt);

	    $extRs = array();

        $mSubMenu = $this->model('vi_sub_menu');

        foreach($rs as $row) {

            if ($row['menuBehaviorId'] == 3) {
                $rsSubMenu = $mSubMenu->select('where menuId = ?', array($row['id']));
                $row['subMenu'] = $rsSubMenu;
            }

            $extRs[] = $row;
        }

        $extra = array(
            'rowsCount' => $rowsCount,
            'pageCount' => $pageCount,
        );

		return $this->success('DATA_READ', $extRs, $extra);
	}

    public function filterData($params = array())
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();

        // query

        $field = $this->request('field');
        $keyword = $this->request('keyword');

        $queryStmt = "where $field like ?";
        $rowsCount = $this->getRowsCount($queryStmt, array("$keyword%"));

        // Limit

        $page = $this->request('page');
		$count = $this->request('count');

        $limitStmt = '';
        $pageCount = 1;

        if (!empty($page) && !empty($count)) {
            $row = ($page * $count) - $count;
            $limitStmt = "limit $row, $count";

            $pageCount = ceil($rowsCount / $count);

            if($pageCount < 1)
                $pageCount = 1;
        }

        $rs = $this->select($queryStmt, array("$keyword%"));

        $extra = array(
            'rowsCount' => $rowsCount,
            'pageCount' => $pageCount,
        );

		return $this->success('DATA_READ', $rs, $extra);
	}

	public function detailData($params = array())
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();

		$id = $this->request('id');

        // Query

		$rs = $this->find($id);

		$mItems = $this->model('vi_sub_menu');
		$rs['subMenu'] = $mItems->select('where menuId = ? order by id', array($id));

        if($rs)
            return $this->success('DATA_READ', $rs);
        else
            return $this->failed('DATA_NOT_FOUND');
	}

    private function checkRequest() {
        $menuCaption = $this->request('menuCaption');
        $publishStatusId = $this->request('publishStatusId');

		$error = array();

		if(empty($menuCaption))
			$error['menuCaption'] = $this->string('MENU_CAPTION_REQUIRED');

		if(empty($publishStatusId))
			$error['publishStatusId'] = $this->string('PUBLISH_STATUS_REQUIRED');


        return $error;
    }

	public function createData($params = array())
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

		$error = $this->checkRequest();

        $menuCaption = $this->request('menuCaption');
        $menuCaption_sl = $this->request('menuCaptionSl');
        $menuCaption_el = $this->request('menuCaptionEl');
        $menuBehaviorId = $this->request('menuBehaviorId');
        $pagesId = $this->request('pagesId');
        $latitude = $this->request('latitude');
        $url = $this->request('url');
        $openURLOnNewTabId = $this->request('openURLOnNewTabId');
        $subMenu = $this->request('subMenu');
        $publishStatusId = $this->request('publishStatusId');

        $rs = $this->find('menuCaption', $menuCaption);

		if($rs)
            $error['menuCaption'] = $this->string('MENU_CAPTION_IS_REG');

		if(count($error) > 0)
			return $this->invalid('PLEASE_CORRECT', $error);

		$rs = array(
            'menuCaption' => $menuCaption,
			'menuCaption_sl' => $menuCaption_sl,
			'menuCaption_el' => $menuCaption_el,
			'menuBehaviorId' => $menuBehaviorId,
            'pagesId' => $pagesId,
            'url' => $url,
            'openUrlOnNewTabId' => $openURLOnNewTabId,
			'publishStatusId' => $publishStatusId,
        );

		$this->insert($rs);

        // position
        $lastId = $this->getLastInsertId();
        $this->update($lastId, array('position' => $lastId));

        if ($menuBehaviorId==3){
	        $mSubMenu = $this->model('tb_sub_menu');
	        if($subMenu){
		        foreach($subMenu as $sub) {
		        	$mSubMenu->insert(array(
		        			'menuId' => $lastId,
		        			'subMenuCaption' => $sub['subMenuCaption'],
		        			'subMenuCaption_sl' => $sub['subMenuCaption_sl'],
		        			'subMenuCaption_el' => $sub['subMenuCaption_el'],
		        			'subMenuBehaviorId' => $sub['subMenuBehaviorId'],
		        			'pagesId' => $sub['pagesId'],
		        			'url' => $sub['url'],
		        			'openUrlOnNewTabId' => $sub['openUrlOnNewTabId'],
		        	));
		        }
	        }
        }
		return $this->success('DATA_CREATED');
	}

	public function updateData($params = array())
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

        $id = $this->request('id');
        $menuCaption = $this->request('menuCaption');
        $menuCaption_sl = $this->request('menuCaptionSl');
        $menuCaption_el = $this->request('menuCaptionEl');
        $menuBehaviorId = $this->request('menuBehaviorId');
        $pagesId = $this->request('pagesId');
        if(!$pagesId)
        $pagesId=0;

        $latitude = $this->request('latitude');
        $url = $this->request('url');
        $openURLOnNewTabId = $this->request('openURLOnNewTabId');
        $subMenu = $this->request('subMenu');
        $publishStatusId = $this->request('publishStatusId');

		$error = $this->checkRequest();

        $rs = $this->find($id);
        if(!$rs)
            return $this->failed('DATA_NOT_FOUND');

        $last_menuCaption = $rs['menuCaption'];

        if($menuCaption != $last_menuCaption) {
            $rs = $this->find('menuCaption', $menuCaption);

            if($rs)
                $error['menuCaption'] = $this->string('MENU_CAPTION_IS_REG');
        }

		if(count($error) > 0)
			return $this->invalid('PLEASE_CORRECT', $error);

		$rs = array(
            'menuCaption' => $menuCaption,
			'menuCaption_sl' => $menuCaption_sl,
			'menuCaption_el' => $menuCaption_el,
			'menuBehaviorId' => $menuBehaviorId,
            'pagesId' => $pagesId,
            'url' => $url,
            'openUrlOnNewTabId' => $openURLOnNewTabId,
			'publishStatusId' => $publishStatusId,
        );

		$this->update($id, $rs);

		if ($menuBehaviorId==3){
			$mSubMenu = $this->model('tb_sub_menu');
			$mSubMenu->delete('menuId', $id);

			if($subMenu){
			$mSubMenu = $this->model('tb_sub_menu');


				foreach($subMenu as $sub) {
                    $pagesIdd= $sub['pagesId'];
                    if(!$pagesIdd)
                    $pagesIdd =0;
					$mSubMenu->insert(array(
							'menuId' => $id,
							'subMenuCaption' => $sub['subMenuCaption'],
							'subMenuCaption_sl' => $sub['subMenuCaption_sl'],
							'subMenuCaption_el' => $sub['subMenuCaption_el'],
		        			'subMenuBehaviorId' => $sub['subMenuBehaviorId'],
		        			'pagesId' => $pagesIdd,
		        			'url' => $sub['url'],
		        			'openUrlOnNewTabId' => $sub['openUrlOnNewTabId'],
					));
				}
			}
		}
		return $this->success('DATA_UPDATED');
	}

    public function moveData($params = array())
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

		$idFrom = $this->request('idFrom');
		$idTo = $this->request('idTo');

        $rsFrom = $this->find($idFrom);
        $rsTo = $this->find($idTo);

        $positionFrom = $rsFrom['position'];
        $positionTo = $rsTo['position'];

		$this->update($idFrom, array('position' => $positionTo));
        $this->update($idTo, array('position' => $positionFrom));

		return $this->success('DATA_UPDATED');
	}

	public function deleteData($params = array())
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

		$id = $this->request('id');
        $rs = $this->find($id);

		$this->delete($id);
		return $this->success('DATA_DELETED');
	}

	public function multipleDeleteData($params = array())
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

		$multipleId = $this->request('multipleId');

		foreach($multipleId as $id){
			$mSubMenu = $this->model('tb_sub_menu');
			$mSubMenu->delete('menuId', $id);

			$this->delete($id);
		}


		return $this->success('DATA_DELETED');
	}

}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SoalAkd extends MY_Model {
    protected $table = 'tb_soal_akd';
    protected $view = 'vi_soal_akd';

    public function readData($params = array())
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();

        // Order

        $akunId = $this->request('akunId');

        $orderBy = $this->request('orderBy');
        $reverse = $this->request('reverse');

        $orderStmt = '';

        if (!empty($orderBy)) {
            $orderStmt = "order by $orderBy";

            if (!empty($reverse)) {
                if ($reverse == 1)
                    $orderStmt .= ' desc';
            }
        }

        // Limit
        $page = $this->request('page');
		$count = $this->request('count');
		$telahDijawabId = 1;

        $limitStmt = '';

        if (!empty($akunId))
          $rowsCount = $this->getRowsCount("where akunId = ? and telahDijawabId = ? ", array($akunId, $telahDijawabId));
        else
          $rowsCount = $this->getRowsCount("where telahDijawabId = ? ", array($telahDijawabId));

      	$pageCount = 1;

        if (!empty($page) && !empty($count)) {
            $row = ($page * $count) - $count;
            $limitStmt = "limit $row, $count";

            $pageCount = ceil($rowsCount / $count);

            if($pageCount < 1)
                $pageCount = 1;
        }

        // Query

        if (!empty($akunId))
          $rs = $this->select("where akunId = ? and telahDijawabId = ? ".$orderStmt.' '.$limitStmt, array($akunId, $telahDijawabId));
        else
          $rs = $this->select("where telahDijawabId = ? ".$orderStmt.' '.$limitStmt, array($telahDijawabId));

        $extra = array(
            'rowsCount' => $rowsCount,
            'pageCount' => $pageCount,
        );

		return $this->success('DATA_READ', $rs, $extra);
	}

	public function readDataJawaban($params = array())
	{
		if(!$this->setSettings(array(
				'authen' => 'free',
				'requestSource' => $params,
		)))
			return $this->denied();

		// Order

    $akunId = $this->request('akunId');

		$orderBy = $this->request('orderBy');
		$reverse = $this->request('reverse');

		$orderStmt = '';

		if (!empty($orderBy)) {
			$orderStmt = "order by $orderBy";

			if (!empty($reverse)) {
				if ($reverse == 1)
					$orderStmt .= ' desc';
			}
		}

		// Limit
		$page = $this->request('page');
		$count = $this->request('count');
		$akunId = $this->request('akunId');
		$telahDijawabId = 2;

		$limitStmt = '';
		if (!empty($akunId))
			$rowsCount = $this->getRowsCount("where telahDijawabId = ? and akunId = ? ", array($telahDijawabId, $akunId));
		else
			$rowsCount = $this->getRowsCount("where telahDijawabId = ? ", array($telahDijawabId));

		$pageCount = 1;

		if (!empty($page) && !empty($count)) {
			$row = ($page * $count) - $count;
			$limitStmt = "limit $row, $count";

			$pageCount = ceil($rowsCount / $count);

			if($pageCount < 1)
				$pageCount = 1;
		}

		// Query
		if (!empty($akunId))
			$rs = $this->select("where telahDijawabId = ? and akunId = ? ".$orderStmt.' '.$limitStmt, array($telahDijawabId,$akunId));
		else
			$rs = $this->select("where telahDijawabId = ? ".$orderStmt.' '.$limitStmt, array($telahDijawabId));

		$extra = array(
				'rowsCount' => $rowsCount,
				'pageCount' => $pageCount,
		);

		return $this->success('DATA_READ', $rs, $extra);
	}

    public function filterData($params = array())
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();

        // Query

        $field = $this->request('field');
        $keyword = $this->request('keyword');

        $queryStmt = "where $field like ?";
        $rowsCount = $this->getRowsCount($queryStmt, array("$keyword%"));

        // Limit

        $page = $this->request('page');
		$count = $this->request('count');

        $limitStmt = '';
        $pageCount = 1;

        if (!empty($page) && !empty($count)) {
            $row = ($page * $count) - $count;
            $limitStmt = "limit $row, $count";

            $pageCount = ceil($rowsCount / $count);

            if($pageCount < 1)
                $pageCount = 1;
        }

        $rs = $this->select($queryStmt, array("$keyword%"));

        $extra = array(
            'rowsCount' => $rowsCount,
            'pageCount' => $pageCount,
        );

		return $this->success('DATA_READ', $rs, $extra);
	}

	public function detailData($params = array())
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();

		$id = $this->request('id');

        // Query

		$rs = $this->find($id);

		$mItems = $this->model('vi_soal_akd_rumusan_kompetensi');
		$rs['links'] = $mItems->select('where soalAkdId = ? order by id', array($id));

        if($rs)
            return $this->success('DATA_READ', $rs);
        else
            return $this->failed('DATA_NOT_FOUND');
	}

    private function checkRequest() {
        $jabatan = $this->request('jabatan');
        $unitKerja = $this->request('unitKerja');
        $instansi = $this->request('instansi');
        $akunId = $this->request('akunId');
        $kedudukanId = $this->request('kedudukanId');

		$error = array();

		if(empty($jabatan))
			$error['jabatan'] = $this->string('JABATAN_REQUIRED');
		
		if(empty($unitKerja))
			$error['unitKerja'] = $this->string('UNIT_KERJA_REQUIRED');

		if(empty($instansi))
			$error['instansi'] = $this->string('INSTANSI_REQUIRED');
		
		if(empty($akunId))
			$error['akunId'] = $this->string('ANGGOTA_REQUIRED');
		
		if(empty($kedudukanId))
			$error['kedudukanId'] = $this->string('KEDUDUKAN_REQUIRED');

        return $error;
    }

    private function checkRequestJawaban() {
    	$links = $this->request('links');

    	$error = array();

    	foreach($links as $link) {

    		if(empty($link['tingkatKesanggupanId']))
    			$error['tingkatKesanggupanId'] = $this->string('QUESTION_UNCOMPLETED');
    	}

    	return $error;
    }

	public function createData($params = array())
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

		$error = $this->checkRequest();
        $jabatan = $this->request('jabatan');
        $unitKerja = $this->request('unitKerja');
        $instansi = $this->request('instansi');
        $akunId = $this->request('akunId');
        $kedudukanId = $this->request('kedudukanId');
		$links = $this->request('links');

		if(count($error) > 0)
			return $this->invalid('PLEASE_CORRECT', $error);

		$rs = array(
			'tanggalPembuatan' => date('Y-m-d'),
            'jabatan' => $jabatan,
			'unitKerja' => $unitKerja,
			'instansi' => $instansi,
			'akunId' => $akunId,
			'kedudukanId' => $kedudukanId,
			'telahDijawabId' => 1,
        );

		$this->insert($rs);
		$lastId = $this->getLastInsertId();

		if($links){
			$mItems = $this->model('tb_soal_akd_rumusan_kompetensi');

			foreach($links as $row) {
				$mItems->insert(array(
						'soalAkdId' => $lastId,
						'rumusanKompetensi' => $row['rumusanKompetensi'],
				));
			}
		}

		return $this->success('DATA_CREATED');
	}

	public function updateData($params = array())
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

        $id = $this->request('id');
        $jabatan = $this->request('jabatan');
        $unitKerja = $this->request('unitKerja');
        $instansi = $this->request('instansi');
        $akunId = $this->request('akunId');
        $kedudukanId = $this->request('kedudukanId');
		$links = $this->request('links');

        $error = $this->checkRequest();

        $rs = $this->find($id);

        if(!$rs)
            return $this->failed('DATA_NOT_FOUND');

		if(count($error) > 0)
			return $this->invalid('PLEASE_CORRECT', $error);

		$rs = array(
            'jabatan' => $jabatan,
			'unitKerja' => $unitKerja,
			'instansi' => $instansi,
			'akunId' => $akunId,
			'kedudukanId' => $kedudukanId,
		);

		$this->update($id, $rs);

		if($links){
			$mItems = $this->model('tb_soal_akd_rumusan_kompetensi');
			$mItems->delete('soalAkdId', $id);

			foreach($links as $link) {
				$mItems->insert(array(
						'soalAkdId' => $id,
						'rumusanKompetensi' => $link['rumusanKompetensi'],
				));
			}
		}

		return $this->success('DATA_UPDATED');
	}

	public function updateDataJawaban($params = array())
	{
		if(!$this->setSettings(array(
				'authen' => 'user',
				'requestSource' => $params,
		)))
			return $this->denied();

		$id = $this->request('id');
		$links = $this->request('links');

		$error = $this->checkRequestJawaban();

		$rs = $this->find($id);

		if(!$rs)
			return $this->failed('DATA_NOT_FOUND');

		if(count($error) > 0)
			return $this->invalid('PLEASE_CORRECT', $error);

		$rs = array(
				'telahDijawabId' => 2,
				'tanggalPengisian' => date('Y-m-d'),
		);

		$this->update($id, $rs);

		if($links){
			$mItems = $this->model('tb_soal_akd_rumusan_kompetensi');
			$mItems->delete('soalAkdId', $id);

			foreach($links as $link) {

				$mItems->insert(array(
						'soalAkdId' => $id,
						'rumusanKompetensi' => $link['rumusanKompetensi'],
						'tingkatKesanggupanId' => $link['tingkatKesanggupanId'],
				));
			}
		}

		return $this->success('DATA_UPDATED');
	}

	public function sendEmailAkd($params = array())
	{
		if(!$this->setSettings(array(
				'authen' => 'user',
				'requestSource' => $params,
		)))
			return $this->denied();

		$id = $this->request('id');
		$rs = $this->find($id);
		if(!$rs)
			return $this->failed('DATA_NOT_FOUND');

		$email = $rs['email'];

		$config = Array(
				'protocol' => 'smtp',
				//    'smtp_host' => 'ssl://smtp.googlemail.com',
				'smtp_host' => 'ssl://iix77.rumahweb.com',
				'smtp_port' => 465,
				'smtp_user' => 'f.anaturdasa@diklatlampung.info',
				'smtp_pass' => 'sasa!1#1',
				'mailtype'  => 'html',
				'charset'   => 'iso-8859-1'
		);

		$this->load->library('email', $config);
		$this->email->set_newline("\r\n");

		$this->email->initialize($config);

		$this->email->from('f.anaturdasa@diklatlampung.info', 'fadlun');
		$this->email->to($email);

		$this->email->subject('Pemberitahuan Diklatlampung');
		$this->email->message('Anda memiliki test pertanyaan, silakan melakukan login di http://diklatlampung.info');

		$this->email->send();

		return $this->success('NOTIF_SEND_EMAIL');
	}

	public function deleteData($params = array())
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

		$id = $this->request('id');
        $rs = $this->find($id);

		$this->delete($id);
		return $this->success('DATA_DELETED');
	}

	public function multipleDeleteData($params = array())
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

		$multipleId = $this->request('multipleId');

		foreach($multipleId as $id){
			
			$rs = $this->find($id);

			$mSubMenu = $this->model('tb_soal_akd_rumusan_kompetensi');
			$mSubMenu->delete('soalAkdId', $id);

			$this->delete($id);
		}


		return $this->success('DATA_DELETED');
	}

	public function createPDFDetail($params = array())
	{
		if(!$this->setSettings(array(
				'authen' => 'user',
				'requestSource' => $params,
		)))
			return $this->denied();

		// PDF Settings

		$this->load->library('headless_pdf');
    $this->headless_pdf->setSettings(array(
				'strings' => $this->config->item('strings'),
				'pageOrientation' => 'P',
		));

		// Request

		$id = $this->request('id');
		$mItems = $this->model('vi_soal_akd_rumusan_kompetensi');
		$rsItems = $mItems->select('where soalAkdId = ? order by id', array($id));

		// Query

		$row = $this->find($id);

		// Output

			if (!empty($row)) {


				$fileUrl = $this->headless_pdf->render(
						'JawabanAkd',
						array(
								'row' => $row,
								'rsItems' => $rsItems,
						)
				);

			return $this->success('FILE_CREATED', $fileUrl);
		}
		else
			return $this->failed('FILE_CREATE_FAILED');
	}

}

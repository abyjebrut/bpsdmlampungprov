<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PengajarDiklat extends MY_Model {
    protected $table = 'tb_pengajar_diklat';
    protected $view = 'vi_pengajar_diklat';
    
    public function readData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();

        // Order

        $orderBy = $this->request('orderBy');
        $reverse = $this->request('reverse');

        $orderStmt = '';

        if (!empty($orderBy)) {
            $orderStmt = "order by $orderBy";

            if (!empty($reverse)) {
                if ($reverse == 1)
                    $orderStmt .= ' desc';
            }
        }

        // Limit
        $page = $this->request('page');
		$count = $this->request('count');
        $publishStatusId = $this->request('publishStatusId');
        $limitStmt = '';

      	if (!empty($publishStatusId))
	      $rowsCount = $this->getRowsCount("where publishStatusId = ? ", array($publishStatusId));
	    else
	      $rowsCount = $this->getRowsCount($orderStmt);
        
      	$pageCount = 1;

        if (!empty($page) && !empty($count)) {
            $row = ($page * $count) - $count;
            $limitStmt = "limit $row, $count";

            $pageCount = ceil($rowsCount / $count);

            if($pageCount < 1)
                $pageCount = 1;
        }

        // Query
        
	      if (!empty($publishStatusId))
	      $rs = $this->select("where publishStatusId = ? ".$orderStmt.' '.$limitStmt, array($publishStatusId));
	    else
	      $rs = $this->select($orderStmt.' '.$limitStmt);
    
        $extra = array(
            'rowsCount' => $rowsCount,
            'pageCount' => $pageCount,
        );

		return $this->success('DATA_READ', $rs, $extra);
	}
    
    public function readDataDashboard($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();

        // Order

        $orderBy = $this->request('orderBy');
        $reverse = $this->request('reverse');

        $orderStmt = '';

        if (!empty($orderBy)) {
            $orderStmt = "order by $orderBy";

            if (!empty($reverse)) {
                if ($reverse == 1)
                    $orderStmt .= ' desc';
            }
        }

        // Limit
        $page = $this->request('page');
		$count = $this->request('count');

        $limitStmt = '';

      	$rowsCount = $this->getRowsCount($orderStmt);
        
      	$pageCount = 1;

        if (!empty($page) && !empty($count)) {
            $row = ($page * $count) - $count;
            $limitStmt = "limit $row, $count";

            $pageCount = ceil($rowsCount / $count);

            if($pageCount < 1)
                $pageCount = 1;
        }

        // Query

	      $rs = $this->select($orderStmt.' '.$limitStmt);
    
        $extra = array(
            'rowsCount' => $rowsCount,
            'pageCount' => $pageCount,
        );

		return $this->success('DATA_READ', $rs, $extra);
	}

    public function filterData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();
        
        // Query

        $field = $this->request('field');
        $keyword = $this->request('keyword');

        $queryStmt = "where $field like ?";
        $rowsCount = $this->getRowsCount($queryStmt, array("$keyword%"));

        // Limit

        $page = $this->request('page');
		$count = $this->request('count');

        $limitStmt = '';
        $pageCount = 1;

        if (!empty($page) && !empty($count)) {
            $row = ($page * $count) - $count;
            $limitStmt = "limit $row, $count";

            $pageCount = ceil($rowsCount / $count);

            if($pageCount < 1)
                $pageCount = 1;
        }

        $rs = $this->select($queryStmt, array("$keyword%"));

        $extra = array(
            'rowsCount' => $rowsCount,
            'pageCount' => $pageCount,
        );

		return $this->success('DATA_READ', $rs, $extra);
	}

	public function detailData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();

		$id = $this->request('id');
        
        // Query
        
		$rs = $this->find($id);

        if($rs)
            return $this->success('DATA_READ', $rs);
        else
            return $this->failed('DATA_NOT_FOUND');
	}

    private function checkRequest() {
        $publishStatusId = $this->request('publishStatusId');
        $nama = $this->request('nama');

		$error = array();
		
        if(empty($publishStatusId))
			$error['publishStatusId'] = $this->string('PUBLISH_STATUS_REQUIRED');
        
		if(empty($nama))
			$error['nama'] = $this->string('NAMA_REQUIRED');

        return $error;
    }

	public function createData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

		$error = $this->checkRequest();
        
        $publishStatusId = $this->request('publishStatusId');
		$foto = $this->request('foto');
		$nama = $this->request('nama');
		$deskripsi = $this->request('deskripsi');

		if(count($error) > 0)
			return $this->invalid('PLEASE_CORRECT', $error);

		if(!empty($foto)) {
			if(!$this->moveToArchive($foto))
				return $this->failed('FILE_UPLOAD_FAILED');
		}
		
		$rs = array(
            'publishStatusId' => $publishStatusId,
            'foto' => $foto,
			'nama' => $nama,
			'deskripsi' => $deskripsi,
        );

		$this->insert($rs);
		
		// position
		$lastId = $this->getLastInsertId();
		$this->update($lastId, array('position' => $lastId));
		
		return $this->success('DATA_CREATED');
	}

	public function updateData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

        $id = $this->request('id');
        $publishStatusId = $this->request('publishStatusId');
        $foto = $this->request('foto');
		$nama = $this->request('nama');
		$deskripsi = $this->request('deskripsi');
		
        $error = $this->checkRequest();

        $rs = $this->find($id);
        
        if(!empty($foto)) {
        	if($foto != $rs['foto']) {
        
        		$last_foto = $rs['foto'];
        		$this->deleteArchive($last_foto);
        
        		if(!$this->moveToArchive($foto))
        			return $this->failed('FILE_UPLOAD_FAILED');
        	}
        }
        
        if(!$rs)
            return $this->failed('DATA_NOT_FOUND');

		if(count($error) > 0)
			return $this->invalid('PLEASE_CORRECT', $error);
		
		$rs = array(
            'publishStatusId' => $publishStatusId,
			'foto' => $foto,
			'nama' => $nama,
			'deskripsi' => $deskripsi,
		);

		$this->update($id, $rs);
		return $this->success('DATA_UPDATED');
	}
	
	public function moveData($params = array())
	{
		if(!$this->setSettings(array(
				'authen' => 'user',
				'requestSource' => $params,
		)))
			return $this->denied();
	
		$idFrom = $this->request('idFrom');
		$idTo = $this->request('idTo');
	
		$rsFrom = $this->find($idFrom);
		$rsTo = $this->find($idTo);
	
		$positionFrom = $rsFrom['position'];
		$positionTo = $rsTo['position'];
	
		$this->update($idFrom, array('position' => $positionTo));
		$this->update($idTo, array('position' => $positionFrom));
	
		return $this->success('DATA_UPDATED');
	}

	public function deleteData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

		$id = $this->request('id');
        $rs = $this->find($id);

		$this->delete($id);
		return $this->success('DATA_DELETED');
	}

	public function multipleDeleteData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

		$multipleId = $this->request('multipleId');

		foreach($multipleId as $id){
			$rs = $this->find($id);
			
			if(!empty($rs['foto']))
				$this->deleteArchive($rs['foto']);
			
			$this->delete($id);
		}
            

		return $this->success('DATA_DELETED');
	}
    
    
}
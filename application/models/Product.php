<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends MY_Model {
    protected $table = 'tb_product';
    protected $view = 'vi_product';

    public function readData($params = array())
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();

        // Order

        $orderBy = $this->request('orderBy');
        $reverse = $this->request('reverse');

        $orderStmt = '';

        if (!empty($orderBy)) {
            $orderStmt = "order by $orderBy";

            if (!empty($reverse)) {
                if ($reverse == 1)
                    $orderStmt .= ' desc';
            }
        }

        // Limit

        $categoryId = $this->request('categoryId');
        $publishStatusId = $this->request('publishStatusId');
        $page = $this->request('page');
		    $count = $this->request('count');

        $limitStmt = '';

        if (!empty($categoryId) && !empty($publishStatusId))
            $rowsCount = $this->getRowsCount("where productCategoryId = ? and publishStatusId = ? ", array($categoryId, $publishStatusId));
        elseif (!empty($categoryId))
            $rowsCount = $this->getRowsCount("where productCategoryId = ? ", array($categoryId));
        elseif (!empty($publishStatusId))
            $rowsCount = $this->getRowsCount("where publishStatusId = ? ", array($publishStatusId));
        else
            $rowsCount = $this->getRowsCount($orderStmt);

        $pageCount = 1;

        if (!empty($page) && !empty($count)) {
            $row = ($page * $count) - $count;
            $limitStmt = "limit $row, $count";

            $pageCount = ceil($rowsCount / $count);

            if($pageCount < 1)
                $pageCount = 1;
        }

        // Query

        if (!empty($categoryId) && !empty($publishStatusId))
            $rs = $this->select("where productCategoryId = ? and publishStatusId = ? ".$orderStmt.' '.$limitStmt, array($categoryId, $publishStatusId));
        elseif (!empty($categoryId))
            $rs = $this->select("where productCategoryId = ? ".$orderStmt.' '.$limitStmt, array($categoryId));
        elseif (!empty($publishStatusId))
            $rs = $this->select("where publishStatusId = ? ".$orderStmt.' '.$limitStmt, array($publishStatusId));
        else
            $rs = $this->select($orderStmt.' '.$limitStmt);

        $extra = array(
            'rowsCount' => $rowsCount,
            'pageCount' => $pageCount,
        );

		    return $this->success('DATA_READ', $rs, $extra);
	}

    public function filterData($params = array())
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();

        // Query

        $field = $this->request('field');
        $keyword = $this->request('keyword');

        $queryStmt = "where $field like ?";
        $rowsCount = $this->getRowsCount($queryStmt, array("$keyword%"));

        // Limit

        $page = $this->request('page');
		$count = $this->request('count');

        $limitStmt = '';
        $pageCount = 1;

        if (!empty($page) && !empty($count)) {
            $row = ($page * $count) - $count;
            $limitStmt = "limit $row, $count";

            $pageCount = ceil($rowsCount / $count);

            if($pageCount < 1)
                $pageCount = 1;
        }

        $rs = $this->select($queryStmt, array("$keyword%"));

        $extra = array(
            'rowsCount' => $rowsCount,
            'pageCount' => $pageCount,
        );

		return $this->success('DATA_READ', $rs, $extra);
	}

	public function detailData($params = array())
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();

		$id = $this->request('id');

        // Query

		$rs = $this->find($id);

        if($rs)
            return $this->success('DATA_READ', $rs);
        else
            return $this->failed('DATA_NOT_FOUND');
	}

    private function checkRequest() {
        $productName = $this->request('productName');
        $publishStatusId = $this->request('publishStatusId');

		$error = array();

		if(empty($productName))
			$error['productName'] = $this->string('PRODUCT_NAME_REQUIRED');

		if(empty($publishStatusId))
			$error['publishStatusId'] = $this->string('PUBLISH_STATUS_REQUIRED');

        return $error;
    }

	public function createData($params = array())
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

		$error = $this->checkRequest();

        $productCategoryId = $this->request('productCategoryId');
        $productName = $this->request('productName');
        $productName_sl = $this->request('productNameSl');
        $productName_el = $this->request('productNameEl');
        $coverImage = $this->request('coverImage');
        $description = $this->request('description');
        $description_sl = $this->request('descriptionSl');
    	$description_el = $this->request('descriptionEl');
    	$moreDescription = $this->request('moreDescription');
    	$moreDescription_sl = $this->request('moreDescriptionSl');
    	$moreDescription_el = $this->request('moreDescriptionEl');
        $showPriceId = $this->request('showPriceId');
        if (empty($showPriceId))
        	$showPriceId = 2;
        $price = $this->request('price');
        $images = $this->request('images');
        $publishStatusId = $this->request('publishStatusId');

        $rs = $this->find('productName', $productName);

		if($rs)
            $error['productName'] = $this->string('PRODUCT_NAME_IS_REG');

		if(count($error) > 0)
			return $this->invalid('PLEASE_CORRECT', $error);

		if(!empty($coverImage)) {
			if(!$this->moveToArchive($coverImage))
				return $this->failed('FILE_UPLOAD_FAILED');
		}

		if(!empty($images)) {
			foreach($images as $image) {
				if(!$this->moveToArchive($image))
					return $this->failed('UPLOAD_FAILED');
			}
		}

		if($images)
			$images = implode(',', $images);

		$rs = array(
            'productCategoryId' => $productCategoryId,
			'productName' => $productName,
			'productName_sl' => $productName_sl,
			'productName_el' => $productName_el,
			'coverImage' => $coverImage,
			'description' => $description,
			'description_sl' => $description_sl,
    	  	'description_el' => $description_el,
	      	'moreDescription' => $moreDescription,
    	  	'moreDescription_sl' => $moreDescription_sl,
    	  	'moreDescription_el' => $moreDescription_el,
			'showPriceId' => $showPriceId,
			'price' => $price,
			'images' => $images,
			'publishStatusId' => $publishStatusId,
        );

		$this->insert($rs);

		// position
		$lastId = $this->getLastInsertId();
		$this->update($lastId, array('position' => $lastId));

		return $this->success('DATA_CREATED');
	}

	public function updateData($params = array())
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

        $id = $this->request('id');
        $productCategoryId = $this->request('productCategoryId');
        $productName = $this->request('productName');
        $productName_sl = $this->request('productNameSl');
        $productName_el = $this->request('productNameEl');
        $coverImage = $this->request('coverImage');
        $description = $this->request('description');
        $description_sl = $this->request('descriptionSl');
    	$description_el = $this->request('descriptionEl');
    	$moreDescription = $this->request('moreDescription');
    	$moreDescription_sl = $this->request('moreDescriptionSl');
    	$moreDescription_el = $this->request('moreDescriptionEl');
        $showPriceId = $this->request('showPriceId');
        $price = $this->request('price');
        $images = $this->request('images');
        $publishStatusId = $this->request('publishStatusId');

        $error = $this->checkRequest();

        $rs = $this->find($id);

        if(!empty($coverImage)) {
        	if($coverImage != $rs['coverImage']) {

        		$last_coverImage = $rs['coverImage'];
        		$this->deleteArchive($last_coverImage);

        		if(!$this->moveToArchive($coverImage))
        			return $this->failed('FILE_UPLOAD_FAILED');
        	}
        }

        if(!empty($images)) {

        	$lastImage = $rs['images'];
        	$explodedLastImage = explode(',', $lastImage);

        	foreach($images as $image) {
        		$imageFound = false;

        		if(!empty($lastImage)) {
        			foreach($explodedLastImage as $lastImage) {
        				if($image == $lastImage)
        					$imageFound = true;
        			}
        		}

        		if(!$imageFound) {
        			if(!$this->moveToArchive($image))
        				return $this->failed('UPLOAD_FAILED');
        		}
        	}

        }

        if(!$rs)
            return $this->failed('DATA_NOT_FOUND');

        $last_productName = $rs['productName'];

        if($productName != $last_productName) {
            $rs = $this->find('productName', $productName);

            if($rs)
                $error['productName'] = $this->string('PRODUCT_NAME_IS_REG');
        }

		if(count($error) > 0)
			return $this->invalid('PLEASE_CORRECT', $error);

		if ($images)
			$images = implode(',', $images);

		$rs = array(
			'productCategoryId' => $productCategoryId,
			'productName' => $productName,
			'productName_sl' => $productName_sl,
			'productName_el' => $productName_el,
			'coverImage' => $coverImage,
			'description' => $description,
			'description_sl' => $description_sl,
    	  	'description_el' => $description_el,
	      	'moreDescription' => $moreDescription,
    	  	'moreDescription_sl' => $moreDescription_sl,
    	  	'moreDescription_el' => $moreDescription_el,
			'showPriceId' => $showPriceId,
			'price' => $price,
			'images' => $images,
			'publishStatusId' => $publishStatusId,
		);

		$this->update($id, $rs);
		return $this->success('DATA_UPDATED');
	}

	public function moveData($params = array())
	{
		if(!$this->setSettings(array(
				'authen' => 'user',
				'requestSource' => $params,
		)))
			return $this->denied();

		$idFrom = $this->request('idFrom');
		$idTo = $this->request('idTo');

		$rsFrom = $this->find($idFrom);
		$rsTo = $this->find($idTo);

		$positionFrom = $rsFrom['position'];
		$positionTo = $rsTo['position'];

		$this->update($idFrom, array('position' => $positionTo));
		$this->update($idTo, array('position' => $positionFrom));

		return $this->success('DATA_UPDATED');
	}

	public function deleteData($params = array())
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

		$id = $this->request('id');
        $rs = $this->find($id);

		$this->delete($id);
		return $this->success('DATA_DELETED');
	}

	public function multipleDeleteData($params = array())
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

		$multipleId = $this->request('multipleId');

    	foreach($multipleId as $id){
			$rs = $this->find($id);

			if(!empty($rs['coverImage']))
				$this->deleteArchive($rs['coverImage']);

			$images = $rs['images'];

			if(!empty($images)) {
				$images = explode(',', $images);

				foreach($images as $image)
					$this->deleteArchive($image);
			}
			$this->delete($id);
		}

		return $this->success('DATA_DELETED');
	}

}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LaporanRekapitulasi extends MY_Model {
    protected $view = 'vi_diklat';
    
    public function readData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();

        // Order

        $orderBy = $this->request('orderBy');
        $reverse = $this->request('reverse');

        $orderStmt = '';

        if (!empty($orderBy)) {
            $orderStmt = "order by $orderBy";

            if (!empty($reverse)) {
                if ($reverse == 1)
                    $orderStmt .= ' desc';
            }
        }

        // Limit
        $page = $this->request('page');
		$count = $this->request('count');

        $limitStmt = '';

      	$rowsCount = $this->getRowsCount($orderStmt);
        
      	$pageCount = 1;

        if (!empty($page) && !empty($count)) {
            $row = ($page * $count) - $count;
            $limitStmt = "limit $row, $count";

            $pageCount = ceil($rowsCount / $count);

            if($pageCount < 1)
                $pageCount = 1;
        }

        // Query

        $jenisDiklatId = $this->request('jenisDiklatId');
        
        $publishStatusId = $this->request('publishStatusId');
        
        if (!empty($jenisDiklatId) )
            $rs = $this->select("where jenisDiklatId = ? and publishStatusId = ? ".$orderStmt.' '.$limitStmt, array($jenisDiklatId, $publishStatusId));
        else
            $rs = $this->select("where publishStatusId = ?  ".$orderStmt.' '.$limitStmt, array($publishStatusId));
        
        $extra = array(
            'rowsCount' => $rowsCount,
            'pageCount' => $pageCount,
        );

		return $this->success('DATA_READ', $rs, $extra);
    }

	public function detailData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();

		$id = $this->request('id');
        
        // Query
        
        $rs = $this->find($id);
        $mItems = $this->model('vi_pendaftaran_diklat');
		$rs['listLaporan'] = $mItems->select('where diklatId = ? order by id', array($id));


        if($rs)
            return $this->success('DATA_READ', $rs);
        else
            return $this->failed('DATA_NOT_FOUND');
    }
    
    public function createExcelDetail($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();
        
		// Excel Settings
		$id = $this->request('id');
		
        $this->load->library('excel');

        $render = 'renderDetail';
        $id = $this->request('id');
        $mPria = $this->model('vi_pendaftaran_diklat');
        $rsAll = $mPria->select('where diklatId = ? order by id', array($id));
        $rsPria = $mPria->select('where jenisKelaminId = 1 and diklatId = ? order by id', array($id));
        $rsWanita = $mPria->select('where jenisKelaminId = 2 and diklatId = ? order by id', array($id));
        
        $rs = $this->find($id);
        $mItems = $this->model('vi_pendaftaran_diklat');
		$rsLaporan = $mItems->select('where diklatId = ? order by id', array($id));
        // Output
        
        if (count($rs) > 0) {
            
			$nameFile='RekapLaporan';
            $fileUrl = $this->$render(
                $nameFile,
                array(
                    'rs' => $rs,
                    'jmlAll' => count($rsAll),
                    'jmlPria' => count($rsPria),
                    'jmlWanita' => count($rsWanita),
                    'rsLaporan' => $rsLaporan,
                    )
            );

            return $this->success('FILE_CREATED', $fileUrl);
        }
        else
            return $this->failed('FILE_CREATE_FAILED');
	}
	
	
    private function renderDetail($template, $data) {

        $rs = $data['rs'];
        $jmlAll = $data['jmlAll'];
        $jmlPria = $data['jmlPria'];
        $jmlWanita = $data['jmlWanita'];
        $rsLaporan = $data['rsLaporan'];

        $reader = PHPExcel_IOFactory::createReader('Excel5');
        $templateUrl = "application/views/excel/$template.xls";

        $excel = $reader->load($templateUrl);
        $sheet = $excel->setActiveSheetIndex(0);

        $i = 0;
        $x =7;
		
		$jenisDiklat = $rs['jenisDiklat'];
		$angkatan = $rs['angkatan'];
		$tahun = $rs['tahun'];
		$lokasi = $rs['lokasi'];
		$sheet->setCellValue("A1", 'REKAPITULASI JUDUL LAPORAN AKTUALISASI');
		$sheet->setCellValue("A2", $jenisDiklat.' '.$angkatan );
		$sheet->setCellValue("A3", 'DI LINGKUNGAN PEMERINTAH '.$lokasi);
		$sheet->setCellValue("A4", 'TAHUN ANGGARAN '.$tahun);
		$styleArrayNo = array(
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				)
                ),
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                )
        );
        
        $styleArrayNama = array(
            'font'  => array(
				'bold'  => true,
            ),
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				)
			)
        );
        
        $styleArray = array(
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				)
			)
		);
		
        foreach($rsLaporan as $row) {
			$x++;
			$i++;

			$sheet->setCellValue("A$x", $i);
			$sheet->setCellValue("B$x", $row['nama']);
			$sheet->setCellValue("C$x", $row['unitKerja']);
			$sheet->setCellValue("D$x", $row['judulLaporan']);
            $sheet->getStyle("A$x")->applyFromArray($styleArrayNo);
            $sheet->getStyle("B$x")->applyFromArray($styleArrayNama);
            $sheet->getStyle("C$x")->applyFromArray($styleArray);
            $sheet->getStyle("D$x")->applyFromArray($styleArray);
        }
        $styleArray1 = array(
            'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			)
        );
        $styleArray2 = array(
			'font'  => array(
				'bold'  => true,
            ),
            'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			)
        );

        $styleArray3 = array(
			'font'  => array(
				'underline' => PHPExcel_Style_Font::UNDERLINE_SINGLE
			));
            
            $x++;   $x++;
        
            $sheet->setCellValue("B$x", 'LAKI - LAKI         =   '.$jmlPria.' PESERTA');
            $sheet->setCellValue("D$x", 'PENANGGUNG JAWAB,');
		    $sheet->getStyle("D$x")->applyFromArray($styleArray1);
            $x++;
            $sheet->setCellValue("B$x", 'PEREMPUAN      =   '.$jmlWanita.' PESERTA');
		    $sheet->getStyle("B$x")->applyFromArray($styleArray3);
            $x++;
            $sheet->setCellValue("B$x", '        JUMLAH     =  '.$jmlAll.' PESERTA');
            $sheet->setCellValue("D$x", 'Kepala Bidang PKM');
		    $sheet->getStyle("D$x")->applyFromArray($styleArray2);
            
        $fileName = $this->encrypt().'.xls';
        $filePath = "asset/tmp/$fileName";

        $writer = PHPExcel_IOFactory::createWriter($excel, 'Excel5');
        $writer->save($filePath);

        $fileUrl = base_url().$filePath;

        return $fileUrl;
    }
    
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MY_Model {
    protected $table = 'tb_user';
    protected $view = 'vi_user';

    public function readData($params = array())
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

        // Order

        $orderBy = $this->request('orderBy');
        $reverse = $this->request('reverse');

        $orderStmt = '';

        if (!empty($orderBy)) {
            $orderStmt = "order by $orderBy";

            if (!empty($reverse)) {
                if ($reverse == 1)
                    $orderStmt .= ' desc';
            }
        }

        // Limit

        $page = $this->request('page');
		$count = $this->request('count');

        $limitStmt = '';

        $rowsCount = $this->getRowsCount();
        $pageCount = 1;

        if (!empty($page) && !empty($count)) {
            $row = ($page * $count) - $count;
            $limitStmt = "limit $row, $count";

            $pageCount = ceil($rowsCount / $count);

            if($pageCount < 1)
                $pageCount = 1;
        }

        // Query

		$tipeUserId = $this->request('tipeUserId');
		$instansiId = $this->request('instansiId');
        $aktivasiId = 1;

        if ($instansiId == 0)
        	$rs = $this->select();
        else {
        	$rs = $this->select("where aktivasiId = ? and tipeUserId = ? ".$orderStmt.' '.$limitStmt, array($aktivasiId, $tipeUserId));
        }
        $extra = array(
            'rowsCount' => $rowsCount,
            'pageCount' => $pageCount,
        );

		return $this->success('DATA_READ', $rs, $extra);
    }

    public function readDataPengajar($params = array())
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

        // Order

        $orderBy = 'name';
        $reverse = 2;

		$instansiId = $this->request('instansiId');
		$tipeUserId = 2;
        $aktivasiId = 1;

        $orderStmt = '';

        if (!empty($orderBy)) {
            $orderStmt = "order by $orderBy";

            if (!empty($reverse)) {
                if ($reverse == 1)
                    $orderStmt .= ' desc';
            }
        }

        // Limit

        $page = $this->request('page');
		$count = $this->request('count');

        $limitStmt = '';
        $rowsCount = $this->getRowsCount("where jenisPengajarId = 7 and aktivasiId = ? and tipeUserId = ? ", array($aktivasiId, $tipeUserId));
       
        $pageCount = 1;

        if (!empty($page) && !empty($count)) {
            $row = ($page * $count) - $count;
            $limitStmt = "limit $row, $count";

            $pageCount = ceil($rowsCount / $count);

            if($pageCount < 1)
                $pageCount = 1;
        }

        // Query


        $rs = $this->select("where jenisPengajarId = 7 and aktivasiId = ? and tipeUserId = ? ".$orderStmt.' '.$limitStmt, array($aktivasiId, $tipeUserId));
        $extra = array(
            'rowsCount' => $rowsCount,
            'pageCount' => $pageCount,
        );

		return $this->success('DATA_READ', $rs, $extra);
    }

    public function readDataAktivasi($params = array())
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

        // Order

        $orderBy = $this->request('orderBy');
        $reverse = $this->request('reverse');

        $orderStmt = '';

        if (!empty($orderBy)) {
            $orderStmt = "order by $orderBy";

            if (!empty($reverse)) {
                if ($reverse == 1)
                    $orderStmt .= ' desc';
            }
        }

        // Limit

        $page = $this->request('page');
		$count = $this->request('count');

        $limitStmt = '';

        $rowsCount = $this->getRowsCount();
        $pageCount = 1;

        if (!empty($page) && !empty($count)) {
            $row = ($page * $count) - $count;
            $limitStmt = "limit $row, $count";

            $pageCount = ceil($rowsCount / $count);

            if($pageCount < 1)
                $pageCount = 1;
        }

        // Query

		$instansiId = $this->request('instansiId');
        $aktivasiId = 2;

        if ($instansiId == 0)
        	$rs = $this->select();
        else {
        	$rs = $this->select("where aktivasiId = ?  ".$orderStmt.' '.$limitStmt, array($aktivasiId));
        }
        $extra = array(
            'rowsCount' => $rowsCount,
            'pageCount' => $pageCount,
        );

		return $this->success('DATA_READ', $rs, $extra);
    }
    
    public function filterData($params = array())
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

        // Query

        $field = $this->request('field');
        $keyword = $this->request('keyword');

        $queryStmt = "where $field like ?";
        $rowsCount = $this->getRowsCount($queryStmt, array("$keyword%"));

        // Limit

        $page = $this->request('page');
		$count = $this->request('count');

        $limitStmt = '';
        $pageCount = 1;

        if (!empty($page) && !empty($count)) {
            $row = ($page * $count) - $count;
            $limitStmt = "limit $row, $count";

            $pageCount = ceil($rowsCount / $count);

            if($pageCount < 1)
                $pageCount = 1;
        }

        $rs = $this->select($queryStmt, array("$keyword%"));

        $extra = array(
            'rowsCount' => $rowsCount,
            'pageCount' => $pageCount,
        );

		return $this->success('DATA_READ', $rs, $extra);
	}

	public function detailData($params = array())
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

		$id = $this->request('id');

        // Query

        $rs = $this->find($id);
        $mItems = $this->model('vi_user_label_mapel');
        $rs['listMapel'] = $mItems->select('where userId = ?  order by id', array($id));
        
        $mItemsLampiran = $this->model('tb_user_kopetensi_lampiran');
        $rs['listLampiran'] = $mItemsLampiran->select("where userId = ?", array($id));

        if($rs)
            return $this->success('DATA_READ', $rs);
        else
            return $this->failed('DATA_NOT_FOUND');
    }

    public function readUserMapelData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();
        
        // Order By
        
        $orderBy = $this->input->post('orderBy');
        $reverse = $this->input->post('reverse');
        
        $orderStmt = '';
        
        if (!empty($orderBy)) {
            $orderStmt = "order by $orderBy";

            if (!empty($reverse)) {
                if ($reverse == 1)
                    $orderStmt .= ' desc';
            }
        }

        // Query
        $labelMapelId = $this->request('labelMapelId');
        $jenisDiklatId = $this->request('jenisDiklatId');

        // Query

        $mItems = $this->model('vi_user_label_mapel');
		$rs = $mItems->select('where jenisDiklatId = ? and labelMapelId = ?  order by id', array($jenisDiklatId,$labelMapelId));
        $rsCount = count($rs);

		return $this->success('DATA_READ', $rs, $rsCount);
    }
    
    private function checkRequest() {
        $name = $this->request('name');
		$email = $this->request('email');
        $tipeUserId = $this->request('tipeUserId');
        $pangkatGolonganId = $this->request('pangkatGolonganId');
        $jabatanId = $this->request('jabatanId');
        $jenisPengajarId = $this->request('jenisPengajarId');

		$error = array();

		if(empty($name))
            $error['name'] = $this->string('Nama beum diisi');
            
        if(empty($email))
			$error['email'] = $this->string('EMAIL_REQUIRED');
		else {
        //    if(!$this->valid->email($email))
        //        $error['email'] = $this->string('INVALID_EMAIL');
        }
        
        if($tipeUserId==2){
            if(empty($pangkatGolonganId))
                $error['pangkatGolonganId'] = $this->string('Pangkat / Golongan belum diisi');

            if(empty($jabatanId))
                $error['jabatanId'] = $this->string('Jabatan belum diisi');

            if(empty($jenisPengajarId))
                $error['jenisPengajarId'] = $this->string('Jenis Pengajar belum diisi');
        } 

        return $error;
    }

	public function createData($params = array())
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

		$error = $this->checkRequest();

        $name = $this->request('name');
		$nip = $this->request('nip');
		$photo = $this->request('photo');
		$email = $this->request('email');
        $password = $this->request('password');
        $retype_password = $this->request('retype_password');
		$privilegesId = $this->request('privilegesId');
		$genderId = $this->request('genderId');
		$dateOfBirth = $this->request('dateOfBirth');
		$mobileNumber = $this->request('mobileNumber');
		$branchId = $this->request('branchId');
        $address = $this->request('address');

        $pangkatGolonganId = $this->request('pangkatGolonganId');
        if(!$pangkatGolonganId)
        $pangkatGolonganId=0;
        $jabatanId = $this->request('jabatanId');
        if(!$jabatanId)
        $jabatanId=0;
        $jenisPengajarId = $this->request('jenisPengajarId');
        if(!$jenisPengajarId)
        $jenisPengajarId=0;
        $tipeUserId = $this->request('tipeUserId');
        $aktivasiId = $this->request('aktivasiId');
        $listMapel = $this->request('listMapel');
        $listLampiran = $this->request('listLampiran');

        $rs = $this->find('email', $email);

		if($rs)
            $error['email'] = $this->string('EMAIL_IS_REG');

        if(empty($password))
			$error['password'] = $this->string('PASSWORD_REQUIRED');
		else {
            if(strlen($password) < 6)
        	   $error['password'] = $this->string('PASSWORD_THAN_SIX');
        }

        if(empty($retype_password))
			$error['retype_password'] = $this->string('RETYPE_PASSWORD_REQUIRED');

        if(!empty($password) && !empty($retype_password)) {

            if(strlen($password) >= 6) {
                if($password != $retype_password)
                    $error['retype_password'] = $this->string('PASSWORD_NOT_MATCH');
            }

        }

        if(empty($privilegesId))
            $error['privilegesId'] = $this->string('Hak akses belum diisi');

		if(count($error) > 0)
			return $this->invalid('PLEASE_CORRECT', $error);

        if(!empty($photo)) {
            if(!$this->moveToArchive($photo))
                return $this->failed('FILE_UPLOAD_FAILED');
        }

        $salt = $this->encrypt();
        $password = $this->encrypt($password, $salt);

		$rs = array(
            'name' => $name,
            'nip' => $nip,
            'photo' => $photo,
            'email' => $email,
            'password' => $password,
            'salt' => $salt,
            'privilegesId' => $privilegesId,
            'genderId' => $genderId,
            'dateOfBirth' => $dateOfBirth,
            'mobileNumber' => $mobileNumber,
            'branchId' => $branchId,
		    'address' => $address,
		    'jabatanId' => $jabatanId,
		    'pangkatGolonganId' => $pangkatGolonganId,
		    'jenisPengajarId' => $jenisPengajarId,
		    'tipeUserId' => $tipeUserId,
		    'aktivasiId' => 1,
		);

        $this->insert($rs);
        $lastId = $this->getLastInsertId();

        $mRs = $this->model('tb_user_label_mapel');
        if($tipeUserId == 2){
            if($listMapel){
                foreach($listMapel as $sub) {
                    $mRs->insert(array(
                            'userId' => $lastId,
                            'labelMapelId' => $sub['labelMapelId'],
                            'jenisDiklatId' => $sub['jenisDiklatId']
                    ));
                }
            }
            if($listLampiran){
                $mLampiran = $this->model('tb_user_kopetensi_lampiran');
                foreach($listLampiran as $row) {
                    $this->moveToArchive($row['lampiran']);
                    $mLampiran->insert(array(
                        'userId' => $lastId,
                        'judul' => $row['judul'],
                        'lampiran' => $row['lampiran']
                    ));
                }
            }
              
        }

		return $this->success('DATA_CREATED');
	}

	public function updateData($params = array())
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

        $id = $this->request('id');
        $name = $this->request('name');
		$nip = $this->request('nip');
		$photo = $this->request('photo');
		$email = $this->request('email');
		$genderId = $this->request('genderId');
        $dateOfBirth = $this->request('dateOfBirth');
		$mobileNumber = $this->request('mobileNumber');
		$branchId = $this->request('branchId');
        $address = $this->request('address');
        
        $pangkatGolonganId = $this->request('pangkatGolonganId');
        if(!$pangkatGolonganId)
        $pangkatGolonganId=0;
        $jabatanId = $this->request('jabatanId');
        if(!$jabatanId)
        $jabatanId=0;
        $jenisPengajarId = $this->request('jenisPengajarId');
        if(!$jenisPengajarId)
        $jenisPengajarId=0;
        $tipeUserId = $this->request('tipeUserId');
        $aktivasiId = $this->request('aktivasiId');
        $listMapel = $this->request('listMapel');
        $listLampiran = $this->request('listLampiran');

		$error = $this->checkRequest();

        $rs = $this->find($id);
        if(!$rs)
            return $this->failed('DATA_NOT_FOUND');

        $last_email = $rs['email'];

        if($email != $last_email) {
            $rsFound = $this->find('email', $email);

            if($rsFound)
                $error['email'] = $this->string('EMAIL_IS_REG');
        }

		if(count($error) > 0)
			return $this->invalid('PLEASE_CORRECT', $error);

        if(!empty($photo)) {
            if($photo != $rs['photo']) {

                $last_photo = $rs['photo'];
                $this->deleteArchive($last_photo);

                if(!$this->moveToArchive($photo))
                    return $this->failed('FILE_UPLOAD_FAILED');
            }
        }

		$rs = array(
            'name' => $name,
            'nip' => $nip,
            'photo' => $photo,
            'email' => $email,
            'genderId' => $genderId,
            'dateOfBirth' => $dateOfBirth,
            'mobileNumber' => $mobileNumber,
            'branchId' => $branchId,
            'address' => $address,
            'jabatanId' => $jabatanId,
            'pangkatGolonganId' => $pangkatGolonganId,
            'jenisPengajarId' => $jenisPengajarId,
            'tipeUserId' => $tipeUserId,
		);

        if($tipeUserId == 2){
            if($listMapel){
                $mRs = $this->model('tb_user_label_mapel');
                $mRs->delete('userId', $id);
    
                foreach($listMapel as $sub) {
                    $mRs->insert(array(
                        'userId' => $id,
                        'labelMapelId' => $sub['labelMapelId'],
                        'jenisDiklatId' => $sub['jenisDiklatId']
                    ));
                }  
            }

            if($listLampiran){
                $mLampiran = $this->model('tb_user_kopetensi_lampiran');
                $rsLampiran = $mLampiran->select("where userId = ?", array($id));
        
                foreach($rsLampiran as $row) {
                    $this->moveToTemp($row['lampiran']);
                }
        
                $mLampiran->delete('userId', $id);
                
                foreach($listLampiran as $row) {
                    $this->moveToArchive($row['lampiran']);
                    $mLampiran->insert(array(
                        'userId' => $id,
                        'judul' => $row['judul'],
                        'lampiran' => $row['lampiran']
                    ));
                }
            }
            
        }

		$this->update($id, $rs);
		return $this->success('DATA_UPDATED');
	}

    public function updatePassword($params = array())
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

        $id = $this->request('id');

        if(empty($id))
            return $this->failed('ID_REQUIRED');

        $rs = $this->find($id);

        if(!$rs)
            return $this->failed('DATA_NOT_FOUND');

        $error = array();

        $new_password = $this->request('new_password');
        $retype_password = $this->request('retype_password');

        if(empty($new_password))
			$error['new_password'] = $this->string('NEW_PASSWORD_REQUIRED');
		else {
            if(strlen($new_password) < 6)
        	   $error['new_password'] = $this->string('PASSWORD_THAN_SIX');
        }

        if(empty($retype_password))
			$error['retype_password'] = $this->string('RETYPE_PASSWORD_REQUIRED');

        if(!empty($new_password) && !empty($retype_password)) {

            if(strlen($new_password) >= 6) {
                if($new_password != $retype_password)
                    $error['retype_password'] = $this->string('PASSWORD_NOT_MATCH');
            }

        }

		if(count($error) > 0)
			return $this->invalid('PLEASE_CORRECT', $error);

        $salt = $this->encrypt();
        $new_password = $this->encrypt($new_password, $salt);

        $rs = array(
            'password' => $new_password,
            'salt' => $salt,
		);

        $this->update($id, $rs);
		return $this->success('PASSWORD_CHANGED');
	}

    public function updatePrivileges($params = array())
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

        $id = $this->request('id');

        if(empty($id))
            return $this->failed('ID_REQUIRED');

        $rs = $this->find($id);

        if(!$rs)
            return $this->failed('DATA_NOT_FOUND');

        $error = array();

        $privilegesId = $this->request('privilegesId');


		if(count($error) > 0)
			return $this->invalid('PLEASE_CORRECT', $error);

        $rs = array(
            'privilegesId' => $privilegesId,
        );

        $this->update($id, $rs);
        return $this->success('PRIVILEGES_SAVED');
    }
    
    public function updateActivation($params = array())
	{
		if(!$this->setSettings(array(
				'authen' => 'user',
				'requestSource' => $params,
		)))
			return $this->denied();

		$id = $this->request('id');

		if(empty($id))
			return $this->failed('ID_REQUIRED');

		$rs = $this->find($id);

		if(!$rs)
			return $this->failed('DATA_NOT_FOUND');

		$error = array();

		$aktivasiId = $this->request('aktivasiId');


		if(count($error) > 0)
			return $this->invalid('PLEASE_CORRECT', $error);

		$rs = array(
            'aktivasiId' => $aktivasiId,
		);

		$this->update($id, $rs);
		return $this->success('ACTIVATION_SAVED');
	}

	public function deleteData($params = array())
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

		$id = $this->request('id');
        $rs = $this->find($id);

        if(!empty($rs['photo']))
            $this->deleteArchive($rs['photo']);

		$this->delete($id);

		return $this->success('DATA_DELETED');
	}

	public function multipleDeleteData($params = array())
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

            $multipleId = $this->request('multipleId');
            $mUserMapel = $this->model('tb_user_label_mapel');
            $mItems = $this->model('tb_bahan_jadwal_diklat');
            $mItems1 = $this->model('tb_user_login');
            $mItems2 = $this->model('tb_bahan_jadwal_diklat_pengajar_detail');
            $mItemsLampiran = $this->model('tb_user_kopetensi_lampiran');
            $error = array();
            $i = 0;

        foreach($multipleId as $id){
            $rs = $this->find($id);
            $userId = $id;

            $i++;
            $rsItems = $mItems->select("where pengajarDiklatId = ?", array($userId));
            if($rsItems){
                if(count($error) == 0)
                    $error['0'] = 'Data dibawah ini tidak bisa di hapus karena terkait dengan Data lain';
                
                $rs = $this->find($id);
                $error[$i] = $rs['name'];        
            }

            $rsItems1 = $mItems1->select("where userId = ?", array($userId));
            if($rsItems1){
                if(count($error) == 0)
                $error['0'] = 'Data dibawah ini tidak bisa di hapus karena terkait dengan Data lain';
                
                $rs = $this->find($id);
                $error[$i] = $rs['name'];
            }

            $rsItems2 = $mItems2->select("where pengajarDiklatId = ?", array($userId));
            if($rsItems2){
                if(count($error) == 0)
                $error['0'] = 'Data dibawah ini tidak bisa di hapus karena terkait dengan Data lain';
                
                $rs = $this->find($id);
                $error[$i] = $rs['name'];
            }
        
            if(!$rsItems && !$rsItems1 && !$rsItems2 ){
                $rs = $this->find($id);
                $rsLampiran = $mItemsLampiran->select("where userId = ?", array($id));

                foreach($rsLampiran as $row) {
                    $this->deleteArchive($row['lampiran']);
                }
    
                $mItemsLampiran->delete('userId', $id);

                if(!empty($rs['photo']))
                    $this->deleteArchive($rs['photo']);
                    
			        $mUserMapel->delete('userId', $id);
                    $this->delete($id);
            }

        }

        if(count($error) > 0){
        return $this->failed('PLEASE_CORRECT', $error);
        exit;
        }else{
        return $this->success('DATA_DELETED');
        }

	}

    public function createPDFList($params = array())
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

        // PDF Settings

        $this->load->library('pdf');
        $this->pdf->setSettings(array(
            'strings' => $this->config->item('strings'),
            'title' => 'USER_LIST',
            'pageOrientation' => 'P',
        ));

        // Query

        $rs = $this->select();

        // Output

        if (count($rs) > 0) {
            $fileUrl = $this->pdf->render(
                'UserList',
                array('rs' => $rs)
            );

            return $this->success('FILE_CREATED', $fileUrl);
        }
        else
            return $this->failed('FILE_CREATE_FAILED');
    }

    public function deleteDataLampiran($params = array())
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

        $lampiran = $this->request('lampiran');
        $this->deleteTemp($lampiran);
        
		return $this->success('DATA_DELETED');
	}

    public function createPDFDetail($params = array())
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

        // PDF Settings

        $this->load->library('pdf');
        $this->pdf->setSettings(array(
            'strings' => $this->config->item('strings'),
            'title' => 'USER_DETAIL',
            'pageOrientation' => 'P',
        ));

        // Request

        $id = $this->request('id');

        // Query

        $row = $this->find($id);

        // Output

        if (!empty($row)) {
            $fileUrl = $this->pdf->render(
                'UserDetail',
                $row
            );

            return $this->success('FILE_CREATED', $fileUrl);
        }
        else
            return $this->failed('FILE_CREATE_FAILED');
    }

    public function createExcelListPengajar($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();
        
        
        // Excel Settings
        
        $this->load->library('excel');

		$render = 'renderDetailPengajar';
        $rs = $this->select("where aktivasiId = 1 and tipeUserId = 2 order by name asc ");
		
        if (count($rs) > 0) {
            // return $this->invalid('DATA_READ', $data);
            $nameFile='PengajarList';
            $fileUrl = $this->$render(
                $nameFile,
                array(
                    'rs' => $rs,
                    )
            );

            return $this->success('FILE_CREATED', $fileUrl);
        }
        else
            return $this->failed('FILE_CREATE_FAILED');
    }

    private function renderDetailPengajar($template, $data) {

        $rs = $data['rs'];

        $reader = PHPExcel_IOFactory::createReader('Excel5');
        $templateUrl = "application/views/excel/$template.xls";

        $excel = $reader->load($templateUrl);
        $sheet = $excel->setActiveSheetIndex(0);

        $i = 0;
        $x =4;
		
		
		$styleArray = array(
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				)
			)
		);
		$mItems1 = $this->model('vi_user_label_mapel');
        $ab=0;
        $noUrut=0;
        foreach($rs as $row) {
            $x++;
            $noUrut++;
			$ab++;

				$userId = $row['id'];
				$rsIsi = $mItems1->select('where userId = ? order by labelMapel ', array($userId));
                if($rsIsi){
                    $jumlah =count($rsIsi);
                    $jmlRow = ((int)$x-1) + (int)$jumlah;
                    $sheet->mergeCells("A$x".":A$jmlRow");
                    $sheet->mergeCells("B$x".":B$jmlRow");

                    $sheet->getStyle("A$x".":A$jmlRow")->applyFromArray($styleArray);
                    $sheet->getStyle("B$x".":B$jmlRow")->applyFromArray($styleArray);
                    
                    $sheet->getStyle("C$x".":D$jmlRow")->applyFromArray($styleArray);
                    $sheet->setCellValue("A$x", $noUrut);
                    $sheet->setCellValue("B$x", $row['name']);
                    $x=(int)$x-1;
                    foreach($rsIsi as $row1) {
                        $x++;
                        $sheet->setCellValue("C$x", $row1['namaSingkat']);
                        $sheet->setCellValue("D$x", $row1['labelMapel']);
                    }
                }else{
                    $sheet->setCellValue("A$x", $noUrut);
                    $sheet->setCellValue("B$x", $row['name']);
                    $sheet->getStyle("A$x")->applyFromArray($styleArray);
                    $sheet->getStyle("B$x")->applyFromArray($styleArray);
                    $sheet->getStyle("C$x")->applyFromArray($styleArray);
                }
                
				
				
		}
		$styleLeft = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
			)
		);


        $fileName = $this->encrypt().'.xls';
        $filePath = "asset/tmp/$fileName";

        $writer = PHPExcel_IOFactory::createWriter($excel, 'Excel5');
        $writer->save($filePath);

        $fileUrl = base_url().$filePath;

        return $fileUrl;
    }
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends MY_Model {
  protected $table = 'tb_user';
  protected $view = 'vi_user';

  public function login($params = array())
  {
    if(!$this->setSettings(array(
      'authen' => 'free',
      'requestSource' => $params,
    )))
      return $this->denied();

      $files = glob("asset/tmp/*");

      foreach($files as $file){
        $lastModifiedTime = filemtime($file);
        $currentTime = time();
        $timeDiff = abs($currentTime - $lastModifiedTime)/(60*60);
        if (is_file($file) && $timeDiff > 30)
            unlink($file);
      }

    $email = $this->request('email');
    $password = $this->request('password');

    $error = array();

    if (empty($email))
      $error['email'] = $this->string('EMAIL_REQUIRED');

    if (empty($password))
      $error['password'] = $this->string('PASSWORD_REQUIRED');

    if (count($error) > 0)
      return $this->invalid('PLEASE_CORRECT', $error);

    if ($email == $this->config->item('adminEmail')) {
      // Super Admin

      if ($password == $this->config->item('adminPassword')) {

        $userId = 0;
        $namaUser = 'Noname';
        $auth = $this->encrypt();
        $type = 'admin';
        $menuAccess = 'All';
        $panelAccess = 'All';

        // Kick Out if already Login

        $mUserLog = $this->model('vi_user_log');
        $rowUserLog = $mUserLog->single('where userId = ? and stillInsideId = ?', array($userId, 1));

        if (!empty($rowUserLog)) {
          $userLoginId = $rowUserLog['userLoginId'];

          $mUserLogout = $this->model('tb_user_logout');
          $mUserLogout->insert(array(
            'userLoginId' => $userLoginId,
            'logoutOn' => date('Y-m-d h:i:s'),
          ));
        }

        // Stamp In

        $mUserLogin = $this->model('tb_user_login');
        $mUserLogin->insert(array(
          'userId' => $userId,
          'auth' => $auth,
          'loginOn' => date('Y-m-d h:i:s'),
        ));

        return $this->success('LOGIN_SUCCESS', array(
          'auth' => $auth,
          'type' => $type,
          'userId' => $userId,
          'namaUser' => $namaUser,
          'menuAccess' => $menuAccess,
          'panelAccess' => $panelAccess,
          'instansiId' => 0,
        ));
      }
      else
        return $this->invalid('LOGIN_FAILED', array('password' => $this->string('INVALID_PASSWORD')));
    }
    else {
      // General User

      $rowUser = $this->find('email', $email);

      if (!empty($rowUser)) {
        $password = $this->encrypt($password, $rowUser['salt']);

        if ($password == $rowUser['password']) {

          $userId = $rowUser['id'];
          $namaUser = $rowUser['name'];
          $auth = $this->encrypt();
          $type = 'user';
          $menuAccess = $rowUser['menuAccess'];
          $panelAccess = $rowUser['panelAccess'];

          // Kick Out if already Login

          $mUserLog = $this->model('vi_user_log');
          $rowUserLog = $mUserLog->single('where userId = ? and stillInsideId = ?', array($userId, 1));

          if (!empty($rowUserLog)) {
            $userLoginId = $rowUserLog['userLoginId'];

            $mUserLogout = $this->model('tb_user_logout');
            $mUserLogout->insert(array(
              'userLoginId' => $userLoginId,
              'logoutOn' => date('Y-m-d h:i:s'),
            ));
          }

          // Stamp In

          $mUserLogin = $this->model('tb_user_login');
          $mUserLogin->insert(array(
            'userId' => $userId,
            'auth' => $auth,
            'loginOn' => date('Y-m-d h:i:s'),
          ));

          return $this->success('LOGIN_SUCCESS', array(
            'auth' => $auth,
            'userId' => $userId,
            'type' => $type,
          	'namaUser' => $namaUser,
            'menuAccess' => $menuAccess,
            'panelAccess' => $panelAccess,
            'instansiId' => 1,
          ));
        }
        else
          return $this->invalid('LOGIN_FAILED', array('password' => $this->string('INVALID_PASSWORD')));
      }
      else
        return $this->invalid('LOGIN_FAILED', array('password' => $this->string('USER_NOT_FOUND')));
    }
  }

  public function logout($params = array())
  {
    if(!$this->setSettings(array(
      'authen' => 'user',
      'requestSource' => $params,
    )))
      return $this->denied();

    $auth = $this->request('auth');

    $mUserLog = $this->model('vi_user_log');
    $rowUserLog = $mUserLog->find('auth', $auth);

    if (!empty($rowUserLog)) {
      $userLoginId = $rowUserLog['userLoginId'];

      $mUserLogout = $this->model('tb_user_logout');
      $mUserLogout->insert(array(
        'userLoginId' => $userLoginId,
        'logoutOn' => date('Y-m-d h:i:s'),
      ));

      return $this->success('LOGOUT_SUCCESS');
    }
    else
      return $this->failed('LOGOUT_FAILED');
  }

  public function detailData($params = array())
  {
    if(!$this->setSettings(array(
      'authen' => 'user',
      'requestSource' => $params,
    )))
      return $this->denied();

    $auth = $this->request('auth');

    $mUserLog = $this->model('vi_user_log');
    $rowUserLog = $mUserLog->find('auth', $auth);

    $userId = $rowUserLog['userId'];

    if ($userId == 0)
      return $this->success('DATA_READ', array('name' => $this->config->item('adminName')));
    else {
      $mUser = $this->model('vi_user');
      $rsUser = $mUser->find($userId);
      $mItems = $this->model('vi_user_label_mapel');
		  $rsUser['listMapel'] = $mItems->select('where userId = ?  order by id', array($userId));

      $mItemsLampiran = $this->model('tb_user_kopetensi_lampiran');
      $rsUser['listLampiran'] = $mItemsLampiran->select("where userId = ?", array($userId));

      return $this->success('DATA_READ', $rsUser);
    }
  }

  private function checkRequest() {
    $name = $this->request('name');
    $email = $this->request('email');
    $photo = $this->request('photo');

    $error = array();

    if(empty($name))
      $error['name'] = $this->string('Nama belum diisi');
    

    if(empty($email))
      $error['email'] = $this->string('EMAIL_REQUIRED');
    else {
      // if(!$this->valid->email($email))
      //   $error['email'] = $this->string('INVALID_EMAIL');
    }

    return $error;
  }

  public function saveData($params = array())
  {
    if(!$this->setSettings(array(
      'authen' => 'user',
      'requestSource' => $params,
    )))
      return $this->denied();

    $error = $this->checkRequest();

    // Get User Id by Auth Key
    $auth = $this->request('auth');
    $mUserLog = $this->model('vi_user_log');
    $rowUserLog = $mUserLog->find('auth', $auth);
    $id = $rowUserLog['userId'];

    $name = $this->request('name');
		$nip = $this->request('nip');
		$photo = $this->request('photo');
		$email = $this->request('email');
		$genderId = $this->request('genderId');
    $dateOfBirth = $this->request('dateOfBirth');
    $mobileNumber = $this->request('mobileNumber');
    $address = $this->request('address');
    $pangkatGolonganId = $this->request('pangkatGolonganId');
    if(!$pangkatGolonganId)
    $pangkatGolonganId=0;
    $jabatanId = $this->request('jabatanId');
    if(!$jabatanId)
    $jabatanId=0;
    $jenisPengajarId = $this->request('jenisPengajarId');
    if(!$jenisPengajarId)
    $jenisPengajarId=0;
    $listMapel = $this->request('listMapel');
    $listLampiran = $this->request('listLampiran');

    $rs = $this->find($id);
    $tipeUserId =$rs['tipeUserId'];
    if(!$rs)
      return $this->failed('DATA_NOT_FOUND');

    $last_email = $rs['email'];

    if($email != $last_email) {
      $rsFound = $this->find('email', $email);

      if($rsFound)
        $error['email'] = $this->string('EMAIL_IS_REG');
    }

    if(count($error) > 0)
      return $this->invalid('PLEASE_CORRECT', $error);

    if(!empty($photo)) {
      if($photo != $rs['photo']) {

        $last_photo = $rs['photo'];
        $this->deleteArchive($last_photo);

        if(!$this->moveToArchive($photo))
        return $this->failed('FILE_UPLOAD_FAILED');
      }
    }

    $rs = array(
      'name' => $name,
      'nip' => $nip,
      'photo' => $photo,
      'email' => $email,
      'genderId' => $genderId,
      'dateOfBirth' => $dateOfBirth,
      'mobileNumber' => $mobileNumber,
      'address' => $address,
      'jabatanId' => $jabatanId,
      'pangkatGolonganId' => $pangkatGolonganId,
      'jenisPengajarId' => $jenisPengajarId,
    );

    if($tipeUserId == 2){
      $mRs = $this->model('tb_user_label_mapel');
      $mRs->delete('userId', $id);

      if($listMapel){
        foreach($listMapel as $sub) {
            $mRs->insert(array(
                'userId' => $id,
                'labelMapelId' => $sub['labelMapelId'],
                'jenisDiklatId' => $sub['jenisDiklatId']
            ));
        }  
      }

      if($listLampiran){
        $mLampiran = $this->model('tb_user_kopetensi_lampiran');
        $rsLampiran = $mLampiran->select("where userId = ?", array($id));

        foreach($rsLampiran as $row) {
            $this->moveToTemp($row['lampiran']);
        }

        $mLampiran->delete('userId', $id);
        
        foreach($listLampiran as $row) {
            $this->moveToArchive($row['lampiran']);
            $mLampiran->insert(array(
                'userId' => $id,
                'judul' => $row['judul'],
                'lampiran' => $row['lampiran']
            ));
        }
    }
  }

    $this->update($id, $rs);
    return $this->success('DATA_UPDATED');
  }

  public function updatePassword($params = array())
  {
    if(!$this->setSettings(array(
      'authen' => 'user',
      'requestSource' => $params,
    )))
      return $this->denied();

    // Get User Id by Auth Key
    $auth = $this->request('auth');
    $mUserLog = $this->model('vi_user_log');
    $rowUserLog = $mUserLog->find('auth', $auth);
    $id = $rowUserLog['userId'];

    $rs = $this->find($id);

    if(!$rs)
      return $this->failed('DATA_NOT_FOUND');

    $error = array();

    $old_password = $this->request('old_password');
    $new_password = $this->request('new_password');
    $retype_password = $this->request('retype_password');

    if(empty($old_password))
      $error['old_password'] = $this->string('OLD_PASSWORD_REQUIRED');
    else {

      $encrypted_old_password = $this->encrypt($old_password, $rs['salt']);

      if($encrypted_old_password != $rs['password'])
      $error['old_password'] = $this->string('INVALID_OLD_PASSWORD');
    }

    if(empty($new_password))
      $error['new_password'] = $this->string('NEW_PASSWORD_REQUIRED');
    else {
      if(strlen($new_password) < 6)
        $error['new_password'] = $this->string('PASSWORD_THAN_SIX');
    }

    if(empty($retype_password))
      $error['retype_password'] = $this->string('RETYPE_PASSWORD_REQUIRED');

    if(!empty($new_password) && !empty($retype_password)) {

      if(strlen($new_password) >= 6) {
        if($new_password != $retype_password)
          $error['retype_password'] = $this->string('PASSWORD_NOT_MATCH');
      }

    }

    if(count($error) > 0)
      return $this->invalid('PLEASE_CORRECT', $error);

    $salt = $this->encrypt();
    $new_password = $this->encrypt($new_password, $salt);

    $rs = array(
      'password' => $new_password,
      'salt' => $salt,
    );

    $this->update($id, $rs);
    return $this->success('PASSWORD_CHANGED');
  }

  public function createPDFDetail($params = array())
  {
    if(!$this->setSettings(array(
      'authen' => 'user',
      'requestSource' => $params,
    )))
      return $this->denied();

    // PDF Settings

    $this->load->library('pdf');
    $this->pdf->setSettings(array(
      'strings' => $this->config->item('strings'),
      'title' => 'USER_DETAIL',
      'pageOrientation' => 'P',
    ));

    // Get User Id by Auth Key
    $auth = $this->request('auth');
    $mUserLog = $this->model('vi_user_log');
    $rowUserLog = $mUserLog->find('auth', $auth);
    $id = $rowUserLog['userId'];

    // Query

    $row = $this->find($id);

    // Output

    if (!empty($row)) {
      $fileUrl = $this->pdf->render(
        'UserDetail',
        $row
      );

      return $this->success('FILE_CREATED', $fileUrl);
    }
    else
      return $this->failed('FILE_CREATE_FAILED');
  }

}

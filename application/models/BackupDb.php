<?php
defined('BASEPATH') or exit('No direct script access allowed');

class BackupDb extends MY_Model
{

    public function _backupDb($params = array())
    {
        if (!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        ))) {
            return $this->denied();
        }

        $this->load->dbutil();
        $aturan = array(
            'format'      => 'zip',
            'filename'    => 'my_db_backup_diklat.sql'
        );
        $backup = $this->dbutil->backup($aturan);
        $namaDatabase = 'dbdiklat-backup-on-' . date("Y-m-d-H-i-s") . '.zip';
        $simpan = 'asset/tmp/' . $namaDatabase;
        $this->load->helper('file');
        write_file($simpan, $backup);

        $fileUrl = base_url() . $simpan;

        return $this->success('FILE_CREATED', $fileUrl);
    }
}

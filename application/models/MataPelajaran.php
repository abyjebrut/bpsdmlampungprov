<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MataPelajaran extends MY_Model {
    protected $table = 'tb_mata_pelajaran';
    protected $view = 'vi_mata_pelajaran';
    
    public function readData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();

        // Order

        $orderBy = $this->request('orderBy');
        $reverse = $this->request('reverse');

        $orderStmt = '';

        if (!empty($orderBy)) {
            $orderStmt = "order by $orderBy";

            if (!empty($reverse)) {
                if ($reverse == 1)
                    $orderStmt .= ' desc';
            }
        }

        // Limit
        $page = $this->request('page');
		$count = $this->request('count');

        $limitStmt = '';

      	$rowsCount = $this->getRowsCount($orderStmt);
        
      	$pageCount = 1;

        if (!empty($page) && !empty($count)) {
            $row = ($page * $count) - $count;
            $limitStmt = "limit $row, $count";

            $pageCount = ceil($rowsCount / $count);

            if($pageCount < 1)
                $pageCount = 1;
        }

        // Query

        $jenisDiklatId = $this->request('jenisDiklatId');
        $publishStatusId = $this->request('publishStatusId');
        if (!empty($jenisDiklatId))
            $rs = $this->select("where publishStatusId = ? and jenisDiklatId = ? ".$orderStmt.' '.$limitStmt, array($publishStatusId,$jenisDiklatId));
        else
            $rs = $this->select("where publishStatusId = ? ".$orderStmt.' '.$limitStmt, array($publishStatusId));
        $extra = array(
            'rowsCount' => $rowsCount,
            'pageCount' => $pageCount,
        );

		return $this->success('DATA_READ', $rs, $extra);
    }

    public function readDataAsli($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();

        // Order

        $orderBy = $this->request('orderBy');
        $reverse = $this->request('reverse');

        $orderStmt = '';

        if (!empty($orderBy)) {
            $orderStmt = "order by $orderBy";

            if (!empty($reverse)) {
                if ($reverse == 1)
                    $orderStmt .= ' desc';
            }
        }

        // Limit
        $page = $this->request('page');
		$count = $this->request('count');

        $limitStmt = '';

      	$rowsCount = $this->getRowsCount($orderStmt);
        
      	$pageCount = 1;

        if (!empty($page) && !empty($count)) {
            $row = ($page * $count) - $count;
            $limitStmt = "limit $row, $count";

            $pageCount = ceil($rowsCount / $count);

            if($pageCount < 1)
                $pageCount = 1;
        }

        // Query

        $jenisDiklatId = $this->request('jenisDiklatId');
        $publishStatusId = $this->request('publishStatusId');
        if (!empty($jenisDiklatId))
            $rs = $this->select("where publishStatusId = ? and jenisDiklatId = ? ".$orderStmt.' '.$limitStmt, array($publishStatusId,$jenisDiklatId));
        else
            $rs = $this->select("where (jenisDiklatId = 6 or jenisDiklatId = 8 or jenisDiklatId = 9 or jenisDiklatId = 10 ) and publishStatusId = ? ".$orderStmt.' '.$limitStmt, array($publishStatusId));
        $extra = array(
            'rowsCount' => $rowsCount,
            'pageCount' => $pageCount,
        );

		return $this->success('DATA_READ', $rs, $extra);
    }

    public function readDataFungsional($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();

        // Order

        $orderBy = $this->request('orderBy');
        $reverse = $this->request('reverse');

        $orderStmt = '';

        if (!empty($orderBy)) {
            $orderStmt = "order by $orderBy";

            if (!empty($reverse)) {
                if ($reverse == 1)
                    $orderStmt .= ' desc';
            }
        }

        // Limit
        $page = $this->request('page');
		$count = $this->request('count');

        $limitStmt = '';

      	$rowsCount = $this->getRowsCount($orderStmt);
        
      	$pageCount = 1;

        if (!empty($page) && !empty($count)) {
            $row = ($page * $count) - $count;
            $limitStmt = "limit $row, $count";

            $pageCount = ceil($rowsCount / $count);

            if($pageCount < 1)
                $pageCount = 1;
        }

        // Query

        $jenisDiklatId = $this->request('jenisDiklatId');
        $publishStatusId = $this->request('publishStatusId');
        if (!empty($jenisDiklatId))
            $rs = $this->select("where publishStatusId = ? and jenisDiklatId = ? ".$orderStmt.' '.$limitStmt, array($publishStatusId,$jenisDiklatId));
        else
            $rs = $this->select("where (jenisDiklatId = 11 or jenisDiklatId = 12 ) and publishStatusId = ? ".$orderStmt.' '.$limitStmt, array($publishStatusId));
        $extra = array(
            'rowsCount' => $rowsCount,
            'pageCount' => $pageCount,
        );

		return $this->success('DATA_READ', $rs, $extra);
    }

    public function readFilterLabelMapel($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();

        // Order

        $orderBy = 'mataPelajaran';
        $reverse = $this->request('reverse');

        $orderStmt = '';

        if (!empty($orderBy)) {
            $orderStmt = "order by $orderBy";

            if (!empty($reverse)) {
                if ($reverse == 1)
                    $orderStmt .= ' desc';
            }
        }

        // Limit
        $page = $this->request('page');
		$count = $this->request('count');

        $limitStmt = '';

      	$rowsCount = $this->getRowsCount($orderStmt);
        
      	$pageCount = 1;

        if (!empty($page) && !empty($count)) {
            $row = ($page * $count) - $count;
            $limitStmt = "limit $row, $count";

            $pageCount = ceil($rowsCount / $count);

            if($pageCount < 1)
                $pageCount = 1;
        }

        // Query
        
        $mItems = $this->model('vi_mata_pelajaran');
        // $mItems = $this->model('vi_filter_label_mapel');

        $jenisDiklatId = $this->request('jenisDiklatId');
        $publishStatusId = 2;
        // if (!empty($jenisDiklatId))
        //     $rs = $mItems->select("where publishStatusId = ? and jenisDiklatId = ? ".$orderStmt.' '.$limitStmt, array($publishStatusId,$jenisDiklatId));
        // else
        //     $rs = $mItems->select("where publishStatusId = ? ".$orderStmt.' '.$limitStmt, array($publishStatusId));
        if (!empty($jenisDiklatId))
            $rs = $mItems->query("select * from vi_mata_pelajaran where publishStatusId = ? and jenisDiklatId = ? GROUP BY jenisDiklatId, labelMapelId ".$orderStmt.' '.$limitStmt, array($publishStatusId,$jenisDiklatId));
        else
            $rs = $mItems->query("select * from vi_mata_pelajaran where publishStatusId = ? GROUP BY jenisDiklatId, labelMapelId ".$orderStmt.' '.$limitStmt, array($publishStatusId));
        
            $extra = array(
            'rowsCount' => $rowsCount,
            'pageCount' => $pageCount,
        );

		return $this->success('DATA_READ', $rs, $extra);
    }

    
    public function filterData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();
        
        // Query

        $field = $this->request('field');
        $keyword = $this->request('keyword');

        $queryStmt = "where $field like ?";
        $rowsCount = $this->getRowsCount($queryStmt, array("$keyword%"));

        // Limit

        $page = $this->request('page');
		$count = $this->request('count');

        $limitStmt = '';
        $pageCount = 1;

        if (!empty($page) && !empty($count)) {
            $row = ($page * $count) - $count;
            $limitStmt = "limit $row, $count";

            $pageCount = ceil($rowsCount / $count);

            if($pageCount < 1)
                $pageCount = 1;
        }

        $rs = $this->select($queryStmt, array("$keyword%"));

        $extra = array(
            'rowsCount' => $rowsCount,
            'pageCount' => $pageCount,
        );

		return $this->success('DATA_READ', $rs, $extra);
	}

	public function detailData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();

		$id = $this->request('id');
        
        // Query
        
		$rs = $this->find($id);

        if($rs)
            return $this->success('DATA_READ', $rs);
        else
            return $this->failed('DATA_NOT_FOUND');
	}

    private function checkRequest() {

        $noUrut = $this->request('noUrut');
        $labelMapelId = $this->request('labelMapelId');
        $jamMulai = $this->request('jamMulai');
        $jamSelesai = $this->request('jamSelesai');
        $jenisDiklatId = $this->request('jenisDiklatId');
        $tipeMataPelajaranId = $this->request('tipeMataPelajaranId');
        $jumlahHari = $this->request('jumlahHari');
        $tipeWaktuId = $this->request('tipeWaktuId');
        $tipeTanggalId = $this->request('tipeTanggalId');
        $jumlahJp = $this->request('jumlahJp');
        
		$error = array();

        if(empty($jenisDiklatId))
            $error['jenisDiklatId'] = $this->string('Jenis Diklat diperlukan');
            
		// if(empty($noUrut))
        //     $error['noUrut'] = $this->string('No Urut diperlukan');
            
        if(empty($labelMapelId))
			$error['labelMapelId'] = $this->string('Mata Pelajaran diperlukan');

        if($tipeWaktuId == 2){
            if  (empty($jumlahHari) || $jumlahHari==0)
                $error['jumlahHari'] = $this->string('Jumlah Hari diperlukan');
                
                if  (empty($tipeTanggalId))
			    $error['tipeTanggalId'] = $this->string('Tipe Tanggal diperlukan');
        }else{
            if($tipeMataPelajaranId==1){
                if  (empty($jumlahJp) || $jumlahJp==0)
                $error['jumlahJp'] = $this->string('Jumlah JP diperlukan');
            }
        }

        return $error;
    }

    public function lihatData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

        $error = array();

		$publishStatusId = $this->request('publishStatusId');
		$noUrut = $this->request('noUrut');
        $labelMapelId = $this->request('labelMapelId');
        $jamMulai = $this->request('jamMulai');
        $jamSelesai = $this->request('jamSelesai');
        $jenisDiklatId = $this->request('jenisDiklatId');
        $tipeMataPelajaranId = $this->request('tipeMataPelajaranId');
        $jumlahHari = $this->request('jumlahHari');
        if(!$jumlahHari)
        $jumlahHari=0;
        $tipeWaktuId = $this->request('tipeWaktuId');
        $tipeTanggalId = $this->request('tipeTanggalId');
        if(!$tipeTanggalId)
        $tipeTanggalId=0;
        $nilaiJamMulai = $this->request('nilaiJamMulai');
        $nilaiJamSelesai = $this->request('nilaiJamSelesai');
        $jumlahJp = $this->request('jumlahJp');
        
        
        // if ($tipeMataPelajaranId == 1 && $publishStatusId == 2){
        // if ($publishStatusId == 2){
        //     $rs = $this->select("where publishStatusId = 2 and jenisDiklatId = ? and labelMapelId = ? and noUrut = ? and nilaiJamSelesai > ? ", array($jenisDiklatId,$labelMapelId,$noUrut, $nilaiJamMulai));

	    // 	if($rs)
        //         $error['labelMapelId'] = $this->string('Mata Pelajaran sudah terdaftar');
                
        //     if($tipeWaktuId == 1){
        //         $rs = $this->select("where publishStatusId = 2 and tipeWaktuId = 1  and jenisDiklatId = ? AND noUrut = ? AND ((jamMulai1 between ? AND ?) OR  (jamSelesai1 between ? and ?) OR (jamMulai1 < ? AND jamSelesai1 > ?))", array($jenisDiklatId, $noUrut, $jamMulai, $jamSelesai, $jamMulai, $jamSelesai, $jamMulai, $jamSelesai));
    
        //     if($rs)
        //         $error['jamMulai'] = $this->string('Jam Mulai Salah'.' '.$jamMulai.' '.$jamSelesai);
        //     }
        // }
        
        
		// if(count($error) > 0)
		// 	return $this->invalid('PLEASE_CORRECT', $error);

        $rs = $this->select("where publishStatusId = 2 and tipeWaktuId = 1  and jenisDiklatId = ? AND noUrut = ? AND ((jamMulai1 between ? AND ?) OR  (jamSelesai1 between ? and ?) OR (jamMulai1 < ? AND jamSelesai1 > ?))", array($jenisDiklatId, $noUrut, $jamMulai, $jamSelesai, $jamMulai, $jamSelesai, $jamMulai, $jamSelesai));
        
        foreach($rs as $row) {
            $rsa = array(
                'publishStatusId' => 1,
            );
    
            $this->update($row['id'], $rsa);
        }

		return $this->success('Lihat Berhasil');
    }
    

	public function createData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

		$error = $this->checkRequest();

		$publishStatusId = $this->request('publishStatusId');
		$noUrut = $this->request('noUrut');
        $labelMapelId = $this->request('labelMapelId');
        $jamMulai = $this->request('jamMulai');
        $jamSelesai = $this->request('jamSelesai');
        $jenisDiklatId = $this->request('jenisDiklatId');
        $tipeMataPelajaranId = $this->request('tipeMataPelajaranId');
        $jumlahHari = $this->request('jumlahHari');
        if(!$jumlahHari)
        $jumlahHari=0;
        $tipeWaktuId = $this->request('tipeWaktuId');
        $tipeTanggalId = $this->request('tipeTanggalId');
        if(!$tipeTanggalId)
        $tipeTanggalId=0;
        $nilaiJamMulai = $this->request('nilaiJamMulai');
        $nilaiJamSelesai = $this->request('nilaiJamSelesai');
        $jumlahJp = $this->request('jumlahJp');
        
        
        // if ($tipeMataPelajaranId == 1 && $publishStatusId == 2){
        if ($publishStatusId == 2){
            $rs = $this->select("where publishStatusId = 2 and jenisDiklatId = ? and labelMapelId = ? and noUrut = ? and nilaiJamSelesai > ? ", array($jenisDiklatId,$labelMapelId,$noUrut, $nilaiJamMulai));

	    	if($rs)
                $error['labelMapelId'] = $this->string('Mata Pelajaran sudah terdaftar');
                
            if($tipeWaktuId == 1){
                $rs = $this->select("where publishStatusId = 2 and tipeWaktuId = 1  and jenisDiklatId = ? AND noUrut = ? AND ((jamMulai1 between ? AND ?) OR  (jamSelesai1 between ? and ?) OR (jamMulai1 < ? AND jamSelesai1 > ?))", array($jenisDiklatId, $noUrut, $jamMulai, $jamSelesai, $jamMulai, $jamSelesai, $jamMulai, $jamSelesai));
    
            if($rs)
                $error['jamMulai'] = $this->string('Jam Mulai Salah'.' '.$jamMulai.' '.$jamSelesai);
            }
        }
        
        
		if(count($error) > 0)
			return $this->invalid('PLEASE_CORRECT', $error);

		$rs = array(
            'publishStatusId' => $publishStatusId,
            'noUrut' => $noUrut,
            'labelMapelId' => $labelMapelId,
            'jamMulai' => $jamMulai,
            'jamSelesai' => $jamSelesai,
            'jumlahHari' => $jumlahHari,
            'jenisDiklatId' => $jenisDiklatId,
            'tipeMataPelajaranId' => $tipeMataPelajaranId,
            'tipeWaktuId' => $tipeWaktuId,
            'tipeTanggalId' => $tipeTanggalId,
            'jumlahJp' => $jumlahJp,
        );

		$this->insert($rs);
		
		return $this->success('DATA_CREATED');
	}

	public function updateData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

        $id = $this->request('id');
        $publishStatusId = $this->request('publishStatusId');
        $noUrut = $this->request('noUrut');
        $labelMapelId = $this->request('labelMapelId');
        $jamMulai = $this->request('jamMulai');
        $jamSelesai = $this->request('jamSelesai');
        $jenisDiklatId = $this->request('jenisDiklatId');
        $tipeMataPelajaranId = $this->request('tipeMataPelajaranId');
        $jumlahHari = $this->request('jumlahHari');
        if(!$jumlahHari)
        $jumlahHari=0;
        $tipeWaktuId = $this->request('tipeWaktuId');
        $tipeTanggalId = $this->request('tipeTanggalId');
        if(!$tipeTanggalId)
            $tipeTanggalId=0;

        $jumlahJp = $this->request('jumlahJp');
		
        $error = $this->checkRequest();

        $rs = $this->find($id);
        
        if(!$rs)
            return $this->failed('DATA_NOT_FOUND');
            

            if($tipeWaktuId == 1 ){
                // if($tipeWaktuId == 1 && $tipeMataPelajaranId ==1){
                $lastJamMulai = $rs['jamMulai'];
                $lastJamSelesai = $rs['jamSelesai'];
                if ($jamMulai != $lastJamMulai || $jamSelesai != $lastJamSelesai) {
                    $rs = array(
                        'jamMulai' => '00:00',
                        'jamSelesai' => '00:00',
                    );
            
                    $this->update($id, $rs);

                    $rs = $this->select("where tipeWaktuId = 1 and  jenisDiklatId = ? AND noUrut = ? AND ((jamMulai1 between ? AND ?) OR  (jamSelesai1 between ? and ?) OR (jamMulai1 < ? AND jamSelesai1 > ?)) ", array($jenisDiklatId, $noUrut, $jamMulai, $jamSelesai, $jamMulai, $jamSelesai, $jamMulai, $jamSelesai));
                   
                    if($rs)
                        $error['jamMulai'] = $this->string('Jam Mulai Salah');

                        $rs = array(
                            'jamMulai' => $lastJamMulai,
                            'jamSelesai' => $lastJamSelesai,
                        );
                        $this->update($id, $rs);
                };
            };
           
		if(count($error) > 0)
			return $this->invalid('PLEASE_CORRECT', $error);
            if($tipeMataPelajaranId == 2 )
                $jumlahJp=0;
		$rs = array(
            'publishStatusId' => $publishStatusId,
            'noUrut' => $noUrut,
            'labelMapelId' => $labelMapelId,
            'jamMulai' => $jamMulai,
            'jamSelesai' => $jamSelesai,
            'jumlahHari' => $jumlahHari,
            'jenisDiklatId' => $jenisDiklatId,
            'tipeMataPelajaranId' => $tipeMataPelajaranId,
            'tipeWaktuId' => $tipeWaktuId,
            'tipeTanggalId' => $tipeTanggalId,
            'jumlahJp' => $jumlahJp,
		);

		$this->update($id, $rs);
		return $this->success('DATA_UPDATED');
	}
	
	public function moveData($params = array())
	{
		if(!$this->setSettings(array(
				'authen' => 'user',
				'requestSource' => $params,
		)))
			return $this->denied();
	
		$idFrom = $this->request('idFrom');
		$idTo = $this->request('idTo');
	
		$rsFrom = $this->find($idFrom);
		$rsTo = $this->find($idTo);
	
		$positionFrom = $rsFrom['position'];
		$positionTo = $rsTo['position'];
	
		$this->update($idFrom, array('position' => $positionTo));
		$this->update($idTo, array('position' => $positionFrom));
	
		return $this->success('DATA_UPDATED');
	}

	public function deleteData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

		$id = $this->request('id');
        $rs = $this->find($id);

		$this->delete($id);
		return $this->success('DATA_DELETED');
	}

	public function multipleDeleteData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

            $multipleId = $this->request('multipleId');
            $mItems = $this->model('tb_bahan_jadwal_diklat');
            $error = array();
            $i = 0;

        foreach($multipleId as $id){
            $rs = $this->find($id);
            
            $i++;
            $rsItems = $mItems->select("where mataPelajaranId = ?", array($id));
            if($rsItems){
                if(count($error) == 0)
                    $error['0'] = 'Data dibawah ini tidak bisa di hapus karena terkait dengan Data lain';
                
                $rs = $this->find($id);
                $error[$i] = $rs['mataPelajaran'];        
            }

            if(!$rsItems ){
                $this->delete($id);
            }

        }

        if(count($error) > 0){
        return $this->failed('PLEASE_CORRECT', $error);
        exit;
        }else{
        return $this->success('DATA_DELETED');
        }
	}
    
    
}

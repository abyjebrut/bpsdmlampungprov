<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contents extends MY_Model {
  protected $table = 'vi_contents';

  public function searchData($params = array())
  {
    if(!$this->setSettings(array(
      'authen' => 'free',
      'requestSource' => $params,
    )))
      return $this->denied();

    // Query

    $keyword = $this->request('keyword');

    $queryStmt = "where title like ? or description like ?";
    $rowsCount = $this->getRowsCount($queryStmt, array("%$keyword%", "%$keyword%"));

    // Limit

    $page = $this->request('page');
    $count = $this->request('count');

    $limitStmt = '';
    $pageCount = 1;

    if (!empty($page) && !empty($count)) {
      $row = ($page * $count) - $count;
      $limitStmt = "limit $row, $count";

      $pageCount = ceil($rowsCount / $count);

      if($pageCount < 1)
        $pageCount = 1;
    }

    $rs = $this->select($queryStmt, array("$keyword%", "%$keyword%"));

    $extra = array(
      'rowsCount' => $rowsCount,
      'pageCount' => $pageCount,
    );

    return $this->success('DATA_READ', $rs, $extra);
  }

}

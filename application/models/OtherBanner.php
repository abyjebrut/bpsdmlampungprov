<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class OtherBanner extends MY_Model {
    protected $table = 'tb_other_banner';
//     protected $view = 'vi_other_banner';
    protected $view = 'vi_all_ordered_pages';
    
    public function readData($params = array())
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();

        // Order

        $orderBy = $this->request('orderBy');
        $reverse = $this->request('reverse');

        $orderStmt = '';

        if (!empty($orderBy)) {
            $orderStmt = "order by $orderBy";

            if (!empty($reverse)) {
                if ($reverse == 1)
                    $orderStmt .= ' desc';
            }
        }

        // Limit

        $page = $this->request('page');
		$count = $this->request('count');

        $limitStmt = '';

        $rowsCount = $this->getRowsCount();
        $pageCount = 1;

        if (!empty($page) && !empty($count)) {
            $row = ($page * $count) - $count;
            $limitStmt = "limit $row, $count";

            $pageCount = ceil($rowsCount / $count);

            if($pageCount < 1)
                $pageCount = 1;
        }

        // Query
        $noId = 9990;
        
// 		$rs = $this->select($orderStmt.' '.$limitStmt);
		$rs = $this->select("where id <> ? ".$orderStmt.' '.$limitStmt, array($noId));
		
        $extra = array(
            'rowsCount' => $rowsCount,
            'pageCount' => $pageCount,
        );

		return $this->success('DATA_READ', $rs, $extra);
	}

    public function filterData($params = array())
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();

        // Query

        $field = $this->request('field');
        $keyword = $this->request('keyword');

        $queryStmt = "where $field like ?";
        $rowsCount = $this->getRowsCount($queryStmt, array("$keyword%"));

        // Limit

        $page = $this->request('page');
		$count = $this->request('count');

        $limitStmt = '';
        $pageCount = 1;

        if (!empty($page) && !empty($count)) {
            $row = ($page * $count) - $count;
            $limitStmt = "limit $row, $count";

            $pageCount = ceil($rowsCount / $count);

            if($pageCount < 1)
                $pageCount = 1;
        }

        $rs = $this->select($queryStmt, array("$keyword%"));

        $extra = array(
            'rowsCount' => $rowsCount,
            'pageCount' => $pageCount,
        );

		return $this->success('DATA_READ', $rs, $extra);
	}

	public function detailData($params = array())
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();

		$pagesId = $this->request('id');
		$rs = $this->find($pagesId);
		$page=$rs['title'];
        // Query
		
		$mItems1 = $this->model('vi_other_banner');
		$rsItems = $mItems1->single('where pagesId=?', array($pagesId));

        if($rs)
            return $this->success('DATA_READ', $rsItems, $page);
        else
            return $this->failed('DATA_NOT_FOUND');
	}

    private function checkRequest() {
        $publishStatusId = $this->request('publishStatusId');
        $banner = $this->request('banner');

		$error = array();

		if(empty($publishStatusId))
			$error['publishStatusId'] = $this->string('PUBLISH_STATUS_REQUIRED');
		
		if(empty($banner))
			$error['banner'] = $this->string('BANNER_REQUIRED');

        return $error;
    }

    public function saveData($params = array())
    {
    	if(!$this->setSettings(array(
    			'authen' => 'user',
    			'requestSource' => $params,
    	)))
    		return $this->denied();

    	$error = $this->checkRequest();

    	if(count($error) > 0)
    		return $this->invalid('PLEASE_CORRECT', $error);

    	$pagesId = $this->request('pagesId');
        $title = $this->request('title');
        $title_sl = $this->request('titleSl');
        $title_el = $this->request('titleEl');
        $banner = $this->request('banner');
        $publishStatusId = $this->request('publishStatusId');

        $rs = array(
        		'pagesId' => $pagesId,
        		'title' => $title,
        		'title_sl' => $title_sl,
        		'title_el' => $title_el,
        		'banner' => $banner,
        		'publishStatusId' => $publishStatusId,
        );
        
    	$mItems1 = $this->model('tb_other_banner');
    	$rsFound = $mItems1->single('where pagesId=?', array($pagesId));

    	if($rsFound) {

	    	if(!empty($banner)) {
	        	if($banner != $rsFound['banner']) {
	
	        		$last_banner = $rsFound['banner'];
	        		$this->deleteArchive($last_banner);
	
	        		if(!$this->moveToArchive($banner))
	        			return $this->failed('FILE_UPLOAD_FAILED');
	        	}
	        }
    		
    		$this->update('pagesId',$pagesId, $rs);


    		return $this->success('DATA_UPDATED');
    	}
    	else {
			
	    	if(!empty($banner)) {
				if(!$this->moveToArchive($banner))
					return $this->failed('FILE_UPLOAD_FAILED');
			}
    		
    		$this->insert($rs);

    		return $this->success('DATA_CREATED');
    	}

    }

	public function deleteData($params = array())
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

		$id = $this->request('id');
        $rs = $this->find($id);

		$this->delete($id);
		return $this->success('DATA_DELETED');
	}

	public function multipleDeleteData($params = array())
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

		$multipleId = $this->request('multipleId');
		$mItems1 = $this->model('tb_seo');
		$mKeywords = $this->model('tb_seo_keywords');

		foreach($multipleId as $id){
			$rsItems1 = $mItems1->single('where pagesId=?', array($id));
			$seoId = $rsItems1['id'];

			$mKeywords->delete('seoId', $seoId);
		}

		$this->delete('pagesId',$id);

		return $this->success('DATA_DELETED');
	}

}

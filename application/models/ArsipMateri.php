<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ArsipMateri extends MY_Model {
    protected $table = 'tb_arsip_materi';
    protected $view = 'vi_arsip_materi';
    
    public function readData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();

        // Order

        $orderBy = $this->request('orderBy');
        $reverse = $this->request('reverse');

        $orderStmt = '';

        if (!empty($orderBy)) {
            $orderStmt = "order by $orderBy";

            if (!empty($reverse)) {
                if ($reverse == 1)
                    $orderStmt .= ' desc';
            }
        }

        // Limit
        $page = $this->request('page');
		$count = $this->request('count');
		$jenisDiklatId = $this->request('jenisDiklatId');
		$namaDiklat = $this->request('namaDiklat');
		$publishStatusId = $this->request('publishStatusId');

        $limitStmt = '';

        if (!empty($jenisDiklatId) && !empty($namaDiklat) && !empty($publishStatusId))
        	$rowsCount = $this->getRowsCount("where jenisDiklatId = ? and namaDiklat = ? and publishStatusId = ? ", array($jenisDiklatId, $namaDiklat, $publishStatusId));
      	elseif (!empty($jenisDiklatId) && !empty($namaDiklat))
	      	$rowsCount = $this->getRowsCount("where jenisDiklatId = ? and namaDiklat = ? ", array($jenisDiklatId, $namaDiklat));
      	elseif (!empty($namaDiklat) && !empty($publishStatusId))
      		$rowsCount = $this->getRowsCount("where namaDiklat = ? and publishStatusId = ? ", array($namaDiklat, $publishStatusId));
      	elseif (!empty($jenisDiklatId) && !empty($publishStatusId))
      	$rowsCount = $this->getRowsCount("where jenisDiklatId = ? and publishStatusId = ? ", array($jenisDiklatId, $publishStatusId));
	    elseif (!empty($jenisDiklatId))
	      	$rowsCount = $this->getRowsCount("where jenisDiklatId = ? ", array($jenisDiklatId));
	    elseif (!empty($namaDiklat))
	    $rowsCount = $this->getRowsCount("where namaDiklat = ? ", array($namaDiklat));
	    elseif (!empty($publishStatusId))
	      	$rowsCount = $this->getRowsCount("where publishStatusId = ? ", array($publishStatusId));
	    else
	      	$rowsCount = $this->getRowsCount($orderStmt);
        
      	$pageCount = 1;

        if (!empty($page) && !empty($count)) {
            $row = ($page * $count) - $count;
            $limitStmt = "limit $row, $count";

            $pageCount = ceil($rowsCount / $count);

            if($pageCount < 1)
                $pageCount = 1;
        }

        // Query
          if (!empty($jenisDiklatId) && !empty($namaDiklat) && !empty($publishStatusId))
        	  $rs = $this->select("where jenisDiklatId = ? and namaDiklat = ? and publishStatusId = ? ".$orderStmt.' '.$limitStmt, array($jenisDiklatId, $namaDiklat, $publishStatusId));
	      elseif (!empty($jenisDiklatId) && !empty($namaDiklat))
		      $rs = $this->select("where jenisDiklatId = ? and namaDiklat = ? ".$orderStmt.' '.$limitStmt, array($jenisDiklatId, $namaDiklat));
	      elseif (!empty($jenisDiklatId) && !empty($publishStatusId))
	      	  $rs = $this->select("where jenisDiklatId = ? and publishStatusId = ? ".$orderStmt.' '.$limitStmt, array($jenisDiklatId, $publishStatusId));
	      elseif (!empty($namaDiklat) && !empty($publishStatusId))
	      	  $rs = $this->select("where namaDiklat = ? and publishStatusId = ? ".$orderStmt.' '.$limitStmt, array($namaDiklat, $publishStatusId));
		    elseif (!empty($jenisDiklatId))
		      $rs = $this->select("where jenisDiklatId = ? ".$orderStmt.' '.$limitStmt, array($jenisDiklatId));
		    elseif (!empty($namaDiklat))
		    	$rs = $this->select("where namaDiklat = ? ".$orderStmt.' '.$limitStmt, array($namaDiklat));
		    elseif (!empty($publishStatusId))
		      $rs = $this->select("where publishStatusId = ? ".$orderStmt.' '.$limitStmt, array($publishStatusId));
		    else
		      $rs = $this->select($orderStmt.' '.$limitStmt);
    
        $extra = array(
            'rowsCount' => $rowsCount,
            'pageCount' => $pageCount,
        );

		return $this->success('DATA_READ', $rs, $extra);
	}

    public function filterData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();
        
        // Query

        $field = $this->request('field');
        $keyword = $this->request('keyword');

        $queryStmt = "where $field like ?";
        $rowsCount = $this->getRowsCount($queryStmt, array("$keyword%"));

        // Limit

        $page = $this->request('page');
		$count = $this->request('count');

        $limitStmt = '';
        $pageCount = 1;

        if (!empty($page) && !empty($count)) {
            $row = ($page * $count) - $count;
            $limitStmt = "limit $row, $count";

            $pageCount = ceil($rowsCount / $count);

            if($pageCount < 1)
                $pageCount = 1;
        }

        $rs = $this->select($queryStmt, array("$keyword%"));

        $extra = array(
            'rowsCount' => $rowsCount,
            'pageCount' => $pageCount,
        );

		return $this->success('DATA_READ', $rs, $extra);
	}

	public function detailData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();

		$id = $this->request('id');
        
        // Query
        
		$rs = $this->find($id);
		
		$mItems = $this->model('vi_arsip_materi_pengajar');
		$rs['links'] = $mItems->select('where arsipMateriId = ? order by id', array($id));

        if($rs)
            return $this->success('DATA_READ', $rs);
        else
            return $this->failed('DATA_NOT_FOUND');
	}

    private function checkRequest() {
    	$publishStatusId = $this->request('publishStatusId');
        $diklatId = $this->request('diklatId');
        $mataDiklat = $this->request('mataDiklat');

		$error = array();
		
		if(empty($publishStatusId))
			$error['publishStatusId'] = $this->string('PUBLISH_STATUS_REQUIRED');
		
		if(empty($diklatId))
			$error['diklatId'] = $this->string('NAMA_DIKLAT_REQUIRED');
		
		if(empty($mataDiklat))
			$error['mataDiklat'] = $this->string('MATA_DIKLAT_REQUIRED');

        return $error;
    }

	public function createData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

		$error = $this->checkRequest();

		$publishStatusId = $this->request('publishStatusId');
		$diklatId = $this->request('diklatId');
		$mataDiklat = $this->request('mataDiklat');
		$fileWord = $this->request('fileWord');
		$fileExcel = $this->request('fileExcel');
		$filePowerPoint = $this->request('filePowerPoint');
		$filePdf = $this->request('filePdf');
		$links = $this->request('links');

		if(count($error) > 0)
			return $this->invalid('PLEASE_CORRECT', $error);

		if(!empty($fileWord)) {
			if(!$this->moveToArchive($fileWord))
				return $this->failed('FILE_UPLOAD_FAILED');
		}
		
		if(!empty($fileExcel)) {
			if(!$this->moveToArchive($fileExcel))
				return $this->failed('FILE_UPLOAD_FAILED');
		}
		
		if(!empty($filePowerPoint)) {
			if(!$this->moveToArchive($filePowerPoint))
				return $this->failed('FILE_UPLOAD_FAILED');
		}
		
		if(!empty($filePdf)) {
			if(!$this->moveToArchive($filePdf))
				return $this->failed('FILE_UPLOAD_FAILED');
		}
		
		$rs = array(
			'publishStatusId' => $publishStatusId,
            'diklatId' => $diklatId,
			'mataDiklat' => $mataDiklat,
			'fileWord' => $fileWord,
			'fileExcel' => $fileExcel,
			'filePowerPoint' => $filePowerPoint,
			'filePdf' => $filePdf,
        );

		$this->insert($rs);
		$lastId = $this->getLastInsertId();
		
		if($links){
			$mItems = $this->model('tb_arsip_materi_pengajar');
		
			foreach($links as $link) {
				$mItems->insert(array(
						'arsipMateriId' => $lastId,
						'pengajarDiklatId' => $link['pengajarDiklatId'],
				));
			}
		}
		
		return $this->success('DATA_CREATED');
	}

	public function updateData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

        $id = $this->request('id');
        $publishStatusId = $this->request('publishStatusId');
        $diklatId = $this->request('diklatId');
		$mataDiklat = $this->request('mataDiklat');
		$fileWord = $this->request('fileWord');
		$fileExcel = $this->request('fileExcel');
		$filePowerPoint = $this->request('filePowerPoint');
		$filePdf = $this->request('filePdf');
		$links = $this->request('links');
		
        $error = $this->checkRequest();

        $rs = $this->find($id);
        
        if(!empty($fileWord)) {
        	if($fileWord != $rs['fileWord']) {
        
        		$last_fileWord = $rs['fileWord'];
        		$this->deleteArchive($last_fileWord);
        
        		if(!$this->moveToArchive($fileWord))
        			return $this->failed('FILE_UPLOAD_FAILED');
        	}
        }
        
        if(!empty($fileExcel)) {
        	if($fileExcel != $rs['fileExcel']) {
        
        		$last_fileExcel = $rs['fileExcel'];
        		$this->deleteArchive($last_fileExcel);
        
        		if(!$this->moveToArchive($fileExcel))
        			return $this->failed('FILE_UPLOAD_FAILED');
        	}
        }
        
        if(!empty($filePowerPoint)) {
        	if($filePowerPoint != $rs['filePowerPoint']) {
        
        		$last_filePowerPoint = $rs['filePowerPoint'];
        		$this->deleteArchive($last_filePowerPoint);
        
        		if(!$this->moveToArchive($filePowerPoint))
        			return $this->failed('FILE_UPLOAD_FAILED');
        	}
        }
        
        if(!empty($filePdf)) {
        	if($filePdf != $rs['filePdf']) {
        
        		$last_filePdf = $rs['filePdf'];
        		$this->deleteArchive($last_filePdf);
        
        		if(!$this->moveToArchive($filePdf))
        			return $this->failed('FILE_UPLOAD_FAILED');
        	}
        }
        
        if(!$rs)
            return $this->failed('DATA_NOT_FOUND');

		if(count($error) > 0)
			return $this->invalid('PLEASE_CORRECT', $error);
		
		$rs = array(
			'publishStatusId' => $publishStatusId,
			'diklatId' => $diklatId,
			'mataDiklat' => $mataDiklat,
			'fileWord' => $fileWord,
			'fileExcel' => $fileExcel,
			'filePowerPoint' => $filePowerPoint,
			'filePdf' => $filePdf,
		);

		$this->update($id, $rs);
		
		$mItems = $this->model('tb_arsip_materi_pengajar');
		$mItems->delete('arsipMateriId', $id);
		
		if($links){
			$mItems = $this->model('tb_arsip_materi_pengajar');
		
			foreach($links as $link) {
				$mItems->insert(array(
						'arsipMateriId' => $id,
						'pengajarDiklatId' => $link['pengajarDiklatId'],
				));
			}
		}
		
		return $this->success('DATA_UPDATED');
	}
	
	public function deleteData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

		$id = $this->request('id');
        $rs = $this->find($id);

		$this->delete($id);
		return $this->success('DATA_DELETED');
	}

	public function multipleDeleteData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

		$multipleId = $this->request('multipleId');

		foreach($multipleId as $id){
			$rs = $this->find($id);
			
			if(!empty($rs['fileWord']))
				$this->deleteArchive($rs['fileWord']);
			
			if(!empty($rs['fileExcel']))
				$this->deleteArchive($rs['fileExcel']);
			
			if(!empty($rs['filePowerPoint']))
				$this->deleteArchive($rs['filePowerPoint']);
			
			if(!empty($rs['filePdf']))
				$this->deleteArchive($rs['filePdf']);
			
			$mSubMenu = $this->model('tb_arsip_materi_pengajar');
			$mSubMenu->delete('arsipMateriId', $id);
			
			$this->delete($id);
		}
            

		return $this->success('DATA_DELETED');
	}
    
    
}
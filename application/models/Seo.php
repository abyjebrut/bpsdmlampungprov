<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Seo extends MY_Model {
    protected $table = 'tb_seo';
    protected $view = 'vi_all_ordered_pages';

    public function readData($params = array())
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();

        // Order

        $orderBy = $this->request('orderBy');
        $reverse = $this->request('reverse');

        $orderStmt = '';

        if (!empty($orderBy)) {
            $orderStmt = "order by $orderBy";

            if (!empty($reverse)) {
                if ($reverse == 1)
                    $orderStmt .= ' desc';
            }
        }

        // Limit

        $page = $this->request('page');
		$count = $this->request('count');

        $limitStmt = '';

        $rowsCount = $this->getRowsCount();
        $pageCount = 1;

        if (!empty($page) && !empty($count)) {
            $row = ($page * $count) - $count;
            $limitStmt = "limit $row, $count";

            $pageCount = ceil($rowsCount / $count);

            if($pageCount < 1)
                $pageCount = 1;
        }

        // Query

		$rs = $this->select($orderStmt.' '.$limitStmt);
        $extra = array(
            'rowsCount' => $rowsCount,
            'pageCount' => $pageCount,
        );

		return $this->success('DATA_READ', $rs, $extra);
	}

    public function filterData($params = array())
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();

        // Query

        $field = $this->request('field');
        $keyword = $this->request('keyword');

        $queryStmt = "where $field like ?";
        $rowsCount = $this->getRowsCount($queryStmt, array("$keyword%"));

        // Limit

        $page = $this->request('page');
		$count = $this->request('count');

        $limitStmt = '';
        $pageCount = 1;

        if (!empty($page) && !empty($count)) {
            $row = ($page * $count) - $count;
            $limitStmt = "limit $row, $count";

            $pageCount = ceil($rowsCount / $count);

            if($pageCount < 1)
                $pageCount = 1;
        }

        $rs = $this->select($queryStmt, array("$keyword%"));

        $extra = array(
            'rowsCount' => $rowsCount,
            'pageCount' => $pageCount,
        );

		return $this->success('DATA_READ', $rs, $extra);
	}

	public function detailData($params = array())
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();

		$pagesId = $this->request('id');

        // Query

		$rs = $this->find($pagesId);

		$mItems1 = $this->model('vi_seo');
		$rsItems1 = $mItems1->single('where pagesId=?', array($pagesId));
		if ($rsItems1){
			$seoId = $rsItems1['id'];
			$description = $rsItems1['description'];
		}else{
			$seoId=0;
			$description = '';
		}

		$mItems = $this->model('tb_seo_keywords');
		$rs['listKeywords'] = $mItems->select('where seoId = ? order by id', array($seoId));

        if($rs)
            return $this->success('DATA_READ', $rs, $description);
        else
            return $this->failed('DATA_NOT_FOUND');
	}

    public function detailMeta($params = array())
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();

		    $pagesId = $this->request('id');

        // Query

        $mSeo = $this->model('vi_seo');
		    $rs = $mSeo->find('pagesId', $pagesId);

        if($rs)
            return $this->success('DATA_READ', $rs);
        else
            return $this->failed('DATA_NOT_FOUND');
    }

    private function checkRequest() {
//         $title = $this->request('title');

// 		$error = array();

// 		if(empty($title))
// 			$error['title'] = $this->string('TITLE_REQUIRED');

//         return $error;
    }

    public function saveData($params = array())
    {
    	if(!$this->setSettings(array(
    			'authen' => 'user',
    			'requestSource' => $params,
    	)))
    		return $this->denied();

    	$error = $this->checkRequest();

    	if(count($error) > 0)
    		return $this->invalid('PLEASE_CORRECT', $error);

    	$pagesId = $this->request('pagesId');
        $description = $this->request('description');
        $listKeywords = $this->request('listKeywords');

    	$rs = array(
    		'pagesId' => $pagesId,
			'description' => $description,
    	);

    	$mItems1 = $this->model('tb_seo');
    	$rsFound = $mItems1->single('where pagesId=?', array($pagesId));

    	if($rsFound) {

    		$this->update('pagesId',$pagesId, $rs);

    		if($listKeywords) {

	    		$mItems1 = $this->model('tb_seo');
	    		$rsItems1 = $mItems1->single('where pagesId=?', array($pagesId));
	    		$seoId = $rsItems1['id'];

	    		$mKeywords = $this->model('tb_seo_keywords');
	    		$mKeywords->delete('seoId', $seoId);

	    		$mKeywords = $this->model('tb_seo_keywords');

	    		foreach($listKeywords as $sub) {
	    			$mKeywords->insert(array(
	    					'seoId' => $seoId,
	    					'keywords' => $sub['keywords'],
	    			));
	    		}
    		}

    		return $this->success('DATA_UPDATED');
    	}
    	else {

    		$rs['pagesId'] = $pagesId;

    		$this->insert($rs);
    		$lastId = $this->getLastInsertId();

    		if($listKeywords) {
	    		$mKeywords = $this->model('tb_seo_keywords');

	    		foreach($listKeywords as $sub) {
	    			$mKeywords->insert(array(
	    					'seoId' => $lastId,
	    					'keywords' => $sub['keywords'],
	    			));
	    		}
    		}
    		return $this->success('DATA_CREATED');
    	}

    }

	public function deleteData($params = array())
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

		$id = $this->request('id');
        $rs = $this->find($id);

		$this->delete($id);
		return $this->success('DATA_DELETED');
	}

	public function multipleDeleteData($params = array())
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

		$multipleId = $this->request('multipleId');
		$mItems1 = $this->model('tb_seo');
		$mKeywords = $this->model('tb_seo_keywords');

		foreach($multipleId as $id){
			$rsItems1 = $mItems1->single('where pagesId=?', array($id));
			$seoId = $rsItems1['id'];

			$mKeywords->delete('seoId', $seoId);
		}

		$this->delete('pagesId',$id);

		return $this->success('DATA_DELETED');
	}

}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class JadwalPengajar extends MY_Model {
    protected $table = 'tb_jadwal_pengajar';
    protected $view = 'vi_jadwal_pengajar';
    
    public function readData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();

        // Order

        $orderBy = $this->request('orderBy');
        $reverse = $this->request('reverse');

        $orderStmt = '';

        if (!empty($orderBy)) {
            $orderStmt = "order by $orderBy";

            if (!empty($reverse)) {
                if ($reverse == 1)
                    $orderStmt .= ' desc';
            }
        }

        // Limit
        $page = $this->request('page');
		$count = $this->request('count');
        $publishStatusId = $this->request('publishStatusId');
        $tahun = $this->request('tahun');
        $jenisDiklatId = $this->request('jenisDiklatId');
        $diklatId = $this->request('diklatId');

        $limitStmt = '';

        if (!empty($publishStatusId))
	      $rowsCount = $this->getRowsCount("where publishStatusId = ? ", array($publishStatusId));
	    else
	      $rowsCount = $this->getRowsCount($orderStmt);
        
      	$pageCount = 1;

        if (!empty($page) && !empty($count)) {
            $row = ($page * $count) - $count;
            $limitStmt = "limit $row, $count";

            $pageCount = ceil($rowsCount / $count);

            if($pageCount < 1)
                $pageCount = 1;
        }

        // Query

	   if (!empty($publishStatusId) && !empty($tahun) && !empty($jenisDiklatId) && !empty($diklatId))
	      $rs = $this->select("where publishStatusId = ? and year(_pelaksanaanMulai) = ? and jenisDiklatId = ? and diklatId = ? ".$orderStmt.' '.$limitStmt, array($publishStatusId,$tahun,$jenisDiklatId,$diklatId));
        elseif (!empty($publishStatusId) && !empty($tahun) && !empty($jenisDiklatId))
	      $rs = $this->select("where publishStatusId = ? and year(_pelaksanaanMulai) = ? and jenisDiklatId = ? ".$orderStmt.' '.$limitStmt, array($publishStatusId,$tahun,$jenisDiklatId));
        elseif (!empty($publishStatusId) && !empty($tahun) &&  !empty($diklatId))
	      $rs = $this->select("where publishStatusId = ? and year(_pelaksanaanMulai) = ? and diklatId = ? ".$orderStmt.' '.$limitStmt, array($publishStatusId,$tahun, $diklatId));
        elseif (!empty($publishStatusId) && !empty($jenisDiklatId) && !empty($diklatId))
	      $rs = $this->select("where publishStatusId = ? and jenisDiklatId = ? and diklatId = ? ".$orderStmt.' '.$limitStmt, array($publishStatusId, $jenisDiklatId,$diklatId));
        elseif (!empty($tahun) && !empty($jenisDiklatId) && !empty($diklatId))
	      $rs = $this->select("where year(_pelaksanaanMulai) = ? and jenisDiklatId = ? and diklatId = ? ".$orderStmt.' '.$limitStmt, array($tahun,$jenisDiklatId,$diklatId));
        elseif (!empty($publishStatusId) && !empty($tahun))
	      $rs = $this->select("where publishStatusId = ? and year(_pelaksanaanMulai) = ? ".$orderStmt.' '.$limitStmt, array($publishStatusId,$tahun));
        elseif (!empty($publishStatusId) && !empty($jenisDiklatId))
	      $rs = $this->select("where publishStatusId = ? and jenisDiklatId = ? ".$orderStmt.' '.$limitStmt, array($publishStatusId,$jenisDiklatId));
        elseif (!empty($publishStatusId) && !empty($diklatId))
	      $rs = $this->select("where publishStatusId = ? and diklatId = ? ".$orderStmt.' '.$limitStmt, array($publishStatusId,$diklatId));
        elseif (!empty($tahun) && !empty($jenisDiklatId))
	      $rs = $this->select("where year(_pelaksanaanMulai) = ? and jenisDiklatId = ? ".$orderStmt.' '.$limitStmt, array($tahun,$jenisDiklatId));
        elseif (!empty($tahun) && !empty($diklatId))
	      $rs = $this->select("where year(_pelaksanaanMulai) = ? and diklatId = ? ".$orderStmt.' '.$limitStmt, array($tahun,$diklatId));
        elseif (!empty($jenisDiklatId) && !empty($diklatId))
	      $rs = $this->select("where jenisDiklatId = ? and diklatId = ? ".$orderStmt.' '.$limitStmt, array($jenisDiklatId,$diklatId));
        elseif (!empty($jenisDiklatId))
	      $rs = $this->select("where jenisDiklatId = ? ".$orderStmt.' '.$limitStmt, array($jenisDiklatId));
        elseif (!empty($diklatId))
	      $rs = $this->select("where diklatId = ? ".$orderStmt.' '.$limitStmt, array($diklatId));
        elseif (!empty($publishStatusId))
	      $rs = $this->select("where publishStatusId = ? ".$orderStmt.' '.$limitStmt, array($publishStatusId));
        elseif (!empty($tahun))
	      $rs = $this->select("where year(_pelaksanaanMulai) = ? ".$orderStmt.' '.$limitStmt, array($tahun));                          
	    else
	      $rs = $this->select($orderStmt.' '.$limitStmt);
    
        $extra = array(
            'rowsCount' => $rowsCount,
            'pageCount' => $pageCount,
        );

		return $this->success('DATA_READ', $rs, $extra);
	}
                              
    public function readDataTahun($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();

        // Query

	      $rs = $this->query("SELECT distinct(year(_pelaksanaanMulai)) as tahun FROM vi_jadwal_pengajar;");
    
		return $this->success('DATA_READ', $rs);
	}                              

    public function filterData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();
        
        // Query

        $field = $this->request('field');
        $keyword = $this->request('keyword');

        $queryStmt = "where $field like ?";
        $rowsCount = $this->getRowsCount($queryStmt, array("$keyword%"));

        // Limit

        $page = $this->request('page');
		$count = $this->request('count');

        $limitStmt = '';
        $pageCount = 1;

        if (!empty($page) && !empty($count)) {
            $row = ($page * $count) - $count;
            $limitStmt = "limit $row, $count";

            $pageCount = ceil($rowsCount / $count);

            if($pageCount < 1)
                $pageCount = 1;
        }

        $rs = $this->select($queryStmt, array("$keyword%"));

        $extra = array(
            'rowsCount' => $rowsCount,
            'pageCount' => $pageCount,
        );

		return $this->success('DATA_READ', $rs, $extra);
	}

	public function detailData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();

		$id = $this->request('id');
        
        // Query
        
        $rs = $this->find($id);
        $publishStatusId = 2;
        $jenisDiklatId = $rs['jenisDiklatId'];
        $angkatanId = $rs['angkatanId'];
        $mItems = $this->model('vi_bahan_jadwal_pengajar');
        $rs['listBahanJadwal'] = $mItems->select('where jadwalPengajarId = ? order by id', array($id));
        
        $statusMasukId=0;
        $mItems = $this->model('vi_filter_mapel');
		$rs['listFilterMapel'] = $mItems->select('where angkatanId = ? and jenisDiklatId = ?  order by id', array($angkatanId,$jenisDiklatId));

        $mItems = $this->model('vi_mata_pelajaran');
		$rs['listMapel'] = $mItems->select('where jenisDiklatId = ?  order by id', array($jenisDiklatId));

        $mItems = $this->model('vi_pengajar_diklat');
		$rs['listPengajar'] = $mItems->select('where publishStatusId = ?  order by id', array($publishStatusId));


        if($rs)
            return $this->success('DATA_READ', $rs);
        else
            return $this->failed('DATA_NOT_FOUND');
	}

    private function checkRequest() {
        $publishStatusId = $this->request('publishStatusId');
        $nomor = $this->request('nomor');
        $diklatId = $this->request('diklatId');
        $angkatanId = $this->request('angkatanId');
        
		$error = array();
        
        if(empty($publishStatusId))
			$error['publishStatusId'] = $this->string('PUBLISH_STATUS_REQUIRED');
		
		if(empty($nomor))
			$error['nomor'] = $this->string('Nomor diperlukan');
        if(empty($diklatId))
			$error['diklatId'] = $this->string('Nama Diklat diperlukan');
        if(empty($angkatanId))
			$error['angkatanId'] = $this->string('Angkatan diperlukan');
        
        return $error;
    }

	public function createData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

		$error = $this->checkRequest();
        $publishStatusId = $this->request('publishStatusId');
		$nomor = $this->request('nomor');
        $diklatId = $this->request('diklatId');
        $angkatanId = $this->request('angkatanId');
        
        $rs = $this->find('nomor', $nomor);

		if($rs)
            $error['nomor'] = $this->string('Nomor sudah terdaftar');

		if(count($error) > 0)
			return $this->invalid('PLEASE_CORRECT', $error);

		$rs = array(
                'nomor' => $nomor,
                'publishStatusId' => $publishStatusId,
                'diklatId' => $diklatId,
                'angkatanId' => $angkatanId,
        );

		$this->insert($rs);
		
		return $this->success('DATA_CREATED');
	}

	public function updateData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

        $id = $this->request('id');
        $publishStatusId = $this->request('publishStatusId');
        $nomor = $this->request('nomor');
        $diklatId = $this->request('diklatId');
        $angkatanId = $this->request('angkatanId');
        
        $error = $this->checkRequest();

        $rs = $this->find($id);
        
        if(!$rs)
            return $this->failed('DATA_NOT_FOUND');

        $last_nomor = $rs['nomor'];

        if($nomor != $last_nomor) {
            $rs = $this->find('nomor', $nomor);

            if($rs)
                $error['nomor'] = $this->string('Nomor sudah terdaftar');
        }

		if(count($error) > 0)
			return $this->invalid('PLEASE_CORRECT', $error);
		
		$rs = array(
				'nomor' => $nomor,
                'publishStatusId' => $publishStatusId,
                'diklatId' => $diklatId,
                'angkatanId' => $angkatanId,
		);

		$this->update($id, $rs);
        
		return $this->success('DATA_UPDATED');
	}
	
	
	public function deleteData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

		$id = $this->request('id');
        $rs = $this->find($id);

		$this->delete($id);
		return $this->success('DATA_DELETED');
	}

	public function multipleDeleteData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

		$multipleId = $this->request('multipleId');

		foreach($multipleId as $id){
            $mBahanJadwal = $this->model('tb_bahan_jadwal_pengajar');
            $mBahanJadwal->delete('jadwalPengajarId', $id);
            
            $this->delete($id);
        }
            

		return $this->success('DATA_DELETED');
	}
    
    public function createPDFDetail($params = array())
	{
		if(!$this->setSettings(array(
				'authen' => 'user',
				'requestSource' => $params,
		)))
			return $this->denied();

		// PDF Settings

		$this->load->library('headless_pdf');
        $this->headless_pdf->setSettings(array(
				'strings' => $this->config->item('strings'),
				'pageOrientation' => 'P',
		));

		// Query

		$id = $this->request('id');
        $rs = $this->find($id);
        $mItems = $this->model('vi_bahan_jadwal_pengajar');
		$rsItems = $mItems->select('where jadwalPengajarId = ? order by id', array($id));
		
		// Output
        

		if (count($rs) > 0) {
			$fileUrl = $this->headless_pdf->render(
					'JadwalPengajarDetail',
					array(
            'rs' => $rs,
            'rsItems' => $rsItems,
          )
			);

			return $this->success('FILE_CREATED', $fileUrl);
		}
		else
			return $this->failed('FILE_CREATE_FAILED');
	}
    
    
}

<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Anggota extends MY_Model
{
  protected $table = 'tb_akun';
  protected $view = 'vi_akun';

  public function verifikasiToken($params = array())
  {
    if (!$this->setSettings(array(
      'authen' => 'free',
      'requestSource' => $params,
    )))
      return $this->denied();

    $token = $this->request('token');

    $mVerifikasiAkun = $this->model('tb_verifikasi_akun');
    $rowVerifikasiAkun = $mVerifikasiAkun->find('token', $token);

    if ($rowVerifikasiAkun) {
      if ($rowVerifikasiAkun['verifiedId'] == 2) {
        $akunId = $rowVerifikasiAkun['akunId'];

        $this->update('id', $akunId, array(
          'statusAkunId' => 2,
        ));

        $mVerifikasiAkun->update('akunId', $akunId, array(
          'verifiedId' => 1,
        ));

        return $this->success('DATA_UPDATED');
      } else
        return $this->failed('ACCESS_DENIED');
    } else
      return $this->failed('ACCESS_DENIED');
  }

  public function checkForgotToken($token)
  {

    $mForgotAkun = $this->model('vi_forgot_akun');
    $rowForgotAkun = $mForgotAkun->find('token', $token);

    if ($rowForgotAkun) {
      if ($rowForgotAkun['verifiedId'] == 2)
        return true;
      else
        return false;
    } else
      return false;
  }

  public function forgotPassword($params = array())
  {
    if (!$this->setSettings(array(
      'authen' => 'free',
      'requestSource' => $params,
    )))
      return $this->denied();

    $nip = $this->request('nip');
    $email = $this->request('email');

    $error = array();

    if (empty($nip))
      $error['nip'] = $this->string('NIP_REQUIRED');

    if (empty($email))
      $error['email'] = $this->string('EMAIL_REQUIRED');

    if (count($error) > 0)
      return $this->invalid('PLEASE_CORRECT', $error);

    if (!empty($nip)) {
      $rowAkun = $this->find('nip', $nip);

      if (!$rowAkun)
        $error['nip'] = $this->string('NIP_WRONG');
      else {
        if ($email != $rowAkun['email'])
          $error['email'] = $this->string('EMAIL_WRONG');
      }
    }

    if (count($error) > 0)
      return $this->invalid('PLEASE_CORRECT', $error);

    $akunId = $rowAkun['id'];
    $token = $this->encrypt();

    $mForgotAkun = $this->model('tb_forgot_akun');

    $mForgotAkun->insert(array(
      'akunId' => $akunId,
      'token' => $token,
      'verifiedId' => 2,
    ));

    $message =
      "Untuk mereset Kata Sandi Anda di diklatlampung.info silakan buka halaman Reset Kata Sandi ini " .
      base_url() . "verifikasi/reset_password/$token";

    // mail function here

    $config = array(
      'protocol' => 'smtp',
      'smtp_host' => 'ssl://bpsdm.lampungprov.go.id',
      'smtp_port' => 465,
      'smtp_timeout' => 5,
      'smtp_user' => '_mainaccount@bpsdm.lampungprov.go.id',
      'smtp_pass' => 'Bpsdmlp9^^v(((',
      'mailtype'  => 'html',
      'charset'   => 'iso-8859-1'
    );

    $this->load->library('email');
    $this->email->set_newline("\r\n");

    $this->email->initialize($config);
    $this->email->from($config['smtp_user']);
    $this->email->to($email);

    $this->email->subject('Pemberitahuan Diklatlampung');
    $this->email->message($message);


    $this->email->send();

    return $this->success('DATA_CREATED');
  }

  public function reEmail($params = array())
  {
    if (!$this->setSettings(array(
      'authen' => 'free',
      'requestSource' => $params,
    )))
      return $this->denied();

    $nip = $this->request('nip');
    $email = $this->request('email');

    $error = array();

    if (empty($nip))
      $error['nip'] = $this->string('NIP_REQUIRED');

    if (empty($email))
      $error['email'] = $this->string('EMAIL_REQUIRED');

    if (count($error) > 0)
      return $this->invalid('PLEASE_CORRECT', $error);

    if (!empty($nip)) {
      $rowAkun = $this->find('nip', $nip);

      if (!$rowAkun)
        $error['nip'] = $this->string('NIP_WRONG');
      else {
        if ($email != $rowAkun['email'])
          $error['email'] = $this->string('EMAIL_WRONG');
      }
    }

    if (count($error) > 0)
      return $this->invalid('PLEASE_CORRECT', $error);

    $akunId = $rowAkun['id'];
    $token = $this->encrypt();

    $mVerifikasiAkun = $this->model('tb_verifikasi_akun');

    $mVerifikasiAkun->insert(array(
      'akunId' => $akunId,
      'token' => $token,
      'verifiedId' => 2,
    ));

    $message =
      "Silakan verifikasi Akun siapdiklat.bpsdm.lampungprov.go.id Anda dengan membuka halaman ini atau dengan copy link : " .
      base_url() . "verifikasi/aktifasi/$token Akun Anda baru dapat digunakan setelah melakukan verifikasi ini";

    // mail function here

    $config = array(
      'protocol' => 'smtp',
      'smtp_host' => 'ssl://bpsdm.lampungprov.go.id',
      'smtp_port' => 465,
      'smtp_timeout' => 5,
      'smtp_user' => '_mainaccount@bpsdm.lampungprov.go.id',
      'smtp_pass' => 'Bpsdmlp9^^v(((',
      'mailtype'  => 'html',
      'charset'   => 'iso-8859-1'
    );

    $this->load->library('email');
    $this->email->set_newline("\r\n");
    $this->email->initialize($config);
    $this->email->from($config['smtp_user']);
    $this->email->to($email);
    $this->email->subject('Pemberitahuan Diklatlampung');
    $this->email->message($message);
    $this->email->send();

    return $this->success('DATA_CREATED');
  }

  public function resetPassword($params = array())
  {
    if (!$this->setSettings(array(
      'authen' => 'free',
      'requestSource' => $params,
    )))
      return $this->denied();

    $token = $this->request('token');

    $kataSandiBaru = $this->request('kataSandiBaru');
    $ulangiKataSandi = $this->request('ulangiKataSandi');

    $error = array();

    $mForgotAkun = $this->model('vi_forgot_akun');
    $rowForgotAkun = $mForgotAkun->find('token', $token);

    if ($rowForgotAkun) {
      if ($rowForgotAkun['verifiedId'] == 2) {
        $akunId = $rowForgotAkun['akunId'];
        $rowAkun = $this->find($akunId);
      } else
        $error['token'] = $this->string('ACCESS_DENIED');
    } else
      $error['token'] = $this->string('ACCESS_DENIED');

    if (empty($kataSandiBaru))
      $error['kataSandiBaru'] = $this->string('NEW_PASSWORD_REQUIRED');

    if (empty($ulangiKataSandi))
      $error['ulangiKataSandi'] = $this->string('RETYPE_PASSWORD_REQUIRED');

    if (!empty($kataSandiBaru) && !empty($ulangiKataSandi)) {
      if ($ulangiKataSandi != $kataSandiBaru)
        $error['ulangiKataSandi'] = $this->string('PASSWORD_NOT_MATCH');
    }

    if (count($error) > 0)
      return $this->invalid('PLEASE_CORRECT', $error);

    $salt = $this->encrypt();
    $kataSandi = $this->encrypt($kataSandiBaru, $salt);

    $this->update($akunId, array(
      'kataSandi' => $kataSandi,
      'salt' => $salt,
    ));

    $mForgotAkun->update('akunId', $akunId, array(
      'verifiedId' => 1,
    ));

    return $this->success('DATA_UPDATED');
  }

  public function readData($params = array())
  {
    if (!$this->setSettings(array(
      'authen' => 'free',
      'requestSource' => $params,
    )))
      return $this->denied();

    // Order

    $orderBy = $this->request('orderBy');
    $reverse = $this->request('reverse');

    $orderStmt = '';

    if (!empty($orderBy)) {
      $orderStmt = "order by $orderBy";

      if (!empty($reverse)) {
        if ($reverse == 1)
          $orderStmt .= ' desc';
      }
    }

    // Limit

    $page = $this->request('page');
    $count = $this->request('count');
    $unitKerjaId = $this->request('unitKerjaId');
    $instansiId = $this->request('instansiId');
    $statusAkunId = $this->request('statusAkunId');
    $tipeAkunId = 1;

    $limitStmt = '';

    if (!empty($instansiId) && !empty($statusAkunId))
      $rowsCount = $this->getRowsCount("where tipeAkunId = ? and instansiId = ? and statusAkunId = ? ", array($tipeAkunId, $instansiId, $statusAkunId));
    elseif (!empty($instansiId))
      $rowsCount = $this->getRowsCount("where tipeAkunId = ? and instansiId = ? ", array($tipeAkunId, $instansiId));
    elseif (!empty($statusAkunId))
      $rowsCount = $this->getRowsCount("where tipeAkunId = ? and statusAkunId = ? ", array($tipeAkunId, $statusAkunId));
    else
      $rowsCount = $this->getRowsCount($orderStmt);

    $pageCount = 1;

    if (!empty($page) && !empty($count)) {
      $row = ($page * $count) - $count;
      $limitStmt = "limit $row, $count";

      $pageCount = ceil($rowsCount / $count);

      if ($pageCount < 1)
        $pageCount = 1;
    }

    // Query

    if (!empty($instansiId) && !empty($statusAkunId) && !empty($unitKerjaId))
      $rs = $this->select("where tipeAkunId = ? and instansiId = ? and statusAkunId = ? and unitKerjaId = ? " . $orderStmt . ' ' . $limitStmt, array($tipeAkunId, $instansiId, $statusAkunId, $unitKerjaId));
    elseif (!empty($instansiId) && !empty($statusAkunId) && empty($unitKerjaId))
      $rs = $this->select("where tipeAkunId = ? and instansiId = ? and statusAkunId = ? " . $orderStmt . ' ' . $limitStmt, array($tipeAkunId, $instansiId, $statusAkunId));
    elseif (!empty($instansiId) && empty($statusAkunId) && !empty($unitKerjaId))
      $rs = $this->select("where tipeAkunId = ? and instansiId = ? and  unitKerjaId = ? " . $orderStmt . ' ' . $limitStmt, array($tipeAkunId, $instansiId, $unitKerjaId));
    elseif (empty($instansiId) && !empty($statusAkunId) && !empty($unitKerjaId))
      $rs = $this->select("where tipeAkunId = ? and statusAkunId = ? and unitKerjaId = ? " . $orderStmt . ' ' . $limitStmt, array($tipeAkunId, $statusAkunId, $unitKerjaId));
    elseif (!empty($instansiId) && empty($statusAkunId) && empty($unitKerjaId))
      $rs = $this->select("where tipeAkunId = ? and instansiId = ? " . $orderStmt . ' ' . $limitStmt, array($tipeAkunId, $instansiId));
    elseif (empty($instansiId) && !empty($statusAkunId) && empty($unitKerjaId))
      $rs = $this->select("where tipeAkunId = ? and statusAkunId = ? " . $orderStmt . ' ' . $limitStmt, array($tipeAkunId,  $statusAkunId));
    elseif (empty($instansiId) && empty($statusAkunId) && !empty($unitKerjaId))
      $rs = $this->select("where tipeAkunId = ? and unitKerjaId = ? " . $orderStmt . ' ' . $limitStmt, array($tipeAkunId, $unitKerjaId));
    else
      $rs = $this->select($orderStmt . ' ' . $limitStmt);

    $extra = array(
      'rowsCount' => $rowsCount,
      'pageCount' => $pageCount,
    );

    return $this->success('DATA_READ', $rs, $extra);
  }

  public function readDataPending($params = array())
  {
    if (!$this->setSettings(array(
      'authen' => 'free',
      'requestSource' => $params,
    )))
      return $this->denied();

    // Order

    $orderBy = $this->request('orderBy');
    $reverse = $this->request('reverse');

    $orderStmt = '';

    if (!empty($orderBy)) {
      $orderStmt = "order by $orderBy";

      if (!empty($reverse)) {
        if ($reverse == 1)
          $orderStmt .= ' desc';
      }
    }

    // Limit

    $page = $this->request('page');
    $count = $this->request('count');
    $unitKerjaId = $this->request('unitKerjaId');
    $instansiId = $this->request('instansiId');
    $statusAkunId = $this->request('statusAkunId');
    $tipeAkunId = 1;

    $limitStmt = '';

    if (!empty($instansiId) && !empty($statusAkunId))
      $rowsCount = $this->getRowsCount("where statusAkunId = 1 and tipeAkunId = ? and instansiId = ? and statusAkunId = ? ", array($tipeAkunId, $instansiId, $statusAkunId));
    elseif (!empty($instansiId))
      $rowsCount = $this->getRowsCount("where statusAkunId = 1 and tipeAkunId = ? and instansiId = ? ", array($tipeAkunId, $instansiId));
    elseif (!empty($statusAkunId))
      $rowsCount = $this->getRowsCount("where statusAkunId = 1 and tipeAkunId = ? and statusAkunId = ? ", array($tipeAkunId, $statusAkunId));
    else
      $rowsCount = $this->getRowsCount($orderStmt);

    $pageCount = 1;

    if (!empty($page) && !empty($count)) {
      $row = ($page * $count) - $count;
      $limitStmt = "limit $row, $count";

      $pageCount = ceil($rowsCount / $count);

      if ($pageCount < 1)
        $pageCount = 1;
    }

    // Query

    if (!empty($instansiId) && !empty($statusAkunId) && !empty($unitKerjaId))
      $rs = $this->select("where statusAkunId = 1 and tipeAkunId = ? and instansiId = ? and statusAkunId = ? and unitKerjaId = ? " . $orderStmt . ' ' . $limitStmt, array($tipeAkunId, $instansiId, $statusAkunId, $unitKerjaId));
    elseif (!empty($instansiId) && !empty($statusAkunId) && empty($unitKerjaId))
      $rs = $this->select("where statusAkunId = 1 and tipeAkunId = ? and instansiId = ? and statusAkunId = ? " . $orderStmt . ' ' . $limitStmt, array($tipeAkunId, $instansiId, $statusAkunId));
    elseif (!empty($instansiId) && empty($statusAkunId) && !empty($unitKerjaId))
      $rs = $this->select("where statusAkunId = 1 and tipeAkunId = ? and instansiId = ? and  unitKerjaId = ? " . $orderStmt . ' ' . $limitStmt, array($tipeAkunId, $instansiId, $unitKerjaId));
    elseif (empty($instansiId) && !empty($statusAkunId) && !empty($unitKerjaId))
      $rs = $this->select("where statusAkunId = 1 and tipeAkunId = ? and statusAkunId = ? and unitKerjaId = ? " . $orderStmt . ' ' . $limitStmt, array($tipeAkunId, $statusAkunId, $unitKerjaId));
    elseif (!empty($instansiId) && empty($statusAkunId) && empty($unitKerjaId))
      $rs = $this->select("where statusAkunId = 1 and tipeAkunId = ? and instansiId = ? " . $orderStmt . ' ' . $limitStmt, array($tipeAkunId, $instansiId));
    elseif (empty($instansiId) && !empty($statusAkunId) && empty($unitKerjaId))
      $rs = $this->select("where statusAkunId = 1 and tipeAkunId = ? and statusAkunId = ? " . $orderStmt . ' ' . $limitStmt, array($tipeAkunId,  $statusAkunId));
    elseif (empty($instansiId) && empty($statusAkunId) && !empty($unitKerjaId))
      $rs = $this->select("where statusAkunId = 1 and  tipeAkunId = ? and unitKerjaId = ? " . $orderStmt . ' ' . $limitStmt, array($tipeAkunId, $unitKerjaId));
    else
      $rs = $this->select("where statusAkunId = 1 " . $orderStmt . ' ' . $limitStmt);

    $extra = array(
      'rowsCount' => $rowsCount,
      'pageCount' => $pageCount,
    );

    return $this->success('DATA_READ', $rs, $extra);
  }

  public function readDataAkd($params = array())
  {
    if (!$this->setSettings(array(
      'authen' => 'free',
      'requestSource' => $params,
    )))
      return $this->denied();

    // Order

    $orderBy = $this->request('orderBy');
    $reverse = $this->request('reverse');

    $orderStmt = '';

    if (!empty($orderBy)) {
      $orderStmt = "order by $orderBy";

      if (!empty($reverse)) {
        if ($reverse == 1)
          $orderStmt .= ' desc';
      }
    }

    // Limit

    $page = $this->request('page');
    $count = $this->request('count');
    $tipeAkunId = 2;


    $limitStmt = '';

    $rowsCount = $this->getRowsCount("where tipeAkunId = ? ", array($tipeAkunId));

    $pageCount = 1;

    if (!empty($page) && !empty($count)) {
      $row = ($page * $count) - $count;
      $limitStmt = "limit $row, $count";

      $pageCount = ceil($rowsCount / $count);

      if ($pageCount < 1)
        $pageCount = 1;
    }

    // Query

    $rs = $this->select("where tipeAkunId = ? " . $orderStmt . ' ' . $limitStmt, array($tipeAkunId));

    $extra = array(
      'rowsCount' => $rowsCount,
      'pageCount' => $pageCount,
    );

    return $this->success('DATA_READ', $rs, $extra);
  }

  public function filterData($params = array())
  {
    if (!$this->setSettings(array(
      'authen' => 'user',
      'requestSource' => $params,
    )))
      return $this->denied();

    // Query

    $field = $this->request('field');
    $keyword = $this->request('keyword');

    $queryStmt = "where $field like ?";
    $rowsCount = $this->getRowsCount($queryStmt, array("$keyword%"));

    // Limit

    $page = $this->request('page');
    $count = $this->request('count');

    $limitStmt = '';
    $pageCount = 1;

    if (!empty($page) && !empty($count)) {
      $row = ($page * $count) - $count;
      $limitStmt = "limit $row, $count";

      $pageCount = ceil($rowsCount / $count);

      if ($pageCount < 1)
        $pageCount = 1;
    }

    $rs = $this->select($queryStmt, array("$keyword%"));

    $extra = array(
      'rowsCount' => $rowsCount,
      'pageCount' => $pageCount,
    );

    return $this->success('DATA_READ', $rs, $extra);
  }

  public function detailData($params = array())
  {
    if (!$this->setSettings(array(
      'authen' => 'user',
      'requestSource' => $params,
    )))
      return $this->denied();

    $id = $this->request('id');

    // Query

    $rs = $this->find($id);

    if ($rs)
      return $this->success('DATA_READ', $rs);
    else
      return $this->failed('DATA_NOT_FOUND');
  }

  private function checkRequest()
  {
    $nip = $this->request('nip');
    $nama = $this->request('nama');
    $email = $this->request('email');
    $pangkatGolonganId = $this->request('pangkatGolonganId');
    $jabatanEselon = $this->request('jabatanEselon');
    $unitKerjaId = $this->request('unitKerjaId');
    $instansiId = $this->request('instansiId');
    $instansiLainnya = $this->request('instansiLainnya');
    $pendidikanTerakhirId = $this->request('pendidikanTerakhirId');
    $jenisKelaminId = $this->request('jenisKelaminId');
    $tempatLahir = $this->request('tempatLahir');
    $nik = $this->request('nik');
    $npwp = $this->request('npwp');
    $tanggalLahir = $this->request('tanggalLahir');
    $agamaId = $this->request('agamaId');
    $alamat = $this->request('alamat');
    $telpHp = $this->request('telpHp');
    $foto = $this->request('foto');

    $error = array();

    if (empty($nip))
      $error['nip'] = $this->string('NIP_REQUIRED');

    if (empty($nama))
      $error['nama'] = $this->string('NAMA_REQUIRED');

    if (empty($email))
      $error['email'] = $this->string('EMAIL_REQUIRED');

    if (empty($pangkatGolonganId))
      $error['pangkatGolonganId'] = $this->string('PANGKAT_GOLONGAN_REQUIRED');

    if (empty($jabatanEselon))
      $error['jabatanEselon'] = $this->string('JABATAN_ESELON_REQUIRED');

    if (empty($unitKerjaId))
      $error['unitKerjaId'] = $this->string('UNIT_KERJA_REQUIRED');

    if (empty($instansiId))
      $error['instansiId'] = $this->string('INSTANSI_REQUIRED');

    if ($instansiId == '-1') {
      if (empty($instansiLainnya))
        $error['instansiLainnya'] = $this->string('INSTANSI_LAINNYA_REQUIRED');
    }

    if (empty($pendidikanTerakhirId))
      $error['pendidikanTerakhirId'] = $this->string('PENDIDIKAN_TERAKHIR_REQUIRED');

    if (empty($jenisKelaminId))
      $error['jenisKelaminId'] = $this->string('GENDER_REQUIRED');

    if (empty($tempatLahir))
      $error['tempatLahir'] = $this->string('TEMPAT_LAHIR_REQUIRED');

    if (empty($tanggalLahir))
      $error['tanggalLahir'] = $this->string('TANGGAL_LAHIR_REQUIRED');

    if (empty($agamaId))
      $error['agamaId'] = $this->string('AGAMA_REQUIRED');

    if (empty($alamat))
      $error['alamat'] = $this->string('ALAMAT_REQUIRED');

    if (empty($telpHp))
      $error['telpHp'] = $this->string('TELP_HP_REQUIRED');

    // if(empty($foto))
    // 	$error['foto'] = $this->string('FOTO_REQUIRED');

    return $error;
  }

  private function checkRequestProfile()
  {
    $nama = $this->request('nama');
    $pangkatGolonganId = $this->request('pangkatGolonganId');
    $jabatanEselon = $this->request('jabatanEselon');
    $unitKerja = $this->request('unitKerja');
    $instansiId = $this->request('instansiId');
    $instansiLainnya = $this->request('instansiLainnya');
    $pendidikanTerakhirId = $this->request('pendidikanTerakhirId');
    $jenisKelaminId = $this->request('jenisKelaminId');
    $tempatLahir = $this->request('tempatLahir');
    $tanggalLahir = $this->request('tanggalLahir');
    $agamaId = $this->request('agamaId');
    $alamat = $this->request('alamat');
    $telpHp = $this->request('telpHp');
    $foto = $this->request('foto');

    $error = array();


    if (empty($nama))
      $error['nama'] = $this->string('NAMA_REQUIRED');

    if (empty($pangkatGolonganId))
      $error['pangkatGolonganId'] = $this->string('PANGKAT_GOLONGAN_REQUIRED');

    if (empty($jabatanEselon))
      $error['jabatanEselon'] = $this->string('JABATAN_ESELON_REQUIRED');

    if (empty($unitKerja))
      $error['unitKerja'] = $this->string('UNIT_KERJA_REQUIRED');

    if (empty($instansiId))
      $error['instansiId'] = $this->string('INSTANSI_REQUIRED');

    if ($instansiId == '-1') {
      if (empty($instansiLainnya))
        $error['instansiLainnya'] = $this->string('INSTANSI_LAINNYA_REQUIRED');
    }

    if (empty($pendidikanTerakhirId))
      $error['pendidikanTerakhirId'] = $this->string('PENDIDIKAN_TERAKHIR_REQUIRED');

    if (empty($jenisKelaminId))
      $error['jenisKelaminId'] = $this->string('GENDER_REQUIRED');

    if (empty($tempatLahir))
      $error['tempatLahir'] = $this->string('TEMPAT_LAHIR_REQUIRED');

    if (empty($tanggalLahir))
      $error['tanggalLahir'] = $this->string('TANGGAL_LAHIR_REQUIRED');

    if (empty($agamaId))
      $error['agamaId'] = $this->string('AGAMA_REQUIRED');

    if (empty($alamat))
      $error['alamat'] = $this->string('ALAMAT_REQUIRED');

    if (empty($telpHp))
      $error['telpHp'] = $this->string('TELP_HP_REQUIRED');

    if (empty($foto))
      $error['foto'] = $this->string('FOTO_REQUIRED');

    return $error;
  }

  private function checkRequestDashboard()
  {
    $nip = $this->request('nip');
    $nama = $this->request('nama');
    $email = $this->request('email');
    $foto = $this->request('foto');

    $error = array();

    if (empty($nip))
      $error['nip'] = $this->string('NIP_REQUIRED');

    if (empty($nama))
      $error['nama'] = $this->string('NAMA_REQUIRED');

    if (empty($email))
      $error['email'] = $this->string('EMAIL_REQUIRED');

    if (empty($foto))
      $error['foto'] = $this->string('FOTO_REQUIRED');

    return $error;
  }

  private function checkRequestDashboardAkd()
  {
    $nip = $this->request('nip');
    $nama = $this->request('nama');
    $email = $this->request('email');

    $error = array();

    if (empty($nip))
      $error['nip'] = $this->string('NIP_REQUIRED');

    if (empty($nama))
      $error['nama'] = $this->string('NAMA_REQUIRED');

    if (empty($email))
      $error['email'] = $this->string('EMAIL_REQUIRED');


    return $error;
  }

  private function checkRequestSignUp()
  {
    $nip = $this->request('nip');
    $nama = $this->request('nama');
    $email = $this->request('email');

    $error = array();

    if (empty($nip))
      $error['nip'] = $this->string('NIP_REQUIRED');

    if (empty($nama))
      $error['nama'] = $this->string('NAMA_REQUIRED');

    if (empty($email))
      $error['email'] = $this->string('EMAIL_REQUIRED');

    return $error;
  }

  public function login($params = array())
  {
    if (!$this->setSettings(array(
      'authen' => 'free',
      'requestSource' => $params,
    )))
      return $this->denied();

    $nip = $this->request('nip');
    $kataSandi = $this->request('kataSandi');

    $error = array();

    if (empty($nip))
      $error['nip'] = $this->string('NIP_REQUIRED');

    if (empty($kataSandi))
      $error['kataSandi'] = $this->string('PASSWORD_REQUIRED');

    $rowUser = $this->find('nip', $nip);

    if (!empty($rowUser)) {
      if ($rowUser['statusAkunId'] == 1)
        $error['nip'] = $this->string('ACCOUNT_NOT_ACTIVE');

      if ($rowUser['statusAkunId'] == 3)
        $error['nip'] = $this->string('ACCOUNT_SUSPENDED');
    }

    if (count($error) > 0)
      return $this->invalid('PLEASE_CORRECT', $error);

    if (!empty($rowUser)) {
      $kataSandi = $this->encrypt($kataSandi, $rowUser['salt']);

      if ($kataSandi == $rowUser['kataSandi']) {

        $userId = $rowUser['id'];
        $namaUser = $rowUser['nama'];
        $auth = $this->encrypt();
        $type = 'anggota';

        $tipeAkunId = $rowUser['tipeAkunId'];
        $tipeAkun = $rowUser['tipeAkun'];

        // Session

        // session_start();

        $_SESSION['id'] = $userId;
        $_SESSION['nama'] = $namaUser;
        $_SESSION['auth'] = $auth;
        $_SESSION['type'] = $type;

        $_SESSION['tipeAkunId'] = $tipeAkunId;
        $_SESSION['tipeAkun'] = $tipeAkun;

        // return $this->success('LOGIN_SUCCESS');

        return $this->success('LOGIN_SUCCESS', array(
          'auth' => $auth,
          'type' => $type,
          'namaUser' => $namaUser,
          // 'menuAccess' => $menuAccess,
          // 'panelAccess' => $panelAccess,
        ));
      } else
        return $this->invalid('LOGIN_FAILED', array('kataSandi' => $this->string('INVALID_PASSWORD')));
    } else
      return $this->invalid('LOGIN_FAILED', array('kataSandi' => $this->string('USER_NOT_FOUND')));
  }

  public function logout($params = array())
  {
    if (!$this->setSettings(array(
      'authen' => 'free',
      'requestSource' => $params,
    )))
      return $this->denied();

    // session_start();
    session_destroy();

    return $this->success('LOGOUT_SUCCESS');
  }

  public function signup($params = array())
  {
    if (!$this->setSettings(array(
      'authen' => 'free',
      'requestSource' => $params,
    )))
      return $this->denied();

    $error = $this->checkRequestSignUp();

    $nip = $this->request('nip');
    $ulangiNip = $this->request('ulangiNip');
    $nama = $this->request('nama');
    $email = $this->request('email');
    $kataSandi = $this->request('kataSandi');
    $ulangiKataSandi = $this->request('ulangiKataSandi');
    $tipeAkunId = 1;
    $statusAkunId = 1;
    $activeOn = Date('Y-m-d H:i:s');

    if (empty($ulangiNip))
      $error['ulangiNip'] = $this->string('ULANGI_NIP_REQUIRED');

    if (!empty($nip) && !empty($ulangiNip)) {
      if ($ulangiNip != $nip)
        $error['ulangiNip'] = $this->string('ULANGI_NIP_INVALID');
    }

    $rs = $this->find('nip', $nip);

    if ($rs)
      $error['nip'] = $this->string('Nip Sudah terdaftar');

    $rs = $this->find('email', $email);

    if ($rs)
      $error['email'] = $this->string('EMAIL_IS_REG');

    if (empty($kataSandi))
      $error['kataSandi'] = $this->string('PASSWORD_REQUIRED');
    else {
      if (strlen($kataSandi) < 6)
        $error['kataSandi'] = $this->string('PASSWORD_THAN_SIX');
    }

    if (empty($ulangiKataSandi))
      $error['ulangiKataSandi'] = $this->string('RETYPE_PASSWORD_REQUIRED');

    if (!empty($kataSandi) && !empty($ulangiKataSandi)) {

      if (strlen($kataSandi) >= 6) {
        if ($kataSandi != $ulangiKataSandi)
          $error['ulangiKataSandi'] = $this->string('PASSWORD_NOT_MATCH');
      }
    }

    if (count($error) > 0)
      return $this->invalid('PLEASE_CORRECT', $error);

    // if(!empty($foto)) {
    //   if(!$this->moveToArchive($foto))
    //   return $this->failed('FILE_UPLOAD_FAILED');
    // }

    $salt = $this->encrypt();
    $kataSandi = $this->encrypt($kataSandi, $salt);

    $rs = array(
      'nip' => $nip,
      'nama' => $nama,
      // 'foto' => $foto,
      'email' => $email,
      'kataSandi' => $kataSandi,
      'salt' => $salt,
      'registeredOn' => date('Y-m-d H:i:s'),
      'tipeAkunId' => $tipeAkunId,
      // 'statusAkunId' => $statusAkunId,
      'statusAkunId' => 2,
      'activeOn' => $activeOn,
    );

    $this->insert($rs);

    // Verifikasi

    // $akunId = $this->getLastInsertId();
    // $token = $this->encrypt();

    // $mVerifikasiAkun = $this->model('tb_verifikasi_akun');

    // $mVerifikasiAkun->insert(array(
    //   'akunId' => $akunId,
    //   'token' => $token,
    //   'verifiedId' => 2,
    // ));

    // // $message = 'Silakan verifikasi Akun diklatlampung.info Anda dengan membuka halaman ini - <a href="https://siapdiklat.bpsdm.lampungprov.go.id/verifikasi/aktifasi/'.$token.'">Akun Anda baru dapat digunakan setelah melakukan verifikasi ini</a>';
    // $message =
    // "Silakan verifikasi Akun siapdiklat.bpsdm.lampungprov.go.id Anda dengan membuka halaman ini atau dengan copy link : ".
    // base_url()."verifikasi/aktifasi/$token Akun Anda baru dapat digunakan setelah melakukan verifikasi ini";

    // // mail function

    // $config = Array(
    //   'protocol' => 'smtp',
    //     'smtp_host' => 'ssl://bpsdm.lampungprov.go.id',
    //     'smtp_port' => 465,
    //     'smtp_timeout' => 5,
    //     'smtp_user' => '_mainaccount@bpsdm.lampungprov.go.id',
    //     'smtp_pass' => 'Bpsdmlp9^^v(((',
    //     'mailtype'  => 'html',
    //     'charset'   => 'iso-8859-1'
    // );

    // $this->load->library('email');
    // $this->email->set_newline("\r\n");

    // $this->email->initialize($config);
    // $this->email->from($config['smtp_user']);
    // $this->email->to($email);

    // $this->email->subject('Pemberitahuan Diklatlampung');
    // $this->email->message($message);


    // $this->email->send();

    // return $this->success('DATA_CREATED', $email);
    return $this->success('DATA_CREATED', $rs);
  }

  public function createData($params = array())
  {
    if (!$this->setSettings(array(
      'authen' => 'user',
      'requestSource' => $params,
    )))
      return $this->denied();

    $error = $this->checkRequest();

    $nip = $this->request('nip');
    $nama = $this->request('nama');
    $foto = $this->request('foto');
    $email = $this->request('email');
    $password = $this->request('password');
    $retype_password = $this->request('retype_password');
    $pangkatGolonganId = $this->request('pangkatGolonganId');
    $jabatanEselon = $this->request('jabatanEselon');
    $unitKerjaId = $this->request('unitKerjaId');
    $instansiId = $this->request('instansiId');
    $instansiLainnya = $this->request('instansiLainnya');
    $pendidikanTerakhirId = $this->request('pendidikanTerakhirId');
    $jenisKelaminId = $this->request('jenisKelaminId');
    $tempatLahir = $this->request('tempatLahir');
    $tanggalLahir = $this->request('tanggalLahir');
    $agamaId = $this->request('agamaId');
    $alamat = $this->request('alamat');
    $telpHp = $this->request('telpHp');
    $statusAkunId = $this->request('statusAkunId');
    $activeOn = $this->request('activeOn');

    $rs = $this->find('nip', $nip);

    if ($rs)
      $error['nip'] = $this->string('Nip sudah terdaftar');

    $rs = $this->find('email', $email);

    if ($rs)
      $error['email'] = $this->string('EMAIL_IS_REG');

    if (empty($password))
      $error['password'] = $this->string('PASSWORD_REQUIRED');
    else {
      if (strlen($password) < 6)
        $error['password'] = $this->string('PASSWORD_THAN_SIX');
    }

    if (empty($retype_password))
      $error['retype_password'] = $this->string('RETYPE_PASSWORD_REQUIRED');

    if (!empty($password) && !empty($retype_password)) {

      if (strlen($password) >= 6) {
        if ($password != $retype_password)
          $error['retype_password'] = $this->string('PASSWORD_NOT_MATCH');
      }
    }

    if (count($error) > 0)
      return $this->invalid('PLEASE_CORRECT', $error);

    if (!empty($foto)) {
      if (!$this->moveToArchive($foto))
        return $this->failed('FILE_UPLOAD_FAILED');
    }

    $salt = $this->encrypt();
    $password = $this->encrypt($password, $salt);

    $rs = array(
      'nip' => $nip,
      'nama' => $nama,
      'foto' => $foto,
      'email' => $email,
      'kataSandi' => $password,
      'pangkatGolonganId' => $pangkatGolonganId,
      'jabatanEselon' => $jabatanEselon,
      'unitKerjaId' => $unitKerjaId,
      'instansiId' => $instansiId,
      'instansiLainnya' => $instansiLainnya,
      'pendidikanTerakhirId' => $pendidikanTerakhirId,
      'jenisKelaminId' => $jenisKelaminId,
      'tempatLahir' => $tempatLahir,
      'tanggalLahir' => $tanggalLahir,
      'agamaId' => $agamaId,
      'alamat' => $alamat,
      'telpHp' => $telpHp,
      'registeredOn' => date('Y-m-d H:i:s'),
      'activeOn' => date('Y-m-d H:i:s'),
      'statusAkunId' => $statusAkunId,
      'tipeAkunId' => 1,
    );

    $this->insert($rs);

    return $this->success('DATA_CREATED');
  }

  public function createDataAkd($params = array())
  {
    if (!$this->setSettings(array(
      'authen' => 'user',
      'requestSource' => $params,
    )))
      return $this->denied();

    $error = $this->checkRequestDashboardAkd();

    $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
    $pass = array();
    $alphaLength = strlen($alphabet) - 1;
    for ($i = 0; $i < 8; $i++) {
      $n = rand(0, $alphaLength);
      $pass[] = $alphabet[$n];
    }

    $password = implode($pass);

    $nip = $this->request('nip');
    $nama = $this->request('nama');
    $email = $this->request('email');

    $rs = $this->find('email', $email);

    if ($rs)
      $error['email'] = $this->string('EMAIL_IS_REG');

    if (count($error) > 0)
      return $this->invalid('PLEASE_CORRECT', $error);

    $salt = $this->encrypt();
    $password1 = $this->encrypt($password, $salt);

    $rs = array(
      'nip' => $nip,
      'nama' => $nama,
      'email' => $email,
      'kataSandi' => $password1,
      'salt' => $salt,
      'registeredOn' => date('Y-m-d H:i:s'),
      'statusAkunId' => 2,
      'tipeAkunId' => 2,
    );

    $this->insert($rs);


    $config = array(
      'protocol' => 'smtp',
      'smtp_host' => 'ssl://bpsdm.lampungprov.go.id',
      'smtp_port' => 465,
      'smtp_timeout' => 5,
      'smtp_user' => '_mainaccount@bpsdm.lampungprov.go.id',
      'smtp_pass' => 'Bpsdmlp9^^v(((',
      'mailtype'  => 'html',
      'charset'   => 'iso-8859-1'
    );

    $this->load->library('email', $config);
    $this->email->set_newline("\r\n");

    $this->email->initialize($config);

    $this->email->from('f.anaturdasa@diklatlampung.info', 'Diklatlampung');
    $this->email->to($email);

    $this->email->subject('Pemberitahuan Diklatlampung');
    $this->email->message(
      'Anda telah terdaftar sebagai member di http://diklatlampung.info ' .
        'silakan melakukan login dengan data sebagai berikut;<br><br>' .
        'User: ' . $nip . '<br>' .
        'Kata Sandi: ' . $password . '<br>'
    );

    $this->email->send();

    return $this->success('DATA_CREATED');
  }

  public function updateData($params = array())
  {
    if (!$this->setSettings(array(
      'authen' => 'free',
      'requestSource' => $params,
    )))
      return $this->denied();

    $id = $this->request('id');
    $nip = $this->request('nip');
    $nama = $this->request('nama');
    $nik = $this->request('nik');
    $npwp = $this->request('npwp');
    $foto = $this->request('foto');
    $email = $this->request('email');
    $pangkatGolonganId = $this->request('pangkatGolonganId');
    $jabatanEselon = $this->request('jabatanEselon');
    $unitKerjaId = $this->request('unitKerjaId');
    $instansiId = $this->request('instansiId');
    $instansiLainnya = $this->request('instansiLainnya');
    $pendidikanTerakhirId = $this->request('pendidikanTerakhirId');
    $jenisKelaminId = $this->request('jenisKelaminId');
    $tempatLahir = $this->request('tempatLahir');
    $tanggalLahir = $this->request('tanggalLahir');
    $agamaId = $this->request('agamaId');
    $alamat = $this->request('alamat');
    $telpHp = $this->request('telpHp');
    $statusAkunId = $this->request('statusAkunId');

    $error = $this->checkRequest();

    $rs = $this->find($id);
    if (!$rs)
      return $this->failed('DATA_NOT_FOUND');

    $last_nip = $rs['nip'];

    if ($nip != $last_nip) {
      $rsFound = $this->find('nip', $nip);

      if ($rsFound)
        $error['nip'] = $this->string('Nip sudah terdaftar');
    }

    $last_email = $rs['email'];

    if ($email != $last_email) {
      $rsFound = $this->find('email', $email);

      if ($rsFound)
        $error['email'] = $this->string('EMAIL_IS_REG');
    }

    if (count($error) > 0)
      return $this->invalid('PLEASE_CORRECT', $error);

    if (!empty($foto)) {
      if ($foto != $rs['foto']) {

        $last_foto = $rs['foto'];
        $this->deleteArchive($last_foto);

        if (!$this->moveToArchive($foto))
          return $this->failed('FILE_UPLOAD_FAILED');
      }
    }

    $rs = array(
      'nip' => $nip,
      'nama' => $nama,
      'nik' => $nik,
      'npwp' => $npwp,
      'foto' => $foto,
      'email' => $email,
      'pangkatGolonganId' => $pangkatGolonganId,
      'jabatanEselon' => $jabatanEselon,
      'unitKerjaId' => $unitKerjaId,
      'instansiId' => $instansiId,
      'instansiLainnya' => $instansiLainnya,
      'pendidikanTerakhirId' => $pendidikanTerakhirId,
      'jenisKelaminId' => $jenisKelaminId,
      'tempatLahir' => $tempatLahir,
      'tanggalLahir' => $tanggalLahir,
      'agamaId' => $agamaId,
      'alamat' => $alamat,
      'telpHp' => $telpHp,
      'statusAkunId' => $statusAkunId,
    );

    $this->update($id, $rs);
    return $this->success('DATA_UPDATED');
  }

  public function updateDataPending($params = array())
  {
    if (!$this->setSettings(array(
      'authen' => 'user',
      'requestSource' => $params,
    )))
      return $this->denied();

    $id = $this->request('id');
    $statusAkunId = 2;

    $error = $this->checkRequest();

    $rs = $this->find($id);
    if (!$rs)
      return $this->failed('DATA_NOT_FOUND');

    $rs = array(
      'statusAkunId' => $statusAkunId,
    );

    $this->update($id, $rs);
    return $this->success('DATA_UPDATED');
  }

  public function updateDataProfile($params = array())
  {
    if (!$this->setSettings(array(
      'authen' => 'free',
      'requestSource' => $params,
    )))
      return $this->denied();

    $id = $this->request('id');
    $nip = $this->request('nip');
    $nama = $this->request('nama');
    $foto = $this->request('foto');
    $email = $this->request('email');
    $pangkatGolonganId = $this->request('pangkatGolonganId');
    $jabatanEselon = $this->request('jabatanEselon');
    $unitKerja = $this->request('unitKerja');
    $instansiId = $this->request('instansiId');
    $instansiLainnya = $this->request('instansiLainnya');
    $pendidikanTerakhirId = $this->request('pendidikanTerakhirId');
    $jenisKelaminId = $this->request('jenisKelaminId');
    $tempatLahir = $this->request('tempatLahir');
    $nik = $this->request('nik');
    $npwp = $this->request('npwp');
    $tanggalLahir = $this->request('tanggalLahir');
    $agamaId = $this->request('agamaId');
    $alamat = $this->request('alamat');
    $alamatKantor = $this->request('alamatKantor');
    $telpHp = $this->request('telpHp');
    $suratPerintahTugas = $this->request('suratPerintahTugas');
    $suratPernyataanBebasTugasKedinasan = $this->request('suratPernyataanBebasTugasKedinasan');
    $suratKeputusanCPNS = $this->request('suratKeputusanCPNS');
    $suratKeputusanPangkat = $this->request('suratKeputusanPangkat');
    $suratKeputusanJabatanTerakhir = $this->request('suratKeputusanJabatanTerakhir');
    $ijazahTerakhir = $this->request('ijazahTerakhir');
    $suratKesehatan = $this->request('suratKesehatan');
    $suratKeteranganDokterdariRumahSakitPemerintah = $this->request('suratKeteranganDokterdariRumahSakitPemerintah');
    $askesBPJSatauKIS = $this->request('askesBPJSatauKIS');
    $jumlahUploadDiklat = $this->request('jumlahUploadDiklat');

    $error = $this->checkRequestProfile();

    $rs = $this->find($id);
    if (!$rs)
      return $this->failed('DATA_NOT_FOUND');

    if (count($error) > 0)
      return $this->invalid('PLEASE_CORRECT', $error);

    if (!empty($foto)) {
      if ($foto != $rs['foto']) {

        $last_foto = $rs['foto'];
        $this->deleteArchive($last_foto);

        if (!$this->moveToArchive($foto))
          return $this->failed('FILE_UPLOAD_FAILED');
      }
    }

    $last_suratPerintahTugas = $rs['suratPerintahTugas'];

    if (!empty($suratPerintahTugas)) {
      if ($suratPerintahTugas != $last_suratPerintahTugas) {

        $this->deleteArchive($last_suratPerintahTugas);

        if (!$this->moveToArchive($suratPerintahTugas))
          return $this->failed('FILE_UPLOAD_FAILED');
      }
      $suratPerintahTugas1 = 1;
    } else {
      $suratPerintahTugas1 = 0;
    }

    $last_suratPernyataanBebasTugasKedinasan = $rs['suratPernyataanBebasTugasKedinasan'];

    if (!empty($suratPernyataanBebasTugasKedinasan)) {
      if ($suratPernyataanBebasTugasKedinasan != $last_suratPernyataanBebasTugasKedinasan) {

        $this->deleteArchive($last_suratPernyataanBebasTugasKedinasan);

        if (!$this->moveToArchive($suratPernyataanBebasTugasKedinasan))
          return $this->failed('FILE_UPLOAD_FAILED');
      }
      $suratPernyataanBebasTugasKedinasan1 = 2;
    } else {
      $suratPernyataanBebasTugasKedinasan1 = 0;
    }

    $last_suratKeputusanCPNS = $rs['suratKeputusanCPNS'];

    if (!empty($suratKeputusanCPNS)) {
      if ($suratKeputusanCPNS != $last_suratKeputusanCPNS) {

        $this->deleteArchive($last_suratKeputusanCPNS);

        if (!$this->moveToArchive($suratKeputusanCPNS))
          return $this->failed('FILE_UPLOAD_FAILED');
      }
      $suratKeputusanCPNS1 = 3;
    } else {
      $suratKeputusanCPNS1 = 0;
    }

    $last_suratKeputusanPangkat = $rs['suratKeputusanPangkat'];

    if (!empty($suratKeputusanPangkat)) {
      if ($suratKeputusanPangkat != $last_suratKeputusanPangkat) {

        $this->deleteArchive($last_suratKeputusanPangkat);

        if (!$this->moveToArchive($suratKeputusanPangkat))
          return $this->failed('FILE_UPLOAD_FAILED');
      }
      $suratKeputusanPangkat1 = 4;
    } else {
      $suratKeputusanPangkat1 = 0;
    }

    $last_suratKeputusanJabatanTerakhir = $rs['suratKeputusanJabatanTerakhir'];

    if (!empty($suratKeputusanJabatanTerakhir)) {
      if ($suratKeputusanJabatanTerakhir != $last_suratKeputusanJabatanTerakhir) {

        $this->deleteArchive($last_suratKeputusanJabatanTerakhir);

        if (!$this->moveToArchive($suratKeputusanJabatanTerakhir))
          return $this->failed('FILE_UPLOAD_FAILED');
      }
      $suratKeputusanJabatanTerakhir1 = 5;
    } else {
      $suratKeputusanJabatanTerakhir1 = 0;
    }

    $last_ijazahTerakhir = $rs['ijazahTerakhir'];

    if (!empty($ijazahTerakhir)) {
      if ($ijazahTerakhir != $last_ijazahTerakhir) {

        $this->deleteArchive($last_ijazahTerakhir);

        if (!$this->moveToArchive($ijazahTerakhir))
          return $this->failed('FILE_UPLOAD_FAILED');
      }
      $ijazahTerakhir1 = 6;
    } else {
      $ijazahTerakhir1 = 0;
    }

    $last_suratKesehatan = $rs['suratKesehatan'];

    if (!empty($suratKesehatan)) {
      if ($suratKesehatan != $last_suratKesehatan) {

        $this->deleteArchive($last_suratKesehatan);

        if (!$this->moveToArchive($suratKesehatan))
          return $this->failed('FILE_UPLOAD_FAILED');
      }
      $suratKesehatan1 = 9;
    } else {
      $suratKesehatan1 = 0;
    }


    $last_suratKeteranganDokterdariRumahSakitPemerintah = $rs['suratKeteranganDokterdariRumahSakitPemerintah'];

    if (!empty($suratKeteranganDokterdariRumahSakitPemerintah)) {
      if ($suratKeteranganDokterdariRumahSakitPemerintah != $last_suratKeteranganDokterdariRumahSakitPemerintah) {

        $this->deleteArchive($last_suratKeteranganDokterdariRumahSakitPemerintah);

        if (!$this->moveToArchive($suratKeteranganDokterdariRumahSakitPemerintah))
          return $this->failed('FILE_UPLOAD_FAILED');
      }
      $suratKeteranganDokterdariRumahSakitPemerintah1 = 7;
    } else {
      $suratKeteranganDokterdariRumahSakitPemerintah1 = 0;
    }

    $last_askesBPJSatauKIS = $rs['askesBPJSatauKIS'];

    if (!empty($askesBPJSatauKIS)) {
      if ($askesBPJSatauKIS != $last_askesBPJSatauKIS) {

        $this->deleteArchive($last_askesBPJSatauKIS);

        if (!$this->moveToArchive($askesBPJSatauKIS))
          return $this->failed('FILE_UPLOAD_FAILED');
      }
      $askesBPJSatauKIS1 = 8;
    } else {
      $askesBPJSatauKIS1 = 0;
    }

    $mUnitKerja = $this->model('tb_unit_kerja');
    $rsUnitKerja = $mUnitKerja->single('where unitKerja = ? ', array($unitKerja));

    if ($rsUnitKerja)
      $unitKerjaId = $rsUnitKerja['id'];
    else {
      $rsUnitKerja = array(
        'unitKerja' => $unitKerja,
      );
      $mUnitKerja->insert($rsUnitKerja);
      $unitKerjaId = $mUnitKerja->getLastInsertId();
    }

    $rs = array(
      'nama' => $nama,
      'foto' => $foto,
      'pangkatGolonganId' => $pangkatGolonganId,
      'jabatanEselon' => $jabatanEselon,
      'unitKerjaId' => $unitKerjaId,
      'instansiId' => $instansiId,
      'instansiLainnya' => $instansiLainnya,
      'pendidikanTerakhirId' => $pendidikanTerakhirId,
      'jenisKelaminId' => $jenisKelaminId,
      'tempatLahir' => $tempatLahir,
      'tanggalLahir' => $tanggalLahir,
      'nik' => $nik,
      'npwp' => $npwp,
      'agamaId' => $agamaId,
      'alamat' => $alamat,
      'alamatKantor' => $alamatKantor,
      'telpHp' => $telpHp,
      'suratPerintahTugas' => $suratPerintahTugas,
      'suratPernyataanBebasTugasKedinasan' => $suratPernyataanBebasTugasKedinasan,
      'suratKeputusanCPNS' => $suratKeputusanCPNS,
      'suratKeputusanPangkat' => $suratKeputusanPangkat,
      'suratKeputusanJabatanTerakhir' => $suratKeputusanJabatanTerakhir,
      'ijazahTerakhir' => $ijazahTerakhir,
      'suratKesehatan' => $suratKesehatan,
      'suratKeteranganDokterdariRumahSakitPemerintah' => $suratKeteranganDokterdariRumahSakitPemerintah,
      'askesBPJSatauKIS' => $askesBPJSatauKIS,
    );

    $mItems = $this->model('vi_pendaftaran_diklat');
    $rsDaftar = $mItems->select('where akunId = ? and statusDiklatId = 1', array($id));
    if ($rsDaftar) {
      $idDaftar = $rsDaftar[0]['id'];
      $diklatId = $rsDaftar[0]['diklatId'];

      $mDiklat = $this->model('vi_diklat');
      $rsDiklat = $mDiklat->find($diklatId);
      $jumlahUpload = $rsDiklat['jumlahUpload'];
      if ($jumlahUploadDiklat == $jumlahUpload)
        $statusUpload = "Selesai";
      else
        $statusUpload = "Belum";

      $mAnggota = $this->model('tb_pendaftaran_diklat');
      $mAnggota->update($idDaftar, array(
        'statusUpload' => $statusUpload,
      ));
      $mBerkasDiklat = $this->model('tb_pendaftaran_diklat_berkas');
      if ($suratPerintahTugas1 > 0) {
        $rsPendafBerkas = $mBerkasDiklat->select("where pendaftaranDiklatId = ? and berkasPendaftaranId = ? ", array($idDaftar, $suratPerintahTugas1));
        if ($rsPendafBerkas)
          $mBerkasDiklat->update($rsPendafBerkas[0]['id'], array('berkas' => $suratPerintahTugas, 'status' => 'Selesai'));
      }

      if ($suratPernyataanBebasTugasKedinasan1 > 0) {
        $rsPendafBerkas = $mBerkasDiklat->select("where pendaftaranDiklatId = ? and berkasPendaftaranId = ? ", array($idDaftar, $suratPernyataanBebasTugasKedinasan1));
        if ($rsPendafBerkas)
          $mBerkasDiklat->update($rsPendafBerkas[0]['id'], array('berkas' => $suratPernyataanBebasTugasKedinasan, 'status' => 'Selesai'));
      }

      if ($suratKeputusanCPNS1 > 0) {
        $rsPendafBerkas = $mBerkasDiklat->select("where pendaftaranDiklatId = ? and berkasPendaftaranId = ? ", array($idDaftar, $suratKeputusanCPNS1));
        if ($rsPendafBerkas)
          $mBerkasDiklat->update($rsPendafBerkas[0]['id'], array('berkas' => $suratKeputusanCPNS, 'status' => 'Selesai'));
      }

      if ($suratKeputusanPangkat1 > 0) {
        $rsPendafBerkas = $mBerkasDiklat->select("where pendaftaranDiklatId = ? and berkasPendaftaranId = ? ", array($idDaftar, $suratKeputusanPangkat1));
        if ($rsPendafBerkas)
          $mBerkasDiklat->update($rsPendafBerkas[0]['id'], array('berkas' => $suratKeputusanPangkat, 'status' => 'Selesai'));
      }

      if ($suratKeputusanJabatanTerakhir1 > 0) {
        $rsPendafBerkas = $mBerkasDiklat->select("where pendaftaranDiklatId = ? and berkasPendaftaranId = ? ", array($idDaftar, $suratKeputusanJabatanTerakhir1));
        if ($rsPendafBerkas)
          $mBerkasDiklat->update($rsPendafBerkas[0]['id'], array('berkas' => $suratKeputusanJabatanTerakhir, 'status' => 'Selesai'));
      }

      if ($ijazahTerakhir1 > 0) {
        $rsPendafBerkas = $mBerkasDiklat->select("where pendaftaranDiklatId = ? and berkasPendaftaranId = ? ", array($idDaftar, $ijazahTerakhir1));
        if ($rsPendafBerkas)
          $mBerkasDiklat->update($rsPendafBerkas[0]['id'], array('berkas' => $ijazahTerakhir, 'status' => 'Selesai'));
      }

      if ($suratKesehatan1 > 0) {
        $rsPendafBerkas = $mBerkasDiklat->select("where pendaftaranDiklatId = ? and berkasPendaftaranId = ? ", array($idDaftar, $suratKesehatan1));
        if ($rsPendafBerkas)
          $mBerkasDiklat->update($rsPendafBerkas[0]['id'], array('berkas' => $suratKesehatan, 'status' => 'Selesai'));
        else
          $mBerkasDiklat->insert(array(
            'pendaftaranDiklatId' => $idDaftar,
            'berkasPendaftaranId' => 9,
            'berkas' => '',
            'status' => 'Selesai',
          ));
      }


      if ($suratKeteranganDokterdariRumahSakitPemerintah1 > 0) {
        $rsPendafBerkas = $mBerkasDiklat->select("where pendaftaranDiklatId = ? and berkasPendaftaranId = ? ", array($idDaftar, $suratKeteranganDokterdariRumahSakitPemerintah1));
        if ($rsPendafBerkas)
          $mBerkasDiklat->update($rsPendafBerkas[0]['id'], array('berkas' => $suratKeteranganDokterdariRumahSakitPemerintah, 'status' => 'Selesai'));
      }

      if ($askesBPJSatauKIS1 > 0) {
        $rsPendafBerkas = $mBerkasDiklat->select("where pendaftaranDiklatId = ? and berkasPendaftaranId = ? ", array($idDaftar, $askesBPJSatauKIS1));
        if ($rsPendafBerkas)
          $mBerkasDiklat->update($rsPendafBerkas[0]['id'], array('berkas' => $askesBPJSatauKIS, 'status' => 'Selesai'));
      }
    };

    $this->update($id, $rs);

    return $this->success('DATA_UPDATED');
  }

  public function updateDataDashboard($params = array())
  {
    if (!$this->setSettings(array(
      'authen' => 'user',
      'requestSource' => $params,
    )))
      return $this->denied();

    $id = $this->request('id');
    $nip = $this->request('nip');
    $nama = $this->request('nama');
    $foto = $this->request('foto');
    $email = $this->request('email');

    $statusAkunId = $this->request('statusAkunId');

    $error = $this->checkRequestDashboard();

    $rs = $this->find($id);
    if (!$rs)
      return $this->failed('DATA_NOT_FOUND');

    $last_email = $rs['email'];

    if ($email != $last_email) {
      $rsFound = $this->find('email', $email);

      if ($rsFound)
        $error['email'] = $this->string('EMAIL_IS_REG');
    }

    if (count($error) > 0)
      return $this->invalid('PLEASE_CORRECT', $error);

    if (!empty($foto)) {
      if ($foto != $rs['foto']) {

        $last_foto = $rs['foto'];
        $this->deleteArchive($last_foto);

        if (!$this->moveToArchive($foto))
          return $this->failed('FILE_UPLOAD_FAILED');
      }
    }

    $rs = array(
      'nip' => $nip,
      'nama' => $nama,
      'foto' => $foto,
      'email' => $email,
      'statusAkunId' => $statusAkunId,
    );

    $this->update($id, $rs);
    return $this->success('DATA_UPDATED');
  }

  public function updateDataDashboardAkd($params = array())
  {
    if (!$this->setSettings(array(
      'authen' => 'user',
      'requestSource' => $params,
    )))
      return $this->denied();

    $id = $this->request('id');
    $nip = $this->request('nip');
    $nama = $this->request('nama');
    $email = $this->request('email');

    $error = $this->checkRequestDashboardAkd();

    $rs = $this->find($id);
    if (!$rs)
      return $this->failed('DATA_NOT_FOUND');

    $last_email = $rs['email'];

    if ($email != $last_email) {
      $rsFound = $this->find('email', $email);

      if ($rsFound)
        $error['email'] = $this->string('EMAIL_IS_REG');
    }

    if (count($error) > 0)
      return $this->invalid('PLEASE_CORRECT', $error);

    $rs = array(
      'nip' => $nip,
      'nama' => $nama,
      'email' => $email,
    );

    $this->update($id, $rs);
    return $this->success('DATA_UPDATED');
  }

  public function sendEmailAkd($params = array())
  {
    if (!$this->setSettings(array(
      'authen' => 'user',
      'requestSource' => $params,
    )))
      return $this->denied();

    $id = $this->request('id');
    $rs = $this->find($id);
    $email = $rs['email'];

    if (!$rs)
      return $this->failed('DATA_NOT_FOUND');

    $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
    $pass = array();
    $alphaLength = strlen($alphabet) - 1;
    for ($i = 0; $i < 8; $i++) {
      $n = rand(0, $alphaLength);
      $pass[] = $alphabet[$n];
    }

    $password = implode($pass);

    $salt = $this->encrypt();
    $password1 = $this->encrypt($password, $salt);

    $rs = array(
      'kataSandi' => $password1,
      'salt' => $salt,
    );

    $this->update($id, $rs);

    $config = array(
      'protocol' => 'smtp',
      'smtp_host' => 'ssl://bpsdm.lampungprov.go.id',
      'smtp_port' => 465,
      'smtp_timeout' => 5,
      'smtp_user' => '_mainaccount@bpsdm.lampungprov.go.id',
      'smtp_pass' => 'Bpsdmlp9^^v(((',
      'mailtype'  => 'html',
      'charset'   => 'iso-8859-1'
    );



    $this->load->library('email', $config);
    $this->email->set_newline("\r\n");

    $this->email->initialize($config);

    $this->email->from('f.anaturdasa@diklatlampung.info', 'Diklatlampung');
    $this->email->to($email);

    $this->email->subject('Pemberitahuan Diklatlampung');
    $this->email->message(
      'Kata Sandi http://diklatlampung.info Anda telah direset, berikut ini Kata Sandi Anda yg baru; <br><br>' .
        'Kata Sandi: ' . $password
    );

    $this->email->send();

    return $this->success('PASSWORD_SEND_TO_MEMBER_EMAIL');
  }

  public function updatePassword($params = array())
  {
    if (!$this->setSettings(array(
      'authen' => 'user',
      'requestSource' => $params,
    )))
      return $this->denied();

    $id = $this->request('id');

    if (empty($id))
      return $this->failed('ID_REQUIRED');

    $rs = $this->find($id);

    if (!$rs)
      return $this->failed('DATA_NOT_FOUND');

    $error = array();

    $new_password = $this->request('new_password');
    $retype_password = $this->request('retype_password');

    if (empty($new_password))
      $error['new_password'] = $this->string('NEW_PASSWORD_REQUIRED');
    else {
      if (strlen($new_password) < 6)
        $error['new_password'] = $this->string('PASSWORD_THAN_SIX');
    }

    if (empty($retype_password))
      $error['retype_password'] = $this->string('RETYPE_PASSWORD_REQUIRED');

    if (!empty($new_password) && !empty($retype_password)) {

      if (strlen($new_password) >= 6) {
        if ($new_password != $retype_password)
          $error['retype_password'] = $this->string('PASSWORD_NOT_MATCH');
      }
    }

    if (count($error) > 0)
      return $this->invalid('PLEASE_CORRECT', $error);

    $salt = $this->encrypt();
    $new_password = $this->encrypt($new_password, $salt);

    $rs = array(
      'kataSandi' => $new_password,
      'salt' => $salt,
    );

    $this->update($id, $rs);
    return $this->success('PASSWORD_CHANGED');
  }

  public function deleteData($params = array())
  {
    if (!$this->setSettings(array(
      'authen' => 'user',
      'requestSource' => $params,
    )))
      return $this->denied();

    $id = $this->request('id');
    $rs = $this->find($id);

    if (!empty($rs['foto']))
      $this->deleteArchive($rs['foto']);

    $this->delete($id);

    return $this->success('DATA_DELETED');
  }

  public function multipleDeleteData($params = array())
  {
    if (!$this->setSettings(array(
      'authen' => 'user',
      'requestSource' => $params,
    )))
      return $this->denied();

    $multipleId = $this->request('multipleId');

    foreach ($multipleId as $id) {
      $rs = $this->find($id);

      if (!empty($rs['foto']))
        $this->deleteArchive($rs['foto']);

      $this->delete($id);
    }

    return $this->success('DATA_DELETED');
  }

  public function createPDFDetail($params = array())
  {
    if (!$this->setSettings(array(
      'authen' => 'user',
      'requestSource' => $params,
    )))
      return $this->denied();

    // PDF Settings

    $this->load->library('pdf');
    $this->pdf->setSettings(array(
      'strings' => $this->config->item('strings'),
      'title' => 'Anggota',
      'pageOrientation' => 'P',
    ));

    // Request

    $id = $this->request('id');

    // Query

    $row = $this->find($id);

    // Output

    if (!empty($row)) {
      $fileUrl = $this->pdf->render(
        'AnggotaDetail',
        $row
      );

      return $this->success('FILE_CREATED', $fileUrl);
    } else
      return $this->failed('FILE_CREATE_FAILED');
  }
}

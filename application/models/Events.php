<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Events extends MY_Model {
    protected $table = 'tb_events';
    protected $view = 'vi_events';

    public function readData($params = array())
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();

        // Order

        $orderBy = $this->request('orderBy');
        $reverse = $this->request('reverse');

        $orderStmt = '';

        if (!empty($orderBy)) {
            $orderStmt = "order by $orderBy";

            if (!empty($reverse)) {
                if ($reverse == 1)
                    $orderStmt .= ' desc';
            }
        }

        // Limit

        $publishStatusId = $this->request('publishStatusId');
        
        $page = $this->request('page');
		    $count = $this->request('count');

        $limitStmt = '';

        if (!empty($publishStatusId))
            $rowsCount = $this->getRowsCount("where publishStatusId = ? ", array($publishStatusId));
        else
            $rowsCount = $this->getRowsCount($orderStmt);

        $pageCount = 1;

        if (!empty($page) && !empty($count)) {
            $row = ($page * $count) - $count;
            $limitStmt = "limit $row, $count";

            $pageCount = ceil($rowsCount / $count);

            if($pageCount < 1)
                $pageCount = 1;
        }

        // Query

        $bulan = $this->request('bulan');
        $tahun = $this->request('tahun');
        $tahunNow = date('Y');
        
        if (!empty($bulan) && !empty($tahun) && !empty($publishStatusId))
        	$rs = $this->select("where month(_eventsDate) = ? and  year(_eventsDate) = ? and publishStatusId = ? ".$orderStmt.' '.$limitStmt, array($bulan, $tahun, $publishStatusId));
        elseif (!empty($bulan) && !empty($tahun))
            $rs = $this->select("where month(_eventsDate) = ? and  year(_eventsDate) = ? ".$orderStmt.' '.$limitStmt, array($bulan, $tahun));
        elseif (!empty($publishStatusId) )
            $rs = $this->select("where publishStatusId = ? ".$orderStmt.' '.$limitStmt, array($publishStatusId));
        else
        	$rs = $this->select("where year(_eventsDate) = ? ".$orderStmt.' '.$limitStmt, array($tahunNow));
            

		    $extra = array(
            'rowsCount' => $rowsCount,
            'pageCount' => $pageCount,
        );

		    return $this->success('DATA_READ', $rs, $extra);
	  }

	  public function readDataDistinct($params = array())
	  {
	  	if(!$this->setSettings(array(
	  			'authen' => 'free',
	  			'requestSource' => $params,
	  	)))
	  		return $this->denied();
	  
	  	// query
	  
	  	$mEvents = $this->model('vi_events');
	  
	  	$rsEvents = $mEvents->query('select year(_eventsDate) as tahun from vi_events group by eventsDate desc');
	  
	  	return $this->success('DATA_READ', $rsEvents);
	  }
	  
    public function filterData($params = array())
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();

        // Query

        $field = $this->request('field');
        $keyword = $this->request('keyword');

        $queryStmt = "where $field like ?";
        $rowsCount = $this->getRowsCount($queryStmt, array("$keyword%"));

        // Limit

        $page = $this->request('page');
		$count = $this->request('count');

        $limitStmt = '';
        $pageCount = 1;

        if (!empty($page) && !empty($count)) {
            $row = ($page * $count) - $count;
            $limitStmt = "limit $row, $count";

            $pageCount = ceil($rowsCount / $count);

            if($pageCount < 1)
                $pageCount = 1;
        }

        $rs = $this->select($queryStmt, array("$keyword%"));

        $extra = array(
            'rowsCount' => $rowsCount,
            'pageCount' => $pageCount,
        );

		return $this->success('DATA_READ', $rs, $extra);
	}

	public function detailData($params = array())
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();

		$id = $this->request('id');

        // Query

		$rs = $this->find($id);

        if($rs)
            return $this->success('DATA_READ', $rs);
        else
            return $this->failed('DATA_NOT_FOUND');
	}

    private function checkRequest() {
        $title = $this->request('title');

		$error = array();

		if(empty($title))
			$error['title'] = $this->string('TITLE_REQUIRED');

        return $error;
    }

	public function createData($params = array())
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

		$error = $this->checkRequest();

        $title = $this->request('title');
        $title_sl = $this->request('titleSl');
        $title_el = $this->request('titleEl');
        $location = $this->request('location');
        $location_sl = $this->request('locationSl');
        $location_el = $this->request('locationEl');
        $latitude = $this->request('latitude');
        $longitude = $this->request('longitude');
        $eventsDate = $this->request('eventsDate');
        $eventsTime = $this->request('eventsTime');
        $description = $this->request('description');
        $description_sl = $this->request('descriptionSl');
    	$description_el = $this->request('descriptionEl');
    	$moreDescription = $this->request('moreDescription');
    	$moreDescription_sl = $this->request('moreDescriptionSl');
    	$moreDescription_el = $this->request('moreDescriptionEl');
        $publishStatusId = $this->request('publishStatusId');

        $rs = $this->find('title', $title);

		if($rs)
            $error['title'] = $this->string('TITLE_IS_REG');

		if(count($error) > 0)
			return $this->invalid('PLEASE_CORRECT', $error);

		$rs = array(
            'title' => $title,
			'title_sl' => $title_sl,
			'title_el' => $title_el,
			'location' => $location,
			'location_sl' => $location_sl,
			'location_el' => $location_el,
			'latitude' => $latitude,
			'longitude' => $longitude,
			'eventsDate' => $eventsDate,
			'eventsTime' => $eventsTime,
			'description' => $description,
			'description_sl' => $description_sl,
    	  	'description_el' => $description_el,
	      	'moreDescription' => $moreDescription,
    	  	'moreDescription_sl' => $moreDescription_sl,
    	  	'moreDescription_el' => $moreDescription_el,
			'publishStatusId' => $publishStatusId,
        );

		$this->insert($rs);
		return $this->success('DATA_CREATED');
	}

	public function updateData($params = array())
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

        $id = $this->request('id');
        $title = $this->request('title');
        $title_sl = $this->request('titleSl');
        $title_el = $this->request('titleEl');
        $location = $this->request('location');
        $location_sl = $this->request('locationSl');
        $location_el = $this->request('locationEl');
        $latitude = $this->request('latitude');
        $longitude = $this->request('longitude');
        $eventsDate = $this->request('eventsDate');
        $eventsTime = $this->request('eventsTime');
        $description = $this->request('description');
        $description_sl = $this->request('descriptionSl');
    	$description_el = $this->request('descriptionEl');
    	$moreDescription = $this->request('moreDescription');
    	$moreDescription_sl = $this->request('moreDescriptionSl');
    	$moreDescription_el = $this->request('moreDescriptionEl');
        $publishStatusId = $this->request('publishStatusId');

        $error = $this->checkRequest();

        $rs = $this->find($id);

        if(!$rs)
            return $this->failed('DATA_NOT_FOUND');

        $last_title = $rs['title'];

        if($title != $last_title) {
            $rs = $this->find('title', $title);

            if($rs)
                $error['title'] = $this->string('TITLE_IS_REG');
        }

		if(count($error) > 0)
			return $this->invalid('PLEASE_CORRECT', $error);

		$rs = array(
			'title' => $title,
			'title_sl' => $title_sl,
			'title_el' => $title_el,
			'location' => $location,
			'location_sl' => $location_sl,
			'location_el' => $location_el,
			'latitude' => $latitude,
			'longitude' => $longitude,
			'eventsDate' => $eventsDate,
			'eventsTime' => $eventsTime,
			'description' => $description,
			'description_sl' => $description_sl,
    	  	'description_el' => $description_el,
	      	'moreDescription' => $moreDescription,
    	  	'moreDescription_sl' => $moreDescription_sl,
    	  	'moreDescription_el' => $moreDescription_el,
			'publishStatusId' => $publishStatusId,
		);

		$this->update($id, $rs);
		return $this->success('DATA_UPDATED');
	}

	public function deleteData($params = array())
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

		$id = $this->request('id');
        $rs = $this->find($id);

		$this->delete($id);
		return $this->success('DATA_DELETED');
	}

	public function multipleDeleteData($params = array())
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

		$multipleId = $this->request('multipleId');

		foreach($multipleId as $id)
            $this->delete($id);

		return $this->success('DATA_DELETED');
	}

}

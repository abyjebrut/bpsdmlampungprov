<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Regional extends MY_Model {
    protected $table = 'tb_regional';
    
    public function readData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();

        // Order

        $orderBy = $this->request('orderBy');
        $reverse = $this->request('reverse');

        $orderStmt = '';

        if (!empty($orderBy)) {
            $orderStmt = "order by $orderBy";

            if (!empty($reverse)) {
                if ($reverse == 1)
                    $orderStmt .= ' desc';
            }
        }

        // Limit

        $page = $this->request('page');
		$count = $this->request('count');

        $limitStmt = '';

        $rowsCount = $this->getRowsCount();
        $pageCount = 1;

        if (!empty($page) && !empty($count)) {
            $row = ($page * $count) - $count;
            $limitStmt = "limit $row, $count";

            $pageCount = ceil($rowsCount / $count);

            if($pageCount < 1)
                $pageCount = 1;
        }

        // Query

		$rs = $this->select($orderStmt.' '.$limitStmt);
        $extra = array(
            'rowsCount' => $rowsCount,
            'pageCount' => $pageCount,
        );

		return $this->success('DATA_READ', $rs, $extra);
	}

    public function filterData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();
        
        // Query

        $field = $this->request('field');
        $keyword = $this->request('keyword');

        $queryStmt = "where $field like ?";
        $rowsCount = $this->getRowsCount($queryStmt, array("$keyword%"));

        // Limit

        $page = $this->request('page');
		$count = $this->request('count');

        $limitStmt = '';
        $pageCount = 1;

        if (!empty($page) && !empty($count)) {
            $row = ($page * $count) - $count;
            $limitStmt = "limit $row, $count";

            $pageCount = ceil($rowsCount / $count);

            if($pageCount < 1)
                $pageCount = 1;
        }

        $rs = $this->select($queryStmt, array("$keyword%"));

        $extra = array(
            'rowsCount' => $rowsCount,
            'pageCount' => $pageCount,
        );

		return $this->success('DATA_READ', $rs, $extra);
	}

	public function detailData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();

		$id = $this->request('id');
        
        // Query
        
		$rs = $this->find($id);

        if($rs)
            return $this->success('DATA_READ', $rs);
        else
            return $this->failed('DATA_NOT_FOUND');
	}

    private function checkRequest() {
        $regional = $this->request('regional');

		$error = array();

		if(empty($regional))
			$error['regional'] = $this->string('REGIONAL_REQUIRED');

        return $error;
    }

	public function createData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

		$error = $this->checkRequest();

        $regional = $this->request('regional');

        $rs = $this->find('regional', $regional);

		if($rs)
            $error['regional'] = $this->string('REGIONAL_IS_REG');

		if(count($error) > 0)
			return $this->invalid('PLEASE_CORRECT', $error);

		$rs = array(
            'regional' => $regional,
        );

		$this->insert($rs);
		return $this->success('DATA_CREATED');
	}

	public function updateData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

        $id = $this->request('id');
        $regional = $this->request('regional');
		
        $error = $this->checkRequest();

        $rs = $this->find($id);
        if(!$rs)
            return $this->failed('DATA_NOT_FOUND');

        $last_regional = $rs['regional'];

        if($regional != $last_regional) {
            $rs = $this->find('regional', $regional);

            if($rs)
                $error['regional'] = $this->string('REGIONAL_IS_REG');
        }

		if(count($error) > 0)
			return $this->invalid('PLEASE_CORRECT', $error);

		$rs = array(
				'regional' => $regional,
		);

		$this->update($id, $rs);
		return $this->success('DATA_UPDATED');
	}

	public function deleteData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

		$id = $this->request('id');
        $rs = $this->find($id);

		$this->delete($id);
		return $this->success('DATA_DELETED');
	}

	public function multipleDeleteData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

		$multipleId = $this->request('multipleId');

		foreach($multipleId as $id)
            $this->delete($id);

		return $this->success('DATA_DELETED');
	}
    
    public function createPDFList($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();
        
        // PDF Settings
        
        $this->load->library('pdf');
        $this->pdf->setSettings(array(
            'strings' => $this->config->item('strings'),
            'title' => 'REGIONAL_LIST',
            'pageOrientation' => 'P',
        ));
        
        // Query
        
        $rs = $this->select();
        
        // Output
        
        if (count($rs) > 0) {
            $fileUrl = $this->pdf->render(
                'RegionalList',
                array('rs' => $rs)
            );

            return $this->success('FILE_CREATED', $fileUrl);
        }
        else
            return $this->failed('FILE_CREATE_FAILED');
    }
    
    public function createExcelList($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();
        
        // Excel Settings
        
        $this->load->library('excel');
//        $this->excel->setSettings(array(
//            'strings' => $this->config->item('strings'),
//            'title' => 'REGIONAL_LIST',
//            'pageOrientation' => 'P',
//        ));
        
        // Query
        
        $rs = $this->select();
        
        // Output
        
        if (count($rs) > 0) {
            $fileUrl = $this->excel->renderList(
                'RegionalList',
                array(
                    'dataSource' => $rs,
                    'showFields' => array(
                        'regional',
                    ),
                )
            );

            return $this->success('FILE_CREATED', $fileUrl);
        }
        else
            return $this->failed('FILE_CREATE_FAILED');
    }
    
}
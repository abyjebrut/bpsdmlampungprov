<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Diklat extends MY_Model {
    protected $table = 'tb_diklat';
    protected $view = 'vi_diklat';
    
    public function readData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();

        // Order

        $orderBy = $this->request('orderBy');
        $reverse = $this->request('reverse');

        $orderStmt = '';

        if (!empty($orderBy)) {
            $orderStmt = "order by $orderBy";

            if (!empty($reverse)) {
                if ($reverse == 1)
                    $orderStmt .= ' desc';
            }
        }

        // Limit
        $page = $this->request('page');
		$count = $this->request('count');
		$jenisDiklatId = $this->request('jenisDiklatId');
		$publishStatusId = $this->request('publishStatusId');

        $limitStmt = '';

        if (!empty($jenisDiklatId) && !empty($publishStatusId))
	      $rowsCount = $this->getRowsCount("where jenisDiklatId = ? and publishStatusId = ? ", array($jenisDiklatId, $publishStatusId));
	    elseif (!empty($jenisDiklatId))
	      $rowsCount = $this->getRowsCount("where jenisDiklatId = ? ", array($jenisDiklatId));
	    elseif (!empty($publishStatusId))
	      $rowsCount = $this->getRowsCount("where publishStatusId = ? ", array($publishStatusId));
	    else
	      $rowsCount = $this->getRowsCount($orderStmt);
        
      	$pageCount = 1;

        if (!empty($page) && !empty($count)) {
            $row = ($page * $count) - $count;
            $limitStmt = "limit $row, $count";

            $pageCount = ceil($rowsCount / $count);

            if($pageCount < 1)
                $pageCount = 1;
        }

        // Query

        if (!empty($jenisDiklatId) && !empty($publishStatusId))
	      $rs = $this->select("where jenisDiklatId = ? and publishStatusId = ? ".$orderStmt.' '.$limitStmt, array($jenisDiklatId, $publishStatusId));
	    elseif (!empty($jenisDiklatId))
	      $rs = $this->select("where jenisDiklatId = ? ".$orderStmt.' '.$limitStmt, array($jenisDiklatId));
	    elseif (!empty($publishStatusId))
	      $rs = $this->select("where (jenisDiklatId = 6 or jenisDiklatId = 8 or jenisDiklatId = 9 or jenisDiklatId = 10) and publishStatusId = ? ".$orderStmt.' '.$limitStmt, array($publishStatusId));
	    else
	      $rs = $this->select("where jenisDiklatId = 6 or jenisDiklatId = 8 or jenisDiklatId = 9 or jenisDiklatId = 10 ".$orderStmt.' '.$limitStmt);
    
        $extra = array(
            'rowsCount' => $rowsCount,
            'pageCount' => $pageCount,
        );

		return $this->success('DATA_READ', $rs, $extra);
	}

	public function readDataFungsional($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();

        // Order

        $orderBy = $this->request('orderBy');
        $reverse = $this->request('reverse');

        $orderStmt = '';

        if (!empty($orderBy)) {
            $orderStmt = "order by $orderBy";

            if (!empty($reverse)) {
                if ($reverse == 1)
                    $orderStmt .= ' desc';
            }
        }

        // Limit
        $page = $this->request('page');
		$count = $this->request('count');
		$jenisDiklatId = $this->request('jenisDiklatId');
		$publishStatusId = $this->request('publishStatusId');

        $limitStmt = '';

        if (!empty($jenisDiklatId) && !empty($publishStatusId))
	      $rowsCount = $this->getRowsCount("where jenisDiklatId = ? and publishStatusId = ? ", array($jenisDiklatId, $publishStatusId));
	    elseif (!empty($jenisDiklatId))
	      $rowsCount = $this->getRowsCount("where jenisDiklatId = ? ", array($jenisDiklatId));
	    elseif (!empty($publishStatusId))
	      $rowsCount = $this->getRowsCount("where publishStatusId = ? ", array($publishStatusId));
	    else
	      $rowsCount = $this->getRowsCount($orderStmt);
        
      	$pageCount = 1;

        if (!empty($page) && !empty($count)) {
            $row = ($page * $count) - $count;
            $limitStmt = "limit $row, $count";

            $pageCount = ceil($rowsCount / $count);

            if($pageCount < 1)
                $pageCount = 1;
        }

        // Query

        if (!empty($jenisDiklatId) && !empty($publishStatusId))
	      $rs = $this->select("where jenisDiklatId = ? and publishStatusId = ? ".$orderStmt.' '.$limitStmt, array($jenisDiklatId, $publishStatusId));
	    elseif (!empty($jenisDiklatId))
	      $rs = $this->select("where jenisDiklatId = ? ".$orderStmt.' '.$limitStmt, array($jenisDiklatId));
	    elseif (!empty($publishStatusId))
		$rs = $this->select("where (jenisDiklatId = 11 or jenisDiklatId = 12) and publishStatusId = ? ".$orderStmt.' '.$limitStmt, array($publishStatusId));
	    else
	      $rs = $this->select("where jenisDiklatId = 11 or jenisDiklatId = 12 ".$orderStmt.' '.$limitStmt);
    
    
        $extra = array(
            'rowsCount' => $rowsCount,
            'pageCount' => $pageCount,
        );

		return $this->success('DATA_READ', $rs, $extra);
	}

	public function readDataPkti($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();

        // Order

        $orderBy = $this->request('orderBy');
        $reverse = $this->request('reverse');

        $orderStmt = '';

        if (!empty($orderBy)) {
            $orderStmt = "order by $orderBy";

            if (!empty($reverse)) {
                if ($reverse == 1)
                    $orderStmt .= ' desc';
            }
        }

        // Limit
        $page = $this->request('page');
		$count = $this->request('count');
		$jenisDiklatId = $this->request('jenisDiklatId');
		$publishStatusId = $this->request('publishStatusId');

        $limitStmt = '';

        if (!empty($jenisDiklatId) && !empty($publishStatusId))
	      $rowsCount = $this->getRowsCount("where jenisDiklatId = ? and publishStatusId = ? ", array($jenisDiklatId, $publishStatusId));
	    elseif (!empty($jenisDiklatId))
	      $rowsCount = $this->getRowsCount("where jenisDiklatId = ? ", array($jenisDiklatId));
	    elseif (!empty($publishStatusId))
	      $rowsCount = $this->getRowsCount("where publishStatusId = ? ", array($publishStatusId));
	    else
	      $rowsCount = $this->getRowsCount($orderStmt);
        
      	$pageCount = 1;

        if (!empty($page) && !empty($count)) {
            $row = ($page * $count) - $count;
            $limitStmt = "limit $row, $count";

            $pageCount = ceil($rowsCount / $count);

            if($pageCount < 1)
                $pageCount = 1;
        }

        // Query

        if (!empty($jenisDiklatId) && !empty($publishStatusId))
	      $rs = $this->select("where jenisDiklatId = ? and publishStatusId = ? ".$orderStmt.' '.$limitStmt, array($jenisDiklatId, $publishStatusId));
	    elseif (!empty($jenisDiklatId))
	      $rs = $this->select("where jenisDiklatId = ? ".$orderStmt.' '.$limitStmt, array($jenisDiklatId));
	    elseif (!empty($publishStatusId))
		$rs = $this->select("where (jenisDiklatId = 11 or jenisDiklatId = 12) and publishStatusId = ? ".$orderStmt.' '.$limitStmt, array($publishStatusId));
	    else
	      $rs = $this->select("where jenisDiklatId = 11 or jenisDiklatId = 12 ".$orderStmt.' '.$limitStmt);
    
    
        $extra = array(
            'rowsCount' => $rowsCount,
            'pageCount' => $pageCount,
        );

		return $this->success('DATA_READ', $rs, $extra);
	}

	public function readLokasiData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();

        // Query

		$id = $this->request('id');
		$tahunDiklat = $this->request('tahunDiklat');
		$jenisDiklatId = $this->request('jenisDiklatId');

		$rs = $this->find($id);
		if(empty($tahunDiklat))
			$tahunDiklat =$rs['tahunDiklat'];
		if(empty($jenisDiklatId))
			$jenisDiklatId =$rs['jenisDiklatId'];
		
		$mAngkatan = $this->model('tb_angkatan');
		$rsAngkatan =$mAngkatan->scalar('select count(id) as jmlId from tb_angkatan ');
		$jmlAngkatan=$rsAngkatan['jmlId'];
		$mTempLokasi = $this->model('tb_temp_lokasi');
		$mTempLokasi->execute('delete from tb_temp_lokasi');
		$mLokasi = $this->model('tb_lokasi');
		$rsLokasi =$mLokasi->select();

		foreach($rsLokasi as $row) {
			$lokasiId =$row['id'];
			$rs = $this->scalar("select count(angkatanId) as jmId from tb_diklat where jenisDiklatId = ?  and  tahunDiklat = ? and  lokasiId = ? ", array($jenisDiklatId,$tahunDiklat, $lokasiId));
			$jmId =$rs['jmId'];

			if($jmId!=$jmlAngkatan)
			$mTempLokasi->insert(array(
				'id' => $lokasiId,
				'lokasi' => $row['lokasi']
			));
		} 
		
		$rsLok =$mTempLokasi->select('order by id asc');

		return $this->success('DATA_READ', $rsLok);
	}

	public function readAngkatanData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();

        // Query

		$id = $this->request('id');
		$lokasiId = $this->request('lokasiId');
		$tahunDiklat = $this->request('tahunDiklat');
		$jenisDiklatId = $this->request('jenisDiklatId');
		$rs = $this->find($id);
		if(empty($tahunDiklat))
			$tahunDiklat =$rs['tahunDiklat'];
		if(empty($jenisDiklatId))
			$jenisDiklatId =$rs['jenisDiklatId'];

		$mTempAngkatan = $this->model('tb_temp_angkatan');
		$mTempAngkatan->execute('delete from tb_temp_angkatan');
		$mAngkatan = $this->model('tb_angkatan');
		$rsAngkatan =$mAngkatan->select();

		foreach($rsAngkatan as $row) {
			$angkatanId =$row['id'];
			$rs = $this->select("where jenisDiklatId = ? and angkatanId = ? and  tahunDiklat = ? and  lokasiId = ? ", array($jenisDiklatId,$angkatanId, $tahunDiklat, $lokasiId));
			if(!$rs)
			$mTempAngkatan->insert(array(
				'id' => $angkatanId,
				'angkatan' => $row['angkatan']
			));
		} 
		
		$rsLok =$mTempAngkatan->select('order by id asc');

		return $this->success('DATA_READ', $rsLok);
	}

	public function readTanggalMerahData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();

        // Query

		$id = $this->request('id');
		$tahunDiklat = $this->request('tahunDiklat');
		$rs = $this->find($id);
		$tahunDiklatEnd = (int)$tahunDiklat+1;
		
		$mTglMerah = $this->model('tb_tanggal_merah_detail');
		$rsTanggalMerah = $mTglMerah->select("where year(tanggal) >= ? and year(tanggal) <= ?  ", array($tahunDiklat,$tahunDiklatEnd));

		return $this->success('DATA_READ', $rsTanggalMerah);
	}

	public function readDataFilter($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();

        // Order

        $orderBy = $this->request('orderBy');
        $reverse = $this->request('reverse');

        $orderStmt = '';

        if (!empty($orderBy)) {
            $orderStmt = "order by $orderBy";

            if (!empty($reverse)) {
                if ($reverse == 1)
                    $orderStmt .= ' desc';
            }
        }

        // Limit
        $page = $this->request('page');
		$count = $this->request('count');
		$statusMasukId = 0;
		$publishStatusId = 2;

        $limitStmt = '';
		
		if (!empty($publishStatusId))
	      $rowsCount = $this->getRowsCount("where publishStatusId = ? ", array($publishStatusId));
	    else
	      $rowsCount = $this->getRowsCount($orderStmt);
        
      	$pageCount = 1;

        if (!empty($page) && !empty($count)) {
            $row = ($page * $count) - $count;
            $limitStmt = "limit $row, $count";

            $pageCount = ceil($rowsCount / $count);

            if($pageCount < 1)
                $pageCount = 1;
        }

        // Query

		$rs = $this->query("select * from vi_filter_diklat where publishStatusId = ? and statusMasukId = ? ".$orderStmt.' '.$limitStmt, array($publishStatusId,$statusMasukId));
    
        $extra = array(
            'rowsCount' => $rowsCount,
            'pageCount' => $pageCount,
        );

		return $this->success('DATA_READ', $rs, $extra);
	}
	
	public function readDataDaftarDiklat($params = array())
	{
		if(!$this->setSettings(array(
				'authen' => 'free',
				'requestSource' => $params,
		)))
			return $this->denied();
	
		// Order
	
		$orderBy = $this->request('orderBy');
		$reverse = $this->request('reverse');
	
		$orderStmt = '';
	
		if (!empty($orderBy)) {
			$orderStmt = "order by $orderBy";
	
			if (!empty($reverse)) {
				if ($reverse == 1)
					$orderStmt .= ' desc';
			}
		}
	
		// Limit
		$page = $this->request('page');
		$count = $this->request('count');
		
		$limitStmt = '';
	
		$rowsCount = $this->getRowsCount($orderStmt);
	
		$pageCount = 1;
	
		if (!empty($page) && !empty($count)) {
			$row = ($page * $count) - $count;
			$limitStmt = "limit $row, $count";
	
			$pageCount = ceil($rowsCount / $count);
	
			if($pageCount < 1)
				$pageCount = 1;
		}
	
		// Query
	
		$publishStatusId = 2;
		$rs = $this->select("where publishStatusId = ? and kuotaDiklat < _kuota ".$orderStmt.' '.$limitStmt, array($publishStatusId));
	
		$extra = array(
				'rowsCount' => $rowsCount,
				'pageCount' => $pageCount,
		);
	
		return $this->success('DATA_READ', $rs, $extra);
	}

	public function readDataKelulusan($params = array())
	{
		if(!$this->setSettings(array(
				'authen' => 'free',
				'requestSource' => $params,
		)))
			return $this->denied();
	
		// Order
	
		$orderBy = $this->request('orderBy');
		$reverse = $this->request('reverse');
	
		$orderStmt = '';
	
		if (!empty($orderBy)) {
			$orderStmt = "order by $orderBy";
	
			if (!empty($reverse)) {
				if ($reverse == 1)
					$orderStmt .= ' desc';
			}
		}
	
		// Limit
		$page = $this->request('page');
		$count = $this->request('count');
		$jenisDiklatId = $this->request('jenisDiklatId');

		$tahun = $this->request('tahun');
		$publishStatusId = 2;
	
		$limitStmt = '';
	
		if (!empty($jenisDiklatId) && !empty($tahun))
			$rowsCount = $this->getRowsCount("where publishStatusId = ? and jenisDiklatId = ? and tahun = ? ", array($publishStatusId, $jenisDiklatId, $tahun));
		elseif (!empty($jenisDiklatId))
			$rowsCount = $this->getRowsCount("where publishStatusId = ? and jenisDiklatId = ? ", array($publishStatusId, $jenisDiklatId));
		elseif (!empty($tahun))
			$rowsCount = $this->getRowsCount("where publishStatusId = ? and tahun = ? ", array($publishStatusId, $tahun));
		else
			$rowsCount = $this->getRowsCount($orderStmt);
	
		$pageCount = 1;
	
		if (!empty($page) && !empty($count)) {
			$row = ($page * $count) - $count;
			$limitStmt = "limit $row, $count";
	
			$pageCount = ceil($rowsCount / $count);
	
			if($pageCount < 1)
				$pageCount = 1;
		}
	
		// Query
	
		if (!empty($jenisDiklatId) && !empty($tahun))
			$rs = $this->select("where publishStatusId = ? and jenisDiklatId = ? and tahun = ? ".$orderStmt.' '.$limitStmt, array($publishStatusId, $jenisDiklatId, $tahun ));
		elseif (!empty($jenisDiklatId))
			$rs = $this->select("where publishStatusId = ? and jenisDiklatId = ? ".$orderStmt.' '.$limitStmt, array($publishStatusId,$jenisDiklatId));
		elseif (!empty($tahun))
			$rs = $this->select("where publishStatusId = ? and tahun = ? ".$orderStmt.' '.$limitStmt, array($publishStatusId,$tahun));
		else
			$rs = $this->select($orderStmt.' '.$limitStmt);
	
		$extra = array(
				'rowsCount' => $rowsCount,
				'pageCount' => $pageCount,
		);
	
		return $this->success('DATA_READ', $rs, $extra);
	}

	public function readDataRekapitulasiJp($params = array())
	{
		if(!$this->setSettings(array(
				'authen' => 'free',
				'requestSource' => $params,
		)))
			return $this->denied();
	
		// Order
	
		$orderBy = $this->request('orderBy');
		$reverse = $this->request('reverse');
	
		$orderStmt = '';
	
		if (!empty($orderBy)) {
			$orderStmt = "order by $orderBy";
	
			if (!empty($reverse)) {
				if ($reverse == 1)
					$orderStmt .= ' desc';
			}
		}
	
		// Limit
		$page = $this->request('page');
		$count = $this->request('count');
		$jenisDiklatId = $this->request('jenisDiklatId');

		$tahun = $this->request('tahun');
		$publishStatusId = 2;
	
		$limitStmt = '';
		$mItems = $this->model('vi_bahan_jadwal_diklat_pengajar_detail');

		if (!empty($jenisDiklatId))
			$rowsCount1 = $mItems->query("select vi_bahan_jadwal_diklat_pengajar_detail.id,
			vi_bahan_jadwal_diklat_pengajar_detail.jenisDiklatId,
			vi_bahan_jadwal_diklat_pengajar_detail.tahunDiklat,
			vi_bahan_jadwal_diklat_pengajar_detail.publishStatusId,
			vi_bahan_jadwal_diklat_pengajar_detail.jenisDiklat,
			vi_bahan_jadwal_diklat_pengajar_detail.diklatId,
			vi_bahan_jadwal_diklat_pengajar_detail.tanggal,
			vi_bahan_jadwal_diklat_pengajar_detail.jumlahJp,
			vi_bahan_jadwal_diklat_pengajar_detail.tipeMataPelajaranId,
			vi_bahan_jadwal_diklat_pengajar_detail.tipeWaktuId,
			IFNULL(vi_bahan_jadwal_diklat_pengajar_detail.namaPengajar,'') AS namaPengajar,
			vi_bahan_jadwal_diklat_pengajar_detail.fotoPengajar,
			vi_bahan_jadwal_diklat_pengajar_detail.pengajarDiklatId,
			vi_bahan_jadwal_diklat_pengajar_detail.jenisPengajarId,
			IFNULL(sum(vi_bahan_jadwal_diklat_pengajar_detail.jumlahJp),0) as totalJp from vi_bahan_jadwal_diklat_pengajar_detail where tipeMataPelajaranId=1 AND tipeWaktuId=1 AND jenisPengajarId =7 and jenisDiklatId = ? group by pengajarDiklatId ".$orderStmt.' '.$limitStmt, array($jenisDiklatId));
		else
			$rowsCount1 = $mItems->query("select vi_bahan_jadwal_diklat_pengajar_detail.id,
			vi_bahan_jadwal_diklat_pengajar_detail.jenisDiklatId,
			vi_bahan_jadwal_diklat_pengajar_detail.tahunDiklat,
			vi_bahan_jadwal_diklat_pengajar_detail.publishStatusId,
			vi_bahan_jadwal_diklat_pengajar_detail.jenisDiklat,
			vi_bahan_jadwal_diklat_pengajar_detail.diklatId,
			vi_bahan_jadwal_diklat_pengajar_detail.tanggal,
			vi_bahan_jadwal_diklat_pengajar_detail.jumlahJp,
			vi_bahan_jadwal_diklat_pengajar_detail.tipeMataPelajaranId,
			vi_bahan_jadwal_diklat_pengajar_detail.tipeWaktuId,
			IFNULL(vi_bahan_jadwal_diklat_pengajar_detail.namaPengajar,'') AS namaPengajar,
			vi_bahan_jadwal_diklat_pengajar_detail.fotoPengajar,
			vi_bahan_jadwal_diklat_pengajar_detail.pengajarDiklatId,
			vi_bahan_jadwal_diklat_pengajar_detail.jenisPengajarId,
			IFNULL(sum(vi_bahan_jadwal_diklat_pengajar_detail.jumlahJp),0) as totalJp from vi_bahan_jadwal_diklat_pengajar_detail where tipeMataPelajaranId=1 AND tipeWaktuId=1 AND jenisPengajarId =7 group by pengajarDiklatId ".$orderStmt.' '.$limitStmt);
	
			$rowsCount = count($rowsCount1);
		$pageCount = 1;
	
		if (!empty($page) && !empty($count)) {
			$row = ($page * $count) - $count;
			$limitStmt = "limit $row, $count";
	
			$pageCount = ceil($rowsCount / $count);
	
			if($pageCount < 1)
				$pageCount = 1;
		}
	
		// Query
	
		if (!empty($jenisDiklatId))
			$rs = $mItems->query("select vi_bahan_jadwal_diklat_pengajar_detail.id,
			vi_bahan_jadwal_diklat_pengajar_detail.jenisDiklatId,
			vi_bahan_jadwal_diklat_pengajar_detail.tahunDiklat,
			vi_bahan_jadwal_diklat_pengajar_detail.publishStatusId,
			vi_bahan_jadwal_diklat_pengajar_detail.jenisDiklat,
			vi_bahan_jadwal_diklat_pengajar_detail.diklatId,
			vi_bahan_jadwal_diklat_pengajar_detail.tanggal,
			vi_bahan_jadwal_diklat_pengajar_detail.jumlahJp,
			vi_bahan_jadwal_diklat_pengajar_detail.tipeMataPelajaranId,
			vi_bahan_jadwal_diklat_pengajar_detail.tipeWaktuId,
			IFNULL(vi_bahan_jadwal_diklat_pengajar_detail.namaPengajar,'') AS namaPengajar,
			vi_bahan_jadwal_diklat_pengajar_detail.fotoPengajar,
			vi_bahan_jadwal_diklat_pengajar_detail.pengajarDiklatId,
			vi_bahan_jadwal_diklat_pengajar_detail.jenisPengajarId,
			IFNULL(sum(vi_bahan_jadwal_diklat_pengajar_detail.jumlahJp),0) as totalJp from vi_bahan_jadwal_diklat_pengajar_detail where tipeMataPelajaranId=1 AND tipeWaktuId=1 AND jenisPengajarId =7 and jenisDiklatId = ? group by pengajarDiklatId ".$orderStmt.' '.$limitStmt, array($jenisDiklatId));
		else
			$rs = $mItems->query("select vi_bahan_jadwal_diklat_pengajar_detail.id,
			vi_bahan_jadwal_diklat_pengajar_detail.jenisDiklatId,
			vi_bahan_jadwal_diklat_pengajar_detail.tahunDiklat,
			vi_bahan_jadwal_diklat_pengajar_detail.publishStatusId,
			vi_bahan_jadwal_diklat_pengajar_detail.jenisDiklat,
			vi_bahan_jadwal_diklat_pengajar_detail.diklatId,
			vi_bahan_jadwal_diklat_pengajar_detail.tanggal,
			vi_bahan_jadwal_diklat_pengajar_detail.jumlahJp,
			vi_bahan_jadwal_diklat_pengajar_detail.tipeMataPelajaranId,
			vi_bahan_jadwal_diklat_pengajar_detail.tipeWaktuId,
			IFNULL(vi_bahan_jadwal_diklat_pengajar_detail.namaPengajar,'') AS namaPengajar,
			vi_bahan_jadwal_diklat_pengajar_detail.fotoPengajar,
			vi_bahan_jadwal_diklat_pengajar_detail.pengajarDiklatId,
			vi_bahan_jadwal_diklat_pengajar_detail.jenisPengajarId,
			IFNULL(sum(vi_bahan_jadwal_diklat_pengajar_detail.jumlahJp),0) as totalJp from vi_bahan_jadwal_diklat_pengajar_detail where tipeMataPelajaranId=1 AND tipeWaktuId=1 AND jenisPengajarId =7 group by pengajarDiklatId ".$orderStmt.' '.$limitStmt);
	
		$extra = array(
				'rowsCount' => '',
				'pageCount' => $pageCount,
		);
	
		return $this->success('DATA_READ', $rs, $extra);
	}

	public function readDataRekapitulasiPeserta($params = array())
	{
		if(!$this->setSettings(array(
				'authen' => 'free',
				'requestSource' => $params,
		)))
			return $this->denied();
	
		// Order
	
		$orderBy = $this->request('orderBy');
		$reverse = $this->request('reverse');
	
		$orderStmt = '';
	
		if (!empty($orderBy)) {
			$orderStmt = "order by $orderBy";
	
			if (!empty($reverse)) {
				if ($reverse == 1)
					$orderStmt .= ' desc';
			}
		}
	
		// Limit
		$page = $this->request('page');
		$count = $this->request('count');
	
		$limitStmt = '';
		$mItems = $this->model('vi_rekapitulasi_peserta');

			$rowsCount = $mItems->getRowsCount($orderStmt);
	
		$pageCount = 1;
	
		if (!empty($page) && !empty($count)) {
			$row = ($page * $count) - $count;
			$limitStmt = "limit $row, $count";
	
			$pageCount = ceil($rowsCount / $count);
	
			if($pageCount < 1)
				$pageCount = 1;
		}
	
		// Query
	
			$rs = $mItems->select($orderStmt.' '.$limitStmt);
	
		$extra = array(
				'rowsCount' => $rowsCount,
				'pageCount' => $pageCount,
		);
	
		return $this->success('DATA_READ', $rs, $extra);
	}

	
	public function readDataJadwalDiklat($params = array())
	{
		if(!$this->setSettings(array(
				'authen' => 'free',
				'requestSource' => $params,
		)))
			return $this->denied();

		$mItems = $this->model('vi_bahan_jadwal_diklat');

		$rs = $mItems->select("where publishStatusId = 2");

		$calendar = array();
		foreach ($rs as $key => $val) 
		{

			$namaPengajar = $val['namaPengajar'];
			if ($val['pengajarDiklatId'] <= 0) {
				$namaPengajar = '';
			}
			$mItemsDetail = $this->model('vi_bahan_jadwal_diklat_pengajar_detail');
			$rsDetail = $mItemsDetail->select("where bahanJadwalDiklatId = ? ", array($val['id']));
			$htmlView = '';
			$noi = 0;
			foreach($rsDetail as $row) {
				$noi++;
				$htmlView .= $noi.'. '.$row['namaPengajar'].' ';
			}
			$allPengajar = $htmlView;
					
			if ($val['tipeWaktuId'] == 1) {
			
						$allTanggal = $this->indonesian_date($val['tanggal'] ,"l, j F Y");

						$allWaktu = $val['jamMulai'].' - '.$val['jamSelesai'];

			}else{
				
					$allTanggal = $this->indonesian_date($val['tanggal'] ,"l, j F Y").' s/d '.$this->indonesian_date($val['tanggalSelesai'] ,"l, j F Y") ;

					$allWaktu = $val['jumlahHari'].' hari';
					
			}
		 $calendar[] = array(
		  'id'  => intval($val['id']), 
		  'title' => $val['namaDiklat'], 
		  'namaPengajar' => $val['namaPengajar'],
		  'fotoPengajar' => $val['fotoPengajar'], 
		  'tahunDiklat' => $val['tahunDiklat'], 
		  'lokasi' => $val['lokasi'], 
		  'allTanggal' => $allTanggal, 
		  'allWaktu' => $allWaktu, 
		  'allPengajar' => $allPengajar, 
		  'angkatan' => $val['angkatan'], 
		  'jenisDiklat' => $val['jenisDiklat'], 
		  'pelaksanaanMulai' => $val['pelaksanaanMulai'], 
		  'pelaksanaanSelesai' => $val['pelaksanaanSelesai'], 
		  'ruang' => $val['ruang'], 
		  'mataPelajaran' => $val['mataPelajaran'], 
		  'kuota' => $val['kuota'], 
		  'kuotaDiklat' => $val['kuotaDiklat'], 
		  'jumlahJp' => $val['jumlahJp'], 
		  'keterangan' => $val['keterangan'], 
		  'jumlahJp' => $val['jumlahJp'], 
		  'start' => $val['tanggalJamMulai'],
		  'end' => $val['tanggalJamSelesai'],
		  'backgroundColor' => "#00a65a",
		 );
		}
	  
		$data = array();
		$data   = json_encode($calendar);
		
		return $this->success('DATA_READ', $data);
	}
	
	public function readDataAlumni($params = array())
	{
		if(!$this->setSettings(array(
				'authen' => 'free',
				'requestSource' => $params,
		)))
			return $this->denied();
	
		// Order
	
		$orderBy = $this->request('orderBy');
		$reverse = $this->request('reverse');
	
		$orderStmt = '';
	
		if (!empty($orderBy)) {
			$orderStmt = "order by $orderBy";
	
			if (!empty($reverse)) {
				if ($reverse == 1)
					$orderStmt .= ' desc';
			}
		}
	
		// Limit
		$page = $this->request('page');
		$count = $this->request('count');
		$jenisDiklatId = $this->request('jenisDiklatId');
		$tahunFull=date('Y');
		$tahun = $this->request('tahun');
	
		$limitStmt = '';
	
		if (!empty($jenisDiklatId) && !empty($tahun))
			$rowsCount = $this->getRowsCount("where jenisDiklatId = ? and tahun = ? ", array($jenisDiklatId, $tahun));
		elseif (!empty($jenisDiklatId))
			$rowsCount = $this->getRowsCount("where jenisDiklatId = ? ", array($jenisDiklatId));
		elseif (!empty($tahun))
			$rowsCount = $this->getRowsCount("where tahun = ? ", array($tahun));
		else
			$rowsCount = $this->getRowsCount("where tahun = ? ", array($tahunFull));
	
		$pageCount = 1;
	
		if (!empty($page) && !empty($count)) {
			$row = ($page * $count) - $count;
			$limitStmt = "limit $row, $count";
	
			$pageCount = ceil($rowsCount / $count);
	
			if($pageCount < 1)
				$pageCount = 1;
		}
	
		// Query
	
		if (!empty($jenisDiklatId) && !empty($tahun))
			$rs = $this->select("where jenisDiklatId = ? and tahun = ? ".$orderStmt.' '.$limitStmt, array($jenisDiklatId, $tahun ));
		elseif (!empty($jenisDiklatId))
			$rs = $this->select("where jenisDiklatId = ? ".$orderStmt.' '.$limitStmt, array($jenisDiklatId));
		elseif (!empty($tahun))
			$rs = $this->select("where tahun = ? ".$orderStmt.' '.$limitStmt, array($tahun));
		else
			$rs = $this->select("where tahun = ? ".$orderStmt.' '.$limitStmt, array($tahunFull));
	
		$extra = array(
				'rowsCount' => $rowsCount,
				'pageCount' => $pageCount,
		);
	
		return $this->success('DATA_READ', $rs, $extra);
	}
	
	public function readDataTahun($params = array()) {
		if(!$this->setSettings(array(
				'authen' => 'free',
				'requestSource' => $params,
		)))
			return $this->denied();
	
		$data = $this->query("select distinct(tahun) as tahun from vi_diklat");
	
		return $this->success('DATA_READ', $data);
	}
	
    public function filterData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();
        
        // Query

        $field = $this->request('field');
        $keyword = $this->request('keyword');

        $queryStmt = "where $field like ?";
        $rowsCount = $this->getRowsCount($queryStmt, array("$keyword%"));

        // Limit

        $page = $this->request('page');
		$count = $this->request('count');

        $limitStmt = '';
        $pageCount = 1;

        if (!empty($page) && !empty($count)) {
            $row = ($page * $count) - $count;
            $limitStmt = "limit $row, $count";

            $pageCount = ceil($rowsCount / $count);

            if($pageCount < 1)
                $pageCount = 1;
        }

        $rs = $this->select($queryStmt, array("$keyword%"));

        $extra = array(
            'rowsCount' => $rowsCount,
            'pageCount' => $pageCount,
        );

		return $this->success('DATA_READ', $rs, $extra);
	}

	public function detailData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();

		$id = $this->request('id');
        
        // Query
        
		$rs = $this->find($id);
		
		$jenisDiklatId = $rs['jenisDiklatId'];
		$mBerkas = $this->model('vi_jenis_diklat_berkas');
		$rs['listBerkasPendaftaran'] = $mBerkas->select('where jenisDiklatId = ? order by id asc', array($jenisDiklatId));
		
		$mIsiMapel = $this->model('vi_bahan_jadwal_diklat');
		$rsIsiMapel = $mIsiMapel->scalar("select count(mataPelajaranId) as idIsiMapel from vi_bahan_jadwal_diklat where jenisDiklatId = ? and diklatId = ? ", array($jenisDiklatId,$id));
		$idIsiMapel = $rsIsiMapel['idIsiMapel'];

		$mItems = $this->model('vi_bahan_jadwal_diklat');
		$rs['listJadwalDiklat'] = $mItems->select('where diklatId = ? order by noUrut, jamMulai asc', array($id));
		
		$mItems = $this->model('vi_bahan_jadwal_diklat_pengajar_detail');
		$rs['listJadwalDiklatPengajar'] = $mItems->select('where diklatId = ? order by id asc', array($id));

		$mItems = $this->model('vi_user_label_mapel');
		$aktivasiId =1;
		$rs['listPengajar'] = $mItems->select('where aktivasiId = ? order by id asc', array($aktivasiId));

        if($rs)
            return $this->success('DATA_READ', $rs);
        else
            return $this->failed('DATA_NOT_FOUND');
	}

	public function detailDataJadwal($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();

		$id = $this->request('id');
        
        // Query
        
		$rs = $this->find($id);
		
		$jenisDiklatId = $rs['jenisDiklatId'];
		$mBerkas = $this->model('vi_jenis_diklat_berkas');
		$rs['listBerkasPendaftaran'] = $mBerkas->select('where jenisDiklatId = ? order by id asc', array($jenisDiklatId));
		
		$mIsiMapel = $this->model('vi_bahan_jadwal_diklat');
		$rsIsiMapel = $mIsiMapel->scalar("select count(mataPelajaranId) as idIsiMapel from vi_bahan_jadwal_diklat where jenisDiklatId = ? and diklatId = ? ", array($jenisDiklatId,$id));
		$idIsiMapel = $rsIsiMapel['idIsiMapel'];

		$mItems = $this->model('vi_bahan_jadwal_diklat');
		$rs['listJadwalDiklat'] = $mItems->select('where (diklatId = ? and labelMapelId <> 7 ) AND (tipeWaktuId <> 1 or tipeMataPelajaranId <> 2) order by noUrut, jamMulai asc', array($id));
		
		$mItems = $this->model('vi_bahan_jadwal_diklat_pengajar_detail');
		$rs['listJadwalDiklatPengajar'] = $mItems->select('where diklatId = ? order by id asc', array($id));

		$mItems = $this->model('vi_user_label_mapel');
		$aktivasiId =1;
		$rs['listPengajar'] = $mItems->select('where aktivasiId = ? order by id asc', array($aktivasiId));

        if($rs)
            return $this->success('DATA_READ', $rs);
        else
            return $this->failed('DATA_NOT_FOUND');
	}

    private function checkRequest() {
    	$publishStatusId = $this->request('publishStatusId');
        $jenisDiklatId = $this->request('jenisDiklatId');
        $tahunDiklat = $this->request('tahunDiklat');
        $lokasiId = $this->request('lokasiId');
        $angkatanId = $this->request('angkatanId');
        $namaDiklat = $this->request('namaDiklat');
		$pelaksanaanMulai = $this->request('pelaksanaanMulai');
		$tahunDiklatEnd = (int)$tahunDiklat+1;
		
		$mTglMerah = $this->model('tb_tanggal_merah_detail');
		$rsTanggalMerah = $mTglMerah->select("where year(tanggal) >= ? and year(tanggal) <= ?  ", array($tahunDiklat,$tahunDiklatEnd));

		$daftar_hari = array(
			'Sunday' => 'Minggu',
			'Monday' => 'Senin',
			'Tuesday' => 'Selasa',
			'Wednesday' => 'Rabu',
			'Thursday' => 'Kamis',
			'Friday' => 'Jumat',
			'Saturday' => 'Sabtu'
		   );

		$error = array();
		
		if(empty($publishStatusId))
			$error['publishStatusId'] = $this->string('PUBLISH_STATUS_REQUIRED');
		
		if(empty($jenisDiklatId))
			$error['jenisDiklatId'] = $this->string('JENIS_DIKLAT_REQUIRED');
		
		if(empty($tahunDiklat))
			$error['tahunDiklat'] = $this->string('Tahun belum diisi');
		
		if(empty($namaDiklat))
			$error['namaDiklat'] = $this->string('NAMA_DIKLAT_REQUIRED');

		if(empty($lokasiId))
			$error['lokasiId'] = $this->string('Lokasi belum diisi');

		if(empty($angkatanId))
			$error['angkatanId'] = $this->string('Angkatan belum diisi');
			
		$tglMulaiw = date('Y-m-d', strtotime('+0 days', strtotime($pelaksanaanMulai)));
		$date1=$tglMulaiw;
		$namahari = date('l', strtotime($date1));
		$daftar_hari[$namahari];
		
		if($daftar_hari[$namahari] != 'Minggu'){
			foreach ($rsTanggalMerah as $key => $val) {
				if ($val['tanggal'] == $tglMulaiw) {
					$error['pelaksanaanMulai'] = $this->string('Pelaksanaan Mulai ini tanggal Merah');
				}
			}
		}else{
			$error['pelaksanaanMulai'] = $this->string('Pelaksanaan Mulai ini hari Minggu');
		}


        return $error;
    }

	public function createData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

		$error = $this->checkRequest();

		$publishStatusId = $this->request('publishStatusId');
		$userIdCreated = $this->request('userId');
		$tanggalI = $this->request('tanggalI');
		$jenisDiklatId = $this->request('jenisDiklatId');
		$tahunDiklat = $this->request('tahunDiklat');
        $lokasiId = $this->request('lokasiId');
        $angkatanId = $this->request('angkatanId');
		$namaDiklat = $this->request('namaDiklat');
		$pelaksanaanMulai = $this->request('pelaksanaanMulai');
		$pelaksanaanSelesai = $this->request('pelaksanaanSelesai');
		$kuota = $this->request('kuota');
		$statusDiklatId = $this->request('statusDiklatId');

		$daftar_hari = array(
			'Sunday' => 'Minggu',
			'Monday' => 'Senin',
			'Tuesday' => 'Selasa',
			'Wednesday' => 'Rabu',
			'Thursday' => 'Kamis',
			'Friday' => 'Jumat',
			'Saturday' => 'Sabtu'
			);
		   
		$rs = $this->select("where jenisDiklatId = ? and angkatanId = ? and lokasiId = ? and tahunDiklat = ? ", array($jenisDiklatId,$angkatanId,$lokasiId, $tahunDiklat));

		if($rs)
			$error['lokasiId'] = $this->string('Lokasi sudah terdaftar');
			
		if(count($error) > 0)
			return $this->invalid('PLEASE_CORRECT', $error);
			
			$mJenisDiklat = $this->model('tb_jenis_diklat');
			$rowJenisDiklat = $mJenisDiklat->find($jenisDiklatId);

			$namaSingkat = $rowJenisDiklat['namaSingkat'];
		
			$rsTahun = $this->select("where tahunDiklat = ? order by id desc limit 1", array($tahunDiklat));
			
			if(!$rsTahun)
				$kode='BPSDM'.'/'.$tahunDiklat.'/'.$namaSingkat.'/'.'0001';
			else{
				$noUrut1 = substr($rsTahun[0]['kode'] , -4);
				$hasil = (int)$noUrut1 + 1;
				$jml = strlen($hasil);
				
				if ($jml == 1)
					$noUrut = '000'.$hasil;
				else if ($jml == 2)
					$noUrut = '00'.$hasil;
				else if ($jml == 3)
					$noUrut = '0'.$hasil;
				else
					$noUrut = $hasil;
				$kode='BPSDM'.'/'.$tahunDiklat.'/'.$namaSingkat.'/'.$noUrut;
			}

		$rs = array(
			'userIdCreated' => $userIdCreated,
			'tglCreated' => $tanggalI,
			'userIdUpdated' => 0,
			'publishStatusId' => $publishStatusId,
            'jenisDiklatId' => $jenisDiklatId,
			'kode' => $kode,
			'tahunDiklat' => $tahunDiklat,
			'lokasiId' => $lokasiId,
			'angkatanId' => $angkatanId,
			'namaDiklat' => $namaDiklat,
			'pelaksanaanMulai' => $pelaksanaanMulai,
			'pelaksanaanSelesai' => $pelaksanaanSelesai,
			'kuota' => $kuota,
			'statusDiklatId' => $statusDiklatId,
        );

		$this->insert($rs);
		$lastId = $this->getLastInsertId();
		
		$mMapel = $this->model('vi_mata_pelajaran');
		$rsMapel = $mMapel->select("where publishStatusId = 2 and jenisDiklatId = ? order by noUrut, jamMulai asc ", array($jenisDiklatId));

		$mRs = $this->model('tb_bahan_jadwal_diklat');
		$tahunDiklatEnd = (int)$tahunDiklat+1;
		
		$mTglMerah = $this->model('tb_tanggal_merah_detail');
		$rsTanggalMerah = $mTglMerah->select("where year(tanggal) >= ? and year(tanggal) <= ?  ", array($tahunDiklat,$tahunDiklatEnd));

		foreach($rsMapel as $row) {

			$mRs1 = $this->model('vi_bahan_jadwal_diklat');
			$rsJd = $mRs1->select("where diklatId = ? order by id desc limit 1", array($lastId));
			
			if($rsJd){
				$noU =$rsJd[0]['noUrut']; 
				$t = $rsJd[0]['tanggalSelesai'];

				if ( $row['tipeWaktuId'] == 1){
					$jumHariAsli = 1 ;
					
					$tglSelesai1 = date('Y-m-d', strtotime('+14 days', strtotime($t)));
					$tglMulai1 = date('Y-m-d', strtotime('+1 days', strtotime($t)));
					
					$Variable1 = strtotime($tglMulai1); 
					$Variable2 = strtotime($tglSelesai1); 
					$ijumHari=0;
					for ($currentDate = $Variable1; $currentDate <= $Variable2;  $currentDate += (86400)) {            
						$tglMulai1 = date('Y-m-d', $currentDate); 
						$ijumHari++;
						$namahari = date('l', strtotime($tglMulai1));
						
						if ($daftar_hari[$namahari] != 'Minggu') {
							foreach ($rsTanggalMerah as $key => $val) {
								if ($val['tanggal'] == $tglMulai1) {
									$ijumHari = (int) $ijumHari - 1;
								}
							}
						} else {
							$ijumHari = (int)$ijumHari - 1;
						}

						if ($ijumHari == $jumHariAsli) {
							$ijumHari = 1000;
							$tglSelesai =$tglMulai1;
						}
					};

					if($noU == $row['noUrut']){
						$tglMulai = $t;
						$tglSelesai = $t;
					}else{
						$tglMulai = $tglSelesai;
						$tglSelesai = $tglSelesai;
					}
				}else{
					if($row['tipeTanggalId'] == 1){
						$jumHari = $row['jumlahHari'] ;
						$a = '+'.$jumHari.' days';
						$tglMulai = date('Y-m-d', strtotime('+1 days', strtotime($t)));
						$tglSelesai = date('Y-m-d', strtotime($a, strtotime($t)));

					}else{
						$jumHariAsli = $row['jumlahHari'] ;
						$jumHariAsliMulai = 1 ;
						
						if ($jumHariAsli <= 20)
							$jumHari = 30;
						else
							$jumHari = 120;
						
						$a = '+'.$jumHari.' days';
						$tglSelesai1 = date('Y-m-d', strtotime($a, strtotime($t)));
						$tglMulai1 = date('Y-m-d', strtotime('+1 days', strtotime($t)));
						$ijumHariMulai=0;
						$ijumHari=0;
						$Variable1 = strtotime($tglMulai1); 
						$Variable2 = strtotime($tglSelesai1); 

						for ($currentDate = $Variable1; $currentDate <= $Variable2;  $currentDate += (86400)) {            
							$ijumHari++;
							$ijumHariMulai++;
							$tglMulai1 = date('Y-m-d', $currentDate); 
						
							$namahari = date('l', strtotime($tglMulai1));
							
							if ($daftar_hari[$namahari] != 'Minggu' && $daftar_hari[$namahari] != 'Sabtu') {
								foreach ($rsTanggalMerah as $key => $val) {
									if ($val['tanggal'] == $tglMulai1) {
										$ijumHari = (int)$ijumHari - 1;
										$ijumHariMulai = (int)$ijumHariMulai - 1;
									}
								}
							} else {
								$ijumHari = (int)$ijumHari - 1;
								$ijumHariMulai = (int)$ijumHariMulai - 1;
							}
	
							if ($ijumHariMulai == $jumHariAsliMulai) {
								$tglMulai =$tglMulai1;
								$ijumHariMulai = 100000;
							}

							if ($ijumHari == $jumHariAsli) {
								$tglSelesai =$tglMulai1;
								$ijumHari = 100000;
							}

						};
					}
				}
			}else{
				$tglMulai =$pelaksanaanMulai ;
				$tglSelesai =$pelaksanaanMulai ;
			}

			

			if($row['jumlahJp']=='-')
			$jumJp = 0;
			else
			$jumJp = $row['jumlahJp'];

			$mRs->insert(array(
				'diklatId' => $lastId,
				'userIdCreated' => $userIdCreated,
				'tglCreated' => $tanggalI,
				'userIdUpdated' => 0,
				'pengajarDiklatId' => 0,
				'mataPelajaranId' => $row['id'],
				'ruang' => '',
				'tanggal' => $tglMulai,
				'tanggalSelesai' => $tglSelesai,
				'keterangan' => '',
				'jamMulai' => $row['jamMulai'],
				'jamSelesai' => $row['jamSelesai'],
				'jumlahJp' => $jumJp,
				'jumlahHari' => $row['jumlahHari'],
				'statusSimpan' => 'Belum',
			));

			if($row['tipeMataPelajaranId']==2 && $row['tipeWaktuId']==1)
				$pengajarDiklatId = 34;
			else
				$pengajarDiklatId = 0;
			
			if($row['labelMapelId']==43){
				$bahanJadwalDiklatId = $mRs->getLastInsertId();
				$mSubMenu = $this->model('tb_bahan_jadwal_diklat_pengajar_detail');

					$mSubMenu->insert(array(
						'diklatId' => $lastId,
						'pengajarDiklatId' => 34,
						'bahanJadwalDiklatId' => $bahanJadwalDiklatId,
					));
			}

			if($pengajarDiklatId > 1){
				if($row['labelMapelId']==7){
					$bahanJadwalDiklatId = $mRs->getLastInsertId();
					$mSubMenu = $this->model('tb_bahan_jadwal_diklat_pengajar_detail');

					$mSubMenu->insert(array(
						'diklatId' => $lastId,
						'pengajarDiklatId' => 50,
						'bahanJadwalDiklatId' => $bahanJadwalDiklatId,
					));

				}else{
					

				}

				$bahanJadwalDiklatId = $mRs->getLastInsertId();
					$mSubMenu = $this->model('tb_bahan_jadwal_diklat_pengajar_detail');
					$mSubMenu->insert(array(
						'diklatId' => $lastId,
						'pengajarDiklatId' => $pengajarDiklatId,
						'bahanJadwalDiklatId' => $bahanJadwalDiklatId,
					));
				
			}

			if($row['labelMapelId']==7){
				$bahanJadwalDiklatId = $mRs->getLastInsertId();
				$mSubMenu = $this->model('tb_bahan_jadwal_diklat_pengajar_detail');
				$mSubMenu->insert(array(
					'diklatId' => $lastId,
					'pengajarDiklatId' => 50,
					'bahanJadwalDiklatId' => $bahanJadwalDiklatId,
				));

			}

		} 
		
        $this->update($lastId, array('pelaksanaanSelesai' => $tglSelesai));

		return $this->success('DATA_CREATED');
	}

	public function createDataPkti($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

		$error = $this->checkRequest();

		$publishStatusId = $this->request('publishStatusId');
		$userIdCreated = $this->request('userId');
		$tanggalI = $this->request('tanggalI');
		$jenisDiklatId = $this->request('jenisDiklatId');
		$tahunDiklat = $this->request('tahunDiklat');
        $lokasiId = $this->request('lokasiId');
        $angkatanId = $this->request('angkatanId');
		$namaDiklat = $this->request('namaDiklat');
		$pelaksanaanMulai = $this->request('pelaksanaanMulai');
		$rsMapel = $this->request('listMapel');
		$pelaksanaanSelesai = $this->request('pelaksanaanSelesai');
		$kuota = $this->request('kuota');
		$statusDiklatId = $this->request('statusDiklatId');

		$daftar_hari = array(
			'Sunday' => 'Minggu',
			'Monday' => 'Senin',
			'Tuesday' => 'Selasa',
			'Wednesday' => 'Rabu',
			'Thursday' => 'Kamis',
			'Friday' => 'Jumat',
			'Saturday' => 'Sabtu'
			);
		   
		$rs = $this->select("where jenisDiklatId = ? and angkatanId = ? and lokasiId = ? and tahunDiklat = ? ", array($jenisDiklatId,$angkatanId,$lokasiId, $tahunDiklat));

		if($rs)
			$error['lokasiId'] = $this->string('Lokasi sudah terdaftar');
			
		if(count($error) > 0)
			return $this->invalid('PLEASE_CORRECT', $error);
			
			$mJenisDiklat = $this->model('tb_jenis_diklat');
			$rowJenisDiklat = $mJenisDiklat->find($jenisDiklatId);

			$namaSingkat = $rowJenisDiklat['namaSingkat'];
		
			$rsTahun = $this->select("where tahunDiklat = ? order by id desc limit 1", array($tahunDiklat));
			
			if(!$rsTahun)
				$kode='BPSDM'.'/'.$tahunDiklat.'/'.$namaSingkat.'/'.'0001';
			else{
				$noUrut1 = substr($rsTahun[0]['kode'] , -4);
				$hasil = (int)$noUrut1 + 1;
				$jml = strlen($hasil);
				
				if ($jml == 1)
					$noUrut = '000'.$hasil;
				else if ($jml == 2)
					$noUrut = '00'.$hasil;
				else if ($jml == 3)
					$noUrut = '0'.$hasil;
				else
					$noUrut = $hasil;
				$kode='BPSDM'.'/'.$tahunDiklat.'/'.$namaSingkat.'/'.$noUrut;
			}

		$rs = array(
			'userIdCreated' => $userIdCreated,
			'tglCreated' => $tanggalI,
			'userIdUpdated' => 0,
			'publishStatusId' => $publishStatusId,
            'jenisDiklatId' => $jenisDiklatId,
			'kode' => $kode,
			'tahunDiklat' => $tahunDiklat,
			'lokasiId' => $lokasiId,
			'angkatanId' => $angkatanId,
			'namaDiklat' => $namaDiklat,
			'pelaksanaanMulai' => $pelaksanaanMulai,
			'pelaksanaanSelesai' => $pelaksanaanSelesai,
			'kuota' => $kuota,
			'statusDiklatId' => $statusDiklatId,
        );

		$this->insert($rs);
		$lastId = $this->getLastInsertId();
		
		$mRs = $this->model('tb_bahan_jadwal_diklat');
		$tahunDiklatEnd = (int)$tahunDiklat+1;
		
		$mTglMerah = $this->model('tb_tanggal_merah_detail');
		$rsTanggalMerah = $mTglMerah->select("where year(tanggal) >= ? and year(tanggal) <= ?  ", array($tahunDiklat,$tahunDiklatEnd));

		foreach($rsMapel as $row) {

			$mRs1 = $this->model('vi_bahan_jadwal_diklat');
			$rsJd = $mRs1->select("where diklatId = ? order by id desc limit 1", array($lastId));
			
			if($rsJd){
				$noU =$rsJd[0]['noUrut']; 
				$t = $rsJd[0]['tanggalSelesai'];

				if ( $row['tipeWaktuId'] == 1){
					$jumHariAsli = 1 ;
					
					$tglSelesai1 = date('Y-m-d', strtotime('+14 days', strtotime($t)));
					$tglMulai1 = date('Y-m-d', strtotime('+1 days', strtotime($t)));
					
					$Variable1 = strtotime($tglMulai1); 
					$Variable2 = strtotime($tglSelesai1); 
					$ijumHari=0;
					for ($currentDate = $Variable1; $currentDate <= $Variable2;  $currentDate += (86400)) {            
						$tglMulai1 = date('Y-m-d', $currentDate); 
						$ijumHari++;
						$namahari = date('l', strtotime($tglMulai1));
						
						if ($daftar_hari[$namahari] != 'Minggu') {
							foreach ($rsTanggalMerah as $key => $val) {
								if ($val['tanggal'] == $tglMulai1) {
									$ijumHari = (int) $ijumHari - 1;
								}
							}
						} else {
							$ijumHari = (int)$ijumHari - 1;
						}

						if ($ijumHari == $jumHariAsli) {
							$ijumHari = 1000;
							$tglSelesai =$tglMulai1;
						}
					};

					if($noU == $row['noUrut']){
						$tglMulai = $t;
						$tglSelesai = $t;
					}else{
						$tglMulai = $tglSelesai;
						$tglSelesai = $tglSelesai;
					}
				}else{
					if($row['tipeTanggalId'] == 1){
						$jumHari = $row['jumlahHari'] ;
						$a = '+'.$jumHari.' days';
						$tglMulai = date('Y-m-d', strtotime('+1 days', strtotime($t)));
						$tglSelesai = date('Y-m-d', strtotime($a, strtotime($t)));

					}else{
						$jumHariAsli = $row['jumlahHari'] ;
						$jumHariAsliMulai = 1 ;
						
						if ($jumHariAsli <= 20)
							$jumHari = 30;
						else
							$jumHari = 80;
						
						$a = '+'.$jumHari.' days';
						$tglSelesai1 = date('Y-m-d', strtotime($a, strtotime($t)));
						$tglMulai1 = date('Y-m-d', strtotime('+1 days', strtotime($t)));
						$ijumHariMulai=0;
						$ijumHari=0;
						$Variable1 = strtotime($tglMulai1); 
						$Variable2 = strtotime($tglSelesai1); 

						for ($currentDate = $Variable1; $currentDate <= $Variable2;  $currentDate += (86400)) {            
							$ijumHari++;
							$ijumHariMulai++;
							$tglMulai1 = date('Y-m-d', $currentDate); 
						
							$namahari = date('l', strtotime($tglMulai1));
							
							if ($daftar_hari[$namahari] != 'Minggu' && $daftar_hari[$namahari] != 'Sabtu') {
								foreach ($rsTanggalMerah as $key => $val) {
									if ($val['tanggal'] == $tglMulai1) {
										$ijumHari = (int)$ijumHari - 1;
										$ijumHariMulai = (int)$ijumHariMulai - 1;
									}
								}
							} else {
								$ijumHari = (int)$ijumHari - 1;
								$ijumHariMulai = (int)$ijumHariMulai - 1;
							}
	
							if ($ijumHariMulai == $jumHariAsliMulai) {
								$tglMulai =$tglMulai1;
								$ijumHariMulai = 100000;
							}

							if ($ijumHari == $jumHariAsli) {
								$tglSelesai =$tglMulai1;
								$ijumHari = 100000;
							}

						};
					}
				}
			}else{
				$tglMulai =$pelaksanaanMulai ;
				$tglSelesai =$pelaksanaanMulai ;
			}
			if($row['jumlahJp']=='-')
			$jumJp = 0;
			else
			$jumJp = $row['jumlahJp'];
			if($row['isData']=='lama'){
				$mRs->insert(array(
					'diklatId' => $lastId,
					'userIdCreated' => $userIdCreated,
					'tglCreated' => $tanggalI,
					'userIdUpdated' => 0,
					'pengajarDiklatId' => 0,
					'mataPelajaranId' => $row['id'],
					'ruang' => '',
					'tanggal' => $tglMulai,
					'tanggalSelesai' => $tglSelesai,
					'keterangan' => '',
					'jamMulai' => $row['jamMulai'],
					'jamSelesai' => $row['jamSelesai'],
					'jumlahJp' => $jumJp,
					'jumlahHari' => $row['jumlahHari'],
					'statusSimpan' => 'Belum',
				));

				if($row['tipeMataPelajaranId']==2 && $row['tipeWaktuId']==1)
				$pengajarDiklatId = 34;
			else
				$pengajarDiklatId = 0;
			
			if($row['labelMapelId']==43){
				$bahanJadwalDiklatId = $mRs->getLastInsertId();
				$mSubMenu = $this->model('tb_bahan_jadwal_diklat_pengajar_detail');

					$mSubMenu->insert(array(
						'diklatId' => $lastId,
						'pengajarDiklatId' => 34,
						'bahanJadwalDiklatId' => $bahanJadwalDiklatId,
					));
			}

			if($pengajarDiklatId > 1){
				if($row['labelMapelId']==7){
					$bahanJadwalDiklatId = $mRs->getLastInsertId();
					$mSubMenu = $this->model('tb_bahan_jadwal_diklat_pengajar_detail');

					$mSubMenu->insert(array(
						'diklatId' => $lastId,
						'pengajarDiklatId' => 50,
						'bahanJadwalDiklatId' => $bahanJadwalDiklatId,
					));

				}else{
					

				}

				$bahanJadwalDiklatId = $mRs->getLastInsertId();
					$mSubMenu = $this->model('tb_bahan_jadwal_diklat_pengajar_detail');
					$mSubMenu->insert(array(
						'diklatId' => $lastId,
						'pengajarDiklatId' => $pengajarDiklatId,
						'bahanJadwalDiklatId' => $bahanJadwalDiklatId,
					));
				
			}

			if($row['labelMapelId']==7){
				$bahanJadwalDiklatId = $mRs->getLastInsertId();
				$mSubMenu = $this->model('tb_bahan_jadwal_diklat_pengajar_detail');
				$mSubMenu->insert(array(
					'diklatId' => $lastId,
					'pengajarDiklatId' => 50,
					'bahanJadwalDiklatId' => $bahanJadwalDiklatId,
				));

			}
			
			}else{
				$mMapel = $this->model('tb_mata_pelajaran');

				$rs = array(
					'publishStatusId' => $row['publishStatusId'],
					'noUrut' => $row['noUrut'],
					'labelMapelId' => $row['labelMapelId'],
					'jamMulai' => $row['jamMulai'],
					'jamSelesai' => $row['jamSelesai'],
					'jumlahHari' => $row['jumlahHari'],
					'jenisDiklatId' => $row['jenisDiklatId'],
					'tipeMataPelajaranId' => $row['tipeMataPelajaranId'],
					'tipeWaktuId' => $row['tipeWaktuId'],
					'tipeTanggalId' => $row['tipeTanggalId'],
					'jumlahJp' => $row['jumlahJp'],
				);
				$mMapel->insert($rs);
				$mataPelajaranId = $mMapel->getLastInsertId();

				$mRs->insert(array(
					'diklatId' => $lastId,
					'userIdCreated' => $userIdCreated,
					'tglCreated' => $tanggalI,
					'userIdUpdated' => 0,
					'pengajarDiklatId' => 0,
					'mataPelajaranId' => $mataPelajaranId,
					'ruang' => '',
					'tanggal' => $tglMulai,
					'tanggalSelesai' => $tglSelesai,
					'keterangan' => '',
					'jamMulai' => $row['jamMulai'],
					'jamSelesai' => $row['jamSelesai'],
					'jumlahJp' => $jumJp,
					'jumlahHari' => $row['jumlahHari'],
					'statusSimpan' => 'Belum',
				));
			}
			

		} 
		
        $this->update($lastId, array('pelaksanaanSelesai' => $tglSelesai));

		return $this->success('DATA_CREATED');
	}

	public function updateData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

        $id = $this->request('id');
		$userIdUpdated = $this->request('userId');
		$tanggalI = $this->request('tanggalI');

        $publishStatusId = $this->request('publishStatusId');
        $jenisDiklatId = $this->request('jenisDiklatId');
		$tahunDiklat = $this->request('tahunDiklat');
        $lokasiId = $this->request('lokasiId');
        $angkatanId = $this->request('angkatanId');
		$namaDiklat = $this->request('namaDiklat');
		$kuota = $this->request('kuota');
		$statusDiklatId = $this->request('statusDiklatId');

        $error = $this->checkRequest();

        $rs = $this->find($id);
        
        if(!$rs)
            return $this->failed('DATA_NOT_FOUND');

        $last_lokasiId = $rs['lokasiId'];

        if($lokasiId != $last_lokasiId) {
			$rs = $this->select("where jenisDiklatId = ? and angkatanId = ? and lokasiId = ? and tahunDiklat = ? ", array($jenisDiklatId,$angkatanId,$lokasiId, $tahunDiklat));
            if($rs)
                $error['lokasiId'] = $this->string('Lokasi sudah terdaftar');
        }
			
		if(count($error) > 0)
			return $this->invalid('PLEASE_CORRECT', $error);
		
		$rs = array(
			'userIdUpdated' => $userIdUpdated,
			'tglUpdated' => $tanggalI,
			'publishStatusId' => $publishStatusId,
			'jenisDiklatId' => $jenisDiklatId,
			'tahunDiklat' => $tahunDiklat,
			'lokasiId' => $lokasiId,
			'angkatanId' => $angkatanId,
			'namaDiklat' => $namaDiklat,
			'kuota' => $kuota,
			'statusDiklatId' => $statusDiklatId,
		);
		
		$this->update($id, $rs);
		
		return $this->success('DATA_UPDATED');
	}

	public function updateDataPkti($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

        $id = $this->request('id');
		$userIdUpdated = $this->request('userId');
		$tanggalI = $this->request('tanggalI');

        $publishStatusId = $this->request('publishStatusId');
        $jenisDiklatId = $this->request('jenisDiklatId');
        $pelaksanaanMulai = $this->request('pelaksanaanMulai');
		$tahunDiklat = $this->request('tahunDiklat');
        $lokasiId = $this->request('lokasiId');
        $angkatanId = $this->request('angkatanId');
		$namaDiklat = $this->request('namaDiklat');
		$kuota = $this->request('kuota');
		$rsMapel = $this->request('listMapel');
		$statusDiklatId = $this->request('statusDiklatId');

		$daftar_hari = array(
		'Sunday' => 'Minggu',
		'Monday' => 'Senin',
		'Tuesday' => 'Selasa',
		'Wednesday' => 'Rabu',
		'Thursday' => 'Kamis',
		'Friday' => 'Jumat',
		'Saturday' => 'Sabtu'
		);

        $error = $this->checkRequest();

        $rs = $this->find($id);
        
        if(!$rs)
            return $this->failed('DATA_NOT_FOUND');

        $last_lokasiId = $rs['lokasiId'];

        if($lokasiId != $last_lokasiId) {
			$rs = $this->select("where jenisDiklatId = ? and angkatanId = ? and lokasiId = ? and tahunDiklat = ? ", array($jenisDiklatId,$angkatanId,$lokasiId, $tahunDiklat));
            if($rs)
                $error['lokasiId'] = $this->string('Lokasi sudah terdaftar');
        }
			
		if(count($error) > 0)
			return $this->invalid('PLEASE_CORRECT', $error);
		
		$rs = array(
			'userIdUpdated' => $userIdUpdated,
			'tglUpdated' => $tanggalI,
			'publishStatusId' => $publishStatusId,
			'jenisDiklatId' => $jenisDiklatId,
			'tahunDiklat' => $tahunDiklat,
			'lokasiId' => $lokasiId,
			'angkatanId' => $angkatanId,
			'namaDiklat' => $namaDiklat,
			'kuota' => $kuota,
			'statusDiklatId' => $statusDiklatId,
		);
		
		$this->update($id, $rs);

		$mRs = $this->model('tb_bahan_jadwal_diklat');
        $mRs->delete('diklatId', $id);
		$tahunDiklatEnd = (int)$tahunDiklat+1;
		
		$mTglMerah = $this->model('tb_tanggal_merah_detail');
		$rsTanggalMerah = $mTglMerah->select("where year(tanggal) >= ? and year(tanggal) <= ?  ", array($tahunDiklat,$tahunDiklatEnd));

		foreach($rsMapel as $row) {

			$mRs1 = $this->model('vi_bahan_jadwal_diklat');
			$rsJd = $mRs1->select("where diklatId = ? order by id desc limit 1", array($id));
			
			if($rsJd){
				$noU =$rsJd[0]['noUrut']; 
				$t = $rsJd[0]['tanggalSelesai'];

				if ( $row['tipeWaktuId'] == 1){
					$jumHariAsli = 1 ;
					
					$tglSelesai1 = date('Y-m-d', strtotime('+14 days', strtotime($t)));
					$tglMulai1 = date('Y-m-d', strtotime('+1 days', strtotime($t)));
					
					$Variable1 = strtotime($tglMulai1); 
					$Variable2 = strtotime($tglSelesai1); 
					$ijumHari=0;
					for ($currentDate = $Variable1; $currentDate <= $Variable2;  $currentDate += (86400)) {            
						$tglMulai1 = date('Y-m-d', $currentDate); 
						$ijumHari++;
						$namahari = date('l', strtotime($tglMulai1));
						
						if ($daftar_hari[$namahari] != 'Minggu') {
							foreach ($rsTanggalMerah as $key => $val) {
								if ($val['tanggal'] == $tglMulai1) {
									$ijumHari = (int) $ijumHari - 1;
								}
							}
						} else {
							$ijumHari = (int)$ijumHari - 1;
						}

						if ($ijumHari == $jumHariAsli) {
							$ijumHari = 1000;
							$tglSelesai =$tglMulai1;
						}
					};

					if($noU == $row['noUrut']){
						$tglMulai = $t;
						$tglSelesai = $t;
					}else{
						$tglMulai = $tglSelesai;
						$tglSelesai = $tglSelesai;
					}
				}else{
					if($row['tipeTanggalId'] == 1){
						$jumHari = $row['jumlahHari'] ;
						$a = '+'.$jumHari.' days';
						$tglMulai = date('Y-m-d', strtotime('+1 days', strtotime($t)));
						$tglSelesai = date('Y-m-d', strtotime($a, strtotime($t)));

					}else{
						$jumHariAsli = $row['jumlahHari'] ;
						$jumHariAsliMulai = 1 ;
						
						if ($jumHariAsli <= 20)
							$jumHari = 30;
						else
							$jumHari = 80;
						
						$a = '+'.$jumHari.' days';
						$tglSelesai1 = date('Y-m-d', strtotime($a, strtotime($t)));
						$tglMulai1 = date('Y-m-d', strtotime('+1 days', strtotime($t)));
						$ijumHariMulai=0;
						$ijumHari=0;
						$Variable1 = strtotime($tglMulai1); 
						$Variable2 = strtotime($tglSelesai1); 

						for ($currentDate = $Variable1; $currentDate <= $Variable2;  $currentDate += (86400)) {            
							$ijumHari++;
							$ijumHariMulai++;
							$tglMulai1 = date('Y-m-d', $currentDate); 
						
							$namahari = date('l', strtotime($tglMulai1));
							
							if ($daftar_hari[$namahari] != 'Minggu' && $daftar_hari[$namahari] != 'Sabtu') {
								foreach ($rsTanggalMerah as $key => $val) {
									if ($val['tanggal'] == $tglMulai1) {
										$ijumHari = (int)$ijumHari - 1;
										$ijumHariMulai = (int)$ijumHariMulai - 1;
									}
								}
							} else {
								$ijumHari = (int)$ijumHari - 1;
								$ijumHariMulai = (int)$ijumHariMulai - 1;
							}
	
							if ($ijumHariMulai == $jumHariAsliMulai) {
								$tglMulai =$tglMulai1;
								$ijumHariMulai = 100000;
							}

							if ($ijumHari == $jumHariAsli) {
								$tglSelesai =$tglMulai1;
								$ijumHari = 100000;
							}

						};
					}
				}
			}else{
				$tglMulai =$pelaksanaanMulai ;
				$tglSelesai =$pelaksanaanMulai ;
			}
			if($row['jumlahJp']=='-')
			$jumJp = 0;
			else
			$jumJp = $row['jumlahJp'];

			$mRsPengajar1 = $this->model('tb_bahan_jadwal_diklat_pengajar_detail');
			$rsRsPengajar1 = $mRsPengajar1->select("where diklatId = ? order by id desc limit 1", array($id));

			if($row['isData']=='lama'){
				$mRs->insert(array(
					'diklatId' => $id,
					'userIdCreated' => $userIdUpdated,
					'tglCreated' => $tanggalI,
					'userIdUpdated' => $userIdUpdated,
					'pengajarDiklatId' => 0,
					'mataPelajaranId' => $row['id'],
					'ruang' => '',
					'tanggal' => $tglMulai,
					'tanggalSelesai' => $tglSelesai,
					'keterangan' => '',
					'jamMulai' => $row['jamMulai'],
					'jamSelesai' => $row['jamSelesai'],
					'jumlahJp' => $jumJp,
					'jumlahHari' => $row['jumlahHari'],
					'statusSimpan' => 'Belum',
				));

				if($row['tipeMataPelajaranId']==2 && $row['tipeWaktuId']==1)
				$pengajarDiklatId = 34;
			else
				$pengajarDiklatId = 0;
			
			if($row['labelMapelId']==43){
				$bahanJadwalDiklatId = $mRs->getLastInsertId();
				$mSubMenu = $this->model('tb_bahan_jadwal_diklat_pengajar_detail');

					$mSubMenu->insert(array(
						'diklatId' => $id,
						'pengajarDiklatId' => 34,
						'bahanJadwalDiklatId' => $bahanJadwalDiklatId,
					));
			}

			if($pengajarDiklatId > 1){
				if($row['labelMapelId']==7){
					$bahanJadwalDiklatId = $mRs->getLastInsertId();
					$mSubMenu = $this->model('tb_bahan_jadwal_diklat_pengajar_detail');

					$mSubMenu->insert(array(
						'diklatId' => $id,
						'pengajarDiklatId' => 50,
						'bahanJadwalDiklatId' => $bahanJadwalDiklatId,
					));

				}
				$bahanJadwalDiklatId = $mRs->getLastInsertId();
					$mSubMenu = $this->model('tb_bahan_jadwal_diklat_pengajar_detail');
					$mSubMenu->insert(array(
						'diklatId' => $id,
						'pengajarDiklatId' => $pengajarDiklatId,
						'bahanJadwalDiklatId' => $bahanJadwalDiklatId,
					));
				
			}

			if($row['labelMapelId']==7){
				$bahanJadwalDiklatId = $mRs->getLastInsertId();
				$mSubMenu = $this->model('tb_bahan_jadwal_diklat_pengajar_detail');
				$mSubMenu->insert(array(
					'diklatId' => $id,
					'pengajarDiklatId' => 50,
					'bahanJadwalDiklatId' => $bahanJadwalDiklatId,
				));

			}
			
			}else{
				$mMapel = $this->model('tb_mata_pelajaran');

				$rs = array(
					'publishStatusId' => $row['publishStatusId'],
					'noUrut' => $row['noUrut'],
					'labelMapelId' => $row['labelMapelId'],
					'jamMulai' => $row['jamMulai'],
					'jamSelesai' => $row['jamSelesai'],
					'jumlahHari' => $row['jumlahHari'],
					'jenisDiklatId' => $row['jenisDiklatId'],
					'tipeMataPelajaranId' => $row['tipeMataPelajaranId'],
					'tipeWaktuId' => $row['tipeWaktuId'],
					'tipeTanggalId' => $row['tipeTanggalId'],
					'jumlahJp' => $row['jumlahJp'],
				);
				$mMapel->insert($rs);
				$mataPelajaranId = $mMapel->getLastInsertId();

				$mRs->insert(array(
					'diklatId' => $id,
					'userIdCreated' => $userIdUpdated,
					'tglCreated' => $tanggalI,
					'userIdUpdated' => $userIdUpdated,
					'pengajarDiklatId' => 0,
					'mataPelajaranId' => $mataPelajaranId,
					'ruang' => '',
					'tanggal' => $tglMulai,
					'tanggalSelesai' => $tglSelesai,
					'keterangan' => '',
					'jamMulai' => $row['jamMulai'],
					'jamSelesai' => $row['jamSelesai'],
					'jumlahJp' => $jumJp,
					'jumlahHari' => $row['jumlahHari'],
					'statusSimpan' => 'Belum',
				));
			}
			

		} 
		
        $this->update($id, array('pelaksanaanSelesai' => $tglSelesai));
		
		return $this->success('DATA_UPDATED');
	}

	public function simpanCopyDiklat($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

		$id = $this->request('id');
        $userId = $this->request('userId');
        $tanggalI = $this->request('tanggalI');
        $pelaksanaanMulai = $this->request('pelaksanaanMulai');
		$listLokasi = $this->request('listLokasi');
		$rs = $this->find($id);

		$publishStatusId = $rs['publishStatusId'];
		$jenisDiklatId = $rs['jenisDiklatId'];
		$tahunDiklat = $rs['tahunDiklat'];
		$namaDiklat = $rs['namaDiklat'];
		$pelaksanaanSelesai = $rs['_pelaksanaanSelesai'];
		$kuota = $rs['_kuota'];
		$statusDiklatId = $rs['statusDiklatId'];

		$daftar_hari = array(
			'Sunday' => 'Minggu',
			'Monday' => 'Senin',
			'Tuesday' => 'Selasa',
			'Wednesday' => 'Rabu',
			'Thursday' => 'Kamis',
			'Friday' => 'Jumat',
			'Saturday' => 'Sabtu'
			);
		
		$tahunDiklatEnd = (int)$tahunDiklat + 1;
		$mTglMerah = $this->model('tb_tanggal_merah_detail');
		$rsTanggalMerah = $mTglMerah->select("where year(tanggal) >= ? and year(tanggal) <= ?  ", array($tahunDiklat,$tahunDiklatEnd));
			
		$tglMulaiw = date('Y-m-d', strtotime('+0 days', strtotime($pelaksanaanMulai)));
		$date1=$tglMulaiw;
		$namahari = date('l', strtotime($date1));
		$error = array();

		if($daftar_hari[$namahari] != 'Minggu'){
			foreach ($rsTanggalMerah as $key => $val) {
				if ($val['tanggal'] == $tglMulaiw) {
					$error['pelaksanaanMulai'] = $this->string('Pelaksanaan Mulai ini tanggal Merah');
				}
			}
		}else{
			$error['pelaksanaanMulai'] = $this->string('Pelaksanaan Mulai ini hari Minggu');
		}

		if(count($error) > 0)
			return $this->invalid('PLEASE_CORRECT', $error);

		foreach($listLokasi as $rowLokasi) {
		$lokasiId =$rowLokasi['lokasiId'];
		$angkatanId =$rowLokasi['angkatanId'];
			
			$mJenisDiklat = $this->model('tb_jenis_diklat');
			$rowJenisDiklat = $mJenisDiklat->find($jenisDiklatId);

			$namaSingkat = $rowJenisDiklat['namaSingkat'];
		
			$rsTahun = $this->select("where tahunDiklat = ? order by id desc limit 1", array($tahunDiklat));
			
			if(!$rsTahun)
				$kode='BPSDM'.'/'.$tahunDiklat.'/'.$namaSingkat.'/'.'0001';
			else{
				$noUrut1 = substr($rsTahun[0]['kode'] , -4);
				$hasil = (int)$noUrut1 + 1;
				$jml = strlen($hasil);
				
				if ($jml == 1)
					$noUrut = '000'.$hasil;
				else if ($jml == 2)
					$noUrut = '00'.$hasil;
				else if ($jml == 3)
					$noUrut = '0'.$hasil;
				else
					$noUrut = $hasil;
				$kode='BPSDM'.'/'.$tahunDiklat.'/'.$namaSingkat.'/'.$noUrut;
			}

			$rs = array(
				'userIdCreated' => $userId,
				'tglCreated' => $tanggalI,
				'userIdUpdated' => 0,
				'publishStatusId' => $publishStatusId,
				'jenisDiklatId' => $jenisDiklatId,
				'kode' => $kode,
				'tahunDiklat' => $tahunDiklat,
				'lokasiId' => $lokasiId,
				'angkatanId' => $angkatanId,
				'namaDiklat' => $namaDiklat,
				'pelaksanaanMulai' => $pelaksanaanMulai,
				'pelaksanaanSelesai' => $pelaksanaanSelesai,
				'kuota' => $kuota,
				'statusDiklatId' => $statusDiklatId,
			);

			$this->insert($rs);
			$lastId = $this->getLastInsertId();
			
			$mMapel = $this->model('vi_mata_pelajaran');
			$rsMapel = $mMapel->select("where publishStatusId = 2 and jenisDiklatId = ? order by noUrut, jamMulai asc ", array($jenisDiklatId));
			
			$mRs = $this->model('tb_bahan_jadwal_diklat');
			
			
			foreach($rsMapel as $row) {

				$mRs1 = $this->model('vi_bahan_jadwal_diklat');
				$rsJd = $mRs1->select("where diklatId = ? order by id desc limit 1", array($lastId));
				
				if($rsJd){
					$noU =$rsJd[0]['noUrut']; 
					$t = $rsJd[0]['tanggalSelesai'];
	
					if ( $row['tipeWaktuId'] == 1){
						$jumHariAsli = 1 ;
						
						$tglSelesai1 = date('Y-m-d', strtotime('+14 days', strtotime($t)));
						$tglMulai1 = date('Y-m-d', strtotime('+1 days', strtotime($t)));
						
						$Variable1 = strtotime($tglMulai1); 
						$Variable2 = strtotime($tglSelesai1); 
						$ijumHari=0;
						for ($currentDate = $Variable1; $currentDate <= $Variable2;  $currentDate += (86400)) {            
							$tglMulai1 = date('Y-m-d', $currentDate); 
							$ijumHari++;
							$namahari = date('l', strtotime($tglMulai1));
							
							if ($daftar_hari[$namahari] != 'Minggu') {
								foreach ($rsTanggalMerah as $key => $val) {
									if ($val['tanggal'] == $tglMulai1) {
										$ijumHari = (int) $ijumHari - 1;
									}
								}
							} else {
								$ijumHari = (int)$ijumHari - 1;
							}
	
							if ($ijumHari == $jumHariAsli) {
								$ijumHari = 1000;
								$tglSelesai =$tglMulai1;
							}
						};
	
						if($noU == $row['noUrut']){
							$tglMulai = $t;
							$tglSelesai = $t;
						}else{
							$tglMulai = $tglSelesai;
							$tglSelesai = $tglSelesai;
						}
					}else{
						if($row['tipeTanggalId'] == 1){
							$jumHari = $row['jumlahHari'];
							$a = '+'.$jumHari.' days';
							$tglMulai = date('Y-m-d', strtotime('+1 days', strtotime($t)));
							$tglSelesai = date('Y-m-d', strtotime($a, strtotime($t)));
	
						}else{
							$jumHariAsli = $row['jumlahHari'] ;
							$jumHariAsliMulai = 1 ;
							
							if ($jumHariAsli <= 20)
								$jumHari = 30;
							else
								$jumHari = 120;
							
							$a = '+'.$jumHari.' days';
							$tglSelesai1 = date('Y-m-d', strtotime($a, strtotime($t)));
							$tglMulai1 = date('Y-m-d', strtotime('+1 days', strtotime($t)));
							$ijumHariMulai=0;
							$ijumHari=0;
							$Variable1 = strtotime($tglMulai1); 
							$Variable2 = strtotime($tglSelesai1); 
	
							for ($currentDate = $Variable1; $currentDate <= $Variable2;  $currentDate += (86400)) {            
								$ijumHari++;
								$ijumHariMulai++;
								$tglMulai1 = date('Y-m-d', $currentDate); 
							
								$namahari = date('l', strtotime($tglMulai1));
								
								if ($daftar_hari[$namahari] != 'Minggu' && $daftar_hari[$namahari] != 'Sabtu') {
									foreach ($rsTanggalMerah as $key => $val) {
										if ($val['tanggal'] == $tglMulai1) {
											$ijumHari = (int)$ijumHari - 1;
											$ijumHariMulai = (int)$ijumHariMulai - 1;
										}
									}
								} else {
									$ijumHari = (int)$ijumHari - 1;
									$ijumHariMulai = (int)$ijumHariMulai - 1;
								}
		
								if ($ijumHariMulai == $jumHariAsliMulai) {
									$tglMulai =$tglMulai1;
									$ijumHariMulai = 100000;
								}
	
								if ($ijumHari == $jumHariAsli) {
									$tglSelesai =$tglMulai1;
									$ijumHari = 100000;
								}
	
							};
						}
					}
				}else{
					$tglMulai =$pelaksanaanMulai ;
					$tglSelesai =$pelaksanaanMulai ;
				}
				if($row['jumlahJp']=='-')
				$jumJp = 0;
				else
				$jumJp = $row['jumlahJp'];
				$mRs->insert(array(
					'diklatId' => $lastId,
					'userIdCreated' => $userId,
					'tglCreated' => $tanggalI,
					'userIdUpdated' => 0,
					'pengajarDiklatId' => 0,
					'mataPelajaranId' => $row['id'],
					'ruang' => '',
					'tanggal' => $tglMulai,
					'tanggalSelesai' => $tglSelesai,
					'keterangan' => '',
					'jamMulai' => $row['jamMulai'],
					'jamSelesai' => $row['jamSelesai'],
					'jumlahJp' => $jumJp,
					'jumlahHari' => $row['jumlahHari'],
					'statusSimpan' => 'Belum',
				));
				if($row['tipeMataPelajaranId']==2 && $row['tipeWaktuId']==1)
				$pengajarDiklatId = 34;
			else
				$pengajarDiklatId = 0;

				if($row['labelMapelId']==43){
					$bahanJadwalDiklatId = $mRs->getLastInsertId();
					$mSubMenu = $this->model('tb_bahan_jadwal_diklat_pengajar_detail');
	
						$mSubMenu->insert(array(
							'diklatId' => $lastId,
							'pengajarDiklatId' => 34,
							'bahanJadwalDiklatId' => $bahanJadwalDiklatId,
						));
				}

			if($pengajarDiklatId > 1){
				if($row['labelMapelId']==7){
					$bahanJadwalDiklatId = $mRs->getLastInsertId();
					$mSubMenu = $this->model('tb_bahan_jadwal_diklat_pengajar_detail');

					$mSubMenu->insert(array(
						'diklatId' => $lastId,
						'pengajarDiklatId' => 50,
						'bahanJadwalDiklatId' => $bahanJadwalDiklatId,
					));

				}else{
					

				}

				$bahanJadwalDiklatId = $mRs->getLastInsertId();
					$mSubMenu = $this->model('tb_bahan_jadwal_diklat_pengajar_detail');
					$mSubMenu->insert(array(
						'diklatId' => $lastId,
						'pengajarDiklatId' => $pengajarDiklatId,
						'bahanJadwalDiklatId' => $bahanJadwalDiklatId,
					));
				
			}

			if($row['labelMapelId']==7){
				$bahanJadwalDiklatId = $mRs->getLastInsertId();
				$mSubMenu = $this->model('tb_bahan_jadwal_diklat_pengajar_detail');
				$mSubMenu->insert(array(
					'diklatId' => $lastId,
					'pengajarDiklatId' => 50,
					'bahanJadwalDiklatId' => $bahanJadwalDiklatId,
				));

			}
	
			} 
			
			$this->update($lastId, array('pelaksanaanSelesai' => $tglSelesai));

		}

		return $this->success('DATA_CREATED');
	}

	public function cariJadwalPengajarData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

        $id = $this->request('id');
        $pengajarDiklatId = $this->request('pengajarDiklatId');
        
        $tanggalSelesai = $this->request('tanggalSelesai');
        
        $mMapel = $this->model('vi_bahan_jadwal_diklat');
        $rowMapel = $mMapel->find($id);
		$tanggal = $rowMapel['tanggal'];
        $lastPengajarDiklatId = $rowMapel['pengajarDiklatId'];
        $lastTanggal = $rowMapel['tanggal'];
        $diklatId = $rowMapel['diklatId'];
        $jamMulai = $rowMapel['jamMulai'];
        $jamSelesai = $rowMapel['jamSelesai'];
        $jumlahJp = $rowMapel['jumlahJp'];
        $tipeWaktuId = $rowMapel['tipeWaktuId'];
		$tipeMataPelajaranId = $rowMapel['tipeMataPelajaranId'];
		$tanggalJamMulai = $tanggal.' '.$jamMulai. ":" . "01";
		$tanggalJamSelesai = $tanggal.' '.$jamSelesai. ":" . "01";

		$mMapelDetail = $this->model('vi_bahan_jadwal_diklat_pengajar_detail');
		$error = array();
        if($tipeMataPelajaranId==1 && $tipeWaktuId ==1){
			if($lastPengajarDiklatId == 0){
        		$rsMapel = $mMapelDetail->select("where pengajarDiklatId = ? and ((tanggalJamMulai between ? and ?) OR  (tanggalJamSelesai between ? and ?) or (tanggalJamMulai <= ? and tanggalJamSelesai >= ?))", array($pengajarDiklatId,$tanggalJamMulai,$tanggalJamSelesai,$tanggalJamMulai,$tanggalJamSelesai,$tanggalJamMulai,$tanggalJamSelesai));
				if($rsMapel){
					$errorName=$rsMapel[0]['namaDiklat'];
					$error[0] = $this->string('Pengajar Diklat sudah terdaftar '.$errorName);
				}
			}else if($lastPengajarDiklatId != $pengajarDiklatId || $lastTanggal != $tanggal){
				$rsMapel = $mMapelDetail->select("where pengajarDiklatId = ? and ((tanggalJamMulai between ? and ?) OR  (tanggalJamSelesai between ? and ?) or (tanggalJamMulai <= ? and tanggalJamSelesai >= ?))", array($pengajarDiklatId, $tanggalJamMulai, $tanggalJamSelesai, $tanggalJamMulai, $tanggalJamSelesai, $tanggalJamMulai, $tanggalJamSelesai));
				if($rsMapel){
					$errorName=$rsMapel[0]['namaDiklat'];
					$error[0] = $this->string('Pengajar Diklat sudah terdaftar '.$errorName);
				}
			}
		}	

		if(count($error) > 0){
			return $this->failed('PLEASE_CORRECT', $error);
			exit;
		}else{
			return $this->success('DATA_CREATED');
		}

	}

	public function simpanJadwalData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

        $id = $this->request('id');
        $userId = $this->request('userId');
        $tanggalI = $this->request('tanggalI');
		$pengajarDiklatId = $this->request('pengajarDiklatId');
		
        $ruang = $this->request('ruang');
        $keterangan = $this->request('keterangan');
        $tanggal = $this->request('tanggal');
        $tanggalSelesai = $this->request('tanggalSelesai');
        
        $listPengajar = $this->request('listPengajar');
        $mMapel = $this->model('vi_bahan_jadwal_diklat');
        $rowMapel = $mMapel->find($id);

        $lastPengajarDiklatId = $rowMapel['pengajarDiklatId'];
        $lastTanggal = $rowMapel['tanggal'];
        $diklatId = $rowMapel['diklatId'];
        $jamMulai = $rowMapel['jamMulai'];
        $jamSelesai = $rowMapel['jamSelesai'];
        $jumlahJp = $rowMapel['jumlahJp'];
        $tipeWaktuId = $rowMapel['tipeWaktuId'];
		$tipeMataPelajaranId = $rowMapel['tipeMataPelajaranId'];
		$tanggalJamMulai = $tanggal.' '.$jamMulai;
		$tanggalJamSelesai = $tanggal.' '.$jamSelesai;

		$rs = array(
			'userIdUpdated' => $userId,
			'tglUpdated' => $tanggalI,
			'ruang' => $ruang,
			'keterangan' => '',
			'statusSimpan' => 'Simpan',
		);
		
		$mItems = $this->model('tb_bahan_jadwal_diklat');
		
		$mItems->update($id, $rs);

		$mRs = $this->model('tb_bahan_jadwal_diklat_pengajar_detail');
		$mRs->delete('bahanJadwalDiklatId', $id);
		if($listPengajar){
			foreach($listPengajar as $sub) {
				$mRs->insert(array(
						'bahanJadwalDiklatId' => $id,
						'diklatId' => $diklatId,
						'pengajarDiklatId' => $sub['pengajarDiklatId'],
				));
			}  
		}
		return $this->success('DATA_CREATED');
	}


	public function deleteData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

		$id = $this->request('id');
        $rs = $this->find($id);
		$mItems = $this->model('tb_pendaftaran_diklat');
		$error = array();
		$rsItems = $mItems->select("where diklatId = ?", array($id));

		if($rsItems){
			if(count($error) == 0)
				$error['0'] = 'Data dibawah ini tidak bisa di hapus karena terkait dengan Data lain';
			
			$rs = $this->find($id);
			$error[1] = $rs['namaDiklat'];    

			return $this->failed('PLEASE_CORRECT', $error);
        	exit;   
		}else{
			$mItems = $this->model('tb_bahan_jadwal_diklat');
			$mItems1 = $this->model('tb_bahan_jadwal_diklat_pengajar_detail');
	
			$mItems->delete('diklatId', $id);
			$mItems1->delete('diklatId', $id);
	
			$this->delete($id);
			return $this->success('DATA_DELETED');
		}
		
	}

	public function multipleDeleteData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

		$multipleId = $this->request('multipleId');
		$mItems = $this->model('tb_bahan_jadwal_diklat');
		$mItems1 = $this->model('tb_bahan_jadwal_diklat_pengajar_detail');

		foreach($multipleId as $id){
			
			$mItems->delete('diklatId', $id);
			$mItems1->delete('diklatId', $id);

            $this->delete($id);

		}

		return $this->success('DATA_DELETED');
	}

	public function createExcelDetail($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();
        
        
        // Excel Settings
        
        $id = $this->request('id');
        $this->load->library('excel');

		$render = 'renderDetail';
		$rs = $this->find($id);
		$mItems = $this->model('vi_bahan_jadwal_diklat');
		$rsJadwalDiklat = $mItems->select('where diklatId = ? order by noUrut, jamMulai asc', array($id));
		
        if (count($rs) > 0) {
            
            // return $this->invalid('DATA_READ', $data);
            $nameFile='BahanDiklatDetail';
            $fileUrl = $this->$render(
                $nameFile,
                array(
                    'rs' => $rs,
                    'rsJadwalDiklat' => $rsJadwalDiklat,
                    )
            );

            return $this->success('FILE_CREATED', $fileUrl);
        }
        else
            return $this->failed('FILE_CREATE_FAILED');
    }

    private function renderDetail($template, $data) {

        $rs = $data['rs'];
        $rsJadwalDiklat = $data['rsJadwalDiklat'];

        $reader = PHPExcel_IOFactory::createReader('Excel5');
        $templateUrl = "application/views/excel/$template.xls";

        $excel = $reader->load($templateUrl);
        $sheet = $excel->setActiveSheetIndex(0);

        $i = 0;
        $x =13;
		
		$tahun = $rs['tahunDiklat'];
		$sheet->setCellValue("G3", 'NOMOR       : '.'                               '.$rs['namaSingkat'].'/'.$rs['tahunDiklat']);
		$sheet->setCellValue("G4", 'TANGGAL    : '.$rs['pelaksanaanMulai']);
		$sheet->setCellValue("A7", $rs['namaDiklat']);
		// $sheet->setCellValue("A7", $rs['jenisDiklat'].' '.$rs['angkatan']);
		$sheet->setCellValue("A8", $rs['lokasi']);
		$sheet->setCellValue("A9", 'Tahun Anggaran '.$rs['tahunDiklat']);
		$styleArray = array(
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				)
			)
		);
		$styleArrayPengajar = array(
			'font'  => array(
				'bold'  => true,
			));
			$styleArrayBorderAtas = array(
				'borders' => array(
					'top' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN
					),
					'right' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN
					),
					),
				);
				$styleArrayBorderKanan = array(
					'borders' => array(
						'right' => array(
							'style' => PHPExcel_Style_Border::BORDER_THIN
						),
						),
					);
					$styleArrayBorderKiri = array(
						'borders' => array(
							'left' => array(
								'style' => PHPExcel_Style_Border::BORDER_THIN
							),
							),
						);
				$styleArrayBorderBawah = array(
					'borders' => array(
						'bottom' => array(
							'style' => PHPExcel_Style_Border::BORDER_THIN
						)
						),
				);
		$mItems1 = $this->model('vi_bahan_jadwal_diklat_pengajar_detail');
		$ab=0;
        foreach($rsJadwalDiklat as $row) {
			$x++;
			$ab++;

				$mUrut = $this->model('vi_bahan_jadwal_diklat');
				$noUrut = $row['noUrut'];
				$rsNoUrut = $mUrut->select('where diklatId = ? and noUrut = ? ', array($row['diklatId'], $noUrut));
				
				if($ab != count($rsNoUrut)){
					if($ab == 1){
						$mUrut1 = $this->model('vi_bahan_jadwal_diklat_pengajar_detail');
						$rsUrut1 = $mUrut->select('where diklatId = ? and noUrut = ? ', array($row['diklatId'], $noUrut));
						$tambah =0;
						
						foreach($rsUrut1 as $row1) {

							$rsUruta = $mUrut1->select('where bahanJadwalDiklatId = ? and diklatId = ? and noUrut = ? ', array($row1['id'],$row['diklatId'], $noUrut));
							if(count($rsUruta) > 1){
								$jumdetail1 = count($rsUruta) ;
								$hasil =  (int)$jumdetail1 - 1;
								$tambah =(int)$tambah + (int)$hasil;
							}
						}

						$jumPengajar = count($rsNoUrut) ;
						$jmlRow = ((int)$x-1) + (int)$jumPengajar + (int)$tambah;

						$sheet->mergeCells("A$x".":A$jmlRow");
						$sheet->mergeCells("B$x".":B$jmlRow");
						$sheet->mergeCells("C$x".":C$jmlRow");

						$sheet->getStyle("A$x".":A$jmlRow")->applyFromArray($styleArray);
						$sheet->getStyle("B$x".":B$jmlRow")->applyFromArray($styleArray);
						$sheet->getStyle("C$x".":C$jmlRow")->applyFromArray($styleArray);
						$sheet->getStyle("D$x")->applyFromArray($styleArrayBorderAtas);
						$sheet->getStyle("D$jmlRow")->applyFromArray($styleArrayBorderBawah);
						$sheet->getStyle("D$x".":D$jmlRow")->applyFromArray($styleArrayBorderKanan);
						$sheet->getStyle("E$x")->applyFromArray($styleArrayBorderAtas);
						$sheet->getStyle("E$jmlRow")->applyFromArray($styleArrayBorderBawah);
						$sheet->getStyle("E$x".":E$jmlRow")->applyFromArray($styleArrayBorderKanan);
						$sheet->getStyle("F$x")->applyFromArray($styleArrayBorderAtas);
						$sheet->getStyle("F$jmlRow")->applyFromArray($styleArrayBorderBawah);
						$sheet->getStyle("F$x".":F$jmlRow")->applyFromArray($styleArrayBorderKanan);
						$sheet->getStyle("G$x")->applyFromArray($styleArrayBorderAtas);
						$sheet->getStyle("G$jmlRow")->applyFromArray($styleArrayBorderBawah);
						$sheet->getStyle("G$x".":G$jmlRow")->applyFromArray($styleArrayBorderKanan);
						$sheet->getStyle("H$x")->applyFromArray($styleArrayBorderAtas);
						$sheet->getStyle("H$jmlRow")->applyFromArray($styleArrayBorderBawah);
						$sheet->getStyle("H$x".":H$jmlRow")->applyFromArray($styleArrayBorderKanan);
					}
				}else{
					
					if(count($rsNoUrut)==1){
						$mUrut1 = $this->model('vi_bahan_jadwal_diklat_pengajar_detail');
						
						$rsUrut1 = $mUrut1->select('where diklatId = ? and noUrut = ? ', array($row['diklatId'], $noUrut));
						if(count($rsUrut1) > 1){
							$jumdetail1 = count($rsUrut1) ;
							$tambah = (int)$jumdetail1 -1;
						}else{
							$tambah =0;
						}
	
						$jumPengajar = count($rsNoUrut) ;
						$jmlRow = ((int)$x-1) + (int)$jumPengajar + (int)$tambah;
						$sheet->mergeCells("A$x".":A$jmlRow");
						$sheet->mergeCells("B$x".":B$jmlRow");
						$sheet->mergeCells("C$x".":C$jmlRow");

						$sheet->getStyle("A$x".":A$jmlRow")->applyFromArray($styleArray);
						$sheet->getStyle("B$x".":B$jmlRow")->applyFromArray($styleArray);
						$sheet->getStyle("C$x".":C$jmlRow")->applyFromArray($styleArray);
						$sheet->getStyle("D$x")->applyFromArray($styleArrayBorderAtas);
						$sheet->getStyle("D$jmlRow")->applyFromArray($styleArrayBorderBawah);
						$sheet->getStyle("D$x".":D$jmlRow")->applyFromArray($styleArrayBorderKanan);
						$sheet->getStyle("E$x")->applyFromArray($styleArrayBorderAtas);
						$sheet->getStyle("E$jmlRow")->applyFromArray($styleArrayBorderBawah);
						$sheet->getStyle("E$x".":E$jmlRow")->applyFromArray($styleArrayBorderKanan);
						$sheet->getStyle("F$x")->applyFromArray($styleArrayBorderAtas);
						$sheet->getStyle("F$jmlRow")->applyFromArray($styleArrayBorderBawah);
						$sheet->getStyle("F$x".":F$jmlRow")->applyFromArray($styleArrayBorderKanan);
						$sheet->getStyle("G$x")->applyFromArray($styleArrayBorderAtas);
						$sheet->getStyle("G$jmlRow")->applyFromArray($styleArrayBorderBawah);
						$sheet->getStyle("G$x".":G$jmlRow")->applyFromArray($styleArrayBorderKanan);
						$sheet->getStyle("H$x")->applyFromArray($styleArrayBorderAtas);
						$sheet->getStyle("H$jmlRow")->applyFromArray($styleArrayBorderBawah);
						$sheet->getStyle("H$x".":H$jmlRow")->applyFromArray($styleArrayBorderKanan);
					}
					
					$ab=0;
				}
			
				$statusSimpan = $row['statusSimpan']; 
                if ($row['jumlahJp'] == 0)
						$allJam = '-';
					else
						$allJam = $row['jumlahJp'];
				$sheet->setCellValue("A$x", $row['noUrut']);
				
				if ($row['tipeWaktuId'] == 1) {
						$allTanggal = $this->indonesian_date1($row['tanggal'] ,"j F y");
						$hari = $this->indonesian_date($row['tanggal'] ,"l");
						$allWaktu = $row['jamMulai'].' - '.$row['jamSelesai'];
						
						$sheet->setCellValue("B$x", $hari);
						$sheet->setCellValue("C$x", $allTanggal);
						$sheet->setCellValue("D$x", $allWaktu);
						$sheet->setCellValue("E$x", $row['mataPelajaran']);
						$sheet->setCellValue("F$x", $allJam);
						$sheet->setCellValue("H$x", $row['keterangan']);

						$rslistJadwalDiklatPengajar = $mItems1->select('where bahanJadwalDiklatId = ? order by id asc', array($row['id']));
		
						$xB = (int)$x - 1;
						if(count($rslistJadwalDiklatPengajar) > 1){
							$jumPengajar = count($rslistJadwalDiklatPengajar) ;
							$jmlRow = (int)$xB + (int)$jumPengajar;
		
							$sheet->mergeCells("D$x".":D$jmlRow");
							$sheet->mergeCells("E$x".":E$jmlRow");
							$sheet->mergeCells("F$x".":F$jmlRow");
							$sheet->mergeCells("H$x".":H$jmlRow");

							$noi = 0;
							$x = (int)$x - 1;
							foreach($rslistJadwalDiklatPengajar as $sub) {
								$x++;
								$noi++;
								$sheet->setCellValue("G$x", $noi.'. '.$sub['namaPengajar']);
								
								$sheet->getStyle("G$x")->applyFromArray($styleArrayPengajar);
							};  
						}else{

							foreach($rslistJadwalDiklatPengajar as $sub) {
								$sheet->setCellValue("G$x", $sub['namaPengajar']);
								$sheet->getStyle("G$x")->applyFromArray($styleArrayPengajar);
							};
						}
						

				} else {
					
						$allTanggal = $this->indonesian_date1($row['tanggal'] ,"l j F y").' s/d '.$this->indonesian_date1($row['tanggalSelesai'] ,"l j F y");
						$allWaktu = $row['jumlahHari'].' hari';
						$hari = '';
						$noi = 0;

						$sheet->setCellValue("B$x", $hari);
						$sheet->setCellValue("C$x", $allTanggal);
						
						$sheet->getStyle("C$x")->getAlignment()->setWrapText(true);
						$sheet->setCellValue("D$x", $allWaktu);
						$sheet->setCellValue("E$x", $row['mataPelajaran']);
						$sheet->setCellValue("F$x", $allJam);
						$sheet->setCellValue("H$x", $row['keterangan']);
					
					$rslistJadwalDiklatPengajar = $mItems1->select('where bahanJadwalDiklatId = ? order by id asc', array($row['id']));
		
					$xB = (int)$x - 1;
					if(count($rslistJadwalDiklatPengajar) > 1){
						$jumPengajar = count($rslistJadwalDiklatPengajar) ;
						$jmlRow = (int)$xB + (int)$jumPengajar;
	
						$sheet->mergeCells("D$x".":D$jmlRow");
						$sheet->mergeCells("E$x".":E$jmlRow");
						$sheet->mergeCells("F$x".":F$jmlRow");
						$sheet->mergeCells("H$x".":H$jmlRow");

						$x = (int)$x - 1;
					foreach($rslistJadwalDiklatPengajar as $sub) {
						$x++;
						$noi++;
						$sheet->setCellValue("G$x", $noi.'. '.$sub['namaPengajar']);
						
					$sheet->getStyle("G$x")->applyFromArray($styleArrayPengajar);
					};  

					}else{
						$sheet->getRowDimension($x)->setRowHeight(60);
					foreach($rslistJadwalDiklatPengajar as $sub) {
						$sheet->setCellValue("G$x", $sub['namaPengajar']);
						$sheet->getStyle("G$x")->applyFromArray($styleArrayPengajar);
					};  

					}
					
					$allJam = '';

				}
				
		}
		$styleLeft = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
			)
		);

		$x = (int)$x +3;
		$sheet->mergeCells("A$x".":B$x");
		$sheet->setCellValue("A$x", 'Keterangan :');
		$sheet->mergeCells("D$x".":E$x");
		$sheet->setCellValue("D$x", '10.00 - 10.15 = Istirahat   (Snack)');
		$sheet->getStyle("D$x".":E$x")->applyFromArray($styleLeft);
		$sheet->mergeCells("F$x".":H$x");
		$sheet->setCellValue("F$x", 'KEPALA BADAN PENGEMBANGAN');
		$x = (int)$x +1;
		$sheet->mergeCells("D$x".":E$x");
		$sheet->setCellValue("D$x", '12.30 - 13.30 = Istirahat   (Makan siang/Sholat)');
		$sheet->getStyle("D$x".":E$x")->applyFromArray($styleLeft);
		$sheet->mergeCells("F$x".":H$x");
		$sheet->setCellValue("F$x", 'SUMBER DAYA MANUSIA');
		$x = (int)$x +1;
		$sheet->mergeCells("D$x".":E$x");
		$sheet->setCellValue("D$x", '15.45 - 16.00 = Istirahat   (Snack)');
		$sheet->getStyle("D$x".":E$x")->applyFromArray($styleLeft);
		$sheet->mergeCells("F$x".":H$x");
		$sheet->setCellValue("F$x", 'PROVINSI LAMPUNG');
		$x = (int)$x +1;
		$sheet->mergeCells("D$x".":E$x");
		$sheet->setCellValue("D$x", '18.15 - 19.00 = Istirahat   (Makan malam/sholat)');
		$sheet->getStyle("D$x".":E$x")->applyFromArray($styleLeft);
		$x = (int)$x +1;
		$sheet->mergeCells("A$x".":B$x");
		$sheet->setCellValue("A$x", 'Catatan        :');
		$sheet->mergeCells("D$x".":E$x");
		$sheet->setCellValue("D$x", 'Jadwal sewaktu-waktu bisa berubah');
		$sheet->getStyle("D$x".":E$x")->applyFromArray($styleLeft);
		
		$styleArray1 = array(
			'font'  => array(
				'bold'  => true,
				'size'  => 11,
				'name'  => 'Verdana',
				'underline' => PHPExcel_Style_Font::UNDERLINE_SINGLE
			));
			
			$id = 1;
			$mFooter = $this->model('vi_footer_excel');
			$rsFooter = $mFooter->find($id);

		$x = (int)$x +2;
		$sheet->mergeCells("F$x".":H$x");
		$sheet->setCellValue("F$x", $rsFooter['name']);
		$sheet->getStyle("F$x")->applyFromArray($styleArray1);
		$x = (int)$x +1;
		$sheet->mergeCells("F$x".":H$x");
		$sheet->setCellValue("F$x", 'Plt. Kepala badan');
		$x = (int)$x +1;
		$sheet->mergeCells("F$x".":H$x");
		$sheet->setCellValue("F$x", 'NIP.'.$rsFooter['nip']);

        $fileName = $this->encrypt().'.xls';
        $filePath = "asset/tmp/$fileName";

        $writer = PHPExcel_IOFactory::createWriter($excel, 'Excel5');
        $writer->save($filePath);

        $fileUrl = base_url().$filePath;

        return $fileUrl;
    }
    
    
}
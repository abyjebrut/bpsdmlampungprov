<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class OrderedPages extends MY_Model {
    protected $view = 'vi_all_ordered_pages';
    
    public function readData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();
        
        // Order By
        
        $orderBy = $this->input->post('orderBy');
        $reverse = $this->input->post('reverse');
        
        $orderStmt = '';
        
        if (!empty($orderBy)) {
            $orderStmt = "order by $orderBy";

            if (!empty($reverse)) {
                if ($reverse == 1)
                    $orderStmt .= ' desc';
            }
        }

        // Query

		$rs = $this->select($orderStmt);
        $rsCount = count($rs);

		return $this->success('DATA_READ', $rs, $rsCount);
    }
    
    public function detailData($params = array())
    {
    	if(!$this->setSettings(array(
    			'authen' => 'free',
    			'requestSource' => $params,
    	)))
    		return $this->denied();
    
    	$id = $this->request('id');
    
    	// Query
    
    	$rs = $this->find($id);
    
    	if($rs)
    		return $this->success('DATA_READ', $rs);
    	else
    		return $this->failed('DATA_NOT_FOUND');
    }

}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BahanJadwalPengajar extends MY_Model {
    protected $table = 'tb_bahan_jadwal_pengajar';
    protected $view = 'vi_bahan_jadwal_pengajar';
    
    public function readData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();

        // Order

        
        $orderBy = $this->request('orderBy');
        $reverse = $this->request('reverse');

        $orderStmt = '';

        if (!empty($orderBy)) {
            $orderStmt = "order by $orderBy";

            if (!empty($reverse)) {
                if ($reverse == 1)
                    $orderStmt .= ' desc';
            }
        }

        // Limit
        $jadwalPengajarId = $this->request('jadwalPengajarId');
        $page = $this->request('page');
		$count = $this->request('count');

        $limitStmt = '';

      	$rowsCount = $this->getRowsCount($orderStmt);
        
      	$pageCount = 1;

        if (!empty($page) && !empty($count)) {
            $row = ($page * $count) - $count;
            $limitStmt = "limit $row, $count";

            $pageCount = ceil($rowsCount / $count);

            if($pageCount < 1)
                $pageCount = 1;
        }

        // Query
        
        
//	   $rs = $this->select("where jadwalPengajarId = ? ".$orderStmt.' '.$limitStmt, array($jadwalPengajarId));
        $rs = $this->select("where jadwalPengajarId = ? order by noUrut, tanggal, jamMulai asc ", array($jadwalPengajarId));
        $extRs = array();

        $mSubMenu = $this->model('vi_bahan_jadwal_pengajar_detail');

        foreach($rs as $row) {

            if ($row['tipeWaktuId'] == 2) {
                $rsSubMenu = $mSubMenu->select('where bahanJadwalPengajarId = ?', array($row['id']));
                $row['listPengajar'] = $rsSubMenu;
            }

            $extRs[] = $row;
        }
        $extra = array(
            'rowsCount' => $rowsCount,
            'pageCount' => $pageCount,
        );

		return $this->success('DATA_READ', $extRs, $extra);
	}

    public function filterData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();
        
        // Query

        $field = $this->request('field');
        $keyword = $this->request('keyword');

        $queryStmt = "where $field like ?";
        $rowsCount = $this->getRowsCount($queryStmt, array("$keyword%"));

        // Limit

        $page = $this->request('page');
		$count = $this->request('count');

        $limitStmt = '';
        $pageCount = 1;

        if (!empty($page) && !empty($count)) {
            $row = ($page * $count) - $count;
            $limitStmt = "limit $row, $count";

            $pageCount = ceil($rowsCount / $count);

            if($pageCount < 1)
                $pageCount = 1;
        }

        $rs = $this->select($queryStmt, array("$keyword%"));

        $extra = array(
            'rowsCount' => $rowsCount,
            'pageCount' => $pageCount,
        );

		return $this->success('DATA_READ', $rs, $extra);
	}

	public function detailData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();

		$id = $this->request('id');
        
        // Query
        
        $rs = $this->find($id);
        $jenisDiklatId = $rs['jenisDiklatId'];
        $angkatanId = $rs['angkatanId'];

        $mItems = $this->model('vi_bahan_jadwal_pengajar_detail');
		$rs['listPengajar'] = $mItems->select('where bahanJadwalPengajarId = ? order by id', array($id));
        
        $mItems = $this->model('vi_filter_mapel');
        $rs['listFilterMapel'] = $mItems->select('where angkatanId = ? and jenisDiklatId = ?  order by id', array($angkatanId,$jenisDiklatId));
        
        $mItems = $this->model('vi_mata_pelajaran');
		$rs['listMapel'] = $mItems->select('where jenisDiklatId = ?  order by id', array($jenisDiklatId));


        if($rs)
            return $this->success('DATA_READ', $rs);
        else
            return $this->failed('DATA_NOT_FOUND');
	}

    private function checkRequest() {
        $pengajarDiklatId = $this->request('pengajarDiklatId');
        $mataPelajaranId = $this->request('mataPelajaranId');
        $tempatId = $this->request('tempatId');
        $tanggal = $this->request('tanggal');
        $listPengajar = $this->request('listPengajar');
        $mMapel = $this->model('tb_mata_pelajaran');
        $rowMapel = $mMapel->find($mataPelajaranId);

        $tipeWaktuId = $rowMapel['tipeWaktuId'];
        
		$error = array();
		if($tipeWaktuId==1){
            if(empty($pengajarDiklatId))
                $error['pengajarDiklatId'] = $this->string('Pengajar Diklat diperlukan');
        }
        if(empty($mataPelajaranId))
			$error['mataPelajaranId'] = $this->string('Mata Pelajaran diperlukan');
        if(empty($tempatId))
			$error['tempatId'] = $this->string('Tempat diperlukan');
        if(empty($tanggal))
            $error['tanggal'] = $this->string('Tanggal diperlukan');
            
        if($tipeWaktuId==2){
            if(empty($listPengajar))
                $error['listPengajar'] = $this->string('Pengajar Diklat diperlukan');
        }
        return $error;
    }

	public function createData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

		$error = $this->checkRequest();

        $jadwalPengajarId = $this->request('jadwalPengajarId');
		$pengajarDiklatId = $this->request('pengajarDiklatId');
        $mataPelajaranId = $this->request('mataPelajaranId');
        $tempatId = $this->request('tempatId');
        $tanggal = $this->request('tanggal');
        $tanggalSelesai = $this->request('tanggalSelesai');
        
        $listPengajar = $this->request('listPengajar');
        $mMapel = $this->model('tb_mata_pelajaran');
        $rowMapel = $mMapel->find($mataPelajaranId);

        $jamMulai = $rowMapel['jamMulai'];
        $jamSelesai = $rowMapel['jamSelesai'];
        $jumlahJp = $rowMapel['jumlahJp'];
        $tipeWaktuId = $rowMapel['tipeWaktuId'];
        $tipeMataPelajaranId = $rowMapel['tipeMataPelajaranId'];

        if($tipeMataPelajaranId==1){
            $rs = $this->select("where jadwalPengajarId = ? and pengajarDiklatId = ? and tanggal = ? and mataPelajaranId = ? ", array($jadwalPengajarId,$pengajarDiklatId ,$tanggal ,$mataPelajaranId));

		if($rs)
            $error['pengajarDiklatId'] = $this->string('Pengajar Diklat sudah terdaftar');
        }
        

		if(count($error) > 0)
			return $this->invalid('PLEASE_CORRECT', $error);

        

		$rs = array(
            'jadwalPengajarId' => $jadwalPengajarId,
            'pengajarDiklatId' => $pengajarDiklatId,
            'mataPelajaranId' => $mataPelajaranId,
            'tempatId' => $tempatId,
            'tanggal' => $tanggal,
            'tanggalSelesai' => $tanggalSelesai,
            'jamMulai' => $jamMulai,
            'jamSelesai' => $jamSelesai,
            'jumlahJp' => $jumlahJp,
        );
        $this->insert($rs);
        $lastId = $this->getLastInsertId();

        $mRs = $this->model('tb_bahan_jadwal_pengajar_detail');
        if($tipeWaktuId==2){
            foreach($listPengajar as $sub) {
                $mRs->insert(array(
                        'bahanJadwalPengajarId' => $lastId,
                        'jadwalPengajarId' => $jadwalPengajarId,
                        'pengajarDiklatId' => $sub['pengajarDiklatId'],
                ));
            }  
        }

		return $this->success('DATA_CREATED');
	}

	public function updateData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

        $id = $this->request('id');
        $jadwalPengajarId = $this->request('jadwalPengajarId');
		$pengajarDiklatId = $this->request('pengajarDiklatId');
        $mataPelajaranId = $this->request('mataPelajaranId');
        $tempatId = $this->request('tempatId');
        $tanggal = $this->request('tanggal');
        $tanggalSelesai = $this->request('tanggalSelesai');
        
        $listPengajar = $this->request('listPengajar');
        $mMapel = $this->model('tb_mata_pelajaran');
        $rowMapel = $mMapel->find($mataPelajaranId);

        $jamMulai = $rowMapel['jamMulai'];
        $jamSelesai = $rowMapel['jamSelesai'];
        $jumlahJp = $rowMapel['jumlahJp'];
        $tipeWaktuId = $rowMapel['tipeWaktuId'];
        $tipeMataPelajaranId = $rowMapel['tipeMataPelajaranId'];

        $error = $this->checkRequest();
        
        $rs = $this->find($id);
        
        if(!$rs)
            return $this->failed('DATA_NOT_FOUND');

        $last_mataPelajaranId = $rs['mataPelajaranId'];

        if($mataPelajaranId != $last_mataPelajaranId) {
            $rs = $this->select("where jadwalPengajarId = ? and pengajarDiklatId = ? and tanggal = ? and mataPelajaranId = ? ", array($jadwalPengajarId,$pengajarDiklatId ,$tanggal ,$mataPelajaranId));

		if($rs)
            $error['pengajarDiklatId'] = $this->string('Pengajar Diklat sudah terdaftar');
        }
        
		if(count($error) > 0)
			return $this->invalid('PLEASE_CORRECT', $error);
		
		$rs = array(
            'jadwalPengajarId' => $jadwalPengajarId,
            'pengajarDiklatId' => $pengajarDiklatId,
            'mataPelajaranId' => $mataPelajaranId,
            'tempatId' => $tempatId,
            'tanggal' => $tanggal,
            'tanggalSelesai' => $tanggalSelesai,
            'jamMulai' => $jamMulai,
            'jamSelesai' => $jamSelesai,
            'jumlahJp' => $jumlahJp,
        );

        if($tipeWaktuId==2){
            $mRs = $this->model('tb_bahan_jadwal_pengajar_detail');
            $mRs->delete('bahanJadwalPengajarId', $id);

            foreach($listPengajar as $sub) {
                $mRs->insert(array(
                        'bahanJadwalPengajarId' => $id,
                        'jadwalPengajarId' => $jadwalPengajarId,
                        'pengajarDiklatId' => $sub['pengajarDiklatId'],
                ));
            }  
        }

		$this->update($id, $rs);
        
		return $this->success('DATA_UPDATED');
	}
	
	
	public function deleteData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

		$id = $this->request('id');
            
		$this->delete($id);
		return $this->success('DATA_DELETED');
	}

	public function multipleDeleteData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

		$multipleId = $this->request('multipleId');
        
		foreach($multipleId as $id){
        
            $this->delete($id);
        }
            

		return $this->success('DATA_DELETED');
	}
    
    
}

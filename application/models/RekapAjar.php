<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RekapAjar extends MY_Model {
    protected $view = 'vi_rekapitulasi_jp';
    
    public function readData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();

        // Order

        $orderBy = $this->request('orderBy');
        $reverse = $this->request('reverse');

        $orderStmt = '';

        if (!empty($orderBy)) {
            $orderStmt = "order by $orderBy";

            if (!empty($reverse)) {
                if ($reverse == 1)
                    $orderStmt .= ' desc';
            }
        }

        // Limit
        $page = $this->request('page');
		$count = $this->request('count');

        $limitStmt = '';

      	$rowsCount = $this->getRowsCount($orderStmt);
        
      	$pageCount = 1;

        if (!empty($page) && !empty($count)) {
            $row = ($page * $count) - $count;
            $limitStmt = "limit $row, $count";

            $pageCount = ceil($rowsCount / $count);

            if($pageCount < 1)
                $pageCount = 1;
        }

        // Query
        $mItems = $this->model('vi_bahan_jadwal_diklat_pengajar_detail');

        $pengajarDiklatId = $this->request('pengajarDiklatId');
        $jenisDiklatId = $this->request('jenisDiklatId');
        $tanggalMulai = $this->request('tanggalMulai');
        $tanggalSelesai = $this->request('tanggalSelesai');
        
        if (!empty($jenisDiklatId) && !empty($pengajarDiklatId))
			$rs = $mItems->query("select vi_bahan_jadwal_diklat_pengajar_detail.id,
			vi_bahan_jadwal_diklat_pengajar_detail.jenisDiklatId,
			vi_bahan_jadwal_diklat_pengajar_detail.tahunDiklat,
			vi_bahan_jadwal_diklat_pengajar_detail.publishStatusId,
			vi_bahan_jadwal_diklat_pengajar_detail.jenisDiklat,
			vi_bahan_jadwal_diklat_pengajar_detail.diklatId,
			vi_bahan_jadwal_diklat_pengajar_detail.tanggal,
			vi_bahan_jadwal_diklat_pengajar_detail.jumlahJp,
			vi_bahan_jadwal_diklat_pengajar_detail.tipeMataPelajaranId,
			vi_bahan_jadwal_diklat_pengajar_detail.tipeWaktuId,
			IFNULL(vi_bahan_jadwal_diklat_pengajar_detail.namaPengajar,'') AS namaPengajar,
			vi_bahan_jadwal_diklat_pengajar_detail.fotoPengajar,
			vi_bahan_jadwal_diklat_pengajar_detail.pengajarDiklatId,
			vi_bahan_jadwal_diklat_pengajar_detail.jenisPengajarId,
			IFNULL(sum(vi_bahan_jadwal_diklat_pengajar_detail.jumlahJp),0) as totalJp from vi_bahan_jadwal_diklat_pengajar_detail where tipeMataPelajaranId=1 AND tipeWaktuId=1 AND jenisPengajarId =7 and pengajarDiklatId = ? and jenisDiklatId = ? and tanggal between ? and ? group by pengajarDiklatId ".$orderStmt.' '.$limitStmt, array($pengajarDiklatId,$jenisDiklatId,$tanggalMulai,$tanggalSelesai));
        else if (!empty($jenisDiklatId) && empty($pengajarDiklatId))
        $rs = $mItems->query("select vi_bahan_jadwal_diklat_pengajar_detail.id,
        vi_bahan_jadwal_diklat_pengajar_detail.jenisDiklatId,
        vi_bahan_jadwal_diklat_pengajar_detail.tahunDiklat,
        vi_bahan_jadwal_diklat_pengajar_detail.publishStatusId,
        vi_bahan_jadwal_diklat_pengajar_detail.jenisDiklat,
        vi_bahan_jadwal_diklat_pengajar_detail.diklatId,
        vi_bahan_jadwal_diklat_pengajar_detail.tanggal,
        vi_bahan_jadwal_diklat_pengajar_detail.jumlahJp,
        vi_bahan_jadwal_diklat_pengajar_detail.tipeMataPelajaranId,
        vi_bahan_jadwal_diklat_pengajar_detail.tipeWaktuId,
        IFNULL(vi_bahan_jadwal_diklat_pengajar_detail.namaPengajar,'') AS namaPengajar,
        vi_bahan_jadwal_diklat_pengajar_detail.fotoPengajar,
        vi_bahan_jadwal_diklat_pengajar_detail.pengajarDiklatId,
        vi_bahan_jadwal_diklat_pengajar_detail.jenisPengajarId,
        IFNULL(sum(vi_bahan_jadwal_diklat_pengajar_detail.jumlahJp),0) as totalJp from vi_bahan_jadwal_diklat_pengajar_detail where tipeMataPelajaranId=1 AND tipeWaktuId=1 AND jenisPengajarId =7 and jenisDiklatId = ? and tanggal between ? and ?  group by pengajarDiklatId ".$orderStmt.' '.$limitStmt, array($jenisDiklatId,$tanggalMulai,$tanggalSelesai));
        else if (empty($jenisDiklatId) && !empty($pengajarDiklatId))
        $rs = $mItems->query("select vi_bahan_jadwal_diklat_pengajar_detail.id,
        vi_bahan_jadwal_diklat_pengajar_detail.jenisDiklatId,
        vi_bahan_jadwal_diklat_pengajar_detail.tahunDiklat,
        vi_bahan_jadwal_diklat_pengajar_detail.publishStatusId,
        vi_bahan_jadwal_diklat_pengajar_detail.jenisDiklat,
        vi_bahan_jadwal_diklat_pengajar_detail.diklatId,
        vi_bahan_jadwal_diklat_pengajar_detail.tanggal,
        vi_bahan_jadwal_diklat_pengajar_detail.jumlahJp,
        vi_bahan_jadwal_diklat_pengajar_detail.tipeMataPelajaranId,
        vi_bahan_jadwal_diklat_pengajar_detail.tipeWaktuId,
        IFNULL(vi_bahan_jadwal_diklat_pengajar_detail.namaPengajar,'') AS namaPengajar,
        vi_bahan_jadwal_diklat_pengajar_detail.fotoPengajar,
        vi_bahan_jadwal_diklat_pengajar_detail.pengajarDiklatId,
        vi_bahan_jadwal_diklat_pengajar_detail.jenisPengajarId,
        IFNULL(sum(vi_bahan_jadwal_diklat_pengajar_detail.jumlahJp),0) as totalJp from vi_bahan_jadwal_diklat_pengajar_detail where tipeMataPelajaranId=1 AND tipeWaktuId=1 AND jenisPengajarId =7 and pengajarDiklatId = ? and tanggal between ? and ?  group by pengajarDiklatId ".$orderStmt.' '.$limitStmt, array($pengajarDiklatId,$tanggalMulai,$tanggalSelesai));
        else
			$rs = $mItems->query("select vi_bahan_jadwal_diklat_pengajar_detail.id,
			vi_bahan_jadwal_diklat_pengajar_detail.jenisDiklatId,
			vi_bahan_jadwal_diklat_pengajar_detail.tahunDiklat,
			vi_bahan_jadwal_diklat_pengajar_detail.publishStatusId,
			vi_bahan_jadwal_diklat_pengajar_detail.jenisDiklat,
			vi_bahan_jadwal_diklat_pengajar_detail.diklatId,
			vi_bahan_jadwal_diklat_pengajar_detail.tanggal,
			vi_bahan_jadwal_diklat_pengajar_detail.jumlahJp,
			vi_bahan_jadwal_diklat_pengajar_detail.tipeMataPelajaranId,
			vi_bahan_jadwal_diklat_pengajar_detail.tipeWaktuId,
			IFNULL(vi_bahan_jadwal_diklat_pengajar_detail.namaPengajar,'') AS namaPengajar,
			vi_bahan_jadwal_diklat_pengajar_detail.fotoPengajar,
			vi_bahan_jadwal_diklat_pengajar_detail.pengajarDiklatId,
			vi_bahan_jadwal_diklat_pengajar_detail.jenisPengajarId,
			IFNULL(sum(vi_bahan_jadwal_diklat_pengajar_detail.jumlahJp),0) as totalJp from vi_bahan_jadwal_diklat_pengajar_detail where tipeMataPelajaranId=1 AND tipeWaktuId=1 AND jenisPengajarId =7 and (tanggal between ? and ? ) group by pengajarDiklatId ".$orderStmt.' '.$limitStmt, array($tanggalMulai,$tanggalSelesai));
	
        $extra = array(
            'rowsCount' => $rowsCount,
            'pageCount' => $pageCount,
        );

		return $this->success('DATA_READ', $rs, $extra);
    }

    
    public function createExcel($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();
       
        // Excel Settings
        
        $this->load->library('excel');

        $mItems = $this->model('vi_bahan_jadwal_diklat_pengajar_detail');

        $pengajarDiklatId = $this->request('pengajarDiklatId');
        $jenisDiklatId = $this->request('jenisDiklatId');
        $jenisDiklat = $this->request('jenisDiklat');
        $tanggalMulai = $this->request('tanggalMulai');
        $tanggalSelesai = $this->request('tanggalSelesai');
        
        if (!empty($jenisDiklatId) && !empty($pengajarDiklatId))
			$rs = $mItems->query("select vi_bahan_jadwal_diklat_pengajar_detail.id,
			vi_bahan_jadwal_diklat_pengajar_detail.jenisDiklatId,
			vi_bahan_jadwal_diklat_pengajar_detail.tahunDiklat,
			vi_bahan_jadwal_diklat_pengajar_detail.publishStatusId,
			vi_bahan_jadwal_diklat_pengajar_detail.jenisDiklat,
			vi_bahan_jadwal_diklat_pengajar_detail.diklatId,
			vi_bahan_jadwal_diklat_pengajar_detail.tanggal,
			vi_bahan_jadwal_diklat_pengajar_detail.jumlahJp,
			vi_bahan_jadwal_diklat_pengajar_detail.tipeMataPelajaranId,
			vi_bahan_jadwal_diklat_pengajar_detail.tipeWaktuId,
			IFNULL(vi_bahan_jadwal_diklat_pengajar_detail.namaPengajar,'') AS namaPengajar,
			vi_bahan_jadwal_diklat_pengajar_detail.fotoPengajar,
			vi_bahan_jadwal_diklat_pengajar_detail.pengajarDiklatId,
			vi_bahan_jadwal_diklat_pengajar_detail.jenisPengajarId,
			IFNULL(sum(vi_bahan_jadwal_diklat_pengajar_detail.jumlahJp),0) as totalJp from vi_bahan_jadwal_diklat_pengajar_detail where tipeMataPelajaranId=1 AND tipeWaktuId=1 AND jenisPengajarId =7 and pengajarDiklatId = ? and jenisDiklatId = ? and tanggal between ? and ? group by pengajarDiklatId order by namaPengajar asc", array($pengajarDiklatId,$jenisDiklatId,$tanggalMulai,$tanggalSelesai));
        else if (!empty($jenisDiklatId) && empty($pengajarDiklatId))
        $rs = $mItems->query("select vi_bahan_jadwal_diklat_pengajar_detail.id,
        vi_bahan_jadwal_diklat_pengajar_detail.jenisDiklatId,
        vi_bahan_jadwal_diklat_pengajar_detail.tahunDiklat,
        vi_bahan_jadwal_diklat_pengajar_detail.publishStatusId,
        vi_bahan_jadwal_diklat_pengajar_detail.jenisDiklat,
        vi_bahan_jadwal_diklat_pengajar_detail.diklatId,
        vi_bahan_jadwal_diklat_pengajar_detail.tanggal,
        vi_bahan_jadwal_diklat_pengajar_detail.jumlahJp,
        vi_bahan_jadwal_diklat_pengajar_detail.tipeMataPelajaranId,
        vi_bahan_jadwal_diklat_pengajar_detail.tipeWaktuId,
        IFNULL(vi_bahan_jadwal_diklat_pengajar_detail.namaPengajar,'') AS namaPengajar,
        vi_bahan_jadwal_diklat_pengajar_detail.fotoPengajar,
        vi_bahan_jadwal_diklat_pengajar_detail.pengajarDiklatId,
        vi_bahan_jadwal_diklat_pengajar_detail.jenisPengajarId,
        IFNULL(sum(vi_bahan_jadwal_diklat_pengajar_detail.jumlahJp),0) as totalJp from vi_bahan_jadwal_diklat_pengajar_detail where tipeMataPelajaranId=1 AND tipeWaktuId=1 AND jenisPengajarId =7 and jenisDiklatId = ? and tanggal between ? and ?  group by pengajarDiklatId order by namaPengajar asc ", array($jenisDiklatId,$tanggalMulai,$tanggalSelesai));
        else if (empty($jenisDiklatId) && !empty($pengajarDiklatId))
        $rs = $mItems->query("select vi_bahan_jadwal_diklat_pengajar_detail.id,
        vi_bahan_jadwal_diklat_pengajar_detail.jenisDiklatId,
        vi_bahan_jadwal_diklat_pengajar_detail.tahunDiklat,
        vi_bahan_jadwal_diklat_pengajar_detail.publishStatusId,
        vi_bahan_jadwal_diklat_pengajar_detail.jenisDiklat,
        vi_bahan_jadwal_diklat_pengajar_detail.diklatId,
        vi_bahan_jadwal_diklat_pengajar_detail.tanggal,
        vi_bahan_jadwal_diklat_pengajar_detail.jumlahJp,
        vi_bahan_jadwal_diklat_pengajar_detail.tipeMataPelajaranId,
        vi_bahan_jadwal_diklat_pengajar_detail.tipeWaktuId,
        IFNULL(vi_bahan_jadwal_diklat_pengajar_detail.namaPengajar,'') AS namaPengajar,
        vi_bahan_jadwal_diklat_pengajar_detail.fotoPengajar,
        vi_bahan_jadwal_diklat_pengajar_detail.pengajarDiklatId,
        vi_bahan_jadwal_diklat_pengajar_detail.jenisPengajarId,
        IFNULL(sum(vi_bahan_jadwal_diklat_pengajar_detail.jumlahJp),0) as totalJp from vi_bahan_jadwal_diklat_pengajar_detail where tipeMataPelajaranId=1 AND tipeWaktuId=1 AND jenisPengajarId =7 and pengajarDiklatId = ? and tanggal between ? and ?  group by pengajarDiklatId order by namaPengajar asc ", array($pengajarDiklatId,$tanggalMulai,$tanggalSelesai));
        else
			$rs = $mItems->query("select vi_bahan_jadwal_diklat_pengajar_detail.id,
			vi_bahan_jadwal_diklat_pengajar_detail.jenisDiklatId,
			vi_bahan_jadwal_diklat_pengajar_detail.tahunDiklat,
			vi_bahan_jadwal_diklat_pengajar_detail.publishStatusId,
			vi_bahan_jadwal_diklat_pengajar_detail.jenisDiklat,
			vi_bahan_jadwal_diklat_pengajar_detail.diklatId,
			vi_bahan_jadwal_diklat_pengajar_detail.tanggal,
			vi_bahan_jadwal_diklat_pengajar_detail.jumlahJp,
			vi_bahan_jadwal_diklat_pengajar_detail.tipeMataPelajaranId,
			vi_bahan_jadwal_diklat_pengajar_detail.tipeWaktuId,
			IFNULL(vi_bahan_jadwal_diklat_pengajar_detail.namaPengajar,'') AS namaPengajar,
			vi_bahan_jadwal_diklat_pengajar_detail.fotoPengajar,
			vi_bahan_jadwal_diklat_pengajar_detail.pengajarDiklatId,
			vi_bahan_jadwal_diklat_pengajar_detail.jenisPengajarId,
			IFNULL(sum(vi_bahan_jadwal_diklat_pengajar_detail.jumlahJp),0) as totalJp from vi_bahan_jadwal_diklat_pengajar_detail where tipeMataPelajaranId=1 AND tipeWaktuId=1 AND jenisPengajarId =7 and (tanggal between ? and ? ) group by pengajarDiklatId order by namaPengajar asc ", array($tanggalMulai,$tanggalSelesai));
           
            $mUser = $this->model('vi_user');
            $rowPengajar = $mUser->find($pengajarDiklatId);
        // Output
        
        if (count($rs) > 0) {
            if(empty($pengajarDiklatId)){
                $render = 'renderDetail';
                $nameFile='RekapAjar';
                $rowPengajar='';
            }else{
                $mUser = $this->model('vi_user');
                $rowPengajar = $mUser->find($pengajarDiklatId);
                $render = 'renderDetailWi';
                $nameFile='RekapAjarWi';
            }
            
            $fileUrl = $this->$render(
                $nameFile,
                array(
                    'rs' => $rs,
                    'jenisDiklat' => $jenisDiklat,
                    'tanggalMulai' => $tanggalMulai,
                    'tanggalSelesai' => $tanggalSelesai,
                    'rowPengajar' => $rowPengajar,
                    )
            );

            return $this->success('FILE_CREATED', $fileUrl);
        }
        else
            return $this->failed('FILE_CREATE_FAILED');
    }

    private function renderDetail($template, $data) {

        $rs = $data['rs'];
        $tanggalMulai = $data['tanggalMulai'];
        $tanggalSelesai = $data['tanggalSelesai'];
        $jenisDiklat = $data['jenisDiklat'];

        $reader = PHPExcel_IOFactory::createReader('Excel5');
        $templateUrl = "application/views/excel/$template.xls";

        $excel = $reader->load($templateUrl);
        $sheet = $excel->setActiveSheetIndex(0);

        $i = 0;
        $x =4;
        
        $sheet->setCellValue("A2", 'Rekap Ajar dari Tanggal: '.$this->indonesian_date($tanggalMulai ,"l, j F Y").' s/d '.$this->indonesian_date($tanggalSelesai ,"l, j F Y"));
       if($jenisDiklat)
       $sheet->setCellValue("A3", 'Jenis Diklat: '.$jenisDiklat);
        $styleArray = array(
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				)
			)
        );
        
        foreach($rs as $row) {
            $x++;
            $i++;
            $sheet->setCellValue("A$x", $i);
            $sheet->setCellValue("B$x", $row['namaPengajar']);
            $sheet->setCellValue("C$x", $row['totalJp']);
			$sheet->getStyle("A$x")->applyFromArray($styleArray);
			$sheet->getStyle("B$x")->applyFromArray($styleArray);
			$sheet->getStyle("C$x")->applyFromArray($styleArray);
			
        }

        $fileName = $this->encrypt().'.xls';
        $filePath = "asset/tmp/$fileName";

        $writer = PHPExcel_IOFactory::createWriter($excel, 'Excel5');
        $writer->save($filePath);

        $fileUrl = base_url().$filePath;

        return $fileUrl;
    }

    private function renderDetailWi($template, $data) {

        $rs = $data['rs'];
        $tanggalMulai = $data['tanggalMulai'];
        $tanggalSelesai = $data['tanggalSelesai'];
        $jenisDiklat = $data['jenisDiklat'];
        $rowPengajar = $data['rowPengajar'];

        $reader = PHPExcel_IOFactory::createReader('Excel5');
        $templateUrl = "application/views/excel/$template.xls";

        $excel = $reader->load($templateUrl);
        $sheet = $excel->setActiveSheetIndex(0);

        $i = 0;
        $x =4;
        
        $sheet->setCellValue("A2", 'Rekap Ajar dari Tanggal: '.$this->indonesian_date($tanggalMulai ,"l, j F Y").' s/d '.$this->indonesian_date($tanggalSelesai ,"l, j F Y"));
       if($jenisDiklat){
           $sheet->setCellValue("A3", 'Jenis Diklat: '.$jenisDiklat);

       }
       
       $sheet->setCellValue("B5", ': '.$rowPengajar['name']);
       $sheet->setCellValue("B6", ': '.$rs[0]['totalJp']);
        

        $fileName = $this->encrypt().'.xls';
        $filePath = "asset/tmp/$fileName";

        $writer = PHPExcel_IOFactory::createWriter($excel, 'Excel5');
        $writer->save($filePath);

        $fileUrl = base_url().$filePath;

        return $fileUrl;
    }

    
}
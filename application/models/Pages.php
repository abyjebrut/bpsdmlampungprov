<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends MY_Model {
    protected $table = 'tb_pages';
    protected $view = 'vi_pages';
    
    public function readData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();

        // Order

        $orderBy = $this->request('orderBy');
        $reverse = $this->request('reverse');

        $orderStmt = '';

        if (!empty($orderBy)) {
            $orderStmt = "order by $orderBy";

            if (!empty($reverse)) {
                if ($reverse == 1)
                    $orderStmt .= ' desc';
            }
        }

        // Limit

        $page = $this->request('page');
		$count = $this->request('count');
		$publishStatusId = $this->request('publishStatusId');
		
        $limitStmt = '';

        if (!empty($publishStatusId))
	      $rowsCount = $this->getRowsCount("where publishStatusId = ? ", array($publishStatusId));
	    else
	      $rowsCount = $this->getRowsCount($orderStmt);
	    
        $pageCount = 1;

        if (!empty($page) && !empty($count)) {
            $row = ($page * $count) - $count;
            $limitStmt = "limit $row, $count";

            $pageCount = ceil($rowsCount / $count);

            if($pageCount < 1)
                $pageCount = 1;
        }

        // Query

		if (!empty($publishStatusId))
	      $rs = $this->select("where publishStatusId = ? ".$orderStmt.' '.$limitStmt, array($publishStatusId));
	    else
	      $rs = $this->select($orderStmt.' '.$limitStmt);
	    
        $extra = array(
            'rowsCount' => $rowsCount,
            'pageCount' => $pageCount,
        );

		return $this->success('DATA_READ', $rs, $extra);
	}

    public function filterData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();
        
        // Query

        $field = $this->request('field');
        $keyword = $this->request('keyword');

        $queryStmt = "where $field like ?";
        $rowsCount = $this->getRowsCount($queryStmt, array("$keyword%"));

        // Limit

        $page = $this->request('page');
		$count = $this->request('count');

        $limitStmt = '';
        $pageCount = 1;

        if (!empty($page) && !empty($count)) {
            $row = ($page * $count) - $count;
            $limitStmt = "limit $row, $count";

            $pageCount = ceil($rowsCount / $count);

            if($pageCount < 1)
                $pageCount = 1;
        }

        $rs = $this->select($queryStmt, array("$keyword%"));

        $extra = array(
            'rowsCount' => $rowsCount,
            'pageCount' => $pageCount,
        );

		return $this->success('DATA_READ', $rs, $extra);
	}

	public function detailData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();

		$id = $this->request('id');
        
        // Query
        
		$rs = $this->find($id);

        if($rs)
            return $this->success('DATA_READ', $rs);
        else
            return $this->failed('DATA_NOT_FOUND');
	}
	

    private function checkRequest() {
        $title = $this->request('title');
        $publishStatusId = $this->request('publishStatusId');

		$error = array();

		if(empty($publishStatusId))
			$error['publishStatusId'] = $this->string('PUBLISH_STATUS_REQUIRED');
		
		if(empty($title))
			$error['title'] = $this->string('TITLE_REQUIRED');

        return $error;
    }

	public function createData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

		$error = $this->checkRequest();

		$publishStatusId = $this->request('publishStatusId');
        $title = $this->request('title');
        $title_sl = $this->request('titleSl');
        $title_el = $this->request('titleEl');
        $mediaTypeId = $this->request('mediaTypeId');
        if (empty($mediaTypeId))
        	$mediaTypeId=3;
        $contentLayoutId = $this->request('contentLayoutId');
        $image = $this->request('image');
        $videoEmbed = $this->request('videoEmbed');
        $description = $this->request('description');
        $description_sl = $this->request('descriptionSl');
	    $description_el = $this->request('descriptionEl');
	    $moreDescription = $this->request('moreDescription');
	    $moreDescription_sl = $this->request('moreDescriptionSl');
	    $moreDescription_el = $this->request('moreDescriptionEl');
        
        $rs = $this->find('title', $title);

		if($rs)
            $error['title'] = $this->string('TITLE_IS_REG');

		if(count($error) > 0)
			return $this->invalid('PLEASE_CORRECT', $error);
		
		if(!empty($image)) {
			if(!$this->moveToArchive($image))
				return $this->failed('FILE_UPLOAD_FAILED');
		}
			
		$rs = array(
            'title' => $title,
			'title_sl' => $title_sl,
			'title_el' => $title_el,
			'mediaTypeId' => $mediaTypeId,
			'contentLayoutId' => $contentLayoutId,
			'image' => $image,
			'videoEmbed' => $videoEmbed,
			'description' => $description,
			'description_sl' => $description_sl,
    	  	'description_el' => $description_el,
	      	'moreDescription' => $moreDescription,
    	  	'moreDescription_sl' => $moreDescription_sl,
    	  	'moreDescription_el' => $moreDescription_el,
			'publishStatusId' => $publishStatusId,
        );

		$this->insert($rs);
		
		// position
		$lastId = $this->getLastInsertId();
		$this->update($lastId, array('position' => $lastId));
		
		return $this->success('DATA_CREATED');
	}

	public function updateData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

        $id = $this->request('id');
        $publishStatusId = $this->request('publishStatusId');
        $title = $this->request('title');
        $title_sl = $this->request('titleSl');
        $title_el = $this->request('titleEl');
        $mediaTypeId = $this->request('mediaTypeId');
        $contentLayoutId = $this->request('contentLayoutId');
        $image = $this->request('image');
        $videoEmbed = $this->request('videoEmbed');
        $description = $this->request('description');
        $description_sl = $this->request('descriptionSl');
	    $description_el = $this->request('descriptionEl');
	    $moreDescription = $this->request('moreDescription');
	    $moreDescription_sl = $this->request('moreDescriptionSl');
	    $moreDescription_el = $this->request('moreDescriptionEl');
        
        $error = $this->checkRequest();

        $rs = $this->find($id);
        
        if(!empty($image)) {
        	if($image != $rs['image']) {
        
        		$last_image = $rs['image'];
        		$this->deleteArchive($last_image);
        
        		if(!$this->moveToArchive($image))
        			return $this->failed('FILE_UPLOAD_FAILED');
        	}
        }
        
        if(!$rs)
            return $this->failed('DATA_NOT_FOUND');

        $last_title = $rs['title'];

        if($title != $last_title) {
            $rs = $this->find('title', $title);

            if($rs)
                $error['title'] = $this->string('TITLE_IS_REG');
        }

		if(count($error) > 0)
			return $this->invalid('PLEASE_CORRECT', $error);

		$rs = array(
			'title' => $title,
			'title_sl' => $title_sl,
			'title_el' => $title_el,
			'mediaTypeId' => $mediaTypeId,
			'contentLayoutId' => $contentLayoutId,
			'image' => $image,
			'videoEmbed' => $videoEmbed,
			'description' => $description,
			'description_sl' => $description_sl,
    	  	'description_el' => $description_el,
	      	'moreDescription' => $moreDescription,
    	  	'moreDescription_sl' => $moreDescription_sl,
    	  	'moreDescription_el' => $moreDescription_el,
			'publishStatusId' => $publishStatusId,
		);

		$this->update($id, $rs);
		return $this->success('DATA_UPDATED');
	}

	public function moveData($params = array())
	{
		if(!$this->setSettings(array(
				'authen' => 'user',
				'requestSource' => $params,
		)))
			return $this->denied();
	
		$idFrom = $this->request('idFrom');
		$idTo = $this->request('idTo');
	
		$rsFrom = $this->find($idFrom);
		$rsTo = $this->find($idTo);
	
		$positionFrom = $rsFrom['position'];
		$positionTo = $rsTo['position'];
	
		$this->update($idFrom, array('position' => $positionTo));
		$this->update($idTo, array('position' => $positionFrom));
	
		return $this->success('DATA_UPDATED');
	}
	
	public function deleteData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

		$id = $this->request('id');
        $rs = $this->find($id);

		$this->delete($id);
		return $this->success('DATA_DELETED');
	}

	public function multipleDeleteData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

		$multipleId = $this->request('multipleId');

		foreach($multipleId as $id){
			
			$rs = $this->find($id);
			
			if(!empty($rs['image']))
				$this->deleteArchive($rs['image']);
			
			$this->delete($id);
		}
            

		return $this->success('DATA_DELETED');
	}
    
}
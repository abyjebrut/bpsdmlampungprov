<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ContactProfile extends MY_Model {
    protected $table = 'tb_contact_profile';
    
    public function detailData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();
        
		$id = 1;
        
        // Query
        
		$rs = $this->find($id);
		
		$mAddress = $this->model('tb_contact_address');
		$rs['address'] = $mAddress->select('where contactProfileId = ? order by id', array($id));
		
		$mPhoneNumbers = $this->model('tb_contact_phone_numbers');
		$rs['phoneNumbers'] = $mPhoneNumbers->select('where contactProfileId = ? order by id', array($id));
		
		$mMobileNumbers = $this->model('tb_contact_mobile_numbers');
		$rs['mobileNumbers'] = $mMobileNumbers->select('where contactProfileId = ? order by id', array($id));
		
		$mEmails = $this->model('tb_contact_emails');
		$rs['emails'] = $mEmails->select('where contactProfileId = ? order by id', array($id));

        if($rs)
            return $this->success('DATA_READ', $rs);
        else
            return $this->failed('DATA_NOT_FOUND');
	}

    private function checkRequest() {
        

		$error = array();


        return $error;
    }

	public function saveData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();
        
		$error = $this->checkRequest();

        if(count($error) > 0)
			return $this->invalid('PLEASE_CORRECT', $error);

        $id = 1;
        $companyName = $this->request('companyName');
        $companyName_sl = $this->request('companyNameSl');
        $companyName_el = $this->request('companyNameEl');
        $latitude = $this->request('latitude');
        $longitude = $this->request('longitude');
        $address = $this->request('address');
        $phoneNumbers = $this->request('phoneNumbers');
        $mobileNumbers = $this->request('mobileNumbers');
        $emails = $this->request('emails');

		$rs = array(
			'companyName' => $companyName,
			'companyName_sl' => $companyName_sl,
			'companyName_el' => $companyName_el,
			'latitude' => $latitude,
			'longitude' => $longitude,
		);

        $rsFound = $this->find($id);

        if($rsFound) {

            $this->update($id, $rs);
            
            $mAddress = $this->model('tb_contact_address');
            $mAddress->delete('contactProfileId', $id);
            
            if ($address){
	            $mAddress = $this->model('tb_contact_address');
	            foreach($address as $row) {
	            	$mAddress->insert(array(
	            			'contactProfileId' => $id,
	            			'caption' => $row['caption'],
	            			'caption_sl' => $row['caption_sl'],
	            			'caption_el' => $row['caption_el'],
	            			'address' => $row['address'],
	            	));
	            }
            }
            
            $mPhoneNumber = $this->model('tb_contact_phone_numbers');
            $mPhoneNumber->delete('contactProfileId', $id);
            
            if ($phoneNumbers){
            	$mPhoneNumber = $this->model('tb_contact_phone_numbers');
            	foreach($phoneNumbers as $row) {
            		$mPhoneNumber->insert(array(
            				'contactProfileId' => $id,
            				'caption' => $row['caption'],
            				'caption_sl' => $row['caption_sl'],
            				'caption_el' => $row['caption_el'],
            				'phoneNumber' => $row['phoneNumber'],
            		));
            	}
            }

            $mMobileNumber = $this->model('tb_contact_mobile_numbers');
            $mMobileNumber->delete('contactProfileId', $id);
            
        	if ($mobileNumbers){
            	$mMobileNumber = $this->model('tb_contact_mobile_numbers');
            	foreach($mobileNumbers as $row) {
            		$mMobileNumber->insert(array(
            				'contactProfileId' => $id,
            				'caption' => $row['caption'],
            				'caption_sl' => $row['caption_sl'],
            				'caption_el' => $row['caption_el'],
            				'mobileNumber' => $row['mobileNumber'],
            		));
            	}
            }

            $mEmails = $this->model('tb_contact_emails');
            $mEmails->delete('contactProfileId', $id);
            
            if ($emails){
            	$mEmails = $this->model('tb_contact_emails');
            	foreach($emails as $row) {
            		$mEmails->insert(array(
            				'contactProfileId' => $id,
            				'caption' => $row['caption'],
            				'caption_sl' => $row['caption_sl'],
            				'caption_el' => $row['caption_el'],
            				'email' => $row['email'],
            		));
            	}
            }
            
            return $this->success('DATA_UPDATED');
        }
        else {

            $rs['id'] = $id;

            $this->insert($rs);
            
            if ($address){
            	$mAddress = $this->model('tb_contact_address');
            	 
            	foreach($address as $row) {
            		$mAddress->insert(array(
            				'contactProfileId' => $id,
            				'caption' => $row['caption'],
            				'caption_sl' => $row['caption_sl'],
            				'caption_el' => $row['caption_el'],
            				'address' => $row['address'],
            		));
            	}
            }
            
            if ($phoneNumbers){
            	$mPhoneNumber = $this->model('tb_contact_phone_numbers');
            
            	foreach($phoneNumbers as $row) {
            		$mPhoneNumber->insert(array(
            				'contactProfileId' => $id,
            				'caption' => $row['caption'],
            				'caption_sl' => $row['caption_sl'],
            				'caption_el' => $row['caption_el'],
            				'phoneNumber' => $row['phoneNumber'],
            		));
            	}
            }
            
            if ($mobileNumbers){
            	$mMobileNumber = $this->model('tb_contact_mobile_numbers');
            
            	foreach($mobileNumbers as $row) {
            		$mMobileNumber->insert(array(
            				'contactProfileId' => $id,
            				'caption' => $row['caption'],
            				'caption_sl' => $row['caption_sl'],
            				'caption_el' => $row['caption_el'],
            				'mobileNumber' => $row['mobileNumber'],
            		));
            	}
            }
            
            if ($emails){
	        	$mEmails = $this->model('tb_contact_emails');
	             
            	foreach($emails as $row) {
            		$mEmails->insert(array(
            				'contactProfileId' => $id,
            				'caption' => $row['caption'],
            				'caption_sl' => $row['caption_sl'],
            				'caption_el' => $row['caption_el'],
            				'email' => $row['email'],
            		));
            	}
            }
            
            return $this->success('DATA_CREATED');
        }

	}

}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UnitKerja extends MY_Model {
    protected $table = 'tb_unit_kerja';
    
    public function readData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();

        // Order

        $orderBy = $this->request('orderBy');
        $reverse = $this->request('reverse');

        $orderStmt = '';

        if (!empty($orderBy)) {
            $orderStmt = "order by $orderBy";

            if (!empty($reverse)) {
                if ($reverse == 1)
                    $orderStmt .= ' desc';
            }
        }

        // Limit
        $page = $this->request('page');
		$count = $this->request('count');

        $limitStmt = '';

      	$rowsCount = $this->getRowsCount($orderStmt);
        
      	$pageCount = 1;

        if (!empty($page) && !empty($count)) {
            $row = ($page * $count) - $count;
            $limitStmt = "limit $row, $count";

            $pageCount = ceil($rowsCount / $count);

            if($pageCount < 1)
                $pageCount = 1;
        }

        // Query

	      $rs = $this->select($orderStmt.' '.$limitStmt);
    
        $extra = array(
            'rowsCount' => $rowsCount,
            'pageCount' => $pageCount,
        );

		return $this->success('DATA_READ', $rs, $extra);
	}

    public function filterData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();
        
        // Query

        $field = $this->request('field');
        $keyword = $this->request('keyword');

        $queryStmt = "where $field like ?";
        $rowsCount = $this->getRowsCount($queryStmt, array("$keyword%"));

        // Limit

        $page = $this->request('page');
		$count = $this->request('count');

        $limitStmt = '';
        $pageCount = 1;

        if (!empty($page) && !empty($count)) {
            $row = ($page * $count) - $count;
            $limitStmt = "limit $row, $count";

            $pageCount = ceil($rowsCount / $count);

            if($pageCount < 1)
                $pageCount = 1;
        }

        $rs = $this->select($queryStmt, array("$keyword%"));

        $extra = array(
            'rowsCount' => $rowsCount,
            'pageCount' => $pageCount,
        );

		return $this->success('DATA_READ', $rs, $extra);
	}

	public function detailData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();

		$id = $this->request('id');
        
        // Query
        
		$rs = $this->find($id);

        if($rs)
            return $this->success('DATA_READ', $rs);
        else
            return $this->failed('DATA_NOT_FOUND');
	}

    private function checkRequest() {
        $unitKerja = $this->request('unitKerja');

		$error = array();
		
		if(empty($unitKerja))
			$error['unitKerja'] = $this->string('UNIT_KERJA_REQUIRED');

        return $error;
    }

	public function createData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

		$error = $this->checkRequest();

		$unitKerja = $this->request('unitKerja');

        $rs = $this->find('unitKerja', $unitKerja);

		if($rs)
            $error['unitKerja'] = $this->string('UNIT_KERJA_IS_REG');

		if(count($error) > 0)
			return $this->invalid('PLEASE_CORRECT', $error);

		$rs = array(
            'unitKerja' => $unitKerja,
        );

		$this->insert($rs);
		return $this->success('DATA_CREATED');
	}

	public function updateData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

        $id = $this->request('id');
        $unitKerja = $this->request('unitKerja');
		
        $error = $this->checkRequest();

        $rs = $this->find($id);
        
        if(!$rs)
            return $this->failed('DATA_NOT_FOUND');

        $last_unitKerja = $rs['unitKerja'];

        if($unitKerja != $last_unitKerja) {
            $rs = $this->find('unitKerja', $unitKerja);

            if($rs)
                $error['unitKerja'] = $this->string('UNIT_KERJA_IS_REG');
        }

		if(count($error) > 0)
			return $this->invalid('PLEASE_CORRECT', $error);
		
		$rs = array(
				'unitKerja' => $unitKerja,
		);

		$this->update($id, $rs);
		return $this->success('DATA_UPDATED');
	}
	
	public function deleteData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

		$id = $this->request('id');
        $rs = $this->find($id);

		$this->delete($id);
		return $this->success('DATA_DELETED');
	}

	public function multipleDeleteData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

            $multipleId = $this->request('multipleId');
            $mItems1 = $this->model('tb_akun');
            $error = array();
            $i = 0;

        foreach($multipleId as $id){
            $rs = $this->find($id);
            $i++;
            $rsItems1 = $mItems1->select("where unitKerjaId = ?", array($id));
            if($rsItems1){
                if(count($error) == 0)
                $error['0'] = 'Data dibawah ini tidak bisa di hapus karena terkait dengan Data lain';
                $rs = $this->find($id);
                $error[$i] = $rs['unitKerja'];
            }else{
                $this->delete($id);
            }
        }

        if(count($error) > 0){
        return $this->failed('PLEASE_CORRECT', $error);
        exit;
        }else{
        return $this->success('DATA_DELETED');
        }
	}
    
    
}
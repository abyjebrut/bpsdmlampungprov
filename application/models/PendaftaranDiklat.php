<?php
defined('BASEPATH') or exit('No direct script access allowed');

class PendaftaranDiklat extends MY_Model
{
	protected $table = 'tb_pendaftaran_diklat';
	protected $view = 'vi_pendaftaran_diklat';

	public function readData($params = array())
	{
		if (!$this->setSettings(array(
			'authen' => 'free',
			'requestSource' => $params,
		))) {
			return $this->denied();
		}

		// Order

		$orderBy = $this->request('orderBy');
		$reverse = $this->request('reverse');

		$orderStmt = '';

		if (!empty($orderBy)) {
			$orderStmt = "order by $orderBy";

			if (!empty($reverse)) {
				if ($reverse == 1) {
					$orderStmt .= ' desc';
				}
			}
		}

		// Limit
		$page = $this->request('page');
		$count = $this->request('count');
		$jenisDiklatId = $this->request('jenisDiklatId');
		$namaDiklat = $this->request('namaDiklat');
		$diklatId = $this->request('diklatId');
		$status = 'Approve';

		$limitStmt = '';

		if (!empty($jenisDiklatId) && !empty($diklatId)) {
			$rowsCount = $this->getRowsCount("where status = ? and jenisDiklatId = ? and diklatId = ? ", array($status, $jenisDiklatId, $diklatId));
		} elseif (!empty($jenisDiklatId)) {
			$rowsCount = $this->getRowsCount("where status = ? and jenisDiklatId = ? ", array($status, $jenisDiklatId));
		} elseif (!empty($namaDiklat)) {
			$rowsCount = $this->getRowsCount("where status = ? and namaDiklat = ? ", array($status, $namaDiklat));
		} elseif (!empty($diklatId)) {
			$rowsCount = $this->getRowsCount("where status = ? and diklatId = ? ", array($status, $diklatId));
		} else {
			$rowsCount = $this->getRowsCount("where status = ? ", array($status));
		}

		$pageCount = 1;

		if (!empty($page) && !empty($count)) {
			$row = ($page * $count) - $count;
			$limitStmt = "limit $row, $count";

			$pageCount = ceil($rowsCount / $count);

			if ($pageCount < 1) {
				$pageCount = 1;
			}
		}

		// Query

		if (!empty($jenisDiklatId) && !empty($diklatId)) {
			$rs = $this->select("where status = ? and jenisDiklatId = ? and diklatId = ? " . $orderStmt . ' ' . $limitStmt, array($status, $jenisDiklatId, $diklatId));
		} elseif (!empty($jenisDiklatId)) {
			$rs = $this->select("where status = ? and jenisDiklatId = ? " . $orderStmt . ' ' . $limitStmt, array($status, $jenisDiklatId));
		} elseif (!empty($namaDiklat)) {
			$rs = $this->select("where status = ? and namaDiklat = ? " . $orderStmt . ' ' . $limitStmt, array($status, $namaDiklat));
		} elseif (!empty($diklatId)) {
			$rs = $this->select("where status = ? and diklatId = ? " . $orderStmt . ' ' . $limitStmt, array($status, $diklatId));
		} else {
			$rs = $this->select("where status = ? " . $orderStmt . ' ' . $limitStmt, array($status));
		}

		$extra = array(
			'rowsCount' => $rowsCount,
			'pageCount' => $pageCount,
		);

		return $this->success('DATA_READ', $rs, $extra);
	}

	public function readDataPending($params = array())
	{
		if (!$this->setSettings(array(
			'authen' => 'free',
			'requestSource' => $params,
		)))
			return $this->denied();

		// Order

		$orderBy = $this->request('orderBy');
		$reverse = $this->request('reverse');

		$orderStmt = '';

		if (!empty($orderBy)) {
			$orderStmt = "order by $orderBy";

			if (!empty($reverse)) {
				if ($reverse == 1)
					$orderStmt .= ' desc';
			}
		}

		// Limit
		$page = $this->request('page');
		$count = $this->request('count');
		$instansiId = $this->request('instansiId');
		$namaDiklat = $this->request('namaDiklat');
		$diklatId = $this->request('diklatId');
		$status = 'Pending';

		$limitStmt = '';

		if (!empty($instansiId) && !empty($diklatId))
			$rowsCount = $this->getRowsCount("where status = ? and instansiId = ? and diklatId = ? ", array($status, $instansiId, $diklatId));
		elseif (!empty($instansiId))
			$rowsCount = $this->getRowsCount("where status = ? and instansiId = ? ", array($status, $instansiId));
		elseif (!empty($namaDiklat))
			$rowsCount = $this->getRowsCount("where status = ? and namaDiklat = ? ", array($status, $namaDiklat));
		elseif (!empty($diklatId))
			$rowsCount = $this->getRowsCount("where status = ? and diklatId = ? ", array($status, $diklatId));
		else
			$rowsCount = $this->getRowsCount("where status = ? ", array($status));

		$pageCount = 1;

		if (!empty($page) && !empty($count)) {
			$row = ($page * $count) - $count;
			$limitStmt = "limit $row, $count";

			$pageCount = ceil($rowsCount / $count);

			if ($pageCount < 1)
				$pageCount = 1;
		}

		// Query

		if (!empty($instansiId) && !empty($diklatId))
			$rs = $this->select("where status = ? and instansiId = ? and diklatId = ? " . $orderStmt . ' ' . $limitStmt, array($status, $instansiId, $diklatId));
		elseif (!empty($instansiId))
			$rs = $this->select("where status = ? and instansiId = ? " . $orderStmt . ' ' . $limitStmt, array($status, $instansiId));
		elseif (!empty($namaDiklat))
			$rs = $this->select("where status = ? and namaDiklat = ? " . $orderStmt . ' ' . $limitStmt, array($status, $namaDiklat));
		elseif (!empty($diklatId))
			$rs = $this->select("where status = ? and diklatId = ? " . $orderStmt . ' ' . $limitStmt, array($status, $diklatId));
		else
			$rs = $this->select("where status = ? " . $orderStmt . ' ' . $limitStmt, array($status));

		$extra = array(
			'rowsCount' => $rowsCount,
			'pageCount' => $pageCount,
		);

		return $this->success('DATA_READ', $rs, $extra);
	}

	public function readDataReject($params = array())
	{
		if (!$this->setSettings(array(
			'authen' => 'free',
			'requestSource' => $params,
		)))
			return $this->denied();

		// Order

		$orderBy = $this->request('orderBy');
		$reverse = $this->request('reverse');

		$orderStmt = '';

		if (!empty($orderBy)) {
			$orderStmt = "order by $orderBy";

			if (!empty($reverse)) {
				if ($reverse == 1)
					$orderStmt .= ' desc';
			}
		}

		// Limit
		$page = $this->request('page');
		$count = $this->request('count');
		$instansiId = $this->request('instansiId');
		$namaDiklat = $this->request('namaDiklat');
		$diklatId = $this->request('diklatId');
		$status = 'Reject';

		$limitStmt = '';

		if (!empty($instansiId) && !empty($namaDiklat))
			$rowsCount = $this->getRowsCount("where status = ? and instansiId = ? and namaDiklat = ? ", array($status, $instansiId, $namaDiklat));
		elseif (!empty($instansiId))
			$rowsCount = $this->getRowsCount("where status = ? and instansiId = ? ", array($status, $instansiId));
		elseif (!empty($namaDiklat))
			$rowsCount = $this->getRowsCount("where status = ? and namaDiklat = ? ", array($status, $namaDiklat));
		elseif (!empty($diklatId))
			$rowsCount = $this->getRowsCount("where status = ? and diklatId = ? ", array($status, $diklatId));
		else
			$rowsCount = $this->getRowsCount("where status = ? ", array($status));

		$pageCount = 1;

		if (!empty($page) && !empty($count)) {
			$row = ($page * $count) - $count;
			$limitStmt = "limit $row, $count";

			$pageCount = ceil($rowsCount / $count);

			if ($pageCount < 1)
				$pageCount = 1;
		}

		// Query

		if (!empty($instansiId) && !empty($namaDiklat))
			$rs = $this->select("where status = ? and instansiId = ? and namaDiklat = ? " . $orderStmt . ' ' . $limitStmt, array($status, $instansiId, $namaDiklat));
		elseif (!empty($instansiId))
			$rs = $this->select("where status = ? and instansiId = ? " . $orderStmt . ' ' . $limitStmt, array($status, $instansiId));
		elseif (!empty($namaDiklat))
			$rs = $this->select("where status = ? and namaDiklat = ? " . $orderStmt . ' ' . $limitStmt, array($status, $namaDiklat));
		elseif (!empty($diklatId))
			$rs = $this->select("where status = ? and diklatId = ? " . $orderStmt . ' ' . $limitStmt, array($status, $diklatId));
		else
			$rs = $this->select("where status = ? " . $orderStmt . ' ' . $limitStmt, array($status));

		$extra = array(
			'rowsCount' => $rowsCount,
			'pageCount' => $pageCount,
		);

		return $this->success('DATA_READ', $rs, $extra);
	}

	public function readDataChartAllAnggota($params = array())
	{
		if (!$this->setSettings(array(
			'authen' => 'free',
			'requestSource' => $params,
		)))
			return $this->denied();
		$status = 'Approve';
		$rs = $this->select("where status = ? ", array($status));

		return $this->success('DATA_READ', $rs);
	}

	public function readDataChartJenisKelaminPria($params = array())
	{
		if (!$this->setSettings(array(
			'authen' => 'free',
			'requestSource' => $params,
		)))
			return $this->denied();
		$status = 'Approve';
		$rs = $this->select("where jenisKelaminId = 1 and status = ? ", array($status));

		return $this->success('DATA_READ', $rs);
	}

	public function readDataChartJenisKelaminWanita($params = array())
	{
		if (!$this->setSettings(array(
			'authen' => 'free',
			'requestSource' => $params,
		)))
			return $this->denied();
		$status = 'Approve';
		$rs = $this->select("where jenisKelaminId = 2 and status = ? ", array($status));


		return $this->success('DATA_READ', $rs);
	}

	public function readDataChartLatsar($params = array())
	{
		if (!$this->setSettings(array(
			'authen' => 'free',
			'requestSource' => $params,
		)))
			return $this->denied();
		$status = 'Approve';
		$rs = $this->select("where jenisDiklatId = 6 and status = ? ", array($status));

		return $this->success('DATA_READ', $rs);
	}

	public function readDataChartPka($params = array())
	{
		if (!$this->setSettings(array(
			'authen' => 'free',
			'requestSource' => $params,
		)))
			return $this->denied();
		$status = 'Approve';
		$rs = $this->select("where jenisDiklatId = 8 and status = ? ", array($status));


		return $this->success('DATA_READ', $rs);
	}

	public function readDataChartPkp($params = array())
	{
		if (!$this->setSettings(array(
			'authen' => 'free',
			'requestSource' => $params,
		)))
			return $this->denied();
		$status = 'Approve';
		$rs = $this->select("where jenisDiklatId = 9 and status = ? ", array($status));


		return $this->success('DATA_READ', $rs);
	}

	public function readDataChartPrajab($params = array())
	{
		if (!$this->setSettings(array(
			'authen' => 'free',
			'requestSource' => $params,
		)))
			return $this->denied();
		$status = 'Approve';
		$rs = $this->select("where jenisDiklatId = 10 and status = ? ", array($status));


		return $this->success('DATA_READ', $rs);
	}

	public function readDataChartPktuf($params = array())
	{
		if (!$this->setSettings(array(
			'authen' => 'free',
			'requestSource' => $params,
		)))
			return $this->denied();
		$status = 'Approve';
		$rs = $this->select("where jenisDiklatId = 11 and status = ? ", array($status));


		return $this->success('DATA_READ', $rs);
	}

	public function readDataChartPkti($params = array())
	{
		if (!$this->setSettings(array(
			'authen' => 'free',
			'requestSource' => $params,
		)))
			return $this->denied();
		$status = 'Approve';
		$rs = $this->select("where jenisDiklatId = 12 and status = ? ", array($status));


		return $this->success('DATA_READ', $rs);
	}

	public function readDataChartD1D2($params = array())
	{
		if (!$this->setSettings(array(
			'authen' => 'free',
			'requestSource' => $params,
		)))
			return $this->denied();
		$status = 'Approve';
		$rs = $this->select("where (pendidikanTerakhirId = 9 or pendidikanTerakhirId = 10) and status = ? ", array($status));


		return $this->success('DATA_READ', $rs);
	}

	public function readDataChartD3($params = array())
	{
		if (!$this->setSettings(array(
			'authen' => 'free',
			'requestSource' => $params,
		)))
			return $this->denied();
		$status = 'Approve';
		$rs = $this->select("where pendidikanTerakhirId = 11  and status = ? ", array($status));

		return $this->success('DATA_READ', $rs);
	}

	public function readDataChartD4S1($params = array())
	{
		if (!$this->setSettings(array(
			'authen' => 'free',
			'requestSource' => $params,
		)))
			return $this->denied();
		$status = 'Approve';
		$rs = $this->select("where (pendidikanTerakhirId = 12 or pendidikanTerakhirId = 4) and status = ? ", array($status));


		return $this->success('DATA_READ', $rs);
	}

	public function readDataChartS2($params = array())
	{
		if (!$this->setSettings(array(
			'authen' => 'free',
			'requestSource' => $params,
		)))
			return $this->denied();
		$status = 'Approve';
		$rs = $this->select("where pendidikanTerakhirId = 5 and status = ? ", array($status));


		return $this->success('DATA_READ', $rs);
	}

	public function readDataChartS3($params = array())
	{
		if (!$this->setSettings(array(
			'authen' => 'free',
			'requestSource' => $params,
		)))
			return $this->denied();
		$status = 'Approve';
		$rs = $this->select("where pendidikanTerakhirId = 6 and status = ? ", array($status));

		return $this->success('DATA_READ', $rs);
	}

	public function readDataChartSmpSma($params = array())
	{
		if (!$this->setSettings(array(
			'authen' => 'free',
			'requestSource' => $params,
		)))
			return $this->denied();
		$status = 'Approve';
		$rs = $this->select("where (pendidikanTerakhirId = 7 or pendidikanTerakhirId = 8) and status = ? ", array($status));


		return $this->success('DATA_READ', $rs);
	}

	public function readDataChart1830($params = array())
	{
		if (!$this->setSettings(array(
			'authen' => 'free',
			'requestSource' => $params,
		)))
			return $this->denied();
		$status = 'Approve';
		$rs = $this->select("where umur >= 18 and umur <= 30 and status = ? ", array($status));


		return $this->success('DATA_READ', $rs);
	}

	public function readDataChart3140($params = array())
	{
		if (!$this->setSettings(array(
			'authen' => 'free',
			'requestSource' => $params,
		)))
			return $this->denied();
		$status = 'Approve';
		$rs = $this->select("where umur >= 31 and umur <= 40 and status = ? ", array($status));


		return $this->success('DATA_READ', $rs);
	}

	public function readDataChart4150($params = array())
	{
		if (!$this->setSettings(array(
			'authen' => 'free',
			'requestSource' => $params,
		)))
			return $this->denied();
		$status = 'Approve';
		$rs = $this->select("where umur >= 41 and umur <= 50 and status = ? ", array($status));


		return $this->success('DATA_READ', $rs);
	}

	public function readDataChart51($params = array())
	{
		if (!$this->setSettings(array(
			'authen' => 'free',
			'requestSource' => $params,
		)))
			return $this->denied();
		$status = 'Approve';
		$rs = $this->select("where umur >= 51 and status = ? ", array($status));

		return $this->success('DATA_READ', $rs);
	}

	public function filterData($params = array())
	{
		if (!$this->setSettings(array(
			'authen' => 'free',
			'requestSource' => $params,
		)))
			return $this->denied();

		// Query

		$field = $this->request('field');
		$keyword = $this->request('keyword');

		$queryStmt = "where $field like ?";
		$rowsCount = $this->getRowsCount($queryStmt, array("$keyword%"));

		// Limit

		$page = $this->request('page');
		$count = $this->request('count');

		$limitStmt = '';
		$pageCount = 1;

		if (!empty($page) && !empty($count)) {
			$row = ($page * $count) - $count;
			$limitStmt = "limit $row, $count";

			$pageCount = ceil($rowsCount / $count);

			if ($pageCount < 1)
				$pageCount = 1;
		}

		$rs = $this->select($queryStmt, array("$keyword%"));

		$extra = array(
			'rowsCount' => $rowsCount,
			'pageCount' => $pageCount,
		);

		return $this->success('DATA_READ', $rs, $extra);
	}

	public function saveLaporan($params = array())
	{
		if (!$this->setSettings(array(
			'authen' => 'free',
			'requestSource' => $params,
		)))
			return $this->denied();

		$id = $this->request('id');
		$judul = $this->request('judul');
		$rs = $this->find($id);

		$rs = array(
			'judulLaporan' => $judul,
		);

		$this->update($id, $rs);

		return $this->success('Simpan Judul Laporan Berhasil');
	}

	public function detailData($params = array())
	{
		if (!$this->setSettings(array(
			'authen' => 'free',
			'requestSource' => $params,
		)))
			return $this->denied();

		$id = $this->request('id');

		// Query

		$rs = $this->find($id);
		$mItems = $this->model('vi_pendaftaran_diklat_berkas');
		$rs['listBerkasPendaftaran'] = $mItems->select('where pendaftaranDiklatId = ? order by id', array($id));

		if ($rs)
			return $this->success('DATA_READ', $rs);
		else
			return $this->failed('DATA_NOT_FOUND');
	}

	public function detailDataAnggota($params = array())
	{
		if (!$this->setSettings(array(
			'authen' => 'free',
			'requestSource' => $params,
		)))
			return $this->denied();

		$id = $this->request('id');

		// Query

		$mItems = $this->model('vi_pendaftaran_diklat');
		$rs = $mItems->select("where status = 'Approve' and akunId = ?", array($id));

		if ($rs)
			return $this->success('DATA_READ', $rs);
		else
			return $this->failed('DATA_NOT_FOUND');
	}

	public function detailDiklatAnggota($params = array())
	{
		if (!$this->setSettings(array(
			'authen' => 'free',
			'requestSource' => $params,
		)))
			return $this->denied();

		$id = $this->request('id');

		// Query

		$mItems = $this->model('vi_pendaftaran_diklat');
		$rs = $mItems->select("where status = 'Approve' and akunId = ? and statusDiklatId = 1", array($id));

		if ($rs)
			return $this->success('DATA_READ', $rs[0]);
		else
			return $this->failed('DATA_NOT_FOUND');
	}

	private function checkRequest()
	{
		$diklatId = $this->request('diklatId');
		$akunId = $this->request('akunId');

		$error = array();

		if (empty($diklatId))
			$error['diklatId'] = $this->string('NAMA_DIKLAT_REQUIRED');

		if (empty($akunId))
			$error['akunId'] = $this->string('NAMA_PESERTA_REQUIRED');

		return $error;
	}

	private function checkRequestDiklat()
	{

		$nama = $this->request('nama');
		$pangkatGolonganId = $this->request('pangkatGolonganId');
		$jabatanEselon = $this->request('jabatanEselon');
		$unitKerja = $this->request('unitKerja');
		$instansiId = $this->request('instansiId');
		$instansiLainnya = $this->request('instansiLainnya');
		$jenisKelaminId = $this->request('jenisKelaminId');
		$tempatLahir = $this->request('tempatLahir');
		$tanggalLahir = $this->request('tanggalLahir');
		$agamaId = $this->request('agamaId');
		$alamat = $this->request('alamat');
		$alamatKantor = $this->request('alamatKantor');
		$telpHp = $this->request('telpHp');
		$foto = $this->request('foto');

		$error = array();

		if (empty($nama))
			$error['nama'] = $this->string('NAMA_REQUIRED');

		if (empty($alamat))
			$error['alamat'] = $this->string('Alamat Rumah belum diisi');

		if (empty($alamatKantor))
			$error['alamatKantor'] = $this->string('Alamat Kantor belum diisi');

		if (empty($pangkatGolonganId))
			$error['pangkatGolonganId'] = $this->string('PANGKAT_GOLONGAN_REQUIRED');

		if (empty($jabatanEselon))
			$error['jabatanEselon'] = $this->string('JABATAN_ESELON_REQUIRED');

		if (empty($unitKerja))
			$error['unitKerja'] = $this->string('UNIT_KERJA_REQUIRED');

		if (empty($instansiId))
			$error['instansiId'] = $this->string('INSTANSI_REQUIRED');

		if ($instansiId == '-1') {
			if (empty($instansiLainnya))
				$error['instansiLainnya'] = $this->string('INSTANSI_LAINNYA_REQUIRED');
		}

		if (empty($jenisKelaminId))
			$error['jenisKelaminId'] = $this->string('GENDER_REQUIRED');

		if (empty($tempatLahir))
			$error['tempatLahir'] = $this->string('TEMPAT_LAHIR_REQUIRED');

		if (empty($tanggalLahir))
			$error['tanggalLahir'] = $this->string('TANGGAL_LAHIR_REQUIRED');

		if (empty($agamaId))
			$error['agamaId'] = $this->string('AGAMA_REQUIRED');

		if (empty($telpHp))
			$error['telpHp'] = $this->string('TELP_HP_REQUIRED');

		if (empty($foto))
			$error['foto'] = $this->string('FOTO_REQUIRED');

		return $error;
	}

	public function createData($params = array())
	{
		if (!$this->setSettings(array(
			'authen' => 'user',
			'requestSource' => $params,
		)))
			return $this->denied();

		$error = $this->checkRequest();

		$diklatId = $this->request('diklatId');
		$akunId = $this->request('akunId');

		if (count($error) > 0)
			return $this->invalid('PLEASE_CORRECT', $error);

		$rs = array(
			'diklatId' => $diklatId,
			'akunId' => $akunId,
		);

		$this->insert($rs);

		return $this->success('DATA_CREATED');
	}

	public function daftarDiklat($params = array())
	{
		if (!$this->setSettings(array(
			'authen' => 'free',
			'requestSource' => $params,
		)))
			return $this->denied();

		$error = $this->checkRequestDiklat();

		$diklatId = $this->request('diklatId');
		$akunId = $this->request('akunId');
		$nama = $this->request('nama');
		$nik = $this->request('nik');
		$npwp = $this->request('npwp');
		$pangkatGolonganId = $this->request('pangkatGolonganId');
		$jabatanEselon = $this->request('jabatanEselon');
		$unitKerja = $this->request('unitKerja');
		$instansiId = $this->request('instansiId');
		$instansiLainnya = $this->request('instansiLainnya');
		$pendidikanTerakhirId = $this->request('pendidikanTerakhirId');
		$jenisKelaminId = $this->request('jenisKelaminId');
		$tempatLahir = $this->request('tempatLahir');
		$tanggalLahir = $this->request('tanggalLahir');
		$agamaId = $this->request('agamaId');
		$alamat = $this->request('alamat');
		$alamatKantor = $this->request('alamatKantor');

		$status = $this->request('status');
		$telpHp = $this->request('telpHp');
		$foto = $this->request('foto');
		$suratPerintahTugas = $this->request('suratPerintahTugas');
		$suratPernyataanBebasTugasKedinasan = $this->request('suratPernyataanBebasTugasKedinasan');
		$suratKeputusanCPNS = $this->request('suratKeputusanCPNS');
		$suratKeputusanPangkat = $this->request('suratKeputusanPangkat');
		$suratKeputusanJabatanTerakhir = $this->request('suratKeputusanJabatanTerakhir');
		$ijazahTerakhir = $this->request('ijazahTerakhir');
		$suratKesehatan = $this->request('suratKesehatan');
		$suratKeteranganDokterdariRumahSakitPemerintah = $this->request('suratKeteranganDokterdariRumahSakitPemerintah');
		$askesBPJSatauKIS = $this->request('askesBPJSatauKIS');
		$jumlahUploadDiklat = $this->request('jumlahUploadDiklat');

		$tahunFull = date('Y');

		$rs = $this->scalar("select count(id) as id from vi_pendaftaran_diklat where diklatId = ? ", array($diklatId));
		$idDiklat = $rs['id'];

		$mDiklat = $this->model('vi_diklat');
		$rsDiklat = $mDiklat->find($diklatId);
		$kuota = $rsDiklat['kuota'];
		$kode = $rsDiklat['kode'];
		$jumlahUpload = $rsDiklat['jumlahUpload'];

		$mDiklat = $this->model('tb_diklat');
		if ($jumlahUploadDiklat == $jumlahUpload)
			$statusUpload = "Selesai";
		else
			$statusUpload = "Belum";

		if ($idDiklat) {
			$kodePeserta = $kode . '-' . (((int) $idDiklat) + 1) . '-' . $tahunFull;
		} else {
			$kodePeserta = $kode . '-' . '1' . '-' . $tahunFull;
		}
		if ($kuota == $idDiklat)
			$error['kuota'] = $this->string('KUOTA_FULL');

		$rs = $this->scalar("select * from vi_pendaftaran_diklat where diklatId = ? and akunId = ? ", array($diklatId, $akunId));
		// $rs = $this->scalar("select * from vi_pendaftaran_diklat where (status = 'Approve' or status = 'Pending' ) and statusDiklatId = 1 and akunId = ? ", array($akunId));

		if ($rs)
			$error['akun'] = $this->string('IS_REG');

		if (count($error) > 0)
			return $this->invalid('PLEASE_CORRECT', $error);

		$mUnitKerja = $this->model('tb_unit_kerja');
		$rsUnitKerja = $mUnitKerja->single('where unitKerja = ? ', array($unitKerja));

		if ($rsUnitKerja)
			$unitKerjaId = $rsUnitKerja['id'];
		else {
			$rsUnitKerja = array(
				'unitKerja' => $unitKerja,
			);
			$mUnitKerja->insert($rsUnitKerja);
			$unitKerjaId = $mUnitKerja->getLastInsertId();
		}

		$mAnggota = $this->model('tb_akun');
		$rsAnggota = $mAnggota->single('where id= ?', array($akunId));
		$last_foto = $rsAnggota['foto'];

		if (!empty($foto)) {
			if ($foto != $last_foto) {

				$this->deleteArchive($last_foto);

				if (!$this->moveToArchive($foto))
					return $this->failed('FILE_UPLOAD_FAILED');
			}
		}

		$last_suratPerintahTugas = $rsAnggota['suratPerintahTugas'];

		if (!empty($suratPerintahTugas)) {
			if ($suratPerintahTugas != $last_suratPerintahTugas) {

				$this->deleteArchive($last_suratPerintahTugas);

				if (!$this->moveToArchive($suratPerintahTugas))
					return $this->failed('FILE_UPLOAD_FAILED');
			}
			$suratPerintahTugas1 = 1;
		} else {
			$suratPerintahTugas1 = 0;
		}

		$last_suratPernyataanBebasTugasKedinasan = $rsAnggota['suratPernyataanBebasTugasKedinasan'];

		if (!empty($suratPernyataanBebasTugasKedinasan)) {
			if ($suratPernyataanBebasTugasKedinasan != $last_suratPernyataanBebasTugasKedinasan) {

				$this->deleteArchive($last_suratPernyataanBebasTugasKedinasan);

				if (!$this->moveToArchive($suratPernyataanBebasTugasKedinasan))
					return $this->failed('FILE_UPLOAD_FAILED');
			}
			$suratPernyataanBebasTugasKedinasan1 = 2;
		} else {
			$suratPernyataanBebasTugasKedinasan1 = 0;
		}

		$last_suratKeputusanCPNS = $rsAnggota['suratKeputusanCPNS'];

		if (!empty($suratKeputusanCPNS)) {
			if ($suratKeputusanCPNS != $last_suratKeputusanCPNS) {

				$this->deleteArchive($last_suratKeputusanCPNS);

				if (!$this->moveToArchive($suratKeputusanCPNS))
					return $this->failed('FILE_UPLOAD_FAILED');
			}
			$suratKeputusanCPNS1 = 3;
		} else {
			$suratKeputusanCPNS1 = 0;
		}

		$last_suratKeputusanPangkat = $rsAnggota['suratKeputusanPangkat'];

		if (!empty($suratKeputusanPangkat)) {
			if ($suratKeputusanPangkat != $last_suratKeputusanPangkat) {

				$this->deleteArchive($last_suratKeputusanPangkat);

				if (!$this->moveToArchive($suratKeputusanPangkat))
					return $this->failed('FILE_UPLOAD_FAILED');
			}
			$suratKeputusanPangkat1 = 4;
		} else {
			$suratKeputusanPangkat1 = 0;
		}

		$last_suratKeputusanJabatanTerakhir = $rsAnggota['suratKeputusanJabatanTerakhir'];

		if (!empty($suratKeputusanJabatanTerakhir)) {
			if ($suratKeputusanJabatanTerakhir != $last_suratKeputusanJabatanTerakhir) {

				$this->deleteArchive($last_suratKeputusanJabatanTerakhir);

				if (!$this->moveToArchive($suratKeputusanJabatanTerakhir))
					return $this->failed('FILE_UPLOAD_FAILED');
			}
			$suratKeputusanJabatanTerakhir1 = 5;
		} else {
			$suratKeputusanJabatanTerakhir1 = 0;
		}

		$last_ijazahTerakhir = $rsAnggota['ijazahTerakhir'];

		if (!empty($ijazahTerakhir)) {
			if ($ijazahTerakhir != $last_ijazahTerakhir) {

				$this->deleteArchive($last_ijazahTerakhir);

				if (!$this->moveToArchive($ijazahTerakhir))
					return $this->failed('FILE_UPLOAD_FAILED');
			}
			$ijazahTerakhir1 = 6;
		} else {
			$ijazahTerakhir1 = 0;
		}

		$last_suratKesehatan = $rsAnggota['suratKesehatan'];

		if (!empty($suratKesehatan)) {
			if ($suratKesehatan != $last_suratKesehatan) {

				$this->deleteArchive($last_suratKesehatan);

				if (!$this->moveToArchive($suratKesehatan))
					return $this->failed('FILE_UPLOAD_FAILED');
			}
			$suratKesehatan1 = 9;
		} else {
			$suratKesehatan1 = 0;
		}


		$last_suratKeteranganDokterdariRumahSakitPemerintah = $rsAnggota['suratKeteranganDokterdariRumahSakitPemerintah'];

		if (!empty($suratKeteranganDokterdariRumahSakitPemerintah)) {
			if ($suratKeteranganDokterdariRumahSakitPemerintah != $last_suratKeteranganDokterdariRumahSakitPemerintah) {

				$this->deleteArchive($last_suratKeteranganDokterdariRumahSakitPemerintah);

				if (!$this->moveToArchive($suratKeteranganDokterdariRumahSakitPemerintah))
					return $this->failed('FILE_UPLOAD_FAILED');
			}
			$suratKeteranganDokterdariRumahSakitPemerintah1 = 7;
		} else {
			$suratKeteranganDokterdariRumahSakitPemerintah1 = 0;
		}

		$last_askesBPJSatauKIS = $rsAnggota['askesBPJSatauKIS'];

		if (!empty($askesBPJSatauKIS)) {
			if ($askesBPJSatauKIS != $last_askesBPJSatauKIS) {

				$this->deleteArchive($last_askesBPJSatauKIS);

				if (!$this->moveToArchive($askesBPJSatauKIS))
					return $this->failed('FILE_UPLOAD_FAILED');
			}
			$askesBPJSatauKIS1 = 8;
		} else {
			$askesBPJSatauKIS1 = 0;
		}

		$mAnggota = $this->model('tb_akun');
		$mAnggota->update($akunId, array(
			'nama' => $nama,
			'nik' => $nik,
			'npwp' => $npwp,
			'pangkatGolonganId' => $pangkatGolonganId,
			'jabatanEselon' => $jabatanEselon,
			'unitKerjaId' => $unitKerjaId,
			'instansiId' => $instansiId,
			'instansiLainnya' => $instansiLainnya,
			'pendidikanTerakhirId' => $pendidikanTerakhirId,
			'jenisKelaminId' => $jenisKelaminId,
			'tempatLahir' => $tempatLahir,
			'tanggalLahir' => $tanggalLahir,
			'agamaId' => $agamaId,
			'alamat' => $alamat,
			'alamatKantor' => $alamatKantor,
			'telpHp' => $telpHp,
			'foto' => $foto,
			'suratPerintahTugas' => $suratPerintahTugas,
			'suratPernyataanBebasTugasKedinasan' => $suratPernyataanBebasTugasKedinasan,
			'suratKeputusanCPNS' => $suratKeputusanCPNS,
			'suratKeputusanPangkat' => $suratKeputusanPangkat,
			'suratKeputusanJabatanTerakhir' => $suratKeputusanJabatanTerakhir,
			'ijazahTerakhir' => $ijazahTerakhir,
			'suratKesehatan' => $suratKesehatan,
			'suratKeteranganDokterdariRumahSakitPemerintah' => $suratKeteranganDokterdariRumahSakitPemerintah,
			'askesBPJSatauKIS' => $askesBPJSatauKIS,
		));


		$rs = array(
			'kodePeserta' => $kodePeserta,
			'diklatId' => $diklatId,
			'akunId' => $akunId,
			'statusUpload' => $statusUpload,
			'judulLaporan' => '',
			'status' => $status,
		);

		$this->insert($rs);
		$lastId = $this->getLastInsertId();
		$rs = $this->scalar("select count(id) as id from vi_pendaftaran_diklat where status = 'Approve' and diklatId = ? ", array($diklatId));
		$idDiklat = $rs['id'];
		$mDiklat = $this->model('tb_diklat');
		$rsDiklat = $mDiklat->find($diklatId);
		$kuota = $rsDiklat['kuota'];
		$jenisDiklatId = $rsDiklat['jenisDiklatId'];

		if ($kuota == $idDiklat) {
			$mDiklat = $this->model('tb_diklat');
			$mDiklat->update($diklatId, array(
				'statusDiklatId' => 2,
			));
		}

		$mJenisDiklatBerkas = $this->model('tb_jenis_diklat_berkas');
		$rsJenisDiklatBerkas = $mJenisDiklatBerkas->select("where jenisDiklatId = ? ", array($jenisDiklatId));

		$mBerkasDiklat = $this->model('tb_pendaftaran_diklat_berkas');
		foreach ($rsJenisDiklatBerkas as $sub) {
			$mBerkasDiklat->insert(array(
				'pendaftaranDiklatId' => $lastId,
				'berkasPendaftaranId' => $sub['berkasPendaftaranId'],
				'berkas' => '',
				'status' => 'Belum',
			));
		}

		if ($suratPerintahTugas1 > 0) {
			$rsPendafBerkas = $mBerkasDiklat->select("where pendaftaranDiklatId = ? and berkasPendaftaranId = ? ", array($lastId, $suratPerintahTugas1));
			if ($rsPendafBerkas)
				$mBerkasDiklat->update($rsPendafBerkas[0]['id'], array('berkas' => $suratPerintahTugas, 'status' => 'Selesai'));
		}

		if ($suratPernyataanBebasTugasKedinasan1 > 0) {
			$rsPendafBerkas = $mBerkasDiklat->select("where pendaftaranDiklatId = ? and berkasPendaftaranId = ? ", array($lastId, $suratPernyataanBebasTugasKedinasan1));
			if ($rsPendafBerkas)
				$mBerkasDiklat->update($rsPendafBerkas[0]['id'], array('berkas' => $suratPernyataanBebasTugasKedinasan, 'status' => 'Selesai'));
		}

		if ($suratKeputusanCPNS1 > 0) {
			$rsPendafBerkas = $mBerkasDiklat->select("where pendaftaranDiklatId = ? and berkasPendaftaranId = ? ", array($lastId, $suratKeputusanCPNS1));
			if ($rsPendafBerkas)
				$mBerkasDiklat->update($rsPendafBerkas[0]['id'], array('berkas' => $suratKeputusanCPNS, 'status' => 'Selesai'));
		}

		if ($suratKeputusanPangkat1 > 0) {
			$rsPendafBerkas = $mBerkasDiklat->select("where pendaftaranDiklatId = ? and berkasPendaftaranId = ? ", array($lastId, $suratKeputusanPangkat1));
			if ($rsPendafBerkas)
				$mBerkasDiklat->update($rsPendafBerkas[0]['id'], array('berkas' => $suratKeputusanPangkat, 'status' => 'Selesai'));
		}

		if ($suratKeputusanJabatanTerakhir1 > 0) {
			$rsPendafBerkas = $mBerkasDiklat->select("where pendaftaranDiklatId = ? and berkasPendaftaranId = ? ", array($lastId, $suratKeputusanJabatanTerakhir1));
			if ($rsPendafBerkas)
				$mBerkasDiklat->update($rsPendafBerkas[0]['id'], array('berkas' => $suratKeputusanJabatanTerakhir, 'status' => 'Selesai'));
		}

		if ($ijazahTerakhir1 > 0) {
			$rsPendafBerkas = $mBerkasDiklat->select("where pendaftaranDiklatId = ? and berkasPendaftaranId = ? ", array($lastId, $ijazahTerakhir1));
			if ($rsPendafBerkas)
				$mBerkasDiklat->update($rsPendafBerkas[0]['id'], array('berkas' => $ijazahTerakhir, 'status' => 'Selesai'));
		}

		if ($suratKesehatan1 > 0) {
			$rsPendafBerkas = $mBerkasDiklat->select("where pendaftaranDiklatId = ? and berkasPendaftaranId = ? ", array($lastId, $suratKesehatan1));
			if ($rsPendafBerkas)
				$mBerkasDiklat->update($rsPendafBerkas[0]['id'], array('berkas' => $suratKesehatan, 'status' => 'Selesai'));
		}


		if ($suratKeteranganDokterdariRumahSakitPemerintah1 > 0) {
			$rsPendafBerkas = $mBerkasDiklat->select("where pendaftaranDiklatId = ? and berkasPendaftaranId = ? ", array($lastId, $suratKeteranganDokterdariRumahSakitPemerintah1));
			if ($rsPendafBerkas)
				$mBerkasDiklat->update($rsPendafBerkas[0]['id'], array('berkas' => $suratKeteranganDokterdariRumahSakitPemerintah, 'status' => 'Selesai'));
		}

		if ($askesBPJSatauKIS1 > 0) {
			$rsPendafBerkas = $mBerkasDiklat->select("where pendaftaranDiklatId = ? and berkasPendaftaranId = ? ", array($lastId, $askesBPJSatauKIS1));
			if ($rsPendafBerkas)
				$mBerkasDiklat->update($rsPendafBerkas[0]['id'], array('berkas' => $askesBPJSatauKIS, 'status' => 'Selesai'));
		}

		return $this->success('DATA_CREATED', $lastId);
	}

	public function updateDataPending($params = array())
	{
		if (!$this->setSettings(array(
			'authen' => 'user',
			'requestSource' => $params,
		)))
			return $this->denied();

		$id = $this->request('id');

		$rs = $this->find($id);
		$namaDiklat = $rs['namaDiklat'];
		$email = $rs['email'];
		if (!$rs)
			return $this->failed('DATA_NOT_FOUND');

		$rs = array(
			'status' => 'Approve',
		);

		$this->update($id, $rs);

		// $akunId = $rs['akunId'];
		// $token = $this->encrypt();

		// $mVerifikasiAkun = $this->model('tb_verifikasi_akun');

		// $mVerifikasiAkun->insert(array(
		// 'akunId' => $akunId,
		// 'token' => $token,
		// 'verifiedId' => 2,
		// ));


		$message =
			"Selamat pendaftaran Anda telah diterima dan disetujui untuk mengikuti Pelatihan.  " . "<br>" .
			"Selanjutnya Anda dipersilahkan untuk masuk/login dengan account anda di website resmi kami <br> " . base_url() . "<br> lalu masuk ke Menu User Profile untuk mencetak Form & Jadwal.";


		// mail function

		$config = array(
			'protocol' => 'smtp',
			'smtp_host' => 'ssl://bpsdm.lampungprov.go.id',
			'smtp_port' => 465,
			'smtp_timeout' => 5,
			'smtp_user' => '_mainaccount@bpsdm.lampungprov.go.id',
			'smtp_pass' => 'Bpsdmlp9^^v(((',
			'mailtype'  => 'html',
			'charset'   => 'iso-8859-1'
		);

		$this->load->library('email');
		$this->email->set_newline("\r\n");

		$this->email->initialize($config);
		$this->email->from($config['smtp_user']);
		$this->email->to($email);

		$this->email->subject('Pemberitahuan Diklatlampung');
		$this->email->message($message);


		$this->email->send();

		return $this->success('DATA_UPDATED');
	}

	public function updateDataReject($params = array())
	{
		if (!$this->setSettings(array(
			'authen' => 'user',
			'requestSource' => $params,
		)))
			return $this->denied();

		$id = $this->request('id');

		$rs = $this->find($id);
		$namaDiklat = $rs['namaDiklat'];
		$email = $rs['email'];
		if (!$rs)
			return $this->failed('DATA_NOT_FOUND');

		$rs = array(
			'status' => 'Reject',
		);

		$this->update($id, $rs);

		$message =
			"Mohon maaf, Anda tidak bisa mengikuti Pelatihan ini. Silahkan dicoba kembali untuk pelatihan pada periode selanjutnya.
		Untuk keterangan lebih lanjut, dipersilahkan untuk mengunjungi halaman kontak di website resmi kami  " . "<br>" .
			base_url() . "contact_us " . "<br> Terima kasih.";

		// mail function

		$config = array(
			'protocol' => 'smtp',
			'smtp_host' => 'ssl://bpsdm.lampungprov.go.id',
			'smtp_port' => 465,
			'smtp_timeout' => 5,
			'smtp_user' => '_mainaccount@bpsdm.lampungprov.go.id',
			'smtp_pass' => 'Bpsdmlp9^^v(((',
			'mailtype'  => 'html',
			'charset'   => 'iso-8859-1'
		);

		$this->load->library('email');
		$this->email->set_newline("\r\n");

		$this->email->initialize($config);
		$this->email->from($config['smtp_user']);
		$this->email->to($email);

		$this->email->subject('Pemberitahuan Diklatlampung');
		$this->email->message($message);


		$this->email->send();

		return $this->success('DATA_UPDATED');
	}

	public function updateData($params = array())
	{
		if (!$this->setSettings(array(
			'authen' => 'user',
			'requestSource' => $params,
		)))
			return $this->denied();

		$id = $this->request('id');
		$diklatId = $this->request('diklatId');
		$akunId = $this->request('akunId');

		$error = $this->checkRequest();

		$rs = $this->find($id);

		if (!$rs)
			return $this->failed('DATA_NOT_FOUND');

		if (count($error) > 0)
			return $this->invalid('PLEASE_CORRECT', $error);

		$rs = array(
			'diklatId' => $diklatId,
			'akunId' => $akunId,
		);

		$this->update($id, $rs);
		return $this->success('DATA_UPDATED');
	}

	public function cariAnggota($params = array())
	{
		if (!$this->setSettings(array(
			'authen' => 'free',
			'requestSource' => $params,
		)))
			return $this->denied();

		$akunId = $this->request('akunId');
		$diklatId = $this->request('diklatId');

		// Query
		// $rs = $this->select("where akunId = ? and diklatId = ? ", array($akunId,$diklatId));
		$publishStatusId = 2;
		$rs = $this->select("where akunId = ? and diklatId = ? and publishStatusId = ?", array($akunId, $diklatId, $publishStatusId));

		if (!$rs)
			$rs = $this->select("where status = 'Approve' and akunId = ? and statusDiklatId = 1", array($akunId));

		if ($rs)
			return $this->success('DATA_READ', $rs);
		else
			return $this->failed('DATA_NOT_FOUND');
	}

	public function deleteData($params = array())
	{
		if (!$this->setSettings(array(
			'authen' => 'user',
			'requestSource' => $params,
		)))
			return $this->denied();

		$id = $this->request('id');
		$rs = $this->find($id);

		$this->delete($id);
		return $this->success('DATA_DELETED');
	}

	public function multipleDeleteData($params = array())
	{
		if (!$this->setSettings(array(
			'authen' => 'user',
			'requestSource' => $params,
		)))
			return $this->denied();

		$multipleId = $this->request('multipleId');
		$mItems = $this->model('tb_pendaftaran_diklat_berkas');

		foreach ($multipleId as $id) {
			$rs = $this->find($id);

			$mItems->delete('pendaftaranDiklatId', $id);
			$this->delete($id);
		}

		return $this->success('DATA_DELETED');
	}

	public function createExcelList($params = array())
	{
		if (!$this->setSettings(array(
			'authen' => 'free',
			'requestSource' => $params,
		)))
			return $this->denied();

		// Excel Settings
		$diklatId = $this->request('id');

		$this->load->library('excel');

		$render = 'renderDetail';
		$rs = $this->select("where diklatId = ?  ", array($diklatId));

		// Output

		if (count($rs) > 0) {


			$nameFile = 'PendaftaranDiklat';
			$fileUrl = $this->$render(
				$nameFile,
				array(
					'rs' => $rs,
				)
			);

			return $this->success('FILE_CREATED', $fileUrl);
		} else
			return $this->failed('FILE_CREATE_FAILED');
	}


	private function renderDetail($template, $data)
	{

		$rs = $data['rs'];

		$reader = PHPExcel_IOFactory::createReader('Excel5');
		$templateUrl = "application/views/excel/$template.xls";

		$excel = $reader->load($templateUrl);
		$sheet = $excel->setActiveSheetIndex(0);

		$i = 0;
		$x = 3;

		$namaDiklat = $rs[0]['namaDiklat'];
		$sheet->setCellValue("A1", $namaDiklat);
		$styleArray = array(
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				)
			)
		);

		foreach ($rs as $row) {
			$x++;
			$i++;

			$sheet->setCellValue("A$x", $i);
			// $sheet->getStyle("A$x")->applyFromArray($styleArray);
			$sheet->setCellValue("B$x", $row['lokasi']);
			// $sheet->getStyle("B$x")->applyFromArray($styleArray);
			if ($row['instansiId'])
				$sheet->setCellValue("C$x", $row['instansi']);
			else
				$sheet->setCellValue("C$x", $row['instansiLainnya']);

			$sheet->setCellValue("G$x", "'" . $row['nip']);
			$sheet->setCellValue("I$x", $row['nama']);
			$sheet->setCellValue("J$x", $row['tempatLahir']);
			$sheet->setCellValue("K$x", $row['_tanggalLahir']);
			$sheet->setCellValue("L$x", $row['jenisKelamin']);
			$sheet->setCellValue("M$x", $row['agama']);
			$sheet->setCellValue("N$x", $row['pangkatGolongan']);

			$sheet->setCellValue("P$x", $row['alamat']);
			$sheet->setCellValue("Q$x", $row['alamatKantor']);
			$sheet->setCellValue("R$x", $row['telpHp']);
			$sheet->setCellValue("S$x", $row['jabatanEselon']);
			$sheet->setCellValue("T$x", $row['email']);
			$sheet->setCellValue("U$x", $row['nik']);
			$sheet->setCellValue("V$x", $row['npwp']);
			$sheet->setCellValue("W$x", $row['judulLaporan']);
		}

		$fileName = $this->encrypt() . '.xls';
		$filePath = "asset/tmp/$fileName";

		$writer = PHPExcel_IOFactory::createWriter($excel, 'Excel5');
		$writer->save($filePath);

		$fileUrl = base_url() . $filePath;

		return $fileUrl;
	}


	public function createPDFDetail($params = array())
	{
		if (!$this->setSettings(array(
			'authen' => 'free',
			'requestSource' => $params,
		)))
			return $this->denied();

		$this->load->library('Pdf');
		$this->pdf->setPrintHeader(false);
		$this->pdf->SetPrintFooter(false);
		$this->pdf->setSettings(array(
			'strings' => $this->config->item('strings'),
			'pageOrientation' => 'P',
			'pageSize' => 'LEGAL',
			// 'fontName' => 'times',
		));

		$id = $this->request('id');
		$row = $this->find($id);
		$mBerkasDiklat = $this->model('vi_pendaftaran_diklat_berkas');
		$rsPendafBerkas = $mBerkasDiklat->select("where pendaftaranDiklatId = ?  ", array($id));

		if (!empty($row)) {
			$fileUrl = $this->pdf->render1(
				'PendaftaranDiklatDetail',
				array(
					'row' => $row,
					'rsBerkas' => $rsPendafBerkas,
				)
			);
			return $this->success('FILE_CREATED', $fileUrl);
		} else
			return $this->failed('FILE_CREATE_FAILED');
	}
}

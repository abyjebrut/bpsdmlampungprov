<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class HeadContent extends MY_Model {
    protected $table = 'tb_head_content';
    
    public function detailData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();
        
		$id = 1;
        
        // Query
        
		$rs = $this->find($id);
		
		$mItems = $this->model('vi_head_links');
		$rs['links'] = $mItems->select('where headContentId = ? order by id', array($id));

        if($rs)
            return $this->success('DATA_READ', $rs);
        else
            return $this->failed('DATA_NOT_FOUND');
	}

    private function checkRequest() {
        

		$error = array();


        return $error;
    }

	public function saveData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();
        
		$error = $this->checkRequest();

        if(count($error) > 0)
			return $this->invalid('PLEASE_CORRECT', $error);

        $id = 1;
        $description = $this->request('description');
        $description_sl = $this->request('descriptionSl');
        $description_el = $this->request('descriptionEl');
        $links = $this->request('links');

		$rs = array(
				'description' => $description,
				'description_sl' => $description_sl,
				'description_el' => $description_el,
		);

        $rsFound = $this->find($id);

        if($rsFound) {

            $this->update($id, $rs);
            
            $mItems = $this->model('tb_head_links');
            $mItems->delete('headContentId', $id);
            
            if($links){
	            $mItems = $this->model('tb_head_links');
	             
	            foreach($links as $link) {
	            	$mItems->insert(array(
	            			'headContentId' => $id,
	            			'linkCaption' => $link['linkCaption'],
	            			'linkCaption_sl' => $link['linkCaption_sl'],
	            			'linkCaption_el' => $link['linkCaption_el'],
	            			'linkBehaviorId' => $link['linkBehaviorId'],
	            			'pagesId' => $link['pagesId'],
	            			'url' => $link['url'],
	            			'openUrlOnNewTabId' => $link['openUrlOnNewTabId'],
	            	));
	            }
            }
            return $this->success('DATA_UPDATED');
        }
        else {

            $rs['id'] = $id;

            $this->insert($rs);
            
            if($links){
	            $mItems = $this->model('tb_head_links');
	             
	            foreach($links as $link) {
	            	$mItems->insert(array(
	            			'headContentId' => $id,
	            			'linkCaption' => $link['linkCaption'],
	            			'linkCaption_sl' => $link['linkCaption_sl'],
	            			'linkCaption_el' => $link['linkCaption_el'],
	            			'linkBehaviorId' => $link['linkBehaviorId'],
	            			'pagesId' => $link['pagesId'],
	            			'url' => $link['url'],
	            			'openUrlOnNewTabId' => $link['openUrlOnNewTabId'],
	            	));
	            }
            }
            return $this->success('DATA_CREATED');
        }

	}

}

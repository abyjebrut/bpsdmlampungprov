<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class JenisDiklat extends MY_Model {
    protected $table = 'tb_jenis_diklat';
    protected $view = 'vi_jenis_diklat';
    
    public function readData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();

        // Order

        $orderBy = $this->request('orderBy');
        $reverse = $this->request('reverse');

        $orderStmt = '';

        if (!empty($orderBy)) {
            $orderStmt = "order by $orderBy";

            if (!empty($reverse)) {
                if ($reverse == 1)
                    $orderStmt .= ' desc';
            }
        }

        // Limit
        $page = $this->request('page');
		$count = $this->request('count');

        $limitStmt = '';

      	$rowsCount = $this->getRowsCount($orderStmt);
        
      	$pageCount = 1;

        if (!empty($page) && !empty($count)) {
            $row = ($page * $count) - $count;
            $limitStmt = "limit $row, $count";

            $pageCount = ceil($rowsCount / $count);

            if($pageCount < 1)
                $pageCount = 1;
        }

        // Query

            $rs = $this->select($orderStmt.' '.$limitStmt);
    
        $extra = array(
            'rowsCount' => $rowsCount,
            'pageCount' => $pageCount,
        );

		return $this->success('DATA_READ', $rs, $extra);
    }

    public function readDataAsli($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();

        // Order

        $orderBy = $this->request('orderBy');
        $reverse = $this->request('reverse');

        $orderStmt = '';

        if (!empty($orderBy)) {
            $orderStmt = "order by $orderBy";

            if (!empty($reverse)) {
                if ($reverse == 1)
                    $orderStmt .= ' desc';
            }
        }

        // Limit
        $page = $this->request('page');
		$count = $this->request('count');

        $limitStmt = '';

      	$rowsCount = $this->getRowsCount($orderStmt);
        
      	$pageCount = 1;

        if (!empty($page) && !empty($count)) {
            $row = ($page * $count) - $count;
            $limitStmt = "limit $row, $count";

            $pageCount = ceil($rowsCount / $count);

            if($pageCount < 1)
                $pageCount = 1;
        }

        // Query
        $mItems = $this->model('vi_jenis_diklat_asli');
        $rs = $mItems->select($orderStmt.' '.$limitStmt);
    
        $extra = array(
            'rowsCount' => $rowsCount,
            'pageCount' => $pageCount,
        );

		return $this->success('DATA_READ', $rs, $extra);
    }

    public function readDataFungsional($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();

        // Order

        $orderBy = $this->request('orderBy');
        $reverse = $this->request('reverse');

        $orderStmt = '';

        if (!empty($orderBy)) {
            $orderStmt = "order by $orderBy";

            if (!empty($reverse)) {
                if ($reverse == 1)
                    $orderStmt .= ' desc';
            }
        }

        // Limit
        $page = $this->request('page');
		$count = $this->request('count');

        $limitStmt = '';

      	$rowsCount = $this->getRowsCount($orderStmt);
        
      	$pageCount = 1;

        if (!empty($page) && !empty($count)) {
            $row = ($page * $count) - $count;
            $limitStmt = "limit $row, $count";

            $pageCount = ceil($rowsCount / $count);

            if($pageCount < 1)
                $pageCount = 1;
        }

        // Query
        $mItems = $this->model('vi_jenis_diklat_fungsional');
        $rs = $mItems->select($orderStmt.' '.$limitStmt);
    
        $extra = array(
            'rowsCount' => $rowsCount,
            'pageCount' => $pageCount,
        );

		return $this->success('DATA_READ', $rs, $extra);
    }
    
    public function filterData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();
        
        // Query

        $field = $this->request('field');
        $keyword = $this->request('keyword');

        $queryStmt = "where $field like ?";
        $rowsCount = $this->getRowsCount($queryStmt, array("$keyword%"));

        // Limit

        $page = $this->request('page');
		$count = $this->request('count');

        $limitStmt = '';
        $pageCount = 1;

        if (!empty($page) && !empty($count)) {
            $row = ($page * $count) - $count;
            $limitStmt = "limit $row, $count";

            $pageCount = ceil($rowsCount / $count);

            if($pageCount < 1)
                $pageCount = 1;
        }

        $rs = $this->select($queryStmt, array("$keyword%"));

        $extra = array(
            'rowsCount' => $rowsCount,
            'pageCount' => $pageCount,
        );

		return $this->success('DATA_READ', $rs, $extra);
	}

	public function detailData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();

		$id = $this->request('id');
        
        // Query
        
        $rs = $this->find($id);
        $mItems = $this->model('vi_jenis_diklat_berkas');
		$rs['listBerkasPendaftaran'] = $mItems->select('where jenisDiklatId = ? order by id', array($id));


        if($rs)
            return $this->success('DATA_READ', $rs);
        else
            return $this->failed('DATA_NOT_FOUND');
	}

    private function checkRequest() {
        $jenisDiklat = $this->request('jenisDiklat');
        $listBerkasPendaftaran = $this->request('listBerkasPendaftaran');

		$error = array();
		
		if(empty($jenisDiklat))
            $error['jenisDiklat'] = $this->string('JENIS_DIKLAT_REQUIRED');
            
            if(empty($listBerkasPendaftaran))
			$error['berkasPendaftaranId'] = $this->string('Berkas Pendaftaran diperlukan');

        return $error;
    }

	public function createData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

		$error = $this->checkRequest();

		$jenisDiklat = $this->request('jenisDiklat');
		$namaSingkat = $this->request('namaSingkat');
		$listBerkasPendaftaran = $this->request('listBerkasPendaftaran');

        $rs = $this->find('jenisDiklat', $jenisDiklat);

		if($rs)
            $error['jenisDiklat'] = $this->string('JENIS_DIKLAT_IS_REG');

		if(count($error) > 0)
			return $this->invalid('PLEASE_CORRECT', $error);

		$rs = array(
            'jenisDiklat' => $jenisDiklat,
            'namaSingkat' => $namaSingkat,
        );

		$this->insert($rs);
		// position
		$lastId = $this->getLastInsertId();
        $this->update($lastId, array('position' => $lastId));
        
        $mSubMenu = $this->model('tb_jenis_diklat_berkas');
        foreach($listBerkasPendaftaran as $sub) {
            $mSubMenu->insert(array(
                    'jenisDiklatId' => $lastId,
                    'berkasPendaftaranId' => $sub['berkasPendaftaranId'],
            ));
        }
        
		return $this->success('DATA_CREATED');
	}

	public function updateData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

        $id = $this->request('id');
        $jenisDiklat = $this->request('jenisDiklat');
        $namaSingkat = $this->request('namaSingkat');
        $listBerkasPendaftaran = $this->request('listBerkasPendaftaran');
        
        $error = $this->checkRequest();

        $rs = $this->find($id);
        
        if(!$rs)
            return $this->failed('DATA_NOT_FOUND');

        $last_jenisDiklat = $rs['jenisDiklat'];

        if($jenisDiklat != $last_jenisDiklat) {
            $rs = $this->find('jenisDiklat', $jenisDiklat);

            if($rs)
                $error['jenisDiklat'] = $this->string('JENIS_DIKLAT_IS_REG');
        }

		if(count($error) > 0)
			return $this->invalid('PLEASE_CORRECT', $error);
		
		$rs = array(
				'jenisDiklat' => $jenisDiklat,
				'namaSingkat' => $namaSingkat,
		);

        $this->update($id, $rs);

        $mSubMenu = $this->model('tb_jenis_diklat_berkas');
        $mSubMenu->delete('jenisDiklatId', $id);

        foreach($listBerkasPendaftaran as $sub) {
            $mSubMenu->insert(array(
                    'jenisDiklatId' => $id,
                    'berkasPendaftaranId' => $sub['berkasPendaftaranId'],
            ));
        }
            
		return $this->success('DATA_UPDATED');
	}
	
	public function moveData($params = array())
	{
		if(!$this->setSettings(array(
				'authen' => 'user',
				'requestSource' => $params,
		)))
			return $this->denied();
	
		$idFrom = $this->request('idFrom');
		$idTo = $this->request('idTo');
	
		$rsFrom = $this->find($idFrom);
		$rsTo = $this->find($idTo);
	
		$positionFrom = $rsFrom['position'];
		$positionTo = $rsTo['position'];
	
		$this->update($idFrom, array('position' => $positionTo));
		$this->update($idTo, array('position' => $positionFrom));
	
		return $this->success('DATA_UPDATED');
	}

	public function deleteData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

		$id = $this->request('id');
        $rs = $this->find($id);

		$this->delete($id);
		return $this->success('DATA_DELETED');
	}

	public function multipleDeleteData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

            $multipleId = $this->request('multipleId');
            $mItems = $this->model('tb_diklat');
            $error = array();
            $i = 0;

        foreach($multipleId as $id){
            $rs = $this->find($id);
            
            $i++;
            $rsItems = $mItems->select("where jenisDiklatId = ?", array($id));
            if($rsItems){
                if(count($error) == 0)
                    $error['0'] = 'Data dibawah ini tidak bisa di hapus karena terkait dengan Data lain';
                
                $rs = $this->find($id);
                $error[$i] = $rs['jenisDiklat'];        
            }

            if(!$rsItems ){
                $this->delete($id);
            }

        }

        if(count($error) > 0){
        return $this->failed('PLEASE_CORRECT', $error);
        exit;
        }else{
        return $this->success('DATA_DELETED');
        }
	}
    
    
}
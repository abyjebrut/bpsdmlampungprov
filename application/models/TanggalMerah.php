<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class TanggalMerah extends MY_Model {
    protected $table = 'tb_tanggal_merah';
    protected $view = 'vi_tanggal_merah';
    
    public function readData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();

        // Order

        $orderBy = $this->request('orderBy');
        $reverse = $this->request('reverse');

        $orderStmt = '';

        if (!empty($orderBy)) {
            $orderStmt = "order by $orderBy";

            if (!empty($reverse)) {
                if ($reverse == 1)
                    $orderStmt .= ' desc';
            }
        }

        // Limit
        $page = $this->request('page');
		$count = $this->request('count');

        $limitStmt = '';

      	$rowsCount = $this->getRowsCount($orderStmt);
        
      	$pageCount = 1;

        if (!empty($page) && !empty($count)) {
            $row = ($page * $count) - $count;
            $limitStmt = "limit $row, $count";

            $pageCount = ceil($rowsCount / $count);

            if($pageCount < 1)
                $pageCount = 1;
        }

        // Query
        
        $tahun = $this->request('tahun');
	      $rs = $this->select("where year(_tanggalMulai) = ? ".$orderStmt.' '.$limitStmt, array($tahun));
    
        $extra = array(
            'rowsCount' => $rowsCount,
            'pageCount' => $pageCount,
        );

		return $this->success('DATA_READ', $rs, $extra);
	}

    public function filterData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();
        
        // Query

        $field = $this->request('field');
        $keyword = $this->request('keyword');

        $queryStmt = "where $field like ?";
        $rowsCount = $this->getRowsCount($queryStmt, array("$keyword%"));

        // Limit

        $page = $this->request('page');
		$count = $this->request('count');

        $limitStmt = '';
        $pageCount = 1;

        if (!empty($page) && !empty($count)) {
            $row = ($page * $count) - $count;
            $limitStmt = "limit $row, $count";

            $pageCount = ceil($rowsCount / $count);

            if($pageCount < 1)
                $pageCount = 1;
        }

        $rs = $this->select($queryStmt, array("$keyword%"));

        $extra = array(
            'rowsCount' => $rowsCount,
            'pageCount' => $pageCount,
        );

		return $this->success('DATA_READ', $rs, $extra);
	}

	public function detailData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();

		$id = $this->request('id');
        
        // Query
        
		$rs = $this->find($id);

        if($rs)
            return $this->success('DATA_READ', $rs);
        else
            return $this->failed('DATA_NOT_FOUND');
	}

    private function checkRequest() {
        $tanggalMerah = $this->request('tanggalMerah');

		$error = array();
		
		if(empty($tanggalMerah))
			$error['tanggalMerah'] = $this->string('Tanggal Merah diperlukan');

        return $error;
    }

	public function createData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

		$error = $this->checkRequest();

		$tanggalMerah = $this->request('tanggalMerah');
		$tanggalMulai = $this->request('tanggalMulai');
		$tanggalSelesai = $this->request('tanggalSelesai');
        
        $rs = $this->select("where (_tanggalMulai between ? and ?) OR  (_tanggalSelesai between ? and ?) or (_tanggalMulai <= ? and _tanggalSelesai >= ?)", array($tanggalMulai,$tanggalSelesai,$tanggalMulai,$tanggalSelesai,$tanggalMulai,$tanggalSelesai));
        
		if($rs)
            $error['tanggalMerah'] = $this->string('Tanggal Merah sudah terdaftar');

		if(count($error) > 0)
			return $this->invalid('PLEASE_CORRECT', $error);

		$rs = array(
            'tanggalMerah' => $tanggalMerah,
            'tanggalMulai' => $tanggalMulai,
            'tanggalSelesai' => $tanggalSelesai,
        );

        $this->insert($rs);
        $lastId = $this->getLastInsertId();
        $mDetail = $this->model('tb_tanggal_merah_detail');
        
        $Variable1 = strtotime($tanggalMulai); 
        $Variable2 = strtotime($tanggalSelesai); 

        for ($currentDate = $Variable1; $currentDate <= $Variable2;  
                                $currentDate += (86400)) {            
            $Store = date('Y-m-d', $currentDate); 
            $mDetail->insert(array(
                'tanggalMerahId' => $lastId,
                'tanggal' => $Store,
            ));
        } 

            
		return $this->success('DATA_CREATED');
	}

	public function updateData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

        $id = $this->request('id');
        $tanggalMerah = $this->request('tanggalMerah');
		$tanggalMulai = $this->request('tanggalMulai');
		$tanggalSelesai = $this->request('tanggalSelesai');
		
        $error = $this->checkRequest();

        $rs = $this->find($id);
        
        if(!$rs)
            return $this->failed('DATA_NOT_FOUND');

        $last_tanggalMulai = $rs['_tanggalMulai'];
        $last_tanggalSelesai = $rs['_tanggalSelesai'];

        if ($tanggalMulai != $last_tanggalMulai || $tanggalSelesai != $last_tanggalSelesai) {
            $rs = array(
                'tanggalMerah' => $tanggalMerah,
                'tanggalMulai' => '0000-00-00',
                'tanggalSelesai' => '0000-00-00',
            );
    
            $this->update($id, $rs);

            $rs = $this->select("where (_tanggalMulai between ? and ?) OR  (_tanggalSelesai between ? and ?) or (_tanggalMulai <= ? and _tanggalSelesai >= ?)", array($tanggalMulai,$tanggalSelesai,$tanggalMulai,$tanggalSelesai,$tanggalMulai,$tanggalSelesai));
        
            if($rs)
                $error['tanggalMerah'] = $this->string('Tanggal Merah sudah terdaftar');

            $rs = array(
                'tanggalMerah' => $tanggalMerah,
                'tanggalMulai' => $last_tanggalMulai,
                'tanggalSelesai' => $last_tanggalSelesai,
            );
    
            $this->update($id, $rs);

        }

		if(count($error) > 0)
			return $this->invalid('PLEASE_CORRECT', $error);
		
		$rs = array(
            'tanggalMerah' => $tanggalMerah,
            'tanggalMulai' => $tanggalMulai,
            'tanggalSelesai' => $tanggalSelesai,
		);

        $this->update($id, $rs);
        $mDetail = $this->model('tb_tanggal_merah_detail');
        $mDetail->delete('tanggalMerahId', $id);
        $Variable1 = strtotime($tanggalMulai); 
        $Variable2 = strtotime($tanggalSelesai); 

        for ($currentDate = $Variable1; $currentDate <= $Variable2;  
                                $currentDate += (86400)) {            
            $Store = date('Y-m-d', $currentDate); 
            $mDetail->insert(array(
                'tanggalMerahId' => $id,
                'tanggal' => $Store,
            ));
        } 
		return $this->success('DATA_UPDATED');
	}
	
	public function moveData($params = array())
	{
		if(!$this->setSettings(array(
				'authen' => 'user',
				'requestSource' => $params,
		)))
			return $this->denied();
	
		$idFrom = $this->request('idFrom');
		$idTo = $this->request('idTo');
	
		$rsFrom = $this->find($idFrom);
		$rsTo = $this->find($idTo);
	
		$positionFrom = $rsFrom['position'];
		$positionTo = $rsTo['position'];
	
		$this->update($idFrom, array('position' => $positionTo));
		$this->update($idTo, array('position' => $positionFrom));
	
		return $this->success('DATA_UPDATED');
	}

	public function deleteData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

		$id = $this->request('id');
        $rs = $this->find($id);

		$this->delete($id);
		return $this->success('DATA_DELETED');
	}

	public function multipleDeleteData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

            $multipleId = $this->request('multipleId');
            $mDetail = $this->model('tb_tanggal_merah_detail');

        foreach($multipleId as $id){
            $mDetail->delete('tanggalMerahId', $id);
            $this->delete($id);
        }

        return $this->success('DATA_DELETED');
        
	}
    
    
}

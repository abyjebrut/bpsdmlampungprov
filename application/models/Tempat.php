<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tempat extends MY_Model {
    protected $table = 'tb_tempat';
    protected $view = 'vi_tempat';
    
    public function readData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();

        // order

        $orderBy = $this->request('orderBy');
        $reverse = $this->request('reverse');

        $orderStmt = '';

        if (!empty($orderBy)) {
            $orderStmt = "order by $orderBy";

            if (!empty($reverse)) {
                if ($reverse == 1)
                    $orderStmt .= ' desc';
            }
        }

        // Limit

        $page = $this->request('page');
		$count = $this->request('count');

        $limitStmt = '';

        $rowsCount = $this->getRowsCount();
        $pageCount = 1;

        if (!empty($page) && !empty($count)) {
            $row = ($page * $count) - $count;
            $limitStmt = "limit $row, $count";

            $pageCount = ceil($rowsCount / $count);

            if($pageCount < 1)
                $pageCount = 1;
        }

        // query

        $lokasiId = $this->request('lokasiId');

        if(!empty($lokasiId))
            $rs = $this->select("where lokasiId = ? ".$orderStmt.' '.$limitStmt, array($lokasiId));
        else
            $rs = $this->select($orderStmt.' '.$limitStmt);

		$extra = array(
            'rowsCount' => $rowsCount,
            'pageCount' => $pageCount,
        );

		return $this->success('DATA_READ', $rs, $extra);
	}

    public function filterData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();
        // query

        $field = $this->request('field');
        $keyword = $this->request('keyword');

        $queryStmt = "where $field like ?";
        $rowsCount = $this->getRowsCount($queryStmt, array("$keyword%"));

        // Limit

        $page = $this->request('page');
		$count = $this->request('count');

        $limitStmt = '';
        $pageCount = 1;

        if (!empty($page) && !empty($count)) {
            $row = ($page * $count) - $count;
            $limitStmt = "limit $row, $count";

            $pageCount = ceil($rowsCount / $count);

            if($pageCount < 1)
                $pageCount = 1;
        }

        $rs = $this->select($queryStmt, array("$keyword%"));

        $extra = array(
            'rowsCount' => $rowsCount,
            'pageCount' => $pageCount,
        );

		return $this->success('DATA_READ', $rs, $extra);
	}

	public function detailData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();

		$id = $this->request('id');
        
        // Query
        
		$rs = $this->find($id);

        if($rs)
            return $this->success('DATA_READ', $rs);
        else
            return $this->failed('DATA_NOT_FOUND');
	}

    private function checkRequest() {
        $lokasiId = $this->request('lokasiId');
        $tempat = $this->request('tempat');

		$error = array();

        if(empty($lokasiId))
			$error['lokasiId'] = $this->string('Lokasi diperlukan');

		if(empty($tempat))
			$error['tempat'] = $this->string('Tempat diperlukan');

        return $error;
    }

	public function createData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

		$error = $this->checkRequest();

        $lokasiId = $this->request('lokasiId');
        $tempat = $this->request('tempat');

        $rs = $this->single('where lokasiId = ? and tempat = ?', array(
            $lokasiId,
            $tempat,
        ));

		if($rs)
            $error['tempat'] = $this->string('Tempat sudah terdaftar');

		if(count($error) > 0)
			return $this->invalid('PLEASE_CORRECT', $error);

		$rs = array(
            'lokasiId' => $lokasiId,
            'tempat' => $tempat,
        );

		$this->insert($rs);
		return $this->success('DATA_CREATED');
	}

	public function updateData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

        $id = $this->request('id');
        $lokasiId = $this->request('lokasiId');
        $tempat = $this->request('tempat');

		$error = $this->checkRequest();
        
        $rs = $this->find($id);
        if(!$rs)
            return $this->failed('DATA_NOT_FOUND');

        $last_tempat = $rs['tempat'];
        $last_lokasiId = $rs['lokasiId'];

        if($tempat != $last_tempat || $lokasiId != $last_lokasiId) {
            $rs = $this->single('where lokasiId = ? and tempat = ?', array(
            $lokasiId,
            $tempat,
        ));

            if($rs)
                $error['tempat'] = $this->string('Tempat sudah terdaftar');
        }

		if(count($error) > 0)
			return $this->invalid('PLEASE_CORRECT', $error);

		$rs = array(
				'lokasiId' => $lokasiId,
                'tempat' => $tempat,
		);

		$this->update($id, $rs);
		return $this->success('DATA_UPDATED');
	}

	public function deleteData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

		$id = $this->request('id');
        $rs = $this->find($id);

		$this->delete($id);
		return $this->success('DATA_DELETED');
	}

	public function multipleDeleteData($params = array()) 
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

		$multipleId = $this->request('multipleId');

		foreach($multipleId as $id)
            $this->delete($id);

		return $this->success('DATA_DELETED');
	}
    
    
    
    

}
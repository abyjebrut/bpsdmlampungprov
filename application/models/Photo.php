<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Photo extends MY_Model {
    protected $table = 'tb_photo';
    protected $view = 'vi_photo';

    public function readData($params = array())
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();

        // Order

        $orderBy = $this->request('orderBy');
        $reverse = $this->request('reverse');

        $orderStmt = '';

        if (!empty($orderBy)) {
            $orderStmt = "order by $orderBy";

            if (!empty($reverse)) {
                if ($reverse == 1)
                    $orderStmt .= ' desc';
            }
        }

        // Limit

		$categoryId = $this->request('categoryId');
        $publishStatusId = $this->request('publishStatusId');
        $page = $this->request('page');
		    $count = $this->request('count');

        $limitStmt = '';

        if (!empty($categoryId) && !empty($publishStatusId))
            $rowsCount = $this->getRowsCount("where photoCategoryId = ? and publishStatusId = ? ", array($categoryId, $publishStatusId));
        elseif (!empty($categoryId))
            $rowsCount = $this->getRowsCount("where photoCategoryId = ? ", array($categoryId));
        elseif (!empty($publishStatusId))
            $rowsCount = $this->getRowsCount("where publishStatusId = ? ", array($publishStatusId));
        else
            $rowsCount = $this->getRowsCount($orderStmt);
        
        $pageCount = 1;

        if (!empty($page) && !empty($count)) {
            $row = ($page * $count) - $count;
            $limitStmt = "limit $row, $count";

            $pageCount = ceil($rowsCount / $count);

            if($pageCount < 1)
                $pageCount = 1;
        }

        // Query

		if (!empty($categoryId) && !empty($publishStatusId))
            $rs = $this->select("where photoCategoryId = ? and publishStatusId = ? ".$orderStmt.' '.$limitStmt, array($categoryId, $publishStatusId));
        elseif (!empty($categoryId))
            $rs = $this->select("where photoCategoryId = ? ".$orderStmt.' '.$limitStmt, array($categoryId));
        elseif (!empty($publishStatusId))
            $rs = $this->select("where publishStatusId = ? ".$orderStmt.' '.$limitStmt, array($publishStatusId));
        else
            $rs = $this->select($orderStmt.' '.$limitStmt);
        
        $extra = array(
            'rowsCount' => $rowsCount,
            'pageCount' => $pageCount,
        );

		return $this->success('DATA_READ', $rs, $extra);
	}

    public function filterData($params = array())
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();

        // Query

        $field = $this->request('field');
        $keyword = $this->request('keyword');

        $queryStmt = "where $field like ?";
        $rowsCount = $this->getRowsCount($queryStmt, array("$keyword%"));

        // Limit

        $page = $this->request('page');
		$count = $this->request('count');

        $limitStmt = '';
        $pageCount = 1;

        if (!empty($page) && !empty($count)) {
            $row = ($page * $count) - $count;
            $limitStmt = "limit $row, $count";

            $pageCount = ceil($rowsCount / $count);

            if($pageCount < 1)
                $pageCount = 1;
        }

        $rs = $this->select($queryStmt, array("$keyword%"));

        $extra = array(
            'rowsCount' => $rowsCount,
            'pageCount' => $pageCount,
        );

		return $this->success('DATA_READ', $rs, $extra);
	}

	public function detailData($params = array())
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();

		$id = $this->request('id');

        // Query

		$rs = $this->find($id);

        if($rs)
            return $this->success('DATA_READ', $rs);
        else
            return $this->failed('DATA_NOT_FOUND');
	}

    private function checkRequest() {
        $title = $this->request('title');
        $publishStatusId = $this->request('publishStatusId');

		$error = array();

		if(empty($title))
			$error['title'] = $this->string('TITLE_REQUIRED');

		if(empty($publishStatusId))
			$error['publishStatusId'] = $this->string('PUBLISH_STATUS_REQUIRED');

        return $error;
    }

	public function createData($params = array())
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

		$error = $this->checkRequest();

		$photoCategoryId = $this->request('photoCategoryId');
        $title = $this->request('title');
        $title_sl = $this->request('titleSl');
        $title_el = $this->request('titleEl');
        $coverPhoto = $this->request('coverPhoto');
        $description = $this->request('description');
        $description_sl = $this->request('descriptionSl');
        $description_el = $this->request('descriptionEl');
        $photos = $this->request('photos');
        $publishStatusId = $this->request('publishStatusId');

        $rs = $this->find('title', $title);

		if($rs)
            $error['title'] = $this->string('TITLE_IS_REG');

		if(count($error) > 0)
			return $this->invalid('PLEASE_CORRECT', $error);

		if(!empty($coverPhoto)) {
			if(!$this->moveToArchive($coverPhoto))
				return $this->failed('FILE_UPLOAD_FAILED');
		}

		if(!empty($photos)) {
			foreach($photos as $photo) {
				if(!$this->moveToArchive($photo))
					return $this->failed('UPLOAD_FAILED');
			}
		}

		if($photos)
			$photos = implode(',', $photos);

		$rs = array(
			'photoCategoryId' => $photoCategoryId,
            'title' => $title,
			'title_sl' => $title_sl,
			'title_el' => $title_el,
			'coverPhoto' => $coverPhoto,
			'description' => $description,
			'description_sl' => $description_sl,
			'description_el' => $description_el,
			'photos' => $photos,
			'publishStatusId' => $publishStatusId,
        );

		$this->insert($rs);
		// position
		$lastId = $this->getLastInsertId();
		$this->update($lastId, array('position' => $lastId));
		return $this->success('DATA_CREATED');
	}

	public function updateData($params = array())
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

        $id = $this->request('id');
        $photoCategoryId = $this->request('photoCategoryId');
        $title = $this->request('title');
        $title_sl = $this->request('titleSl');
        $title_el = $this->request('titleEl');
        $coverPhoto = $this->request('coverPhoto');
        $description = $this->request('description');
        $description_sl = $this->request('descriptionSl');
        $description_el = $this->request('descriptionEl');
        $photos = $this->request('photos');
        $publishStatusId = $this->request('publishStatusId');

        $error = $this->checkRequest();

        $rs = $this->find($id);

        if(!empty($coverPhoto)) {
        	if($coverPhoto != $rs['coverPhoto']) {

        		$last_coverPhoto = $rs['coverPhoto'];
        		$this->deleteArchive($last_coverPhoto);

        		if(!$this->moveToArchive($coverPhoto))
        			return $this->failed('FILE_UPLOAD_FAILED');
        	}
        }


        if(!empty($photos)) {

        	$lastPhoto = $rs['photos'];
        	$explodedLastPhoto = explode(',', $lastPhoto);

        	foreach($photos as $photo) {
        		$imageFound = false;

        		if(!empty($lastPhoto)) {
        			foreach($explodedLastPhoto as $lastPhoto) {
        				if($photo == $lastPhoto)
        					$imageFound = true;
        			}
        		}

        		if(!$imageFound) {
        			if(!$this->moveToArchive($photo))
        				return $this->failed('UPLOAD_FAILED');
        		}
        	}

        }

        if(!$rs)
            return $this->failed('DATA_NOT_FOUND');

        $last_title = $rs['title'];

        if($title != $last_title) {
            $rs = $this->find('title', $title);

            if($rs)
                $error['title'] = $this->string('TITLE_IS_REG');
        }

		if(count($error) > 0)
			return $this->invalid('PLEASE_CORRECT', $error);

		if ($photos)
			$photos = implode(',', $photos);

		$rs = array(
			'photoCategoryId' => $photoCategoryId,
			'title' => $title,
			'title_sl' => $title_sl,
			'title_el' => $title_el,
			'coverPhoto' => $coverPhoto,
			'description' => $description,
			'description_sl' => $description_sl,
			'description_el' => $description_el,
			'photos' => $photos,
			'publishStatusId' => $publishStatusId,
		);

		$this->update($id, $rs);
		return $this->success('DATA_UPDATED');
	}

	public function moveData($params = array())
	{
		if(!$this->setSettings(array(
				'authen' => 'user',
				'requestSource' => $params,
		)))
			return $this->denied();

		$idFrom = $this->request('idFrom');
		$idTo = $this->request('idTo');

		$rsFrom = $this->find($idFrom);
		$rsTo = $this->find($idTo);

		$positionFrom = $rsFrom['position'];
		$positionTo = $rsTo['position'];

		$this->update($idFrom, array('position' => $positionTo));
		$this->update($idTo, array('position' => $positionFrom));

		return $this->success('DATA_UPDATED');
	}

	public function deleteData($params = array())
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

		$id = $this->request('id');
        $rs = $this->find($id);

		$this->delete($id);
		return $this->success('DATA_DELETED');
	}

	public function multipleDeleteData($params = array())
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

		$multipleId = $this->request('multipleId');

		foreach($multipleId as $id){
			$rs = $this->find($id);

			if(!empty($rs['coverPhoto']))
				$this->deleteArchive($rs['coverPhoto']);

			$photos = $rs['photos'];

			if(!empty($photos)) {
				$photos = explode(',', $photos);

				foreach($photos as $photo)
					$this->deleteArchive($photo);
			}
			$this->delete($id);
		}

		return $this->success('DATA_DELETED');
	}

}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Comments extends MY_Model {
    protected $table = 'tb_comments';
    protected $view = 'vi_comments';

    public function readData($params = array())
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();

        // Order

        $orderBy = $this->request('orderBy');
        $reverse = $this->request('reverse');

        $orderStmt = '';

        if (!empty($orderBy)) {
            $orderStmt = "order by $orderBy";

            if (!empty($reverse)) {
                if ($reverse == 1)
                    $orderStmt .= ' desc';
            }
        }

        // Limit

        $commentsAtId = $this->request('commentsAtId');
        $postsId = $this->request('postsId');
        $publishStatusId = $this->request('publishStatusId');
        $page = $this->request('page');
		    $count = $this->request('count');

        $limitStmt = '';

        if (!empty($commentsAtId) && !empty($postsId))
            $rowsCount = $this->getRowsCount("where commentsAtId = ? and postsId = ? and publishStatusId = 2 ", array($commentsAtId, $postsId));
        elseif (!empty($commentsAtId) && !empty($publishStatusId))
            $rowsCount = $this->getRowsCount("where commentsAtId = ? and publishStatusId = ? ", array($commentsAtId, $publishStatusId));
        elseif (!empty($commentsAtId))
            $rowsCount = $this->getRowsCount("where commentsAtId = ? ", array($commentsAtId));
        elseif (!empty($publishStatusId))
            $rowsCount = $this->getRowsCount("where publishStatusId = ? ", array($publishStatusId));
        else
            $rowsCount = $this->getRowsCount($orderStmt);

        $pageCount = 1;

        if (!empty($page) && !empty($count)) {
            $row = ($page * $count) - $count;
            $limitStmt = "limit $row, $count";

            $pageCount = ceil($rowsCount / $count);

            if($pageCount < 1)
                $pageCount = 1;
        }

        // Query

        if (!empty($commentsAtId) && !empty($postsId))
            $rs = $this->select("where commentsAtId = ? and postsId = ? ".$orderStmt.' '.$limitStmt, array($commentsAtId, $postsId));
        elseif (!empty($commentsAtId) && !empty($publishStatusId))
            $rs = $this->select("where commentsAtId = ? and publishStatusId = ? ".$orderStmt.' '.$limitStmt, array($commentsAtId, $publishStatusId));
        elseif (!empty($commentsAtId))
            $rs = $this->select("where commentsAtId = ? ".$orderStmt.' '.$limitStmt, array($commentsAtId));
        elseif (!empty($publishStatusId))
            $rs = $this->select("where publishStatusId = ? ".$orderStmt.' '.$limitStmt, array($publishStatusId));
        else
            $rs = $this->select($orderStmt.' '.$limitStmt);

        $extra = array(
            'rowsCount' => $rowsCount,
            'pageCount' => $pageCount,
        );

		    return $this->success('DATA_READ', $rs, $extra);
	}

    public function filterData($params = array())
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();

        // Query

        $field = $this->request('field');
        $keyword = $this->request('keyword');

        $queryStmt = "where $field like ?";
        $rowsCount = $this->getRowsCount($queryStmt, array("$keyword%"));

        // Limit

        $page = $this->request('page');
		$count = $this->request('count');

        $limitStmt = '';
        $pageCount = 1;

        if (!empty($page) && !empty($count)) {
            $row = ($page * $count) - $count;
            $limitStmt = "limit $row, $count";

            $pageCount = ceil($rowsCount / $count);

            if($pageCount < 1)
                $pageCount = 1;
        }

        $rs = $this->select($queryStmt, array("$keyword%"));

        $extra = array(
            'rowsCount' => $rowsCount,
            'pageCount' => $pageCount,
        );

		return $this->success('DATA_READ', $rs, $extra);
	}

	public function detailData($params = array())
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();

		$id = $this->request('id');

        // Query

		$rs = $this->find($id);

        if($rs)
            return $this->success('DATA_READ', $rs);
        else
            return $this->failed('DATA_NOT_FOUND');
	}

    private function checkRequest() {
        $author = $this->request('author');
        $publishStatusId = $this->request('publishStatusId');
        $email = $this->request('email');
        $comment = $this->request('comment');

		$error = array();

		if(empty($author))
			$error['author'] = $this->string('AUTHOR_REQUIRED');

// 		if(empty($publishStatusId))
// 			$error['publishStatusId'] = $this->string('PUBLISH_STATUS_REQUIRED');

		if(empty($comment))
			$error['comment'] = $this->string('COMMENT_REQUIRED');

		// if(empty($email))
		// 	$error['email'] = $this->string('EMAIL_REQUIRED');
		// else {
		// 	           if(!$this->valid->email($email))
		// 		               $error['email'] = $this->string('INVALID_EMAIL');
		// }

        return $error;
    }

	public function createData($params = array())
    {
        if(!$this->setSettings(array(
            'authen' => 'free',
            'requestSource' => $params,
        )))
            return $this->denied();

		$error = $this->checkRequest();

		$author = $this->request('author');
		$email = $this->request('email');
		$url = $this->request('url');
		$comment = $this->request('comment');
		$commentsAtId = $this->request('commentsAtId');
        $postsId = $this->request('postsId');
        $publishStatusId = $this->request('publishStatusId');

		if(count($error) > 0)
			return $this->invalid('PLEASE_CORRECT', $error);

		$rs = array(
				'author' => $author,
				'email' => $email,
				'url' => $url,
				'comment' => $comment,
				'commentsAtId' => $commentsAtId,
				'postsId' => $postsId,
				'postedOn' => date('Y-m-d H:i:s'),
				'publishStatusId' => $publishStatusId,
        );

		$this->insert($rs);

		return $this->success('DATA_CREATED');
	}

	public function updateData($params = array())
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

        $id = $this->request('id');
        $author = $this->request('author');
		$email = $this->request('email');
		$url = $this->request('url');
		$comment = $this->request('comment');
		$commentsAtId = $this->request('commentsAtId');
        $postsId = $this->request('postsId');
        $publishStatusId = $this->request('publishStatusId');

        $error = $this->checkRequest();

        $rs = $this->find($id);

        if(!$rs)
            return $this->failed('DATA_NOT_FOUND');

		if(count($error) > 0)
			return $this->invalid('PLEASE_CORRECT', $error);

		$rs = array(
				'author' => $author,
				'email' => $email,
				'url' => $url,
				'comment' => $comment,
				'commentsAtId' => $commentsAtId,
// 				'postsId' => $postsId,
				'publishStatusId' => $publishStatusId,
		);

		$this->update($id, $rs);
		return $this->success('DATA_UPDATED');
	}

	public function deleteData($params = array())
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

		$id = $this->request('id');
        $rs = $this->find($id);

		$this->delete($id);
		return $this->success('DATA_DELETED');
	}

	public function multipleDeleteData($params = array())
    {
        if(!$this->setSettings(array(
            'authen' => 'user',
            'requestSource' => $params,
        )))
            return $this->denied();

		$multipleId = $this->request('multipleId');

    	foreach($multipleId as $id){
			$this->delete($id);
		}

		return $this->success('DATA_DELETED');
	}

}

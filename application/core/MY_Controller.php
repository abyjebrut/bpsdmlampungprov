<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

  private $settings;

  public function __construct()
  {
    parent::__construct();

    $this->setSettings(array(
      'template' => 'Main',
    ));

    session_start();
  }

  // Begin Settings methods

  protected function setSettings($settings = array())
  {
    $settings['title'] = $this->config->item('title');

    foreach($settings as $k => $v)
      $this->settings[$k] = $v;
  }

  protected function getSetting($setting = '')
  {
    return $this->settings[$setting];
  }

  protected function getSettings($setting = '')
  {
    return $this->settings;
  }

  // End Settings methods

  // Begin Authen methods

  protected function encrypt($text = '', $salt = '')
  {
    if (empty($text))
      $text = uniqid();

    $encryptText = sha1($text.$salt);
    $encryptText = strtoupper($encryptText);

    return $encryptText;
  }

  // End Authen methods

  // Begin API Operation methods

  protected function json($data)
  {
    // $this->output->set_header('Access-Control-Allow-Origin: *');
    // $this->output->set_content_type('application/json');
    // $this->output->set_output(json_encode($data));

    header('Access-Control-Allow-Origin: *');
		header('Content-Type: application/json');
    echo json_encode($data);

    exit;
  }

  protected function string($variable)
  {
    $strings = $this->config->item('strings');
    $lang = $this->input->post('lang');

    if(isset($strings[$lang])) {
      $strings = $strings[$lang];

      if (isset($strings[$variable]))
        return $strings[$variable];
      else
        return $variable;
    }
    else
      return $variable;
  }

  private function result($status, $message = '', $data = array(), $extra = array())
  {
    $result = array(
      'status' => $status,
      'message' => $this->string($message),
      'data' => $data,
      'extra' => $extra,
    );

    $this->json($result);
  }

  protected function success($message = '', $data = array(), $extra = array())
  {
    $status = 'success';
    return $this->result($status, $message, $data, $extra);
  }

  protected function failed($message = '', $data = array(), $extra = array())
  {
    $status = 'failed';
    return $this->result($status, $message, $data, $extra);
  }

  protected function invalid($message, $data = array(), $extra = array())
  {
    $status = 'invalid';
    return $this->result($status, $message, $data, $extra);
  }

  protected function denied($data = array(), $extra = array())
  {
    $status = 'denied';
    $message = 'ACCESS_DENIED';
    return $this->result($status, $message, $data, $extra);
  }

  protected function callApi($api, $params = array(), $dualResult = false) {

    $explodedApi = explode('/', $api);
    $className = $explodedApi[0];
    $methodName = $explodedApi[1];

    eval('$classFound = isset($this->'.$className.');');
    if (!$classFound)
      $this->load->model($className);

    eval('$result = $this->'.$className.'->'.$methodName.'($params);');
    $data = $result['data'];
    $extra = $result['extra'];

    if (!$dualResult)
      return $data;
    else
      return array('data' => $data, 'extra' => $extra);
  }

  // End API Operation methods

  // Begin File Operation methods

  protected function deleteArchive($arcFileName) {

    if(empty($arcFileName))
      return;

    $arcFilePath = "asset/archive/$arcFileName";

    if(file_exists($arcFilePath))
      unlink($arcFilePath);
  }

  protected function moveToArchive($tmpFileName) {
//        $tmpFilePath = parse_url($fileUrl, PHP_URL_PATH);
//        $pathInfo = pathinfo($tmpFilePath);
//
//        $tmpFileName = $pathInfo['basename'];
//        $tmpFilePath = "asset/tmp/$tmpFileName";
    if(empty($tmpFileName))
      return;

    $toPath = 'asset/archive';

    $tmpFilePath = 'asset/tmp/'.$tmpFileName;
    $arcFilePath = $toPath.'/'.$tmpFileName;

    if(!file_exists($tmpFilePath))
      return false;

    if(copy($tmpFilePath, $arcFilePath)) {
      unlink($tmpFilePath);
      return true;
    }
    else
      return false;
  }

  // End File Operation methods

  // Latest: Templating methods

  protected function render($page, $viewData = array())
  {
    $headData = $this->getHeadData();
    $headData['settings'] = $this->getSettings();

    $footData = $this->getFootData();
    $footData['settings'] = $this->getSettings();

    $viewData['settings'] = $this->getSettings();

    $allData = array(
      'settings' => $this->getSettings(),
      'page' => 'pages/'.$page,
      'headData' => $headData,
      'footData' => $footData,
      'viewData' => $viewData,
      );

    $template = $this->getSetting('template');
    $this->load->view($template, $allData);
  }

  private function getHeadData() {

    // $rowHeadContent = $this->callApi('HeadContent/detailData');
    $rowSocialNetwork = $this->callApi('SocialNetwork/detailData');

    $rsMenu = $this->callApi('Menu/readData', array(
      'publishStatusId' => 2,
      'orderBy' => 'position'
    ));

    return array(
      // 'rowHeadContent' => $rowHeadContent,
      'rowSocialNetwork' => $rowSocialNetwork,
      'rsMenu' => $rsMenu,
    );
  }

  private function getFootData() {

    $rowFootContent = $this->callApi('FootContent/detailData');

    return array(
      'rowFootContent' => $rowFootContent,
    );
  }

}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Model extends CI_Model {

  private $apiRequest = false;
  private $settings;

  protected $table = '';
  protected $view = '';

  protected $primaryKey = 'id';

  public function __construct()
  {
    parent::__construct();

    if (empty($this->view))
      $this->view = $this->table;
  }

  // Begin Settings methods

  public function setApiRequest($apiRequest)
  {
    $this->apiRequest = $apiRequest;
  }
  
  protected function setSettings($settings = array())
  {
    $this->settings = $settings;

    if ($this->apiRequest)
      return $this->authen($this->settings['authen']);
    else
      return true;
  }

  // End Settings methods

  // Begin Authen methods

  protected function encrypt($text = '', $salt = '')
  {
    if (empty($text))
      $text = uniqid();

    $encryptText = sha1($text.$salt);
    $encryptText = strtoupper($encryptText);

    return $encryptText;
  }

  private function authen($type)
  {
    $key = $this->input->post('key');
    $auth = $this->input->post('auth');

    if (empty($key))
      return false;

    if ($key != $this->config->item('apiKey'))
      return false;

    if ($type == 'free')
      return true;

    if (empty($auth))
      return false;

    $mUserLog = $this->model('vi_user_log');
    $rowUserLog = $mUserLog->find('auth', $auth);

    if (empty($rowUserLog))
      return false;

    $userId = $rowUserLog['userId'];
    $stillInsideId = $rowUserLog['stillInsideId'];

    if ($userId == 0)
      $userType = 'admin';
    else
      $userType = 'user';

    // If already logout
    if ($stillInsideId == 2)
      return false;

    switch($type) {
      case 'member':

        if (
        $userType == 'admin' ||
        $userType == 'user' ||
        $userType == 'member'
        )
          return true;

        break;

      case 'user':

        if (
        $userType == 'admin' ||
        $userType == 'user'
        )
          return true;

        break;

      case 'admin':

        if (
        $userType == 'admin'
        )
          return true;

    }

    return false;
  }

  // End Authen methods

  // Begin API Operation methods

  protected function request($variable)
  {
    $requestSource = $this->settings['requestSource'];
    if (count($requestSource) > 0) {

      if (isset($requestSource[$variable]))
        return $requestSource[$variable];
      else
        return '';
    }
    else
      return $this->input->post($variable);
  }

  protected function deleteTemp($arcFileName) {

    if(empty($arcFileName))
        return;

    $arcFilePath = "asset/tmp/$arcFileName";

    if(file_exists($arcFilePath))
        unlink($arcFilePath);
}

protected function moveToTemp($tmpFileName) {
        
  if(empty($tmpFileName))
      return;

  $toPath = 'asset/tmp';

  $tmpFilePath = 'asset/archive/'.$tmpFileName;
  $arcFilePath = $toPath.'/'.$tmpFileName;

  if(!file_exists($tmpFilePath))
      return false;

  if(copy($tmpFilePath, $arcFilePath)) {
      unlink($tmpFilePath);
      return true;
  }
  else
  return false;
}

  protected function string($variable)
  {
    $strings = $this->config->item('strings');
    $lang = $this->request('lang');

    if(isset($strings[$lang])) {
      $strings = $strings[$lang];

      if (isset($strings[$variable]))
        return $strings[$variable];
      else
        return $variable;
    }
    else
      return $variable;
  }

  private function result($status, $message = '', $data = array(), $extra = array())
  {
    $result = array(
      'status' => $status,
      'message' => $this->string($message),
      'data' => $data,
      'extra' => $extra,
    );

    return $result;
  }

  protected function success($message = '', $data = array(), $extra = array())
  {
    $status = 'success';
    return $this->result($status, $message, $data, $extra);
  }

  protected function failed($message = '', $data = array(), $extra = array())
  {
    $status = 'failed';
    return $this->result($status, $message, $data, $extra);
  }

  protected function invalid($message, $data = array(), $extra = array())
  {
    $status = 'invalid';
    return $this->result($status, $message, $data, $extra);
  }

  protected function denied($data = array(), $extra = array())
  {
    $status = 'denied';
    $message = 'ACCESS_DENIED';
    return $this->result($status, $message, $data, $extra);
  }

  // End API Operation methods

  // Begin Database methods

  protected function model($table, $view = '')
  {
    $model = new MY_Model;
    $model->table = $table;

    if (empty($view))
      $model->view = $model->table;
    else
      $model->view = $view;

    return $model;
  }

  protected function execute($sql, $data = array())
  {
    $this->db->query($sql, $data);
  }

  protected function delete($field, $value = '')
  {
    if (empty($value)) {
      $value = $field;
      $field = $this->primaryKey;
    }
    $this->execute('delete from '.$this->table.' where '.$field.' = ?', array($value));
  }

  protected function query($sql, $data = array())
  {
    $query = $this->db->query($sql, $data);
    return $query->result_array();
  }

  protected function scalar($sql, $data = array())
  {
    $result = $this->query($sql, $data);

    if($result)
    $result = $result[0];
    else
    $result = '';

    return $result;
  }

  protected function select($sql = '', $data = array())
  {
    $sql = 'select * from '.$this->view.' '.$sql;
    return $this->query($sql, $data);
  }

  protected function single($sql = '', $data = array())
  {
    $sql = 'select * from '.$this->view.' '.$sql;
    return $this->scalar($sql, $data);
  }

  protected function find($field, $value = '')
  {
    if (empty($value)) {
      $value = $field;
      $field = $this->primaryKey;
    }

    return $this->single('where '.$field.' = ?', array($value));
  }

  protected function getRowsCount($sql = '', $data = array())
  {
    $result = $this->scalar('select count(*) as rowsCount from '.$this->view.' '.$sql, $data);
    $rowsCount = $result['rowsCount'] + 0;

    return $rowsCount;
  }

  protected function insert($data)
  {
    $this->db->insert($this->table, $data);
  }

  protected function getLastInsertId()
  {
    return $this->db->insert_id();
  }

  protected function update($field, $value = '', $data = array())
  {
    if (is_array($value)) {
      $data = $value;
      $value = $field;
      $field = $this->primaryKey;
    }

    $this->db->where($field, $value);
    $this->db->update($this->table, $data);
  }

  // End Database methods

  // Begin File Operation methods

  protected function deleteArchive($arcFileName) {

    if(empty($arcFileName))
    return;

    $arcFilePath = "asset/archive/$arcFileName";

    if(file_exists($arcFilePath))
    unlink($arcFilePath);
  }

  protected function moveToArchive($tmpFileName) {
    //  $tmpFilePath = parse_url($fileUrl, PHP_URL_PATH);
    //  $pathInfo = pathinfo($tmpFilePath);
    //  $tmpFileName = $pathInfo['basename'];
    //  $tmpFilePath = "asset/tmp/$tmpFileName";

    if(empty($tmpFileName))
    return;

    $toPath = 'asset/archive';

    $tmpFilePath = 'asset/tmp/'.$tmpFileName;
    $arcFilePath = $toPath.'/'.$tmpFileName;

    if(!file_exists($tmpFilePath))
    return false;

    if(copy($tmpFilePath, $arcFilePath)) {
      unlink($tmpFilePath);
      return true;
    }
    else
      return false;
  }

  protected function indonesian_date ($timestamp = '', $date_format = 'l, j F Y | H:i', $suffix = '') {
    if (trim ($timestamp) == '')
    {
      $timestamp = time ();
    }
    elseif (!ctype_digit ($timestamp))
    {
      $timestamp = strtotime ($timestamp);
    }
    # remove S (st,nd,rd,th) there are no such things in indonesia :p
    $date_format = preg_replace ("/S/", "", $date_format);
    $pattern = array (
        '/Mon[^day]/','/Tue[^sday]/','/Wed[^nesday]/','/Thu[^rsday]/',

        '/Fri[^day]/','/Sat[^urday]/','/Sun[^day]/','/Monday/','/Tuesday/',
        '/Wednesday/','/Thursday/','/Friday/','/Saturday/','/Sunday/',
        '/Jan[^uary]/','/Feb[^ruary]/','/Mar[^ch]/','/Apr[^il]/','/May/',
        '/Jun[^e]/','/Jul[^y]/','/Aug[^ust]/','/Sep[^tember]/','/Oct[^ober]/',
        '/Nov[^ember]/','/Dec[^ember]/','/January/','/February/','/March/',
        '/April/','/June/','/July/','/August/','/September/','/October/',
        '/November/','/December/',
    );

    $replace = array ( 'Sen','Sel','Rab','Kam','Jum','Sab','Min',
        'Senin','Selasa','Rabu','Kamis','Jumat','Sabtu','Minggu',
        'Jan','Feb','Mar','Apr','Mei','Jun','Jul','Ags','Sep','Okt','Nov','Des',
        'Januari','Februari','Maret','April','Juni','Juli','Agustus','September',
        'Oktober','November','Desember',
    );
    
    $date = date ($date_format, $timestamp);
    $date = preg_replace ($pattern, $replace, $date);
    $date = "{$date} {$suffix}";
    return $date;
  }

  protected function indonesian_date1 ($timestamp = '', $date_format = 'l, j F Y | H:i', $suffix = ' ') {
    if (trim ($timestamp) == '')
    {
      $timestamp = time ();
    }
    elseif (!ctype_digit ($timestamp))
    {
      $timestamp = strtotime ($timestamp);
    }
    # remove S (st,nd,rd,th) there are no such things in indonesia :p
    $date_format = preg_replace ("/S/", "", $date_format);
    $pattern = array (
        '/Mon[^day]/','/Tue[^sday]/','/Wed[^nesday]/','/Thu[^rsday]/',

        '/Fri[^day]/','/Sat[^urday]/','/Sun[^day]/','/Monday/','/Tuesday/',
        '/Wednesday/','/Thursday/','/Friday/','/Saturday/','/Sunday/',
        '/Jan[^uary]/','/Feb[^ruary]/','/Mar[^ch]/','/Apr[^il]/','/May/',
        '/Jun[^e]/','/Jul[^y]/','/Aug[^ust]/','/Sep[^tember]/','/Oct[^ober]/',
        '/Nov[^ember]/','/Dec[^ember]/','/January/','/February/','/March/',
        '/April/','/June/','/July/','/August/','/September/','/October/',
        '/November/','/December/',
    );

    $replace = array ( 'Sen','Sel','Rab','Kam','Jum','Sab','Min',
        'Senin','Selasa','Rabu','Kamis','Jumat','Sabtu','Minggu',
        'Jan','Feb','Mar','Apr','-Mei-','Jun','Jul','Ags','Sep','Okt','Nov','Des',
        '-Jan-','-Feb-','-Mar-','-Apr-','-Jun-','-Jul-','-Ags-','-Sep-',
        '-Okt-','-Nov-','-Des-',
    );
    
    $date = date ($date_format, $timestamp);
    $date = preg_replace ($pattern, $replace, $date);
    $date = "{$date} {$suffix}";
    return $date;
  }

  // End File Operation Methods
}

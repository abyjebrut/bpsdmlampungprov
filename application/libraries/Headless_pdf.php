<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once 'asset/libraries/tcpdf/tcpdf.php';

class Headless_pdf extends TCPDF {

    private $settings = array();

    public function __construct() {
        parent::__construct('P', 'pt', 'A4');
    }

    public function setData($data) {

      $this->data = $data;
    }

    public function Header() {

      // $judul = '';
      // if (isset($this->data['judul']))
      //   $judul = $this->data['judul'];
      //
      // $this->SetTopMargin(140);
      // $this->SetY(30);
      //
      // $this->AddFont('freemono', '', 'asset/lib/tcpdf/fonts/freemono.php');
      // $this->SetFont('freemono', '', 10);
      //
      // $this->writeHTML('');

    }

    public function Footer()
    {
        $this->AddFont($this->settings['fontName'], '', 'asset/libraries/tcpdf/fonts/'.$this->settings['fontName'].'.php');
        $this->SetFont($this->settings['fontName'], '', $this->settings['fontSize']);
        $this->SetY(-15);
        $this->Cell(0, 10,
                    'Page: '.$this->getAliasNumPage().' '.
                    'From: '.$this->getAliasNbPages().' '.
                    'Pages',
                    0,
                    false,
                    'R',
                    0,
                    '',
                    0,
                    false,
                    'T',
                    'M'
                   );
    }

    private function getMainData()
    {
        return array('title' => $this->settings['title']);
    }

    public function setSettings($settings)
    {
        foreach($settings as $k => $v)
            $this->settings[$k] = $v;

        if (!isset($this->settings['template']))
            $this->settings['template'] = 'Main';

        if (!isset($this->settings['title']))
            $this->settings['title'] = '';

        if (!isset($this->settings['pageOrientation']))
            $this->settings['pageOrientation'] = 'P';

        if (!isset($this->settings['pageSize']))
            $this->settings['pageSize'] = 'A4';

        if (!isset($this->settings['fontName']))
            $this->settings['fontName'] = 'dejavusans';

        if (!isset($this->settings['fontSize']))
            $this->settings['fontSize'] = 9;

        $this->setPageOrientation($this->settings['pageOrientation']);

        $this->AddFont($this->settings['fontName'], '', 'asset/libraries/tcpdf/fonts/'.$this->settings['fontName'].'.php');
        $this->SetFont($this->settings['fontName'], '', $this->settings['fontSize']);
        $this->SetTextColor(0, 0, 0);
    }

    protected function string($variable)
    {
        $strings = $this->settings['strings'];
        $lang = $_POST['lang'];

        if(isset($strings[$lang])) {
            $strings = $strings[$lang];

            if (isset($strings[$variable]))
                return $strings[$variable];
            else
                return $variable;
        }
        else
            return $variable;
    }

    private function encrypt($text = '', $salt = '')
    {
        if (empty($text))
            $text = uniqid();

        $encryptText = sha1($text.$salt);
        $encryptText = strtoupper($encryptText);

        return $encryptText;
    }

    public function view($page, $viewData = array())
    {
        extract($viewData);
        require_once $page;
    }

    public function render($page, $viewData = array())
    {
        $template = $this->settings['template'];
        $mainData = $this->getMainData();

        $this->AddPage();

        $templateUrl = "application/views/pdf/$template.php";
        $pageUrl = "application/views/pdf/pages/$page.php";
        $page = $pageUrl;

        ob_start();
        extract($mainData);
        require_once $templateUrl;
        $contents = ob_get_clean();

        $this->writeHTML($contents);

        $fileName = $this->encrypt().'.pdf';
        $filePath = "asset/tmp/$fileName";

        $this->Output($filePath, 'F');
        $fileUrl = base_url().$filePath;

        return $fileUrl;
    }

}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once 'asset/libraries/tcpdf/tcpdf.php';

class PDF extends TCPDF {

    private $settings = array();

    public function __construct() {
        parent::__construct('P', 'pt', 'A4');
    }

    public function setData($data) {

      $this->data = $data;
    }

    public function Header() {

      $judul = '';
      if (isset($this->data['judul']))
        $judul = $this->data['judul'];

      // $jumlahHalaman = $this->getAliasNbPages();

      $this->SetTopMargin(140);
      $this->SetY(30);

      $this->AddFont('freemono', '', 'asset/lib/tcpdf/fonts/freemono.php');
      $this->SetFont('freemono', '', 10);

      $this->writeHTML('
      <table border="1" bgcolor="cyan">
          <tr>
              <td width="320">
                  <table cellpadding="2">
                      <tr>
                          <td rowspan="4" width="48"><img src="asset/diklat/img/logo-text.png"></td>
                          <td width="310"><h4>SISTEM INFORMASI KEDIKLATAN</h4></td>
                      </tr>
                      <tr>
                          <td><h5>BADAN PENDIDIKAN DAN LATIHAN DAERAH</h5></td>
                      </tr>
                      <tr>
                          <td><h4>PROVINSI LAMPUNG</h4></td>
                      </tr>
                  </table>
                  <br>&nbsp;
              </td>
              <td align="center" width="220">
                <h2>FORMULIR<br>'.$judul.'<br>&nbsp;</h2>
              </td>
          </tr>
          <tr>
            <td>
              <br>&nbsp;
              <b>TANGGAL: '.Date('m-d-Y').'</b>
              <br>&nbsp;
            </td>
            <td>
              <br>&nbsp;
              <b>Halaman '.$this->getAliasNumPage().'</b>
              <br>&nbsp;
            </td>
          </tr>
      </table>
      ');

    }

    public function Footer()
    {
        $this->AddFont($this->settings['fontName'], '', 'asset/libraries/tcpdf/fonts/'.$this->settings['fontName'].'.php');
        $this->SetFont($this->settings['fontName'], '', $this->settings['fontSize']);
        $this->SetY(-15);
        $this->Cell(0, 10,
                    'Page: '.$this->getAliasNumPage().' '.
                    'From: '.$this->getAliasNbPages().' '.
                    'Pages',
                    0,
                    false,
                    'R',
                    0,
                    '',
                    0,
                    false,
                    'T',
                    'M'
                   );
    }

    private function getMainData()
    {
        return array('title' => $this->settings['title']);
    }

    public function setSettings($settings)
    {
        foreach($settings as $k => $v)
            $this->settings[$k] = $v;

        if (!isset($this->settings['template']))
            $this->settings['template'] = 'Main';

        if (!isset($this->settings['title']))
            $this->settings['title'] = '';

        if (!isset($this->settings['pageOrientation']))
            $this->settings['pageOrientation'] = 'P';

        if (!isset($this->settings['pageSize']))
            $this->settings['pageSize'] = 'A4';

        if (!isset($this->settings['fontName']))
            $this->settings['fontName'] = 'dejavusans';

        if (!isset($this->settings['fontSize']))
            $this->settings['fontSize'] = 9;

        $this->SetMargins(30, -30, 20);
        $this->SetHeaderMargin(0);
        $this->SetFooterMargin(0);

        $this->setPageOrientation($this->settings['pageOrientation']);

        $this->AddFont($this->settings['fontName'], '', 'asset/libraries/tcpdf/fonts/'.$this->settings['fontName'].'.php');
        $this->SetFont($this->settings['fontName'], '', $this->settings['fontSize']);
        $this->SetTextColor(0, 0, 0);
    }

    protected function string($variable)
    {
        $strings = $this->settings['strings'];
        $lang = $_POST['lang'];

        if(isset($strings[$lang])) {
            $strings = $strings[$lang];

            if (isset($strings[$variable]))
                return $strings[$variable];
            else
                return $variable;
        }
        else
            return $variable;
    }

    private function encrypt($text = '', $salt = '')
    {
        if (empty($text))
            $text = uniqid();

        $encryptText = sha1($text.$salt);
        $encryptText = strtoupper($encryptText);

        return $encryptText;
    }

    public function view($page, $viewData = array())
    {
        extract($viewData);
        require_once $page;
    }

    public function render($page, $viewData = array())
    {
        $template = $this->settings['template'];
        $mainData = $this->getMainData();

        $this->AddPage();

        $templateUrl = "application/views/pdf/$template.php";
        $pageUrl = "application/views/pdf/pages/$page.php";
        $page = $pageUrl;

        ob_start();
        extract($mainData);
        require_once $templateUrl;
        $contents = ob_get_clean();

        // $this->writeHTML($contents);
        $this->writeHTML($contents, true, false, true, false, '');

        $fileName = $this->encrypt().'.pdf';
        $filePath = "asset/tmp/$fileName";

        $this->Output($filePath, 'F');
        $fileUrl = base_url().$filePath;

        return $fileUrl;
    }

    public function render1($page, $viewData = array())
    {
        $template = $this->settings['template'];
        $mainData = $this->getMainData();
        $foto = base_url().'asset/archive/'.$viewData['foto'];

        $this->AddPage();
        // $templateUrl = "application/views/pdf/$template.php";
        // $pageUrl = "application/views/pdf/pages/$page.php";
        // $page = $pageUrl;

        // ob_start();
        // extract($mainData);
        // require_once $templateUrl;
        $contents =  <<<EOF
        <!-- EXAMPLE OF CSS STYLE -->
        <style>
            h1 {
                color: navy;
                font-family: times;
                font-size: 24pt;
                text-decoration: underline;
            }
            p.first {
                color: #003300;
                font-family: helvetica;
                font-size: 12pt;
            }
            p.first span {
                color: #006600;
                font-style: italic;
            }
            p#second {
                color: rgb(00,63,127);
                font-family: times;
                font-size: 12pt;
                text-align: justify;
            }
            p#second > span {
                background-color: #FFFFAA;
            }
            table.first {
                color: #003300;
                font-family: helvetica;
                font-size: 8pt;
                border-left: 3px solid red;
                border-right: 3px solid #FF00FF;
                border-top: 3px solid green;
                border-bottom: 3px solid blue;
                background-color: #ccffcc;
            }
            td {
                border: 2px solid blue;
                background-color: #ffffee;
            }
            td.second {
                border: 2px dashed green;
            }
            div.test {
                color: #CC0000;
                background-color: #FFFF66;
                font-family: helvetica;
                font-size: 10pt;
                border-style: solid solid solid solid;
                border-width: 2px 2px 2px 2px;
                border-color: green #FF00FF blue red;
                text-align: center;
            }
            .lowercase {
                text-transform: lowercase;
            }
            .uppercase {
                text-transform: uppercase;
            }
            .capitalize {
                text-transform: capitalize;
            }
        </style>
        <div id="wrapper">
            <div class="container form-cetak-pendaftaran">
                <div class="row">
                    <div class="content-fcp">
                        <div class="col-100">
                            <h5 class="fcp-title"> BPSDM PROVINSI LAMPUNG</h5>
                        </div>
                        <div class="cp1">
                        <div class="cp1-1">.</div>
                        <div class="cp1-2">
                            <img class="img-fcp" src="$foto" alt="Foto 4x6">
                        </div>
                        <div class="cp1-3">
                            <table>
                                <tr>
                                    <td>Nomor Absensi</td>
                                    <td>:</td>
                                    <td><span>$foto</span></td>
                                </tr>
                                <tr>
                                    <td>Angkatan</td>
                                    <td>:</td>
                                    <td><span></span></td>
                                </tr>
                            </table>
                        </div>
                    </div>

                    </div>
                </div>
            </div>
        </div>
        
        EOF;

        // $this->writeHTML($contents);
        $this->writeHTML($contents, true, false, true, false, '');

        $fileName = $this->encrypt().'.pdf';
        $filePath = "asset/tmp/$fileName";

        $this->Output($filePath, 'F');
        $fileUrl = base_url().$filePath;

        return $fileUrl;
    }

}

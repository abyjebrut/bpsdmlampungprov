<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once 'asset/libraries/tcpdf/tcpdf.php';

class Pdf extends TCPDF {

    private $settings = array();

    public function __construct() {
        parent::__construct('P', 'pt', 'A4');
    }

    public function setData($data) {

      $this->data = $data;
    }

    public function Header() {

      $judul = '';
      if (isset($this->data['judul']))
        $judul = $this->data['judul'];

      // $jumlahHalaman = $this->getAliasNbPages();

      $this->SetTopMargin(140);
      $this->SetY(30);

      $this->AddFont('freemono', '', 'asset/lib/tcpdf/fonts/freemono.php');
      $this->SetFont('freemono', '', 10);

      $this->writeHTML('
      <table border="1" bgcolor="cyan">
          <tr>
              <td width="320">
                  <table cellpadding="2">
                      <tr>
                          <td rowspan="4" width="48"><img src="asset/diklat/img/logo-text.png"></td>
                          <td width="310"><h4>SISTEM INFORMASI KEDIKLATAN</h4></td>
                      </tr>
                      <tr>
                          <td><h5>BADAN PENDIDIKAN DAN LATIHAN DAERAH</h5></td>
                      </tr>
                      <tr>
                          <td><h4>PROVINSI LAMPUNG</h4></td>
                      </tr>
                  </table>
                  <br>&nbsp;
              </td>
              <td align="center" width="220">
                <h2>FORMULIR<br>'.$judul.'<br>&nbsp;</h2>
              </td>
          </tr>
          <tr>
            <td>
              <br>&nbsp;
              <b>TANGGAL: '.Date('m-d-Y').'</b>
              <br>&nbsp;
            </td>
            <td>
              <br>&nbsp;
              <b>Halaman '.$this->getAliasNumPage().'</b>
              <br>&nbsp;
            </td>
          </tr>
      </table>
      ');

    }

    public function Footer()
    {
        $this->AddFont($this->settings['fontName'], '', 'asset/libraries/tcpdf/fonts/'.$this->settings['fontName'].'.php');
        $this->SetFont($this->settings['fontName'], '', $this->settings['fontSize']);
        $this->SetY(-15);
        $this->Cell(0, 10,
                    'Page: '.$this->getAliasNumPage().' '.
                    'From: '.$this->getAliasNbPages().' '.
                    'Pages',
                    0,
                    false,
                    'R',
                    0,
                    '',
                    0,
                    false,
                    'T',
                    'M'
                   );
    }

    private function getMainData()
    {
        return array('title' => $this->settings['title']);
    }

    public function setSettings($settings)
    {
        foreach($settings as $k => $v)
            $this->settings[$k] = $v;

        if (!isset($this->settings['template']))
            $this->settings['template'] = 'Main';

        if (!isset($this->settings['title']))
            $this->settings['title'] = '';

        if (!isset($this->settings['pageOrientation']))
            $this->settings['pageOrientation'] = 'P';

        if (!isset($this->settings['pageSize']))
            $this->settings['pageSize'] = 'A4';

        if (!isset($this->settings['fontName']))
            $this->settings['fontName'] = 'dejavusans';

        if (!isset($this->settings['fontSize']))
            $this->settings['fontSize'] = 9;

        $this->SetMargins(30, -30, 20);
        $this->SetHeaderMargin(0);
        $this->SetFooterMargin(0);

        $this->setPageOrientation($this->settings['pageOrientation']);

        $this->AddFont($this->settings['fontName'], '', 'asset/libraries/tcpdf/fonts/'.$this->settings['fontName'].'.php');
        $this->SetFont($this->settings['fontName'], '', $this->settings['fontSize']);
        $this->SetTextColor(0, 0, 0);
    }

    protected function string($variable)
    {
        $strings = $this->settings['strings'];
        $lang = $_POST['lang'];

        if(isset($strings[$lang])) {
            $strings = $strings[$lang];

            if (isset($strings[$variable]))
                return $strings[$variable];
            else
                return $variable;
        }
        else
            return $variable;
    }

    private function encrypt($text = '', $salt = '')
    {
        if (empty($text))
            $text = uniqid();

        $encryptText = sha1($text.$salt);
        $encryptText = strtoupper($encryptText);

        return $encryptText;
    }

    public function view($page, $viewData = array())
    {
        extract($viewData);
        require_once $page;
    }

    public function render($page, $viewData = array())
    {
        $template = $this->settings['template'];
        $mainData = $this->getMainData();

        $this->AddPage();

        $templateUrl = "application/views/pdf/$template.php";
        $pageUrl = "application/views/pdf/pages/$page.php";
        $page = $pageUrl;

        ob_start();
        extract($mainData);
        require_once $templateUrl;
        $contents = ob_get_clean();

        // $this->writeHTML($contents);
        $this->writeHTML($contents, true, false, true, false, '');

        $fileName = $this->encrypt().'.pdf';
        $filePath = "asset/tmp/$fileName";

        $this->Output($filePath, 'F');
        $fileUrl = base_url().$filePath;

        return $fileUrl;
    }

    public function render1($page, $viewData = array())
    {
        $template = $this->settings['template'];
        $mainData = $this->getMainData();
        
        $row = $viewData['row'];

        $foto = base_url().'asset/archive/'.$row['foto'];
        $angkatan = $row['angkatan'];
        $namaDiklat = $row['namaDiklat'];
        $pelaksanaan = $this->indonesian_date($row['_pelaksanaanMulai'] ,"l, j F Y").' s/d '.$this->indonesian_date($row['_pelaksanaanSelesai'] ,"l, j F Y");
        $lokasi = $row['lokasi'];
        $nama = $row['nama'];
        $nip = $row['nip'];
        $alamat = $row['alamat'];
        $alamatKantor = $row['alamatKantor'];
        $tempatLahir = $row['tempatLahir'];
        $tanggalLahir = $this->indonesian_date($row['_tanggalLahir'] ,"l, j F Y");
        $agama = $row['agama'];
        $jenisKelamin = $row['jenisKelamin'];
        $pangkatGolongan = $row['pangkatGolongan'];
        $jabatanEselon = $row['jabatanEselon'];
        $unitKerja = $row['unitKerja'];
        $instansi = $row['instansi'];
        $instansiLainnya = $row['instansiLainnya'];
        $email = $row['email'];
        $tgl = date("Y-m-d");
        $tanggalSekarang = $this->indonesian_date($tgl ,"j F Y");
        // $tanggalSekarang = $this->indonesian_date($tgl ,"l, j F Y");

        $rsBerkas = $viewData['rsBerkas'];
        $suratPerintahTugas='.....';
        $suratPernyataanBebasTugasKedinasan='.....';
        $suratKeputusanCPNS='.....';
        $suratKeputusanPangkat='.....';
        $suratKeputusanJabatanTerakhir='.....';
        $suratKeteranganDokterdariRumahSakitPemerintah='.....';
        $ijazahTerakhir ='.....';
        $askesBPJSatauKIS ='.....';
        $suratKesehatan ='.....';
        foreach ($rsBerkas as $key => $val) {

            if ($val['berkasPendaftaranId'] == 1 && $val['status'] == 'Selesai') {
                $suratPerintahTugas=' <span style="font-family:zapfdingbats;">3</span> ';
            }
            if ($val['berkasPendaftaranId'] == 2 && $val['status'] == 'Selesai') {
                $suratPernyataanBebasTugasKedinasan=' <span style="font-family:zapfdingbats;">3</span> ';
            }
            if ($val['berkasPendaftaranId'] == 3 && $val['status'] == 'Selesai') {
                $suratKeputusanCPNS=' <span style="font-family:zapfdingbats;">3</span> ';
            }
            if ($val['berkasPendaftaranId'] == 4 && $val['status'] == 'Selesai') {
                $suratKeputusanPangkat=' <span style="font-family:zapfdingbats;">3</span> ';
            }
            if ($val['berkasPendaftaranId'] == 5 && $val['status'] == 'Selesai') {
                $suratKeputusanJabatanTerakhir=' <span style="font-family:zapfdingbats;">3</span> ';
            }
            if ($val['berkasPendaftaranId'] == 6 && $val['status'] == 'Selesai') {
                $suratKeteranganDokterdariRumahSakitPemerintah=' <span style="font-family:zapfdingbats;">3</span> ';
            }
            if ($val['berkasPendaftaranId'] == 7 && $val['status'] == 'Selesai') {
                $ijazahTerakhir=' <span style="font-family:zapfdingbats;">3</span> ';
            }
            if ($val['berkasPendaftaranId'] == 8 && $val['status'] == 'Selesai') {
                $askesBPJSatauKIS=' <span style="font-family:zapfdingbats;">3</span> ';
            }
            if ($val['berkasPendaftaranId'] == 9 && $val['status'] == 'Selesai') {
                $suratKesehatan=' <span style="font-family:zapfdingbats;">3</span> ';
            }
        }

        $this->AddPage();
        
        $contents =  '
        <!-- EXAMPLE OF CSS STYLE -->
        <style>
            h1 {
                color: #fff;               
                font-size: 24pt;                  
            }
            .page-title{
                color: black;
                font-family: helvetica;
                font-size: 12pt;
                text-decoration: underline;                
            }   
            table {
                color: black;
                font-family: helvetica;
                font-size: 10pt;                          
            }
            .absensi{
                color: black;
                font-family: helvetica;
                font-size: 11pt; 
            }                    
            table.wrapbiodata {
                color: black;
                font-family: helvetica;
                font-size: 10pt;
                border-left: 2px solid black;
                border-right: 2px solid black;
                border-top: 2px solid black;
                border-bottom: 2px solid black;                
            }
            .captionbold{
                color: black;                
                font-size: 12pt;
                text-decoration: underline;                 
            }     
            .underlines{
                border-bottom: 1px solid black;   
            }                
            .lowercase {
                text-transform: lowercase;
            }
            .uppercase {
                text-transform: uppercase;
            }
            .capitalize {
                text-transform: capitalize;
            }
        </style>
        
        <h1 style="color:white">.</h1>        
        <h4 class="page-title">BPSDM PROVINSI LAMPUNG</h4>       
        <table border="0" style="text-align:center">
        <br>      
        <br>
        <br>               
            <tr>
                <td>.</td>
                <td><img src="'.$foto.'" alt="Foto 4x6" border="1" height="150" width="120" /></td>
                <td style="text-align:left">                                                      
                    <table class="absensi">
                        <tr style="text-align:left">
                            <td width="90">Nomor Absensi</td>
                            <td width="10">:</td>
                            <td width="83">.......................<br></td>                               
                        </tr>
                        <tr style="text-align:left">
                            <td width="90">Angkatan</td>
                            <td width="10">:</td>
                            <td width="83">'.$angkatan.'</td>
                        </tr>
                    </table>
                </td>
            </tr>                        
        </table>
        <br>       
        <br>
        <table class="wrapbiodata" cellpadding="1" cellspacing="1" border="0">
            <tr style="text-align:center">                
                <td class="captionbold"><br>BIODATA<br></td>
            </tr>    
            <tr>                
                <td style="text-align:left">                                                      
                    <table cellpadding="1" cellspacing="1" border="0">
                        <tr style="text-align:left">
                            <td width="130">Nama Diklat</td>
                            <td width="6">:</td>
                            <td width="385">'.$namaDiklat.'</td>
                        </tr>
                        <tr style="text-align:left">
                            <td width="130">Tanggal Pelaksanaan</td>
                            <td width="6">:</td>
                            <td width="385">'.$pelaksanaan.'</td>
                        </tr>
                        <tr style="text-align:left">
                            <td width="130">Tempat Penyelenggaraan</td>
                            <td width="6">:</td>
                            <td width="385">'.$lokasi.'<br></td>                            
                        </tr>                       
                    </table>
                </td>
            </tr>                        
        </table>
        <br>
        <br>      
        <table class="wrapbiodata" cellpadding="1" cellspacing="1" border="0">               
            <tr>                
                <td style="text-align:left">                                                      
                    <table cellpadding="1" cellspacing="1" border="0">
                    <tr style="text-align:left">
                            <br>                  
                            <td width="136">  01. Nama Lengkap</td>
                            <td width="6">:</td>
                            <td width="385">'.$nama.'</td>
                        </tr>
                        <tr style="text-align:left">
                            <td width="136">  02. NIP</td>
                            <td width="6">:</td>
                            <td width="385">'.$nip.'</td>
                        </tr>
                        <tr style="text-align:left">
                            <td width="136">  03. Alamat Rumah</td>
                            <td width="6">:</td>
                            <td width="385">'.$alamat.' </td>
                        </tr> 
                         <tr style="text-align:left">
                            <td width="136">  04. Tempat Lahir</td>
                            <td width="6">:</td>
                            <td width="385">'.$tempatLahir.'</td>
                        </tr>  
                        <tr style="text-align:left">
                            <td width="136">  05. Tanggal Lahir</td>
                            <td width="6">:</td>
                            <td width="385">'.$tanggalLahir.'</td>
                        </tr>
                        <tr style="text-align:left">
                            <td width="136">  06. Agama</td>
                            <td width="6">:</td>
                            <td width="385">'.$agama.'</td>
                        </tr>  
                        <tr style="text-align:left">
                            <td width="136">  07. Jenis Kelamin</td>
                            <td width="6">:</td>
                            <td width="385">'.$jenisKelamin.'</td>
                        </tr>  
                        <tr style="text-align:left">
                            <td width="136">  08. Pangkat/ Golongan</td>
                            <td width="6">:</td>
                            <td width="385">'.$pangkatGolongan.'</td>
                        </tr>    
                        <tr style="text-align:left">
                            <td width="136">  09. Jabatan</td>
                            <td width="6">:</td>
                            <td width="385">'.$jabatanEselon.'</td>
                        </tr>  
                        <tr style="text-align:left">
                            <td width="136">  10. Unit Kerja</td>
                            <td width="6">:</td>
                            <td width="385">'.$unitKerja.'</td>
                        </tr>  
                        <tr style="text-align:left">
                            <td width="136">  11. Instansi</td>
                            <td width="6">:</td>
                            <td width="385">'.$instansi.'</td>
                        </tr> 
                        <tr style="text-align:left">
                            <td width="136">  12. Alamat Kantor</td>
                            <td width="6">:</td>
                            <td width="385">'.$alamatKantor.'</td>
                        </tr>   
                        <tr style="text-align:left">
                            <td width="136">  13. E-Mail</td>
                            <td width="6">:</td>
                            <td width="385">'.$email.'<br></td>                                
                        </tr>                                    
                    </table>
                </td>
            </tr>                        
        </table>
        <br>      
        <br>
        <br>                
        <table cellpadding="1" cellspacing="1" border="0">
            <tr style="text-align:left">       
                <td>
                    <table>
                        <tr>
                            <td style="text-align:left" width="340">                                                      
                                <table>
                                    <tr style="text-align:left">                      
                                        <td>Persyaratan Berkas : <br></td>                
                                    </tr>    
                                    <tr style="text-align:left">       
                                        <td width="25">['.$suratPerintahTugas.']</td>                  
                                        <td width="330">Surat Perintah Tugas</td>                
                                    </tr>    
                                    <tr style="text-align:left"> 
                                        <td width="25">['.$suratPernyataanBebasTugasKedinasan.']</td>                       
                                        <td width="300">Surat Pernyataan Bebas Tugas Kedinasan</td>                
                                    </tr>      
                                    <tr style="text-align:left">                      
                                        <td width="25">['.$suratKeputusanCPNS.']</td>                   
                                        <td width="300">Surat Keputusan CPNS</td>                
                                    </tr>     
                                    <tr style="text-align:left">                      
                                        <td width="25">['.$suratKeputusanPangkat.']</td>                  
                                        <td width="300">Surat Keputusan Pangkat</td>                
                                    </tr>    
                                    <tr style="text-align:left">                      
                                        <td width="25">['.$suratKeputusanJabatanTerakhir.']</td>                  
                                        <td width="300">Surat Keputusan Jabatan Terakhir</td>                
                                    </tr>      
                                    <tr style="text-align:left">                      
                                        <td width="25">['.$ijazahTerakhir.']</td>                  
                                        <td width="300">Ijazah Terakhir</td>                
                                    </tr>
                                    <tr style="text-align:left">                      
                                        <td width="25">['.$suratKeteranganDokterdariRumahSakitPemerintah.']</td>                  
                                        <td width="300">Surat Keterangan Dokter dari Rumah Sakit Pemerintah</td>                
                                    </tr>
                                    <tr style="text-align:left">                      
                                        <td width="25">['.$askesBPJSatauKIS.']</td>                  
                                        <td width="300">Askes, BPJS atau KIS</td>                
                                    </tr>    
                                    <tr style="text-align:left">                      
                                        <td width="25">['.$suratKesehatan.']</td>                  
                                        <td width="300">Surat Kesehatan Covid 19</td>                
                                    </tr>        
                                </table>
                            </td>
                            <td style="text-align:left" width="190">                                                      
                                <table>                                                                                                                                                                   
                                    <tr style="text-align:left">
                                        <td height="20">Bandar Lampung, '.$tanggalSekarang.'</td> 
                                    </tr>
                                    <tr style="text-align:center">
                                        <td height="80">PESERTA, <br></td> 
                                    </tr>
                                    <tr style="text-align:left">
                                        <td class="underlines">'.$nama.'</td> 
                                    </tr>
                                     <tr style="text-align:left">
                                        <td>NIP. '.$nip.'</td> 
                                    </tr>
                                </table>
                            </td>
                        </tr>                        
                    </table>
                </td>                                            
            </tr>                            
        </table>';
            
        // $this->writeHTML($contents);
        $this->writeHTML($contents, true, false, true, false, '');

        $fileName = $this->encrypt().'.pdf';
        $filePath = "asset/tmp/$fileName";

        $this->Output($filePath, 'F');
        $fileUrl = base_url().$filePath;

        return $fileUrl;
    }

    protected function indonesian_date ($timestamp = '', $date_format = 'l, j F Y | H:i', $suffix = '') {
        if (trim ($timestamp) == '')
        {
          $timestamp = time ();
        }
        elseif (!ctype_digit ($timestamp))
        {
          $timestamp = strtotime ($timestamp);
        }
        # remove S (st,nd,rd,th) there are no such things in indonesia :p
        $date_format = preg_replace ("/S/", "", $date_format);
        $pattern = array (
            '/Mon[^day]/','/Tue[^sday]/','/Wed[^nesday]/','/Thu[^rsday]/',
    
            '/Fri[^day]/','/Sat[^urday]/','/Sun[^day]/','/Monday/','/Tuesday/',
            '/Wednesday/','/Thursday/','/Friday/','/Saturday/','/Sunday/',
            '/Jan[^uary]/','/Feb[^ruary]/','/Mar[^ch]/','/Apr[^il]/','/May/',
            '/Jun[^e]/','/Jul[^y]/','/Aug[^ust]/','/Sep[^tember]/','/Oct[^ober]/',
            '/Nov[^ember]/','/Dec[^ember]/','/January/','/February/','/March/',
            '/April/','/June/','/July/','/August/','/September/','/October/',
            '/November/','/December/',
        );
    
        $replace = array ( 'Sen','Sel','Rab','Kam','Jum','Sab','Min',
            'Senin','Selasa','Rabu','Kamis','Jumat','Sabtu','Minggu',
            'Jan','Feb','Mar','Apr','Mei','Jun','Jul','Ags','Sep','Okt','Nov','Des',
            'Januari','Februari','Maret','April','Juni','Juli','Agustus','September',
            'Oktober','November','Desember',
        );
        
        $date = date ($date_format, $timestamp);
        $date = preg_replace ($pattern, $replace, $date);
        $date = "{$date} {$suffix}";
        return $date;
      }
};

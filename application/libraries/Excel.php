<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once 'asset/libraries/phpexcel/Classes/PHPExcel.php';

class Excel {
    
    private function encrypt($text = '', $salt = '')
    {
        if (empty($text))
            $text = uniqid();
        
        $encryptText = sha1($text.$salt);
        $encryptText = strtoupper($encryptText);
        
        return $encryptText;
    }
    
    public function renderList($template, $data) {
        $dataSource = $data['dataSource'];
        $showFields = $data['showFields'];
        
        $reader = PHPExcel_IOFactory::createReader('Excel5');
        $templateUrl = "application/views/excel/$template.xls";
        
        $excel = $reader->load($templateUrl);
        $sheet = $excel->setActiveSheetIndex(0);

        $i = 0;
        $now = date('d M Y');

        $sheet->setCellValue('A1', $now);

        foreach($dataSource as $rs) {
            $i++;
            $x = $i + 4;

            $style = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN
                    )
                )
            );

            $sheet->setCellValue("A$x", " $i.");
            $ascIndex = 65;

            foreach($showFields as $field) {
                $ascIndex++;
                $sheet->setCellValue(chr($ascIndex).$x, " ".$rs[$field]);
            }

            $sheet->getStyle("A$x:".chr($ascIndex).$x)->applyFromArray($style);
            unset($style);
        }

        $fileName = $this->encrypt().'.xls';
        $filePath = "asset/tmp/$fileName";

        $writer = PHPExcel_IOFactory::createWriter($excel, 'Excel5');
        $writer->save($filePath);

        $fileUrl = base_url().$filePath;

        return $fileUrl;
    }
    
    public function renderDetail($template, $data) {

        $dataSource = $data['dataSource'];
        $showFields = $data['showFields'];

        $reader = PHPExcel_IOFactory::createReader('Excel5');
        $templateUrl = "application/views/excel/$template.xls";

        $excel = $reader->load($templateUrl);
        $sheet = $excel->setActiveSheetIndex(0);

        $i = 0;
        $now = date('d M Y');

        $sheet->setCellValue('A1', $now);

        foreach($showFields as $field) {
            $i++;
            $x = $i + 3;

            $sheet->setCellValue("B$x", " ".$dataSource[$field]);
        }

        $fileName = $this->encrypt().'.xls';
        $filePath = "asset/tmp/$fileName";

        $writer = PHPExcel_IOFactory::createWriter($excel, 'Excel5');
        $writer->save($filePath);

        $fileUrl = base_url().$filePath;

        return $fileUrl;
    }
    
}
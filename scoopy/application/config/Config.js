scoopy.application.config.Config = function () {

  this.app = {
    title: 'BPSDM Provinsi Lampung',
  };
  
  this.url = {
    // api: 'https://bpsdmlampung.xyz/api',
    api: 'http://localhost:8081/bpsdmlampungprov/api',
  };

  this.widgets = {
    HeadBar: true,
    FootBar: true,
    SideBar: true,
  };

  this.controllers = [
  ];

  this.load = function (callback) {

    // Code Igniter adjusment
    
    this.url.api = this.url.api + '/';

    var url = this.url.api + 'System/readConfig';

    $.post(url, function (result) {
      if (result.status == 'success') {

        // if (!localStorage.lang)
        // localStorage.lang = result.data.lang;
        localStorage.lang = 'id';

        localStorage.apiKey = result.data.apiKey;

        if (!localStorage.authKey)
        localStorage.authKey = '';

        if (!localStorage.userType)
        localStorage.userType = 'free';

        localStorage.urlSite = result.data.urlSite;
        localStorage.urlApi = result.data.urlApi;
        localStorage.urlTmp = result.data.urlTmp;
        localStorage.urlArchive = result.data.urlArchive;

        callback();
      }
    }.bind(this));

  };
};

scoopy.application.core.Controller = function () {
  scoopy.system.core.Controller.call(this);

  this.popbox = function (title, imgUrl) {
    var $scpPopbox = this.getScope('Popbox');

    var $message = $scpPopbox.getRef('message');
    var $title = $scpPopbox.getRef('title');

    $title.html(title);
    $message.html('<img src="' + imgUrl + '" style="width: 100%">');

    $scpPopbox.modal();
  };

  this.alert = function (message, callback) {
    var $scpAlert = this.getScope('Alert');

    var $message = $scpAlert.getRef('message');
    var $btnOK = $scpAlert.getRef('btnOK');

    $message.html(message);

    $btnOK.off('click');
    $btnOK.on('click', function () {

      if($.isFunction(callback))
        callback();
    });

    $scpAlert.modal();
  };

  this.confirm = function (question, callback) {
    var $scpConfirm = this.getScope('Confirm');

    var $message = $scpConfirm.getRef('message');
    var $btnOK = $scpConfirm.getRef('btnOK');

    $message.html(question);

    $btnOK.off('click');
    $btnOK.on('click', function () {

      if($.isFunction(callback))
      callback();
    });

    $scpConfirm.modal();
  };

};

scoopy.application.core.Controller.prototype =
Object.create(scoopy.system.core.Controller);

scoopy.application.widgets.SideBar = function () {
  scoopy.application.core.Controller.call(this);
  var $scope = this.getScope('SideBar');

  var $scpLogin = this.getScope('Login');
  var $scpSignup = this.getScope('Signup');
  var $scpForgotPassword = this.getScope('ForgotPassword');
  var $scpGuestBook = this.getScope('GuestBook');
  var $scpReEmail = this.getScope('ReEmail');

  this.init = function () {

    // Define Login

    $scpLogin.$nip = $scpLogin.getRef('nip');
    $scpLogin.$kataSandi = $scpLogin.getRef('kataSandi');

    $scpLogin.$btnLogin = $scpLogin.getRef('btnLogin');

    // Define Signup

    $scpSignup.$nip = $scpSignup.getRef('nip');
    $scpSignup.$ulangiNip = $scpSignup.getRef('ulangiNip');
    $scpSignup.$nama = $scpSignup.getRef('nama');
    $scpSignup.$email = $scpSignup.getRef('email');
    $scpSignup.$kataSandi = $scpSignup.getRef('kataSandi');
    $scpSignup.$ulangiKataSandi = $scpSignup.getRef('ulangiKataSandi');

    $scpSignup.$btnKirim = $scpSignup.getRef('btnKirim');
    $scpSignup.$btnBatal = $scpSignup.getRef('btnBatal');

    // Define Forgot Password

    $scpForgotPassword.$nip = $scpForgotPassword.getRef('nip');
    $scpForgotPassword.$email = $scpForgotPassword.getRef('email');

    $scpForgotPassword.$btnKirim = $scpForgotPassword.getRef('btnKirim');
    $scpForgotPassword.$btnBatal = $scpForgotPassword.getRef('btnBatal');


    $scpReEmail.$nip = $scpReEmail.getRef('nip');
    $scpReEmail.$email = $scpReEmail.getRef('email');

    $scpReEmail.$btnKirim = $scpReEmail.getRef('btnKirim');
    $scpReEmail.$btnBatal = $scpReEmail.getRef('btnBatal');

    // Define Guest Book

    $scpGuestBook.$name = $scpGuestBook.getRef('name');
    $scpGuestBook.$email = $scpGuestBook.getRef('email');
    $scpGuestBook.$subject = $scpGuestBook.getRef('subject');
    $scpGuestBook.$message = $scpGuestBook.getRef('message');
    $scpGuestBook.$btnSend = $scpGuestBook.getRef('btnSend');

  };

  this.driveEvents = function () {
    // Drive Login

    $scpLogin.$btnLogin.on('click', this.login.bind(this));
    $scpLogin.$nip.on('keydown', function (event) {
      if (event.keyCode == 13)
        $scpLogin.$kataSandi.focus();
    });
    $scpLogin.$kataSandi.on('keydown', function (event) {
      if (event.keyCode == 13)
        $scpLogin.$btnLogin.trigger('click');
    });

    // Drive Signup

    $scpSignup.$btnKirim.on('click', this.signup.bind(this));

    // Drive Forgot Password

    $scpForgotPassword.$btnKirim.on('click', this.forgotPassword.bind(this));
    $scpReEmail.$btnKirim.on('click', this.reEmail.bind(this));

    // Drive Guest Book

    $scpGuestBook.$btnSend.on('click', this.sendMessage.bind(this));
  };

  this.login = function () {

    var url = 'Anggota/login';
    var data = {
      nip: $scpLogin.$nip.val(),
      kataSandi: $scpLogin.$kataSandi.val(),
    };

    $scpLogin.$btnLogin.html('Wait...');

    api.post(url, data, function (result) {

      switch (result.status) {
        case 'success':

          this.clearLogin();
          location.reload();
          localStorage.authKey = result.data.auth;
          window.location = localStorage.urlSite;
          break;

        default:
          var message = '<b>' + result.message + '</b><br><br>';

          if (result.data.nip)
            message += result.data.nip + '<br>';
          if (result.data.kataSandi)
            message += result.data.kataSandi + '<br>';

          this.alert(message);
      }

      $scpLogin.$btnLogin.html('Login');
    }.bind(this));

  };

  this.clearLogin = function () {
    $scpLogin.$nip.val('');
    $scpLogin.$kataSandi.val('');
  };

  this.signup = function () {
    var url = 'Anggota/signup';
    var data = {
      nip: $scpSignup.$nip.val(),
      ulangiNip: $scpSignup.$ulangiNip.val(),
      nama: $scpSignup.$nama.val(),
      email: $scpSignup.$email.val(),
      kataSandi: $scpSignup.$kataSandi.val(),
      ulangiKataSandi: $scpSignup.$ulangiKataSandi.val(),
    };

    $scpSignup.$btnKirim.html('Wait...');

    api.post(url, data, function (result) {

      switch (result.status) {
        case 'success':
          this.clearSignup();

          // this.alert(
          //   'Anda telah terdaftar sebagai anggota,' +
          //   'silakan periksa email Anda dan ikuti instruksinya ' +
          //   'agar Akun Anda dapat diaktifkan'
          // , function () {
          //   $scpSignup.$btnBatal.trigger('click');
          //   });
          this.alert(
            'Akun anda telah berhasil dibuat, silahkan login'
            , function () {
              $scpSignup.$btnBatal.trigger('click');
            });
          break;

        default:
          var message = '<b>' + result.message + '</b><br><br>';

          if (result.data.nip)
            message += result.data.nip + '<br>';
          if (result.data.ulangiNip)
            message += result.data.ulangiNip + '<br>';
          if (result.data.nama)
            message += result.data.nama + '<br>';
          if (result.data.email)
            message += result.data.email + '<br>';
          if (result.data.kataSandi)
            message += result.data.kataSandi + '<br>';
          if (result.data.ulangiKataSandi)
            message += result.data.ulangiKataSandi + '<br>';

          this.alert(message);
      }

      $scpSignup.$btnKirim.html('Kirim');
    }.bind(this));

  };

  this.clearSignup = function () {
    $scpSignup.$nip.val('');
    $scpSignup.$ulangiNip.val('');
    $scpSignup.$nama.val('');
    $scpSignup.$email.val('');
    $scpSignup.$kataSandi.val('');
    $scpSignup.$ulangiKataSandi.val('');
  };

  this.forgotPassword = function () {
    var url = 'Anggota/forgotPassword';
    var data = {
      nip: $scpForgotPassword.$nip.val(),
      email: $scpForgotPassword.$email.val(),
    };

    $scpForgotPassword.$btnKirim.html('Wait...');

    api.post(url, data, function (result) {

      switch (result.status) {
        case 'success':

          this.clearForgotPassword();
          this.alert(
            'Kami telah mengirim email untuk mereset Kata Sandi Anda<br>' +
            'Silakan cek email Anda, dan ikuti instruksi yg diberikan'
            , function () {
              $scpForgotPassword.$btnBatal.trigger('click');
            });
          break;

        default:
          var message = '<b>' + result.message + '</b><br><br>';

          if (result.data.nip)
            message += result.data.nip + '<br>';
          if (result.data.email)
            message += result.data.email + '<br>';

          this.alert(message);
      }

      $scpForgotPassword.$btnKirim.html('Kirim');
    }.bind(this));

  };



  this.clearForgotPassword = function () {
    $scpForgotPassword.$nip.val('');
    $scpForgotPassword.$email.val('');
  };

  this.reEmail = function () {
    var url = 'Anggota/reEmail';
    var data = {
      nip: $scpReEmail.$nip.val(),
      email: $scpReEmail.$email.val(),
    };

    $scpReEmail.$btnKirim.html('Wait...');

    api.post(url, data, function (result) {

      switch (result.status) {
        case 'success':

          this.clearReEmail();
          this.alert(
            'Kami telah mengirim email untuk mereset Kata Sandi Anda<br>' +
            'Silakan cek email Anda, dan ikuti instruksi yg diberikan'
            , function () {
              $scpReEmail.$btnBatal.trigger('click');
            });
          break;

        default:
          var message = '<b>' + result.message + '</b><br><br>';

          if (result.data.nip)
            message += result.data.nip + '<br>';
          if (result.data.email)
            message += result.data.email + '<br>';

          this.alert(message);
      }

      $scpReEmail.$btnKirim.html('Kirim');
    }.bind(this));

  };


  this.clearReEmail = function () {
    $scpReEmail.$nip.val('');
    $scpReEmail.$email.val('');
  };

  this.sendMessage = function () {

    var url = 'GuestBook/createData';
    var data = {
      name: $scpGuestBook.$name.val(),
      email: $scpGuestBook.$email.val(),
      subject: $scpGuestBook.$subject.val(),
      message: $scpGuestBook.$message.val(),
    };

    $scpGuestBook.$btnSend.html('WAIT...');

    api.post(url, data, function (result) {

      switch (result.status) {
        case 'success':
          $scpGuestBook.$name.val('');
          $scpGuestBook.$email.val('');
          $scpGuestBook.$subject.val('');
          $scpGuestBook.$message.val('');

          this.alert('Message was sent');
          break;

        default:
          var message = '<b>' + result.message + '</b><br><br>';

          if (result.data.name)
            message += result.data.name + '<br>';

          if (result.data.email)
            message += result.data.email + '<br>';

          if (result.data.subject)
            message += result.data.subject + '<br>';

          if (result.data.message)
            message += result.data.message + '<br>';

          this.alert(message);
      }

      $scpGuestBook.$btnSend.html('KIRIM <i class="wow fadeIn fa fa-paper-plane"></i>');
    }.bind(this));

  };

};

scoopy.application.widgets.SideBar.prototype =
  Object.create(scoopy.application.core.Controller.prototype);

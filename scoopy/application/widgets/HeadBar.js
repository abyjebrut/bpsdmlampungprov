scoopy.application.widgets.HeadBar = function () {
  scoopy.application.core.Controller.call(this);
  var $scope = this.getScope('HeadBar');

  var $scpMenuBar;
  var $scpSearchBar;

  this.init = function () {

    // Define MenuBar

    $scpMenuBar = this.getScope('MenuBar');
    $scpMenuBar.$menuItems = $scpMenuBar.find('li>a');
    $scpMenuBar.$linkLogout = $scpMenuBar.getRef('linkLogout');

    // Define SearchBar

    // $scpSearchBar = this.getScope('SearchBar');
    //
    // $scpSearchBar.$keyword = $scpSearchBar.getRef('keyword');
    // $scpSearchBar.$btnSearch = $scpSearchBar.getRef('btnSearch');

    // Initialize

    this.activateMenu();
  };

  this.driveEvents = function () {

    // MenuBar Events

    $scpMenuBar.$linkLogout.on('click', this.logout.bind(this));

    // SearchBar Events

    // $scpSearchBar.on('submit', this.searchWebsite.bind(this));
  };

  // MenuBar Methods

  this.activateMenu = function () {
    $.each($scpMenuBar.$menuItems, function (i, a) {
      var href = $(a).attr('href');

      if (href == window.location)
      $(a).parent().addClass('active');
    });
  };

  this.logout = function () {
    this.confirm('Logout dari website ini?', function () {
      $scpMenuBar.$linkLogout.html('Wait...');

      api.post('Anggota/logout', function () {
        location.reload();
      });
    }.bind(this));
  };

  // SearchBar Methods

  // this.searchWebsite = function () {
  //
  //   if ($scpSearchBar.$keyword.val().trim() == '') {
  //       this.alert('Keyword required');
  //       return false;
  //   }
  //
  // };

};

scoopy.application.widgets.HeadBar.prototype =
Object.create(scoopy.application.core.Controller.prototype);

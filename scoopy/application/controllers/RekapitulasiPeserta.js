scoopy.application.controllers.RekapitulasiPeserta = function () {
  scoopy.application.core.Controller.call(this);
  var $scope = this.getScope('Content');
  var $keyword;
  var $listData;

  var rsDiklat = [];

  this.init = function () {
    $keyword = $scope.getRef('keyword');
    $listData = $scope.getRef('listData');
  };

  this.reload = function () {
    this.readData();
  };

  this.driveEvents = function () {
    $keyword.on('keyup', this.renderBySearching.bind(this));
  };

  this.readData = function () {

    var url = 'Diklat/readDataRekapitulasiPeserta';
    var data = {};

    $listData.html('<tr><td colspan="3">Loading...</td></tr>');

    api.post(url, data, function (result) {
      rsDiklat = result.data;
      var html = '';

      $.each(rsDiklat, function (i, row) {
        html += this.renderList(i, row);
      }.bind(this));

      $listData.html(html);
    }.bind(this));
  };

  this.renderBySearching = function () {

    var filteredRs = rsDiklat.filter(function (data) {
      var regex = new RegExp($keyword.val(), 'gi');
      var found = false;

      if (data.tahunDiklat.match(regex))
        found = true;

      return found;
    });

    var html = '';

    $.each(filteredRs, function (i, row) {
      html += this.renderList(i, row);
    }.bind(this));

    $listData.html(html);
  };

  this.renderList = function (i, row) {
    
    var num = i + 1;
    var html = '<tr>' +
      '<td data-title="No" class="numbers">' + num + '.</td>' +
      '<td data-title="Tahun" class="numbers">' + row.tahunDiklat +' </td>' +
      '<td data-title="Target Peserta" class="numbers">' + row._kuota +' Orang</td>' +
      '<td data-title="Widyaiswara" class="numbers">' + row.kuotaDiklat + ' Orang</td>' +
      '</tr>';
    return html;
  };

  this.search = function () {
  };

};

scoopy.application.controllers.RekapitulasiPeserta.prototype =
  Object.create(scoopy.application.core.Controller.prototype);

scoopy.application.controllers.BahanAjar = function () {
  scoopy.application.core.Controller.call(this);
  var $scope = this.getScope('Content');

  var $jenisDiklat;
  var $namaDiklat;
  var $listData;

  var rsArsipMateri = [];

  this.init = function () {
    $jenisDiklat = $scope.getRef('jenisDiklat');
    $namaDiklat = $scope.getRef('namaDiklat');
    $listData = $scope.getRef('listData');
  };

  this.reload = function () {
  };

  this.driveEvents = function () {
    $jenisDiklat.on('change', this.renderByJenis.bind(this));
    $namaDiklat.on('change', this.renderByNamaDiklat.bind(this));
  };

  this.renderByJenis = function () {

    $namaDiklat.html('<option data-hidden="true" value="-1">--Nama Diklat--</option>');

    if ($jenisDiklat.val() == '-1'){
      // $namaDiklat.html('disabled');
    }
    else {
      $namaDiklat.prop('disabled', false);
      $namaDiklat.focus();

      var url = 'Diklat/readData';
      var data = {
        jenisDiklatId: $jenisDiklat.val(),
      };

      api.post(url, data, function (result) {
        var htmlNamaDiklat = '<option value="-1">--Nama Diklat--</option>';

        $.each(result.data, function (i, row) {
          htmlNamaDiklat +=
            '<option value="' + row.id + '">' + row.namaDiklat + '</option>'
        });

        $namaDiklat.html(htmlNamaDiklat);
        // $namaDiklat.selectpicker();
      });
    }

    var url = 'ArsipMateri/readData';
    var data = {
      jenisDiklatId: $jenisDiklat.val(),
      publishStatusId: 2,
    };

    $listData.html('<tr><td colspan="4">Loading...</td></tr>');
    
    api.post(url, data, function (result) {
      rsArsipMateri = result.data;

      var html = '';

      $.each(rsArsipMateri, function (i, row) {
        html += this.renderList(i, row);
      }.bind(this));

      $listData.html(html);
    }.bind(this));
  };

  this.renderByNamaDiklat = function () {
    var filteredRs = rsArsipMateri.filter(function (data) {
      // var regex = new RegExp($namaDiklat.val(), 'gi');
      var found = false;

      // if (data.kode.match(regex))
      //   found = true;

      // if (data.namaDiklat.match(regex))
      if (data.diklatId == $namaDiklat.val())
        found = true;

      return found;
    });

    var html = '';

    $.each(filteredRs, function (i, row) {
      html += this.renderList(i, row);
    }.bind(this));

    $listData.html(html);
  };

  this.renderList = function(i, row) {

    var num = i + 1;
    var widyaiswara = row.pengajar.replace(/,/g, '<br>');

    var htmlFile = '';

    if (row.fileWord)
      htmlFile +=
        '<a href="' + localStorage.urlSite + 'asset/archive/' + row.fileWord + '">' +
          '<i class="fa fa-file-word-o" aria-hidden="true"></i>' +
        '</a>&nbsp;';
    if (row.fileExcel)
      htmlFile +=
        '<a href="' + localStorage.urlSite + 'asset/archive/' + row.fileExcel + '">' +
          '<i class="fa fa-file-excel-o" aria-hidden="true"></i>' +
        '</a>&nbsp;';
    if (row.filePowerPoint)
      htmlFile +=
        '<a href="' + localStorage.urlSite + 'asset/archive/' + row.filePowerPoint + '">' +
          '<i class="fa fa-file-powerpoint-o" aria-hidden="true"></i>' +
        '</a>&nbsp;';
    if (row.filePdf)
      htmlFile +=
        '<a href="' + localStorage.urlSite + 'asset/archive/' + row.filePdf + '">' +
          '<i class="fa fa-file-pdf-o" aria-hidden="true"></i>' +
        '</a>&nbsp;';

    var html = '<tr>' +
      '<td data-title="No" class="numbers">' + num + '.</td>' +
      '<td data-title="Mata Diklat">' + row.mataDiklat + '</td>' +
      '<td data-title="File" class="numbers">' + htmlFile + '</td>' +
      '<td data-title="Widyaiswara">' + widyaiswara + '</td>' +
    '</tr>';
    return html;
  };

  this.search = function () {
  };

};

scoopy.application.controllers.BahanAjar.prototype =
Object.create(scoopy.application.core.Controller.prototype);

scoopy.application.controllers.InfoAjar = function () {
  scoopy.application.core.Controller.call(this);
  var $scope = this.getScope('Content');
 
  var $calendar;
  var $jenisDiklat;
  var $namaDiklat;
  var $listData;

  var rsDiklat = [];

  var $btnCari;
  var $btnPdf;

  this.init = function () {
    $calendar = $scope.getRef('calendar').fullCalendar({
        themeSystem: 'bootstrap3',
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay,listMonth'
        },
        weekNumbers: true,
        eventLimit: true, // allow "more" link when too many events
        
    });
    
  };

  this.reload = function () {
    this.readData();
    
  };

  this.readData = function () {

    var Jenis = '';
    
    var url = 'Diklat/readDataJadwalDiklat';
    var data = {
      jenisDiklatId: Jenis,
    };

    api.post(url, data, function (result) {
      rsDiklat = result.data;
      $calendar = $scope.getRef('calendar').fullCalendar({
        themeSystem: 'bootstrap3',
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay,listMonth'
        },
        weekNumbers: true,
        eventLimit: true, // allow "more" link when too many events
                selectable: true,
                selectHelper: true,
                select: function(start, end) {
                    $('#create_modal input[name=start_date]').val(moment(start).format('YYYY-MM-DD'));
                    $('#create_modal input[name=end_date]').val(moment(end).format('YYYY-MM-DD'));
                    $('#create_modal').modal('show');
                    save();
                    $calendar.fullCalendar('unselect');
                },
        events: JSON.parse(rsDiklat),
        
    });
    }.bind(this));
  };

  this.driveEvents = function () {
    // $btnCari.on('click', this.searchData.bind(this));
    // $namaDiklat.on('keyup', this.renderByFilter.bind(this));
    // $btnPdf.on('click', this.createReportPdf.bind(this));
  };

  this.createReportPdf = function () {
    alert('helo sandri');
    // var url = 'Kelulusan/readDataAlumni';
    //
    // api.post(url, data, function (result) {
    //
    // });
  };

  this.searchData = function () {
    var url = 'Diklat/readDataAlumni';
    var data = {
      tahun: $tahun.val(),
      jenisDiklatId: $jenisDiklat.val(),
    };

    $listData.html('<tr>' +
    '<td colspan="2">Loading...</td>' +
    '</tr>');

    api.post(url, data, function (result) {

      rsDiklat = result.data;
      var htmlList = '';

      $.each(rsDiklat, function (i, row) {
        var num = i + 1;

        htmlList +=
          '<tr>' +
            '<td data-title="No" class="numbers">' + num + '.</td>' +
            '<td data-title="Program Diklat">' +
              '<a href="' + localStorage.urlSite + 'jadwal/detail_alumni/' + row.id +'">' +
                row.namaDiklatKode +
              '</a>' +
            '</td>' +
          '</tr>';
      });

      $listData.html(htmlList);

    }.bind(this));

  };

  this.renderByFilter = function () {

    var filteredRs = rsDiklat.filter(function (data) {
      var regex = new RegExp($namaDiklat.val(), 'gi');
      var found = false;

      if (data.namaDiklatKode.match(regex))
        found = true;

      return found;
    });

    var htmlList = '';

    $.each(filteredRs, function (i, row) {
      var num = i + 1;

      htmlList +=
        '<tr>' +
          '<td data-title="No" class="numbers">' + num + '.</td>' +
          '<td data-title="Program Diklat">' +
            '<a href="' + localStorage.urlSite + 'jadwal/detail_alumni/' + row.id +'">' +
              row.namaDiklatKode +
            '</a>' +
          '</td>' +
        '</tr>';
    });

    $listData.html(htmlList);
  };

};

scoopy.application.controllers.InfoAjar.prototype =
Object.create(scoopy.application.core.Controller.prototype);

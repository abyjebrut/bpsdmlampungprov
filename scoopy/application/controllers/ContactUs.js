scoopy.application.controllers.ContactUs = function() {
    scoopy.application.core.Controller.call(this);
    var $scope = this.getScope('Content');
    var $scpGuestBook = this.getScope('GuestBook');

    this.init = function() {

        // Google Maps

        var mapCanvas = document.getElementById('map');
        var mapOptions = {
            center: new google.maps.LatLng(LATITUDE, LONGITUDE),
            zoom: 15,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        var map = new google.maps.Map(mapCanvas, mapOptions);

        $scpGuestBook.$name = $scpGuestBook.getRef('name');
        $scpGuestBook.$email = $scpGuestBook.getRef('email');
        $scpGuestBook.$subject = $scpGuestBook.getRef('subject');
        $scpGuestBook.$message = $scpGuestBook.getRef('message');
        $scpGuestBook.$btnSendContactUs = $scpGuestBook.getRef('btnSendContactUs');
    };

    this.reload = function() {};

    this.driveEvents = function() {
        $scpGuestBook.$btnSendContactUs.on('click', this.sendMessage.bind(this));
    };

    this.sendMessage = function() {

        var url = 'GuestBook/createData';
        var data = {
            name: $scpGuestBook.$name.val(),
            email: $scpGuestBook.$email.val(),
            subject: $scpGuestBook.$subject.val(),
            message: $scpGuestBook.$message.val(),
        };

        $scpGuestBook.$btnSendContactUs.html('WAIT...');

        api.post(url, data, function(result) {

            switch (result.status) {
                case 'success':
                    $scpGuestBook.$name.val('');
                    $scpGuestBook.$email.val('');
                    $scpGuestBook.$subject.val('');
                    $scpGuestBook.$message.val('');

                    this.alert('Pesan sudah terkirim');
                    break;

                default:
                    var message = '<b>' + result.message + '</b><br><br>';

                    if (result.data.name)
                        message += result.data.name + '<br>';

                    if (result.data.email)
                        message += result.data.email + '<br>';

                    if (result.data.subject)
                        message += result.data.subject + '<br>';

                    if (result.data.message)
                        message += result.data.message + '<br>';

                    this.alert(message);
            }

            $scpGuestBook.$btnSendContactUs.html('KIRIM <i class="wow fadeIn fa fa-paper-plane"></i>');
        }.bind(this));

    };

};

scoopy.application.controllers.ContactUs.prototype =
    Object.create(scoopy.application.core.Controller.prototype);
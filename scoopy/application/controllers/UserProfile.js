scoopy.application.controllers.UserProfile = function () {
  scoopy.application.core.Controller.call(this);
  var $scope = this.getScope('Content');
  var $listData;
  var $linkCetak;
  var $linkDownload;
  var $judulLaporan;

  let overlay = document.getElementsByClassName("loading-overlay")[0];
  document.getElementById("load-button").addEventListener("click", (e) => overlay.classList.toggle("is-active"));
  document.getElementById("load-button-d").addEventListener("click", (e) => overlay.classList.toggle("is-active"));

  var $scpJudulLaporan = this.getScope('judulLaporan');

  this.init = function () {

    $scpJudulLaporan.$pendaftaranId = $scpJudulLaporan.getRef('pendaftaranId');
    $scpJudulLaporan.$judul = $scpJudulLaporan.getRef('judul');
    $scpJudulLaporan.$btnLaporan = $scpJudulLaporan.getRef('btnLaporan');
    $scpJudulLaporan.$btnBatalLaporan = $scpJudulLaporan.getRef('btnBatalLaporan');

    $listData = $scope.getRef('listData');
    $linkCetak = $scope.getRef('linkCetak');
    $linkDownload = $scope.getRef('linkDownload');
    $judulLaporan = $scope.getRef('judulLaporan');
  };

  this.reload = function () {
  };

  this.driveEvents = function () {
    $linkCetak.on('click', this.createPDF.bind(this));
    $linkDownload.on('click', this.downloadExcel.bind(this));
    $judulLaporan.on('click', this.showDiklatId.bind(this));
    $scpJudulLaporan.$btnLaporan.on('click', this.saveLaporan.bind(this));
  };

  this.showDiklatId = function (event) {
    var id = $(event.currentTarget).data('id');
    if (!id) {
      alert('Ulangi Lagi Click ')
      return
    }
    var url = 'PendaftaranDiklat/detailData';
    var data = {
      id: id,
    };

    api.post(url, data, function (result) {
      switch (result.status) {
        case 'success':
          $scpJudulLaporan.$judul.val(result.data.judulLaporan);
          break;
      }

    }.bind(this));
    
    $scpJudulLaporan.$pendaftaranId.val(id);
  };

  this.downloadExcel = function (event) {
    var id = $(event.currentTarget).data('id');
    if (!id) {
      alert('Ulangi Lagi Click ')
      return
    }

    var url = 'Diklat/createExcelDetail';
    var data = {
      id: id,
    };

    api.post(url, data, function (result) {

      switch (result.status) {
        case 'success':

          window.open(result.data);
          document.getElementById("load-button-d").addEventListener("focus", (e) => overlay.classList.replace("is-active", "is-hide"));
          // location.reload(true);
          break;

        default:
          this.alert(result.message);
      }

    }.bind(this));

  };

  this.saveLaporan = function () {

    var url = 'PendaftaranDiklat/saveLaporan';
    var data = {
      id: $scpJudulLaporan.$pendaftaranId.val(),
      judul: $scpJudulLaporan.$judul.val(),
    };

    api.post(url, data, function (result) {
      switch (result.status) {
        case 'success':
          this.alert(result.message,function () {
            $scpJudulLaporan.$btnBatalLaporan.trigger('click');
          });
          break;
        default:
          this.alert(result.message,function () {
            $scpJudulLaporan.$btnBatalLaporan.trigger('click');
          });
      }

    }.bind(this));

  };

  this.createPDF = function (event) {

    var url = 'PendaftaranDiklat/createPDFDetail';
    var data = {
      id: $(event.currentTarget).data('id'),
    };

    api.post(url, data, function (result) {


      switch (result.status) {
        case 'success':
          document.getElementById("load-button").addEventListener("focus", (e) => overlay.classList.replace("is-active", "is-hide"));

          window.open(result.data);
          // location.reload(true);
          break;
        default:
          this.alert(result.message);
      }

    }.bind(this));

  };

};

scoopy.application.controllers.UserProfile.prototype =
  Object.create(scoopy.application.core.Controller.prototype);

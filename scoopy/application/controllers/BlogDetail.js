scoopy.application.controllers.BlogDetail = function () {
  scoopy.application.core.Controller.call(this);
  var $scope = this.getScope('Content');

  var $author = $scope.getRef('author');
  var $email = $scope.getRef('email');
  var $url = $scope.getRef('url');
  var $comment = $scope.getRef('comment');
  var $btnSubmit = $scope.getRef('btnSubmit');

  var $commentsCount = $scope.getRef('commentsCount');
  var $commentsList = $scope.getRef('commentsList');

  // Blog
  var pagesId = 1;

  this.init = function () {
    this.readCommentsList();
  };

  this.driveEvents = function () {
    $btnSubmit.on('click', this.sendMessage.bind(this));
  };

  this.readCommentsList = function () {

    var url = 'Comments/readData';
    var data = {
      // Blog
      commentsAtId: pagesId,
      postsId: _postsId,
      // Query Condition
      orderBy: 'id',
      reverse: 0,
    };

    $commentsList.html('Loading...');

    api.post(url, data, function (result) {
      var html = '';
      $commentsCount.html(result.data.length);

      $.each(result.data, function (i, row) {
        var contactHtml = '';

        if (row.email)
            contactHtml +=
            '<small> | <a href="mailto:' + row.email + '">' +
              row.email +
            '</a></small>';

        if (row.url)
            contactHtml +=
            '<small> | <a href="' + row.url + '">' +
              row.url +
            '</a></small>';

        html +=
          '<article>' +
            '<h1>' +
              row.author + contactHtml +
              '<small style="float: right">' + row.postedOn + '</small>' +
            '</h1>' +
            '<p>' + row.comment + '</p>' +
          '</article>';
      });

      $commentsList.html(html);
    });
  };

  this.sendMessage = function () {
    var url = 'Comments/createData';
    var data = {
      author: $author.val(),
      email: $email.val(),
      url: $url.val(),
      comment: $comment.val(),
      // Blog
      commentsAtId: pagesId,
      postsId: _postsId,
      // Publish
      publishStatusId: 2,
    };

    $btnSubmit.html('Wait...');

    api.post(url, data, function (result) {

      switch (result.status) {
        case 'success':
          $author.val('');
          $email.val('');
          $url.val('');
          $comment.val('');

          this.readCommentsList();

          break;

        default:
          var message = '<b>' + result.message + '</b><br><br>';

          if (result.data.author)
            message += result.data.author + '<br>';

          if (result.data.email)
            message += result.data.email + '<br>';

          if (result.data.subject)
            message += result.data.subject + '<br>';

          if (result.data.comment)
            message += result.data.comment + '<br>';

          this.alert(message);
      }

      $btnSubmit.html('Submit');
    }.bind(this));
  };

};

scoopy.application.controllers.BlogDetail.prototype =
Object.create(scoopy.application.core.Controller.prototype);

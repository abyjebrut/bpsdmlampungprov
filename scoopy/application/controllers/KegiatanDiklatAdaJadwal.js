scoopy.application.controllers.KegiatanDiklat = function () {
  scoopy.application.core.Controller.call(this);
  var $scope = this.getScope('Content');

  var $jenisDiklat;
  var $keyword;
  var $listData;
  var $linkDownload;

  var rsDiklat = [];

  this.init = function () {
    $jenisDiklat = $scope.getRef('jenisDiklat');
    $keyword = $scope.getRef('keyword');
    $listData = $scope.getRef('listData');
    $linkDownload = $scope.getRef('linkDownload');
  };

  this.reload = function () {
    this.readData();
  };

  this.readData = function () {

    if ($jenisDiklat.val() < 0)
      var Jenis = '';
    else
      var Jenis = $jenisDiklat.val();
      var url = 'Diklat/readData';
      var data = {
        jenisDiklatId: Jenis,
        publishStatusId: 2,
      };

    $listData.html('<tr><td colspan="8">Loading...</td></tr>');

    api.post(url, data, function (result) {
      rsDiklat = result.data;
      var html = '';

      $.each(rsDiklat, function (i, row) {
        html += this.renderList(i, row);
      }.bind(this));

      $listData.html(html);
      
      $listData.find('.act-detail').on('click', this.createPDF.bind(this));

    }.bind(this));
  };

  this.driveEvents = function () {
    $jenisDiklat.on('change', this.renderByJenis.bind(this));
    $keyword.on('keyup', this.renderBySearching.bind(this));
    $linkDownload.on('click', this.createPDF.bind(this));
  };

  this.renderByJenis = function () {

    $keyword.val('');
    $keyword.focus();
   
    if ($jenisDiklat.val() < 0)
      var Jenis = '';
    else
      var Jenis = $jenisDiklat.val();

    var url = 'Diklat/readData';
    var data = {
      jenisDiklatId: Jenis,
      publishStatusId: 2,
    };

    $listData.html('<tr><td colspan="8">Loading...</td></tr>');
    
    api.post(url, data, function (result) {
      rsDiklat = result.data;

      var html = '';

      $.each(rsDiklat, function (i, row) {
        html += this.renderList(i, row);
      }.bind(this));

      $listData.html(html);
      $listData.find('.act-detail').on('click', this.createPDF.bind(this));
    }.bind(this));
  };

  this.renderBySearching = function () {

    var filteredRs = rsDiklat.filter(function (data) {
      var regex = new RegExp($keyword.val(), 'gi');
      var found = false;

      if (data.kode.match(regex))
        found = true;

      if (data.namaDiklat.match(regex))
        found = true;

      return found;
    });

    var html = '';

    $.each(filteredRs, function (i, row) {
      html += this.renderList(i, row);
    }.bind(this));

    $listData.html(html);
    $listData.find('.act-detail').on('click', this.createPDF.bind(this));
  };

  this.renderList = function(i, row) {
    if (AKUN_ID > 0 ){
      var urlPendaftaran = '<td data-title="Daftar" class="numbers">' +
      '<a href="' + localStorage.urlSite + '/pendaftaran_diklat/' + row.id + '"><i class="fa fa-cloud-upload" aria-hidden="true"></i></a>' +
      '</td>' ;

      var urlDownload = '<td data-title="Daftar" class="numbers">' +
      '<a class="act-detail" data-id="' + row.id + '" href="javascript:"  ><i class="fa fa-file-excel-o" aria-hidden="true"></i></a>' +
      '</td>' ;
    }else{
      var urlPendaftaran = '<td data-title="Daftar" class="numbers">' +
      '<a href="javascript:" data-toggle="modal" data-target="#alertLogin"><i class="fa fa-cloud-upload" aria-hidden="true"></i></a>' +
      '</td>' ;

      // var urlDownload = '<td data-title="Daftar" class="numbers">' +
      // '<a href="javascript:" data-toggle="modal" data-target="#alertLogin"><i class="fa fa-file-excel-o" aria-hidden="true"></i></a>' +
      // '</td>' ;
    }
    
    
    var num = i + 1;
    var html = '<tr>' +
      '<td data-title="No" class="numbers">' + num + '.</td>' +
      '<td data-title="Kode">' + row.kode + '</td>' +
      '<td data-title="Nama Diklat">' + row.namaDiklat + '</td>' +
      '<td data-title="Pelaksanaan">' +
      row.pelaksanaanMulai +' s.d. ' + row.pelaksanaanSelesai +
      '</td>' +
      '<td data-title="Kuota">' + row.kuota + '</td>' +
      '<td data-title="Status">' + row.statusDiklat + '</td>' +
       urlPendaftaran + urlDownload +
    '</tr>';
    
    return html;
  };

  this.search = function () {
  };

  this.createPDF = function (event) {
    var id =$(event.currentTarget).data('id');
    if(!id){
      alert('ok')
      return
    }


    var url = 'Diklat/createExcelDetail';
    var data = {
      id: id,
    };

    api.post(url, data, function (result) {

      switch (result.status) {
        case 'success':
          window.open(result.data);
          break;

        default:
          this.alert(result.message);
      }

    }.bind(this));

  };

};

scoopy.application.controllers.KegiatanDiklat.prototype =
Object.create(scoopy.application.core.Controller.prototype);

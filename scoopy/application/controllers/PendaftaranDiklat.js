scoopy.application.controllers.PendaftaranDiklat = function () {
  scoopy.application.core.Controller.call(this);
  var $scope = this.getScope("Content");

  var $kodePeserta;
  var $email;
  var $idDiklat;
  var $nip;
  var $namaLengkap;
  var $nik;
  var $npwp;
  var $pangkatGolongan;
  var $jabatanEselon;
  var $unitKerja;
  var $instansi;
  var $namaInstansi;
  var $pendidikanTerakhir;
  var $jenisKelamin;
  var $tempatLahir;
  var $tanggalLahir;
  var $agama;
  var $alamatLengkap;
  var $alamatKantor;
  var $telpHp;
  var $foto;
  var $suratPerintahTugas;
  var $suratPernyataanBebasTugasKedinasan;
  var $suratKeputusanCPNS;
  var $suratKeputusanPangkat;
  var $suratKeputusanJabatanTerakhir;
  var $ijazahTerakhir;
  var $suratKeteranganDokterdariRumahSakitPemerintah;
  var $askesBPJSatauKIS;
  var $suratKesehatan;
  var $jenisDiklatId;

  var $btnDaftar;
  var $btnBatal;

  this.init = function () {
    $kodePeserta = $scope.getRef("kodePeserta");
    $email = $scope.getRef("email");
    $idDiklat = $scope.getRef("idDiklat");
    $nip = $scope.getRef("nip");
    $namaLengkap = $scope.getRef("namaLengkap");
    $nik = $scope.getRef("nik");
    $npwp = $scope.getRef("npwp");
    $pangkatGolongan = $scope.getRef("pangkatGolongan");
    $jabatanEselon = $scope.getRef("jabatanEselon");
    $unitKerja = $scope.getRef("unitKerja");
    $instansi = $scope.getRef("instansi");
    $namaInstansi = $scope.getRef("namaInstansi");
    $pendidikanTerakhir = $scope.getRef("pendidikanTerakhir");
    $jenisKelamin = $scope.getRef("jenisKelamin");
    $tempatLahir = $scope.getRef("tempatLahir");
    $tanggalLahir = $scope.getRef("tanggalLahir");
    $agama = $scope.getRef("agama");
    $alamatLengkap = $scope.getRef("alamatLengkap");
    $alamatKantor = $scope.getRef("alamatKantor");
    $telpHp = $scope.getRef("telpHp");
    $jenisDiklatId = $scope.getRef("jenisDiklatId");

    $fotoPreview = $scope.getRef("fotoPreview");
    $foto = $scope.getRef("foto");
    $uploadIndicator = $scope.getRef("uploadIndicator");

    $suratPerintahTugasPreview = $scope.getRef("suratPerintahTugasPreview");
    $suratPerintahTugasPreview.hide();
    $suratPerintahTugas = $scope.getRef("suratPerintahTugas");
    $uploadIndicatorSuratPerintahTugas = $scope.getRef(
      "uploadIndicatorSuratPerintahTugas"
    );

    $suratPernyataanBebasTugasKedinasanPreview = $scope.getRef(
      "suratPernyataanBebasTugasKedinasanPreview"
    );
    $suratPernyataanBebasTugasKedinasanPreview.hide();
    $suratPernyataanBebasTugasKedinasan = $scope.getRef(
      "suratPernyataanBebasTugasKedinasan"
    );
    $uploadIndicatorSuratPernyataanBebasTugasKedinasan = $scope.getRef(
      "uploadIndicatorSuratPernyataanBebasTugasKedinasan"
    );

    $suratKeputusanCPNSPreview = $scope.getRef("suratKeputusanCPNSPreview");
    $suratKeputusanCPNSPreview.hide();
    $suratKeputusanCPNS = $scope.getRef("suratKeputusanCPNS");
    $uploadIndicatorSuratKeputusanCPNS = $scope.getRef(
      "uploadIndicatorSuratKeputusanCPNS"
    );

    $suratKeputusanPangkatPreview = $scope.getRef(
      "suratKeputusanPangkatPreview"
    );
    $suratKeputusanPangkatPreview.hide();
    $suratKeputusanPangkat = $scope.getRef("suratKeputusanPangkat");
    $uploadIndicatorSuratKeputusanPangkat = $scope.getRef(
      "uploadIndicatorSuratKeputusanPangkat"
    );

    $suratKeputusanJabatanTerakhirPreview = $scope.getRef(
      "suratKeputusanJabatanTerakhirPreview"
    );
    $suratKeputusanJabatanTerakhirPreview.hide();
    $suratKeputusanJabatanTerakhir = $scope.getRef(
      "suratKeputusanJabatanTerakhir"
    );
    $uploadIndicatorSuratKeputusanJabatanTerakhir = $scope.getRef(
      "uploadIndicatorSuratKeputusanJabatanTerakhir"
    );

    $ijazahTerakhirPreview = $scope.getRef("ijazahTerakhirPreview");
    $ijazahTerakhirPreview.hide();
    $ijazahTerakhir = $scope.getRef("ijazahTerakhir");
    $uploadIndicatorIjazahTerakhir = $scope.getRef(
      "uploadIndicatorIjazahTerakhir"
    );

    $suratKesehatanPreview = $scope.getRef("suratKesehatanPreview");
    $suratKesehatanPreview.hide();
    $suratKesehatan = $scope.getRef("suratKesehatan");
    $uploadIndicatorSuratKesehatan = $scope.getRef(
      "uploadIndicatorSuratKesehatan"
    );

    $suratKeteranganDokterdariRumahSakitPemerintahPreview = $scope.getRef(
      "suratKeteranganDokterdariRumahSakitPemerintahPreview"
    );
    $suratKeteranganDokterdariRumahSakitPemerintahPreview.hide();
    $suratKeteranganDokterdariRumahSakitPemerintah = $scope.getRef(
      "suratKeteranganDokterdariRumahSakitPemerintah"
    );
    $uploadIndicatorSuratKeteranganDokterdariRumahSakitPemerintah = $scope.getRef(
      "uploadIndicatorSuratKeteranganDokterdariRumahSakitPemerintah"
    );

    $askesBPJSatauKISPreview = $scope.getRef("askesBPJSatauKISPreview");
    $askesBPJSatauKISPreview.hide();
    $askesBPJSatauKIS = $scope.getRef("askesBPJSatauKIS");
    $uploadIndicatorAskesBPJSatauKIS = $scope.getRef(
      "uploadIndicatorAskesBPJSatauKIS"
    );

    $btnDaftar = $scope.getRef("btnDaftar");
    $btnBatal = $scope.getRef("btnBatal");
  };

  this.reload = function () { };

  this.driveEvents = function () {
    $instansi.on("change", this.toggleNamaInstansi.bind(this));
    $foto.on("change", this.uploadFoto.bind(this));
    $suratPerintahTugas.on(
      "change",
      this.uploadPdfsuratPerintahTugas.bind(this)
    );
    $suratPernyataanBebasTugasKedinasan.on(
      "change",
      this.uploadPdfsuratPernyataanBebasTugasKedinasan.bind(this)
    );
    $suratKeputusanCPNS.on(
      "change",
      this.uploadPdfsuratKeputusanCPNS.bind(this)
    );
    $suratKeputusanPangkat.on(
      "change",
      this.uploadPdfsuratKeputusanPangkat.bind(this)
    );
    $suratKeputusanJabatanTerakhir.on(
      "change",
      this.uploadPdfsuratKeputusanJabatanTerakhir.bind(this)
    );
    $ijazahTerakhir.on("change", this.uploadPdfijazahTerakhir.bind(this));
    $suratKesehatan.on("change", this.uploadPdfSuratKesehatan.bind(this));
    $suratKeteranganDokterdariRumahSakitPemerintah.on(
      "change",
      this.uploadPdfsuratKeteranganDokterdariRumahSakitPemerintah.bind(this)
    );
    $askesBPJSatauKIS.on("change", this.uploadPdfaskesBPJSatauKIS.bind(this));

    $btnDaftar.on("click", this.daftarDiklat.bind(this));
    $btnBatal.on("click", this.batalDaftar.bind(this));
  };

  this.toggleNamaInstansi = function () {
    if ($instansi.val() == "-1") $namaInstansi.slideDown();
    else $namaInstansi.slideUp();
  };

  this.uploadFoto = function (event) {
    var formData = new FormData();

    $.each(event.target.files, function (key, value) {
      formData.append(key, value);
    });

    $uploadIndicator.html("Loading...");
    $foto.prop("disabled", true);

    $.ajax({
      url: config.url.api + "System/uploadImage",
      type: "POST",
      data: formData,
      cache: false,
      dataType: "json",
      processData: false,
      contentType: false,
      success: function (result, textStatus, jqXHR) {
        if (result.status == "success") {
          $fotoPreview
            .load(function () {
              var splitFotoValue = result.data.split("/");
              var fotoValue = splitFotoValue[splitFotoValue.length - 1];

              $foto.data("value", fotoValue);

              $uploadIndicator.html("Pilih File");
              $foto.prop("disabled", false);
            })
            .prop("src", result.data);
        } else {
          $uploadIndicator.html("Pilih File");
          $foto.prop("disabled", false);
          $foto.val("");

          this.alert(result.message);
        }
      }.bind(this),
      error: function (jqXHR, textStatus, errorThrown) {
        console.log(textStatus);
        // $loading.hide();
        $foto.prop("disabled", false);
      }.bind(this),
    });
  };

  this.uploadPdfsuratPerintahTugas = function (event) {
    var formData = new FormData();

    $.each(event.target.files, function (key, value) {
      formData.append(key, value);
    });

    $uploadIndicatorSuratPerintahTugas.html("Loading...");
    $suratPerintahTugas.prop("disabled", true);

    $.ajax({
      url: config.url.api + "System/uploadPdf",
      type: "POST",
      data: formData,
      cache: false,
      dataType: "json",
      processData: false,
      contentType: false,
      success: function (result, textStatus, jqXHR) {
        if (result.status == "success") {
          $suratPerintahTugasPreview.show();
          $suratPerintahTugasPreview
            .load(function () {
              var splitValue = result.data.split("/");
              var suratPerintahTugasValue = splitValue[splitValue.length - 1];

              $suratPerintahTugas.data("value", suratPerintahTugasValue);

              $uploadIndicatorSuratPerintahTugas.html("Pilih File");
              $suratPerintahTugas.prop("disabled", false);
            })
            .prop("src", result.data);
        } else {
          $uploadIndicatorSuratPerintahTugas.html("Pilih File");
          $suratPerintahTugas.prop("disabled", false);
          $suratPerintahTugas.val("");

          this.alert(result.message);
        }
      }.bind(this),
      error: function (jqXHR, textStatus, errorThrown) {
        $suratPerintahTugas.prop("disabled", false);
      }.bind(this),
    });
  };

  this.uploadPdfsuratPernyataanBebasTugasKedinasan = function (event) {
    var formData = new FormData();

    $.each(event.target.files, function (key, value) {
      formData.append(key, value);
    });

    $uploadIndicatorSuratPernyataanBebasTugasKedinasan.html("Loading...");
    $suratPernyataanBebasTugasKedinasan.prop("disabled", true);

    $.ajax({
      url: config.url.api + "System/uploadPdf",
      type: "POST",
      data: formData,
      cache: false,
      dataType: "json",
      processData: false,
      contentType: false,
      success: function (result, textStatus, jqXHR) {
        if (result.status == "success") {
          $suratPernyataanBebasTugasKedinasanPreview.show();
          $suratPernyataanBebasTugasKedinasanPreview
            .load(function () {
              var splitValue = result.data.split("/");
              var suratPernyataanBebasTugasKedinasanValue =
                splitValue[splitValue.length - 1];

              $suratPernyataanBebasTugasKedinasan.data(
                "value",
                suratPernyataanBebasTugasKedinasanValue
              );

              $uploadIndicatorSuratPernyataanBebasTugasKedinasan.html(
                "Pilih File"
              );
              $suratPernyataanBebasTugasKedinasan.prop("disabled", false);
            })
            .prop("src", result.data);
        } else {
          $uploadIndicatorSuratPernyataanBebasTugasKedinasan.html("Pilih File");
          $suratPernyataanBebasTugasKedinasan.prop("disabled", false);
          $suratPernyataanBebasTugasKedinasan.val("");

          this.alert(result.message);
        }
      }.bind(this),
      error: function (jqXHR, textStatus, errorThrown) {
        $suratPernyataanBebasTugasKedinasan.prop("disabled", false);
      }.bind(this),
    });
  };

  this.uploadPdfsuratKeputusanCPNS = function (event) {
    var formData = new FormData();

    $.each(event.target.files, function (key, value) {
      formData.append(key, value);
    });

    $uploadIndicatorSuratKeputusanCPNS.html("Loading...");
    $suratKeputusanCPNS.prop("disabled", true);

    $.ajax({
      url: config.url.api + "System/uploadPdf",
      type: "POST",
      data: formData,
      cache: false,
      dataType: "json",
      processData: false,
      contentType: false,
      success: function (result, textStatus, jqXHR) {
        if (result.status == "success") {
          $suratKeputusanCPNSPreview.show();
          $suratKeputusanCPNSPreview
            .load(function () {
              var splitValue = result.data.split("/");
              var suratKeputusanCPNSValue = splitValue[splitValue.length - 1];

              $suratKeputusanCPNS.data("value", suratKeputusanCPNSValue);

              $uploadIndicatorSuratKeputusanCPNS.html("Pilih File");
              $suratKeputusanCPNS.prop("disabled", false);
            })
            .prop("src", result.data);
        } else {
          $uploadIndicatorSuratKeputusanCPNS.html("Pilih File");
          $suratKeputusanCPNS.prop("disabled", false);
          $suratKeputusanCPNS.val("");

          this.alert(result.message);
        }
      }.bind(this),
      error: function (jqXHR, textStatus, errorThrown) {
        $suratKeputusanCPNS.prop("disabled", false);
      }.bind(this),
    });
  };

  this.uploadPdfsuratKeputusanPangkat = function (event) {
    var formData = new FormData();

    $.each(event.target.files, function (key, value) {
      formData.append(key, value);
    });

    $uploadIndicatorSuratKeputusanPangkat.html("Loading...");
    $suratKeputusanPangkat.prop("disabled", true);

    $.ajax({
      url: config.url.api + "System/uploadPdf",
      type: "POST",
      data: formData,
      cache: false,
      dataType: "json",
      processData: false,
      contentType: false,
      success: function (result, textStatus, jqXHR) {
        if (result.status == "success") {
          $suratKeputusanPangkatPreview.show();
          $suratKeputusanPangkatPreview
            .load(function () {
              var splitValue = result.data.split("/");
              var suratKeputusanPangkatValue =
                splitValue[splitValue.length - 1];

              $suratKeputusanPangkat.data("value", suratKeputusanPangkatValue);

              $uploadIndicatorSuratKeputusanPangkat.html("Pilih File");
              $suratKeputusanPangkat.prop("disabled", false);
            })
            .prop("src", result.data);
        } else {
          $uploadIndicatorSuratKeputusanPangkat.html("Pilih File");
          $suratKeputusanPangkat.prop("disabled", false);
          $suratKeputusanPangkat.val("");

          this.alert(result.message);
        }
      }.bind(this),
      error: function (jqXHR, textStatus, errorThrown) {
        $suratKeputusanPangkat.prop("disabled", false);
      }.bind(this),
    });
  };

  this.uploadPdfsuratKeputusanJabatanTerakhir = function (event) {
    var formData = new FormData();

    $.each(event.target.files, function (key, value) {
      formData.append(key, value);
    });

    $uploadIndicatorSuratKeputusanJabatanTerakhir.html("Loading...");
    $suratKeputusanJabatanTerakhir.prop("disabled", true);

    $.ajax({
      url: config.url.api + "System/uploadPdf",
      type: "POST",
      data: formData,
      cache: false,
      dataType: "json",
      processData: false,
      contentType: false,
      success: function (result, textStatus, jqXHR) {
        if (result.status == "success") {
          $suratKeputusanJabatanTerakhirPreview.show();
          $suratKeputusanJabatanTerakhirPreview
            .load(function () {
              var splitValue = result.data.split("/");
              var suratKeputusanJabatanTerakhirValue =
                splitValue[splitValue.length - 1];

              $suratKeputusanJabatanTerakhir.data(
                "value",
                suratKeputusanJabatanTerakhirValue
              );

              $uploadIndicatorSuratKeputusanJabatanTerakhir.html("Pilih File");
              $suratKeputusanJabatanTerakhir.prop("disabled", false);
            })
            .prop("src", result.data);
        } else {
          $uploadIndicatorSuratKeputusanJabatanTerakhir.html("Pilih File");
          $suratKeputusanJabatanTerakhir.prop("disabled", false);
          $suratKeputusanJabatanTerakhir.val("");

          this.alert(result.message);
        }
      }.bind(this),
      error: function (jqXHR, textStatus, errorThrown) {
        $suratKeputusanJabatanTerakhir.prop("disabled", false);
      }.bind(this),
    });
  };

  this.uploadPdfijazahTerakhir = function (event) {
    var formData = new FormData();

    $.each(event.target.files, function (key, value) {
      formData.append(key, value);
    });

    $uploadIndicatorIjazahTerakhir.html("Loading...");
    $ijazahTerakhir.prop("disabled", true);

    $.ajax({
      url: config.url.api + "System/uploadPdf",
      type: "POST",
      data: formData,
      cache: false,
      dataType: "json",
      processData: false,
      contentType: false,
      success: function (result, textStatus, jqXHR) {
        if (result.status == "success") {
          $ijazahTerakhirPreview.show();
          $ijazahTerakhirPreview
            .load(function () {
              var splitValue = result.data.split("/");
              var ijazahTerakhirValue = splitValue[splitValue.length - 1];

              $ijazahTerakhir.data("value", ijazahTerakhirValue);

              $uploadIndicatorIjazahTerakhir.html("Pilih File");
              $ijazahTerakhir.prop("disabled", false);
            })
            .prop("src", result.data);
        } else {
          $uploadIndicatorIjazahTerakhir.html("Pilih File");
          $ijazahTerakhir.prop("disabled", false);
          $ijazahTerakhir.val("");

          this.alert(result.message);
        }
      }.bind(this),
      error: function (jqXHR, textStatus, errorThrown) {
        $ijazahTerakhir.prop("disabled", false);
      }.bind(this),
    });
  };

  this.uploadPdfSuratKesehatan = function (event) {
    var formData = new FormData();

    $.each(event.target.files, function (key, value) {
      formData.append(key, value);
    });

    $uploadIndicatorSuratKesehatan.html("Loading...");
    $suratKesehatan.prop("disabled", true);

    $.ajax({
      url: config.url.api + "System/uploadPdf",
      type: "POST",
      data: formData,
      cache: false,
      dataType: "json",
      processData: false,
      contentType: false,
      success: function (result, textStatus, jqXHR) {
        if (result.status == "success") {
          $suratKesehatanPreview.show();
          $suratKesehatanPreview
            .load(function () {
              var splitValue = result.data.split("/");
              var suratKesehatanValue = splitValue[splitValue.length - 1];

              $suratKesehatan.data("value", suratKesehatanValue);

              $uploadIndicatorSuratKesehatan.html("Pilih File");
              $suratKesehatan.prop("disabled", false);
            })
            .prop("src", result.data);
        } else {
          $uploadIndicatorSuratKesehatan.html("Pilih File");
          $suratKesehatan.prop("disabled", false);
          $suratKesehatan.val("");

          this.alert(result.message);
        }
      }.bind(this),
      error: function (jqXHR, textStatus, errorThrown) {
        $suratKesehatan.prop("disabled", false);
      }.bind(this),
    });
  };

  this.uploadPdfsuratKeteranganDokterdariRumahSakitPemerintah = function (
    event
  ) {
    var formData = new FormData();

    $.each(event.target.files, function (key, value) {
      formData.append(key, value);
    });

    $uploadIndicatorSuratKeteranganDokterdariRumahSakitPemerintah.html(
      "Loading..."
    );
    $suratKeteranganDokterdariRumahSakitPemerintah.prop("disabled", true);

    $.ajax({
      url: config.url.api + "System/uploadPdf",
      type: "POST",
      data: formData,
      cache: false,
      dataType: "json",
      processData: false,
      contentType: false,
      success: function (result, textStatus, jqXHR) {
        if (result.status == "success") {
          $suratKeteranganDokterdariRumahSakitPemerintahPreview.show();
          $suratKeteranganDokterdariRumahSakitPemerintahPreview
            .load(function () {
              var splitValue = result.data.split("/");
              var suratKeteranganDokterdariRumahSakitPemerintahValue =
                splitValue[splitValue.length - 1];

              $suratKeteranganDokterdariRumahSakitPemerintah.data(
                "value",
                suratKeteranganDokterdariRumahSakitPemerintahValue
              );

              $uploadIndicatorSuratKeteranganDokterdariRumahSakitPemerintah.html(
                "Pilih File"
              );
              $suratKeteranganDokterdariRumahSakitPemerintah.prop(
                "disabled",
                false
              );
            })
            .prop("src", result.data);
        } else {
          $uploadIndicatorSuratKeteranganDokterdariRumahSakitPemerintah.html(
            "Pilih File"
          );
          $suratKeteranganDokterdariRumahSakitPemerintah.prop(
            "disabled",
            false
          );
          $suratKeteranganDokterdariRumahSakitPemerintah.val("");

          this.alert(result.message);
        }
      }.bind(this),
      error: function (jqXHR, textStatus, errorThrown) {
        $suratKeteranganDokterdariRumahSakitPemerintah.prop("disabled", false);
      }.bind(this),
    });
  };

  this.uploadPdfaskesBPJSatauKIS = function (event) {
    var formData = new FormData();

    $.each(event.target.files, function (key, value) {
      formData.append(key, value);
    });

    $uploadIndicatorAskesBPJSatauKIS.html("Loading...");
    $askesBPJSatauKIS.prop("disabled", true);

    $.ajax({
      url: config.url.api + "System/uploadPdf",
      type: "POST",
      data: formData,
      cache: false,
      dataType: "json",
      processData: false,
      contentType: false,
      success: function (result, textStatus, jqXHR) {
        if (result.status == "success") {
          $askesBPJSatauKISPreview.show();
          $askesBPJSatauKISPreview
            .load(function () {
              var splitValue = result.data.split("/");
              var askesBPJSatauKISValue = splitValue[splitValue.length - 1];

              $askesBPJSatauKIS.data("value", askesBPJSatauKISValue);

              $uploadIndicatorAskesBPJSatauKIS.html("Pilih File");
              $askesBPJSatauKIS.prop("disabled", false);
            })
            .prop("src", result.data);
        } else {
          $uploadIndicatorAskesBPJSatauKIS.html("Pilih File");
          $askesBPJSatauKIS.prop("disabled", false);
          $askesBPJSatauKIS.val("");

          this.alert(result.message);
        }
      }.bind(this),
      error: function (jqXHR, textStatus, errorThrown) {
        $askesBPJSatauKIS.prop("disabled", false);
      }.bind(this),
    });
  };

  this.daftarDiklat = function () {
    var _jenisKelaminId = "";
    var jumlahUploadDiklat = 0;

    $.each($jenisKelamin, function (i, jenisKelamin) {
      if ($(jenisKelamin).is(":checked"))
        _jenisKelaminId = $(jenisKelamin).val();
    });

    var _instansiLainnya = "";
    if ($instansi.val() == "-1") var _instansiLainnya = $namaInstansi.val();

    var splitTanggalLahir = $tanggalLahir.val().split("/");
    if (splitTanggalLahir.length == 3) {
      var _tanggalLahir =
        splitTanggalLahir[2] +
        "-" +
        splitTanggalLahir[1] +
        "-" +
        splitTanggalLahir[0];
    } else {
      var _tanggalLahir = "";
    }

    if (!$foto.data("value")) var _foto = "";
    else var _foto = $foto.data("value");

    if (!$suratPerintahTugas.data("value")) var _suratPerintahTugas = "";
    else {
      var _suratPerintahTugas = $suratPerintahTugas.data("value");
      jumlahUploadDiklat++;
    }

    if (!$suratPernyataanBebasTugasKedinasan.data("value"))
      var _suratPernyataanBebasTugasKedinasan = "";
    else {
      var _suratPernyataanBebasTugasKedinasan = $suratPernyataanBebasTugasKedinasan.data(
        "value"
      );
      jumlahUploadDiklat++;
    }

    if (!$suratKeputusanCPNS.data("value")) var _suratKeputusanCPNS = "";
    else {
      var _suratKeputusanCPNS = $suratKeputusanCPNS.data("value");
      jumlahUploadDiklat++;
    }

    if (!$suratKeputusanPangkat.data("value")) var _suratKeputusanPangkat = "";
    else {
      var _suratKeputusanPangkat = $suratKeputusanPangkat.data("value");
      jumlahUploadDiklat++;
    }

    if (!$suratKeputusanJabatanTerakhir.data("value"))
      var _suratKeputusanJabatanTerakhir = "";
    else {
      var _suratKeputusanJabatanTerakhir = $suratKeputusanJabatanTerakhir.data(
        "value"
      );
      jumlahUploadDiklat++;
    }

    if (!$ijazahTerakhir.data("value")) var _ijazahTerakhir = "";
    else {
      var _ijazahTerakhir = $ijazahTerakhir.data("value");
      jumlahUploadDiklat++;
    }

    if (!$suratKesehatan.data("value")) var _suratKesehatan = "";
    else {
      var _suratKesehatan = $suratKesehatan.data("value");
      jumlahUploadDiklat++;
    }

    if (!$suratKeteranganDokterdariRumahSakitPemerintah.data("value"))
      var _suratKeteranganDokterdariRumahSakitPemerintah = "";
    else {
      var _suratKeteranganDokterdariRumahSakitPemerintah = $suratKeteranganDokterdariRumahSakitPemerintah.data(
        "value"
      );
      jumlahUploadDiklat++;
    }

    if (!$askesBPJSatauKIS.data("value")) var _askesBPJSatauKIS = "";
    else {
      var _askesBPJSatauKIS = $askesBPJSatauKIS.data("value");
      jumlahUploadDiklat++;
    }
    var status = "Approve";
    var pesan = "Pendaftaran Diklat sukses";
    if ($jenisDiklatId.val() == 11) {
      status = "Pending";
      pesan = "Pendaftaran Diklat sukses Tunggu Konfirmasi Admin via Email ";
    }

    var url = "PendaftaranDiklat/daftarDiklat";
    var data = {
      diklatId: $idDiklat.val(),
      akunId: AKUN_ID,
      nama: $namaLengkap.val(),
      nik: $nik.val(),
      npwp: $npwp.val(),
      status: status,
      jumlahUploadDiklat: jumlahUploadDiklat,
      pangkatGolonganId: $pangkatGolongan.val(),
      jabatanEselon: $jabatanEselon.val(),
      unitKerja: $unitKerja.val(),
      instansiId: $instansi.val(),
      instansiLainnya: _instansiLainnya,
      pendidikanTerakhirId: $pendidikanTerakhir.val(),
      jenisKelaminId: _jenisKelaminId,
      tempatLahir: $tempatLahir.val(),
      tanggalLahir: _tanggalLahir,
      agamaId: $agama.val(),
      alamat: $alamatLengkap.val(),
      alamatKantor: $alamatKantor.val(),
      telpHp: $telpHp.val(),
      foto: _foto,
      suratPerintahTugas: _suratPerintahTugas,
      suratPernyataanBebasTugasKedinasan: _suratPernyataanBebasTugasKedinasan,
      suratKeputusanCPNS: _suratKeputusanCPNS,
      suratKeputusanPangkat: _suratKeputusanPangkat,
      suratKeputusanJabatanTerakhir: _suratKeputusanJabatanTerakhir,
      ijazahTerakhir: _ijazahTerakhir,
      suratKesehatan: _suratKesehatan,
      suratKeteranganDokterdariRumahSakitPemerintah: _suratKeteranganDokterdariRumahSakitPemerintah,
      askesBPJSatauKIS: _askesBPJSatauKIS,
    };

    $btnDaftar.html("Wait...");

    api.post(
      url,
      data,
      function (result) {
        switch (result.status) {
          case "success":
            this.alert(
              pesan,
              function () {
                if ($jenisDiklatId.val() != 11) {
                  var url = "PendaftaranDiklat/createPDFDetail";
                  var data = {
                    id: result.data,
                  };

                  api.post(
                    url,
                    data,
                    function (result) {
                      if (result.status == "success") {
                        window.open(result.data);
                        document.location =
                          localStorage.urlSite + "/kegiatan_diklat";
                      } else this.alert(result.message);
                    }.bind(this)
                  );
                } else {
                  document.location = localStorage.urlSite;
                }
              }.bind(this)
            );
            break;

          default:
            var message = "<b>" + result.message + "</b><br><br>";

            if (result.data.diklatId) message += result.data.diklatId + "<br>";
            if (result.data.nama) message += result.data.nama + "<br>";
            if (result.data.alamat) message += result.data.alamat + "<br>";
            if (result.data.tempatLahir)
              message += result.data.tempatLahir + "<br>";
            if (result.data.tanggalLahir)
              message += result.data.tanggalLahir + "<br>";
            if (result.data.agamaId) message += result.data.agamaId + "<br>";
            if (result.data.jenisKelaminId)
              message += result.data.jenisKelaminId + "<br>";
            if (result.data.pangkatGolonganId)
              message += result.data.pangkatGolonganId + "<br>";
            if (result.data.jabatanEselon)
              message += result.data.jabatanEselon + "<br>";
            if (result.data.unitKerja)
              message += result.data.unitKerja + "<br>";
            if (result.data.instansiId)
              message += result.data.instansiId + "<br>";
            if (result.data.instansiLainnya)
              message += result.data.instansiLainnya + "<br>";
            if (result.data.pendidikanTerakhirId)
              message += result.data.pendidikanTerakhirId + "<br>";
            if (result.data.alamatKantor)
              message += result.data.alamatKantor + "<br>";

            if (result.data.telpHp) message += result.data.telpHp + "<br>";
            if (result.data.foto) message += result.data.foto + "<br>";

            this.alert(message);
        }
        if ($jenisDiklatId.val() != 11) $btnDaftar.html("Simpan & Cetak");
        else $btnDaftar.html("Simpan");
      }.bind(this)
    );
  };

  this.batalDaftar = function () {
    window.location = localStorage.urlSite + "/kegiatan_diklat";
  };
};

scoopy.application.controllers.PendaftaranDiklat.prototype = Object.create(
  scoopy.application.core.Controller.prototype
);

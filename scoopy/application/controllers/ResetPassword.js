scoopy.application.controllers.ResetPassword = function () {
  scoopy.application.core.Controller.call(this);
  var $scope = this.getScope('ResetPassword');

  var $kataSandiBaru;
  var $ulangiKataSandi;
  var $btnKirim;

  this.init = function () {

    $kataSandiBaru = $scope.getRef('kataSandiBaru');
    $ulangiKataSandi = $scope.getRef('ulangiKataSandi');

    $btnKirim = $scope.getRef('btnKirim');
  };

  this.reload = function () {
  };

  this.driveEvents = function () {
    $btnKirim.on('click', this.resetPassword.bind(this));
  };

  this.resetPassword = function () {

    var url = 'Anggota/resetPassword';
    var data = {
      token: TOKEN,
      kataSandiBaru: $kataSandiBaru.val(),
      ulangiKataSandi: $ulangiKataSandi.val(),
    };

    $btnKirim.html('Wait...');

    api.post(url, data, function (result) {

      switch (result.status) {
        case 'success':
          $ulangiKataSandi.val('');

          document.location = localStorage.urlSite + 'verifikasi/reset_berhasil';
          
          break;

        default:
          var message = '<b>' + result.message + '</b><br><br>';

          if (result.data.token)
            message += result.data.token + '<br>';

          if (result.data.kataSandiBaru)
            message += result.data.kataSandiBaru + '<br>';

          if (result.data.ulangiKataSandi)
            message += result.data.ulangiKataSandi + '<br>';

          this.alert(message);
      }

      $btnKirim.html('Kirim');
    }.bind(this));

  };

};

scoopy.application.controllers.ResetPassword.prototype =
Object.create(scoopy.application.core.Controller.prototype);

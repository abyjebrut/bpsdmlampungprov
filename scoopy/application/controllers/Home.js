scoopy.application.controllers.Home = function () {
  scoopy.application.core.Controller.call(this);
  var $scope = this.getScope('Content');
  var $scpGuestBook = this.getScope('HomeGuestBook');

  this.init = function () {
    $scpGuestBook.$name = $scpGuestBook.getRef('name');
    $scpGuestBook.$email = $scpGuestBook.getRef('email');
    $scpGuestBook.$subject = $scpGuestBook.getRef('subject');
    $scpGuestBook.$message = $scpGuestBook.getRef('message');
    $scpGuestBook.$btnSend = $scpGuestBook.getRef('btnSend');
  };

  this.reload = function () {
  };

  this.driveEvents = function () {
    $scpGuestBook.$btnSend.on('click', this.sendMessage.bind(this));
  };

  this.sendMessage = function () {

    var url = 'GuestBook/createData';
    var data = {
      name: $scpGuestBook.$name.val(),
      email: $scpGuestBook.$email.val(),
      subject: $scpGuestBook.$subject.val(),
      message: $scpGuestBook.$message.val(),
    };

    $scpGuestBook.$btnSend.html('Wait...');

    api.post(url, data, function (result) {

      switch (result.status) {
        case 'success':
          $scpGuestBook.$name.val('');
          $scpGuestBook.$email.val('');
          $scpGuestBook.$subject.val('');
          $scpGuestBook.$message.val('');

          this.alert('Message was sent');
          break;

        default:
          var message = '<b>' + result.message + '</b><br><br>';

          if (result.data.name)
            message += result.data.name + '<br>';

          if (result.data.email)
            message += result.data.email + '<br>';

          if (result.data.subject)
            message += result.data.subject + '<br>';

          if (result.data.message)
            message += result.data.message + '<br>';

          this.alert(message);
      }

      $scpGuestBook.$btnSend.html('Send');
    }.bind(this));

  };

};

scoopy.application.controllers.Home.prototype =
Object.create(scoopy.application.core.Controller.prototype);

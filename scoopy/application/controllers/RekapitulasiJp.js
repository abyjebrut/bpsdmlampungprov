scoopy.application.controllers.RekapitulasiJp = function () {
  scoopy.application.core.Controller.call(this);
  var $scope = this.getScope('Content');

  var $jenisDiklat;
  var $keyword;
  var $listData;

  var rsDiklat = [];

  this.init = function () {
    $jenisDiklat = $scope.getRef('jenisDiklat');
    $keyword = $scope.getRef('keyword');
    $listData = $scope.getRef('listData');
  };

  this.reload = function () {
    this.readData();
  };

  this.driveEvents = function () {
    $jenisDiklat.on('change', this.renderByJenis.bind(this));
    $keyword.on('keyup', this.renderBySearching.bind(this));
  };

  this.readData = function () {

    if ($jenisDiklat.val() < 0)
      var Jenis = '';
    else
      var Jenis = $jenisDiklat.val();
    var url = 'Diklat/readDataRekapitulasiJp';
    var data = {
      jenisDiklatId: Jenis,
      orderBy: 'namaPengajar',
      reverse: 2
    };

    $listData.html('<tr><td colspan="3">Loading...</td></tr>');

    api.post(url, data, function (result) {
      rsDiklat = result.data;
      // var result = [];
      // rsDiklat.reduce(function (res, value) {
      //   if (!res[value.pengajarDiklatId]) {
      //     res[value.pengajarDiklatId] = { pengajarDiklatId: value.pengajarDiklatId, namaPengajar: value.namaPengajar, fotoPengajar: value.fotoPengajar, jumlahJp: 0 };
      //     result.push(res[value.pengajarDiklatId])
      //   }
      //   res[value.pengajarDiklatId].jumlahJp += parseInt(value.jumlahJp) ;
      //   return res;
      // }, {});

      var html = '';

      $.each(rsDiklat, function (i, row) {
        html += this.renderList(i, row);
      }.bind(this));

      $listData.html(html);
    }.bind(this));
  };

  this.renderByJenis = function () {

    if ($jenisDiklat.val() < 0)
      var Jenis = '';
    else
      var Jenis = $jenisDiklat.val();
    var url = 'Diklat/readDataRekapitulasiJp';
    var data = {
      jenisDiklatId: Jenis,
      orderBy: 'namaPengajar',
      reverse: 2
    };

    $listData.html('<tr><td colspan="3">Loading...</td></tr>');

    api.post(url, data, function (result) {
      rsDiklat = result.data;
      // var result = [];
      // rsDiklat.reduce(function (res, value) {
      //   if (!res[value.pengajarDiklatId]) {
      //     res[value.pengajarDiklatId] = { pengajarDiklatId: value.pengajarDiklatId, namaPengajar: value.namaPengajar, fotoPengajar: value.fotoPengajar, jumlahJp: 0 };
      //     result.push(res[value.pengajarDiklatId])
      //   }
      //   res[value.pengajarDiklatId].jumlahJp += parseInt(value.jumlahJp) ;
      //   return res;
      // }, {});
      var html = '';

      $.each(rsDiklat, function (i, row) {
        html += this.renderList(i, row);
      }.bind(this));

      $listData.html(html);
    }.bind(this));
  };

  this.renderBySearching = function () {

    var filteredRs = rsDiklat.filter(function (data) {
      var regex = new RegExp($keyword.val(), 'gi');
      var found = false;

      if (data.namaPengajar.match(regex))
        found = true;

      return found;
    });

    var html = '';

    $.each(filteredRs, function (i, row) {
      html += this.renderList(i, row);
    }.bind(this));

    $listData.html(html);
  };

  this.renderList = function (i, row) {
    if (!row.namaPengajar)
      var pp = '';
    else
      var pp = row.namaPengajar;
    var num = i + 1;
    var html = '<tr>' +
      '<td data-title="No" class="numbers">' + num + '.</td>' +
      '<td data-title="Widyaiswara">' +
      '<img class="img-tb-rekap" src="' + localStorage.urlSite + 'asset/archive/' + row.fotoPengajar + '" alt="">' +
      '<span>' + pp + '</span>' +
      '</td>' +
      '<td data-title="Jumlah JP" class="numbers">' + row.totalJp + '</td>' +
      '</tr>';
    return html;
  };

  this.search = function () {
  };

};

scoopy.application.controllers.RekapitulasiJp.prototype =
  Object.create(scoopy.application.core.Controller.prototype);

scoopy.application.controllers.DetailAlumni = function () {
  scoopy.application.core.Controller.call(this);
  var $scope = this.getScope('Content');
  var $btnPdf;

  this.init = function () {
    $btnPdf = $scope.getRef('btnPdf');
  };

  this.reload = function () {
  };

  this.driveEvents = function () {
    $btnPdf.on('click', this.createReportPdf.bind(this));
  };

  this.createReportPdf = function () {
    var url = 'Kelulusan/createPDFList';
    var data = {
      namaDiklatId: DIKLAT_ID,
    };

    var lastLabel = $btnPdf.html();

    $btnPdf.html('Wait...');

    api.post(url, data, function (result) {
      $btnPdf.html(lastLabel);
      window.open(result.data);
    });
  };

};

scoopy.application.controllers.DetailAlumni.prototype =
Object.create(scoopy.application.core.Controller.prototype);

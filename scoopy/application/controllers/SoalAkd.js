scoopy.application.controllers.SoalAkd = function () {
  scoopy.application.core.Controller.call(this);
  var $scope = this.getScope('Content');

  var $btnSubmit;
  var $btnPdf;

  this.init = function () {

    $btnSubmit = $scope.getRef('btnSubmit');
    $btnPdf = $scope.getRef('btnPdf');
  };

  this.reload = function () {
  };

  this.driveEvents = function () {
    $btnSubmit.on('click', this.submitData.bind(this));
    $btnPdf.on('click', this.generatePdf.bind(this));
  };

  this.submitData = function () {

    var $jawaban = [];

    var jawaban;
    var jawabanRumusan;

    var rumusanKompetensi = [];

    $btnSubmit.html('WAIT...');

    for(var i= 1; i <= JUMLAH_SOAL; i++) {
      $jawaban[i] = $scope.getRef('jawaban_'+i);

      jawaban = $jawaban[i].filter(':checked').val();
      if (!jawaban)
        jawaban = '';

      jawabanRumusan = $jawaban[i].data('rumusan-kompetensi');

      rumusanKompetensi.push({
        rumusanKompetensi: jawabanRumusan,
        tingkatKesanggupanId: jawaban,
      });
    }

    var url = 'SoalAkd/updateDataJawaban';
    var data = {
      id: SOAL_AKD_ID,
      links: rumusanKompetensi,
    };

    api.post(url, data, function (result) {
      $btnSubmit.html('SUBMIT');

      if (result.status == 'success')
        window.location = CI_BASE_URL + 'akd';
      else if (result.status == 'invalid') {
        this.alert(result.data.tingkatKesanggupanId);
      }
      else
        this.alert(result.message);
    }.bind(this));

  };

  this.generatePdf = function (event) {
    var id = $(event.currentTarget).data('id');

    var url = 'SoalAkd/createPDFDetail';
    var data = {
      id: id,
    };

    $(event.currentTarget).find('i').css('color', 'gray');

    api.post(url, data, function (result) {
      $(event.currentTarget).find('i').css('color', 'darkred');
      window.open(result.data);
    });

  };

};

scoopy.application.controllers.SoalAkd.prototype =
Object.create(scoopy.application.core.Controller.prototype);

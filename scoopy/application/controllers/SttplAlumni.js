scoopy.application.controllers.SttplAlumni = function () {
  scoopy.application.core.Controller.call(this);
  var $scope = this.getScope('Content');

  var $tahun;
  var $jenisDiklat;
  var $namaDiklat;
  var $listData;

  var rsDiklat = [];

  var $btnCari;
  var $btnPdf;

  this.init = function () {

    rsDiklat = RS_DIKLAT;

    $tahun = $scope.getRef('tahun');
    $jenisDiklat = $scope.getRef('jenisDiklat');
    $namaDiklat = $scope.getRef('namaDiklat');
    $listData = $scope.getRef('listData');
    $btnCari = $scope.getRef('btnCari');

    $btnPdf = $scope.getRef('btnPdf');
  };

  this.reload = function () {
  };

  this.driveEvents = function () {
    $btnCari.on('click', this.searchData.bind(this));
    $namaDiklat.on('keyup', this.renderByFilter.bind(this));
    $btnPdf.on('click', this.createReportPdf.bind(this));
  };

  this.createReportPdf = function () {
    alert('helo sandri');
    // var url = 'Kelulusan/readDataAlumni';
    //
    // api.post(url, data, function (result) {
    //
    // });
  };

  this.searchData = function () {
    var url = 'Diklat/readDataAlumni';
    var data = {
      tahun: $tahun.val(),
      jenisDiklatId: $jenisDiklat.val(),
    };

    $listData.html('<tr>' +
    '<td colspan="2">Loading...</td>' +
    '</tr>');

    api.post(url, data, function (result) {

      rsDiklat = result.data;
      var htmlList = '';

      $.each(rsDiklat, function (i, row) {
        var num = i + 1;

        htmlList +=
          '<tr>' +
            '<td data-title="No" class="numbers">' + num + '.</td>' +
            '<td data-title="Program Diklat">' +
              '<a href="' + localStorage.urlSite + 'sttpl_alumni/detail_alumni/' + row.id +'">' +
                row.namaDiklatKode +
              '</a>' +
            '</td>' +
          '</tr>';
      });

      $listData.html(htmlList);

    }.bind(this));

  };

  this.renderByFilter = function () {

    var filteredRs = rsDiklat.filter(function (data) {
      var regex = new RegExp($namaDiklat.val(), 'gi');
      var found = false;

      if (data.namaDiklatKode.match(regex))
        found = true;

      return found;
    });

    var htmlList = '';

    $.each(filteredRs, function (i, row) {
      var num = i + 1;

      htmlList +=
        '<tr>' +
          '<td data-title="No" class="numbers">' + num + '.</td>' +
          '<td data-title="Program Diklat">' +
            '<a href="' + localStorage.urlSite + 'sttpl_alumni/detail_alumni/' + row.id +'">' +
              row.namaDiklatKode +
            '</a>' +
          '</td>' +
        '</tr>';
    });

    $listData.html(htmlList);
  };

};

scoopy.application.controllers.SttplAlumni.prototype =
Object.create(scoopy.application.core.Controller.prototype);

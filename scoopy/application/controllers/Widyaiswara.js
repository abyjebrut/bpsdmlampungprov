scoopy.application.controllers.Widyaiswara = function () {
  scoopy.application.core.Controller.call(this);
  var $scope = this.getScope('Content');
  var $scpPertanyaan = this.getScope('Pertanyaan');

  this.init = function () {
    $scpPertanyaan.$nama = $scpPertanyaan.getRef('nama');
    $scpPertanyaan.$email = $scpPertanyaan.getRef('email');
    // $scpPertanyaan.$subject = $scpPertanyaan.getRef('subject');
    $scpPertanyaan.$pertanyaan = $scpPertanyaan.getRef('pertanyaan');
    $scpPertanyaan.$btnSend = $scpPertanyaan.getRef('btnSend');
  };

  this.reload = function () {
  };

  this.driveEvents = function () {
    $scpPertanyaan.$btnSend.on('click', this.sendMessage.bind(this));
  };

  this.sendMessage = function () {

    var url = 'Pertanyaan/createData';
    var data = {
      nama: $scpPertanyaan.$nama.val(),
      email: $scpPertanyaan.$email.val(),
      // subject: $scpPertanyaan.$subject.val(),
      pertanyaan: $scpPertanyaan.$pertanyaan.val(),
    };

    $scpPertanyaan.$btnSend.html('WAIT...');

    api.post(url, data, function (result) {

      switch (result.status) {
        case 'success':
          $scpPertanyaan.$nama.val('');
          $scpPertanyaan.$email.val('');
          // $scpPertanyaan.$subject.val('');
          $scpPertanyaan.$pertanyaan.val('');

          this.alert('Message was sent');
          break;

        default:
          var pertanyaan = '<b>' + result.pertanyaan + '</b><br><br>';

          if (result.data.nama)
            pertanyaan += result.data.nama + '<br>';

          if (result.data.email)
            pertanyaan += result.data.email + '<br>';

          // if (result.data.subject)
          //   pertanyaan += result.data.subject + '<br>';

          if (result.data.pertanyaan)
            pertanyaan += result.data.pertanyaan + '<br>';

          this.alert(pertanyaan);
      }

      $scpPertanyaan.$btnSend.html('KIRIM <i class="wow fadeIn fa fa-paper-plane"></i>');
    }.bind(this));

  };

};

scoopy.application.controllers.Widyaiswara.prototype =
Object.create(scoopy.application.core.Controller.prototype);

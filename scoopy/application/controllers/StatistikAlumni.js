scoopy.application.controllers.StatistikAlumni = function () {
  scoopy.application.core.Controller.call(this);
  var $scope = this.getScope('Content');
 
  var $jenisDiklat;
  var $namaDiklat;
  var $listData;

  var rsAlumni = [];

  var $btnCari;
  var $btnPdf;

  this.init = function () {
    
  };

  this.reload = function () {
    // this.readData();
    
  };

  

  this.driveEvents = function () {
    // $btnCari.on('click', this.searchData.bind(this));
    // $namaDiklat.on('keyup', this.renderByFilter.bind(this));
    // $btnPdf.on('click', this.createReportPdf.bind(this));
  };

  this.createReportPdf = function () {
    alert('helo sandri');
    // var url = 'Kelulusan/readDataAlumni';
    //
    // api.post(url, data, function (result) {
    //
    // });
  };

  this.searchData = function () {
    var url = 'Diklat/readDataAlumni';
    var data = {
      tahun: $tahun.val(),
      jenisDiklatId: $jenisDiklat.val(),
    };

    $listData.html('<tr>' +
    '<td colspan="2">Loading...</td>' +
    '</tr>');

    api.post(url, data, function (result) {

      rsAlumni = result.data;
      var htmlList = '';

      $.each(rsAlumni, function (i, row) {
        var num = i + 1;

        htmlList +=
          '<tr>' +
            '<td data-title="No" class="numbers">' + num + '.</td>' +
            '<td data-title="Program Diklat">' +
              '<a href="' + localStorage.urlSite + 'jadwal/detail_alumni/' + row.id +'">' +
                row.namaDiklatKode +
              '</a>' +
            '</td>' +
          '</tr>';
      });

      $listData.html(htmlList);

    }.bind(this));

  };

  this.renderByFilter = function () {

    var filteredRs = rsAlumni.filter(function (data) {
      var regex = new RegExp($namaDiklat.val(), 'gi');
      var found = false;

      if (data.namaDiklatKode.match(regex))
        found = true;

      return found;
    });

    var htmlList = '';

    $.each(filteredRs, function (i, row) {
      var num = i + 1;

      htmlList +=
        '<tr>' +
          '<td data-title="No" class="numbers">' + num + '.</td>' +
          '<td data-title="Program Diklat">' +
            '<a href="' + localStorage.urlSite + 'jadwal/detail_alumni/' + row.id +'">' +
              row.namaDiklatKode +
            '</a>' +
          '</td>' +
        '</tr>';
    });

    $listData.html(htmlList);
  };

};

scoopy.application.controllers.StatistikAlumni.prototype =
Object.create(scoopy.application.core.Controller.prototype);

scoopy.application.controllers.GalleryDetail = function () {
  scoopy.application.core.Controller.call(this);
  var $scope = this.getScope('Content');
  var $thumbnails = $scope.getRef('thumbnails');

  this.driveEvents = function () {
    $thumbnails.find('a').on('click', this.preview.bind(this));
  };

  this.preview = function (event) {
    var $img = $(event.currentTarget).find('img');
    var title = $img.prop('alt');
    var imgUrl = $img.prop('src');

    this.popbox(title, imgUrl);
  };

};

scoopy.application.controllers.GalleryDetail.prototype =
Object.create(scoopy.application.core.Controller.prototype);

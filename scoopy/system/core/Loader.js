scoopy.system.core.Loader = function () {

  this.putPopboxHtml = function () {
    var popboxHtml =
      '<div data-scope="Popbox" class="msgbox modal fade" role="dialog">' +
        '<div class="modal-dialog">' +
          '<div class="modal-content">' +
            '<div class="modal-header">' +
              '<button type="button" class="close" data-dismiss="modal">&times;</button>' +
              '<h4 data-ref="title" class="modal-title"></h4>' +
            '</div>' +
            '<div data-ref="message" class="modal-body">' +
            '</div>' +
          '</div>' +
        '</div>' +
      '</div>';

    $('body').append(popboxHtml);
  };

  this.putAlertHtml = function () {
    var alertHtml =
      '<div data-scope="Alert" class="msgbox modal fade" role="dialog">' +
        '<div class="modal-dialog">' +
          '<div class="modal-content">' +
            '<div class="modal-header">' +
              '<button type="button" class="close" data-dismiss="modal">&times;</button>' +
              '<h4 class="modal-title">Alert</h4>' +
            '</div>' +
            '<div data-ref="message" class="modal-body">' +
            '</div>' +
            '<div class="modal-footer">' +
              '<button data-ref="btnOK" type="button" class="btn btn-primary" data-dismiss="modal">OK</button>' +
            '</div>' +
          '</div>' +
        '</div>' +
      '</div>';

    $('body').append(alertHtml);
  };

  this.putConfirmHtml = function () {
    var confirmHtml =
      '<div data-scope="Confirm" class="msgbox modal fade" role="dialog">' +
        '<div class="modal-dialog">' +
          '<div class="modal-content">' +
            '<div class="modal-header">' +
              '<button type="button" class="close" data-dismiss="modal">&times;</button>' +
              '<h4 class="modal-title">Confirm</h4>' +
            '</div>' +
            '<div data-ref="message" class="modal-body">' +
            '</div>' +
            '<div class="modal-footer">' +
              '<button data-ref="btnOK" type="button" class="btn btn-primary" data-dismiss="modal">OK</button>' +
              '<button data-ref="btnCancel" type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>' +
            '</div>' +
          '</div>' +
        '</div>' +
      '</div>';

    $('body').append(confirmHtml);
  };

  this.load = function () {
    this.putPopboxHtml();
    this.putAlertHtml();
    this.putConfirmHtml();

    $.getScript(baseUrl + '/application/config/Config.js', function () {
      config = new scoopy.application.config.Config();

      config.load(function () {

        $.getScript(baseUrl + '/system/core/Api.js', function () {
          api = new scoopy.system.core.Api();

          $.getScript(baseUrl + '/system/core/Controller.js', function () {
            $.getScript(baseUrl + '/application/core/Controller.js', function () {

              // Load Widgets

              if(config.widgets.HeadBar)
              $.getScript(baseUrl + '/application/widgets/HeadBar.js', function () {
                var widget = new scoopy.application.widgets.HeadBar();

                if(widget.init)
                  widget.init();

                if(widget.reload) {
                  widget.reload();
                  $(window).on('hashchange', function () {
                    widget.reload();
                  });
                }

                if(widget.driveEvents)
                  widget.driveEvents();
              });

              if(config.widgets.SideBar)
              $.getScript(baseUrl + '/application/widgets/SideBar.js', function () {
                var widget = new scoopy.application.widgets.SideBar();

                if(widget.init)
                  widget.init();

                if(widget.reload) {
                  widget.reload();
                  $(window).on('hashchange', function () {
                    widget.reload();
                  });
                }

                if(widget.driveEvents)
                  widget.driveEvents();
              });

              if(config.widgets.FootBar)
              $.getScript(baseUrl + '/application/widgets/FootBar.js', function () {
                var widget = new scoopy.application.widgets.FootBar();

                if(widget.init)
                  widget.init();

                if(widget.reload) {
                  widget.reload();
                  $(window).on('hashchange', function () {
                    widget.reload();
                  });
                }

                if(widget.driveEvents)
                widget.driveEvents();
              });

              // Load Main Controller

              var mainController = $('script[src="' + baseUrl + '/scoopy.js"]').data('controller');

              if(mainController)
              $.getScript(baseUrl + '/application/controllers/' + mainController + '.js', function () {

                if (mainController) {
                  var controller = new scoopy.application.controllers[mainController]();

                  if(controller.init)
                    controller.init();

                  if(controller.reload) {
                    controller.reload();
                    $(window).on('hashchange', function () {
                      controller.reload();
                    });
                  }

                  if(controller.driveEvents)
                    controller.driveEvents();
                }

              });

              // Load Controllers

              var controllersScript = [];

              $.each(config.controllers, function (i, classController) {
                controllersScript.push($.getScript(baseUrl + '/application/controllers/' + classController + '.js'));
              });

              $.when.apply(this, controllersScript).done(function () {

                $.each(config.controllers, function (i, classController) {

                  var controller = new scoopy.application.controllers[classController]();

                  if(controller.init)
                    controller.init();

                  if(controller.reload) {
                    controller.reload();
                    $(window).on('hashchange', function () {
                      controller.reload();
                    });
                  }

                  if(controller.driveEvents)
                    controller.driveEvents();

                });

              });
            });

          });
        });

      });
    });

  };

};

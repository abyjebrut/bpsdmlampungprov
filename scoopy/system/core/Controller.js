scoopy.system.core.Controller = function () {

  this.getScope = function (scope) {
    return $('[data-scope="' + scope + '"]');
  };

  this.enhanceJQuery = function () {

    $.fn.getRef = function (name) {
      return this.find('[data-ref="' + name + '"]');
    };

  };
  this.enhanceJQuery();

  this.getParam = function (index) {
    var params = location.hash.split('/');
    return params[index];
  };

  this.getParamsCount = function () {
    var params = location.hash.split('/');
    var paramsCount = params.length - 1;

    return paramsCount;
  };

  this.getPage = function () {
    return this.getParam(1);
  };

  this.getRequest = function(name) {
    var paramsCount = this.getParamsCount();
    var result = '';

    for (var i = 2;i <= paramsCount;i++) {
      var param = this.getParam(i);

      if (param == name) {
        if (i < paramsCount) {
          var resultPos = i + 1;
          result = this.getParam(resultPos);
        }
      }

    }

    return result;
  };

  this.setTitle = function (title) {
    document.title = config.app.title + ' | ' + title;
  };

};

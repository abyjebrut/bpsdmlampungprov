var scoopy = {};

scoopy.system = {};
scoopy.system.core = {};

scoopy.application = {};
scoopy.application.config = {};
scoopy.application.controllers = {};
scoopy.application.core = {};
scoopy.application.widgets = {};

var config;
var loader;
var api;

if(!baseUrl)
    var baseUrl = 'scoopy';

$.getScript(baseUrl + '/system/core/Loader.js', function () {
    loader = new scoopy.system.core.Loader();
    loader.load();
});

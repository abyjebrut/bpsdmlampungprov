dashboard.application.core.Controller = function ($el) {
    dashboard.system.core.Controller.call(this, $el);

    this.init = function () {
        this.driveEvents();
    };

    this.driveEvents = function () {

        this.setUpPrivileges();
        $(window).on('hashchange', this.setUpPrivileges.bind(this));
    };

    this.setUpPrivileges = function () {

//        if(localStorage.userType == 'admin') {
//            $panel.setVisible('MyAccount', false);
//            $panel.setVisible('UserManagement', true);
//        }
//        else {
//            $panel.setVisible('MyAccount', true);
//            $panel.setVisible('UserManagement', false);
//        }

        $menu.hideAll();
        $panel.hideAll();

        if(localStorage.userType == 'admin') {
            $menu.showAll();
            $panel.showAll();

            $.each(config.panel, function (key, item) {
                if (item.access == ACCESS_USER_ONLY)
                    $panel.setVisible(key, false);
            });
        }
        else {
            // menu

            var splitMenuAccess = localStorage.menuAccess.split(',');

            $.each(splitMenuAccess, function (i, menuAccess) {
                $menu.setVisible(menuAccess, true);
            });

            // panel

            $.each(config.panel, function (key, item) {
                if (item.access == ACCESS_FREE)
                    $panel.setVisible(key, true);

                if (item.access == ACCESS_ADMIN && localStorage.userType == 'admin')
                    $panel.setVisible(key, true);
            });

			if (localStorage.panelAccess && localStorage.panelAccess != 'All') {
				var splitPanelAccess = localStorage.panelAccess.split(',');

				$.each(splitPanelAccess, function (i, panelAccess) {
					if (config.panel[panelAccess].access == ACCESS_USER ||
                        config.panel[panelAccess].access == ACCESS_USER_ONLY)
						$panel.setVisible(panelAccess, true);
				});
			}
        }

    };

    this.renderTable = function ($table, dataSource, keyFields, range, pageActive) {

        if($(window).height() <= 640)
            range = 10;
        else if($(window).height() >= 900)
            range = 15;

        if (!pageActive)
            var pageActive = 1;

        var rowActive = (pageActive - 1) * range;
        var rows = [];

        for (var i = rowActive; i < (rowActive + range); i++) {
            var row = [];
            var field = dataSource[i];

            if (field) {

                keyFields.forEach(function (keyField) {
                    row.push(field[keyField]);
                });

                rows.push({value: field.id, text: row});
            }
        }

        $table.render({'rows': rows});
        return dataSource;
    };

    this.renderTableNoPaging = function ($table, dataSource, keyFields) {
        var rows = [];

        $.each(dataSource, function (i, field) {
            var row = [];

            if (field) {

                keyFields.forEach(function (keyField) {
                    row.push(field[keyField]);
                });

                rows.push({
                    value: field.id,
                    text: row
                });
            }
        });

        $table.render({
            'rows': rows
        });
        return dataSource;
    };

    this.renderImageTable = function ($table, dataSource, keyFields, imageKey, range, pageActive) {

        if($(window).height() <= 640)
            range = 10;
        else if($(window).height() >= 900)
            range = 15;

        if (!pageActive)
            var pageActive = 1;

        var rowActive = (pageActive - 1) * range;
        var rows = [];

        for (var i = rowActive; i < (rowActive + range); i++) {
            var row = [];
            var field = dataSource[i];

            if (field) {

                keyFields.forEach(function (keyField) {
                    if(keyField == imageKey) {
                        var imageFile = localStorage.urlArchive + '/' + field[keyField];
                        var data =
                            '<img src="' + imageFile + '" style="width: 120px">';
                    }
                    else
                        var data = field[keyField];

                    row.push(data);
                });

                rows.push({value: field.id, text: row});
            }
        }

        $table.render({'rows': rows});
        return dataSource;
    };

    this.renderExpandTable = function ($table, dataSource, keyFields, range, pageActive) {

        if($(window).height() <= 640)
            range = 10;
        else if($(window).height() >= 900)
            range = 15;

        if (!pageActive)
            var pageActive = 1;

        $table.clear();
        var rowActive = (pageActive - 1) * range;

        for (var i = rowActive; i < (rowActive + range); i++) {
            var row = new Array();
            var field = dataSource[i];

            if (field) {

                keyFields.forEach(function (keyField) {
                    row.push(field[keyField], field[keyField]['child']);
                });

                $table.push(field.id, row);
            }
        }

        $table.render();
        return dataSource;
    };

    this.renderPaging = function ($paging, dataSource, range) {

        if($(window).height() <= 640)
            range = 10;
        else if($(window).height() >= 900)
            range = 15;

        var pagingCount = dataSource.length / range;
        pagingCount = Math.ceil(pagingCount);

        $paging.render(pagingCount);
    };

    this.init();
};

dashboard.application.core.Controller.prototype =
Object.create(dashboard.system.core.Controller.prototype);

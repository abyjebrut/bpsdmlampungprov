dashboard.application.controllers.DiklatPktuf = function () {
    var $el = $page.getScope('DiklatPktuf');
    dashboard.application.core.Controller.call(this, $el);

    var rs = [];
    var rows = [];
    var rsBahan = [];
    var renderedRs = [];

    var rsRange = config.paging.range;

    var tableHeads = ['Jenis Diklat', 'Kode', 'Nama Diklat', 'Kuota', 'Status'];
    var tableFields = ['jenisDiklat', 'kode', 'namaDiklat', 'kuota', 'statusDiklat'];

    var rsBahanMapel = [];
    var viewHeadsMapel = ['Hari Ke', 'Waktu', 'Mata Pelajaran', 'JML JP'];
    var viewFieldsMapel = ['noUrut', 'waktu', 'mataPelajaran', 'jumlahJp_'];

    var viewHeadsBahan = ['Hari Ke', 'Tanggal', 'Waktu', 'Mata Pelajaran', 'Nama Pengajar', 'Ruang', 'Jumlah Jam'];
    var viewFieldsBahan = ['noUrut', 'allTanggal', 'allWaktu', 'mataPelajaran', 'allPengajar', 'ruang', 'allJam'];

    var rsPengajar = [];
    var cartHeads = ['Nama Pengajar'];
    var cartFields = ['namaPengajar'];

    var rsLokasi = [];
    var cartHeadsLokasi = ['Lokasi Pelaksanaan', 'Angkatan'];
    var cartFieldsLokasi = ['lokasi', 'angkatan'];

    var rsPengajarD = [];
    var rsIsiMapel = [];

    var saveMode;
    var saveModeDialog;
    var detailMode;
    var tipeWaktuId;
    var tipeTanggalId;
    var jenisDiklatIdPengajar;
    var angkatanIdPengajar;
    var tglSelesai;

    var jenisDiklatId = 11;

    var idMapelSelect = ''
    var idDiklat = ''
    var $tab;
    var $list;
    var $listBahan;
    var $preview;
    var $form;

    var monthRecords = [
        'Jan',
        'Feb',
        'Mar',
        'Apr',
        'Mei',
        'Jun',
        'Jul',
        'Ags',
        'Sept',
        'Okt',
        'Nov',
        'Dec',
    ]

    var hariRecords = [
        'Minggu',
        'Senin',
        'Selasa',
        'Rabu',
        'Kamis',
        'Jum`at',
        'Sabtu',
    ]

    this.init = function () {

        // tab

        $tab = $el.find('#tab');
        $tab.$list = $tab.getRef('list');
        $tab.$preview = $tab.getRef('preview');
        $tab.$form = $tab.getRef('form');
        $tab.$new = $tab.getRef('new');

        // list

        $list = $el.find('#list');
        $list.$publishStatusId = $list.getRef('publishStatusId').uiRadioBox();
        $list.$search = $list.getRef('search').uiTextBox({ icon: 'search', placeholder: 'Search...' });
        $list.$table = $list.getRef('table');
        $list.$paging = $list.getRef('paging').uiPaging();

        $list.$button = $list.find('#button');
        $list.$button.$delete = $list.$button.getRef('delete').uiButton();
        $list.$button.$detail = $list.$button.getRef('detail').uiButton();
        $list.$button.$edit = $list.$button.getRef('edit').uiButton();
        $list.$button.$jadwal = $list.$button.getRef('jadwal').uiButton();

        $list.$button.$refresh = $list.$button.getRef('refresh').uiButton();

        // preview

        $preview = $el.find('#preview');

        $preview.$publishStatus = $preview.getRef('publishStatus').uiTextView();
        $preview.$jenisDiklat = $preview.getRef('jenisDiklat').uiTextView();
        $preview.$kode = $preview.getRef('kode').uiTextView();
        $preview.$elDetail = $preview.getRef('elDetail');
        $preview.$elDetail.hide();
        $preview.$listJadwalDetail = $preview.getRef('listJadwalDetail').uiTableViewClick(viewHeadsBahan);

        $preview.$elDetailInput = $preview.getRef('elDetailInput');
        $preview.$elDetailInput.hide();

        $preview.$listJadwalDetailInput = $preview.getRef('listJadwalDetailInput');

        $preview.$namaDiklat = $preview.getRef('namaDiklat').uiTextAreaView();
        $preview.$tahunDiklat = $preview.getRef('tahunDiklat').uiTextView();
        $preview.$userCreated = $preview.getRef('userCreated').uiTextView();
        $preview.$tglCreated = $preview.getRef('tglCreated').uiTextView();
        $preview.$userUpdated = $preview.getRef('userUpdated').uiTextView();
        $preview.$tglUpdated = $preview.getRef('tglUpdated').uiTextView();
        $preview.$lokasi = $preview.getRef('lokasi').uiTextView();
        $preview.$angkatan = $preview.getRef('angkatan').uiTextView();
        $preview.$pelaksanaanMulai = $preview.getRef('pelaksanaanMulai').uiTextView();
        $preview.$pelaksanaanSelesai = $preview.getRef('pelaksanaanSelesai').uiTextView();
        $preview.$kuota = $preview.getRef('kuota').uiTextView();
        $preview.$statusDiklat = $preview.getRef('statusDiklat').uiTextView();


        $preview.$button = $preview.find('#button');
        $preview.$button.$edit = $preview.$button.getRef('edit').uiButton();
        $preview.$button.$cancel = $preview.$button.getRef('cancel').uiButton();
        $preview.$button.$excel = $preview.$button.getRef('excel').uiButton();

        // dialog User

        $preview.$dialogUser = $preview.getRef('dialogUser');

        $preview.$dialogUser.$userCreated = $preview.$dialogUser.getRef('userCreated').uiTextView();
        $preview.$dialogUser.$tglCreated = $preview.$dialogUser.getRef('tglCreated').uiTextView();
        $preview.$dialogUser.$userUpdated = $preview.$dialogUser.getRef('userUpdated').uiTextView();
        $preview.$dialogUser.$tglUpdated = $preview.$dialogUser.getRef('tglUpdated').uiTextView();
        $preview.$dialogUser.$keteranganUser = $preview.$dialogUser.getRef('keteranganUser').uiTextView();
        $preview.$dialogUser.$noUrutUser = $preview.$dialogUser.getRef('noUrutUser').uiTextView();
        $preview.$dialogUser.$mataPelajaranUser = $preview.$dialogUser.getRef('mataPelajaranUser').uiTextView();
        $preview.$dialogUser.$button = $preview.$dialogUser.find('.actions');
        $preview.$dialogUser.$button.$cancel = $preview.$dialogUser.$button.getRef('cancel').uiButton();

        // end dialog User

        // dialog

        $preview.$editDialog = $preview.getRef('editDialog');

        $preview.$editDialog.$pengajarDiklatId = $preview.$editDialog.getRef('pengajarDiklatId').uiComboBox();
        $preview.$editDialog.$mataPelajaran = $preview.$editDialog.getRef('mataPelajaran').uiTextView();
        $preview.$editDialog.$ruang = $preview.$editDialog.getRef('ruang').uiTextBox();
        $preview.$editDialog.$keterangan = $preview.$editDialog.getRef('keterangan').uiTextBox();
        $preview.$editDialog.$tanggal = $preview.$editDialog.getRef('tanggal').uiDatePicker();
        $preview.$editDialog.$elWaktu = $preview.$editDialog.getRef('elWaktu');
        $preview.$editDialog.$elWaktu.hide();
        $preview.$editDialog.$elTanggal = $preview.$editDialog.getRef('elTanggal');
        $preview.$editDialog.$elTanggal.hide();
        $preview.$editDialog.$noUrut = $preview.$editDialog.getRef('noUrut').uiTextView();
        $preview.$editDialog.$jamMulai = $preview.$editDialog.getRef('jamMulai').uiTextView();
        $preview.$editDialog.$jamSelesai = $preview.$editDialog.getRef('jamSelesai').uiTextView();
        $preview.$editDialog.$jumlahHari = $preview.$editDialog.getRef('jumlahHari').uiTextView();
        $preview.$editDialog.$tanggalSelesai_ = $preview.$editDialog.getRef('tanggalSelesai_').uiTextView();
        $preview.$editDialog.$listPengajar = $preview.$editDialog.getRef('listPengajar').uiTable(cartHeads);

        $preview.$editDialog.$add = $preview.$editDialog.getRef('add');
        $preview.$editDialog.$delete = $preview.$editDialog.getRef('delete');
        // $preview.$editDialog.$delete.hide();
        $preview.$editDialog.$button = $preview.$editDialog.find('.actions');

        $preview.$editDialog.$button.$save = $preview.$editDialog.$button.getRef('save').uiButton();
        $preview.$editDialog.$button.$cancel = $preview.$editDialog.$button.getRef('cancel').uiButton();

        // end dialog

        // form

        $form = $el.find('#form').uiForm();

        $form.$publishStatusId = $form.getRef('publishStatusId').uiRadioBox();
        $form.$kode = $form.getRef('kode').uiTextView();
        $form.$elKode = $form.getRef('elKode');
        $form.$elKode.hide();
        $form.$lNew = $form.getRef('lNew');
        $form.$lNew.hide();
        $form.$lEdit = $form.getRef('lEdit');
        $form.$lEdit.hide();
        $form.$lManual = $form.getRef('lManual');
        $form.$lManual.hide();
        $form.$lAuto = $form.getRef('lAuto');
        $form.$lAuto.hide();
        $form.$jenisDiklat = $form.getRef('jenisDiklat').uiTextView();
        $form.$pelaksanaanSelesaiEdit = $form.getRef('pelaksanaanSelesaiEdit').uiTextView();
        $form.$namaDiklat = $form.getRef('namaDiklat').uiTextArea();
        $form.$tahunDiklat = $form.getRef('tahunDiklat').uiComboBox();
        $form.$lokasiId = $form.getRef('lokasiId').uiComboBox();
        $form.$angkatanId = $form.getRef('angkatanId').uiComboBox();
        $form.$pelaksanaanMulai = $form.getRef('pelaksanaanMulai').uiDatePicker();
        $form.$pelaksanaanSelesai = $form.getRef('pelaksanaanSelesai').uiDatePicker();
        $form.$kuota = $form.getRef('kuota').uiNumericBox({ maxlength: 3 });
        $form.$statusDiklatId = $form.getRef('statusDiklatId').uiRadioBox();
        $form.$sabtu = $form.getRef('sabtu').uiCheckBox();
        $form.$sabtu.render([{ value: 1, text: 'Include Sabtu' }]);

        $form.$elWaktu = $form.getRef('elWaktu');
        $form.$elWaktu.hide();

        $form.$elTanggal = $form.getRef('elTanggal');
        $form.$elTanggal.hide();

        $form.$mataPelajaranId = $form.getRef('mataPelajaranId').uiComboBox();
        $form.$noUrut = $form.getRef('noUrut').uiNumericBox({ maxlength: 2 });
        $form.$labelMapelId = $form.getRef('labelMapelId').uiComboBox();
        $form.$jamMulai = $form.getRef('jamMulai').uiNumericBox({ maxlength: 2 });
        $form.$jamMulaiMenit = $form.getRef('jamMulaiMenit').uiNumericBox({ maxlength: 2 });
        $form.$jamSelesai = $form.getRef('jamSelesai').uiNumericBox({ maxlength: 2 });
        $form.$jamSelesaiMenit = $form.getRef('jamSelesaiMenit').uiNumericBox({ maxlength: 2 });
        $form.$jumlahJp = $form.getRef('jumlahJp').uiNumericBox({ maxlength: 2 });
        $form.$tipeMataPelajaranId = $form.getRef('tipeMataPelajaranId').uiRadioBox();
        $form.$tipeWaktuId = $form.getRef('tipeWaktuId').uiRadioBox();
        $form.$tipeTanggalId = $form.getRef('tipeTanggalId').uiRadioBox();
        $form.$jumlahHari = $form.getRef('jumlahHari').uiNumericBox({ maxlength: 2 });

        $form.$listMataPelajaran = $form.getRef('listMataPelajaran').uiTable(viewHeadsMapel);
        $form.$add = $form.getRef('add');
        $form.$addAuto = $form.getRef('addAuto');
        $form.$edit = $form.getRef('edit');
        $form.$edit.hide();
        $form.$delete = $form.getRef('delete');
        $form.$delete.hide();
        $form.$manual = $form.getRef('manual');
        // $form.$manual.hide();
        $form.$auto = $form.getRef('auto');
        // $form.$auto.hide();

        $form.$button = $form.find('#button');
        $form.$button.$save = $form.$button.getRef('save').uiButton();
        $form.$button.$cancel = $form.$button.getRef('cancel').uiButton();

        // dialog

        $form.$editDialog = $form.getRef('editDialog');
        $form.$editDialog.$dialogNoUrut = $form.$editDialog.getRef('dialogNoUrut').uiNumericBox({ maxlength: 2 });
        $form.$editDialog.$dialogLabelMapelId = $form.$editDialog.getRef('dialogLabelMapelId').uiComboBox();
        $form.$editDialog.$dialogJamMulai = $form.$editDialog.getRef('dialogJamMulai').uiNumericBox({ maxlength: 2 });
        $form.$editDialog.$dialogJamMulaiMenit = $form.$editDialog.getRef('dialogJamMulaiMenit').uiNumericBox({ maxlength: 2 });
        $form.$editDialog.$dialogJamSelesai = $form.$editDialog.getRef('dialogJamSelesai').uiNumericBox({ maxlength: 2 });
        $form.$editDialog.$dialogJamSelesaiMenit = $form.$editDialog.getRef('dialogJamSelesaiMenit').uiNumericBox({ maxlength: 2 });
        $form.$editDialog.$dialogJumlahJp = $form.$editDialog.getRef('dialogJumlahJp').uiNumericBox({ maxlength: 2 });

        $form.$editDialog.$button = $form.$editDialog.find('.actions');

        $form.$editDialog.$button.$save = $form.$editDialog.$button.getRef('save').uiButton();
        $form.$editDialog.$button.$cancel = $form.$editDialog.$button.getRef('cancel').uiButton();

        // end dialog

    };

    this.renderRsTable = function ($tableRs, dataSource, keyFields, range, pageActive) {

        if ($(window).height() > 900)
            range = 15;

        if (!pageActive)
            var pageActive = 1;

        var rowActive = (pageActive - 1) * range;
        rows = [];

        for (var i = rowActive; i < dataSource.length; i++) {
            var row = [];
            var field = dataSource[i];

            if (field) {

                keyFields.forEach(function (keyField) {
                    row.push(field[keyField]);
                });

                rows.push({
                    id: field.id,
                    jenisDiklatId: field.jenisDiklatId,
                    jenisDiklat: field.jenisDiklat,
                    kode: field.kode,
                    namaDiklat: field.namaDiklat,
                    kuota: field.kuota,
                    kuotaDiklat: field.kuotaDiklat,
                    statusDiklat: field.statusDiklat,
                });
            }
        }

        var tableHeadsHtml = '';

        tableHeadsHtml += `<th style="display:none;" >Id</th>`;
        for (tableHead of tableHeads) {
            tableHeadsHtml += `<th>${tableHead}</th>`;
        }

        var html = `
        <table id="faiz1" class="hover" style="width: 100%">
        <thead>
            <tr>
                ${tableHeadsHtml}
                <th>Aksi</th>
            </tr>
            
        </thead>
        <tbody>
        `;
        var button1 = '';
        for (row of rows) {
            html += '<tr style="cursor: pointer">';
            html += `<td style="display:none; border-bottom: solid silver 1px">${row.id}</td>`;
            html += `<td style="border-bottom: solid silver 1px">${row.jenisDiklat}</td>`;
            html += `<td style="border-bottom: solid silver 1px">${row.kode}</td>`;
            html += `<td style="border-bottom: solid silver 1px">${row.namaDiklat}</td>`;
            html += `<td style="border-bottom: solid silver 1px">${row.kuota}</td>`;
            html += `<td style="border-bottom: solid silver 1px">${row.statusDiklat}</td>`;
            button1 = `<button class="act-delete" data-id="${row.id}" href="javascript:">
                Hapus
                </button>
                <button class="act-detail" data-id="${row.id}" href="javascript:">
                Lihat
                </button>
                <button class="act-edit" data-id="${row.id}" href="javascript:">
                Ubah
                </button>
                <button class="act-jadwal" data-id="${row.id}" href="javascript:">
                Jadwal
                </button>
                `

            html += `<td style="width: 90px">${button1}
                    </td>`;

            html += '</tr>';
        }
        html += '</tbody></table>'

        $tableRs.html(html);
        $tableRs.find('table').DataTable({
            searching: false,
            "paging": false,
        });

        var table = $('#faiz1').DataTable();

        $('#faiz1 tbody').on('click', 'button', function () {
            var data = table.row($(this).parents('tr')).data();
        });


        $tableRs.find('.act-delete, .act-detail, .act-edit, .act-jadwal, .act-copy').off('click');
        $tableRs.find('.act-delete').on('click', this.deleteData.bind(this));
        $tableRs.find('.act-detail').on('click', this.detailData.bind(this));
        $tableRs.find('.act-jadwal').on('click', this.detailData.bind(this));
        $tableRs.find('.act-detail').on('click', () => {
            detailMode = 'Detail';
            saveModeDialog = 'Detail';
            $tab.$preview.trigger('click');
        });
        $tableRs.find('.act-edit').on('click', this.editData.bind(this));
        $tableRs.find('.act-edit').on('click', () => {
            $tab.$form.trigger('click');
        });
        $tableRs.find('.act-jadwal').on('click', () => {
            detailMode = 'Input';
            saveModeDialog = 'Detail';
            $tab.$preview.trigger('click');
        });
        return dataSource;
    };

    this.driveEvents = function () {
        // tab

        $tab.$preview.on('click', this.detailData.bind(this));

        $tab.$form.on('click', this.editData.bind(this));
        $tab.$new.on('click', this.newData.bind(this));

        // list

        $list.$search.on('keyup', this.searchData.bind(this));
        $list.$publishStatusId.on('click', this.readData.bind(this));


        $list.$paging.on('click', this.moveData.bind(this));
        $list.$button.$delete.on('click', this.deleteData.bind(this));

        $list.$button.$detail.on('click', function () {
            detailMode = 'Detail'
            $tab.$preview.trigger('click');
        });
        $list.$button.$jadwal.on('click', function () {
            detailMode = 'Input'
            $tab.$preview.trigger('click');
        });

        $list.$button.$edit.on('click', function () {
            $tab.$form.trigger('click');
        });

        $list.$button.$refresh.on('click', this.loadData.bind(this));

        // preview

        $preview.$button.$edit.on('click', function () {
            $tab.$form.trigger('click');
        });

        $preview.$button.$cancel.on('click', function () {
            $tab.$list.trigger('click');
        });


        $preview.$listJadwalDetail.on('dblclick', this.showDetailUserCreated.bind(this));

        $preview.$dialogUser.$button.$cancel.on('click', this.hideDetailUserCreated.bind(this));

        $preview.$button.$excel.on('click', this.exportDetailToExcel.bind(this));

        $preview.$editDialog.$add.on('click', this.addToCart.bind(this));
        $preview.$editDialog.$delete.on('click', this.deleteFromCart.bind(this));
        $preview.$editDialog.$button.$save.on('click', this.saveJadwal.bind(this));
        $preview.$editDialog.$button.$cancel.on('click', this.clearEditDataSubMenu.bind(this));

        $preview.$editDialog.$pengajarDiklatId.on('change', this.clearDataShowError.bind(this));
        // form
        $form.$tipeMataPelajaranId.on('click', this.showJp.bind(this));
        $form.$tipeWaktuId.on('click', this.showHideWaktu.bind(this));
        $form.$jamMulai.on('change', this.showJp.bind(this));
        $form.$jamMulaiMenit.on('change', this.showJp.bind(this));
        $form.$jamSelesai.on('change', this.showJp.bind(this));
        $form.$jamSelesaiMenit.on('change', this.showJp.bind(this));

        $form.$add.on('click', this.addBahanMapelToCart.bind(this));
        $form.$addAuto.on('click', this.addBahanMapelToCartAuto.bind(this));
        $form.$edit.on('click', this.editBahanMapelCart.bind(this));
        $form.$delete.on('click', this.deleteBahanMapelCart.bind(this));

        $form.$manual.on('click', this.showManual.bind(this));
        $form.$auto.on('click', this.showAuto.bind(this));

        $form.$lokasiId.on('change', this.showFormAngkatan.bind(this));
        $form.$tahunDiklat.on('change', this.showFormLokasi1.bind(this));
        $form.$button.$save.on('click', this.saveData.bind(this));

        $form.$button.$cancel.on('click', function () {
            $tab.$list.trigger('click');
        });

    };

    this.showHideWaktu = function () {
        this.showJp();
        $form.$elTanggal.hide();
        $form.$elWaktu.hide();

        if ($form.$tipeWaktuId.getValue() == 1)
            $form.$elWaktu.show();
        else
            $form.$elTanggal.show();

    };

    this.driveFieldSequence = function () {

    };

    this.showManual = function () {
        $form.$lManual.show();
        $form.$elTanggal.show();
        $form.$elWaktu.show();
        $form.$lAuto.hide();
    };

    this.showAuto = function () {
        $form.$lManual.hide();
        $form.$elTanggal.hide();
        $form.$elWaktu.hide();
        $form.$lAuto.show();
    };

    this.showJp = function () {

        if ($form.$tipeWaktuId.getValue() == 1 && $form.$tipeMataPelajaranId.getValue() == 1) {
            var menitM = $form.$jamMulaiMenit.getValue()
            if (menitM == '')
                menitM = '00'
            var menitS = $form.$jamSelesaiMenit.getValue()
            if (menitS == '')
                menitS = '00'
            var tgl = new Date();
            var aJmMulai = tgl.getFullYear() +
                "-" +
                (tgl.getMonth() + 1) +
                "-" +
                tgl.getDate() + " " + $form.$jamMulai.getValue() + ":" + menitM
            var aJmSelesai = tgl.getFullYear() +
                "-" +
                (tgl.getMonth() + 1) +
                "-" +
                tgl.getDate() + " " + $form.$jamSelesai.getValue() + ":" + menitS

            var dt1 = new Date(aJmMulai);
            var dt2 = new Date(aJmSelesai);

            var diff = dt2.getTime() - dt1.getTime();
            var hh = Math.floor((diff % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var mm = Math.floor((diff % (1000 * 60 * 60)) / (1000 * 60));
            mm = mm.toString();
            if (mm == 0 || mm == '')
                mm = '00'
            if (mm.length == 1)
                mm = '0' + mm;

            var hasil = hh + '' + mm
            if (parseInt(hasil) >= 30 && parseInt(hasil) <= 100)
                $form.$jumlahJp.setValue(1);
            else if (parseInt(hasil) > 100 && parseInt(hasil) <= 145)
                $form.$jumlahJp.setValue(2);
            else if (parseInt(hasil) >= 146 && parseInt(hasil) <= 245)
                $form.$jumlahJp.setValue(3);
            else if (parseInt(hasil) >= 246 && parseInt(hasil) <= 345)
                $form.$jumlahJp.setValue(4);
            else if (parseInt(hasil) >= 346 && parseInt(hasil) <= 445)
                $form.$jumlahJp.setValue(5);
            else if (parseInt(hasil) >= 446 && parseInt(hasil) <= 545)
                $form.$jumlahJp.setValue(5);
            else
                $form.$jumlahJp.setValue(0);

        } else {
            $form.$jumlahJp.setValue(0);
        }
    };

    this.showFormLokasi = function () {

        var tahunDiklat = $form.$tahunDiklat.getValue();

        if (jenisDiklatId && tahunDiklat)
            $form.$lokasiId.renderApi('Diklat/readLokasiData', { id: idDiklat, jenisDiklatId: jenisDiklatId, tahunDiklat: tahunDiklat }, { text: 'lokasi' });

        var url = 'MataPelajaran/readData';
        var data = {
            orderBy: 'jenisDiklatId, noUrut, jamMulai',
            reverse: 2,
            jenisDiklatId: jenisDiklatId,
            publishStatusId: 2,
        };

        rsBahanMapel = [];
        api.post(url, data, function (result) {

            result.data.forEach(row => {
                var aWaktu = row.tipeWaktuId;
                if (aWaktu == 1) {
                    var waktu = row.jamMulai + ' s/d ' + row.jamSelesai;
                } else {
                    var waktu = row.jumlahHari + ' hari';
                }

                var noUrut = row.noUrut;
                var jmMulai = row._jamMulai;
                var jmMulaiMenit = row.jamMulaiMenit;
                var jmSelesai = row.jamSelesai;
                var jmSelesaiMenit = row.jamSelesaiMenit;

                var tgl = new Date(row.tanggalJamMulai)
                var jumDay = parseInt(noUrut) - 1;
                var day1 = tgl.getDate() + jumDay;
                var bulan1 = (tgl.getMonth() + 1);
                var tahun1 = tgl.getFullYear();
                var tanggalJamMulai = tahun1 + '-' + bulan1 + '-' + day1 + ' ' + jmMulai + ":" + jmMulaiMenit + ":01"
                var tanggalJamSelesai = tahun1 + '-' + bulan1 + '-' + day1 + ' ' + jmSelesai + ":" + jmSelesaiMenit + ":00"

                rsBahanMapel.push({
                    id: row.id,
                    publishStatusId: row.publishStatusId,
                    publishStatus: row.publishStatus,
                    noUrut: row.noUrut,
                    waktu: waktu,
                    dMulai1: row.dMulai1,
                    bMulai1: row.bMulai1,
                    tMulai1: row.tMulai1,
                    hMulai1: row.hMulai1,
                    mMulai1: row.mMulai1,
                    sMulai1: row.sMulai1,
                    dSelesai1: row.dSelesai1,
                    bSelesai1: row.bSelesai1,
                    tSelesai1: row.tSelesai1,
                    hSelesai1: row.hSelesai1,
                    mSelesai1: row.mSelesai1,
                    sSelesai1: row.sSelesai1,
                    labelMapelId: row.labelMapelId,
                    mataPelajaran: row.mataPelajaran,
                    jamMulai: row.jamMulai,
                    nilaiJamMulai: row.nilaiJamMulai,
                    tanggalJamMulai: tanggalJamMulai,
                    tanggalJamSelesai: tanggalJamSelesai,
                    _jamMulai: row._jamMulai,
                    jamMulaiMenit: row.jamMulaiMenit,
                    jamSelesai: row.jamSelesai,
                    nilaiJamSelesai: row.nilaiJamSelesai,
                    _jamSelesai: row._jamSelesai,
                    jamSelesaiMenit: row.jamSelesaiMenit,
                    jumlahHari: row.jumlahHari,
                    jenisDiklatId: row.jenisDiklatId,
                    jenisDiklat: row.jenisDiklat,
                    tipeMataPelajaranId: row.tipeMataPelajaranId,
                    tipeMataPelajaran: row.tipeMataPelajaran,
                    tipeWaktuId: row.tipeWaktuId,
                    tipeWaktu: row.tipeWaktu,
                    tipeTanggalId: row.tipeTanggalId,
                    tipeTanggal: row.tipeTanggal,
                    jumlahJp: row.jumlahJp,
                    jumlahJp_: row.jumlahJp_,
                    isData: 'lama',
                });
            });

            $form.$delete.hide();
            $form.$edit.hide();
            if (rsBahanMapel.length > 0) {
                $form.$delete.show();
                // $form.$edit.show();

            }

            this.renderCartBahanMapel();
        }.bind(this));

    };

    this.renderCartBahanMapel = function () {
        this.renderTableNoPaging($form.$listMataPelajaran, rsBahanMapel, viewFieldsMapel, 1000);
    };

    this.addBahanMapelToCart = function () {

        if (!$form.$noUrut.getValue() || $form.$noUrut.getValue() == 0) {
            $msgbox.alert('Hari ke belum diisi');
            return;
        }

        if (!$form.$labelMapelId.getValue()) {
            $msgbox.alert('Mata Pelajaran belum diisi');
            return;
        }

        if (!$form.$jamMulai.getValue()) {
            $msgbox.alert('Jam Mulai belum diisi');
            return;
        }

        if (!$form.$jamSelesai.getValue()) {
            $msgbox.alert('Jam Mulai belum diisi');
            return;
        }

        if ($form.$tipeWaktuId.getValue() == 2) {
            if (!$form.$jumlahHari.getValue() || $form.$jumlahHari.getValue() == 0) {
                $msgbox.alert('Jumlah Hari belum diisi');
                return;
            }
        }

        if ($form.$tipeMataPelajaranId.getValue() == 1) {
            if (!$form.$jumlahJp.getValue() || $form.$jumlahJp.getValue() == 0) {
                $msgbox.alert('Jumlah JP belum diisi');
                return;
            }
        }

        var jmMulai = $form.$jamMulai.getValue();
        if (jmMulai.length == 1) {
            jmMulai = "0" + jmMulai;
        }

        var jmMulaiMenit = $form.$jamMulaiMenit.getValue();
        if (jmMulaiMenit.length == 1) {
            jmMulaiMenit = "0" + jmMulaiMenit;
        } else if (!$form.$jamMulaiMenit.getValue()) {
            jmMulaiMenit = "00";
        }

        var jmSelesai = $form.$jamSelesai.getValue();
        if (jmSelesai.length == 1) {
            jmSelesai = "0" + jmSelesai;
        }

        var jmSelesaiMenit = $form.$jamSelesaiMenit.getValue();
        if (jmSelesaiMenit.length == 1) {
            jmSelesaiMenit = "0" + jmSelesaiMenit;
        } else if (!$form.$jamSelesaiMenit.getValue()) {
            jmSelesaiMenit = "00";
        }
        var waktu = jmMulai + ":" + jmMulaiMenit + ' s/d ' + jmSelesai + ":" + jmSelesaiMenit;

        var jumlahJp_ = $form.$jumlahJp.getValue() + "JP";
        var jumDay = $form.$noUrut.getValue();
        jumDay = parseInt(jumDay) - 1;

        var cariTgl = '';
        var iTgl = 0;
        rsBahanMapel.forEach(function (data) {
            if (data.noUrut == $form.$noUrut.getValue()) {
                cariTgl = data.tanggalJamMulai;
                iTgl++;
            }
        });

        if (saveMode == EDIT_MODE) {

            if (iTgl == 0) {
                if (rsBahanMapel.length > 0) {
                    rsBahanMapel.forEach(function (data) {
                        if (data.noUrut == 1) {
                            cariTgl = data.tanggalJamMulai;
                        }
                    });
                } else {
                    cariTgl = new Date();
                }

                cariTgl = new Date(cariTgl);
                cariTgl.setDate(cariTgl.getDate() + ($form.$noUrut.getValue() - 1));
            }
            var tgl = new Date(cariTgl)
        } else
            var tgl = new Date()


        var day1 = tgl.getDate() + jumDay;
        var bulan1 = (tgl.getMonth() + 1);
        var tahun1 = tgl.getFullYear();
        var tanggalJamMulai = tahun1 + '-' + bulan1 + '-' + day1 + ' ' + jmMulai + ":" + jmMulaiMenit + ":01"
        var tanggalJamSelesai = tahun1 + '-' + bulan1 + '-' + day1 + ' ' + jmSelesai + ":" + jmSelesaiMenit + ":00"

        var found = false;
        rsBahanMapel.forEach(function (data) {

            var from = new Date(data.dMulai1, parseInt(data.bMulai1) - 1, data.tMulai1, data.hMulai1, data.mMulai1, data.sMulai1);
            var to = new Date(data.dSelesai1, parseInt(data.bSelesai1) - 1, data.tSelesai1, data.hSelesai1, data.mSelesai1, data.sSelesai1);
            var checkMulai = new Date(day1, parseInt(bulan1) - 1, tahun1, jmMulai, jmMulaiMenit, 01);
            var checkSelesai = new Date(day1, parseInt(bulan1) - 1, tahun1, jmSelesai, jmSelesaiMenit, 00);
            if (data.noUrut == $form.$noUrut.getValue() && checkMulai > from && checkMulai < to) {
                found = true;
            }
            if (data.noUrut == $form.$noUrut.getValue() && checkSelesai > from && checkSelesai < to) {
                found = true;
            }
            if (data.noUrut == $form.$noUrut.getValue() && from > checkMulai && to < checkSelesai) {
                found = true;
            }

        });

        if (found) {
            $msgbox.alert('Jam Mulai Atau Jam Selesai sudah ada');
            return;
        }

        // var url = 'MataPelajaran/LihatData';
        // var data = {
        //     publishStatusId: 2,
        //     noUrut: $form.$noUrut.getValue(),
        //     labelMapelId: $form.$labelMapelId.getValue(),
        //     jamMulai: $form.$jamMulai.getValue() + ":" + jmMulaiMenit + ":" + "01",
        //     jamSelesai: $form.$jamSelesai.getValue() + ":" + jmSelesaiMenit + ":" + "00",
        //     nilaiJamMulai: jmMulai + jmMulaiMenit,
        //     nilaiJamSelesai: jmSelesai + jmSelesaiMenit,
        //     jenisDiklatId: 12,
        //     tipeMataPelajaranId: 1,
        //     tipeWaktuId: 1,
        //     tipeTanggalId: 0,
        //     jumlahHari: 0,
        //     jumlahJp: $form.$jumlahJp.getValue(),
        // };
        // var foundCari = false;
        // api.post(url, data, function (result) {

        //     if (result.status == 'success') {
        //         foundCari=true;
        //     }

        // });

        // if (foundCari = false) {
        //     $msgbox.alert('Jam Mulai Atau Jam Selesai sudah ada di Master Mata Pelajaran');
        //     return;
        // }
        var jumHari = $form.$noUrut.getValue();
        if (!jumHari)
            jumHari = 0;

        var tipeTanggalId = $form.$tipeTanggalId.getValue();
        if (!tipeTanggalId)
            tipeTanggalId = 0;

        rsBahanMapel.push({
            id: this.guid(),
            publishStatusId: 2,
            noUrut: $form.$noUrut.getValue(),
            waktu: waktu,
            dMulai1: day1,
            bMulai1: bulan1,
            tMulai1: tahun1,
            hMulai1: jmMulai,
            mMulai1: jmMulaiMenit,
            sMulai1: '01',
            dSelesai1: day1,
            bSelesai1: bulan1,
            tSelesai1: tahun1,
            hSelesai1: jmSelesai,
            mSelesai1: jmSelesaiMenit,
            sSelesai1: '00',
            labelMapelId: $form.$labelMapelId.getValue(),
            mataPelajaran: $form.$labelMapelId.getText(),
            jamMulai: jmMulai + ":" + jmMulaiMenit + ":" + "01",
            jamSelesai: jmSelesai + ":" + jmSelesaiMenit + ":" + "00",
            jenisDiklatId: jenisDiklatId,
            tanggalJamMulai: tanggalJamMulai,
            tanggalJamSelesai: tanggalJamSelesai,
            tipeMataPelajaranId: $form.$tipeMataPelajaranId.getValue(),
            tipeWaktuId: $form.$tipeWaktuId.getValue(),
            tipeTanggalId: tipeTanggalId,
            jumlahHari: jumHari,
            jumlahJp: $form.$jumlahJp.getValue(),
            jumlahJp_: jumlahJp_,
            isData: 'baru',
        });

        rsBahanMapel.sort(function (a, b) {
            return new Date(a.tanggalJamMulai) - new Date(b.tanggalJamMulai);
        });


        this.clearDataMapel();

        $form.$delete.show();
        // $form.$edit.show();
        this.renderCartBahanMapel();
    };

    this.addBahanMapelToCartAuto = function () {
        let labelMapelId, mataPelajaran, jmMulai;
        var jmMulaiMenit; var day1; var bulan1; var tahun1; var tanggalJamMulai; var tanggalJamSelesai;
        var jmSelesai; var jmSelesaiMenit; var waktu; var noUrut; var jumlahJp; var jumlahJp_;

        if (!$form.$mataPelajaranId.getValue()) {
            $msgbox.alert('Mata Pelajaran belum diisi');
            return;
        }

        var found1 = false;
        rsBahanMapel.forEach(function (data) {
            if (data.id == $form.$mataPelajaranId.getValue())
                found1 = true;
        });

        if (found1) {
            $msgbox.alert('Mata Pelajaran sudah terdaftar');
            return;
        }

        var url = 'MataPelajaran/detailData';
        var data = {
            id: $form.$mataPelajaranId.getValue()
        };
        api.post(url, data, function (result) {

            if (result.status == 'success') {
                labelMapelId = result.data.labelMapelId;
                mataPelajaran = result.data.mataPelajaran;

                jmMulai = result.data._jamMulai;
                if (jmMulai.length == 1) {
                    jmMulai = "0" + jmMulai;
                }

                jmMulaiMenit = result.data.jamMulaiMenit;
                if (jmMulaiMenit.length == 1) {
                    jmMulaiMenit = "0" + jmMulaiMenit;
                }

                jmSelesai = result.data._jamSelesai;
                if (jmSelesai.length == 1) {
                    jmSelesai = "0" + jmSelesai;
                }

                jmSelesaiMenit = result.data.jamSelesaiMenit;
                if (jmSelesaiMenit.length == 1) {
                    jmSelesaiMenit = "0" + jmSelesaiMenit;
                }
                waktu = jmMulai + ":" + jmMulaiMenit + ' s/d ' + jmSelesai + ":" + jmSelesaiMenit;
                noUrut = result.data.noUrut;

                jumlahJp = result.data.jumlahJp;
                jumlahJp_ = result.data.jumlahJp_;
                var jumDay = noUrut;
                jumDay = parseInt(jumDay) - 1;
                var cariTgl = '';

                rsBahanMapel.forEach(function (data) {
                    if (data.noUrut == noUrut)
                        cariTgl = data.tanggalJamMulai;
                });

                if (saveMode == EDIT_MODE)
                    var tgl = new Date(cariTgl)
                else
                    var tgl = new Date()

                if (jumDay > 0) {
                    day1 = tgl.getDate() + parseInt(jumDay);
                } else {
                    day1 = tgl.getDate();
                }

                bulan1 = (tgl.getMonth() + 1);
                tahun1 = tgl.getFullYear();
                tanggalJamMulai = tahun1 + '-' + bulan1 + '-' + day1 + ' ' + jmMulai + ":" + jmMulaiMenit + ":01"
                tanggalJamSelesai = tahun1 + '-' + bulan1 + '-' + day1 + ' ' + jmSelesai + ":" + jmSelesaiMenit + ":00"

                var found = false;
                rsBahanMapel.forEach(function (data) {

                    var from = new Date(data.dMulai1, parseInt(data.bMulai1) - 1, data.tMulai1, data.hMulai1, data.mMulai1, data.sMulai1);
                    var to = new Date(data.dSelesai1, parseInt(data.bSelesai1) - 1, data.tSelesai1, data.hSelesai1, data.mSelesai1, data.sSelesai1);
                    var checkMulai = new Date(day1, parseInt(bulan1) - 1, tahun1, jmMulai, jmMulaiMenit, 01);
                    var checkSelesai = new Date(day1, parseInt(bulan1) - 1, tahun1, jmSelesai, jmSelesaiMenit, 00);

                    if (checkMulai > from && checkMulai < to)
                        found = true;
                    if (checkSelesai > from && checkSelesai < to)
                        found = true;
                    if (from > checkMulai && to < checkSelesai)
                        found = true;

                });

                if (found) {
                    $msgbox.alert('Jam Mulai Atau Jam Selesai sudah ada');
                    return;
                }

                rsBahanMapel.push({
                    id: $form.$mataPelajaranId.getValue(),
                    publishStatusId: 2,
                    noUrut: noUrut,
                    waktu: waktu,
                    dMulai1: day1,
                    bMulai1: bulan1,
                    tMulai1: tahun1,
                    hMulai1: jmMulai,
                    mMulai1: jmMulaiMenit,
                    sMulai1: '01',
                    dSelesai1: day1,
                    bSelesai1: bulan1,
                    tSelesai1: tahun1,
                    hSelesai1: jmSelesai,
                    mSelesai1: jmSelesaiMenit,
                    sSelesai1: '00',
                    labelMapelId: labelMapelId,
                    mataPelajaran: mataPelajaran,
                    jamMulai: jmMulai + ":" + jmMulaiMenit + ":" + "01",
                    jamSelesai: jmSelesai + ":" + jmSelesaiMenit + ":" + "00",
                    jenisDiklatId: jenisDiklatId,
                    tanggalJamMulai: tanggalJamMulai,
                    tanggalJamSelesai: tanggalJamSelesai,
                    tipeMataPelajaranId: result.data.tipeMataPelajaranId,
                    tipeWaktuId: result.data.tipeWaktuId,
                    tipeTanggalId: result.data.tipeTanggalId,
                    jumlahHari: 0,
                    jumlahJp: jumlahJp,
                    jumlahJp_: jumlahJp_,
                    isData: 'update',
                });
                rsBahanMapel.sort(function (a, b) {
                    return new Date(a.tanggalJamMulai) - new Date(b.tanggalJamMulai);
                });


                $form.$mataPelajaranId.clear();
                $form.$mataPelajaranId.focus();

                $form.$delete.show();
                // $form.$edit.show();
                this.renderCartBahanMapel();

            }

            $loading.hide();
        }.bind(this));

    };

    this.clearDataMapel = function () {
        $form.$tipeMataPelajaranId.setValue(1);
        $form.$tipeWaktuId.setValue(1);
        $form.$tipeTanggalId.setValue(1);
        $form.$jumlahHari.clear();
        $form.$noUrut.clear();
        $form.$noUrut.focus();
        $form.$labelMapelId.clear();
        var date = new Date();
        var jam = `${date.getHours()}`;
        var menit = `${date.getMinutes()}`;

        $form.$jamMulai.setValue(jam);
        $form.$jamMulaiMenit.setValue(menit);
        $form.$jamSelesai.setValue(jam);
        $form.$jamSelesaiMenit.setValue(menit);
        $form.$jumlahJp.setValue(0);
        this.showHideWaktu();
    };

    this.clearEditDataMapel = function () {
        $form.$editDialog.$dialogNoUrut.clear();
        $form.$editDialog.$dialogLabelMapelId.clear();
        $form.$editDialog.$dialogJamMulai.clear();
        $form.$editDialog.$dialogJamMulaiMenit.clear();
        $form.$editDialog.$dialogJamSelesai.clear();
        $form.$editDialog.$dialogJamSelesaiMenit.clear();
        $form.$editDialog.$dialogJumlahJp.clear();
        $form.$editDialog.modal('hide');

    };

    this.editBahanMapelCart = function () {

        var id = $form.$listMataPelajaran.getValue();

        if (!id)
            return;


        $form.$editDialog.$dialogNoUrut.clear();
        $form.$editDialog.$dialogLabelMapelId.clear();
        $form.$editDialog.$dialogJamMulai.clear();
        $form.$editDialog.$dialogJamMulaiMenit.clear();
        $form.$editDialog.$dialogJamSelesai.clear();
        $form.$editDialog.$dialogJamSelesaiMenit.clear();
        $form.$editDialog.$dialogJumlahJp.clear();

        var rowMapel = [];

        $.each(rsBahanMapel, function (i, row) {
            if (row.id == id)
                rowMapel = row;
        });

        $form.$editDialog.$dialogNoUrut.setValue(rowMapel.noUrut);
        $form.$editDialog.$dialogJamMulai.setValue(rowMapel.jamMulai);
        $form.$editDialog.$dialogJamMulaiMenit.setValue(rowMapel.jamMulaiMenit);
        $form.$editDialog.$dialogJamSelesai.setValue(rowMapel.jamSelesai);
        $form.$editDialog.$dialogJamSelesaiMenit.setValue(rowMapel.jamSelesaiMenit);
        $form.$editDialog.$dialogJumlahJp.setValue(rowMapel.jumlahJp);

        $form.$editDialog.$dialogLabelMapelId.renderApi('MataPelajaran/readFilterLabelMapel', { jenisDiklatId: jenisDiklatId }, {
            value: 'labelMapelId', text: 'mataPelajaran', done: function () {
                $form.$editDialog.$dialogLabelMapelId.setValue(rowMapel.labelMapelId);
            }
        });

        $form.$editDialog.modal('setting', 'closable', false).modal('show');
    };

    this.saveCart = function () {

        if (!$form.$editDialog.$dialogNoUrut.getValue()) {
            $form.$editDialog.$dialogNoUrut.showError('Hari ke belum diisi');
            return;
        }

        if (!$form.$editDialog.$dialogLabelMapelId.getValue()) {
            $form.$editDialog.$dialogLabelMapelId.showError('Mata Pelajaran belum diisi');
            return;
        }

        if (!$form.$editDialog.$dialogJamMulai.getValue()) {
            $form.$editDialog.$dialogJamMulai.showError('Jam Mulai belum diisi');
            return;
        }

        if (!$form.$editDialog.$dialogJamSelesai.getValue()) {
            $form.$editDialog.$dialogJamSelesai.showError('Jam Mulai belum diisi');
            return;
        }

        if (!$form.$editDialog.$dialogJumlahJp.getValue() || $form.$editDialog.$dialogJumlahJp.getValue() == 0) {
            $form.$editDialog.$dialogJamSelesai.showError('Jumlah JP belum diisi');
            return;
        }


        var id = $form.$listMataPelajaran.getValue();

        var jmMulai = $form.$editDialog.$dialogJamMulai.getValue();
        if (jmMulai.length == 1) {
            jmMulai = "0" + jmMulai;
        }

        var jmMulaiMenit = $form.$editDialog.$dialogJamMulaiMenit.getValue();
        if (jmMulaiMenit.length == 1) {
            jmMulaiMenit = "0" + jmMulaiMenit;
        } else if (!$form.$editDialog.$dialogJamMulaiMenit.getValue()) {
            jmMulaiMenit = "00";
        }

        var jmSelesai = $form.$editDialog.$dialogJamSelesai.getValue();
        if (jmSelesai.length == 1) {
            jmSelesai = "0" + jmSelesai;
        }

        var jmSelesaiMenit = $form.$editDialog.$dialogJamSelesaiMenit.getValue();
        if (jmSelesaiMenit.length == 1) {
            jmSelesaiMenit = "0" + jmSelesaiMenit;
        } else if (!$form.$editDialog.$jamSelesaiMenit.getValue()) {
            jmSelesaiMenit = "00";
        }

        $.each(rsBahanMapel, function (i, row) {
            if (row.id == id) {
                rsBahanMapel[i].noUrut = $form.$editDialog.$dialogNoUrut.getValue();
                rsBahanMapel[i].labelMapel = $form.$editDialog.$dialogLabelMapelId.getText();
                rsBahanMapel[i].labelMapelId = $form.$editDialog.$dialogLabelMapelId.getValue();
                rsBahanMapel[i].jamMulai = jmMulai + ":" + jmMulaiMenit + ":" + "01";
                rsBahanMapel[i].jamSelesai = jmSelesai + ":" + jmSelesaiMenit + ":" + "00";
                rsBahanMapel[i].jumlahJp = $form.$editDialog.$dialogJumlahJp.getValue();

            }
        });

        this.renderCartBahanMapel();
        this.clearEditDataMapel();
    };

    this.deleteBahanMapelCart = function () {
        var checkedId = $form.$listMataPelajaran.getCheckedValues();
        if (checkedId.length <= 0)
            return;

        $msgbox.confirm('Delete selected data', function () {

            checkedId.forEach(function (indexId) {
                var i = -1;

                rsBahanMapel.forEach(function (data) {
                    i++;

                    if (data.id == indexId)
                        rsBahanMapel.splice(i, 1);

                });
                if (rsBahanMapel.length <= 0)
                    $form.$delete.hide();
            });

            this.renderCartBahanMapel();
        }.bind(this));

    };

    this.showFormLokasi1 = function () {

        var tahunDiklat = $form.$tahunDiklat.getValue();

        if (jenisDiklatId && tahunDiklat)
            $form.$lokasiId.renderApi('Diklat/readLokasiData', { id: idDiklat, jenisDiklatId: jenisDiklatId, tahunDiklat: tahunDiklat }, { text: 'lokasi' });
    };

    this.showFormAngkatan = function () {

        var tahunDiklat = $form.$tahunDiklat.getValue();
        var lokasiId = $form.$lokasiId.getValue();
        if (lokasiId)
            $form.$angkatanId.renderApi('Diklat/readAngkatanData', { id: idDiklat, jenisDiklatId: jenisDiklatId, tahunDiklat: tahunDiklat, lokasiId: lokasiId }, { text: 'angkatan' });

    };

    this.clearEditDataSubMenu = function () {
        $preview.$editDialog.modal('hide');
    };

    this.showDetailUserCreated = function () {
        var rowSubMenu = [];

        $.each(rsBahan, function (i, row) {
            if (row.id == $preview.$listJadwalDetail.getValue())
                rowSubMenu = row;
        });

        if (rowSubMenu.userIdUpdated == 0)
            var tglUpdate = '';
        else
            var tglUpdate = rowSubMenu._tanggalUpdated;
        $preview.$dialogUser.$userCreated.setValue(rowSubMenu.nameCreated);
        $preview.$dialogUser.$tglCreated.setValue(rowSubMenu._tanggalCreated);
        $preview.$dialogUser.$userUpdated.setValue(rowSubMenu.nameUpdated);
        $preview.$dialogUser.$tglUpdated.setValue(tglUpdate);
        $preview.$dialogUser.$keteranganUser.setValue(rowSubMenu.keterangan);
        $preview.$dialogUser.$noUrutUser.setValue(rowSubMenu.noUrut);
        $preview.$dialogUser.$mataPelajaranUser.setValue(rowSubMenu.mataPelajaran);

        $preview.$dialogUser.modal('setting', 'closable', false).modal('show');
    };

    this.hideDetailUserCreated = function () {
        $preview.$dialogUser.modal('hide');
    };

    this.reload = function () {
        $tab.$list.trigger('click');
    };

    this.loadData = function () {

        // var i =2018
        var rsTahun = [];
        for (let i = 2018; i < 2040; i++) {
            rsTahun.push({
                value: i,
                text: i,
            });
        }

        $list.$publishStatusId.renderApi('PublishStatus/readData', {
            text: 'publishStatus',
            done: function () {
                $list.$publishStatusId.setValue(2);
                this.readData();
            }.bind(this),
        });
        $form.$labelMapelId.renderApi('LabelMapel/readData', { text: 'labelMapel' });

        $form.$tahunDiklat.render(rsTahun);
        $form.$statusDiklatId.renderApi('StatusDiklat/readData', { text: 'statusDiklat' });
        $form.$publishStatusId.renderApi('PublishStatus/readData', { text: 'publishStatus' });
        $form.$angkatanId.renderApi('Angkatan/readData', { text: 'angkatan' });
        $form.$lokasiId.renderApi('Lokasi/readData', { text: 'lokasi' });
        $form.$mataPelajaranId.renderApi('MataPelajaran/readData', { jenisDiklatId: jenisDiklatId, publishStatusId: 2, orderBy: 'noUrut, jamMulai' }, { text: 'comboMataPelajaran' });
        $form.$tipeMataPelajaranId.renderApi('TipeMataPelajaran/readData', { text: 'tipeMataPelajaran' });
        $form.$tipeWaktuId.renderApi('TipeWaktu/readData', { text: 'tipeWaktu' });
        $form.$tipeTanggalId.renderApi('TipeTanggal/readData', { text: 'tipeTanggal' });
    };
    this.readData = function (callback) {

        if (!callback)
            $loading.show();

        var url = 'Diklat/readDataPkti';
        var data = {
            orderBy: '_pelaksanaanMulai',
            reverse: 1,
            jenisDiklatId: jenisDiklatId,
            publishStatusId: $list.$publishStatusId.getValue(),
        };

        api.post(url, data, function (result) {

            rs = result.data;
            var rsCount = rs.length;

            if (rsCount > 0) {
                this.dataShow();
            }
            else {
                this.dataHide();
            }

            renderedRs = this.renderRsTable($list.$table, rs, tableFields, rsRange);
            this.renderPaging($list.$paging, renderedRs, rsRange);

            if ($.isFunction(callback))
                callback();
            else
                $loading.hide();
        }.bind(this));
    };

    this.dataHide = function () {
        $list.$paging.hide();
        // $tab.$form.hide();
        $list.$button.$delete.hide();
        $list.$button.$edit.hide();
        $list.$button.$detail.hide();
        $list.$button.$jadwal.hide();
    };

    this.dataShow = function () {
        $list.$paging.hide();
        // $tab.$form.show();
        $list.$button.$delete.show();
        $list.$button.$edit.show();
        $list.$button.$detail.show();
        $list.$button.$jadwal.show();
    };

    this.searchData = function () {

        var filteredRs = rs.filter(function (data) {
            var regex = new RegExp($list.$search.getValue(), 'gi');
            var found = false;

            tableFields.forEach(function (field) {
                if (data[field])
                    if (data[field].match(regex))
                        found = true;
            });

            return found;
        });

        renderedRs = this.renderRsTable($list.$table, filteredRs, tableFields, rsRange);
        if (renderedRs.length > 0) {
            this.renderPaging($list.$paging, renderedRs, rsRange);
            this.dataShow();
        } else {
            this.dataHide();
        }
    };

    this.moveData = function () {
        renderedRs = this.renderTable($list.$table, renderedRs, tableFields, rsRange, $list.$paging.getValue());
    };

    this.deleteData = function () {
        // var checkedId = $list.$table.getCheckedValues();
        // var checkedId = [id];
        var id = $(event.target).data('id');
        if (!id)
            return;

        // if (!id) {
        //     $msgbox.alert('NO_DATA_CHECKED');
        //     return;
        // };

        var url = 'Diklat/deleteData';
        var data = {
            id: id,
        };

        var rowDik = [];
        $.each(rows, function (i, row) {
            if (row.id == id)
                rowDik = row;
        });

        if (rowDik.kuotaDiklat > 0) {
            $msgbox.alert('Data ini tidak bisa di hapus karena sudah ada yang mendaftar ');
            return;
        }
        $msgbox.confirm('Hapus Data Nama Diklat = ' + ' ' + rowDik.namaDiklat + ' ??', function () {
            api.post(url, data, function (result) {

                switch (result.status) {
                    case 'success':
                        $splash.show(result.message);
                        this.readData();
                        break;
                    case 'failed':
                        $msgbox.alert(result.message);
                }

            }.bind(this));
        }.bind(this));

    };

    this.detailData = function () {
        var id
        if (saveModeDialog != 'Modal') {
            id = $(event.target).data('id');
            idDiklat = id
        }
        if (!id)
            id = idDiklat;
        else if (!id)
            return;
        $loading.show();

        if (detailMode == 'Detail') {
            var url = 'Diklat/detailData';
        } else {
            var url = 'Diklat/detailDataJadwal';
        }
        var data = {
            id: id
        };

        api.post(url, data, function (result) {

            if (result.status == 'success') {
                $preview.$publishStatus.setValue(result.data.publishStatus);
                $preview.$jenisDiklat.setValue(result.data.jenisDiklat);
                $preview.$kode.setValue(result.data.kode);
                $preview.$namaDiklat.setValue(result.data.namaDiklat);
                $preview.$tahunDiklat.setValue(result.data.tahunDiklat);
                $preview.$userCreated.setValue(result.data.nameCreated);
                if (result.data.userIdCreated == 0)
                    var tglC = ''
                else
                    var tglC = result.data._tanggalCreated

                $preview.$tglCreated.setValue(tglC);
                $preview.$userUpdated.setValue(result.data.nameUpdated);
                if (result.data.userIdUpdated == 0)
                    var tglU = ''
                else
                    var tglU = result.data._tanggalUpdated

                $preview.$tglUpdated.setValue(tglU);
                $preview.$lokasi.setValue(result.data.lokasi);
                $preview.$angkatan.setValue(result.data.angkatan);
                $preview.$pelaksanaanMulai.setValue(result.data.pelaksanaanMulai);
                $preview.$pelaksanaanSelesai.setValue(result.data.pelaksanaanSelesai);
                $preview.$kuota.setValue(result.data.kuota);
                $preview.$statusDiklat.setValue(result.data.statusDiklat);

                var filterReloadJadwal = result.data.filterReloadJadwal;

                $preview.$elDetail.hide();
                $preview.$elDetailInput.hide();
                $preview.$button.$edit.hide();;
                if (detailMode == 'Detail') {
                    $preview.$elDetail.show();
                    $preview.$button.$edit.show();
                } else {
                    $preview.$elDetailInput.show();

                }
                rsBahan = [];
                rsPengajarD = result.data.listPengajar
                idDiklat = result.data.id
                result.data.listJadwalDiklat.forEach(data1 => {

                    var date1 = new Date(data1.tanggal);
                    var bulan = monthRecords[date1.getMonth()]
                    var hari = hariRecords[date1.getDay()]
                    var date1Selesai = new Date(data1.tanggalSelesai);
                    var bulanSelesai = monthRecords[date1Selesai.getMonth()]
                    var hariSelesai = hariRecords[date1Selesai.getDay()]
                    var allTanggal, allWaktu, allPengajar, allJam
                    var statusSimpan = data1.statusSimpan
                    var pengajarDiklatId = data1.pengajarDiklatId;
                    var htmlView = '';
                    var noi = 0
                    rsPengajar = [];
                    result.data.listJadwalDiklatPengajar.forEach(row1 => {
                        if (data1.id == row1.bahanJadwalDiklatId) {
                            noi++
                            htmlView += noi + '. ' + row1.namaPengajar + '<br>';
                            rsPengajar.push({
                                id: row1.id,
                                bahanJadwalDiklatId: row1.id,
                                pengajarDiklatId: row1.pengajarDiklatId,
                                namaPengajar: row1.namaPengajar,
                            });
                        }

                    });

                    this.renderCart();

                    allPengajar = htmlView
                    if (data1.jumlahJp == 0)
                        allJam = '-'
                    else
                        allJam = data1.jumlahJp

                    if (data1.tipeWaktuId == 1) {

                        allTanggal = hari + '<br>' + date1.getDate() + ' ' + bulan + ' ' + date1.getFullYear()

                        allWaktu = data1.jamMulai + ' - ' + data1.jamSelesai


                    } else {

                        allTanggal = hari + ', ' + date1.getDate() + ' ' + bulan + ' ' + date1.getFullYear() + '<br>' + ' s/d ' + '<br>' + hariSelesai + ', ' + date1Selesai.getDate() + ' ' + bulanSelesai + ' ' + date1Selesai.getFullYear()
                        allWaktu = data1.jumlahHari + ' hari ' + data1.tipeTanggal

                    }
                    rsBahan.push({
                        id: data1.id,
                        allWaktu: allWaktu,
                        allTanggal: allTanggal,
                        allPengajar: allPengajar,
                        allJam: allJam,
                        diklatId: data1.diklatId,
                        jenisDiklatId: data1.jenisDiklatId,
                        listJadwalDiklatPengajar: result.data.listJadwalDiklatPengajar,
                        ruang: data1.ruang,
                        pengajarDiklatId: data1.pengajarDiklatId,
                        userIdCreated: data1.userIdCreated,
                        nameCreated: data1.nameCreated,
                        _tanggalCreated: data1._tanggalCreated,
                        userIdUpdated: data1.userIdUpdated,
                        nameUpdated: data1.nameUpdated,
                        _tanggalUpdated: data1._tanggalUpdated,
                        namaPengajar: data1.namaPengajar,
                        keterangan: data1.keterangan,
                        mataPelajaranId: data1.mataPelajaranId,
                        labelMapelId: data1.labelMapelId,
                        mataPelajaran: data1.mataPelajaran,
                        noUrut: data1.noUrut,
                        tipeTanggalId: data1.tipeTanggalId,
                        jumlahHari: data1.jumlahHari,
                        tipeWaktuId: data1.tipeWaktuId,
                        tipeWaktu: data1.tipeWaktu,
                        tipeMataPelajaranId: data1.tipeMataPelajaranId,
                        tipeMataPelajaran: data1.tipeMataPelajaran,
                        statusSimpan: statusSimpan,
                        tanggal: data1.tanggal,
                        _tanggal: data1._tanggal,
                        tanggalSelesai: data1.tanggalSelesai,
                        _tanggalSelesai: data1._tanggalSelesai,
                        jamMulai: data1.jamMulai,
                        nilaiJamMulai: data1.nilaiJamMulai,
                        _jamMulai: data1._jamMulai,
                        jamMulaiMenit: data1.jamMulaiMenit,
                        jamSelesai: data1.jamSelesai,
                        nilaiJamSelesai: data1.nilaiJamSelesai,
                        _jamSelesai: data1._jamSelesai,
                        jamSelesaiMenit: data1.jamSelesaiMenit,
                        jumlahJp: data1.jumlahJp,
                        jumlahJam: data1.jumlahJam,
                    });
                });

                if (detailMode == 'Detail') {
                    this.renderViewCart();
                } else {
                    this.renderEditCart();
                }


            }

            $loading.hide();
        }.bind(this));
    };


    this.serializeData = function () {
        if ($form.$sabtu.isChecked())
            var sabtu = 'Yes';
        else
            var sabtu = 'No';

        var date1 = new Date();
        var tanggalI = date1.getFullYear() + '-' + (date1.getMonth() + 1) + '-' + date1.getDate() + ' ' + date1.getHours() + ':' + date1.getMinutes() + ':' + date1.getSeconds();
        var data = {
            userId: localStorage.userId,
            tanggalI: tanggalI,
            publishStatusId: $form.$publishStatusId.getValue(),
            jenisDiklatId: jenisDiklatId,
            sabtu: sabtu,
            lokasiId: $form.$lokasiId.getValue(),
            tahunDiklat: $form.$tahunDiklat.getValue(),
            angkatanId: $form.$angkatanId.getValue(),
            namaDiklat: $form.$namaDiklat.getValue(),
            pelaksanaanMulai: $form.$pelaksanaanMulai.getValue(),
            pelaksanaanSelesai: $form.$pelaksanaanSelesai.getValue(),
            kuota: $form.$kuota.getValue(),
            statusDiklatId: $form.$statusDiklatId.getValue(),
            listMapel: rsBahanMapel,
        };

        if (saveMode == EDIT_MODE)
            data.id = idDiklat;

        return data;
    };


    this.clearData = function () {
        $form.clear();
        let tgl1 = new Date()
        let tgl = tgl1.getFullYear() + '-' + (tgl1.getMonth() + 1) + '-' + tgl1.getDate()

        $form.$publishStatusId.setValue(2);
        $form.$kode.clear();
        $form.$namaDiklat.clear();
        $form.$tahunDiklat.setValue(tgl1.getFullYear());
        $form.$angkatanId.clear();
        $form.$lokasiId.clear();
        $form.$pelaksanaanMulai.setValue(tgl);
        $form.$pelaksanaanSelesai.setValue(tgl);
        $form.$kuota.setValue(30);
        $form.$statusDiklatId.setValue(1);
        // $form.$sabtu.setValue([1]);
        $form.$sabtu.clear();

        this.clearDataMapel();

        rsBahanMapel = new Array();
        this.renderCartBahanMapel();

    };

    this.editData = function () {
        var id = $(event.target).data('id');

        if (!id)
            return;
        idDiklat = id;
        $loading.show();

        this.clearData();
        $form.$lEdit.show();
        $form.$lNew.hide();
        saveMode = EDIT_MODE;
        $form.$elKode.show();
        var url = 'Diklat/detailData';
        var data = {
            id: id,
        };

        api.post(url, data, function (result) {

            if (result.status == 'success') {

                $form.$publishStatusId.setValue(result.data.publishStatusId);
                $form.$kode.setValue(result.data.kode);
                $form.$tahunDiklat.setValue(result.data.tahunDiklat);
                $form.$lokasiId.setValue(result.data.lokasiId);
                $form.$angkatanId.setValue(result.data.angkatanId);
                $form.$namaDiklat.setValue(result.data.namaDiklat);
                $form.$pelaksanaanMulai.setValue(result.data._pelaksanaanMulai);
                $form.$pelaksanaanSelesai.setValue(result.data._pelaksanaanSelesai);
                $form.$kuota.setValue(result.data._kuota);
                $form.$statusDiklatId.setValue(result.data.statusDiklatId);
                $form.$jenisDiklat.setValue(result.data.jenisDiklat);
                $form.$pelaksanaanSelesaiEdit.setValue(result.data.pelaksanaanSelesai);
                $form.$sabtu.clear();
                if (result.data.sabtu == 'Yes')
                    $form.$sabtu.setValue([1]);

                rsBahanMapel = [];
                result.data.listJadwalDiklat.forEach(row => {
                    var aWaktu = row.tipeWaktuId;
                    if (aWaktu == 1) {
                        var waktu = row.jamMulai + ' s/d ' + row.jamSelesai;
                    } else {
                        var waktu = row.jumlahHari + ' hari';
                    }
                    var noUrut = row.noUrut;
                    var jmMulai = row._jamMulai;
                    var jmMulaiMenit = row.jamMulaiMenit;
                    var jmSelesai = row._jamSelesai;
                    var jmSelesaiMenit = row.jamSelesaiMenit;
                    var tgl = new Date(row.tanggalJamMulai)
                    var jumDay = parseInt(noUrut) - 1;
                    var day1 = tgl.getDate() + jumDay;
                    var bulan1 = (tgl.getMonth() + 1);
                    var tahun1 = tgl.getFullYear();
                    var tanggalJamMulai = tahun1 + '-' + bulan1 + '-' + day1 + ' ' + jmMulai + ":" + jmMulaiMenit + ":01"
                    var tanggalJamSelesai = tahun1 + '-' + bulan1 + '-' + day1 + ' ' + jmSelesai + ":" + jmSelesaiMenit + ":00"

                    rsBahanMapel.push({
                        id: row.mataPelajaranId,
                        publishStatusId: row.publishStatusId,
                        publishStatus: row.publishStatus,
                        noUrut: row.noUrut,
                        waktu: waktu,
                        dMulai1: day1,
                        bMulai1: bulan1,
                        tMulai1: tahun1,
                        hMulai1: jmMulai,
                        mMulai1: jmMulaiMenit,
                        sMulai1: '01',
                        dSelesai1: day1,
                        bSelesai1: bulan1,
                        tSelesai1: tahun1,
                        hSelesai1: jmSelesai,
                        mSelesai1: jmSelesaiMenit,
                        sSelesai1: '00',
                        labelMapelId: row.labelMapelId,
                        mataPelajaran: row.mataPelajaran,
                        jamMulai: row.jamMulai,
                        nilaiJamMulai: row.nilaiJamMulai,
                        tanggalJamMulai: tanggalJamMulai,
                        tanggalJamSelesai: tanggalJamSelesai,
                        _jamMulai: row._jamMulai,
                        jamMulaiMenit: row.jamMulaiMenit,
                        jamSelesai: row.jamSelesai,
                        nilaiJamSelesai: row.nilaiJamSelesai,
                        _jamSelesai: row._jamSelesai,
                        jamSelesaiMenit: row.jamSelesaiMenit,
                        jumlahHari: row.jumlahHari,
                        jenisDiklatId: row.jenisDiklatId,
                        jenisDiklat: row.jenisDiklat,
                        tipeMataPelajaranId: row.tipeMataPelajaranId,
                        tipeMataPelajaran: row.tipeMataPelajaran,
                        tipeWaktuId: row.tipeWaktuId,
                        tipeWaktu: row.tipeWaktu,
                        tipeTanggalId: row.tipeTanggalId,
                        tipeTanggal: row.tipeTanggal,
                        jumlahJp: row.jumlahJp,
                        jumlahJp_: row.jumlahJp_,
                        isData: 'lama',
                    });
                });
                this.renderCartBahanMapel();

                $form.$lManual.show();
                $form.$lAuto.hide();
                if (rsBahanMapel.length > 0)
                    $form.$delete.show();
                $form.$kode.focus();
            }

            $loading.hide();
        }.bind(this));
    };


    this.newData = function () {
        saveMode = NEW_MODE;

        $form.$elKode.hide();
        $form.$lNew.show();
        $form.$lManual.show();
        $form.$lAuto.hide();
        $form.$lEdit.hide();
        this.clearData();
        $form.$tahunDiklat.focus();
        this.showFormLokasi();
    };

    this.saveData = function () {

        if (!$form.$pelaksanaanMulai.getValue()) {
            $msgbox.alert('PELAKSANAAN_MULAI_REQUIRED');
            return;
        }

        $form.$button.$save.loading();

        switch (saveMode) {
            case EDIT_MODE:
                var url = 'Diklat/updateDataPkti';
                break;
            case NEW_MODE:
                var url = 'Diklat/createDataPkti';
        }

        var data = this.serializeData();

        api.post(url, data, function (result) {

            switch (result.status) {
                case 'success':
                    $splash.show(result.message);

                    this.readData(function () {
                        switch (saveMode) {
                            case EDIT_MODE:
                                $tab.$list.trigger('click');
                                break;
                            case NEW_MODE:
                                $tab.$list.trigger('click');
                        }

                        $form.$button.$save.release();
                        window.scrollTo(0, 0);
                    }.bind(this));

                    break;
                case 'invalid':
                    $form.error(result);
                    $form.$button.$save.release();
            }

        }.bind(this));
    };
    this.renderDataTable = function ($table, dataSource, keyFields, range, pageActive) {

        if ($(window).height() > 900)
            range = 15;

        if (!pageActive)
            var pageActive = 1;

        var rowActive = (pageActive - 1) * range;
        rsIsiMapel = [];

        for (var i = rowActive; i < dataSource.length; i++) {
            var row = [];
            var field = dataSource[i];

            if (field) {

                keyFields.forEach(function (keyField) {
                    row.push(field[keyField]);
                });

                rsIsiMapel.push({
                    id: field.id,
                    noUrut: field.noUrut,
                    tipeTanggalId: field.tipeTanggalId,
                    tipeWaktuId: field.tipeWaktuId,
                    labelMapelId: field.labelMapelId,
                    tipeMataPelajaranId: field.tipeMataPelajaranId,
                    allTanggal: field.allTanggal,
                    allWaktu: field.allWaktu,
                    tanggal: field.tanggal,
                    pengajarDiklatId: field.pengajarDiklatId,
                    allPengajar: field.allPengajar,
                    mataPelajaran: field.mataPelajaran,
                    allJam: field.allJam,
                    ruang: field.ruang,
                    keterangan: field.keterangan,
                    statusSimpan: field.statusSimpan,
                });
            }


        }

        var tableHeadsHtml = '';
        tableHeadsHtml += `<th style="display:none;" >Id</th>`;
        for (tableHead of viewHeadsBahan) {
            tableHeadsHtml += `<th>${tableHead}</th>`;
        }

        var html = `
        <table id="zea" class="hover" style="width: 100%">
        <thead>
            <tr>
                ${tableHeadsHtml}
                <th>Aksi</th>
            </tr>
            
        </thead>
        <tbody>
        `;

        for (row of rsIsiMapel) {
            html += '<tr style="cursor: pointer">';
            html += `<td style="display:none; border-bottom: solid silver 1px">${row.id}</td>`;
            html += `<td style="border-bottom: solid silver 1px">${row.noUrut}</td>`;
            html += `<td style="border-bottom: solid silver 1px">${row.allTanggal}</td>`;
            html += `<td style="border-bottom: solid silver 1px">${row.allWaktu}</td>`;
            html += `<td style="border-bottom: solid silver 1px">${row.mataPelajaran}</td>`;
            html += `<td style="border-bottom: solid silver 1px">${row.allPengajar}</td>`;
            html += `<td style="border-bottom: solid silver 1px">${row.ruang}</td>`;
            html += `<td style="border-bottom: solid silver 1px">${row.allJam}</td>`;
            html += `<td style="border-bottom: solid silver 1px; width: 40px">
                    <button data-toggle="modal" data-backdrop="static" data-keyboard="false" class="act-simpan" data-id="${row.id}" href="javascript:">
                     Input
                    </button>
            </td>`;
            html += '</tr>';
        }
        html += '</tbody></table>'


        $table.html(html);
        $table.find('table').DataTable({
            searching: true,
            "paging": false,
        });

        var table = $('#zea').DataTable();

        $('#zea tbody').on('click', 'button', function () {
            var data = table.row($(this).parents('tr')).data();
            idMapelSelect = data[0];
        });

        var order = table.order();

        table
            .order([1, 'asc'], [3, 'asc'])
            .draw();


        $table.find('.act-simpan').off('click');
        $table.find('.act-simpan').on('click', this.addJadwal.bind(this));
        return dataSource;
    };

    this.clearDataShowError = function () {
        $preview.$editDialog.$pengajarDiklatId.hideError();
    };

    this.addJadwal = function () {

        // $preview.$editDialog.$delete.hide();
        // if (rsPengajar.length > 0)
        //     $preview.$editDialog.$delete.show();

        var id = $(event.target).data('id');
        if (!id)
            return;

        $preview.$editDialog.$noUrut.clear();
        $preview.$editDialog.$mataPelajaran.clear();
        $preview.$editDialog.$ruang.clear();
        $preview.$editDialog.$keterangan.clear();
        $preview.$editDialog.$jamMulai.clear();
        $preview.$editDialog.$jamSelesai.clear();
        $preview.$editDialog.$jumlahHari.clear();
        $preview.$editDialog.$tanggalSelesai_.clear();
        $preview.$editDialog.$pengajarDiklatId.clear();

        var rowSubMenu = [];

        $.each(rsBahan, function (i, row) {
            if (row.id == id)
                rowSubMenu = row;
        });

        $preview.$editDialog.$noUrut.setValue(rowSubMenu.noUrut);
        $preview.$editDialog.$ruang.setValue(rowSubMenu.ruang);
        $preview.$editDialog.$keterangan.setValue(rowSubMenu.keterangan);
        $preview.$editDialog.$mataPelajaran.setValue(rowSubMenu.mataPelajaran);
        $preview.$editDialog.$jamMulai.setValue(rowSubMenu.jamMulai);
        $preview.$editDialog.$jamSelesai.setValue(rowSubMenu.jamSelesai);
        $preview.$editDialog.$jumlahHari.setValue(rowSubMenu.jumlahHari);

        rsPengajar = [];
        rowSubMenu.listJadwalDiklatPengajar.forEach(row1 => {
            if (rowSubMenu.id == row1.bahanJadwalDiklatId) {
                rsPengajar.push({
                    id: row1.id,
                    pengajarDiklatId: row1.pengajarDiklatId,
                    namaPengajar: row1.namaPengajar,
                });
            }
        });


        this.renderCart();

        var tipeWaktuId = rowSubMenu.tipeWaktuId
        $preview.$editDialog.$elWaktu.hide();
        $preview.$editDialog.$elTanggal.hide();

        if (tipeWaktuId == 1) {
            $preview.$editDialog.$elWaktu.show();
        } else {
            $preview.$editDialog.$elTanggal.show();
        }

        $preview.$editDialog.$pengajarDiklatId.renderApi('User/readUserMapelData', { jenisDiklatId: jenisDiklatId, labelMapelId: rowSubMenu.labelMapelId }, {
            value: 'userId', text: 'namaPengajar', done: function () {
                // $preview.$editDialog.$pengajarDiklatId.setValue(rowSubMenu.pengajarDiklatId);
            }
        });

        $preview.$editDialog.modal('setting', 'closable', false).modal('show');
    };

    this.addToCart = function () {
        var id = idMapelSelect;
        if (!$preview.$editDialog.$pengajarDiklatId.getValue()) {
            $preview.$editDialog.$pengajarDiklatId.showError('Pengajar Diklat belum diisi');
            return;
        }
        var found = false;
        rsPengajar.forEach(function (data) {
            if (data.pengajarDiklatId == $preview.$editDialog.$pengajarDiklatId.getValue())
                found = true;
        });

        if (found) {
            $preview.$editDialog.$pengajarDiklatId.showError('Pengajar Diklat sudah terdaftar');
            return;
        }
        var url = 'Diklat/cariJadwalPengajarData';
        var data = {
            userId: localStorage.userId,
            id: id,
            pengajarDiklatId: $preview.$editDialog.$pengajarDiklatId.getValue(),
        };

        api.post(url, data, function (result) {

            if (result.status == 'success') {

                rsPengajar.push({
                    id: this.guid(),
                    pengajarDiklatId: $preview.$editDialog.$pengajarDiklatId.getValue(),
                    namaPengajar: $preview.$editDialog.$pengajarDiklatId.getText(),
                });

                // $preview.$editDialog.$delete.show();
                $preview.$editDialog.$pengajarDiklatId.clear();
                $preview.$editDialog.$pengajarDiklatId.focus();
                this.renderCart();

            } else if (result.status == 'failed') {
                $preview.$editDialog.$pengajarDiklatId.showError(result.data[0]);
            }
        }.bind(this));

    };

    this.deleteFromCart = function () {
        var checkedId = $preview.$editDialog.$listPengajar.getCheckedValues();

        if (checkedId.length == 0) {
            $preview.$editDialog.$pengajarDiklatId.showError('No Ceklist Nama Pengajar');
            return;
        };

        checkedId.forEach(function (indexId) {
            var i = -1;

            rsPengajar.forEach(function (data) {
                i++;

                if (data.id == indexId)
                    rsPengajar.splice(i, 1);

            });

            // if (rsPengajar.length <= 0)
            //     $preview.$editDialog.$delete.hide();

        });

        this.renderCart();

    };

    this.renderCart = function () {
        this.renderTableNoPaging($preview.$editDialog.$listPengajar, rsPengajar, cartFields, 1000);
    };

    this.saveJadwal = function () {
        var id = idMapelSelect;

        saveModeDialog = 'Modal';
        $preview.$editDialog.$button.$save.loading();

        var date1 = new Date();
        var tanggalI = date1.getFullYear() + '-' + (date1.getMonth() + 1) + '-' + date1.getDate() + ' ' + date1.getHours() + ':' + date1.getMinutes() + ':' + date1.getSeconds();

        var url = 'Diklat/simpanJadwalData';
        var data = {
            userId: localStorage.userId,
            tanggalI: tanggalI,
            id: id,
            pengajarDiklatId: $preview.$editDialog.$pengajarDiklatId.getValue(),
            ruang: $preview.$editDialog.$ruang.getValue(),
            keterangan: $preview.$editDialog.$keterangan.getValue(),
            listPengajar: rsPengajar,
        };


        api.post(url, data, function (result) {

            if (result.status == 'success') {
                detailMode = 'Input'
                $tab.$preview.trigger('click');
                $preview.$editDialog.modal('hide');
            } else if (result.status == 'failed') {
                $preview.$editDialog.$pengajarDiklatId.showError(result.data[0]);
            }
        });
        $preview.$editDialog.$button.$save.release();
    };


    this.renderViewCart = function () {
        this.renderTableNoPaging($preview.$listJadwalDetail, rsBahan, viewFieldsBahan, 1000);
    };

    this.renderEditCart = function () {
        this.renderDataTable($preview.$listJadwalDetailInput, rsBahan, viewFieldsBahan, 1000);
    };

    this.exportData = function (type, url, id) {

        $loading.show();

        var data = {};
        if (id)
            data.id = id;

        api.post(url, data, function (result) {

            $loading.hide();

            switch (result.status) {
                case 'success':

                    if (type == 'pdf')
                        router.open(result.data);
                    else
                        router.redirect(result.data);

                    break;
                case 'failed':
                    $msgbox.alert(result.message);
            }
        });
    };

    this.exportDetailToExcel = function () {
        var type = 'xls';
        var url = 'Diklat/createExcelDetail';
        var id = idDiklat;

        this.exportData(type, url, id);
    };

};

dashboard.application.controllers.DiklatPktuf.prototype =
    Object.create(dashboard.application.core.Controller.prototype);

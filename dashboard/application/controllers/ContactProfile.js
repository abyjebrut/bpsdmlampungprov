dashboard.application.controllers.ContactProfile = function () {
    var $el = $page.getScope('ContactProfile');
    dashboard.application.core.Controller.call(this, $el);


    var rsAddress = [];
    var cartHeadsAddress = ['CAPTION', 'ADDRESS'];
    var cartFieldsAddress = ['caption', 'address'];

    var rsPhoneNumbers = [];
    var cartHeadsPhoneNumbers = ['CAPTION', 'PHONE_NUMBER'];
    var cartFieldsPhoneNumbers = ['caption', 'phoneNumber'];

    var rsMobileNumbers = [];
    var cartHeadsMobileNumbers = ['CAPTION', 'MOBILE_NUMBER'];
    var cartFieldsMobileNumbers = ['caption', 'mobileNumber'];

    var rsEmails = [];
    var cartHeadsEmails = ['CAPTION', 'EMAIL'];
    var cartFieldsEmails = ['caption', 'email'];

    var $tab;
    var $preview;
    var $form;

    this.init = function () {

        // tab

        $tab = $el.find('#tab');
        $tab.$preview = $tab.getRef('preview');
        $tab.$form = $tab.getRef('form');

        // preview

        $preview = $el.find('#preview');
        $preview.$slContainer = $preview.getRef('slContainer');
        $preview.$slContainer.hide();
        $preview.$elContainer = $preview.getRef('elContainer');
        $preview.$elContainer.hide();
        $preview.$labelSl = $preview.getRef('labelSl');
        $preview.$labelEl = $preview.getRef('labelEl');

        $preview.$companyName = $preview.getRef('companyName').uiTextView();
        $preview.$companyNameSl = $preview.getRef('companyNameSl').uiTextView();
        $preview.$companyNameEl = $preview.getRef('companyNameEl').uiTextView();
        $preview.$mapLocation = $preview.getRef('mapLocation').uiMapView();
        $preview.$latitude = $preview.getRef('latitude').uiTextView();
        $preview.$longitude = $preview.getRef('longitude').uiTextView();
        $preview.$address = $preview.getRef('address').uiTableView(cartHeadsAddress);
        $preview.$phoneNumbers = $preview.getRef('phoneNumbers').uiTableView(cartHeadsPhoneNumbers);
        $preview.$mobileNumbers = $preview.getRef('mobileNumbers').uiTableView(cartHeadsMobileNumbers);
        $preview.$emails = $preview.getRef('emails').uiTableView(cartHeadsEmails);

        $preview.$button = $preview.find('#button');
        $preview.$button.$edit = $preview.$button.getRef('edit').uiButton();
        $preview.$button.$cancel = $preview.$button.getRef('cancel').uiButton();

        // form

        $form = $el.find('#form').uiForm();
        $form.$slContainer = $form.getRef('slContainer');
        $form.$slContainer.hide();
        $form.$elContainer = $form.getRef('elContainer');
        $form.$elContainer.hide();
        $form.$labelSl = $form.getRef('labelSl');
        $form.$labelEl = $form.getRef('labelEl');

        $form.$languageCode = $form.getRef('languageCode').uiLangBox();
        $form.$companyName = $form.getRef('companyName').uiTextBox();
        $form.$companyNameSl = $form.getRef('companyNameSl').uiTextBox();
        $form.$companyNameEl = $form.getRef('companyNameEl').uiTextBox();
        $form.$mapLocation = $form.getRef('mapLocation').uiMapPicker();
        $form.$latitude = $form.getRef('latitude').uiTextBox();
        $form.$longitude = $form.getRef('longitude').uiTextBox();

        $form.$slAddressContainer = $form.getRef('slAddressContainer');
        $form.$slAddressContainer.hide();
        $form.$elAddressContainer = $form.getRef('elAddressContainer');
        $form.$elAddressContainer.hide();

        $form.$languageCodeAddress = $form.getRef('languageCodeAddress').uiLangBox();
        $form.$captionAddress = $form.getRef('captionAddress').uiTextBox();
        $form.$captionAddressSl = $form.getRef('captionAddressSl').uiTextBox();
        $form.$captionAddressEl = $form.getRef('captionAddressEl').uiTextBox();
        $form.$addres = $form.getRef('addres').uiTextArea();
        $form.$address = $form.getRef('address').uiTable(cartHeadsAddress);
        $form.$addAddress = $form.getRef('addAddress');
        $form.$deleteAddress = $form.getRef('deleteAddress');
        $form.$editAddress = $form.getRef('editAddress');

        // dialog

        $form.$editDialogAddress = $form.getRef('editDialogAddress');

        $form.$editDialogAddress.$slEditAddressContainer = $form.$editDialogAddress.getRef('slEditAddressContainer');
        $form.$editDialogAddress.$slEditAddressContainer.hide();
        $form.$editDialogAddress.$elEditAddressContainer = $form.$editDialogAddress.getRef('elEditAddressContainer');
        $form.$editDialogAddress.$elEditAddressContainer.hide();

        $form.$editDialogAddress.$languageCodeEditAddress = $form.$editDialogAddress.getRef('languageCodeEditAddress').uiLangBox();
        $form.$editDialogAddress.$editCaptionAddress = $form.$editDialogAddress.getRef('editCaptionAddress').uiTextBox();
        $form.$editDialogAddress.$editCaptionAddressSl = $form.$editDialogAddress.getRef('editCaptionAddressSl').uiTextBox();
        $form.$editDialogAddress.$editCaptionAddressEl = $form.$editDialogAddress.getRef('editCaptionAddressEl').uiTextBox();
        $form.$editDialogAddress.$editAddres = $form.$editDialogAddress.getRef('editAddres').uiTextArea();

        $form.$editDialogAddress.$button = $form.$editDialogAddress.find('.actions');

        $form.$editDialogAddress.$button.$saveAddress = $form.$editDialogAddress.$button.getRef('saveAddress').uiButton();
        $form.$editDialogAddress.$button.$cancelAddress = $form.$editDialogAddress.$button.getRef('cancelAddress').uiButton();

        // end dialog

        $form.$slPhoneNumberContainer = $form.getRef('slPhoneNumberContainer');
        $form.$slPhoneNumberContainer.hide();
        $form.$elPhoneNumberContainer = $form.getRef('elPhoneNumberContainer');
        $form.$elPhoneNumberContainer.hide();

        $form.$languageCodePhoneNumber = $form.getRef('languageCodePhoneNumber').uiLangBox();
        $form.$captionPhoneNumber = $form.getRef('captionPhoneNumber').uiTextBox();
        $form.$captionPhoneNumberSl = $form.getRef('captionPhoneNumberSl').uiTextBox();
        $form.$captionPhoneNumberEl = $form.getRef('captionPhoneNumberEl').uiTextBox();
        $form.$phoneNumber = $form.getRef('phoneNumber').uiTextBox();
        $form.$phoneNumbers = $form.getRef('phoneNumbers').uiTable(cartHeadsPhoneNumbers);
        $form.$addPhoneNumbers = $form.getRef('addPhoneNumbers');
        $form.$deletePhoneNumbers = $form.getRef('deletePhoneNumbers');
        $form.$editPhoneNumbers = $form.getRef('editPhoneNumbers');

        // dialog

        $form.$editDialogPhoneNumbers = $form.getRef('editDialogPhoneNumbers');

        $form.$editDialogPhoneNumbers.$slEditPhoneNumbersContainer = $form.$editDialogPhoneNumbers.getRef('slEditPhoneNumbersContainer');
        $form.$editDialogPhoneNumbers.$slEditPhoneNumbersContainer.hide();
        $form.$editDialogPhoneNumbers.$elEditPhoneNumbersContainer = $form.$editDialogPhoneNumbers.getRef('elEditPhoneNumbersContainer');
        $form.$editDialogPhoneNumbers.$elEditPhoneNumbersContainer.hide();

        $form.$editDialogPhoneNumbers.$languageCodeEditPhoneNumbers = $form.$editDialogPhoneNumbers.getRef('languageCodeEditPhoneNumbers').uiLangBox();
        $form.$editDialogPhoneNumbers.$editCaptionPhoneNumbers = $form.$editDialogPhoneNumbers.getRef('editCaptionPhoneNumbers').uiTextBox();
        $form.$editDialogPhoneNumbers.$editCaptionPhoneNumbersSl = $form.$editDialogPhoneNumbers.getRef('editCaptionPhoneNumbersSl').uiTextBox();
        $form.$editDialogPhoneNumbers.$editCaptionPhoneNumbersEl = $form.$editDialogPhoneNumbers.getRef('editCaptionPhoneNumbersEl').uiTextBox();
        $form.$editDialogPhoneNumbers.$editPhoneNumber = $form.$editDialogPhoneNumbers.getRef('editPhoneNumber').uiTextBox();

        $form.$editDialogPhoneNumbers.$button = $form.$editDialogPhoneNumbers.find('.actions');

        $form.$editDialogPhoneNumbers.$button.$savePhoneNumbers = $form.$editDialogPhoneNumbers.$button.getRef('savePhoneNumbers').uiButton();
        $form.$editDialogPhoneNumbers.$button.$cancelPhoneNumbers = $form.$editDialogPhoneNumbers.$button.getRef('cancelPhoneNumbers').uiButton();

        // end dialog

        $form.$slMobileNumberContainer = $form.getRef('slMobileNumberContainer');
        $form.$slMobileNumberContainer.hide();
        $form.$elMobileNumberContainer = $form.getRef('elMobileNumberContainer');
        $form.$elMobileNumberContainer.hide();

        $form.$languageCodeMobileNumber = $form.getRef('languageCodeMobileNumber').uiLangBox();
        $form.$captionMobileNumber = $form.getRef('captionMobileNumber').uiTextBox();
        $form.$captionMobileNumberSl = $form.getRef('captionMobileNumberSl').uiTextBox();
        $form.$captionMobileNumberEl = $form.getRef('captionMobileNumberEl').uiTextBox();
        $form.$mobileNumber = $form.getRef('mobileNumber').uiTextBox();
        $form.$mobileNumbers = $form.getRef('mobileNumbers').uiTable(cartHeadsMobileNumbers);
        $form.$addMobileNumbers = $form.getRef('addMobileNumbers');
        $form.$deleteMobileNumbers = $form.getRef('deleteMobileNumbers');
        $form.$editMobileNumbers = $form.getRef('editMobileNumbers');

        // dialog

        $form.$editDialogMobileNumbers = $form.getRef('editDialogMobileNumbers');

        $form.$editDialogMobileNumbers.$slEditMobileNumbersContainer = $form.$editDialogMobileNumbers.getRef('slEditMobileNumbersContainer');
        $form.$editDialogMobileNumbers.$slEditMobileNumbersContainer.hide();
        $form.$editDialogMobileNumbers.$elEditMobileNumbersContainer = $form.$editDialogMobileNumbers.getRef('elEditMobileNumbersContainer');
        $form.$editDialogMobileNumbers.$elEditMobileNumbersContainer.hide();

        $form.$editDialogMobileNumbers.$languageCodeEditMobileNumbers = $form.$editDialogMobileNumbers.getRef('languageCodeEditMobileNumbers').uiLangBox();
        $form.$editDialogMobileNumbers.$editCaptionMobileNumbers = $form.$editDialogMobileNumbers.getRef('editCaptionMobileNumbers').uiTextBox();
        $form.$editDialogMobileNumbers.$editCaptionMobileNumbersSl = $form.$editDialogMobileNumbers.getRef('editCaptionMobileNumbersSl').uiTextBox();
        $form.$editDialogMobileNumbers.$editCaptionMobileNumbersEl = $form.$editDialogMobileNumbers.getRef('editCaptionMobileNumbersEl').uiTextBox();
        $form.$editDialogMobileNumbers.$editMobileNumber = $form.$editDialogMobileNumbers.getRef('editMobileNumber').uiTextBox();

        $form.$editDialogMobileNumbers.$button = $form.$editDialogMobileNumbers.find('.actions');

        $form.$editDialogMobileNumbers.$button.$saveMobileNumbers = $form.$editDialogMobileNumbers.$button.getRef('saveMobileNumbers').uiButton();
        $form.$editDialogMobileNumbers.$button.$cancelMobileNumbers = $form.$editDialogMobileNumbers.$button.getRef('cancelMobileNumbers').uiButton();

        // end dialog

        $form.$slEmailContainer = $form.getRef('slEmailContainer');
        $form.$slEmailContainer.hide();
        $form.$elEmailContainer = $form.getRef('elEmailContainer');
        $form.$elEmailContainer.hide();

        $form.$languageCodeEmail = $form.getRef('languageCodeEmail').uiLangBox();
        $form.$captionEmail = $form.getRef('captionEmail').uiTextBox();
        $form.$captionEmailSl = $form.getRef('captionEmailSl').uiTextBox();
        $form.$captionEmailEl = $form.getRef('captionEmailEl').uiTextBox();
        $form.$email = $form.getRef('email').uiTextBox();
        $form.$emails = $form.getRef('emails').uiTable(cartHeadsEmails);
        $form.$addEmails = $form.getRef('addEmails');
        $form.$deleteEmails = $form.getRef('deleteEmails');
        $form.$editEmails = $form.getRef('editEmails');

        // dialog

        $form.$editDialogEmails = $form.getRef('editDialogEmails');

        $form.$editDialogEmails.$slEditEmailsContainer = $form.$editDialogEmails.getRef('slEditEmailsContainer');
        $form.$editDialogEmails.$slEditEmailsContainer.hide();
        $form.$editDialogEmails.$elEditEmailsContainer = $form.$editDialogEmails.getRef('elEditEmailsContainer');
        $form.$editDialogEmails.$elEditEmailsContainer.hide();

        $form.$editDialogEmails.$languageCodeEditEmails = $form.$editDialogEmails.getRef('languageCodeEditEmails').uiLangBox();
        $form.$editDialogEmails.$editCaptionEmails = $form.$editDialogEmails.getRef('editCaptionEmails').uiTextBox();
        $form.$editDialogEmails.$editCaptionEmailsSl = $form.$editDialogEmails.getRef('editCaptionEmailsSl').uiTextBox();
        $form.$editDialogEmails.$editCaptionEmailsEl = $form.$editDialogEmails.getRef('editCaptionEmailsEl').uiTextBox();
        $form.$editDialogEmails.$editEmail = $form.$editDialogEmails.getRef('editEmail').uiTextBox();

        $form.$editDialogEmails.$button = $form.$editDialogEmails.find('.actions');

        $form.$editDialogEmails.$button.$saveEmails = $form.$editDialogEmails.$button.getRef('saveEmails').uiButton();
        $form.$editDialogEmails.$button.$cancelEmails = $form.$editDialogEmails.$button.getRef('cancelEmails').uiButton();

        // end dialog

        $form.$button = $form.find('#button');
        $form.$button.$save = $form.$button.getRef('save').uiButton();
        $form.$button.$cancel = $form.$button.getRef('cancel').uiButton();

    };

    this.driveEvents = function () {
        // tab

        $tab.$form.on('click', this.editData.bind(this));

        // preview

        $preview.$button.$edit.on('click', function () {
            $tab.$form.trigger('click');
        });


        // form

        $form.$addAddress.on('click', this.addToCartAddress.bind(this));
        $form.$deleteAddress.on('click', this.deleteFromCartAddress.bind(this));
        $form.$editAddress.on('click', this.editCartAddress.bind(this));
        $form.$address.on('dblclick', this.editCartAddress.bind(this));
        $form.$editDialogAddress.$button.$saveAddress.on('click', this.saveCartAddress.bind(this));

        $form.$addPhoneNumbers.on('click', this.addToCartPhoneNumbers.bind(this));
        $form.$deletePhoneNumbers.on('click', this.deleteFromCartPhoneNumbers.bind(this));
        $form.$editPhoneNumbers.on('click', this.editCartPhoneNumbers.bind(this));
        $form.$phoneNumbers.on('dblclick', this.editCartPhoneNumbers.bind(this));
        $form.$editDialogPhoneNumbers.$button.$savePhoneNumbers.on('click', this.saveCartPhoneNumbers.bind(this));

        $form.$addMobileNumbers.on('click', this.addToCartMobileNumbers.bind(this));
        $form.$deleteMobileNumbers.on('click', this.deleteFromCartMobileNumbers.bind(this));
        $form.$editMobileNumbers.on('click', this.editCartMobileNumbers.bind(this));
        $form.$mobileNumbers.on('dblclick', this.editCartMobileNumbers.bind(this));
        $form.$editDialogMobileNumbers.$button.$saveMobileNumbers.on('click', this.saveCartMobileNumbers.bind(this));

        $form.$addEmails.on('click', this.addToCartEmails.bind(this));
        $form.$deleteEmails.on('click', this.deleteFromCartEmails.bind(this));
        $form.$editEmails.on('click', this.editCartEmails.bind(this));
        $form.$emails.on('dblclick', this.editCartEmails.bind(this));
        $form.$editDialogEmails.$button.$saveEmails.on('click', this.saveCartEmails.bind(this));

        $form.$button.$save.on('click', this.saveData.bind(this));

        $form.$mapLocation.on('change', function () {
            var location = $form.$mapLocation.getValue().split(',');
            $form.$latitude.setValue(location[0]);
            $form.$longitude.setValue(location[1]);
        });

        $form.$button.$cancel.on('click', function () {
            $tab.$preview.trigger('click');
        });

        $form.$languageCode.on('click', this.showHideLangContainers.bind(this));
        $form.$languageCodeAddress.on('click', this.showHideLangAddressContainers.bind(this));
        $form.$editDialogAddress.$languageCodeEditAddress.on('click', this.showHideEditLangAddressContainers.bind(this));
        $form.$editDialogPhoneNumbers.$languageCodeEditPhoneNumbers.on('click', this.showHideEditLangPhoneNumbersContainers.bind(this));
        $form.$editDialogMobileNumbers.$languageCodeEditMobileNumbers.on('click', this.showHideEditLangMobileNumbersContainers.bind(this));
        $form.$editDialogEmails.$languageCodeEditEmails.on('click', this.showHideEditLangEmailsContainers.bind(this));
        $form.$languageCodePhoneNumber.on('click', this.showHideLangPhoneNumberContainers.bind(this));
        $form.$languageCodeMobileNumber.on('click', this.showHideLangMobileNumberContainers.bind(this));
        $form.$languageCodeEmail.on('click', this.showHideLangEmailContainers.bind(this));

    };

    this.driveFieldSequence = function () {

    };

    this.reload = function () {
        $tab.$preview.trigger('click');



    };

    this.showHideLangContainers = function () {

        if ($form.$languageCode.getValue() == localStorage.primaryLang) {
            $form.$slContainer.hide();
            $form.$elContainer.hide();
        } else if ($form.$languageCode.getValue() == localStorage.secondaryLang) {
            $form.$slContainer.show();
            $form.$elContainer.hide();
        } else if ($form.$languageCode.getValue() == localStorage.extendedLang) {
            $form.$slContainer.hide();
            $form.$elContainer.show();
        }
    };

    this.showHideLangAddressContainers = function () {

        if ($form.$languageCodeAddress.getValue() == localStorage.primaryLang) {
            $form.$slAddressContainer.hide();
            $form.$elAddressContainer.hide();
        } else if ($form.$languageCodeAddress.getValue() == localStorage.secondaryLang) {
            $form.$slAddressContainer.show();
            $form.$elAddressContainer.hide();
        } else if ($form.$languageCodeAddress.getValue() == localStorage.extendedLang) {
            $form.$slAddressContainer.hide();
            $form.$elAddressContainer.show();
        }
    };

    this.showHideEditLangAddressContainers = function () {

        if ($form.$editDialogAddress.$languageCodeEditAddress.getValue() == localStorage.primaryLang) {
            $form.$editDialogAddress.$slEditAddressContainer.hide();
            $form.$editDialogAddress.$elEditAddressContainer.hide();
        } else if ($form.$editDialogAddress.$languageCodeEditAddress.getValue() == localStorage.secondaryLang) {
            $form.$editDialogAddress.$slEditAddressContainer.show();
            $form.$editDialogAddress.$elEditAddressContainer.hide();
        } else if ($form.$editDialogAddress.$languageCodeEditAddress.getValue() == localStorage.extendedLang) {
            $form.$editDialogAddress.$slEditAddressContainer.hide();
            $form.$editDialogAddress.$elEditAddressContainer.show();
        }
    };

    this.showHideLangPhoneNumberContainers = function () {

        if ($form.$languageCodePhoneNumber.getValue() == localStorage.primaryLang) {
            $form.$slPhoneNumberContainer.hide();
            $form.$elPhoneNumberContainer.hide();
        } else if ($form.$languageCodePhoneNumber.getValue() == localStorage.secondaryLang) {
            $form.$slPhoneNumberContainer.show();
            $form.$elPhoneNumberContainer.hide();
        } else if ($form.$languageCodePhoneNumber.getValue() == localStorage.extendedLang) {
            $form.$slPhoneNumberContainer.hide();
            $form.$elPhoneNumberContainer.show();
        }
    };

    this.showHideEditLangPhoneNumbersContainers = function () {

        if ($form.$editDialogPhoneNumbers.$languageCodeEditPhoneNumbers.getValue() == localStorage.primaryLang) {
            $form.$editDialogPhoneNumbers.$slEditPhoneNumbersContainer.hide();
            $form.$editDialogPhoneNumbers.$elEditPhoneNumbersContainer.hide();
        } else if ($form.$editDialogPhoneNumbers.$languageCodeEditPhoneNumbers.getValue() == localStorage.secondaryLang) {
            $form.$editDialogPhoneNumbers.$slEditPhoneNumbersContainer.show();
            $form.$editDialogPhoneNumbers.$elEditPhoneNumbersContainer.hide();
        } else if ($form.$editDialogPhoneNumbers.$languageCodeEditPhoneNumbers.getValue() == localStorage.extendedLang) {
            $form.$editDialogPhoneNumbers.$slEditPhoneNumbersContainer.hide();
            $form.$editDialogPhoneNumbers.$elEditPhoneNumbersContainer.show();
        }
    };

    this.showHideLangMobileNumberContainers = function () {

        if ($form.$languageCodeMobileNumber.getValue() == localStorage.primaryLang) {
            $form.$slMobileNumberContainer.hide();
            $form.$elMobileNumberContainer.hide();
        } else if ($form.$languageCodeMobileNumber.getValue() == localStorage.secondaryLang) {
            $form.$slMobileNumberContainer.show();
            $form.$elMobileNumberContainer.hide();
        } else if ($form.$languageCodeMobileNumber.getValue() == localStorage.extendedLang) {
            $form.$slMobileNumberContainer.hide();
            $form.$elMobileNumberContainer.show();
        }
    };

    this.showHideEditLangMobileNumbersContainers = function () {

        if ($form.$editDialogMobileNumbers.$languageCodeEditMobileNumbers.getValue() == localStorage.primaryLang) {
            $form.$editDialogMobileNumbers.$slEditMobileNumbersContainer.hide();
            $form.$editDialogMobileNumbers.$elEditMobileNumbersContainer.hide();
        } else if ($form.$editDialogMobileNumbers.$languageCodeEditMobileNumbers.getValue() == localStorage.secondaryLang) {
            $form.$editDialogMobileNumbers.$slEditMobileNumbersContainer.show();
            $form.$editDialogMobileNumbers.$elEditMobileNumbersContainer.hide();
        } else if ($form.$editDialogMobileNumbers.$languageCodeEditMobileNumbers.getValue() == localStorage.extendedLang) {
            $form.$editDialogMobileNumbers.$slEditMobileNumbersContainer.hide();
            $form.$editDialogMobileNumbers.$elEditMobileNumbersContainer.show();
        }
    };

    this.showHideLangEmailContainers = function () {

        if ($form.$languageCodeEmail.getValue() == localStorage.primaryLang) {
            $form.$slEmailContainer.hide();
            $form.$elEmailContainer.hide();
        } else if ($form.$languageCodeEmail.getValue() == localStorage.secondaryLang) {
            $form.$slEmailContainer.show();
            $form.$elEmailContainer.hide();
        } else if ($form.$languageCodeEmail.getValue() == localStorage.extendedLang) {
            $form.$slEmailContainer.hide();
            $form.$elEmailContainer.show();
        }
    };

    this.showHideEditLangEmailsContainers = function () {

        if ($form.$editDialogEmails.$languageCodeEditEmails.getValue() == localStorage.primaryLang) {
            $form.$editDialogEmails.$slEditEmailsContainer.hide();
            $form.$editDialogEmails.$elEditEmailsContainer.hide();
        } else if ($form.$editDialogEmails.$languageCodeEditEmails.getValue() == localStorage.secondaryLang) {
            $form.$editDialogEmails.$slEditEmailsContainer.show();
            $form.$editDialogEmails.$elEditEmailsContainer.hide();
        } else if ($form.$editDialogEmails.$languageCodeEditEmails.getValue() == localStorage.extendedLang) {
            $form.$editDialogEmails.$slEditEmailsContainer.hide();
            $form.$editDialogEmails.$elEditEmailsContainer.show();
        }
    };

    this.detailData = function (callback) {
        $loading.show();
        $form.$languageCode.renderApi('Language/readData', { text: 'language', flag: 'flag' });
        $form.$languageCodeAddress.renderApi('Language/readData', { text: 'language', flag: 'flag' });
        $form.$editDialogAddress.$languageCodeEditAddress.renderApi('Language/readData', { text: 'language', flag: 'flag' });
        $form.$editDialogPhoneNumbers.$languageCodeEditPhoneNumbers.renderApi('Language/readData', { text: 'language', flag: 'flag' });
        $form.$editDialogMobileNumbers.$languageCodeEditMobileNumbers.renderApi('Language/readData', { text: 'language', flag: 'flag' });
        $form.$editDialogEmails.$languageCodeEditEmails.renderApi('Language/readData', { text: 'language', flag: 'flag' });
        $form.$languageCodePhoneNumber.renderApi('Language/readData', { text: 'language', flag: 'flag' });
        $form.$languageCodeMobileNumber.renderApi('Language/readData', { text: 'language', flag: 'flag' });
        $form.$languageCodeEmail.renderApi('Language/readData', { text: 'language', flag: 'flag' });
        var url = 'Language/detailData';

        api.post(url, function (result) {

            if (result.status == 'success') {
                $languageS = result.data.secondaryEnabledId;
                $languageE = result.data.extendedEnabledId;

                if ($languageS == 1)
                    $preview.$slContainer.show();
                else
                    $preview.$slContainer.hide();

                if ($languageE == 1)
                    $preview.$elContainer.show();
                else
                    $preview.$elContainer.hide();

                $preview.$labelSl.html(result.data.secondaryLanguage);
                $preview.$labelEl.html(result.data.extendedLanguage);
            }
        });

        var url = 'ContactProfile/detailData';

        api.post(url, function (result) {

            if (result.status == 'success') {

                $preview.$companyName.setValue(result.data.companyName);
                $preview.$companyNameSl.setValue(result.data.companyName_sl);
                $preview.$companyNameEl.setValue(result.data.companyName_el);

                $preview.$mapLocation.setValue(result.data.latitude, result.data.longitude);
                $preview.$latitude.setValue(result.data.latitude);
                $preview.$longitude.setValue(result.data.longitude);

                rsAddress = result.data.address;
                this.renderViewCartAddress();

                rsPhoneNumbers = result.data.phoneNumbers;
                this.renderViewCartPhoneNumbers();

                rsMobileNumbers = result.data.mobileNumbers;
                this.renderViewCartMobileNumbers();

                rsEmails = result.data.emails;
                this.renderViewCartEmails();

                if ($.isFunction(callback))
                    callback();
            }

            $loading.hide();
        }.bind(this));
    };

    this.serializeData = function () {
        var data = {
            companyName: $form.$companyName.getValue(),
            companyNameSl: $form.$companyNameSl.getValue(),
            companyNameEl: $form.$companyNameEl.getValue(),
            latitude: $form.$latitude.getValue(),
            longitude: $form.$longitude.getValue(),
            address: rsAddress,
            phoneNumbers: rsPhoneNumbers,
            mobileNumbers: rsMobileNumbers,
            emails: rsEmails,
        };

        return data;
    };

    this.clearData = function () {
        $form.clear();

        var url = 'Language/detailData';

        api.post(url, function (result) {

            if (result.status == 'success') {
                $form.$labelSl.html(result.data.secondaryLanguage);
                $form.$labelEl.html(result.data.extendedLanguage);
            }
        });

        $form.$languageCode.setValue(localStorage.primaryLang);
        this.showHideLangContainers();

        $form.$languageCodeAddress.setValue(localStorage.primaryLang);
        this.showHideLangAddressContainers();

        $form.$languageCodePhoneNumber.setValue(localStorage.primaryLang);
        this.showHideLangPhoneNumberContainers();

        $form.$languageCodeMobileNumber.setValue(localStorage.primaryLang);
        this.showHideLangMobileNumberContainers();

        $form.$languageCodeEmail.setValue(localStorage.primaryLang);
        this.showHideLangEmailContainers();

        $form.$companyName.clear();
        $form.$companyNameSl.clear();
        $form.$companyNameEl.clear();
        $form.$latitude.clear();
        $form.$longitude.clear();

        rsAddress = new Array();
        this.renderEditCartAddress();

        rsPhoneNumbers = new Array();
        this.renderEditCartPhoneNumbers();

        rsMobileNumbers = new Array();
        this.renderEditCartMobileNumbers();

        rsEmails = new Array();
        this.renderEditCartEmails();

    };



    this.editData = function () {
        $loading.show();

        this.clearData();

        var url = 'ContactProfile/detailData';

        api.post(url, function (result) {

            if (result.status == 'success') {
                $form.$companyName.setValue(result.data.companyName);
                $form.$companyNameSl.setValue(result.data.companyName_sl);
                $form.$companyNameEl.setValue(result.data.companyName_el);
                $form.$mapLocation.setValue(result.data.latitude, result.data.longitude);
                $form.$latitude.setValue(result.data.latitude);
                $form.$longitude.setValue(result.data.longitude);

                rsAddress = result.data.address;
                this.renderEditCartAddress();

                rsPhoneNumbers = result.data.phoneNumbers;
                this.renderEditCartPhoneNumbers();

                rsMobileNumbers = result.data.mobileNumbers;
                this.renderEditCartMobileNumbers();

                rsEmails = result.data.emails;
                this.renderEditCartEmails();

                $form.$companyName.focus();
            }

            $loading.hide();
        }.bind(this));
    };

    this.saveData = function () {
        $form.$button.$save.loading();

        var url = 'ContactProfile/saveData';
        var data = this.serializeData();

        api.post(url, data, function (result) {

            switch (result.status) {
                case 'success':
                    $splash.show(result.message);

                    this.detailData(function () {
                        $tab.$preview.trigger('click');

                        $form.$button.$save.release();
                        window.scrollTo(0, 0);
                    }.bind(this));

                    break;
                case 'invalid':
                    $form.error(result);
                    $form.$button.$save.release();
            }

        }.bind(this));
    };

    this.renderViewCartAddress = function () {
        this.renderTable($preview.$address, rsAddress, cartFieldsAddress, 100);
    };

    this.renderEditCartAddress = function () {
        this.renderTable($form.$address, rsAddress, cartFieldsAddress, 100);
    };

    this.addToCartAddress = function () {

        if (!$form.$captionAddress.getValue()) {
            $msgbox.alert('CAPTION_REQUIRED');
            return;
        }

        if (!$form.$addres.getValue()) {
            $msgbox.alert('ADDRESS_REQUIRED');
            return;
        }

        var found = false;
        rsAddress.forEach(function (data) {

            if (data.caption == $form.$captionAddress.getValue())
                found = true;
        });

        if (found) {
            $msgbox.alert('CAPTION_IS_REG');
            return;
        }

        rsAddress.push({
            id: this.guid(),
            caption: $form.$captionAddress.getValue(),
            caption_sl: $form.$captionAddressSl.getValue(),
            caption_el: $form.$captionAddressEl.getValue(),
            address: $form.$addres.getValue(),
        });

        $form.$captionAddress.clear();
        $form.$captionAddressSl.clear();
        $form.$captionAddressEl.clear();
        $form.$addres.clear();
        $form.$captionAddress.focus();

        this.renderEditCartAddress();
    };

    this.editCartAddress = function () {

        $form.$editDialogAddress.$languageCodeEditAddress.setValue(localStorage.primaryLang);
        this.showHideEditLangAddressContainers();
        $form.$editDialogAddress.$editCaptionAddress.clear();
        $form.$editDialogAddress.$editCaptionAddressSl.clear();
        $form.$editDialogAddress.$editCaptionAddressEl.clear();
        $form.$editDialogAddress.$editAddres.clear();

        var id = $form.$address.getValue();

        if (!id)
            return;

        var rowAddress = [];

        $.each(rsAddress, function (i, row) {
            if (row.id == id)
                rowAddress = row;
        });

        $form.$editDialogAddress.$editCaptionAddress.setValue(rowAddress.caption);
        $form.$editDialogAddress.$editCaptionAddressSl.setValue(rowAddress.caption_sl);
        $form.$editDialogAddress.$editCaptionAddressEl.setValue(rowAddress.caption_el);
        $form.$editDialogAddress.$editAddres.setValue(rowAddress.address);

        var lastId = rowAddress.pagesId;

        $form.$editDialogAddress.modal('show');
    };

    this.saveCartAddress = function () {

        var id = $form.$address.getValue();

        $.each(rsAddress, function (i, row) {
            if (row.id == id) {

                rsAddress[i].caption = $form.$editDialogAddress.$editCaptionAddress.getValue();
                rsAddress[i].caption_sl = $form.$editDialogAddress.$editCaptionAddressSl.getValue();
                rsAddress[i].caption_el = $form.$editDialogAddress.$editCaptionAddressEl.getValue();
                rsAddress[i].address = $form.$editDialogAddress.$editAddres.getValue();
            }
        });

        this.renderEditCartAddress();
        $form.$editDialogAddress.modal('hide');
    };

    this.deleteFromCartAddress = function () {
        var checkedId = $form.$address.getCheckedValues();
        if (checkedId.length <= 0)
            return;

        $msgbox.confirm('Delete selected data', function () {

            checkedId.forEach(function (indexId) {
                var i = -1;

                rsAddress.forEach(function (data) {
                    i++;

                    if (data.id == indexId)
                        rsAddress.splice(i, 1);

                });
            });

            this.renderEditCartAddress();
        }.bind(this));

    };

    this.renderViewCartPhoneNumbers = function () {
        this.renderTable($preview.$phoneNumbers, rsPhoneNumbers, cartFieldsPhoneNumbers, 100);
    };

    this.renderEditCartPhoneNumbers = function () {
        this.renderTable($form.$phoneNumbers, rsPhoneNumbers, cartFieldsPhoneNumbers, 100);
    };

    this.addToCartPhoneNumbers = function () {

        if (!$form.$captionPhoneNumber.getValue()) {
            $msgbox.alert('CAPTION_REQUIRED');
            return;
        }

        if (!$form.$phoneNumber.getValue()) {
            $msgbox.alert('PHONE_NUMBER_REQUIRED');
            return;
        }

        var found = false;
        rsPhoneNumbers.forEach(function (data) {

            if (data.caption == $form.$captionPhoneNumber.getValue())
                found = true;
        });

        if (found) {
            $msgbox.alert('CAPTION_IS_REG');
            return;
        }

        rsPhoneNumbers.push({
            id: this.guid(),
            caption: $form.$captionPhoneNumber.getValue(),
            caption_sl: $form.$captionPhoneNumberSl.getValue(),
            caption_el: $form.$captionPhoneNumberEl.getValue(),
            phoneNumber: $form.$phoneNumber.getValue(),
        });

        $form.$captionPhoneNumber.clear();
        $form.$captionPhoneNumberSl.clear();
        $form.$captionPhoneNumberEl.clear();
        $form.$phoneNumber.clear();
        $form.$captionPhoneNumber.focus();

        this.renderEditCartPhoneNumbers();
    };

    this.editCartPhoneNumbers = function () {

        $form.$editDialogPhoneNumbers.$languageCodeEditPhoneNumbers.setValue(localStorage.primaryLang);
        this.showHideEditLangPhoneNumbersContainers();
        $form.$editDialogPhoneNumbers.$editCaptionPhoneNumbers.clear();
        $form.$editDialogPhoneNumbers.$editCaptionPhoneNumbersSl.clear();
        $form.$editDialogPhoneNumbers.$editCaptionPhoneNumbersEl.clear();
        $form.$editDialogPhoneNumbers.$editPhoneNumber.clear();

        var id = $form.$phoneNumbers.getValue();

        if (!id)
            return;

        var rowPhoneNumbers = [];

        $.each(rsPhoneNumbers, function (i, row) {
            if (row.id == id)
                rowPhoneNumbers = row;
        });

        $form.$editDialogPhoneNumbers.$editCaptionPhoneNumbers.setValue(rowPhoneNumbers.caption);
        $form.$editDialogPhoneNumbers.$editCaptionPhoneNumbersSl.setValue(rowPhoneNumbers.caption_sl);
        $form.$editDialogPhoneNumbers.$editCaptionPhoneNumbersEl.setValue(rowPhoneNumbers.caption_el);
        $form.$editDialogPhoneNumbers.$editPhoneNumber.setValue(rowPhoneNumbers.phoneNumber);

        var lastId = rowPhoneNumbers.pagesId;

        $form.$editDialogPhoneNumbers.modal('show');
    };

    this.saveCartPhoneNumbers = function () {

        var id = $form.$phoneNumbers.getValue();

        $.each(rsPhoneNumbers, function (i, row) {
            if (row.id == id) {

                rsPhoneNumbers[i].caption = $form.$editDialogPhoneNumbers.$editCaptionPhoneNumbers.getValue();
                rsPhoneNumbers[i].caption_sl = $form.$editDialogPhoneNumbers.$editCaptionPhoneNumbersSl.getValue();
                rsPhoneNumbers[i].caption_el = $form.$editDialogPhoneNumbers.$editCaptionPhoneNumbersEl.getValue();
                rsPhoneNumbers[i].phoneNumber = $form.$editDialogPhoneNumbers.$editPhoneNumber.getValue();
            }
        });

        this.renderEditCartPhoneNumbers();
        $form.$editDialogPhoneNumbers.modal('hide');
    };

    this.deleteFromCartPhoneNumbers = function () {
        var checkedId = $form.$phoneNumbers.getCheckedValues();
        if (checkedId.length <= 0)
            return;

        $msgbox.confirm('Delete selected data', function () {

            checkedId.forEach(function (indexId) {
                var i = -1;

                rsPhoneNumbers.forEach(function (data) {
                    i++;

                    if (data.id == indexId)
                        rsPhoneNumbers.splice(i, 1);

                });
            });

            this.renderEditCartPhoneNumbers();
        }.bind(this));

    };

    this.renderViewCartMobileNumbers = function () {
        this.renderTable($preview.$mobileNumbers, rsMobileNumbers, cartFieldsMobileNumbers, 100);
    };

    this.renderEditCartMobileNumbers = function () {
        this.renderTable($form.$mobileNumbers, rsMobileNumbers, cartFieldsMobileNumbers, 100);
    };

    this.addToCartMobileNumbers = function () {

        if (!$form.$captionMobileNumber.getValue()) {
            $msgbox.alert('CAPTION_REQUIRED');
            return;
        }

        if (!$form.$mobileNumber.getValue()) {
            $msgbox.alert('PHONE_NUMBER_REQUIRED');
            return;
        }

        var found = false;
        rsMobileNumbers.forEach(function (data) {

            if (data.caption == $form.$captionMobileNumber.getValue())
                found = true;
        });

        if (found) {
            $msgbox.alert('CAPTION_IS_REG');
            return;
        }

        rsMobileNumbers.push({
            id: this.guid(),
            caption: $form.$captionMobileNumber.getValue(),
            caption_sl: $form.$captionMobileNumberSl.getValue(),
            caption_el: $form.$captionMobileNumberEl.getValue(),
            mobileNumber: $form.$mobileNumber.getValue(),
        });

        $form.$captionMobileNumber.clear();
        $form.$captionMobileNumberSl.clear();
        $form.$captionMobileNumberEl.clear();
        $form.$mobileNumber.clear();
        $form.$captionMobileNumber.focus();

        this.renderEditCartMobileNumbers();
    };

    this.editCartMobileNumbers = function () {

        $form.$editDialogMobileNumbers.$languageCodeEditMobileNumbers.setValue(localStorage.primaryLang);
        this.showHideEditLangMobileNumbersContainers();
        $form.$editDialogMobileNumbers.$editCaptionMobileNumbers.clear();
        $form.$editDialogMobileNumbers.$editCaptionMobileNumbersSl.clear();
        $form.$editDialogMobileNumbers.$editCaptionMobileNumbersEl.clear();
        $form.$editDialogMobileNumbers.$editMobileNumber.clear();

        var id = $form.$mobileNumbers.getValue();

        if (!id)
            return;

        var rowMobileNumbers = [];

        $.each(rsMobileNumbers, function (i, row) {
            if (row.id == id)
                rowMobileNumbers = row;
        });

        $form.$editDialogMobileNumbers.$editCaptionMobileNumbers.setValue(rowMobileNumbers.caption);
        $form.$editDialogMobileNumbers.$editCaptionMobileNumbersSl.setValue(rowMobileNumbers.caption_sl);
        $form.$editDialogMobileNumbers.$editCaptionMobileNumbersEl.setValue(rowMobileNumbers.caption_el);
        $form.$editDialogMobileNumbers.$editMobileNumber.setValue(rowMobileNumbers.mobileNumber);

        var lastId = rowMobileNumbers.pagesId;

        $form.$editDialogMobileNumbers.modal('show');
    };

    this.saveCartMobileNumbers = function () {

        var id = $form.$mobileNumbers.getValue();

        $.each(rsMobileNumbers, function (i, row) {
            if (row.id == id) {

                rsMobileNumbers[i].caption = $form.$editDialogMobileNumbers.$editCaptionMobileNumbers.getValue();
                rsMobileNumbers[i].caption_sl = $form.$editDialogMobileNumbers.$editCaptionMobileNumbersSl.getValue();
                rsMobileNumbers[i].caption_el = $form.$editDialogMobileNumbers.$editCaptionMobileNumbersEl.getValue();
                rsMobileNumbers[i].mobileNumber = $form.$editDialogMobileNumbers.$editMobileNumber.getValue();
            }
        });

        this.renderEditCartMobileNumbers();
        $form.$editDialogMobileNumbers.modal('hide');
    };

    this.deleteFromCartMobileNumbers = function () {
        var checkedId = $form.$mobileNumbers.getCheckedValues();
        if (checkedId.length <= 0)
            return;

        $msgbox.confirm('Delete selected data', function () {

            checkedId.forEach(function (indexId) {
                var i = -1;

                rsMobileNumbers.forEach(function (data) {
                    i++;

                    if (data.id == indexId)
                        rsMobileNumbers.splice(i, 1);

                });
            });

            this.renderEditCartMobileNumbers();
        }.bind(this));

    };

    this.renderViewCartEmails = function () {
        this.renderTable($preview.$emails, rsEmails, cartFieldsEmails, 100);
    };

    this.renderEditCartEmails = function () {
        this.renderTable($form.$emails, rsEmails, cartFieldsEmails, 100);
    };

    this.addToCartEmails = function () {

        if (!$form.$email.getValue()) {
            $msgbox.alert('EMAIL_REQUIRED');
            return;
        }

        var found = false;
        rsEmails.forEach(function (data) {

            if (data.email == $form.$email.getValue())
                found = true;
        });

        if (found) {
            $msgbox.alert('EMAIL_IS_REG');
            return;
        }

        rsEmails.push({
            id: this.guid(),
            caption: $form.$captionEmail.getValue(),
            caption_sl: $form.$captionEmailSl.getValue(),
            caption_el: $form.$captionEmailEl.getValue(),
            email: $form.$email.getValue(),
        });

        $form.$captionEmail.clear();
        $form.$captionEmailSl.clear();
        $form.$captionEmailEl.clear();
        $form.$email.clear();
        $form.$captionEmail.focus();

        this.renderEditCartEmails();
    };

    this.editCartEmails = function () {

        $form.$editDialogEmails.$languageCodeEditEmails.setValue(localStorage.primaryLang);
        this.showHideEditLangEmailsContainers();
        $form.$editDialogEmails.$editCaptionEmails.clear();
        $form.$editDialogEmails.$editCaptionEmailsSl.clear();
        $form.$editDialogEmails.$editCaptionEmailsEl.clear();
        $form.$editDialogEmails.$editEmail.clear();

        var id = $form.$emails.getValue();

        if (!id)
            return;

        var rowEmails = [];

        $.each(rsEmails, function (i, row) {
            if (row.id == id)
                rowEmails = row;
        });

        $form.$editDialogEmails.$editCaptionEmails.setValue(rowEmails.caption);
        $form.$editDialogEmails.$editCaptionEmailsSl.setValue(rowEmails.caption_sl);
        $form.$editDialogEmails.$editCaptionEmailsEl.setValue(rowEmails.caption_el);
        $form.$editDialogEmails.$editEmail.setValue(rowEmails.email);

        var lastId = rowEmails.pagesId;

        $form.$editDialogEmails.modal('show');
    };

    this.saveCartEmails = function () {

        var id = $form.$emails.getValue();

        $.each(rsEmails, function (i, row) {
            if (row.id == id) {

                rsEmails[i].caption = $form.$editDialogEmails.$editCaptionEmails.getValue();
                rsEmails[i].caption_sl = $form.$editDialogEmails.$editCaptionEmailsSl.getValue();
                rsEmails[i].caption_el = $form.$editDialogEmails.$editCaptionEmailsEl.getValue();
                rsEmails[i].email = $form.$editDialogEmails.$editEmail.getValue();
            }
        });

        this.renderEditCartEmails();
        $form.$editDialogEmails.modal('hide');
    };

    this.deleteFromCartEmails = function () {
        var checkedId = $form.$emails.getCheckedValues();
        if (checkedId.length <= 0)
            return;

        $msgbox.confirm('Delete selected data', function () {

            checkedId.forEach(function (indexId) {
                var i = -1;

                rsEmails.forEach(function (data) {
                    i++;

                    if (data.id == indexId)
                        rsEmails.splice(i, 1);

                });
            });

            this.renderEditCartEmails();
        }.bind(this));

    };

};

dashboard.application.controllers.ContactProfile.prototype =
    Object.create(dashboard.application.core.Controller.prototype);

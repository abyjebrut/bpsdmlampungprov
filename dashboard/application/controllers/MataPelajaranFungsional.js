dashboard.application.controllers.MataPelajaranFungsional = function () {
    var $el = $page.getScope('MataPelajaranFungsional');
    dashboard.application.core.Controller.call(this, $el);

    var rs = [];
    var renderedRs = [];

    var rsRange = 10;

    var tableHeads = ['No', 'Mata Pelajaran', 'Waktu', 'Jenis Diklat', 'Tipe', 'JML JP'];
    var tableFields = ['noUrut', 'mataPelajaran', 'waktu', 'jenisDiklat', 'tipeMataPelajaran', 'jumlahJp_'];

    var saveMode;

    var $tab;
    var $list;
    var $preview;
    var $form;

    this.init = function () {

        // tab

        $tab = $el.find('#tab');
        $tab.$list = $tab.getRef('list');
        $tab.$preview = $tab.getRef('preview');
        $tab.$form = $tab.getRef('form');
        $tab.$new = $tab.getRef('new');

        // list

        $list = $el.find('#list');

        $list.$jenisDiklatId = $list.getRef('jenisDiklatId').uiComboBox({ placeholder: 'Jenis Diklat' });
        $list.$publishStatusId = $list.getRef('publishStatusId').uiRadioBox();
        $list.$search = $list.getRef('search').uiTextBox({ icon: 'search', placeholder: 'Search No Urut ...' });
        $list.$table = $list.getRef('table').uiTable({ headers: tableHeads });
        $list.$paging = $list.getRef('paging').uiPaging();

        $list.$button = $list.find('#button');
        $list.$button.$delete = $list.$button.getRef('delete').uiButton();
        $list.$button.$detail = $list.$button.getRef('detail').uiButton();
        $list.$button.$edit = $list.$button.getRef('edit').uiButton();

        $list.$button.$refresh = $list.$button.getRef('refresh').uiButton();


        // preview

        $preview = $el.find('#preview');

        $preview.$elWaktu = $preview.getRef('elWaktu');
        $preview.$elWaktu.hide();

        $preview.$elTanggal = $preview.getRef('elTanggal');
        $preview.$elTanggal.hide();

        $preview.$publishStatus = $preview.getRef('publishStatus').uiTextView();
        $preview.$noUrut = $preview.getRef('noUrut').uiTextView();
        $preview.$mataPelajaran = $preview.getRef('mataPelajaran').uiTextView();
        $preview.$jamMulai = $preview.getRef('jamMulai').uiTextView();
        $preview.$jamSelesai = $preview.getRef('jamSelesai').uiTextView();
        $preview.$jenisDiklat = $preview.getRef('jenisDiklat').uiTextView();
        $preview.$tipeWaktu = $preview.getRef('tipeWaktu').uiTextView();
        $preview.$tipeTanggal = $preview.getRef('tipeTanggal').uiTextView();
        $preview.$jumlahHari = $preview.getRef('jumlahHari').uiTextView();
        $preview.$jumlahJp = $preview.getRef('jumlahJp').uiTextView();
        $preview.$tipeMataPelajaran = $preview.getRef('tipeMataPelajaran').uiTextView();

        $preview.$button = $preview.find('#button');
        $preview.$button.$edit = $preview.$button.getRef('edit').uiButton();
        $preview.$button.$cancel = $preview.$button.getRef('cancel').uiButton();

        // form

        $form = $el.find('#form').uiForm();

        $form.$elWaktu = $form.getRef('elWaktu');
        $form.$elWaktu.hide();

        $form.$elTanggal = $form.getRef('elTanggal');
        $form.$elTanggal.hide();

        $form.$publishStatusId = $form.getRef('publishStatusId').uiRadioBox();
        $form.$noUrut = $form.getRef('noUrut').uiNumericBox({ maxlength: 2 });
        $form.$labelMapelId = $form.getRef('labelMapelId').uiComboBox();
        $form.$jamMulai = $form.getRef('jamMulai').uiNumericBox({ maxlength: 2 });
        $form.$jamMulaiMenit = $form.getRef('jamMulaiMenit').uiNumericBox({ maxlength: 2 });
        $form.$jamSelesai = $form.getRef('jamSelesai').uiNumericBox({ maxlength: 2 });
        $form.$jamSelesaiMenit = $form.getRef('jamSelesaiMenit').uiNumericBox({ maxlength: 2 });
        $form.$jumlahHari = $form.getRef('jumlahHari').uiNumericBox({ maxlength: 2 });
        $form.$jenisDiklatId = $form.getRef('jenisDiklatId').uiComboBox();
        $form.$tipeMataPelajaranId = $form.getRef('tipeMataPelajaranId').uiRadioBox();
        $form.$tipeWaktuId = $form.getRef('tipeWaktuId').uiRadioBox();
        $form.$tipeTanggalId = $form.getRef('tipeTanggalId').uiRadioBox();
        $form.$jumlahJp = $form.getRef('jumlahJp').uiNumericBox({ maxlength: 2 });

        $form.$button = $form.find('#button');
        $form.$button.$save = $form.$button.getRef('save').uiButton();
        $form.$button.$cancel = $form.$button.getRef('cancel').uiButton();

    };

    this.driveEvents = function () {
        // tab

        $tab.$preview.on('click', this.detailData.bind(this));
        $tab.$form.on('click', this.editData.bind(this));
        $tab.$new.on('click', this.newData.bind(this));

        // list

        $list.$jenisDiklatId.on('change', this.readData.bind(this));
        $list.$publishStatusId.on('click', this.readData.bind(this));
        $list.$search.on('keyup', this.searchData.bind(this));

        $list.$table.on('dblclick', function () {
            if (!$list.$search.getValue()) {
                if (rs.length > 0)
                    $tab.$preview.trigger('click');
            } else {
                if (renderedRs.length > 0)
                    $tab.$preview.trigger('click');
            }
        });

        $list.$paging.on('click', this.moveData.bind(this));
        $list.$button.$delete.on('click', this.deleteData.bind(this));

        $list.$button.$detail.on('click', function () {
            $tab.$preview.trigger('click');
        });

        $list.$button.$edit.on('click', function () {
            $tab.$form.trigger('click');
        });

        $list.$button.$refresh.on('click', this.loadData.bind(this));

        // preview

        $preview.$button.$edit.on('click', function () {
            $tab.$form.trigger('click');
        });

        $preview.$button.$cancel.on('click', function () {
            $tab.$list.trigger('click');
        });

        // form
        $form.$tipeMataPelajaranId.on('click', this.showJp.bind(this));
        $form.$tipeWaktuId.on('click', this.showHideWaktu.bind(this));
        $form.$jamMulai.on('change', this.showJp.bind(this));
        $form.$jamMulaiMenit.on('change', this.showJp.bind(this));
        $form.$jamSelesai.on('change', this.showJp.bind(this));
        $form.$jamSelesaiMenit.on('change', this.showJp.bind(this));
        $form.$button.$save.on('click', this.saveData.bind(this));

        $form.$button.$cancel.on('click', function () {
            $tab.$list.trigger('click');
        });

    };

    this.driveFieldSequence = function () {


    };

    this.reload = function () {
        $tab.$list.trigger('click');

    };

    this.loadData = function () {

        $list.$jenisDiklatId.clear();
        $list.$publishStatusId.renderApi('PublishStatus/readData', {
            text: 'publishStatus',
            done: function () {
                $list.$publishStatusId.setValue(2);
                this.readData();
            }.bind(this),
        });

        $list.$jenisDiklatId.renderApi('JenisDiklat/readDataFungsional', { text: 'jenisDiklat' });
        $form.$jenisDiklatId.renderApi('JenisDiklat/readDataFungsional', { text: 'jenisDiklat' });
        $form.$labelMapelId.renderApi('LabelMapel/readData', { text: 'labelMapel' });
        $form.$tipeMataPelajaranId.renderApi('TipeMataPelajaran/readData', { text: 'tipeMataPelajaran' });
        $form.$tipeWaktuId.renderApi('TipeWaktu/readData', { text: 'tipeWaktu' });
        $form.$tipeTanggalId.renderApi('TipeTanggal/readData', { text: 'tipeTanggal' });
        $form.$publishStatusId.renderApi('PublishStatus/readData', { text: 'publishStatus' });

    };

    this.showJp = function () {

        if ($form.$tipeWaktuId.getValue() == 1 && $form.$tipeMataPelajaranId.getValue() == 1) {
            var menitM = $form.$jamMulaiMenit.getValue()
            if (menitM == '')
                menitM = '00'
            var menitS = $form.$jamSelesaiMenit.getValue()
            if (menitS == '')
                menitS = '00'
            var tgl = new Date();
            var aJmMulai = tgl.getFullYear() +
                "-" +
                (tgl.getMonth() + 1) +
                "-" +
                tgl.getDate() + " " + $form.$jamMulai.getValue() + ":" + menitM
            var aJmSelesai = tgl.getFullYear() +
                "-" +
                (tgl.getMonth() + 1) +
                "-" +
                tgl.getDate() + " " + $form.$jamSelesai.getValue() + ":" + menitS

            var dt1 = new Date(aJmMulai);
            var dt2 = new Date(aJmSelesai);

            var diff = dt2.getTime() - dt1.getTime();
            var hh = Math.floor((diff % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var mm = Math.floor((diff % (1000 * 60 * 60)) / (1000 * 60));
            mm = mm.toString();
            if (mm == 0 || mm == '')
                mm = '00'
            if (mm.length == 1)
                mm = '0' + mm;

            var hasil = hh + '' + mm
            if (parseInt(hasil) >= 30 && parseInt(hasil) <= 100)
                $form.$jumlahJp.setValue(1);
            else if (parseInt(hasil) > 100 && parseInt(hasil) <= 145)
                $form.$jumlahJp.setValue(2);
            else if (parseInt(hasil) >= 146 && parseInt(hasil) <= 245)
                $form.$jumlahJp.setValue(3);
            else if (parseInt(hasil) >= 246 && parseInt(hasil) <= 345)
                $form.$jumlahJp.setValue(4);
            else if (parseInt(hasil) >= 346 && parseInt(hasil) <= 445)
                $form.$jumlahJp.setValue(5);
            else if (parseInt(hasil) >= 446 && parseInt(hasil) <= 545)
                $form.$jumlahJp.setValue(5);
            else
                $form.$jumlahJp.setValue(0);

        } else {
            $form.$jumlahJp.setValue(0);
        }
    };

    this.showHideWaktu = function () {
        this.showJp();
        $form.$elTanggal.hide();
        $form.$elWaktu.hide();

        if ($form.$tipeWaktuId.getValue() == 1)
            $form.$elWaktu.show();
        else
            $form.$elTanggal.show();

    };

    this.readData = function (callback) {

        if (!callback)
            $loading.show();

        var url = 'MataPelajaran/readDataFungsional';
        var data = {
            orderBy: 'jenisDiklatId, noUrut, jamMulai',
            reverse: 2,
            jenisDiklatId: $list.$jenisDiklatId.getValue(),
            publishStatusId: $list.$publishStatusId.getValue(),
        };
        rs = []
        api.post(url, data, function (result) {

            result.data.forEach(row => {
                var aWaktu = row.tipeWaktuId;
                if (aWaktu == 1) {
                    var waktu = row.jamMulai + ' s/d ' + row.jamSelesai;
                } else {
                    var waktu = row.jumlahHari + ' hari';
                }

                rs.push({
                    id: row.id,
                    publishStatusId: row.publishStatusId,
                    publishStatus: row.publishStatus,
                    noUrut: row.noUrut,
                    waktu: waktu,
                    labelMapelId: row.labelMapelId,
                    mataPelajaran: row.mataPelajaran,
                    jamMulai: row.jamMulai,
                    nilaiJamMulai: row.nilaiJamMulai,
                    _jamMulai: row._jamMulai,
                    jamMulaiMenit: row.jamMulaiMenit,
                    jamSelesai: row.jamSelesai,
                    nilaiJamSelesai: row.nilaiJamSelesai,
                    _jamSelesai: row._jamSelesai,
                    jamSelesaiMenit: row.jamSelesaiMenit,
                    jumlahHari: row.jumlahHari,
                    jenisDiklatId: row.jenisDiklatId,
                    jenisDiklat: row.jenisDiklat,
                    tipeMataPelajaranId: row.tipeMataPelajaranId,
                    tipeMataPelajaran: row.tipeMataPelajaran,
                    tipeWaktuId: row.tipeWaktuId,
                    tipeWaktu: row.tipeWaktu,
                    tipeTanggalId: row.tipeTanggalId,
                    tipeTanggal: row.tipeTanggal,
                    jumlahJp: row.jumlahJp,
                    jumlahJp_: row.jumlahJp_,
                });
            });

            var rsCount = rs.length;

            if (rsCount > 0) {
                this.dataShow();
            }
            else {
                this.dataHide();
            }


            renderedRs = this.renderTable($list.$table, rs, tableFields, rsRange);
            this.renderPaging($list.$paging, renderedRs, rsRange);

            if ($.isFunction(callback))
                callback();
            else
                $loading.hide();
        }.bind(this));
    };

    this.dataHide = function () {
        $list.$paging.hide();
        $tab.$preview.hide();
        $tab.$form.hide();
        $list.$button.$delete.hide();
        $list.$button.$edit.hide();
        $list.$button.$detail.hide();
    };

    this.dataShow = function () {
        $list.$paging.show();
        $tab.$preview.show();
        $tab.$form.show();
        $list.$button.$delete.show();
        $list.$button.$edit.show();
        $list.$button.$detail.show();
    };

    this.searchData = function () {
        var filteredRs = rs.filter(function (data) {
            var regex = new RegExp($list.$search.getValue(), 'gi');
            var found = false;

            tableFields.forEach(function (field) {
                if (data.noUrut == $list.$search.getValue())
                    // if (data[field])
                    // if (data[field].match(regex))
                    found = true;
            });

            return found;
        });

        renderedRs = this.renderTable($list.$table, filteredRs, tableFields, rsRange);
        if (renderedRs.length > 0) {
            this.renderPaging($list.$paging, renderedRs, rsRange);
            this.dataShow();
        } else {
            this.dataHide();
        }
    };

    this.moveData = function () {
        renderedRs = this.renderTable($list.$table, renderedRs, tableFields, rsRange, $list.$paging.getValue());
    };

    this.deleteData = function () {
        var checkedId = $list.$table.getCheckedValues();

        if (checkedId.length == 0) {
            $msgbox.alert('NO_DATA_CHECKED');
            return;
        };

        var url = 'MataPelajaran/multipleDeleteData';
        var data = {
            multipleId: checkedId,
        };

        $msgbox.confirm('DELETE_CHECKED_DATA', function () {
            api.post(url, data, function (result) {

                switch (result.status) {
                    case 'success':
                        $splash.show(result.message);
                        this.readData();

                        break;
                    case 'failed':
                        var htmlError = '';

                        $.each(result.data, function (key, value) {
                            htmlError += value + '<br>';
                        });
                        $msgbox.alert(htmlError);
                        this.readData();
                }

            }.bind(this));
        }.bind(this));

    };

    this.detailData = function () {
        $loading.show();

        var url = 'MataPelajaran/detailData';
        var data = {
            id: $list.$table.getValue()
        };

        api.post(url, data, function (result) {

            if (result.status == 'success') {
                $preview.$publishStatus.setValue(result.data.publishStatus);
                $preview.$noUrut.setValue(result.data.noUrut);
                $preview.$mataPelajaran.setValue(result.data.mataPelajaran);
                $preview.$jamMulai.setValue(result.data.jamMulai);
                $preview.$jamSelesai.setValue(result.data.jamSelesai);
                $preview.$jenisDiklat.setValue(result.data.jenisDiklat);
                $preview.$tipeMataPelajaran.setValue(result.data.tipeMataPelajaran);
                $preview.$tipeTanggal.setValue(result.data.tipeTanggal);
                $preview.$tipeWaktu.setValue(result.data.tipeWaktu);
                $preview.$jumlahHari.setValue(result.data.jumlahHari);
                $preview.$jumlahJp.setValue(result.data.jumlahJp_);
                $preview.$elWaktu.hide()
                $preview.$elTanggal.hide()

                if (result.data.tipeWaktuId == 1)
                    $preview.$elWaktu.show()
                else
                    $preview.$elTanggal.show()

            }

            $loading.hide();
        });
    };

    this.serializeData = function () {

        var jmMulai = $form.$jamMulai.getValue();
        if (jmMulai.length == 1) {
            jmMulai = "0" + jmMulai;
        }

        var jmMulaiMenit = $form.$jamMulaiMenit.getValue();
        if (jmMulaiMenit.length == 1) {
            jmMulaiMenit = "0" + jmMulaiMenit;
        } else if (!$form.$jamMulaiMenit.getValue()) {
            jmMulaiMenit = "00";
        }

        var jmSelesai = $form.$jamSelesai.getValue();
        if (jmSelesai.length == 1) {
            jmSelesai = "0" + jmSelesai;
        }

        var jmSelesaiMenit = $form.$jamSelesaiMenit.getValue();
        if (jmSelesaiMenit.length == 1) {
            jmSelesaiMenit = "0" + jmSelesaiMenit;
        } else if (!$form.$jamSelesaiMenit.getValue()) {
            jmSelesaiMenit = "00";
        }

        var jumlahJp = $form.$jumlahJp.getValue();
        if ($form.$tipeMataPelajaranId.getValue() == 2)
            jumlahJp = 0;

        var data = {
            publishStatusId: $form.$publishStatusId.getValue(),
            noUrut: $form.$noUrut.getValue(),
            labelMapelId: $form.$labelMapelId.getValue(),
            jamMulai: $form.$jamMulai.getValue() + ":" + jmMulaiMenit + ":" + "01",
            jamSelesai: $form.$jamSelesai.getValue() + ":" + jmSelesaiMenit + ":" + "00",
            nilaiJamMulai: jmMulai + jmMulaiMenit,
            nilaiJamSelesai: jmSelesai + jmSelesaiMenit,
            jenisDiklatId: $form.$jenisDiklatId.getValue(),
            tipeMataPelajaranId: $form.$tipeMataPelajaranId.getValue(),
            tipeWaktuId: $form.$tipeWaktuId.getValue(),
            tipeTanggalId: $form.$tipeTanggalId.getValue(),
            jumlahHari: $form.$jumlahHari.getValue(),
            jumlahJp: jumlahJp,
        };

        if (saveMode == EDIT_MODE)
            data.id = $list.$table.getValue();

        return data;
    };

    this.clearData = function () {
        $form.clear();
        $form.$noUrut.clear();
        $form.$labelMapelId.clear();
        var date = new Date();
        var jam = `${date.getHours()}`;
        var menit = `${date.getMinutes()}`;

        $form.$jamMulai.setValue(jam);
        $form.$jamMulaiMenit.setValue(menit);
        $form.$jamSelesai.setValue(jam);
        $form.$jamSelesaiMenit.setValue(menit);
        $form.$jenisDiklatId.clear();
        $form.$tipeMataPelajaranId.setValue(1);
        $form.$tipeWaktuId.setValue(1);
        $form.$tipeTanggalId.setValue(1);
        $form.$jumlahHari.clear();
        $form.$jumlahJp.clear();
        $form.$publishStatusId.setValue(2);
    };

    this.editData = function () {
        $loading.show();
        saveMode = EDIT_MODE;

        this.clearData();

        var url = 'MataPelajaran/detailData';
        var data = {
            id: $list.$table.getValue()
        };

        api.post(url, data, function (result) {

            if (result.status == 'success') {
                $form.$publishStatusId.setValue(result.data.publishStatusId);
                $form.$noUrut.setValue(result.data.noUrut);
                $form.$labelMapelId.setValue(result.data.labelMapelId);
                $form.$jamMulai.setValue(result.data._jamMulai);
                $form.$jamMulaiMenit.setValue(result.data.jamMulaiMenit);
                $form.$jamSelesai.setValue(result.data._jamSelesai);
                $form.$jamSelesaiMenit.setValue(result.data.jamSelesaiMenit);
                $form.$jenisDiklatId.setValue(result.data.jenisDiklatId);
                $form.$tipeMataPelajaranId.setValue(result.data.tipeMataPelajaranId);
                $form.$tipeWaktuId.setValue(result.data.tipeWaktuId);
                $form.$tipeTanggalId.setValue(result.data.tipeTanggalId);
                $form.$jumlahHari.setValue(result.data.jumlahHari);
                $form.$jumlahJp.setValue(result.data.jumlahJp);
                $form.$elWaktu.hide()
                $form.$elTanggal.hide()

                if (result.data.tipeWaktuId == 1)
                    $form.$elWaktu.show()
                else
                    $form.$elTanggal.show()

                $form.$tipeMataPelajaranId.focus();
            }

            $loading.hide();
        });
    };

    this.newData = function () {
        saveMode = NEW_MODE;
        $form.$elWaktu.show();
        $form.$elTanggal.hide();
        this.clearData();
        $form.$tipeMataPelajaranId.focus();
    };

    this.saveData = function () {
        if ($form.$tipeWaktuId.getValue() == 1) {

            if ($form.$jamMulai.getValue() >= 25) {
                $msgbox.alert('Jam Mulai Kelebihan');
                return;
            }

            if ($form.$jamMulaiMenit.getValue() >= 60) {
                $msgbox.alert('Jam Mulai Menit Kelebihan');
                return;
            }

            if ($form.$jamSelesai.getValue() >= 25) {
                $msgbox.alert('Jam Selesai Kelebihan');
                return;
            }

            if ($form.$jamSelesaiMenit.getValue() >= 60) {
                $msgbox.alert('Jam Selesai Menit Kelebihan');
                return;
            }

            var jmMulai = $form.$jamMulai.getValue();
            if (jmMulai.length == 1) {
                jmMulai = "0" + jmMulai;
            }

            var jmMulaiMenit = $form.$jamMulaiMenit.getValue();
            if (jmMulaiMenit.length == 1) {
                jmMulaiMenit = "0" + jmMulaiMenit;
            }

            var jmSelesai = $form.$jamSelesai.getValue();
            if (jmSelesai.length == 1) {
                jmSelesai = "0" + jmSelesai;
            }

            var jmSelesaiMenit = $form.$jamSelesaiMenit.getValue();
            if (jmSelesaiMenit.length == 1) {
                jmSelesaiMenit = "0" + jmSelesaiMenit;
            }

            var jamMulai = jmMulai + jmMulaiMenit;
            var jamSelesai = jmSelesai + jmSelesaiMenit;

            if (jamMulai > jamSelesai) {
                $msgbox.alert('Jam Selesai lebih kecil dari Jam Mulai');
                return;
            }

        }


        $form.$button.$save.loading();

        switch (saveMode) {
            case EDIT_MODE:
                var url = 'MataPelajaran/updateData';
                break;
            case NEW_MODE:
                var url = 'MataPelajaran/createData';
        }

        var data = this.serializeData();

        api.post(url, data, function (result) {

            switch (result.status) {
                case 'success':
                    $splash.show(result.message);

                    $list.$publishStatusId.setValue(2);
                    this.readData(function () {
                        switch (saveMode) {
                            case EDIT_MODE:
                                $tab.$list.trigger('click');
                                break;
                            case NEW_MODE:
                                this.clearData();
                                $form.$elWaktu.show();
                                $form.$elTanggal.hide();
                                $form.$tipeMataPelajaranId.focus();
                        }

                        $form.$button.$save.release();
                        window.scrollTo(0, 0);
                    }.bind(this));

                    break;
                case 'invalid':
                    $form.error(result);
                    $form.$button.$save.release();
            }

        }.bind(this));
    };

};

dashboard.application.controllers.MataPelajaranFungsional.prototype =
    Object.create(dashboard.application.core.Controller.prototype);

dashboard.application.controllers.PendaftaranDiklat = function () {
    var $el = $page.getScope('PendaftaranDiklat');
    dashboard.application.core.Controller.call(this, $el);

    var rs = [];
    var renderedRs = [];

    var rsRange = config.paging.range;

    var tableHeads = ['NIP', 'NAMA', 'NAMA_DIKLAT', 'Status Upload'];
    var tableFields = ['nip', 'nama', 'namaDiklat', 'statusUpload'];

    var rsBerkasPendaftaran = [];
    var cartHeads = ['Berkas Pendaftaran', 'Status'];
    var cartFields = ['berkasPendaftaran', 'status'];

    var saveMode;

    var $tab;
    var $list;
    var $preview;
    var $form;

    this.init = function () {

        // tab

        $tab = $el.find('#tab');
        $tab.$list = $tab.getRef('list');
        $tab.$preview = $tab.getRef('preview');
        $tab.$form = $tab.getRef('form');
        $tab.$new = $tab.getRef('new');

        // list

        $list = $el.find('#list');
        $list.$jenisDiklatId = $list.getRef('jenisDiklatId').uiComboBox({ placeholder: 'Filter By Jenis Diklat' });
        $list.$namaDiklat = $list.getRef('namaDiklat').uiComboBox({ placeholder: 'Filter By Nama Diklat' });
        $list.$search = $list.getRef('search').uiTextBox({ icon: 'search', placeholder: 'Search...' });
        $list.$table = $list.getRef('table').uiTable({ headers: tableHeads });
        $list.$paging = $list.getRef('paging').uiPaging();

        $list.$button = $list.find('#button');
        $list.$button.$delete = $list.$button.getRef('delete').uiButton();
        $list.$button.$detail = $list.$button.getRef('detail').uiButton();
        $list.$button.$edit = $list.$button.getRef('edit').uiButton();

        $list.$button.$refresh = $list.$button.getRef('refresh').uiButton();
        $list.$button.$excel = $list.$button.getRef('excel').uiButton();

        // preview

        $preview = $el.find('#preview');

        $preview.$jenisDiklat = $preview.getRef('jenisDiklat').uiTextView();
        $preview.$namaDiklat = $preview.getRef('namaDiklat').uiTextAreaView();
        $preview.$nip = $preview.getRef('nip').uiTextView();
        $preview.$nama = $preview.getRef('nama').uiTextView();
        $preview.$nik = $preview.getRef('nik').uiTextView();
        $preview.$npwp = $preview.getRef('npwp').uiTextView();
        $preview.$foto = $preview.getRef('foto').uiImageView();
        $preview.$email = $preview.getRef('email').uiTextView();
        $preview.$pangkatGolongan = $preview.getRef('pangkatGolongan').uiTextView();
        $preview.$jabatanEselon = $preview.getRef('jabatanEselon').uiTextView();
        $preview.$unitKerja = $preview.getRef('unitKerja').uiTextView();
        $preview.$instansi = $preview.getRef('instansi').uiTextView();
        $preview.$instansiLainnya = $preview.getRef('instansiLainnya').uiTextView();
        $preview.$pendidikanTerakhir = $preview.getRef('pendidikanTerakhir').uiTextView();
        $preview.$gender = $preview.getRef('gender').uiTextView();
        $preview.$tempatLahir = $preview.getRef('tempatLahir').uiTextView();
        $preview.$tanggalLahir = $preview.getRef('tanggalLahir').uiTextView();
        $preview.$agama = $preview.getRef('agama').uiTextView();
        $preview.$alamat = $preview.getRef('alamat').uiTextAreaView();
        $preview.$telpHp = $preview.getRef('telpHp').uiTextView();
        $preview.$listBerkasPendaftaran = $preview.getRef('listBerkasPendaftaran').uiTableView(cartHeads);

        $preview.$button = $preview.find('#button');
        $preview.$button.$edit = $preview.$button.getRef('edit').uiButton();
        $preview.$button.$cancel = $preview.$button.getRef('cancel').uiButton();
        $preview.$button.$pdf = $preview.$button.getRef('pdf').uiButton();

        // form

        $form = $el.find('#form').uiForm();

        $form.$jenisDiklatId = $form.getRef('jenisDiklatId').uiComboBox();
        $form.$namaDiklat = $form.getRef('namaDiklat').uiComboBox();
        $form.$nipNama = $form.getRef('nipNama').uiComboBox();
        $form.$nik = $form.getRef('nik').uiTextView();
        $form.$npwp = $form.getRef('npwp').uiTextView();
        $form.$foto = $form.getRef('foto').uiImageView();
        $form.$email = $form.getRef('email').uiTextView();
        $form.$pangkatGolongan = $form.getRef('pangkatGolongan').uiTextView();
        $form.$jabatanEselon = $form.getRef('jabatanEselon').uiTextView();
        $form.$unitKerja = $form.getRef('unitKerja').uiTextView();
        $form.$instansi = $form.getRef('instansi').uiTextView();
        $form.$instansiLainnya = $form.getRef('instansiLainnya').uiTextView();
        $form.$pendidikanTerakhir = $form.getRef('pendidikanTerakhir').uiTextView();
        $form.$gender = $form.getRef('gender').uiTextView();
        $form.$tempatLahir = $form.getRef('tempatLahir').uiTextView();
        $form.$tanggalLahir = $form.getRef('tanggalLahir').uiTextView();
        $form.$agama = $form.getRef('agama').uiTextView();
        $form.$alamat = $form.getRef('alamat').uiTextAreaView();
        $form.$telpHp = $form.getRef('telpHp').uiTextView();


        $form.$button = $form.find('#button');
        $form.$button.$save = $form.$button.getRef('save').uiButton();
        $form.$button.$cancel = $form.$button.getRef('cancel').uiButton();

    };

    this.driveEvents = function () {
        // tab

        $tab.$preview.on('click', this.detailData.bind(this));
        $tab.$form.on('click', this.editData.bind(this));
        $tab.$new.on('click', this.newData.bind(this));

        // list

        $list.$jenisDiklatId.on('change', this.readData.bind(this));
        $list.$namaDiklat.on('change', this.readData.bind(this));
        $list.$search.on('keyup', this.searchData.bind(this));

        $list.$table.on('dblclick', function () {
            if (!$list.$search.getValue()) {
                if (rs.length > 0)
                    $tab.$preview.trigger('click');
            } else {
                if (renderedRs.length > 0)
                    $tab.$preview.trigger('click');
            }
        });

        $list.$paging.on('click', this.moveData.bind(this));
        $list.$button.$delete.on('click', this.deleteData.bind(this));

        $list.$button.$excel.on('click', this.exportListToExcel.bind(this));
        $list.$button.$detail.on('click', function () {
            $tab.$preview.trigger('click');
        });

        $list.$button.$edit.on('click', function () {
            $tab.$form.trigger('click');
        });

        $list.$button.$refresh.on('click', this.loadData.bind(this));

        // preview

        $preview.$button.$edit.on('click', function () {
            $tab.$form.trigger('click');
        });

        $preview.$button.$cancel.on('click', function () {
            $tab.$list.trigger('click');
        });

        $preview.$button.$pdf.on('click', this.exportDetailToPdf.bind(this));

        // form

        $form.$jenisDiklatId.on('change', this.filterNamaDiklat.bind(this));
        $form.$nipNama.on('change', this.lihatAnggota.bind(this));

        $form.$button.$save.on('click', this.saveData.bind(this));

        $form.$button.$cancel.on('click', function () {
            $tab.$list.trigger('click');
        });

    };

    this.driveFieldSequence = function () {

        //        $form.$instansi.on('enter', function () {
        //            $form.$button.$save.trigger('click');
        //        });

    };

    this.reload = function () {
        $tab.$list.trigger('click');
    };

    this.loadData = function () {
        $list.$jenisDiklatId.clear();
        $list.$namaDiklat.clear();
        this.readData();

        $list.$jenisDiklatId.renderApi('JenisDiklat/readData', { orderBy: 'position' }, { text: 'jenisDiklat' });
        //kosongin data
        $list.$namaDiklat.renderApi('Diklat/readData', { jenisDiklatId: 3000, publishStatusId: 5 }, { text: 'namaDiklat' });
        // $list.$namaDiklat.renderApi('Diklat/readData', {text: 'namaDiklat'});
        $form.$jenisDiklatId.renderApi('JenisDiklat/readData', { orderBy: 'position' }, { text: 'jenisDiklat' });
        $form.$nipNama.renderApi('Anggota/readData', { text: 'nipNama' });
    };

    this.filterNamaDiklat = function () {
        var jenisDiklatId = $form.$jenisDiklatId.getValue();
        $form.$namaDiklat.renderApi('Diklat/readData', { jenisDiklatId: jenisDiklatId, publishStatusId: 2 }, { text: 'namaDiklat' });
    };

    this.lihatAnggota = function () {
        $loading.show();

        var url = 'Anggota/detailData';
        var data = {
            id: $form.$nipNama.getValue()
        };

        api.post(url, data, function (result) {

            if (result.status == 'success') {
                $form.$foto.setValue(result.data.foto);
                $form.$email.setValue(result.data.email);
                $form.$nik.setValue(result.data.nik);
                $form.$npwp.setValue(result.data.npwp);
                $form.$pangkatGolongan.setValue(result.data.pangkatGolongan);
                $form.$jabatanEselon.setValue(result.data.jabatanEselon);
                $form.$unitKerja.setValue(result.data.unitKerja);
                $form.$instansi.setValue(result.data.instansi);
                $form.$instansiLainnya.setValue(result.data.instansiLainnya);
                $form.$pendidikanTerakhir.setValue(result.data.pendidikanTerakhir);
                $form.$gender.setValue(result.data.jenisKelamin);
                $form.$tanggalLahir.setValue(result.data.tanggalLahir);
                $form.$tempatLahir.setValue(result.data.tempatLahir);
                $form.$agama.setValue(result.data.agama);
                $form.$alamat.setValue(result.data.alamat);
                $form.$telpHp.setValue(result.data.telpHp);
            }

            $loading.hide();
        });
    };

    this.readData = function (callback) {

        if (!$list.$namaDiklat.getValue() && $list.$jenisDiklatId.getValue()) {
            $list.$namaDiklat.clear();
            var jenisDiklatId = $list.$jenisDiklatId.getValue();
            $list.$namaDiklat.renderApi('Diklat/readData', { jenisDiklatId: jenisDiklatId, publishStatusId: 2 }, { text: 'namaDiklat' });
        }

        if (!callback)
            $loading.show();

        var url = 'PendaftaranDiklat/readData';
        var data = {
            orderBy: 'nip',
            reverse: 1,
            jenisDiklatId: $list.$jenisDiklatId.getValue(),
            namaDiklat: $list.$namaDiklat.getText(),
            diklatId: $list.$namaDiklat.getValue(),
        };

        api.post(url, data, function (result) {

            rs = result.data;
            var rsCount = rs.length;
            $list.$button.$excel.hide();
            if (rsCount > 0) {
                this.dataShow();
            }
            else {
                this.dataHide();
            }


            renderedRs = this.renderTable($list.$table, rs, tableFields, rsRange);
            this.renderPaging($list.$paging, renderedRs, rsRange);

            if ($.isFunction(callback))
                callback();
            else
                $loading.hide();
        }.bind(this));
    };

    this.dataHide = function () {
        $list.$paging.hide();
        $tab.$form.hide();
        $tab.$preview.hide();
        $list.$button.$delete.hide();
        $list.$button.$detail.hide();
        $list.$button.$edit.hide();
        $list.$button.$excel.hide();
    };

    this.dataShow = function () {
        $list.$paging.show();
        $list.$button.$delete.show();
        $list.$button.$detail.show();
        $list.$button.$edit.show();
        $tab.$preview.show();
        $tab.$form.show();
        if ($list.$namaDiklat.getValue() && $list.$jenisDiklatId.getValue())
            $list.$button.$excel.show();
    };

    this.searchData = function () {

        var filteredRs = rs.filter(function (data) {
            var regex = new RegExp($list.$search.getValue(), 'gi');
            var found = false;

            tableFields.forEach(function (field) {
                if (data[field])
                    if (data[field].match(regex))
                        found = true;
            });

            return found;
        });

        renderedRs = this.renderTable($list.$table, filteredRs, tableFields, rsRange);
        if (renderedRs.length > 0) {
            this.renderPaging($list.$paging, renderedRs, rsRange);
            this.dataShow();
        } else {
            this.dataHide();
        }
    };

    this.moveData = function () {
        renderedRs = this.renderTable($list.$table, renderedRs, tableFields, rsRange, $list.$paging.getValue());
    };

    this.deleteData = function () {
        var checkedId = $list.$table.getCheckedValues();

        if (checkedId.length == 0) {
            $msgbox.alert('NO_DATA_CHECKED');
            return;
        };

        var url = 'PendaftaranDiklat/multipleDeleteData';
        var data = {
            multipleId: checkedId,
        };

        $msgbox.confirm('DELETE_CHECKED_DATA', function () {
            api.post(url, data, function (result) {

                switch (result.status) {
                    case 'success':
                        $splash.show(result.message);
                        this.readData();

                        break;
                    case 'failed':
                        $msgbox.alert(result.message);
                }

            }.bind(this));
        }.bind(this));

    };

    this.detailData = function () {
        $loading.show();

        var url = 'PendaftaranDiklat/detailData';
        var data = {
            id: $list.$table.getValue()
        };

        api.post(url, data, function (result) {

            if (result.status == 'success') {
                $preview.$jenisDiklat.setValue(result.data.jenisDiklat);
                $preview.$namaDiklat.setValue(result.data.namaDiklat);
                $preview.$nip.setValue(result.data.nip);
                $preview.$nama.setValue(result.data.nama);
                $preview.$nik.setValue(result.data.nik);
                $preview.$npwp.setValue(result.data.npwp);
                $preview.$foto.setValue(result.data.foto);
                $preview.$email.setValue(result.data.email);
                $preview.$pangkatGolongan.setValue(result.data.pangkatGolongan);
                $preview.$jabatanEselon.setValue(result.data.jabatanEselon);
                $preview.$unitKerja.setValue(result.data.unitKerja);
                $preview.$instansi.setValue(result.data.instansi);
                $preview.$instansiLainnya.setValue(result.data.instansiLainnya);
                $preview.$pendidikanTerakhir.setValue(result.data.pendidikanTerakhir);
                $preview.$gender.setValue(result.data.jenisKelamin);
                $preview.$tanggalLahir.setValue(result.data.tanggalLahir);
                $preview.$tempatLahir.setValue(result.data.tempatLahir);
                $preview.$agama.setValue(result.data.agama);
                $preview.$alamat.setValue(result.data.alamat);
                $preview.$telpHp.setValue(result.data.telpHp);
                rsBerkasPendaftaran = result.data.listBerkasPendaftaran;
                this.renderViewCart();
            }

            $loading.hide();
        }.bind(this));
    };

    this.renderViewCart = function () {
        this.renderTable($preview.$listBerkasPendaftaran, rsBerkasPendaftaran, cartFields, 100);
    };

    this.serializeData = function () {
        var data = {
            diklatId: $form.$namaDiklat.getValue(),
            akunId: $form.$nipNama.getValue(),
        };

        if (saveMode == EDIT_MODE)
            data.id = $list.$table.getValue();

        return data;
    };

    this.clearData = function () {
        $form.clear();
        $form.$nipNama.clear();
        $form.$nik.clear();
        $form.$npwp.clear();
        $form.$jenisDiklatId.clear();
        $form.$namaDiklat.clear();
        $form.$foto.clear();
        $form.$email.clear();
        $form.$pangkatGolongan.clear();
        $form.$jabatanEselon.clear();
        $form.$unitKerja.clear();
        $form.$instansi.clear();
        $form.$instansiLainnya.clear();
        $form.$pendidikanTerakhir.clear();
        $form.$gender.clear();
        $form.$tanggalLahir.clear();
        $form.$tempatLahir.clear();
        $form.$agama.clear();
        $form.$alamat.clear();
        $form.$telpHp.clear();
    };

    this.editData = function () {
        $loading.show();
        saveMode = EDIT_MODE;

        this.clearData();

        var url = 'PendaftaranDiklat/detailData';
        var data = {
            id: $list.$table.getValue()
        };

        api.post(url, data, function (result) {

            if (result.status == 'success') {
                $form.$nipNama.setValue(result.data.akunId);
                $form.$jenisDiklatId.setValue(result.data.jenisDiklatId);
                $form.$namaDiklat.setValue(result.data.diklatId);
                $form.$nik.setValue(result.data.nik);
                $form.$npwp.setValue(result.data.npwp);
                $form.$foto.setValue(result.data.foto);
                $form.$email.setValue(result.data.email);
                $form.$pangkatGolongan.setValue(result.data.pangkatGolongan);
                $form.$jabatanEselon.setValue(result.data.jabatanEselon);
                $form.$unitKerja.setValue(result.data.unitKerja);
                $form.$instansi.setValue(result.data.instansi);
                $form.$instansiLainnya.setValue(result.data.instansiLainnya);
                $form.$pendidikanTerakhir.setValue(result.data.pendidikanTerakhir);
                $form.$gender.setValue(result.data.jenisKelamin);
                $form.$tanggalLahir.setValue(result.data.tanggalLahir);
                $form.$tempatLahir.setValue(result.data.tempatLahir);
                $form.$agama.setValue(result.data.agama);
                $form.$alamat.setValue(result.data.alamat);
                $form.$telpHp.setValue(result.data.telpHp);

                $form.$jenisDiklatId.focus();
            }

            $loading.hide();
        });
    };

    this.newData = function () {
        saveMode = NEW_MODE;

        this.clearData();
        $form.$jenisDiklatId.focus();
    };

    this.saveData = function () {
        $form.$button.$save.loading();

        switch (saveMode) {
            case EDIT_MODE:
                var url = 'PendaftaranDiklat/updateData';
                break;
            case NEW_MODE:
                var url = 'PendaftaranDiklat/createData';
        }

        var data = this.serializeData();

        api.post(url, data, function (result) {

            switch (result.status) {
                case 'success':
                    $splash.show(result.message);

                    this.readData(function () {
                        switch (saveMode) {
                            case EDIT_MODE:
                                $tab.$list.trigger('click');
                                break;
                            case NEW_MODE:
                                this.clearData();
                                $form.$jenisDiklatId.focus();
                        }

                        $form.$button.$save.release();
                        window.scrollTo(0, 0);
                    }.bind(this));

                    break;
                case 'invalid':
                    $form.error(result);
                    $form.$button.$save.release();
            }

        }.bind(this));
    };

    this.exportData = function (type, url, id) {

        $loading.show();

        var data = {};
        if (id)
            data.id = id;

        api.post(url, data, function (result) {

            $loading.hide();

            switch (result.status) {
                case 'success':

                    if (type == 'pdf')
                        router.open(result.data);
                    else
                        router.redirect(result.data);

                    break;
                case 'failed':
                    $msgbox.alert(result.message);
            }
        });
    };

    this.exportListToExcel = function () {
        var type = 'xls';
        var url = 'PendaftaranDiklat/createExcelList';
        var id = $list.$namaDiklat.getValue();

        this.exportData(type, url, id);
    };

    this.exportDetailToPdf = function () {
        var type = 'pdf';
        var url = 'PendaftaranDiklat/createPDFDetail';
        var id = $list.$table.getValue();

        this.exportData(type, url, id);
    };

};

dashboard.application.controllers.PendaftaranDiklat.prototype =
    Object.create(dashboard.application.core.Controller.prototype);

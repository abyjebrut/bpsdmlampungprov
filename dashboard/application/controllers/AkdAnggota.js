dashboard.application.controllers.AkdAnggota = function () {
    var $el = $page.getScope('AkdAnggota');
    dashboard.application.core.Controller.call(this, $el);

    var rs = [];
    var renderedRs = [];

    var rsRange = config.paging.range;

    var tableHeads = ['NIP', 'NAMA', 'EMAIL'];
    var tableFields = ['nip', 'nama', 'email'];

    var saveMode;
    var lockShowPrivileges = false;

    var $tab;
    var $list;
    var $preview;
    var $form;
    var $change_password;

    this.init = function () {

        // tab

        $tab = $el.find('#tab');
        $tab.$list = $tab.getRef('list');
        $tab.$preview = $tab.getRef('preview');
        $tab.$form = $tab.getRef('form');
        $tab.$new = $tab.getRef('new');
        $tab.$change_password = $tab.getRef('change_password');

        // list

        $list = $el.find('#list');
        $list.$search = $list.getRef('search').uiTextBox({ icon: 'search', placeholder: 'Search...' });
        $list.$table = $list.getRef('table').uiTable({ headers: tableHeads });
        $list.$paging = $list.getRef('paging').uiPaging();

        $list.$button = $list.find('#button');
        $list.$button.$delete = $list.$button.getRef('delete').uiButton();
        $list.$button.$detail = $list.$button.getRef('detail').uiButton();

        $list.$button.$refresh = $list.$button.getRef('refresh').uiButton();
        $list.$button.$edit = $list.$button.getRef('edit').uiButton();
        $list.$button.$sendEmail = $list.$button.getRef('sendEmail').uiButton();

        // preview

        $preview = $el.find('#preview');
        $preview.$nip = $preview.getRef('nip').uiTextView();
        $preview.$nama = $preview.getRef('nama').uiTextView();
        $preview.$email = $preview.getRef('email').uiTextView();


        $preview.$button = $preview.find('#button');
        $preview.$button.$edit = $preview.$button.getRef('edit').uiButton();
        $preview.$button.$cancel = $preview.$button.getRef('cancel').uiButton();

        // form

        $form = $el.find('#form').uiForm();
        $form.$nip = $form.getRef('nip').uiTextBox();
        $form.$nama = $form.getRef('nama').uiTextBox();
        $form.$email = $form.getRef('email').uiTextBox();

        $form.$button = $form.find('#button');
        $form.$button.$save = $form.$button.getRef('save').uiButton();
        $form.$button.$cancel = $form.$button.getRef('cancel').uiButton();

        // change_password

        $change_password = $el.find('#change_password').uiForm();
        $change_password.$name = $change_password.getRef('name').uiTextView();
        $change_password.$email = $change_password.getRef('email').uiTextView();
        $change_password.$new_password = $change_password.getRef('new_password').uiTextBox({ type: TYPE_PASSWORD });
        $change_password.$retype_password = $change_password.getRef('retype_password').uiTextBox({ type: TYPE_PASSWORD });

        $change_password.$button = $change_password.find('#button');
        $change_password.$button.$save = $change_password.$button.getRef('save').uiButton();
        $change_password.$button.$cancel = $change_password.$button.getRef('cancel').uiButton();

    };

    this.driveEvents = function () {
        // tab

        $tab.$preview.on('click', this.detailData.bind(this));
        $tab.$form.on('click', this.editData.bind(this));
        $tab.$new.on('click', this.newData.bind(this));
        $tab.$change_password.on('click', this.detailPassword.bind(this));

        // list

        $list.$search.on('keyup', this.searchData.bind(this));

        $list.$table.on('dblclick', function () {
            $tab.$preview.trigger('click');
        });

        $list.$paging.on('click', this.moveData.bind(this));
        $list.$button.$delete.on('click', this.deleteData.bind(this));

        $list.$button.$detail.on('click', function () {
            $tab.$preview.trigger('click');
        });

        $list.$button.$edit.on('click', function () {
            $tab.$form.trigger('click');
        });

        $list.$button.$refresh.on('click', this.readData.bind(this));

        $list.$button.$sendEmail.on('click', this.sendEmail.bind(this));
        // preview

        $preview.$button.$edit.on('click', function () {
            $tab.$form.trigger('click');
        });

        $preview.$button.$cancel.on('click', function () {
            $tab.$list.trigger('click');
        });

        $form.$button.$save.on('click', this.saveData.bind(this));
        $form.$button.$cancel.on('click', function () {
            $tab.$list.trigger('click');
        });

        // change_password

        $change_password.$button.$save.on('click', this.savePassword.bind(this));
        $change_password.$button.$cancel.on('click', function () {
            $tab.$list.trigger('click');
        });

    };

    this.driveFieldSequence = function () {

    };

    this.reload = function () {
        $tab.$list.trigger('click');
    };

    this.readData = function (callback) {

        if (!callback)
            $loading.show();

        var url = 'Anggota/readDataAkd';
        var data = {
            orderBy: 'id',
            reverse: 1,
        };

        api.post(url, data, function (result) {

            rs = result.data;
            var rsCount = rs.length;

            if (rsCount > 0) {
                this.dataShow();
            }
            else {
                this.dataHide();
            }

            renderedRs = this.renderTable($list.$table, rs, tableFields, rsRange);
            this.renderPaging($list.$paging, renderedRs, rsRange);

            if ($.isFunction(callback))
                callback();
            else
                $loading.hide();
        }.bind(this));
    };

    this.dataHide = function () {
        $list.$paging.hide();
        $tab.$form.hide();
        $tab.$preview.hide();
        $list.$button.$delete.hide();
        $list.$button.$detail.hide();
        $list.$button.$edit.hide();
        $tab.$change_password.hide();
        $list.$button.$sendEmail.hide();
    };

    this.dataShow = function () {
        $list.$paging.show();
        $tab.$form.show();
        $tab.$preview.show();
        $tab.$change_password.show();
        $list.$button.$delete.show();
        $list.$button.$detail.show();
        $list.$button.$edit.show();
        $list.$button.$sendEmail.show();
    };

    this.searchData = function () {

        var filteredRs = rs.filter(function (data) {
            var regex = new RegExp($list.$search.getValue(), 'gi');
            var found = false;

            tableFields.forEach(function (field) {
                if (data[field])
                    if (data[field].match(regex))
                        found = true;
            });

            return found;
        });

        renderedRs = this.renderTable($list.$table, filteredRs, tableFields, rsRange);
        if (renderedRs.length > 0) {
            this.renderPaging($list.$paging, renderedRs, rsRange);
            this.dataShow();
        } else {
            this.dataHide();
        }
    };

    this.moveData = function () {
        renderedRs = this.renderTable($list.$table, renderedRs, tableFields, rsRange, $list.$paging.getValue());
    };

    this.deleteData = function () {
        var checkedId = $list.$table.getCheckedValues();

        if (checkedId.length == 0) {
            $msgbox.alert('NO_DATA_CHECKED');
            return;
        };

        var url = 'Anggota/multipleDeleteData';
        var data = {
            multipleId: checkedId,
        };

        $msgbox.confirm('DELETE_CHECKED_DATA', function () {
            api.post(url, data, function (result) {

                switch (result.status) {
                    case 'success':
                        $splash.show(result.message);
                        this.readData();

                        break;
                    case 'failed':
                        $msgbox.alert(result.message);
                }

            }.bind(this));
        }.bind(this));

    };

    this.detailData = function () {
        $loading.show();

        var url = 'Anggota/detailData';
        var data = {
            id: $list.$table.getValue()
        };

        api.post(url, data, function (result) {

            if (result.status == 'success') {
                $preview.$nip.setValue(result.data.nip);
                $preview.$nama.setValue(result.data.nama);
                $preview.$email.setValue(result.data.email);
            }

            $loading.hide();
        });
    };

    this.sendEmail = function () {

        var checkedId = $list.$table.getCheckedValues();

        if (checkedId.length > 1) {
            $msgbox.alert('CANNOT_PROCESS_MULTIPLE_SELECTION');
            return;
        };

        $msgbox.confirm('CONFRIM_RESET_PASSWORD', function () {
            $loading.show();

            var url = 'Anggota/sendEmailAkd';
            var data = {
                id: $list.$table.getValue()
            };

            api.post(url, data, function (result) {

                if (result.status == 'success') {
                    $splash.show(result.message);
                }

                $loading.hide();
            });
        }.bind(this));
    };

    this.detailPassword = function () {
        $loading.show();

        $change_password.clear();
        $change_password.$new_password.clear();
        $change_password.$retype_password.clear();

        var url = 'Anggota/detailData';
        var data = {
            id: $list.$table.getValue()
        };

        api.post(url, data, function (result) {

            if (result.status == 'success') {
                $change_password.$name.setValue(result.data.nama);
                $change_password.$email.setValue(result.data.email);
            }

            $loading.hide();
        });
    };

    this.serializeData = function () {
        var data = {
            nip: $form.$nip.getValue(),
            nama: $form.$nama.getValue(),
            email: $form.$email.getValue(),
        };

        if (saveMode == EDIT_MODE)
            data.id = $list.$table.getValue();

        return data;
    };

    this.clearData = function () {
        $form.clear();

        $form.$nip.clear();
        $form.$nama.clear();
        $form.$email.clear();
    };

    this.editData = function () {
        $loading.show();
        saveMode = EDIT_MODE;

        this.clearData();

        var url = 'Anggota/detailData';
        var data = {
            id: $list.$table.getValue()
        };

        api.post(url, data, function (result) {

            if (result.status == 'success') {
                $form.$nip.setValue(result.data.nip);
                $form.$nama.setValue(result.data.nama);
                $form.$email.setValue(result.data.email);

                $form.$nip.focus();
            }

            $loading.hide();
        });
    };

    this.newData = function () {
        saveMode = NEW_MODE;

        this.clearData();
        $form.$nip.focus();
    };

    this.saveData = function () {
        $form.$button.$save.loading();

        switch (saveMode) {
            case EDIT_MODE:
                var url = 'Anggota/updateDataDashboardAkd';
                break;
            case NEW_MODE:
                var url = 'Anggota/createDataAkd';
        }

        var data = this.serializeData();

        api.post(url, data, function (result) {

            switch (result.status) {
                case 'success':
                    $splash.show(result.message);

                    this.readData(function () {
                        switch (saveMode) {
                            case EDIT_MODE:
                                $tab.$list.trigger('click');
                                break;
                            case NEW_MODE:
                                this.clearData();
                                $form.$nip.focus();
                        }

                        $form.$button.$save.release();
                        window.scrollTo(0, 0);
                    }.bind(this));

                    break;
                case 'invalid':
                    $form.error(result);
                    $form.$button.$save.release();
            }

        }.bind(this));
    };

    this.savePassword = function () {
        $change_password.$button.$save.loading();

        var url = 'Anggota/updatePassword';
        var data = {
            id: $list.$table.getValue(),
            new_password: $change_password.$new_password.getValue(),
            retype_password: $change_password.$retype_password.getValue(),
        };

        api.post(url, data, function (result) {

            switch (result.status) {
                case 'success':
                    $splash.show(result.message);

                    this.readData(function () {

                        $change_password.$new_password.clear();
                        $change_password.$retype_password.clear();

                        $change_password.$button.$save.release();
                    }.bind(this));

                    break;
                case 'invalid':
                    $change_password.error(result);
                    $change_password.$new_password.focus();

                    $change_password.$button.$save.release();
            }

        }.bind(this));
    };



};

dashboard.application.controllers.AkdAnggota.prototype =
    Object.create(dashboard.application.core.Controller.prototype);

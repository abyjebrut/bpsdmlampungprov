dashboard.application.controllers.ApproveAnggota = function () {
    var $el = $page.getScope('ApproveAnggota');
    dashboard.application.core.Controller.call(this, $el);

    var rs = [];
    var renderedRs = [];

    var rsRange = config.paging.range;

    var tableHeads = ['NIP', 'NAMA', 'EMAIL', 'STATUS_AKUN'];
    var tableFields = ['nip', 'nama', 'email', 'statusAkun'];

    var saveMode;

    var $tab;
    var $list;
    var $preview;

    this.init = function () {

        // tab

        $tab = $el.find('#tab');
        $tab.$list = $tab.getRef('list');
        $tab.$preview = $tab.getRef('preview');

        // list

        $list = $el.find('#list');
        $list.$unitKerjaId = $list.getRef('unitKerjaId').uiComboBox({ placeholder: 'Filter By Unit Kerja' });
        $list.$instansiId = $list.getRef('instansiId').uiComboBox({ placeholder: 'Filter By Instansi' });
        $list.$statusAkunId = $list.getRef('statusAkunId').uiComboBox({ placeholder: 'Filter By Status Akun' });
        $list.$search = $list.getRef('search').uiTextBox({ icon: 'search', placeholder: 'Search...' });
        $list.$table = $list.getRef('table').uiTable({ headers: tableHeads });
        $list.$paging = $list.getRef('paging').uiPaging();

        $list.$button = $list.find('#button');
        $list.$button.$detail = $list.$button.getRef('detail').uiButton();
        $list.$button.$refresh = $list.$button.getRef('refresh').uiButton();

        // preview

        $preview = $el.find('#preview');
        $preview.$nip = $preview.getRef('nip').uiTextView();
        $preview.$nama = $preview.getRef('nama').uiTextView();
        $preview.$foto = $preview.getRef('foto').uiImageView();
        $preview.$email = $preview.getRef('email').uiTextView();


        $preview.$button = $preview.find('#button');
        $preview.$button.$approve = $preview.$button.getRef('approve').uiButton();
        $preview.$button.$cancel = $preview.$button.getRef('cancel').uiButton();
        $preview.$button.$pdf = $preview.$button.getRef('pdf').uiButton();

    };

    this.driveEvents = function () {
        // tab

        $tab.$preview.on('click', this.detailData.bind(this));

        // list

        $list.$unitKerjaId.on('change', this.readData.bind(this));
        $list.$instansiId.on('change', this.readData.bind(this));
        $list.$statusAkunId.on('change', this.readData.bind(this));
        $list.$search.on('keyup', this.searchData.bind(this));

        $list.$table.on('dblclick', function () {
            if (!$list.$search.getValue()) {
                if (rs.length > 0)
                    $tab.$preview.trigger('click');
            } else {
                if (renderedRs.length > 0)
                    $tab.$preview.trigger('click');
            }
        });

        $list.$paging.on('click', this.moveData.bind(this));

        $list.$button.$detail.on('click', function () {
            $tab.$preview.trigger('click');
        });

        $list.$button.$refresh.on('click', this.loadData.bind(this));

        // preview

        $preview.$button.$approve.on('click', this.approveData.bind(this));

        $preview.$button.$cancel.on('click', function () {
            $tab.$list.trigger('click');
        });

        $preview.$button.$pdf.on('click', this.exportDetailToPdf.bind(this));

    };

    this.driveFieldSequence = function () {

    };

    this.reload = function () {
        $tab.$list.trigger('click');

    };

    this.loadData = function () {
        $tab.$list.trigger('click');
        this.readData();

        $list.$statusAkunId.renderApi('StatusAkun/readData', { text: 'statusAkun' });
        $list.$unitKerjaId.renderApi('UnitKerja/readData', { text: 'unitKerja' });
        $list.$instansiId.renderApi('Instansi/readData', { text: 'instansi' });

    };


    this.readData = function (callback) {

        if (!callback)
            $loading.show();

        var url = 'Anggota/readDataPending';
        var data = {
            orderBy: '_registeredOn',
            reverse: 1,
            instansiId: $list.$instansiId.getValue(),
            unitKerjaId: $list.$unitKerjaId.getValue(),
            statusAkunId: $list.$statusAkunId.getValue(),
        };

        api.post(url, data, function (result) {

            rs = result.data;
            var rsCount = rs.length;

            if (rsCount > 0) {
                this.dataShow();
            }
            else {
                this.dataHide();
            }

            renderedRs = this.renderTable($list.$table, rs, tableFields, rsRange);
            this.renderPaging($list.$paging, renderedRs, rsRange);

            if ($.isFunction(callback))
                callback();
            else
                $loading.hide();
        }.bind(this));
    };

    this.dataHide = function () {
        $list.$paging.hide();
        $tab.$preview.hide();
        $list.$button.$detail.hide();
    };

    this.dataShow = function () {
        $list.$paging.show();
        $tab.$preview.show();
        $list.$button.$detail.show();
    };

    this.searchData = function () {

        var filteredRs = rs.filter(function (data) {
            var regex = new RegExp($list.$search.getValue(), 'gi');
            var found = false;

            tableFields.forEach(function (field) {
                if (data[field])
                    if (data[field].match(regex))
                        found = true;
            });

            return found;
        });

        renderedRs = this.renderTable($list.$table, filteredRs, tableFields, rsRange);
        if (renderedRs.length > 0) {
            this.renderPaging($list.$paging, renderedRs, rsRange);
            this.dataShow();
        } else {
            this.dataHide();
        }
    };

    this.moveData = function () {
        renderedRs = this.renderTable($list.$table, renderedRs, tableFields, rsRange, $list.$paging.getValue());
    };

    this.detailData = function () {
        $loading.show();

        var url = 'Anggota/detailData';
        var data = {
            id: $list.$table.getValue()
        };

        api.post(url, data, function (result) {

            if (result.status == 'success') {
                $preview.$nip.setValue(result.data.nip);
                $preview.$nama.setValue(result.data.nama);
                $preview.$foto.setValue(result.data.foto);
                $preview.$email.setValue(result.data.email);
            }

            $loading.hide();
        });
    };

    this.approveData = function () {
        $loading.show();

        var url = 'Anggota/updateDataPending';
        var data = {
            id: $list.$table.getValue()
        };

        api.post(url, data, function (result) {

            if (result.status == 'success') {
                $splash.show(result.message);
                this.readData(function () {
                    $tab.$list.trigger('click');

                }.bind(this));
            }

            $loading.hide();
        }.bind(this));
    };

    this.exportData = function (type, url, id) {

        $loading.show();

        var data = {};
        if (id)
            data.id = id;

        api.post(url, data, function (result) {

            $loading.hide();

            switch (result.status) {
                case 'success':

                    if (type == 'pdf')
                        router.open(result.data);
                    else
                        router.redirect(result.data);

                    break;
                case 'failed':
                    $msgbox.alert(result.message);
            }
        });
    };

    this.exportDetailToPdf = function () {
        var type = 'pdf';
        var url = 'Anggota/createPDFDetail';
        var id = $list.$table.getValue();

        this.exportData(type, url, id);
    };


};

dashboard.application.controllers.ApproveAnggota.prototype =
    Object.create(dashboard.application.core.Controller.prototype);

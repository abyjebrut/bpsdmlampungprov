dashboard.application.controllers.TanggalMerah = function () {
    var $el = $page.getScope('TanggalMerah');
    dashboard.application.core.Controller.call(this, $el);

    var rs = [];
    var renderedRs = [];

    var rsRange = config.paging.range;

    var tableHeads = ['Tanggal Merah', 'Tanggal Mulai', 'Tanggal Selesai'];
    var tableFields = ['tanggalMerah', 'tanggalMulai', 'tanggalSelesai'];

    var saveMode;

    var $tab;
    var $list;
    var $preview;
    var $form;

    this.init = function () {

        // tab

        $tab = $el.find('#tab');
        $tab.$list = $tab.getRef('list');
        $tab.$preview = $tab.getRef('preview');
        $tab.$form = $tab.getRef('form');
        $tab.$new = $tab.getRef('new');

        // list

        $list = $el.find('#list');
        $list.$tahun = $list.getRef('tahun').uiComboBox({ placeholder: 'Filter By Tahun' });
        $list.$search = $list.getRef('search').uiTextBox({ icon: 'search', placeholder: 'Search...' });
        $list.$table = $list.getRef('table').uiTable({ headers: tableHeads });
        $list.$paging = $list.getRef('paging').uiPaging();

        $list.$button = $list.find('#button');
        $list.$button.$delete = $list.$button.getRef('delete').uiButton();
        $list.$button.$detail = $list.$button.getRef('detail').uiButton();
        $list.$button.$edit = $list.$button.getRef('edit').uiButton();

        $list.$button.$refresh = $list.$button.getRef('refresh').uiButton();


        // preview

        $preview = $el.find('#preview');

        $preview.$tanggalMerah = $preview.getRef('tanggalMerah').uiTextView();
        $preview.$tanggalMulai = $preview.getRef('tanggalMulai').uiTextView();
        $preview.$tanggalSelesai = $preview.getRef('tanggalSelesai').uiTextView();

        $preview.$button = $preview.find('#button');
        $preview.$button.$edit = $preview.$button.getRef('edit').uiButton();
        $preview.$button.$cancel = $preview.$button.getRef('cancel').uiButton();

        // form

        $form = $el.find('#form').uiForm();

        $form.$tanggalMerah = $form.getRef('tanggalMerah').uiTextBox();
        $form.$tanggalMulai = $form.getRef('tanggalMulai').uiDatePicker();
        $form.$tanggalSelesai = $form.getRef('tanggalSelesai').uiDatePicker();

        $form.$button = $form.find('#button');
        $form.$button.$save = $form.$button.getRef('save').uiButton();
        $form.$button.$cancel = $form.$button.getRef('cancel').uiButton();

    };

    this.driveEvents = function () {
        // tab

        $tab.$preview.on('click', this.detailData.bind(this));
        $tab.$form.on('click', this.editData.bind(this));
        $tab.$new.on('click', this.newData.bind(this));

        // list

        $list.$tahun.on('change', this.readData.bind(this));
        $list.$search.on('keyup', this.searchData.bind(this));

        $list.$table.on('dblclick', function () {
            if (!$list.$search.getValue()) {
                if (rs.length > 0)
                    $tab.$preview.trigger('click');
            } else {
                if (renderedRs.length > 0)
                    $tab.$preview.trigger('click');
            }
        });

        $list.$paging.on('click', this.moveData.bind(this));
        $list.$button.$delete.on('click', this.deleteData.bind(this));

        $list.$button.$detail.on('click', function () {
            $tab.$preview.trigger('click');
        });

        $list.$button.$edit.on('click', function () {
            $tab.$form.trigger('click');
        });

        $list.$button.$refresh.on('click', this.loadData.bind(this));

        // preview

        $preview.$button.$edit.on('click', function () {
            $tab.$form.trigger('click');
        });

        $preview.$button.$cancel.on('click', function () {
            $tab.$list.trigger('click');
        });

        // form
        // $form.$tanggalMulai.on('change', this.showTanggalSelesai.bind(this));
        $form.$button.$save.on('click', this.saveData.bind(this));

        $form.$button.$cancel.on('click', function () {
            $tab.$list.trigger('click');
        });

    };

    this.driveFieldSequence = function () {

        $form.$tanggalMerah.on('enter', function () {
            $form.$button.$save.trigger('click');
        });

    };


    this.reload = function () {
        $tab.$list.trigger('click');
    };

    this.loadData = function () {
        $tab.$list.trigger('click');
        var rsTahun = [];
        for (let i = 2018; i < 2040; i++) {
            rsTahun.push({
                value: i,
                text: i,
            });
        }
        $list.$tahun.render(rsTahun);
        var tgl1 = new Date()
        $list.$tahun.setValue(tgl1.getFullYear());
        this.readData();
    };

    this.showTanggalSelesai = function () {
        $form.$tanggalSelesai.setValue($form.$tanggalMulai.getValue())
    };

    this.readData = function (callback) {

        if (!callback)
            $loading.show();

        var url = 'TanggalMerah/readData';
        var data = {
            orderBy: '_tanggalMulai',
            reverse: 2,
            tahun: $list.$tahun.getValue(),
        };

        api.post(url, data, function (result) {

            rs = result.data;
            var rsCount = rs.length;

            if (rsCount > 0) {
                this.dataShow();
            }
            else {
                this.dataHide();
            }

            renderedRs = this.renderTable($list.$table, rs, tableFields, rsRange);
            this.renderPaging($list.$paging, renderedRs, rsRange);

            if ($.isFunction(callback))
                callback();
            else
                $loading.hide();
        }.bind(this));
    };

    this.dataHide = function () {
        $list.$paging.hide();
        $tab.$form.hide();
        $tab.$preview.hide();
        $list.$button.$delete.hide();
        $list.$button.$detail.hide();
        $list.$button.$edit.hide();
    };

    this.dataShow = function () {
        $list.$paging.show();
        $tab.$form.show();
        $tab.$preview.show();
        $list.$button.$delete.show();
        $list.$button.$detail.show();
        $list.$button.$edit.show();
    };

    this.searchData = function () {

        var filteredRs = rs.filter(function (data) {
            var regex = new RegExp($list.$search.getValue(), 'gi');
            var found = false;

            tableFields.forEach(function (field) {
                if (data[field])
                    if (data[field].match(regex))
                        found = true;
            });

            return found;
        });

        renderedRs = this.renderTable($list.$table, filteredRs, tableFields, rsRange);
        if (renderedRs.length > 0) {
            this.renderPaging($list.$paging, renderedRs, rsRange);
            this.dataShow();
        } else {
            this.dataHide();
        }
    };

    this.moveData = function () {
        renderedRs = this.renderTable($list.$table, renderedRs, tableFields, rsRange, $list.$paging.getValue());
    };

    this.deleteData = function () {
        var checkedId = $list.$table.getCheckedValues();

        if (checkedId.length == 0) {
            $msgbox.alert('NO_DATA_CHECKED');
            return;
        };

        var url = 'TanggalMerah/multipleDeleteData';
        var data = {
            multipleId: checkedId,
        };

        $msgbox.confirm('DELETE_CHECKED_DATA', function () {
            api.post(url, data, function (result) {

                switch (result.status) {
                    case 'success':
                        $splash.show(result.message);
                        this.readData();

                        break;
                    case 'failed':
                        var htmlError = '';

                        $.each(result.data, function (key, value) {
                            htmlError += value + '<br>';
                        });
                        $msgbox.alert(htmlError);
                        this.readData();
                }

            }.bind(this));
        }.bind(this));

    };

    this.detailData = function () {
        $loading.show();

        var url = 'TanggalMerah/detailData';
        var data = {
            id: $list.$table.getValue()
        };

        api.post(url, data, function (result) {

            if (result.status == 'success') {
                $preview.$tanggalMerah.setValue(result.data.tanggalMerah);
                $preview.$tanggalMulai.setValue(result.data.tanggalMulai);
                $preview.$tanggalSelesai.setValue(result.data.tanggalSelesai);

            }

            $loading.hide();
        });
    };

    this.serializeData = function () {
        var data = {
            tanggalMerah: $form.$tanggalMerah.getValue(),
            // tanggalMulai: $form.$tanggalMulai.getValue(),
            // tanggalSelesai: $form.$tanggalSelesai.getValue(),
        };

        if (saveMode == EDIT_MODE)
            data.id = $list.$table.getValue();

        return data;
    };

    this.clearData = function () {
        $form.clear();

        $form.$tanggalMerah.clear();
        let tgl1 = new Date()
        let tgl = tgl1.getFullYear() + '-' + (tgl1.getMonth() + 1) + '-' + tgl1.getDate()
        $form.$tanggalMulai.setValue(tgl);
        $form.$tanggalSelesai.setValue(tgl);
    };

    this.editData = function () {
        $loading.show();
        saveMode = EDIT_MODE;

        this.clearData();

        var url = 'TanggalMerah/detailData';
        var data = {
            id: $list.$table.getValue()
        };

        api.post(url, data, function (result) {

            if (result.status == 'success') {
                $form.$tanggalMerah.setValue(result.data.tanggalMerah);
                $form.$tanggalMulai.setValue(result.data._tanggalMulai);
                $form.$tanggalSelesai.setValue(result.data._tanggalSelesai);

                $form.$tanggalMerah.focus();
            }

            $loading.hide();
        });
    };

    this.newData = function () {
        saveMode = NEW_MODE;

        this.clearData();
        $form.$tanggalMerah.focus();
    };

    this.saveData = function () {


        var d = $form.$tanggalMulai.getValue();
        var splitD = d.split('-');
        var curr_date = splitD[2];
        if (curr_date.length == 1) {
            curr_date = "0" + curr_date;
        }
        var curr_month = splitD[1];
        if (curr_month.length == 1) {
            curr_month = "0" + curr_month;
        }
        var curr_year = splitD[0];

        $tanggalMulai = curr_year + curr_month + curr_date;

        var lTanggalMulai = curr_year + '-' + curr_month + '-' + curr_date;

        var str = $form.$tanggalSelesai.getValue();
        var splitStr = str.split('-');
        var tahunPengujian = splitStr[0];
        var bulanPengujian = splitStr[1];
        if (bulanPengujian.length == 1) {
            bulanPengujian = "0" + bulanPengujian;
        }
        var hariPengujian = splitStr[2];
        if (hariPengujian.length == 1) {
            hariPengujian = "0" + hariPengujian;
        }
        var tanggalSelesai = tahunPengujian + bulanPengujian + hariPengujian;
        var lTanggalSelesai = tahunPengujian + '-' + bulanPengujian + '-' + hariPengujian;
        if (parseInt($tanggalMulai) > parseInt(tanggalSelesai)) {
            $msgbox.alert('Tanggal Selesai lebih kecil dari Tanggal Mulai');
            return;
        }

        $form.$button.$save.loading();

        switch (saveMode) {
            case EDIT_MODE:
                var url = 'TanggalMerah/updateData';
                break;
            case NEW_MODE:
                var url = 'TanggalMerah/createData';
        }

        var data = this.serializeData();
        data.tanggalSelesai = lTanggalSelesai;
        data.tanggalMulai = lTanggalMulai;

        api.post(url, data, function (result) {

            switch (result.status) {
                case 'success':
                    $splash.show(result.message);

                    this.readData(function () {
                        switch (saveMode) {
                            case EDIT_MODE:
                                $tab.$list.trigger('click');
                                break;
                            case NEW_MODE:
                                this.clearData();
                                $form.$tanggalMerah.focus();
                        }

                        $form.$button.$save.release();
                        window.scrollTo(0, 0);
                    }.bind(this));

                    break;
                case 'invalid':
                    $form.error(result);
                    $form.$button.$save.release();
            }

        }.bind(this));
    };

};

dashboard.application.controllers.TanggalMerah.prototype =
    Object.create(dashboard.application.core.Controller.prototype);

dashboard.application.controllers.Pages = function () {
    var $el = $page.getScope('Pages');
    dashboard.application.core.Controller.call(this, $el);

    var rs = [];
    var renderedRs = [];

    var rsRange = config.paging.range;

    var tableHeads = ['TITLE', 'MEDIA_TYPE', 'PUBLISH_STATUS'];
    var tableFields = ['title', 'mediaType', 'publishStatus'];

    var saveMode;

    var $tab;
    var $list;
    var $preview;
    var $form;

    this.init = function () {

        // tab

        $tab = $el.find('#tab');
        $tab.$list = $tab.getRef('list');
        $tab.$preview = $tab.getRef('preview');
        $tab.$form = $tab.getRef('form');
        $tab.$new = $tab.getRef('new');

        // list

        $list = $el.find('#list');
        $list.$publishStatusId = $list.getRef('publishStatusId').uiRadioBox();
        $list.$search = $list.getRef('search').uiTextBox({ icon: 'search', placeholder: 'Search...' });
        $list.$table = $list.getRef('table').uiTable({ headers: tableHeads });
        $list.$paging = $list.getRef('paging').uiPaging();

        $list.$button = $list.find('#button');
        $list.$button.$delete = $list.$button.getRef('delete').uiButton();
        $list.$button.$detail = $list.$button.getRef('detail').uiButton();
        $list.$button.$edit = $list.$button.getRef('edit').uiButton();

        $list.$button.$up = $list.$button.getRef('up').uiButton();
        $list.$button.$down = $list.$button.getRef('down').uiButton();
        $list.$button.$refresh = $list.$button.getRef('refresh').uiButton();

        // preview

        $preview = $el.find('#preview');

        $preview.$contentContainer = $preview.getRef('contentContainer');
        $preview.$contentContainer.hide();
        $preview.$imageContainer = $preview.getRef('imageContainer');
        $preview.$imageContainer.hide();
        $preview.$videoContainer = $preview.getRef('videoContainer');
        $preview.$videoContainer.hide();

        $preview.$slContainer = $preview.getRef('slContainer');
        $preview.$slContainer.hide();
        $preview.$elContainer = $preview.getRef('elContainer');
        $preview.$elContainer.hide();
        $preview.$labelSl = $preview.getRef('labelSl');
        $preview.$labelEl = $preview.getRef('labelEl');

        $preview.$publishStatus = $preview.getRef('publishStatus').uiTextView();
        $preview.$title = $preview.getRef('title').uiTextView();
        $preview.$titleSl = $preview.getRef('titleSl').uiTextView();
        $preview.$titleEl = $preview.getRef('titleEl').uiTextView();
        $preview.$mediaType = $preview.getRef('mediaType').uiTextView();
        $preview.$contentLayout = $preview.getRef('contentLayout').uiTextView();
        $preview.$image = $preview.getRef('image').uiImageView();
        $preview.$videoEmbed = $preview.getRef('videoEmbed').uiTextAreaView();
        $preview.$description = $preview.getRef('description').uiHTMLView();
        $preview.$descriptionSl = $preview.getRef('descriptionSl').uiHTMLView();
        $preview.$descriptionEl = $preview.getRef('descriptionEl').uiHTMLView();
        $preview.$moreDescription = $preview.getRef('moreDescription').uiHTMLView();
        $preview.$moreDescriptionSl = $preview.getRef('moreDescriptionSl').uiHTMLView();
        $preview.$moreDescriptionEl = $preview.getRef('moreDescriptionEl').uiHTMLView();

        $preview.$button = $preview.find('#button');
        $preview.$button.$edit = $preview.$button.getRef('edit').uiButton();
        $preview.$button.$cancel = $preview.$button.getRef('cancel').uiButton();

        // form

        $form = $el.find('#form').uiForm();

        $form.$contentContainer = $form.getRef('contentContainer');
        $form.$imageContainer = $form.getRef('imageContainer');
        $form.$videoContainer = $form.getRef('videoContainer');

        $form.$slContainer = $form.getRef('slContainer');
        $form.$slContainer.hide();
        $form.$elContainer = $form.getRef('elContainer');
        $form.$elContainer.hide();
        $form.$labelSl = $form.getRef('labelSl');
        $form.$labelEl = $form.getRef('labelEl');

        $form.$publishStatusId = $form.getRef('publishStatusId').uiRadioBox();
        $form.$languageCode = $form.getRef('languageCode').uiLangBox();
        $form.$title = $form.getRef('title').uiTextBox();
        $form.$titleSl = $form.getRef('titleSl').uiTextBox();
        $form.$titleEl = $form.getRef('titleEl').uiTextBox();
        $form.$mediaTypeId = $form.getRef('mediaTypeId').uiRadioBox();
        $form.$contentLayoutId = $form.getRef('contentLayoutId').uiRadioBox();
        $form.$image = $form.getRef('image').uiUploadImage();
        $form.$videoEmbed = $form.getRef('videoEmbed').uiTextArea();
        $form.$description = $form.getRef('description').uiRichText();
        $form.$descriptionSl = $form.getRef('descriptionSl').uiRichText();
        $form.$descriptionEl = $form.getRef('descriptionEl').uiRichText();
        $form.$moreDescription = $form.getRef('moreDescription').uiRichText();
        $form.$moreDescriptionSl = $form.getRef('moreDescriptionSl').uiRichText();
        $form.$moreDescriptionEl = $form.getRef('moreDescriptionEl').uiRichText();

        $form.$button = $form.find('#button');
        $form.$button.$save = $form.$button.getRef('save').uiButton();
        $form.$button.$cancel = $form.$button.getRef('cancel').uiButton();

    };

    this.driveEvents = function () {
        // tab

        $tab.$preview.on('click', this.detailData.bind(this));
        $tab.$form.on('click', this.editData.bind(this));
        $tab.$new.on('click', this.newData.bind(this));

        // list

        $list.$publishStatusId.on('click', this.readData.bind(this));
        $list.$search.on('keyup', this.searchData.bind(this));

        $list.$table.on('dblclick', function () {
            $tab.$preview.trigger('click');
        });

        $list.$paging.on('click', this.moveData.bind(this));
        $list.$button.$delete.on('click', this.deleteData.bind(this));

        $list.$button.$detail.on('click', function () {
            $tab.$preview.trigger('click');
        });

        $list.$button.$edit.on('click', function () {
            $tab.$form.trigger('click');
        });

        $list.$button.$up.on('click', this.moveUp.bind(this));
        $list.$button.$down.on('click', this.moveDown.bind(this));
        $list.$button.$refresh.on('click', this.loadData.bind(this));

        // preview

        $preview.$button.$edit.on('click', function () {
            $tab.$form.trigger('click');
        });

        $preview.$button.$cancel.on('click', function () {
            $tab.$list.trigger('click');
        });

        // form

        $form.$button.$save.on('click', this.saveData.bind(this));
        $form.$mediaTypeId.on('change', this.showHideFormContainers.bind(this));
        $form.$mediaTypeId.trigger('change');

        $form.$button.$cancel.on('click', function () {
            $tab.$list.trigger('click');
        });

        $form.$languageCode.on('click', this.showHideLangContainers.bind(this));

    };

    this.driveFieldSequence = function () {

    };

    this.reload = function () {
        $tab.$list.trigger('click');

    };

    this.loadData = function () {
        this.readData();

        $form.$publishStatusId.renderApi('PublishStatus/readData', { text: 'publishStatus' });
        $list.$publishStatusId.renderApi('PublishStatus/readData', { text: 'publishStatus' });
        $form.$languageCode.renderApi('Language/readData', { text: 'language', flag: 'flag' });

        $form.$mediaTypeId.renderApi('MediaType/readData', { text: 'mediaType' });
        //        $form.$contentLayoutId.renderApi('ContentLayout/readData', {text: 'contentLayout'});

        var url = 'ContentLayout/readData';

        api.post(url, function (result) {
            var items = [];

            $.each(result.data, function (i, data) {
                var imageUrl = config.url.site + 'asset/img/' + data.image;
                var htmlImage = '<img src="' + imageUrl + '">';
                items.push({
                    value: data.id,
                    text: htmlImage,
                });
            });

            $form.$contentLayoutId.render(items);
        });
    };

    this.showHideFormContainers = function () {
        $form.$contentContainer.hide();
        $form.$imageContainer.hide();
        $form.$videoContainer.hide();

        switch (parseInt($form.$mediaTypeId.getValue())) {
            case 1:
                $form.$contentContainer.show();
                $form.$imageContainer.show();
                break;
            case 2:
                $form.$videoContainer.show();
        }
    };

    this.showHideLangContainers = function () {

        if ($form.$languageCode.getValue() == localStorage.primaryLang) {
            $form.$slContainer.hide();
            $form.$elContainer.hide();
        } else if ($form.$languageCode.getValue() == localStorage.secondaryLang) {
            $form.$slContainer.show();
            $form.$elContainer.hide();
        } else if ($form.$languageCode.getValue() == localStorage.extendedLang) {
            $form.$slContainer.hide();
            $form.$elContainer.show();
        }
    };

    this.readData = function (callback) {

        if (!callback)
            $loading.show();

        var url = 'Pages/readData';
        var data = {
            orderBy: 'position',
            publishStatusId: $list.$publishStatusId.getValue(),
        };

        api.post(url, data, function (result) {

            rs = result.data;
            var rsCount = rs.length;

            if (rsCount > 0) {
                this.dataShow();
            }
            else {
                this.dataHide();
            }

            renderedRs = this.renderTable($list.$table, rs, tableFields, rsRange);
            this.renderPaging($list.$paging, renderedRs, rsRange);

            if ($.isFunction(callback))
                callback();
            else
                $loading.hide();
        }.bind(this));
    };

    this.dataHide = function () {
        $list.$paging.hide();
        $tab.$form.hide();
        $tab.$preview.hide();
        $list.$button.$delete.hide();
        $list.$button.$detail.hide();
        $list.$button.$edit.hide();
        $list.$button.$down.hide();
        $list.$button.$up.hide();
    };

    this.dataShow = function () {
        $list.$paging.show();
        $tab.$form.show();
        $tab.$preview.show();
        $list.$button.$delete.show();
        $list.$button.$detail.show();
        $list.$button.$edit.show();
        $list.$button.$down.show();
        $list.$button.$up.show();

    };

    this.searchData = function () {

        var filteredRs = rs.filter(function (data) {
            var regex = new RegExp($list.$search.getValue(), 'gi');
            var found = false;

            tableFields.forEach(function (field) {
                if (data[field])
                    if (data[field].match(regex))
                        found = true;
            });

            return found;
        });

        renderedRs = this.renderTable($list.$table, filteredRs, tableFields, rsRange);
        if (renderedRs.length > 0) {
            this.renderPaging($list.$paging, renderedRs, rsRange);
            this.dataShow();
        } else {
            this.dataHide();
        }
    };

    this.moveData = function () {
        renderedRs = this.renderTable($list.$table, renderedRs, tableFields, rsRange, $list.$paging.getValue());
    };

    this.moveUp = function () {

        var activeId = $list.$table.getValue();
        var prevId = $list.$table.find('tr.active').prev().find('input[type="checkbox"]').val()

        if (!prevId)
            return;

        $loading.show();

        var url = 'Pages/moveData';
        var data = {
            idFrom: activeId,
            idTo: prevId,
        };

        api.post(url, data, function (result) {

            if (result.status == 'success') {
                this.reload();
            }

            $loading.hide();
        }.bind(this));

    };

    this.moveDown = function () {

        var activeId = $list.$table.getValue();
        var prevId = $list.$table.find('tr.active').next().find('input[type="checkbox"]').val()

        if (!prevId)
            return;

        $loading.show();

        var url = 'Pages/moveData';
        var data = {
            idFrom: activeId,
            idTo: prevId,
        };

        api.post(url, data, function (result) {

            if (result.status == 'success') {
                this.reload();
            }

            $loading.hide();
        }.bind(this));
    };

    this.deleteData = function () {
        var checkedId = $list.$table.getCheckedValues();

        if (checkedId.length == 0) {
            $msgbox.alert('NO_DATA_CHECKED');
            return;
        };

        var url = 'Pages/multipleDeleteData';
        var data = {
            multipleId: checkedId,
        };

        $msgbox.confirm('DELETE_CHECKED_DATA', function () {
            api.post(url, data, function (result) {

                switch (result.status) {
                    case 'success':
                        $splash.show(result.message);
                        this.readData();

                        break;
                    case 'failed':
                        $msgbox.alert(result.message);
                }

            }.bind(this));
        }.bind(this));

    };

    this.detailData = function () {
        $loading.show();

        var url = 'Language/detailData';

        api.post(url, function (result) {

            if (result.status == 'success') {
                $languageS = result.data.secondaryEnabledId;
                $languageE = result.data.extendedEnabledId;

                if ($languageS == 1)
                    $preview.$slContainer.show();
                else
                    $preview.$slContainer.hide();

                if ($languageE == 1)
                    $preview.$elContainer.show();
                else
                    $preview.$elContainer.hide();
                $preview.$labelSl.html(result.data.secondaryLanguage);
                $preview.$labelEl.html(result.data.extendedLanguage);
            }
        });

        var url = 'Pages/detailData';
        var data = {
            id: $list.$table.getValue()
        };

        api.post(url, data, function (result) {

            if (result.status == 'success') {
                $preview.$publishStatus.setValue(result.data.publishStatus);
                $preview.$title.setValue(result.data.title);
                $preview.$titleSl.setValue(result.data.title_sl);
                $preview.$titleEl.setValue(result.data.title_el);
                $preview.$mediaType.setValue(result.data.mediaType);
                $preview.$contentLayout.setValue(result.data.contentLayout);
                $preview.$image.setValue(result.data.image);
                $preview.$videoEmbed.setValue(result.data.videoEmbed);
                var mediaTypeId = result.data.mediaTypeId;

                if (mediaTypeId == 1) {
                    $preview.$contentContainer.show();
                    $preview.$imageContainer.show();
                    $preview.$videoContainer.hide();

                } else if (mediaTypeId == 2) {
                    $preview.$contentContainer.hide();
                    $preview.$imageContainer.hide();
                    $preview.$videoContainer.show();
                } else {
                    $preview.$contentContainer.hide();
                    $preview.$imageContainer.hide();
                    $preview.$videoContainer.hide();
                }

                $preview.$description.setValue(result.data.description);
                $preview.$descriptionSl.setValue(result.data.description_sl);
                $preview.$descriptionEl.setValue(result.data.description_el);
                $preview.$moreDescription.setValue(result.data.moreDescription);
                $preview.$moreDescriptionSl.setValue(result.data.moreDescription_sl);
                $preview.$moreDescriptionEl.setValue(result.data.moreDescription_el);
            }

            $loading.hide();
        });
    };

    this.serializeData = function () {
        var ct = $form.$contentLayoutId.getValue();
        if (!$form.$contentLayoutId.getValue())
            var ct = 0
        var data = {
            title: $form.$title.getValue(),
            titleSl: $form.$titleSl.getValue(),
            titleEl: $form.$titleEl.getValue(),
            mediaTypeId: $form.$mediaTypeId.getValue(),
            contentLayoutId: ct,
            image: $form.$image.getValue(),
            videoEmbed: $form.$videoEmbed.getValue(),
            description: $form.$description.getValue(),
            descriptionSl: $form.$descriptionSl.getValue(),
            descriptionEl: $form.$descriptionEl.getValue(),
            moreDescription: $form.$moreDescription.getValue(),
            moreDescriptionSl: $form.$moreDescriptionSl.getValue(),
            moreDescriptionEl: $form.$moreDescriptionEl.getValue(),
            publishStatusId: $form.$publishStatusId.getValue(),
        };

        if (saveMode == EDIT_MODE)
            data.id = $list.$table.getValue();

        return data;
    };


    this.clearData = function () {
        $form.clear();

        var url = 'Language/detailData';

        api.post(url, function (result) {

            if (result.status == 'success') {
                $form.$labelSl.html(result.data.secondaryLanguage);
                $form.$labelEl.html(result.data.extendedLanguage);

            }
        });

        $form.$contentContainer.hide();
        $form.$imageContainer.hide();
        $form.$videoContainer.hide();

        $form.$languageCode.setValue(localStorage.primaryLang);
        this.showHideLangContainers();
        $form.$title.clear();
        $form.$titleSl.clear();
        $form.$titleEl.clear();
        $form.$mediaTypeId.clear();
        $form.$contentLayoutId.clear();
        $form.$image.clear();
        $form.$videoEmbed.clear();
        $form.$description.clear();
        $form.$descriptionSl.clear();
        $form.$descriptionEl.clear();
        $form.$moreDescription.clear();
        $form.$moreDescriptionSl.clear();
        $form.$moreDescriptionEl.clear();
        $form.$publishStatusId.clear();
    };

    this.editData = function () {
        $loading.show();
        saveMode = EDIT_MODE;

        this.clearData();

        var url = 'Pages/detailData';
        var data = {
            id: $list.$table.getValue()
        };

        api.post(url, data, function (result) {

            if (result.status == 'success') {
                $form.$title.setValue(result.data.title);
                $form.$titleSl.setValue(result.data.title_sl);
                $form.$titleEl.setValue(result.data.title_el);
                $form.$mediaTypeId.setValue(result.data.mediaTypeId);
                $form.$contentLayoutId.setValue(result.data.contentLayoutId);
                $form.$image.setValue(result.data.image);
                $form.$videoEmbed.setValue(result.data.videoEmbed);
                $form.$description.setValue(result.data.description);
                $form.$descriptionSl.setValue(result.data.description_sl);
                $form.$descriptionEl.setValue(result.data.description_el);
                $form.$moreDescription.setValue(result.data.moreDescription);
                $form.$moreDescriptionSl.setValue(result.data.moreDescription_sl);
                $form.$moreDescriptionEl.setValue(result.data.moreDescription_el);
                $form.$publishStatusId.setValue(result.data.publishStatusId);

                $form.$title.focus();
            }

            $loading.hide();
        });
    };

    this.newData = function () {
        saveMode = NEW_MODE;

        this.clearData();
        $form.$title.focus();
    };

    this.saveData = function () {
        $form.$button.$save.loading();

        switch (saveMode) {
            case EDIT_MODE:
                var url = 'Pages/updateData';
                break;
            case NEW_MODE:
                var url = 'Pages/createData';
        }

        var data = this.serializeData();

        api.post(url, data, function (result) {

            switch (result.status) {
                case 'success':
                    $splash.show(result.message);

                    this.readData(function () {
                        switch (saveMode) {
                            case EDIT_MODE:
                                $tab.$list.trigger('click');
                                break;
                            case NEW_MODE:
                                this.clearData();
                                $form.$title.focus();
                        }

                        $form.$button.$save.release();
                        window.scrollTo(0, 0);
                    }.bind(this));

                    break;
                case 'invalid':
                    $form.error(result);
                    $form.$button.$save.release();
            }

        }.bind(this));
    };


};

dashboard.application.controllers.Pages.prototype =
    Object.create(dashboard.application.core.Controller.prototype);

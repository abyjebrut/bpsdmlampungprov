dashboard.application.controllers.UsersAktivasi = function () {
    var $el = $page.getScope('UsersAktivasi');
    dashboard.application.core.Controller.call(this, $el);

    var rs = [];
    var renderedRs = [];

    var rsRange = config.paging.range;

    var tableHeads = ['NAME', 'EMAIL', 'PHONE_NUMBER'];
    var tableFields = ['name', 'email', 'mobileNumber'];



    var $tab;
    var $list;
    var $preview;
    var $form;

    this.init = function () {

        // tab

        $tab = $el.find('#tab');
        $tab.$list = $tab.getRef('list');
        $tab.$preview = $tab.getRef('preview');
        $tab.$form = $tab.getRef('form');

        // list

        $list = $el.find('#list');
        $list.$search = $list.getRef('search').uiTextBox({ icon: 'search', placeholder: 'Search...' });
        $list.$table = $list.getRef('table').uiTable({ headers: tableHeads });
        $list.$paging = $list.getRef('paging').uiPaging();

        $list.$button = $list.find('#button');

        $list.$button.$refresh = $list.$button.getRef('refresh').uiButton();
        $list.$button.$edit = $list.$button.getRef('edit').uiButton();
        $list.$button.$pdf = $list.$button.getRef('pdf').uiButton();

        // preview

        $preview = $el.find('#preview');
        $preview.$name = $preview.getRef('name').uiTextView();
        $preview.$photo = $preview.getRef('photo').uiImageView();
        $preview.$email = $preview.getRef('email').uiTextView();
        $preview.$mobileNumber = $preview.getRef('mobileNumber').uiTextView();
        $preview.$address = $preview.getRef('address').uiTextAreaView();

        $preview.$button = $preview.find('#button');
        $preview.$button.$edit = $preview.$button.getRef('edit').uiButton();
        $preview.$button.$cancel = $preview.$button.getRef('cancel').uiButton();
        $preview.$button.$pdf = $preview.$button.getRef('pdf').uiButton();
        $preview.$button.$pdf.hide();
        // form

        $form = $el.find('#form').uiForm();
        $form.$name = $form.getRef('name').uiTextView();
        $form.$photo = $form.getRef('photo').uiImageView();
        $form.$aktivasiId = $form.getRef('aktivasiId').uiRadioBox();

        $form.$button = $form.find('#button');
        $form.$button.$save = $form.$button.getRef('save').uiButton();
        $form.$button.$cancel = $form.$button.getRef('cancel').uiButton();

    };

    this.driveEvents = function () {
        // tab

        $tab.$preview.on('click', this.detailData.bind(this));
        $tab.$form.on('click', this.editData.bind(this));

        // list

        $list.$search.on('keyup', this.searchData.bind(this));

        $list.$table.on('dblclick', function () {
            $list.$table.on('dblclick', function () {
                if (!$list.$search.getValue()) {
                    if (rs.length > 0)
                        $tab.$preview.trigger('click');
                } else {
                    if (renderedRs.length > 0)
                        $tab.$preview.trigger('click');
                }

            });

        });

        $list.$paging.on('click', this.moveData.bind(this));

        $list.$button.$edit.on('click', function () {
            $tab.$form.trigger('click');
        });

        $list.$button.$refresh.on('click', this.readData.bind(this));
        $list.$button.$pdf.on('click', this.exportListToPdf.bind(this));

        // preview

        $preview.$button.$edit.on('click', function () {
            $tab.$form.trigger('click');
        });

        $preview.$button.$cancel.on('click', function () {
            $tab.$list.trigger('click');
        });

        $preview.$button.$pdf.on('click', this.exportDetailToPdf.bind(this));

        // form

        $form.$button.$save.on('click', this.saveData.bind(this));

        $form.$button.$cancel.on('click', function () {
            $tab.$list.trigger('click');
        });

    };

    this.driveFieldSequence = function () {


    };

    this.reload = function () {
        $tab.$list.trigger('click');

    };

    this.readData = function (callback) {

        if (!callback)
            $loading.show();
        $list.$search.clear();
        $form.$aktivasiId.renderApi('Decision/readData', { text: 'decision' });
        var url = 'User/readDataAktivasi';
        var data = {
            instansiId: localStorage.instansiId,
            orderBy: 'name',
        };

        api.post(url, data, function (result) {

            rs = result.data;
            var rsCount = rs.length;

            if (rsCount > 0) {
                this.dataShow();
            }
            else {
                this.dataHide();
            }


            renderedRs = this.renderTable($list.$table, rs, tableFields, rsRange);
            this.renderPaging($list.$paging, renderedRs, rsRange);

            if ($.isFunction(callback))
                callback();
            else
                $loading.hide();
        }.bind(this));
    };

    this.dataHide = function () {
        $tab.$preview.hide();
        $tab.$form.hide();
        $list.$button.$pdf.hide();
        $list.$button.$edit.hide();
    };

    this.dataShow = function () {
        $tab.$preview.show();
        $tab.$form.show();
        $list.$button.$pdf.hide();
        $list.$button.$edit.show();
    };


    this.searchData = function () {

        var filteredRs = rs.filter(function (data) {
            var regex = new RegExp($list.$search.getValue(), 'gi');
            var foundSearch = false;

            tableFields.forEach(function (field) {
                if (data[field])
                    if (data[field].match(regex))
                        foundSearch = true;
            });

            return foundSearch;
        });

        renderedRs = this.renderTable($list.$table, filteredRs, tableFields, rsRange);
        if (renderedRs.length > 0) {
            this.renderPaging($list.$paging, renderedRs, rsRange);
            this.dataShow();
        } else {
            this.dataHide();
        }
    };

    this.moveData = function () {
        renderedRs = this.renderTable($list.$table, renderedRs, tableFields, rsRange, $list.$paging.getValue());
    };

    this.detailData = function () {
        $loading.show();

        var url = 'User/detailData';
        var data = {
            id: $list.$table.getValue()
        };

        api.post(url, data, function (result) {

            if (result.status == 'success') {
                $preview.$name.setValue(result.data.name);
                $preview.$photo.setValue(result.data.photo);
                $preview.$email.setValue(result.data.email);
                $preview.$mobileNumber.setValue(result.data.mobileNumber);
                $preview.$address.setValue(result.data.address);
            }

            $loading.hide();
        });
    };

    this.serializeData = function () {
        var data = {
            aktivasiId: $form.$aktivasiId.getValue(),
            id: $list.$table.getValue(),
        };

        return data;
    };

    this.clearData = function () {
        $form.clear();

        $form.$name.clear();
        $form.$photo.clear();
        $form.$aktivasiId.clear();
    };

    this.editData = function () {
        $loading.show();

        this.clearData();

        var url = 'User/detailData';
        var data = {
            id: $list.$table.getValue()
        };

        api.post(url, data, function (result) {

            if (result.status == 'success') {
                $form.$name.setValue(result.data.name);
                $form.$photo.setValue(result.data.photo);
                $form.$aktivasiId.setValue(result.data.aktivasiId);

                $form.$aktivasiId.focus();
            }

            $loading.hide();
        });
    };

    this.saveData = function () {
        $form.$button.$save.loading();

        var url = 'User/updateActivation';
        var data = this.serializeData();

        api.post(url, data, function (result) {

            switch (result.status) {
                case 'success':
                    $splash.show(result.message);

                    this.readData(function () {

                        $tab.$list.trigger('click');
                        // break;

                        $form.$button.$save.release();
                        window.scrollTo(0, 0);
                    }.bind(this));

                    break;
                case 'invalid':
                    $form.error(result);
                    $form.$button.$save.release();
            }

        }.bind(this));
    };

    this.exportData = function (type, url, id, instansiId, aktivasiId) {

        $loading.show();

        var data = {};
        if (id)
            data.id = id;
        data.instansiId = instansiId;
        data.aktivasiId = aktivasiId;

        api.post(url, data, function (result) {

            $loading.hide();

            switch (result.status) {
                case 'success':

                    router.open(result.data);

                    break;
                case 'failed':
                    $msgbox.alert(result.message);
            }
        });
    };

    this.exportListToPdf = function () {

        var type = 'pdf';
        var url = 'User/createPDFList';
        var id = $list.$search.getValue();
        var instansiId = localStorage.instansiId;
        var aktivasiId = 2;

        this.exportData(type, url, id, instansiId, aktivasiId);
    };

    this.exportDetailToPdf = function () {
        var type = 'pdf';
        var url = 'User/createPDFDetail';
        var id = $list.$table.getValue();

        this.exportData(type, url, id);
    };

};

dashboard.application.controllers.UsersAktivasi.prototype =
    Object.create(dashboard.application.core.Controller.prototype);

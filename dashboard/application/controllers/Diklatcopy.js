dashboard.application.controllers.Diklat = function () {
    var $el = $page.getScope('Diklat');
    dashboard.application.core.Controller.call(this, $el);

    var rs = [];
    var rsBahan = [];
    var renderedRs = [];

    var rsRange = config.paging.range;

    var tableHeads = ['PUBLISH_STATUS', 'JENIS_DIKLAT', 'KODE', 'NAMA_DIKLAT', 'KUOTA', 'STATUS_DIKLAT'];
    var tableFields = ['publishStatus', 'jenisDiklat', 'kode', 'namaDiklat', 'kuota', 'statusDiklat'];

    var viewHeadsBahan = ['No', 'Tanggal', 'Waktu', 'Mata Pelajaran', 'Nama Pengajar', 'Ruang', 'Jumlah Jam'];
    var viewFieldsBahan = ['noUrut', 'allTanggal', 'allWaktu', 'mataPelajaran', 'allPengajar', 'ruang', 'allJam'];

    var rsPengajar = [];
    var cartHeads = ['Nama Pengajar'];
    var cartFields = ['namaPengajar'];

    var rsPengajarD = [];
    var rsIsiMapel = [];

    var saveMode;
    var detailMode;
    var tipeWaktuId;
    var jenisDiklatIdPengajar;
    var angkatanIdPengajar;
    var tglSelesai;

    var idPengajarSelect = '', idMapelSelect = '', idTglSelect = ''
    var $tab;
    var $list;
    var $listBahan;
    var $preview;
    var $form;

    var monthRecords = [
        'Jan',
        'Feb',
        'Mar',
        'Apr',
        'Mei',
        'Jun',
        'Jul',
        'Ags',
        'Sept',
        'Okt',
        'Nov',
        'Dec',
    ]

    var hariRecords = [
        'Minggu',
        'Senin',
        'Selasa',
        'Rabu',
        'Kamis',
        'Jum`at',
        'Sabtu',
    ]

    this.init = function () {

        // tab

        $tab = $el.find('#tab');
        $tab.$list = $tab.getRef('list');
        $tab.$preview = $tab.getRef('preview');
        $tab.$form = $tab.getRef('form');
        $tab.$new = $tab.getRef('new');

        // list

        $list = $el.find('#list');
        $list.$filter = $list.getRef('filter').uiComboBox({ placeholder: 'Filter By Jenis Diklat' });
        $list.$publishStatusId = $list.getRef('publishStatusId').uiRadioBox();
        $list.$search = $list.getRef('search').uiTextBox({ icon: 'search', placeholder: 'Search...' });
        $list.$table = $list.getRef('table').uiTable({ headers: tableHeads });
        $list.$paging = $list.getRef('paging').uiPaging();

        $list.$button = $list.find('#button');
        $list.$button.$delete = $list.$button.getRef('delete').uiButton();
        $list.$button.$detail = $list.$button.getRef('detail').uiButton();
        $list.$button.$edit = $list.$button.getRef('edit').uiButton();
        $list.$button.$jadwal = $list.$button.getRef('jadwal').uiButton();

        $list.$button.$refresh = $list.$button.getRef('refresh').uiButton();


        // preview

        $preview = $el.find('#preview');

        $preview.$publishStatus = $preview.getRef('publishStatus').uiTextView();
        $preview.$jenisDiklat = $preview.getRef('jenisDiklat').uiTextView();
        $preview.$kode = $preview.getRef('kode').uiTextView();
        $preview.$elDetail = $preview.getRef('elDetail');
        $preview.$elDetail.hide();
        $preview.$listJadwalDetail = $preview.getRef('listJadwalDetail').uiTableView(viewHeadsBahan);

        $preview.$elDetailInput = $preview.getRef('elDetailInput');
        $preview.$elDetailInput.hide();

        $preview.$listJadwalDetailInput = $preview.getRef('listJadwalDetailInput');

        $preview.$nomor = $preview.getRef('nomor').uiTextView();
        $preview.$namaDiklat = $preview.getRef('namaDiklat').uiTextAreaView();
        $preview.$tahunDiklat = $preview.getRef('tahunDiklat').uiTextView();
        $preview.$lokasi = $preview.getRef('lokasi').uiTextView();
        $preview.$angkatan = $preview.getRef('angkatan').uiTextView();
        $preview.$pelaksanaanMulai = $preview.getRef('pelaksanaanMulai').uiTextView();
        $preview.$pelaksanaanSelesai = $preview.getRef('pelaksanaanSelesai').uiTextView();
        $preview.$kuota = $preview.getRef('kuota').uiTextView();
        $preview.$statusDiklat = $preview.getRef('statusDiklat').uiTextView();

        $preview.$button = $preview.find('#button');
        $preview.$button.$edit = $preview.$button.getRef('edit').uiButton();
        $preview.$button.$cancel = $preview.$button.getRef('cancel').uiButton();

        // dialog

        $form.$editDialog = $form.getRef('editDialog');

        $form.$editDialog.$slSubContainer = $form.$editDialog.getRef('slSubContainer');
        $form.$editDialog.$slSubContainer.hide();
        $form.$editDialog.$elSubContainer = $form.$editDialog.getRef('elSubContainer');
        $form.$editDialog.$elSubContainer.hide();
        
        $form.$editDialog.$slSubPageContainer = $form.$editDialog.getRef('slSubPageContainer');
        $form.$editDialog.$slSubPageContainer.hide();
        $form.$editDialog.$elSubPageContainer = $form.$editDialog.getRef('elSubPageContainer');
        $form.$editDialog.$elSubPageContainer.hide();
        
        $form.$editDialog.$languageCodeSub = $form.$editDialog.getRef('languageCodeSub').uiLangBox();
        $form.$editDialog.$subMenuCaption = $form.$editDialog.getRef('subMenuCaption').uiTextBox();
        $form.$editDialog.$subMenuCaptionSl = $form.$editDialog.getRef('subMenuCaptionSl').uiTextBox();
        $form.$editDialog.$subMenuCaptionEl = $form.$editDialog.getRef('subMenuCaptionEl').uiTextBox();
        $form.$editDialog.$subMenuBehaviorId = $form.$editDialog.getRef('subMenuBehaviorId').uiRadioBox();
        $form.$editDialog.$subMenuPagesId = $form.$editDialog.getRef('subMenuPagesId').uiComboBox();
        $form.$editDialog.$subMenuPagesSl = $form.$editDialog.getRef('subMenuPagesSl').uiTextView();
        $form.$editDialog.$subMenuPagesEl = $form.$editDialog.getRef('subMenuPagesEl').uiTextView();
        $form.$editDialog.$subMenuUrl = $form.$editDialog.getRef('subMenuUrl').uiTextBox();
        $form.$editDialog.$subMenuOpenURLOnNewTabId = $form.$editDialog.getRef('subMenuOpenURLOnNewTabId').uiCheckBox();
        $form.$editDialog.$subMenuOpenURLOnNewTabId.render([{value: 1, text: 'Open URL On New Tab'}]);

        $form.$editDialog.$button = $form.$editDialog.find('.actions');

        $form.$editDialog.$button.$save = $form.$editDialog.$button.getRef('save').uiButton();
        $form.$editDialog.$button.$cancel = $form.$editDialog.$button.getRef('cancel').uiButton();

        // end dialog
        
        // form

        $form = $el.find('#form').uiForm();

        $form.$publishStatusId = $form.getRef('publishStatusId').uiRadioBox();
        $form.$jenisDiklatId = $form.getRef('jenisDiklatId').uiComboBox();
        $form.$kode = $form.getRef('kode').uiTextBox();
        $form.$nomor = $form.getRef('nomor').uiTextBox();
        $form.$namaDiklat = $form.getRef('namaDiklat').uiTextArea();
        $form.$tahunDiklat = $form.getRef('tahunDiklat').uiComboBox();
        $form.$lokasiId = $form.getRef('lokasiId').uiComboBox();
        $form.$angkatanId = $form.getRef('angkatanId').uiComboBox();
        $form.$jenisDiklatId = $form.getRef('jenisDiklatId').uiComboBox();
        $form.$pelaksanaanMulai = $form.getRef('pelaksanaanMulai').uiDatePicker();
        $form.$pelaksanaanSelesai = $form.getRef('pelaksanaanSelesai').uiDatePicker();
        $form.$kuota = $form.getRef('kuota').uiNumericBox({ maxlength: 3 });
        $form.$statusDiklatId = $form.getRef('statusDiklatId').uiRadioBox();

        $form.$button = $form.find('#button');
        $form.$button.$save = $form.$button.getRef('save').uiButton();
        $form.$button.$cancel = $form.$button.getRef('cancel').uiButton();

    };

    this.driveEvents = function () {
        // tab

        $tab.$preview.on('click', this.detailData.bind(this));
        $tab.$form.on('click', this.editData.bind(this));
        $tab.$new.on('click', this.newData.bind(this));

        // list

        $list.$filter.on('change', this.readData.bind(this));
        $list.$search.on('keyup', this.searchData.bind(this));
        $list.$publishStatusId.on('click', this.readData.bind(this));


        $list.$paging.on('click', this.moveData.bind(this));
        $list.$button.$delete.on('click', this.deleteData.bind(this));

        $list.$button.$detail.on('click', function () {
            detailMode = 'Detail'
            $tab.$preview.trigger('click');
        });
        $list.$button.$jadwal.on('click', function () {
            detailMode = 'Input'
            $tab.$preview.trigger('click');
        });

        $list.$button.$edit.on('click', function () {
            $tab.$form.trigger('click');
        });

        $list.$button.$refresh.on('click', this.reload.bind(this));

        // preview

        $preview.$button.$edit.on('click', function () {
            $tab.$form.trigger('click');
        });

        $preview.$button.$cancel.on('click', function () {
            $tab.$list.trigger('click');
        });

        // form

        $form.$button.$save.on('click', this.saveData.bind(this));

        $form.$button.$cancel.on('click', function () {
            $tab.$list.trigger('click');
        });

    };

    this.driveFieldSequence = function () {

    };

    this.reload = function () {
        $tab.$list.trigger('click');
        this.readData();
        $list.$filter.clear();
        // var i =2018
        var rsTahun = [];
        for (let i = 2018; i < 2040; i++) {
            rsTahun.push({
                value: i,
                text: i,
            });
        }
        $form.$tahunDiklat.render(rsTahun);
        $list.$filter.renderApi('JenisDiklat/readData', { orderBy: 'position' }, { text: 'jenisDiklat' });
        $form.$jenisDiklatId.renderApi('JenisDiklat/readData', { status: 'Open' }, { text: 'jenisDiklat' });
        $form.$statusDiklatId.renderApi('StatusDiklat/readData', { text: 'statusDiklat' });
        $form.$publishStatusId.renderApi('PublishStatus/readData', { text: 'publishStatus' });
        $list.$publishStatusId.renderApi('PublishStatus/readData', { text: 'publishStatus' });
        $form.$angkatanId.renderApi('Angkatan/readData', { text: 'angkatan' });
        $form.$lokasiId.renderApi('Lokasi/readData', { text: 'lokasi' });

    };
    this.readData = function (callback) {

        if (!callback)
            $loading.show();

        var url = 'Diklat/readData';
        var data = {
            orderBy: '_pelaksanaanMulai',
            reverse: 1,
            jenisDiklatId: $list.$filter.getValue(),
            publishStatusId: $list.$publishStatusId.getValue(),
        };

        api.post(url, data, function (result) {

            rs = result.data;
            var rsCount = rs.length;

            if (rsCount > 0) {
                this.dataShow();
            }
            else {
                this.dataHide();
            }

            renderedRs = this.renderTable($list.$table, rs, tableFields, rsRange);
            this.renderPaging($list.$paging, renderedRs, rsRange);

            if ($.isFunction(callback))
                callback();
            else
                $loading.hide();
        }.bind(this));
    };

    this.dataHide = function () {
        $list.$paging.hide();
        $tab.$form.hide();
        $list.$button.$delete.hide();
        $list.$button.$edit.hide();
        $list.$button.$detail.hide();
        $list.$button.$jadwal.hide();
    };

    this.dataShow = function () {
        $list.$paging.show();
        $tab.$form.show();
        $list.$button.$delete.show();
        $list.$button.$edit.show();
        $list.$button.$detail.show();
        $list.$button.$jadwal.show();
    };

    this.searchData = function () {

        var filteredRs = rs.filter(function (data) {
            var regex = new RegExp($list.$search.getValue(), 'gi');
            var found = false;

            tableFields.forEach(function (field) {
                if (data[field])
                    if (data[field].match(regex))
                        found = true;
            });

            return found;
        });

        renderedRs = this.renderTable($list.$table, filteredRs, tableFields, rsRange);
        if (renderedRs.length > 0) {
            this.renderPaging($list.$paging, renderedRs, rsRange);
            this.dataShow();
        } else {
            this.dataHide();
        }
    };

    this.moveData = function () {
        renderedRs = this.renderTable($list.$table, renderedRs, tableFields, rsRange, $list.$paging.getValue());
    };

    this.deleteData = function () {
        var checkedId = $list.$table.getCheckedValues();

        if (checkedId.length == 0) {
            $msgbox.alert('NO_DATA_CHECKED');
            return;
        };

        var url = 'Diklat/multipleDeleteData';
        var data = {
            multipleId: checkedId,
        };

        $msgbox.confirm('DELETE_CHECKED_DATA', function () {
            api.post(url, data, function (result) {

                switch (result.status) {
                    case 'success':
                        $splash.show(result.message);
                        this.readData();
                        break;
                    case 'failed':
                        $msgbox.alert(result.message);
                }

            }.bind(this));
        }.bind(this));

    };

    this.detailData = function () {
        $loading.show();

        var url = 'Diklat/detailData';
        var data = {
            id: $list.$table.getValue()
        };

        api.post(url, data, function (result) {

            if (result.status == 'success') {
                $preview.$publishStatus.setValue(result.data.publishStatus);
                $preview.$jenisDiklat.setValue(result.data.jenisDiklat);
                $preview.$kode.setValue(result.data.kode);
                $preview.$nomor.setValue(result.data.nomor);
                $preview.$namaDiklat.setValue(result.data.namaDiklat);
                $preview.$tahunDiklat.setValue(result.data.tahunDiklat);
                $preview.$lokasi.setValue(result.data.lokasi);
                $preview.$angkatan.setValue(result.data.angkatan);
                $preview.$pelaksanaanMulai.setValue(result.data.pelaksanaanMulai);
                $preview.$pelaksanaanSelesai.setValue(result.data.pelaksanaanSelesai);
                $preview.$kuota.setValue(result.data.kuota);
                $preview.$statusDiklat.setValue(result.data.statusDiklat);
                rsBahan = [];
                var rsPengajarFilter = [];
                rsPengajarD = result.data.listPengajar

                result.data.listJadwalDiklat.forEach(data1 => {
                    rsPengajarFilter = []
                    rsPengajarD.forEach(arr => {
                        if (data1.labelMapelId == arr.labelMapelId) {
                            rsPengajarFilter.push({
                                id: arr.userId,
                                labelMapelId: arr.labelMapelId,
                                namaPengajar: arr.namaPengajar
                            });
                        }
                    });

                    var date1 = new Date(data1.tanggal);
                    var bulan = monthRecords[date1.getMonth()]
                    var hari = hariRecords[date1.getDay()]
                    var date1Selesai = new Date(data1.tanggalSelesai);
                    var bulanSelesai = monthRecords[date1Selesai.getMonth()]
                    var hariSelesai = hariRecords[date1Selesai.getDay()]
                    var allTanggal, allWaktu, allPengajar, allJam
                    var statusSimpan = data1.statusSimpan
                    var pengajarDiklatId = data1.pengajarDiklatId;
                    if (!pengajarDiklatId)
                        pengajarDiklatId = '';
                    if (data1.tipeWaktuId == 1) {
                        if (statusSimpan = 'Belum')
                            allTanggal = ''
                        else
                            allTanggal = hari + ', ' + date1.getDate() + ' ' + bulan + ' ' + date1.getFullYear()
                        allWaktu = data1.jamMulai + ' - ' + data1.jamSelesai
                        allPengajar = data1.namaPengajar
                        if (data1.jumlahJp == 0)
                            allJam = '-'
                        else
                            allJam = data1.jumlahJp
                    } else {
                        if (statusSimpan = 'Belum')
                            allTanggal = ''
                        else
                            allTanggal = hari + ', ' + date1.getDate() + ' ' + bulan + ' ' + date1.getFullYear() + '<br>' + ' s/d ' + '<br>' + hariSelesai + ', ' + date1Selesai.getDate() + ' ' + bulanSelesai + ' ' + date1Selesai.getFullYear()
                        allWaktu = data1.jumlahHari + ' hari'
                        var htmlView = '';
                        var noi = 0
                        $.each(result.data.listJadwalDiklatPengajar, function (key, value) {
                            noi++
                            htmlView += noi + '. ' + value.namaPengajar + '<br>';
                        });

                        allPengajar = htmlView
                        allJam = ''
                    }

                    rsBahan.push({
                        id: data1.id,
                        allWaktu: allWaktu,
                        allTanggal: allTanggal,
                        allPengajar: allPengajar,
                        allJam: allJam,
                        diklatId: data1.diklatId,
                        rsPengajarFilter: rsPengajarFilter,
                        ruang: data1.ruang,
                        pengajarDiklatId: data1.pengajarDiklatId,
                        namaPengajar: data1.namaPengajar,
                        ruang: data1.ruang,
                        keterangan: data1.keterangan,
                        mataPelajaranId: data1.mataPelajaranId,
                        labelMapelId: data1.labelMapelId,
                        mataPelajaran: data1.mataPelajaran,
                        noUrut: data1.noUrut,
                        jumlahHari: data1.jumlahHari,
                        tipeWaktuId: data1.tipeWaktuId,
                        tipeWaktu: data1.tipeWaktu,
                        tipeMataPelajaranId: data1.tipeMataPelajaranId,
                        tipeMataPelajaran: data1.tipeMataPelajaran,
                        statusSimpan: statusSimpan,
                        tanggal: data1.tanggal,
                        _tanggal: data1._tanggal,
                        tanggalSelesai: data1.tanggalSelesai,
                        _tanggalSelesai: data1._tanggalSelesai,
                        jamMulai: data1.jamMulai,
                        nilaiJamMulai: data1.nilaiJamMulai,
                        _jamMulai: data1._jamMulai,
                        jamMulaiMenit: data1.jamMulaiMenit,
                        jamSelesai: data1.jamSelesai,
                        nilaiJamSelesai: data1.nilaiJamSelesai,
                        _jamSelesai: data1._jamSelesai,
                        jamSelesaiMenit: data1.jamSelesaiMenit,
                        jumlahJp: data1.jumlahJp,
                        jumlahJam: data1.jumlahJam,
                    });
                });

                $preview.$elDetail.hide();
                $preview.$elDetailInput.hide();
                if (detailMode == 'Detail') {
                    this.renderViewCart();
                    $preview.$elDetail.show();
                } else {
                    this.renderEditCart();
                    $preview.$elDetailInput.show();
                }
            }

            $loading.hide();
        }.bind(this));
    };

    this.serializeData = function () {

        var data = {
            publishStatusId: $form.$publishStatusId.getValue(),
            jenisDiklatId: $form.$jenisDiklatId.getValue(),
            kode: $form.$kode.getValue(),
            nomor: $form.$nomor.getValue(),
            lokasiId: $form.$lokasiId.getValue(),
            tahunDiklat: $form.$tahunDiklat.getValue(),
            angkatanId: $form.$angkatanId.getValue(),
            namaDiklat: $form.$namaDiklat.getValue(),
            pelaksanaanMulai: $form.$pelaksanaanMulai.getValue(),
            pelaksanaanSelesai: $form.$pelaksanaanSelesai.getValue(),
            kuota: $form.$kuota.getValue(),
            statusDiklatId: $form.$statusDiklatId.getValue(),
        };

        if (saveMode == EDIT_MODE)
            data.id = $list.$table.getValue();

        return data;
    };


    this.clearData = function () {
        $form.clear();
        let tgl1 = new Date()
        let tgl = tgl1.getFullYear() + '-' + (tgl1.getMonth() + 1) + '-' + tgl1.getDate()

        $form.$publishStatusId.setValue(2);
        $form.$kode.clear();
        $form.$namaDiklat.clear();
        $form.$tahunDiklat.setValue(tgl1.getFullYear());
        $form.$angkatanId.clear();
        $form.$lokasiId.clear();
        $form.$pelaksanaanMulai.setValue(tgl);
        $form.$pelaksanaanSelesai.setValue(tgl);
        $form.$kuota.clear();
        $form.$statusDiklatId.setValue(1);

    };

    this.editData = function () {
        $loading.show();

        this.clearData();
        saveMode = EDIT_MODE;

        var url = 'Diklat/detailData';
        var data = {
            id: $list.$table.getValue(),
        };

        api.post(url, data, function (result) {

            if (result.status == 'success') {

                $form.$publishStatusId.setValue(result.data.publishStatusId);
                $form.$kode.setValue(result.data.kode);
                $form.$nomor.setValue(result.data.nomor);
                $form.$tahunDiklat.setValue(result.data.tahunDiklat);
                $form.$lokasiId.setValue(result.data.lokasiId);
                $form.$angkatanId.setValue(result.data.angkatanId);
                $form.$namaDiklat.setValue(result.data.namaDiklat);
                $form.$pelaksanaanMulai.setValue(result.data._pelaksanaanMulai);
                $form.$pelaksanaanSelesai.setValue(result.data._pelaksanaanSelesai);
                $form.$kuota.setValue(result.data._kuota);
                $form.$statusDiklatId.setValue(result.data.statusDiklatId);
                $form.$jenisDiklatId.setValue(result.data.jenisDiklatId);

                $form.$kode.focus();
            }

            $loading.hide();
        }.bind(this));
    };


    this.newData = function () {
        saveMode = NEW_MODE;

        $form.$jenisDiklatId.setValue($list.$filter.getValue());

        this.clearData();
        $form.$kode.focus();
    };

    this.saveData = function () {

        if (!$form.$pelaksanaanMulai.getValue()) {
            $msgbox.alert('PELAKSANAAN_MULAI_REQUIRED');
            return;
        }

        var d = $form.$pelaksanaanMulai.getValue();
        var splitD = d.split('-');
        var curr_date = splitD[2];
        if (curr_date.length == 1) {
            curr_date = "0" + curr_date;
        }
        var curr_month = splitD[1];
        if (curr_month.length == 1) {
            curr_month = "0" + curr_month;
        }
        var curr_year = splitD[0];

        $pelaksanaanMulai = curr_year + curr_month + curr_date;

        if (!$form.$pelaksanaanSelesai.getValue()) {
            $msgbox.alert('PELAKSANAAN_SELESAI_REQUIRED');
            return;
        }

        var str = $form.$pelaksanaanSelesai.getValue();
        var splitStr = str.split('-');
        var tahunPengujian = splitStr[0];
        var bulanPengujian = splitStr[1];
        if (bulanPengujian.length == 1) {
            bulanPengujian = "0" + bulanPengujian;
        }
        var hariPengujian = splitStr[2];
        if (hariPengujian.length == 1) {
            hariPengujian = "0" + hariPengujian;
        }
        var pelaksanaanSelesai = tahunPengujian + bulanPengujian + hariPengujian;

        if (parseInt($pelaksanaanMulai) > parseInt(pelaksanaanSelesai)) {
            $msgbox.alert('PELAKSANAAN_SALAH');
            return;
        }

        $form.$button.$save.loading();

        switch (saveMode) {
            case EDIT_MODE:
                var url = 'Diklat/updateData';
                break;
            case NEW_MODE:
                var url = 'Diklat/createData';
        }

        var data = this.serializeData();

        api.post(url, data, function (result) {

            switch (result.status) {
                case 'success':
                    $splash.show(result.message);

                    this.readData(function () {
                        switch (saveMode) {
                            case EDIT_MODE:
                                $tab.$list.trigger('click');
                                break;
                            case NEW_MODE:
                                this.clearData();
                                $form.$jenisDiklatId.renderApi('JenisDiklat/readData', { status: 'Open' }, { text: 'jenisDiklat' });
                                $form.$kode.focus();
                        }

                        $form.$button.$save.release();
                        window.scrollTo(0, 0);
                    }.bind(this));

                    break;
                case 'invalid':
                    $form.error(result);
                    $form.$button.$save.release();
            }

        }.bind(this));
    };
    this.renderDataTable = function ($table, dataSource, keyFields, range, pageActive) {

        if ($(window).height() > 900)
            range = 15;

        if (!pageActive)
            var pageActive = 1;

        var rowActive = (pageActive - 1) * range;
        rsIsiMapel = [];

        for (var i = rowActive; i < dataSource.length; i++) {
            var row = [];
            var field = dataSource[i];

            if (field) {

                keyFields.forEach(function (keyField) {
                    row.push(field[keyField]);
                });

                rsIsiMapel.push({
                    id: field.id,
                    noUrut: field.noUrut,
                    tipeWaktuId: field.tipeWaktuId,
                    rsPengajarFilter: field.rsPengajarFilter,
                    labelMapelId: field.labelMapelId,
                    tipeMataPelajaranId: field.tipeMataPelajaranId,
                    allTanggal: field.allTanggal,
                    allWaktu: field.allWaktu,
                    tanggal: field.tanggal,
                    pengajarDiklatId: field.pengajarDiklatId,
                    namaPengajar: field.namaPengajar,
                    mataPelajaran: field.mataPelajaran,
                    allJam: field.allJam,
                    ruang: field.ruang,
                    statusSimpan: field.statusSimpan,
                });
            }
        }

        var tableHeadsHtml = '';
        var select1 = '';
        var ol1 = '';
        tableHeadsHtml += `<th style="display:none;" >Id</th>`;
        tableHeadsHtml += `<th style="display:none;" >TipeWaktu</th>`;
        tableHeadsHtml += `<th style="display:none;" >TipeMapel</th>`;
        for (tableHead of viewHeadsBahan) {
            tableHeadsHtml += `<th>${tableHead}</th>`;
        }

        var html = `
        <table id="example">
        <thead>
            <tr>
                ${tableHeadsHtml}
                <th>Aksi</th>
            </tr>
            
        </thead>
        <tbody>
        `;

        for (row of rsIsiMapel) {

            html += '<tr style="cursor: pointer">';
            html += `<td style="display:none; border-bottom: solid silver 1px">${row.id}</td>`;
            html += `<td style="display:none; border-bottom: solid silver 1px">${row.tipeWaktuId}</td>`;
            html += `<td style="display:none; border-bottom: solid silver 1px">${row.tipeMataPelajaranId}</td>`;
            html += `<td style="border-bottom: solid silver 1px">${row.noUrut}</td>`;
            html += `<td style="border-bottom: solid silver 1px"><input value="${row.allTanggal}" class="datepicker input-width" placeholder="Tanggal" type="text" style="width:75px!important;" ></td>`;
            html += `<td style="border-bottom: solid silver 1px">${row.allWaktu}</td>`;
            html += `<td style="border-bottom: solid silver 1px">${row.mataPelajaran}</td>`;

            for (column of row.rsPengajarFilter) {
                if (column['id'] == row.pengajarDiklatId)
                    select1 += `<option value="${row.pengajarDiklatId}">${row.namaPengajar}</option>`;
                else
                    select1 += `<option value="${column['id']}">${column['namaPengajar']}</option>`;
            }

            rsPengajarFilter = [];
            if (row.tipeWaktuId == 2)
                ol1 += `<ol>
            <li>Coffee</li>
            <li>Tea</li>
            <li>Milk</li>
            </ol> `;
            html += `<td style="width: 140px">
                    <a class="act-pengajar"  data-ref="pengajar" data-id="${row.id}" href="javascript:">
                    <i class="write square icon"></i></a>
            </td>`;
            // html += `<td style="border-bottom: solid silver 1px" class="cpengajar1"><select id='myColor' class="cpengajar" data-ref="pengjarId"><option value="" data-hidden="true">--Pilih Pengajar--</option>${select1}</select>
            // ${ol1}
            // </td>`;
            // $("td.Client", row)
            $('td select.cpengajar').empty();
            // $('#example tbody').on('click', 'tr', function () {
            //     $(this).toggleClass('selected');
            // });
    
            html += `<td style="border-bottom: solid silver 1px"><input value="${row.ruang}" class="ruang input-width" placeholder="Ruang" type="text" style="width:75px!important;" ></td>`;
            html += `<td style="border-bottom: solid silver 1px">${row.allJam}</td>`;

            html += `<td style="width: 140px">
                <button class="act-simpan" data-id="${row.id}" href="javascript:">
                    Simpan
                </button>
                <br>
                ${row.statusSimpan}
            </td>`;
            html += '</tr>';
        }

        html += '</tbody></table>'

        $table.html(html);
        $table.find('table').DataTable({
            searching: true, "paging": false,
            select: true,
            "ordering": true,
            "info": false,
            columnDefs: [{
                orderable: false,
                targets: [1, 2, 3, 4, 5]
            }]
        });
        $input = $table.find('input.datepicker');
        $input.datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd-mm-yy',
            yearRange: '-' + config.ui.pastYearRange + ':+' + config.ui.futureYearRange,

        });
        var foundCari = false;
        var table = $('#example').DataTable();

        $('#example tbody').on('click', 'tr', function () {
            $(this).toggleClass('selected');
        });

        $('#example tbody').on('click', 'tr', function () {
            var rowData = table.row(this).data();
            idMapelSelect = rowData[0]

            $table.find("select.cpengajar").change(function () {
                idPengajarSelect = $(this).children("option:selected").val();
                $.each(rsIsiMapel, function (i, row) {
                    if (row.id == idMapelSelect) {
                        rsIsiMapel[i].pengajarDiklatId = idPengajarSelect;
                    }
                });
            });

            $table.find('input.datepicker').change(function () {
                idTglSelect = $(this).val();
                $.each(rsIsiMapel, function (i, row) {
                    if (row.id == idMapelSelect) {
                        rsIsiMapel[i].tanggal = idTglSelect;
                    }
                });
            });

            $table.find('input.ruang').change(function () {
                var idRuang = $(this).val();
                $.each(rsIsiMapel, function (i, row) {
                    if (row.id == idMapelSelect) {
                        rsIsiMapel[i].ruang = idRuang;
                    }
                });
            });

        });

        $table.find('.act-simpan').click(function () {
            var idData = $(event.target).data('id');
            console.log(rsIsiMapel)
            return false;
        });

        return dataSource;
    };


    this.renderViewCart = function () {
        this.renderTableNoPaging($preview.$listJadwalDetail, rsBahan, viewFieldsBahan, 1000);
    };

    this.renderEditCart = function () {
        this.renderDataTable($preview.$listJadwalDetailInput, rsBahan, viewFieldsBahan, 1000);
    };


};

dashboard.application.controllers.Diklat.prototype =
    Object.create(dashboard.application.core.Controller.prototype);

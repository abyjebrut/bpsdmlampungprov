dashboard.application.controllers.UsersPrivileges = function () {
    var $el = $page.getScope('UsersPrivileges');
    dashboard.application.core.Controller.call(this, $el);

    var rs = [];
    var renderedRs = [];

    var rsRange = config.paging.range;

    var tableHeads = ['PRIVILEGES'];
    var tableFields = ['privileges'];

    var saveMode;

    var $tab;
    var $list;
    var $preview;
    var $form;

    this.init = function () {

        // tab

        $tab = $el.find('#tab');
        $tab.$list = $tab.getRef('list');
        $tab.$preview = $tab.getRef('preview');
        $tab.$form = $tab.getRef('form');
        $tab.$new = $tab.getRef('new');

        // list

        $list = $el.find('#list');
        $list.$search = $list.getRef('search').uiTextBox({ icon: 'search', placeholder: 'Search...' });
        $list.$table = $list.getRef('table').uiTable({ headers: tableHeads });
        $list.$paging = $list.getRef('paging').uiPaging();

        $list.$button = $list.find('#button');
        $list.$button.$delete = $list.$button.getRef('delete').uiButton();
        $list.$button.$detail = $list.$button.getRef('detail').uiButton();
        $list.$button.$edit = $list.$button.getRef('edit').uiButton();

        $list.$button.$refresh = $list.$button.getRef('refresh').uiButton();

        // preview

        $preview = $el.find('#preview');
        $preview.$privileges = $preview.getRef('privileges').uiTextView();
        $preview.$menuAccess = $preview.getRef('menuAccess').uiCheckTree({ structure: config.menu });

        $preview.$button = $preview.find('#button');
        $preview.$button.$edit = $preview.$button.getRef('edit').uiButton();
        $preview.$button.$cancel = $preview.$button.getRef('cancel').uiButton();

        // form

        $form = $el.find('#form').uiForm();
        $form.$privileges = $form.getRef('privileges').uiTextBox();
        $form.$menuAccess = $form.getRef('menuAccess').uiCheckTree({ structure: config.menu });
        $form.$panelAccess = $form.getRef('panelAccess').uiCheckBox();

        $form.$button = $form.find('#button');
        $form.$button.$save = $form.$button.getRef('save').uiButton();
        $form.$button.$cancel = $form.$button.getRef('cancel').uiButton();

    };

    this.driveEvents = function () {
        // tab

        $tab.$preview.on('click', this.detailData.bind(this));
        $tab.$form.on('click', this.editData.bind(this));
        $tab.$new.on('click', this.newData.bind(this));

        // list

        $list.$search.on('keyup', this.searchData.bind(this));

        $list.$table.on('dblclick', function () {
            if (!$list.$search.getValue()) {
                if (rs.length > 0)
                    $tab.$form.trigger('click');
            } else {
                if (renderedRs.length > 0)
                    $tab.$form.trigger('click');
            }
        });

        $list.$paging.on('click', this.moveData.bind(this));
        $list.$button.$delete.on('click', this.deleteData.bind(this));

        $list.$button.$detail.on('click', function () {
            $tab.$preview.trigger('click');
        });

        $list.$button.$edit.on('click', function () {
            $tab.$form.trigger('click');
        });

        $list.$button.$refresh.on('click', this.readData.bind(this));

        // preview

        $preview.$button.$edit.on('click', function () {
            $tab.$form.trigger('click');
        });

        $preview.$button.$cancel.on('click', function () {
            $tab.$list.trigger('click');
        });

        // form

        $form.$button.$save.on('click', this.saveData.bind(this));

        $form.$button.$cancel.on('click', function () {
            $tab.$list.trigger('click');
        });

    };

    this.driveFieldSequence = function () {

        $form.$privileges.on('enter', function () {
            $form.$button.$save.trigger('click');
        });

    };

    this.reload = function () {
        $tab.$list.trigger('click');

    };

    this.readData = function (callback) {

        if (!callback)
            $loading.show();
        var panelAccess = [];
        $.each(config.panel, function (key, item) {

            if (item.access == ACCESS_USER || item.access == ACCESS_USER_ONLY) {
                if (config.strings[localStorage.lang][item.caption])
                    var string = config.strings[localStorage.lang][item.caption];
                else
                    var string = item.caption;

                panelAccess.push({ value: key, text: string });
            }
        });

        $form.$panelAccess.render(panelAccess);
        var url = 'UserPrivileges/readData';
        var data = {
            orderBy: 'privileges',
        };

        api.post(url, data, function (result) {

            rs = result.data;
            var rsCount = rs.length;

            if (rsCount > 0) {
                this.dataShow();
            }
            else {
                this.dataHide();
            }

            renderedRs = this.renderTable($list.$table, rs, tableFields, rsRange);
            this.renderPaging($list.$paging, renderedRs, rsRange);

            if ($.isFunction(callback))
                callback();
            else
                $loading.hide();
        }.bind(this));
    };

    this.dataHide = function () {
        $list.$paging.hide();
        $tab.$form.hide();
        // $tab.$preview.hide();
        $list.$button.$delete.hide();
        // $list.$button.$detail.hide();
        $list.$button.$edit.hide();
    };

    this.dataShow = function () {
        $list.$paging.show();
        $tab.$form.show();
        // $tab.$preview.show();
        $list.$button.$delete.show();
        // $list.$button.$detail.show();
        $list.$button.$edit.show();
    };

    this.searchData = function () {

        var filteredRs = rs.filter(function (data) {
            var regex = new RegExp($list.$search.getValue(), 'gi');
            var found = false;

            tableFields.forEach(function (field) {
                if (data[field])
                    if (data[field].match(regex))
                        found = true;
            });

            return found;
        });

        renderedRs = this.renderTable($list.$table, filteredRs, tableFields, rsRange);
        if (renderedRs.length > 0) {
            this.renderPaging($list.$paging, renderedRs, rsRange);
            this.dataShow();
        } else {
            this.dataHide();
        }
    };

    this.moveData = function () {
        renderedRs = this.renderTable($list.$table, renderedRs, tableFields, rsRange, $list.$paging.getValue());
    };

    this.deleteData = function () {
        var checkedId = $list.$table.getCheckedValues();

        if (checkedId.length == 0) {
            $msgbox.alert('NO_DATA_CHECKED');
            return;
        };

        var url = 'UserPrivileges/multipleDeleteData';
        var data = {
            multipleId: checkedId,
        };

        $msgbox.confirm('DELETE_CHECKED_DATA', function () {
            api.post(url, data, function (result) {

                switch (result.status) {
                    case 'success':
                        $splash.show(result.message);
                        this.readData();

                        break;
                    case 'failed':
                        $msgbox.alert(result.message);
                }

            }.bind(this));
        }.bind(this));

    };

    this.detailData = function () {
        $loading.show();

        var url = 'UserPrivileges/detailData';
        var data = {
            id: $list.$table.getValue(),
        };

        api.post(url, data, function (result) {

            if (result.status == 'success') {
                $preview.$privileges.setValue(result.data.privileges);
                $preview.$menuAccess.setValue(result.data.menuAccess);
            }

            $loading.hide();
        });
    };

    this.serializeData = function () {
        var data = {
            privileges: $form.$privileges.getValue(),
            menuAccess: $form.$menuAccess.getValue(),
            panelAccess: $form.$panelAccess.getValue(),
        };

        if (saveMode == EDIT_MODE)
            data.id = $list.$table.getValue();

        return data;
    };

    this.clearData = function () {
        $form.clear();

        $form.$privileges.clear();
        $form.$menuAccess.checkAll();
        $form.$panelAccess.checkAll();
    };

    this.editData = function () {
        $loading.show();
        saveMode = EDIT_MODE;

        this.clearData();

        var url = 'UserPrivileges/detailData';
        var data = {
            id: $list.$table.getValue(),
        };

        api.post(url, data, function (result) {

            if (result.status == 'success') {
                $form.$privileges.setValue(result.data.privileges);
                $form.$menuAccess.setValue(result.data.menuAccess);
                $form.$panelAccess.setValue(result.data.panelAccess);

                $form.$privileges.focus();
            }

            $loading.hide();
        });
    };

    this.newData = function () {
        saveMode = NEW_MODE;

        this.clearData();
        $form.$privileges.focus();
    };

    this.saveData = function () {
        $form.$button.$save.loading();

        switch (saveMode) {
            case EDIT_MODE:
                var url = 'UserPrivileges/updateData';
                break;
            case NEW_MODE:
                var url = 'UserPrivileges/createData';
        }

        var data = this.serializeData();

        api.post(url, data, function (result) {

            switch (result.status) {
                case 'success':
                    $splash.show(result.message);

                    this.readData(function () {
                        switch (saveMode) {
                            case EDIT_MODE:
                                $tab.$list.trigger('click');
                                break;
                            case NEW_MODE:
                                this.clearData();
                                $form.$privileges.focus();
                        }

                        $form.$button.$save.release();
                        window.scrollTo(0, 0);
                    }.bind(this));

                    break;
                case 'invalid':
                    $form.error(result);
                    $form.$button.$save.release();
            }

        }.bind(this));
    };



};

dashboard.application.controllers.UsersPrivileges.prototype =
    Object.create(dashboard.application.core.Controller.prototype);

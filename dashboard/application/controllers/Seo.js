dashboard.application.controllers.Seo = function () {
    var $el = $page.getScope('Seo');
    dashboard.application.core.Controller.call(this, $el);

    var rs = [];
    var renderedRs = [];

    var rsRange = config.paging.range;

    var tableHeads = ['PAGE'];
    var tableFields = ['title'];

    var rsKeywords = [];
    var cartHeads = ['KEYWORDS'];
    var cartFields = ['keywords'];

    var $tab;
    var $list;
    var $form;

    this.init = function () {

        // tab

        $tab = $el.find('#tab');
        $tab.$list = $tab.getRef('list');
        $tab.$form = $tab.getRef('form');

        // list

        $list = $el.find('#list');
        $list.$search = $list.getRef('search').uiTextBox({ icon: 'search', placeholder: 'Search...' });
        $list.$table = $list.getRef('table').uiTable({ headers: tableHeads });
        $list.$paging = $list.getRef('paging').uiPaging();

        $list.$button = $list.find('#button');

        $list.$button.$refresh = $list.$button.getRef('refresh').uiButton();
        $list.$button.$edit = $list.$button.getRef('edit').uiButton();

        // form

        $form = $el.find('#form').uiForm();

        $form.$pagesId = $form.getRef('pagesId').uiTextView();
        $form.$description = $form.getRef('description').uiTextArea();

        $form.$keywords = $form.getRef('keywords').uiTextBox();
        $form.$listKeywords = $form.getRef('listKeywords').uiTable(cartHeads);
        $form.$add = $form.getRef('add');
        $form.$delete = $form.getRef('delete');
        $form.$edit = $form.getRef('edit');

        $form.$button = $form.find('#button');
        $form.$button.$save = $form.$button.getRef('save').uiButton();
        $form.$button.$cancel = $form.$button.getRef('cancel').uiButton();

        // dialog

        $form.$editDialog = $form.getRef('editDialog');

        $form.$editDialog.$editKeywords = $form.$editDialog.getRef('editKeywords').uiTextBox();

        $form.$editDialog.$button = $form.$editDialog.find('.actions');
        $form.$editDialog.$button.$save = $form.$editDialog.$button.getRef('save').uiButton();
        $form.$editDialog.$button.$cancel = $form.$editDialog.$button.getRef('cancel').uiButton();

        // end dialog

    };

    this.driveEvents = function () {
        // tab

        $tab.$form.on('click', this.editData.bind(this));

        // list

        $list.$search.on('keyup', this.searchData.bind(this));

        $list.$table.on('dblclick', function () {
            $tab.$form.trigger('click');
        });

        $list.$paging.on('click', this.moveData.bind(this));

        $list.$button.$edit.on('click', function () {
            $tab.$form.trigger('click');
        });

        $list.$button.$refresh.on('click', this.readData.bind(this));

        // form

        $form.$button.$save.on('click', this.saveData.bind(this));
        $form.$add.on('click', this.addToCart.bind(this));
        $form.$delete.on('click', this.deleteFromCart.bind(this));

        $form.$edit.on('click', this.editCart.bind(this));
        $form.$listKeywords.on('dblclick', this.editCart.bind(this));
        $form.$editDialog.$button.$save.on('click', this.saveCart.bind(this));

        $form.$button.$cancel.on('click', function () {
            $tab.$list.trigger('click');
        });

    };

    this.driveFieldSequence = function () {

    };

    this.reload = function () {
        $tab.$list.trigger('click');

    };

    this.readData = function (callback) {

        if (!callback)
            $loading.show();

        var url = 'Seo/readData';

        api.post(url, function (result) {

            rs = result.data;
            var rsCount = rs.length;

            if (rsCount > 0) {
                var showButtons = true;
                $tab.$form.show();
            }
            else {
                var showButtons = false;
                $tab.$form.hide();
            }

            $list.$button.$edit.setEnabled(showButtons);

            renderedRs = this.renderTable($list.$table, rs, tableFields, rsRange);
            this.renderPaging($list.$paging, renderedRs, rsRange);

            if ($.isFunction(callback))
                callback();
            else
                $loading.hide();
        }.bind(this));
    };

    this.searchData = function () {

        var filteredRs = rs.filter(function (data) {
            var regex = new RegExp($list.$search.getValue(), 'gi');
            var found = false;

            tableFields.forEach(function (field) {
                if (data[field])
                    if (data[field].match(regex))
                        found = true;
            });

            return found;
        });

        renderedRs = this.renderTable($list.$table, filteredRs, tableFields, rsRange);
        this.renderPaging($list.$paging, renderedRs, rsRange);
    };

    this.moveData = function () {
        renderedRs = this.renderTable($list.$table, renderedRs, tableFields, rsRange, $list.$paging.getValue());
    };

    this.serializeData = function () {
        var data = {
            pagesId: $list.$table.getValue(),
            description: $form.$description.getValue(),
            listKeywords: rsKeywords,
        };

        return data;
    };


    this.clearData = function () {
        $form.clear();

        $form.$pagesId.clear();
        $form.$description.clear();
        $form.$keywords.clear();

        rsKeywords = new Array();
        this.renderEditCart();
    };

    this.editData = function () {
        $loading.show();

        this.clearData();

        var url = 'Seo/detailData';
        var data = {
            id: $list.$table.getValue()
        };

        api.post(url, data, function (result) {

            if (result.status == 'success') {
                $form.$pagesId.setValue(result.data.title);
                $form.$description.setValue(result.extra);

                rsKeywords = result.data.listKeywords;
                this.renderEditCart();

                $form.$description.focus();
            }

            $loading.hide();
        }.bind(this));
    };

    this.saveData = function () {
        $form.$button.$save.loading();

        var url = 'Seo/saveData';
        var data = this.serializeData();

        api.post(url, data, function (result) {

            switch (result.status) {
                case 'success':
                    $splash.show(result.message);
                    $form.$button.$save.release();
                    this.editData(function () {
                        $tab.$form.trigger('click');

                        window.scrollTo(0, 0);
                    }.bind(this));
                    break;
                case 'invalid':
                    $form.error(result);
                    $form.$button.$save.release();
            }

        }.bind(this));
    };

    this.renderViewCart = function () {
        this.renderTable($preview.$listKeywords, rsKeywords, cartFields, 100);
    };

    this.renderEditCart = function () {
        this.renderTable($form.$listKeywords, rsKeywords, cartFields, 100);
    };

    this.addToCart = function () {

        if (!$form.$keywords.getValue()) {
            $msgbox.alert('KEYWORDS_REQUIRED');
            return;
        }

        var found = false;
        rsKeywords.forEach(function (data) {

            if (data.keywords == $form.$keywords.getValue())
                found = true;
        });

        if (found) {
            $msgbox.alert('KEYWORDS_IS_REG');
            return;
        }

        rsKeywords.push({
            id: this.guid(),
            keywords: $form.$keywords.getValue(),
        });

        $form.$keywords.clear();
        $form.$keywords.focus();

        this.renderEditCart();
    };

    this.editCart = function () {

        var id = $form.$listKeywords.getValue();

        if (!id)
            return;

        var rowLinks = [];

        $.each(rsKeywords, function (i, row) {
            if (row.id == id)
                rowLinks = row;
        });

        $form.$editDialog.$editKeywords.setValue(rowLinks.keywords);
        $form.$editDialog.modal('show');
    };

    this.saveCart = function () {

        var id = $form.$listKeywords.getValue();

        $.each(rsKeywords, function (i, row) {
            if (row.id == id) {

                rsKeywords[i].keywords = $form.$editDialog.$editKeywords.getValue();

            }
        });

        this.renderEditCart();
        $form.$editDialog.modal('hide');

    };

    this.deleteFromCart = function () {
        var checkedId = $form.$listKeywords.getCheckedValues();
        if (checkedId.length <= 0)
            return;

        $msgbox.confirm('Delete selected data', function () {

            checkedId.forEach(function (indexId) {
                var i = -1;

                rsKeywords.forEach(function (data) {
                    i++;

                    if (data.id == indexId)
                        rsKeywords.splice(i, 1);

                });
            });

            this.renderEditCart();
        }.bind(this));

    };

};

dashboard.application.controllers.Seo.prototype =
    Object.create(dashboard.application.core.Controller.prototype);

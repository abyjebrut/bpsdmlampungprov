dashboard.application.controllers.Desktop = function () {
    var $el = $page.getScope('Desktop');

    this.init = function () {
        //        this.driveEvents();
        this.adjustLayout();
        $el.find('div').fadeIn('slow');
    };

    this.driveEvents = function () {
        $(window).on('resize', this.adjustLayout.bind(this));
    };

    this.adjustLayout = function () {
        $el.height($(window).innerHeight() - 120);
    };

    //    this.init();
};

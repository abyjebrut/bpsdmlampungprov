dashboard.application.controllers.RekapAjarWi = function () {
    var $el = $page.getScope('RekapAjarWi');
    dashboard.application.core.Controller.call(this, $el);

    var rs = [];
    var renderedRs = [];

    var rsRange = config.paging.range;

    var tableHeads = ['Widyaiswara', 'JML JP'];
    var tableFields = ['namaPengajar', 'totalJp'];

    var $tab;
    var $list;

    this.init = function () {
        // tab

        $tab = $el.find('#tab');
        $tab.$list = $tab.getRef('list');

        // list

        $list = $el.find('#list');
        $list.$pengajarDiklatId = $list.getRef('pengajarDiklatId').uiComboBox();
        $list.$jenisDiklatId = $list.getRef('jenisDiklatId').uiComboBox();
        $list.$tanggalMulai = $list.getRef('tanggalMulai').uiDatePicker();
        $list.$tanggalSelesai = $list.getRef('tanggalSelesai').uiDatePicker();
        $list.$search = $list.getRef('search').uiTextBox({ icon: 'search', placeholder: 'Search...' });
        $list.$table = $list.getRef('table').uiTable({ headers: tableHeads });
        $list.$paging = $list.getRef('paging').uiPaging();

        $list.$button = $list.find('#button');

        $list.$button.$excel = $list.$button.getRef('excel').uiButton();
        $list.$button.$refresh = $list.$button.getRef('refresh').uiButton();

    };

    this.driveEvents = function () {
        // tab

        // list
        $list.$tanggalMulai.on('change', this.readData.bind(this));
        $list.$jenisDiklatId.on('change', this.readData.bind(this));
        $list.$pengajarDiklatId.on('change', this.readData.bind(this));
        $list.$tanggalSelesai.on('change', this.readData.bind(this));
        $list.$search.on('keyup', this.searchData.bind(this));

        $list.$paging.on('click', this.moveData.bind(this));

        $list.$button.$refresh.on('click', this.loadData.bind(this));
        $list.$button.$excel.on('click', this.exportListToExcel.bind(this));

    };

    this.driveFieldSequence = function () {

    };

    this.reload = function () {
        $tab.$list.trigger('click');

    };

    this.loadData = function () {
        $list.$search.clear();
        var tgl1 = new Date();
        let tgl = tgl1.getFullYear() + '-' + (tgl1.getMonth() + 1) + '-' + tgl1.getDate()
        let tglLast = tgl1.getFullYear() + '-' + (tgl1.getMonth() + 2) + '-' + tgl1.getDate();
        $list.$tanggalMulai.setValue(tgl);
        $list.$tanggalSelesai.setValue(tglLast);
        $list.$jenisDiklatId.clear();
        $list.$pengajarDiklatId.clear();
        this.readData();
        $list.$pengajarDiklatId.renderApi('User/readDataPengajar', { text: 'name' });
        $list.$jenisDiklatId.renderApi('JenisDiklat/readData', { text: 'jenisDiklat' });
    };

    this.readData = function (callback) {

        if (!callback)
            $loading.show();

        var url = 'RekapAjar/readData';
        var data = {
            orderBy: 'namaPengajar',
            pengajarDiklatId: $list.$pengajarDiklatId.getValue(),
            jenisDiklatId: $list.$jenisDiklatId.getValue(),
            tanggalMulai: $list.$tanggalMulai.getValue(),
            tanggalSelesai: $list.$tanggalSelesai.getValue(),
            reverse: 2,
        };

        api.post(url, data, function (result) {

            rs = result.data;
            var rsCount = rs.length;

            if (rsCount > 0) {
                $list.$paging.show();
                $list.$button.$excel.show();
            }
            else {
                $list.$paging.hide();
                $list.$button.$excel.hide();
            }

            this.renderTableNoPaging($list.$table, rs, tableFields, 1000);

            if ($.isFunction(callback))
                callback();
            else
                $loading.hide();
        }.bind(this));
    };

    this.searchData = function () {
        var filteredRs = rs.filter(function (data) {
            var regex = new RegExp($list.$search.getValue(), 'gi');
            var foundSearch = false;

            tableFields.forEach(function (field) {
                if (data[field])
                    if (data[field].match(regex))
                        foundSearch = true;
            });

            return foundSearch;

        });
        renderedRs = this.renderTable($list.$table, filteredRs, tableFields, rsRange);
        this.renderTableNoPaging($list.$table, filteredRs, tableFields, 1000);
        if (renderedRs.length > 0) {
            $list.$button.$excel.show();
        }
        else {
            $list.$button.$excel.hide();
        }

    };

    this.moveData = function () {
        renderedRs = this.renderTable($list.$table, renderedRs, tableFields, rsRange, $list.$paging.getValue());
    };
    this.exportData = function (type, url, pengajarDiklatId, jenisDiklatId, jenisDiklat, tanggalMulai, tanggalSelesai) {

        $loading.show();
        var data = {
            pengajarDiklatId: pengajarDiklatId,
            jenisDiklatId: jenisDiklatId,
            jenisDiklat: jenisDiklat,
            tanggalMulai: tanggalMulai,
            tanggalSelesai: tanggalSelesai,
        };

        api.post(url, data, function (result) {

            $loading.hide();

            switch (result.status) {
                case 'success':

                    router.redirect(result.data);

                    break;
                case 'failed':
                    $msgbox.alert(result.message);
            }
        });
    };

    this.exportListToExcel = function () {
        var type = 'xls';
        var url = 'RekapAjar/createExcel';
        var pengajarDiklatId = $list.$pengajarDiklatId.getValue();
        var jenisDiklatId = $list.$jenisDiklatId.getValue();
        var jenisDiklat = $list.$jenisDiklatId.getText();
        var tanggalMulai = $list.$tanggalMulai.getValue();
        var tanggalSelesai = $list.$tanggalSelesai.getValue();

        this.exportData(type, url, pengajarDiklatId, jenisDiklatId, jenisDiklat, tanggalMulai, tanggalSelesai);
    };

};

dashboard.application.controllers.RekapAjarWi.prototype =
    Object.create(dashboard.application.core.Controller.prototype);

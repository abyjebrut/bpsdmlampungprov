dashboard.application.controllers.GuestBook = function () {
    var $el = $page.getScope('GuestBook');
    dashboard.application.core.Controller.call(this, $el);

    var rs = [];
    var renderedRs = [];

    var rsRange = config.paging.range;

    var tableHeads = ['NAME', 'EMAIL', 'SUBJECT', 'POSTED_ON'];
    var tableFields = ['name', 'email', 'subject', 'postedOn'];

    var saveMode;

    var $tab;
    var $list;
    var $preview;
    var $form;

    this.init = function () {

        // tab

        $tab = $el.find('#tab');
        $tab.$list = $tab.getRef('list');
        $tab.$preview = $tab.getRef('preview');
        $tab.$form = $tab.getRef('form');
        $tab.$new = $tab.getRef('new');

        // list

        $list = $el.find('#list');
        $list.$search = $list.getRef('search').uiTextBox({ icon: 'search', placeholder: 'Search...' });
        $list.$table = $list.getRef('table').uiTable({ headers: tableHeads });
        $list.$paging = $list.getRef('paging').uiPaging();

        $list.$button = $list.find('#button');
        $list.$button.$delete = $list.$button.getRef('delete').uiButton();
        $list.$button.$detail = $list.$button.getRef('detail').uiButton();

        $list.$button.$refresh = $list.$button.getRef('refresh').uiButton();
        $list.$button.$edit = $list.$button.getRef('edit').uiButton();
        $list.$button.$pdf = $list.$button.getRef('pdf').uiButton();
        $list.$button.$excel = $list.$button.getRef('excel').uiButton();

        // preview

        $preview = $el.find('#preview');
        $preview.$name = $preview.getRef('name').uiTextView();
        $preview.$email = $preview.getRef('email').uiTextView();
        $preview.$subject = $preview.getRef('subject').uiTextView();
        $preview.$message = $preview.getRef('message').uiHTMLView();
        $preview.$postedOn = $preview.getRef('postedOn').uiTextView();

        $preview.$button = $preview.find('#button');
        $preview.$button.$edit = $preview.$button.getRef('edit').uiButton();
        $preview.$button.$cancel = $preview.$button.getRef('cancel').uiButton();
        $preview.$button.$pdf = $preview.$button.getRef('pdf').uiButton();
        $preview.$button.$excel = $preview.$button.getRef('excel').uiButton();

        // form

        $form = $el.find('#form').uiForm();
        $form.$name = $form.getRef('name').uiTextBox();
        $form.$email = $form.getRef('email').uiTextBox();
        $form.$subject = $form.getRef('subject').uiTextBox();
        $form.$message = $form.getRef('message').uiTextArea();

        $form.$button = $form.find('#button');
        $form.$button.$save = $form.$button.getRef('save').uiButton();
        $form.$button.$cancel = $form.$button.getRef('cancel').uiButton();
    };

    this.driveEvents = function () {
        // tab

        $tab.$preview.on('click', this.detailData.bind(this));
        $tab.$form.on('click', this.editData.bind(this));
        $tab.$new.on('click', this.newData.bind(this));

        // list

        $list.$search.on('keyup', this.searchData.bind(this));

        $list.$table.on('dblclick', function () {
            $tab.$preview.trigger('click');
        });

        $list.$paging.on('click', this.moveData.bind(this));
        $list.$button.$delete.on('click', this.deleteData.bind(this));

        $list.$button.$detail.on('click', function () {
            $tab.$preview.trigger('click');
        });

        $list.$button.$edit.on('click', function () {
            $tab.$form.trigger('click');
        });

        $list.$button.$refresh.on('click', this.readData.bind(this));
        $list.$button.$pdf.on('click', this.exportListToPdf.bind(this));
        $list.$button.$excel.on('click', this.exportListToExcel.bind(this));

        // preview

        $preview.$button.$edit.on('click', function () {
            $tab.$form.trigger('click');
        });

        $preview.$button.$cancel.on('click', function () {
            $tab.$list.trigger('click');
        });

        $preview.$button.$pdf.on('click', this.exportDetailToPdf.bind(this));
        $preview.$button.$excel.on('click', this.exportDetailToExcel.bind(this));

        // form

        $form.$button.$save.on('click', this.saveData.bind(this));

        $form.$button.$cancel.on('click', function () {
            $tab.$list.trigger('click');
        });

    };

    this.driveFieldSequence = function () {

        $form.$name.on('enter', function () {
            $form.$email.focus();
        });

        $form.$email.on('enter', function () {
            $form.$subject.focus();
        });

        $form.$subject.on('enter', function () {
            $form.$message.focus();
        });
    };

    this.reload = function () {
        $tab.$list.trigger('click');
    };

    this.readData = function (callback) {

        if (!callback)
            $loading.show();

        var url = 'GuestBook/readData';
        var data = {
            orderBy: 'id',
            reverse: 1,
        };

        api.post(url, data, function (result) {

            rs = result.data;
            var rsCount = rs.length;

            if (rsCount > 0) {
                var showButtons = true;
                $tab.$preview.show();
                $tab.$form.show();
            }
            else {
                var showButtons = false;
                $tab.$preview.hide();
                $tab.$form.hide();
            }

            $list.$button.$delete.setEnabled(showButtons);
            $list.$button.$detail.setEnabled(showButtons);
            $list.$button.$edit.setEnabled(showButtons);

            renderedRs = this.renderTable($list.$table, rs, tableFields, rsRange);
            this.renderPaging($list.$paging, renderedRs, rsRange);

            if ($.isFunction(callback))
                callback();
            else
                $loading.hide();
        }.bind(this));
    };

    this.searchData = function () {

        var filteredRs = rs.filter(function (data) {
            var regex = new RegExp($list.$search.getValue(), 'gi');
            var found = false;

            tableFields.forEach(function (field) {
                if (data[field])
                    if (data[field].match(regex))
                        found = true;
            });

            return found;
        });

        renderedRs = this.renderTable($list.$table, filteredRs, tableFields, rsRange);
        this.renderPaging($list.$paging, renderedRs, rsRange);
    };

    this.moveData = function () {
        renderedRs = this.renderTable($list.$table, renderedRs, tableFields, rsRange, $list.$paging.getValue());
    };

    this.deleteData = function () {
        var checkedId = $list.$table.getCheckedValues();

        if (checkedId.length == 0) {
            $msgbox.alert('NO_DATA_CHECKED');
            return;
        };

        var url = 'GuestBook/multipleDeleteData';
        var data = {
            multipleId: checkedId,
        };

        $msgbox.confirm('DELETE_CHECKED_DATA', function () {
            api.post(url, data, function (result) {

                switch (result.status) {
                    case 'success':
                        $splash.show(result.message);
                        this.readData();

                        break;
                    case 'failed':
                        $msgbox.alert(result.message);
                }

            }.bind(this));
        }.bind(this));

    };

    this.detailData = function () {
        $loading.show();

        var url = 'GuestBook/detailData';
        var data = {
            id: $list.$table.getValue(),
        };

        api.post(url, data, function (result) {

            if (result.status == 'success') {
                $preview.$name.setValue(result.data.name);
                $preview.$email.setValue(result.data.email);
                $preview.$subject.setValue(result.data.subject);
                $preview.$message.setValue(result.data.message);
                $preview.$postedOn.setValue(result.data.postedOn);
            }

            $loading.hide();
        });
    };

    this.serializeData = function () {
        var data = {
            name: $form.$name.getValue(),
            email: $form.$email.getValue(),
            subject: $form.$subject.getValue(),
            message: $form.$message.getValue(),
        };

        if (saveMode == EDIT_MODE)
            data.id = $list.$table.getValue();

        return data;
    };

    this.clearData = function () {
        $form.clear();

        $form.$name.clear();
        $form.$email.clear();
        $form.$subject.clear();
        $form.$message.clear();

    };

    this.editData = function () {
        $loading.show();
        saveMode = EDIT_MODE;

        this.clearData();

        var url = 'GuestBook/detailData';
        var data = {
            id: $list.$table.getValue()
        };

        api.post(url, data, function (result) {

            if (result.status == 'success') {
                $form.$name.setValue(result.data.name);
                $form.$email.setValue(result.data.email);
                $form.$subject.setValue(result.data.subject);
                $form.$message.setValue(result.data.message);

                $form.$name.focus();
            }

            $loading.hide();
        });
    };

    this.newData = function () {
        saveMode = NEW_MODE;

        this.clearData();
        $form.$name.focus();
    };

    this.saveData = function () {
        $form.$button.$save.loading();

        switch (saveMode) {
            case EDIT_MODE:
                var url = 'GuestBook/updateData';
                break;
            case NEW_MODE:
                var url = 'GuestBook/createData';
        }

        var data = this.serializeData();

        api.post(url, data, function (result) {

            switch (result.status) {
                case 'success':
                    $splash.show(result.message);

                    this.readData(function () {
                        switch (saveMode) {
                            case EDIT_MODE:
                                $tab.$list.trigger('click');
                                break;
                            case NEW_MODE:
                                this.clearData();
                                $form.$name.focus();
                        }

                        $form.$button.$save.release();
                        window.scrollTo(0, 0);
                    }.bind(this));

                    break;
                case 'invalid':
                    $form.error(result);
                    $form.$button.$save.release();
            }

        }.bind(this));
    };

    this.exportData = function (type, url, id) {

        $loading.show();

        var data = {};
        if (id)
            data.id = id;

        api.post(url, data, function (result) {

            $loading.hide();

            switch (result.status) {
                case 'success':

                    if (type == 'pdf')
                        router.open(result.data);
                    else
                        router.redirect(result.data);

                    break;
                case 'failed':
                    $msgbox.alert(result.message);
            }
        });
    };

    this.exportListToPdf = function () {

        var type = 'pdf';
        var url = 'GuestBook/createPDFList';

        this.exportData(type, url);
    };

    this.exportDetailToPdf = function () {
        var type = 'pdf';
        var url = 'GuestBook/createPDFList';
        var id = $list.$table.getValue();

        this.exportData(type, url, id);
    };

    this.exportListToExcel = function () {
        var type = 'xls';
        var url = 'GuestBook/createExcelList';

        this.exportData(type, url);
    };

    this.exportDetailToExcel = function () {
        var type = 'xls';
        var url = 'GuestBook/createExcelDetail';
        var id = $list.$table.getValue();

        this.exportData(type, url, id);
    };

};

dashboard.application.controllers.GuestBook.prototype =
    Object.create(dashboard.application.core.Controller.prototype);

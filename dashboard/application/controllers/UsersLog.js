dashboard.application.controllers.UsersLog = function () {
    var $el = $page.getScope('UsersLog');
    dashboard.application.core.Controller.call(this, $el);

    var rs = [];
    var renderedRs = [];

    var rsRange = config.paging.range;

    var tableHeads = ['NAME', 'LOGIN', 'LOGOUT', 'STILL_INSIDE'];
    var tableFields = ['name', 'loginOn', 'logoutOn', 'stillInside'];

    var saveMode;

    var $tab;
    var $list;
    var $preview;

    this.init = function () {

        // tab

        $tab = $el.find('#tab');
        $tab.$list = $tab.getRef('list');
        $tab.$preview = $tab.getRef('preview');

        // list

        $list = $el.find('#list');
        $list.$search = $list.getRef('search').uiTextBox({ icon: 'search', placeholder: 'Search...' });
        $list.$table = $list.getRef('table').uiTable({ headers: tableHeads });
        $list.$paging = $list.getRef('paging').uiPaging();

        $list.$button = $list.find('#button');
        $list.$button.$detail = $list.$button.getRef('detail').uiButton();

        $list.$button.$refresh = $list.$button.getRef('refresh').uiButton();

        // preview

        $preview = $el.find('#preview');
        $preview.$name = $preview.getRef('name').uiTextView();
        $preview.$loginOn = $preview.getRef('loginOn').uiTextView();
        $preview.$logoutOn = $preview.getRef('logoutOn').uiTextView();
        $preview.$stillInside = $preview.getRef('stillInside').uiTextView();

        $preview.$button = $preview.find('#button');
        $preview.$button.$cancel = $preview.$button.getRef('cancel').uiButton();

    };

    this.driveEvents = function () {
        // tab

        $tab.$preview.on('click', this.detailData.bind(this));

        // list

        $list.$search.on('keyup', this.searchData.bind(this));

        $list.$table.on('dblclick', function () {
            if (!$list.$search.getValue()) {
                if (rs.length > 0)
                    $tab.$preview.trigger('click');
            } else {
                if (renderedRs.length > 0)
                    $tab.$preview.trigger('click');
            }
        });

        $list.$paging.on('click', this.moveData.bind(this));

        $list.$button.$detail.on('click', function () {
            $tab.$preview.trigger('click');
        });

        $list.$button.$refresh.on('click', this.readData.bind(this));

        // preview

        $preview.$button.$cancel.on('click', function () {
            $tab.$list.trigger('click');
        });

    };

    this.driveFieldSequence = function () {


    };

    this.reload = function () {
        $tab.$list.trigger('click');

    };

    this.readData = function (callback) {

        if (!callback)
            $loading.show();

        var url = 'UserLog/readData';
        var data = {
            orderBy: 'userLoginId',
            reverse: 1,
        };

        api.post(url, data, function (result) {

            rs = result.data;
            var rsCount = rs.length;

            if (rsCount > 0) {
                this.dataShow();
            }
            else {
                this.dataHide();
            }

            renderedRs = this.renderTable($list.$table, rs, tableFields, rsRange);
            this.renderPaging($list.$paging, renderedRs, rsRange);

            if ($.isFunction(callback))
                callback();
            else
                $loading.hide();
        }.bind(this));
    };

    this.dataHide = function () {
        $list.$paging.hide();
        $tab.$preview.hide();
        $list.$button.$detail.hide();
    };

    this.dataShow = function () {
        $list.$paging.show();
        $tab.$preview.show();
        $list.$button.$detail.show();
    };

    this.searchData = function () {

        var filteredRs = rs.filter(function (data) {
            var regex = new RegExp($list.$search.getValue(), 'gi');
            var found = false;

            tableFields.forEach(function (field) {
                if (data[field])
                    if (data[field].match(regex))
                        found = true;
            });

            return found;
        });

        renderedRs = this.renderTable($list.$table, filteredRs, tableFields, rsRange);
        if (renderedRs.length > 0) {
            this.renderPaging($list.$paging, renderedRs, rsRange);
            this.dataShow();
        } else {
            this.dataHide();
        }
    };

    this.moveData = function () {
        renderedRs = this.renderTable($list.$table, renderedRs, tableFields, rsRange, $list.$paging.getValue());
    };

    this.detailData = function () {
        $loading.show();

        var url = 'UserLog/detailData';
        var data = {
            userLoginId: $list.$table.getValue(),
        };

        api.post(url, data, function (result) {

            if (result.status == 'success') {
                $preview.$name.setValue(result.data.name);
                $preview.$loginOn.setValue(result.data.loginOn);
                $preview.$logoutOn.setValue(result.data.logoutOn);
                $preview.$stillInside.setValue(result.data.stillInside);
            }

            $loading.hide();
        });
    };


};

dashboard.application.controllers.UsersLog.prototype =
    Object.create(dashboard.application.core.Controller.prototype);

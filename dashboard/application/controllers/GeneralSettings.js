dashboard.application.controllers.GeneralSettings = function () {
    var $el = $page.getScope('GeneralSettings');
    dashboard.application.core.Controller.call(this, $el);

    var $tab;
    var $preview;
    var $form;

    this.init = function () {

        // tab

        $tab = $el.find('#tab');
        $tab.$form = $tab.getRef('form');

        // form

        $form = $el.find('#form').uiForm();

        $form.$website_address = $form.getRef('website_address').uiTextBox();
        $form.$api_address = $form.getRef('api_address').uiTextView();

        $form.$button = $form.find('#button');

        $form.$button.$apply = $form.$button.getRef('apply').uiButton();
        $form.$button.$reset = $form.$button.getRef('reset').uiButton();
    };

    this.driveEvents = function () {
        // tab

        $tab.$form.on('click', this.editData.bind(this));

        // form

        $form.$button.$apply.on('click', this.applyData.bind(this));
        $form.$button.$reset.on('click', this.resetData.bind(this));

    };

    this.driveFieldSequence = function () {


        $form.$website_address.on('enter', function () {
            $form.$api_address.focus();
        });

        $form.$api_address.on('enter', function () {
            $form.$button.$apply.trigger('click');
        });
    };

    this.reload = function () {
        $tab.$form.trigger('click');

        if (localStorage.userType == 'admin') {
            $form.$button.$apply.setEnabled(true);
            $form.$button.$reset.setEnabled(true);
        }
        else {
            $form.$button.$apply.setEnabled(false);
            $form.$button.$reset.setEnabled(false);
        }
    };

    this.clearData = function () {
        $form.clear();

        $form.$website_address.clear();
        $form.$api_address.clear();

    };

    this.editData = function () {
        $form.$website_address.setValue(localStorage.urlSite);
        $form.$api_address.setValue(config.url.api);
    };

    this.applyData = function () {

        $msgbox.confirm('APPLY_SETTINGS', function () {
            localStorage.urlSite = $form.$website_address.getValue();
            window.location.reload();
        });
    };

    this.resetData = function () {

        $msgbox.confirm('RESET_SETTINGS', function () {

            $form.$website_address.setValue(config.url.site);
        }.bind(this));

    };

};

dashboard.application.controllers.GeneralSettings.prototype =
Object.create(dashboard.application.core.Controller.prototype);

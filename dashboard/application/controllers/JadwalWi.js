dashboard.application.controllers.JadwalWi = function () {
    var $el = $page.getScope('JadwalWi');
    dashboard.application.core.Controller.call(this, $el);

    var rs = [];
    var renderedRs = [];

    var rsRange = config.paging.range;

    var tableHeads = ['Nama Diklat', 'Tanggal', 'Waktu', 'Mapel', 'Lokasi'];
    var tableFields = ['namaDiklat', 'tanggal', 'waktu', 'mataPelajaran', 'lokasi'];

    var $tab;
    var $list;
    var monthRecords = [
        'Jan',
        'Feb',
        'Mar',
        'Apr',
        'Mei',
        'Jun',
        'Jul',
        'Ags',
        'Sept',
        'Okt',
        'Nov',
        'Dec',
    ]

    var hariRecords = [
        'Minggu',
        'Senin',
        'Selasa',
        'Rabu',
        'Kamis',
        'Jum`at',
        'Sabtu',
    ]

    this.init = function () {

        // tab

        $tab = $el.find('#tab');
        $tab.$list = $tab.getRef('list');

        // list

        $list = $el.find('#list');
        $list.$jenisDiklatId = $list.getRef('jenisDiklatId').uiComboBox();
        $list.$tanggalMulai = $list.getRef('tanggalMulai').uiDatePicker();
        $list.$tanggalSelesai = $list.getRef('tanggalSelesai').uiDatePicker();
        $list.$search = $list.getRef('search').uiTextBox({ icon: 'search', placeholder: 'Search...' });
        $list.$table = $list.getRef('table').uiTable({ headers: tableHeads });
        $list.$paging = $list.getRef('paging').uiPaging();

        $list.$button = $list.find('#button');

        $list.$button.$excel = $list.$button.getRef('excel').uiButton();
        $list.$button.$refresh = $list.$button.getRef('refresh').uiButton();

    };

    this.driveEvents = function () {


        // list

        $list.$tanggalMulai.on('change', this.readData.bind(this));
        $list.$tanggalSelesai.on('change', this.readData.bind(this));
        $list.$jenisDiklatId.on('change', this.readData.bind(this));
        $list.$search.on('keyup', this.searchData.bind(this));

        $list.$button.$excel.on('click', this.exportListToExcel.bind(this));

        $list.$paging.on('click', this.moveData.bind(this));

        $list.$button.$refresh.on('click', this.reload.bind(this));
    };

    this.driveFieldSequence = function () {

    };

    this.reload = function () {
        $tab.$list.trigger('click');
        $list.$search.clear();
        var tgl1 = new Date();
        let tgl = tgl1.getFullYear() + '-' + (tgl1.getMonth() + 1) + '-' + tgl1.getDate()
        let tglLast = tgl1.getFullYear() + '-' + (tgl1.getMonth() + 2) + '-' + tgl1.getDate();
        $list.$tanggalMulai.setValue(tgl);
        $list.$tanggalSelesai.setValue(tglLast);
        $list.$jenisDiklatId.clear();

        this.readData();

        $list.$jenisDiklatId.renderApi('JenisDiklat/readData', { text: 'jenisDiklat' });
    };

    this.readData = function (callback) {


        if (!callback)
            $loading.show();

        var url = 'JadwalWi/readData';
        var data = {
            orderBy: 'tanggal,jamMulai',
            pengajarDiklatId: localStorage.userId,
            jenisDiklatId: $list.$jenisDiklatId.getValue(),
            tanggalMulai: $list.$tanggalMulai.getValue(),
            tanggalSelesai: $list.$tanggalSelesai.getValue(),
            reverse: 2,
        };

        api.post(url, data, function (result) {
            rs = [];
            result.data.forEach(row1 => {
                var lokasi = '';
                result.extra.rsJadwal.forEach(row2 => {
                    if (row2.id == row1.bahanJadwalDiklatId) {
                        lokasi = row2.lokasi;
                    }
                });

                var tgl = new Date(row1.tanggal)
                var hari = hariRecords[tgl.getDay()]
                var bulan = monthRecords[tgl.getMonth()]
                var day1 = tgl.getDate();
                var bulan1 = (tgl.getMonth() + 1);
                var tahun1 = tgl.getFullYear();
                var tanggalJamMulai = hari + ', ' + day1 + '-' + bulan + '-' + tahun1
                var waktu = '';
                var aWaktu = row1.tipeWaktuId;
                if (aWaktu == 1) {
                    waktu = row1.jamMulai + ' s/d ' + row1.jamSelesai;
                } else {
                    waktu = row1.jumlahHari + ' hari';
                }

                rs.push({
                    id: row1.id,
                    jenisDiklat: row1.jenisDiklat,
                    namaDiklat: row1.namaDiklat,
                    mataPelajaran: row1.mataPelajaran,
                    tanggal: tanggalJamMulai,
                    waktu: waktu,
                    lokasi: lokasi,
                });

            });

            var rsCount = rs.length;
            if (rsCount > 0) {
                this.dataShow();
            }
            else {
                this.dataHide();
            }

            renderedRs = this.renderTable($list.$table, rs, tableFields, rsRange);
            this.renderPaging($list.$paging, renderedRs, rsRange);

            if ($.isFunction(callback))
                callback();
            else
                $loading.hide();
        }.bind(this));
    };

    this.dataHide = function () {
        $list.$paging.hide();
        $list.$button.$excel.hide();
    };

    this.dataShow = function () {
        $list.$paging.show();
        $list.$button.$excel.show();
    };

    this.searchData = function () {

        var filteredRs = rs.filter(function (data) {
            var regex = new RegExp($list.$search.getValue(), 'gi');
            var found = false;

            tableFields.forEach(function (field) {
                if (data[field])
                    if (data[field].match(regex))
                        found = true;
            });

            return found;
        });

        renderedRs = this.renderTable($list.$table, filteredRs, tableFields, rsRange);
        if (renderedRs.length > 0) {
            this.renderPaging($list.$paging, renderedRs, rsRange);
            this.dataShow();
        } else {
            this.dataHide();
        }
    };

    this.moveData = function () {
        renderedRs = this.renderTable($list.$table, renderedRs, tableFields, rsRange, $list.$paging.getValue());
    };

    this.exportData = function (type, url, pengajarDiklatId, jenisDiklatId, jenisDiklat, tanggalMulai, tanggalSelesai) {

        $loading.show();
        var data = {
            pengajarDiklatId: pengajarDiklatId,
            jenisDiklatId: jenisDiklatId,
            jenisDiklat: jenisDiklat,
            tanggalMulai: tanggalMulai,
            tanggalSelesai: tanggalSelesai,
        };

        api.post(url, data, function (result) {

            $loading.hide();
            switch (result.status) {
                case 'success':
                    router.redirect(result.data);
                    break;
                case 'failed':
                    $msgbox.alert(result.message);
            }
        });
    };

    this.exportListToExcel = function () {
        var type = 'xls';
        var url = 'JadwalWi/createExcel';
        var pengajarDiklatId = localStorage.userId;
        var jenisDiklatId = $list.$jenisDiklatId.getValue();
        var jenisDiklat = $list.$jenisDiklatId.getText();
        var tanggalMulai = $list.$tanggalMulai.getValue();
        var tanggalSelesai = $list.$tanggalSelesai.getValue();

        this.exportData(type, url, pengajarDiklatId, jenisDiklatId, jenisDiklat, tanggalMulai, tanggalSelesai);
    };

};

dashboard.application.controllers.JadwalWi.prototype =
    Object.create(dashboard.application.core.Controller.prototype);

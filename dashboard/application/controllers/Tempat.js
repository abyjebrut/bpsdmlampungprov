dashboard.application.controllers.Tempat = function () {
    var $el = $page.getScope('Tempat');
    dashboard.application.core.Controller.call(this, $el);

    var rs = [];
    var renderedRs = [];

    var rsRange = config.paging.range;

    var tableHeads = ['Tempat'];
    var tableFields = ['tempat'];

    var saveMode;

    var $tab;
    var $list;
    var $preview;
    var $form;

    this.init = function () {

        // tab

        $tab = $el.find('#tab');
        $tab.$list = $tab.getRef('list');
        $tab.$preview = $tab.getRef('preview');
        $tab.$form = $tab.getRef('form');
        $tab.$new = $tab.getRef('new');

        // list

        $list = $el.find('#list');

        $list.$filter = $list.getRef('filter').uiComboBox({placeholder: 'Filter By Lokasi'});
        $list.$search = $list.getRef('search').uiTextBox({icon: 'search', placeholder: 'Search...'});
        $list.$table = $list.getRef('table').uiTable({headers: tableHeads});
        $list.$paging = $list.getRef('paging').uiPaging();

        $list.$button = $list.find('#button');
        $list.$button.$delete = $list.$button.getRef('delete').uiButton();
        $list.$button.$detail = $list.$button.getRef('detail').uiButton();

        $list.$button.$refresh = $list.$button.getRef('refresh').uiButton();
        $list.$button.$edit = $list.$button.getRef('edit').uiButton();
        

        // preview

        $preview = $el.find('#preview');

        $preview.$lokasi = $preview.getRef('lokasi').uiTextView();
        $preview.$tempat = $preview.getRef('tempat').uiTextView();

        $preview.$button = $preview.find('#button');
        $preview.$button.$edit = $preview.$button.getRef('edit').uiButton();
        $preview.$button.$cancel = $preview.$button.getRef('cancel').uiButton();
        

        // form

        $form = $el.find('#form').uiForm();

        $form.$lokasiId = $form.getRef('lokasiId').uiComboBox();
        $form.$tempat = $form.getRef('tempat').uiTextBox();

        $form.$button = $form.find('#button');
        $form.$button.$save = $form.$button.getRef('save').uiButton();
        $form.$button.$cancel = $form.$button.getRef('cancel').uiButton();

    };

    this.driveEvents = function () {
        // tab

        $tab.$preview.on('click', this.detailData.bind(this));
        $tab.$form.on('click', this.editData.bind(this));
        $tab.$new.on('click', this.newData.bind(this));

        // list

        $list.$filter.on('change', this.readData.bind(this));
        $list.$search.on('keyup', this.searchData.bind(this));

        $list.$table.on('dblclick', function () {
            $tab.$preview.trigger('click');
        });

        $list.$paging.on('click', this.moveData.bind(this));
        $list.$button.$delete.on('click', this.deleteData.bind(this));

        $list.$button.$detail.on('click', function () {
            $tab.$preview.trigger('click');
        });

        $list.$button.$edit.on('click', function () {
            $tab.$form.trigger('click');
        });

        $list.$button.$refresh.on('click', this.reload.bind(this));
        
        
        // preview

        $preview.$button.$edit.on('click', function () {
            $tab.$form.trigger('click');
        });

        $preview.$button.$cancel.on('click', function () {
            $tab.$list.trigger('click');
        });
        
        // form

        $form.$button.$save.on('click', this.saveData.bind(this));

        $form.$button.$cancel.on('click', function () {
            $tab.$list.trigger('click');
        });

    };

    this.driveFieldSequence = function () {

        $form.$tempat.on('enter', function () {
            $form.$button.$save.trigger('click');
        });

    };

    this.reload = function () {
        $tab.$list.trigger('click');

        $list.$filter.clear();
        this.readData();

        $list.$filter.renderApi('Lokasi/readData', {text: 'lokasi'});
        $form.$lokasiId.renderApi('Lokasi/readData', {text: 'lokasi'});
    };

    this.readData = function (callback) {

        if (!callback)
            $loading.show();

        var url = 'Tempat/readData';
        var data = {
            orderBy: 'tempat',
            lokasiId: $list.$filter.getValue(),
        };

        api.post(url, data, function (result) {

            rs = result.data;
            var rsCount = rs.length;

            if (rsCount > 0) {
                var showButtons = true;
                $tab.$preview.show();
                $tab.$form.show();
            }
            else  {
                var showButtons = false;
                $tab.$preview.hide();
                $tab.$form.hide();
            }

            $list.$button.$delete.setEnabled(showButtons);
            $list.$button.$detail.setEnabled(showButtons);
            $list.$button.$edit.setEnabled(showButtons);

            renderedRs = this.renderTable($list.$table, rs, tableFields, rsRange);
            this.renderPaging($list.$paging, renderedRs, rsRange);

            if ($.isFunction(callback))
                callback();
            else
                $loading.hide();
        }.bind(this));
    };

    this.searchData = function () {

        var filteredRs = rs.filter(function (data) {
            var regex = new RegExp($list.$search.getValue(), 'gi');
            var found = false;

            tableFields.forEach(function (field) {
                if (data[field])
                    if (data[field].match(regex))
                        found = true;
            });

            return found;
        });

        renderedRs = this.renderTable($list.$table, filteredRs, tableFields, rsRange);
        this.renderPaging($list.$paging, renderedRs, rsRange);
    };

    this.moveData = function () {
        renderedRs = this.renderTable($list.$table, renderedRs, tableFields, rsRange, $list.$paging.getValue());
    };

    this.deleteData = function () {
        var checkedId = $list.$table.getCheckedValues();

        if (checkedId.length == 0) {
            $msgbox.alert('NO_DATA_CHECKED');
            return;
        };

        var url = 'Tempat/multipleDeleteData';
        var data = {
            multipleId: checkedId,
        };

        $msgbox.confirm('DELETE_CHECKED_DATA', function () {
            api.post(url, data, function (result) {

                switch (result.status) {
                case 'success':
                    $splash.show(result.message);
                    this.readData();

                    break;
                case 'failed':
                    $msgbox.alert(result.message);
                }

            }.bind(this));
        }.bind(this));

    };

    this.detailData = function () {
        $loading.show();

        var url = 'Tempat/detailData';
        var data = {
            id: $list.$table.getValue()
        };

        api.post(url, data, function (result) {

            if (result.status == 'success') {
                $preview.$lokasi.setValue(result.data.lokasi);
                $preview.$tempat.setValue(result.data.tempat);
            }

            $loading.hide();
        });
    };

    this.serializeData = function () {
        var data = {
            lokasiId: $form.$lokasiId.getValue(),
            tempat: $form.$tempat.getValue(),
        };

        if (saveMode == EDIT_MODE)
            data.id = $list.$table.getValue();

        return data;
    };

    this.clearData = function () {
        $form.clear();

        $form.$tempat.clear();
    };

    this.editData = function () {
        $loading.show();
        saveMode = EDIT_MODE;

        this.clearData();

        var url = 'Tempat/detailData';
        var data = {
            id: $list.$table.getValue()
        };

        api.post(url, data, function (result) {

            if (result.status == 'success') {
                $form.$lokasiId.setValue(result.data.lokasiId);
                $form.$tempat.setValue(result.data.tempat);

                $form.$tempat.focus();
            }

            $loading.hide();
        });
    };

    this.newData = function () {
        saveMode = NEW_MODE;

        this.clearData();
        $form.$lokasiId.setValue($list.$filter.getValue());

        $form.$tempat.focus();
    };

    this.saveData = function () {
        $form.$button.$save.loading();

        switch (saveMode) {
        case EDIT_MODE:
            var url = 'Tempat/updateData';
            break;
        case NEW_MODE:
            var url = 'Tempat/createData';
        }

        var data = this.serializeData();

        api.post(url, data, function (result) {

            switch (result.status) {
            case 'success':
                $splash.show(result.message);

                this.readData(function () {
                    switch (saveMode) {
                    case EDIT_MODE:
                        $tab.$list.trigger('click');
                        break;
                    case NEW_MODE:
                        this.clearData();
                        $form.$tempat.focus();
                    }

                    $form.$button.$save.release();
                    window.scrollTo(0, 0);
                }.bind(this));

                break;
            case 'invalid':
                $form.error(result);
                $form.$button.$save.release();
            }

        }.bind(this));
    };


};

dashboard.application.controllers.Tempat.prototype =
Object.create(dashboard.application.core.Controller.prototype);

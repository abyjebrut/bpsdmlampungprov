dashboard.application.controllers.Anggota = function () {
    var $el = $page.getScope('Anggota');
    dashboard.application.core.Controller.call(this, $el);

    var rs = [];
    var renderedRs = [];

    var rsRange = config.paging.range;

    var tableHeads = ['NIP', 'NAMA', 'EMAIL', 'STATUS_AKUN'];
    var tableFields = ['nip', 'nama', 'email', 'statusAkun'];

    var saveMode;
    var lockShowPrivileges = false;

    var $tab;
    var $list;
    var $preview;
    var $form;
    var $change_password;

    this.init = function () {

        // tab

        $tab = $el.find('#tab');
        $tab.$list = $tab.getRef('list');
        $tab.$preview = $tab.getRef('preview');
        $tab.$form = $tab.getRef('form');
        $tab.$new = $tab.getRef('new');
        $tab.$change_password = $tab.getRef('change_password');

        // list

        $list = $el.find('#list');
        $list.$unitKerjaId = $list.getRef('unitKerjaId').uiComboBox({ placeholder: 'Filter By Unit Kerja' });
        $list.$instansiId = $list.getRef('instansiId').uiComboBox({ placeholder: 'Filter By Instansi' });
        $list.$statusAkunId = $list.getRef('statusAkunId').uiComboBox({ placeholder: 'Filter By Status Akun' });
        $list.$search = $list.getRef('search').uiTextBox({ icon: 'search', placeholder: 'Search...' });
        $list.$table = $list.getRef('table').uiTable({ headers: tableHeads });
        $list.$paging = $list.getRef('paging').uiPaging();

        $list.$button = $list.find('#button');
        $list.$button.$delete = $list.$button.getRef('delete').uiButton();
        $list.$button.$detail = $list.$button.getRef('detail').uiButton();

        $list.$button.$refresh = $list.$button.getRef('refresh').uiButton();
        $list.$button.$edit = $list.$button.getRef('edit').uiButton();
        $list.$button.$pdf = $list.$button.getRef('pdf').uiButton();

        // preview

        $preview = $el.find('#preview');
        $preview.$nip = $preview.getRef('nip').uiTextView();
        $preview.$nama = $preview.getRef('nama').uiTextView();
        $preview.$nik = $preview.getRef('nik').uiTextView();
        $preview.$npwp = $preview.getRef('npwp').uiTextView();
        $preview.$foto = $preview.getRef('foto').uiImageView();
        $preview.$email = $preview.getRef('email').uiTextView();
        $preview.$pangkatGolongan = $preview.getRef('pangkatGolongan').uiTextView();
        $preview.$jabatanEselon = $preview.getRef('jabatanEselon').uiTextView();
        $preview.$unitKerja = $preview.getRef('unitKerja').uiTextView();
        $preview.$instansi = $preview.getRef('instansi').uiTextView();
        $preview.$instansiLainnya = $preview.getRef('instansiLainnya').uiTextView();
        $preview.$pendidikanTerakhir = $preview.getRef('pendidikanTerakhir').uiTextView();
        $preview.$gender = $preview.getRef('gender').uiTextView();
        $preview.$tempatLahir = $preview.getRef('tempatLahir').uiTextView();
        $preview.$tanggalLahir = $preview.getRef('tanggalLahir').uiTextView();
        $preview.$agama = $preview.getRef('agama').uiTextView();
        $preview.$alamat = $preview.getRef('alamat').uiTextAreaView();
        $preview.$telpHp = $preview.getRef('telpHp').uiTextView();
        $preview.$registeredOn = $preview.getRef('registeredOn').uiTextView();
        $preview.$statusAkun = $preview.getRef('statusAkun').uiTextView();
        $preview.$activeOn = $preview.getRef('activeOn').uiTextView();


        $preview.$button = $preview.find('#button');
        $preview.$button.$edit = $preview.$button.getRef('edit').uiButton();
        $preview.$button.$cancel = $preview.$button.getRef('cancel').uiButton();
        $preview.$button.$pdf = $preview.$button.getRef('pdf').uiButton();

        // form

        $form = $el.find('#form').uiForm();
        $form.$nip = $form.getRef('nip').uiTextBox();
        $form.$nama = $form.getRef('nama').uiTextBox();
        $form.$nik = $form.getRef('nik').uiTextBox();
        $form.$npwp = $form.getRef('npwp').uiTextBox();
        $form.$email = $form.getRef('email').uiTextBox();
        $form.$password = $form.getRef('password').uiTextBox({ type: TYPE_PASSWORD });
        $form.$retype_password = $form.getRef('retype_password').uiTextBox({ type: TYPE_PASSWORD });
        $form.$foto = $form.getRef('foto').uiUploadImage();
        $form.$pangkatGolonganId = $form.getRef('pangkatGolonganId').uiComboBox();
        $form.$jabatanEselon = $form.getRef('jabatanEselon').uiTextBox();
        $form.$unitKerjaId = $form.getRef('unitKerjaId').uiComboBox();
        $form.$instansiId = $form.getRef('instansiId').uiComboBox();
        $form.$instansiLainnya = $form.getRef('instansiLainnya').uiTextBox();
        $form.$pendidikanTerakhirId = $form.getRef('pendidikanTerakhirId').uiComboBox();
        $form.$jenisKelaminId = $form.getRef('jenisKelaminId').uiRadioBox();
        $form.$tempatLahir = $form.getRef('tempatLahir').uiTextBox();
        $form.$tanggalLahir = $form.getRef('tanggalLahir').uiDatePicker();
        $form.$agamaId = $form.getRef('agamaId').uiRadioBox();
        $form.$alamat = $form.getRef('alamat').uiTextArea();
        $form.$telpHp = $form.getRef('telpHp').uiTextBox();
        $form.$statusAkunId = $form.getRef('statusAkunId').uiRadioBox();

        $form.$button = $form.find('#button');
        $form.$button.$save = $form.$button.getRef('save').uiButton();
        $form.$button.$cancel = $form.$button.getRef('cancel').uiButton();

        // change_password

        $change_password = $el.find('#change_password').uiForm();
        $change_password.$name = $change_password.getRef('name').uiTextView();
        $change_password.$email = $change_password.getRef('email').uiTextView();
        $change_password.$new_password = $change_password.getRef('new_password').uiTextBox({ type: TYPE_PASSWORD });
        $change_password.$retype_password = $change_password.getRef('retype_password').uiTextBox({ type: TYPE_PASSWORD });

        $change_password.$button = $change_password.find('#button');
        $change_password.$button.$save = $change_password.$button.getRef('save').uiButton();
        $change_password.$button.$cancel = $change_password.$button.getRef('cancel').uiButton();

    };

    this.driveEvents = function () {
        // tab

        $tab.$preview.on('click', this.detailData.bind(this));
        $tab.$form.on('click', this.editData.bind(this));
        $tab.$new.on('click', this.newData.bind(this));
        $tab.$change_password.on('click', this.detailPassword.bind(this));

        // list

        $list.$unitKerjaId.on('change', this.readData.bind(this));
        $list.$instansiId.on('change', this.readData.bind(this));
        $list.$statusAkunId.on('change', this.readData.bind(this));
        $list.$search.on('keyup', this.searchData.bind(this));

        $list.$table.on('dblclick', function () {
            if (!$list.$search.getValue()) {
                if (rs.length > 0)
                    $tab.$preview.trigger('click');
            } else {
                if (renderedRs.length > 0)
                    $tab.$preview.trigger('click');
            }
        });

        $list.$paging.on('click', this.moveData.bind(this));
        $list.$button.$delete.on('click', this.deleteData.bind(this));

        $list.$button.$detail.on('click', function () {
            $tab.$preview.trigger('click');
        });

        $list.$button.$edit.on('click', function () {
            $tab.$form.trigger('click');
        });

        $list.$button.$refresh.on('click', this.loadData.bind(this));
        $list.$button.$pdf.on('click', this.exportListToPdf.bind(this));

        // preview

        $preview.$button.$edit.on('click', function () {
            $tab.$form.trigger('click');
        });

        $preview.$button.$cancel.on('click', function () {
            $tab.$list.trigger('click');
        });

        $preview.$button.$pdf.on('click', this.exportDetailToPdf.bind(this));

        $form.$button.$save.on('click', this.saveData.bind(this));
        $form.$button.$cancel.on('click', function () {
            $tab.$list.trigger('click');
        });

        // change_password

        $change_password.$button.$save.on('click', this.savePassword.bind(this));
        $change_password.$button.$cancel.on('click', function () {
            $tab.$list.trigger('click');
        });

    };

    this.driveFieldSequence = function () {

    };

    this.reload = function () {
        $tab.$list.trigger('click');

    };

    this.loadData = function () {
        this.readData();

        $list.$statusAkunId.renderApi('StatusAkun/readData', { text: 'statusAkun' });
        $list.$unitKerjaId.renderApi('UnitKerja/readData', { text: 'unitKerja' });
        $list.$instansiId.renderApi('Instansi/readData', { text: 'instansi' });
        $form.$statusAkunId.renderApi('StatusAkun/readData', { text: 'statusAkun' });
        $form.$pangkatGolonganId.renderApi('PangkatGolongan/readData', { text: 'pangkatGolongan' });
        $form.$unitKerjaId.renderApi('UnitKerja/readData', { text: 'unitKerja' });
        $form.$instansiId.renderApi('Instansi/readData', { text: 'instansi' });
        $form.$pendidikanTerakhirId.renderApi('PendidikanTerakhir/readData', { text: 'pendidikanTerakhir' });
        $form.$jenisKelaminId.renderApi('Gender/readData', { text: 'gender' });
        $form.$agamaId.renderApi('Agama/readData', { text: 'agama' });

    };


    this.readData = function (callback) {

        if (!callback)
            $loading.show();

        var url = 'Anggota/readData';
        var data = {
            orderBy: '_registeredOn',
            reverse: 1,
            instansiId: $list.$instansiId.getValue(),
            unitKerjaId: $list.$unitKerjaId.getValue(),
            statusAkunId: $list.$statusAkunId.getValue(),
        };

        api.post(url, data, function (result) {

            rs = result.data;
            var rsCount = rs.length;

            if (rsCount > 0) {
                this.dataShow();
            }
            else {
                this.dataHide();
            }

            renderedRs = this.renderTable($list.$table, rs, tableFields, rsRange);
            this.renderPaging($list.$paging, renderedRs, rsRange);

            if ($.isFunction(callback))
                callback();
            else
                $loading.hide();
        }.bind(this));
    };

    this.dataHide = function () {
        $list.$paging.hide();
        $tab.$form.hide();
        $tab.$preview.hide();
        $list.$button.$delete.hide();
        $list.$button.$detail.hide();
        $list.$button.$edit.hide();
        $tab.$change_password.hide();
        $list.$button.$pdf.hide();
    };

    this.dataShow = function () {
        $list.$paging.show();
        $tab.$form.show();
        $tab.$preview.show();
        $list.$button.$delete.show();
        $list.$button.$detail.show();
        $list.$button.$edit.show();
        $tab.$change_password.show();
        $list.$button.$pdf.hide();
    };

    this.searchData = function () {

        var filteredRs = rs.filter(function (data) {
            var regex = new RegExp($list.$search.getValue(), 'gi');
            var found = false;

            tableFields.forEach(function (field) {
                if (data[field])
                    if (data[field].match(regex))
                        found = true;
            });

            return found;
        });

        renderedRs = this.renderTable($list.$table, filteredRs, tableFields, rsRange);
        if (renderedRs.length > 0) {
            this.renderPaging($list.$paging, renderedRs, rsRange);
            this.dataShow();
        } else {
            this.dataHide();
        }
    };

    this.moveData = function () {
        renderedRs = this.renderTable($list.$table, renderedRs, tableFields, rsRange, $list.$paging.getValue());
    };

    this.deleteData = function () {
        var checkedId = $list.$table.getCheckedValues();

        if (checkedId.length == 0) {
            $msgbox.alert('NO_DATA_CHECKED');
            return;
        };

        var url = 'Anggota/multipleDeleteData';
        var data = {
            multipleId: checkedId,
        };

        $msgbox.confirm('DELETE_CHECKED_DATA', function () {
            api.post(url, data, function (result) {

                switch (result.status) {
                    case 'success':
                        $splash.show(result.message);
                        this.readData();

                        break;
                    case 'failed':
                        $msgbox.alert(result.message);
                }

            }.bind(this));
        }.bind(this));

    };

    this.detailData = function () {
        $loading.show();

        var url = 'Anggota/detailData';
        var data = {
            id: $list.$table.getValue()
        };

        api.post(url, data, function (result) {

            if (result.status == 'success') {
                $preview.$nip.setValue(result.data.nip);
                $preview.$nama.setValue(result.data.nama);
                $preview.$nik.setValue(result.data.nik);
                $preview.$npwp.setValue(result.data.npwp);
                $preview.$foto.setValue(result.data.foto);
                $preview.$email.setValue(result.data.email);
                $preview.$pangkatGolongan.setValue(result.data.pangkatGolongan);
                $preview.$jabatanEselon.setValue(result.data.jabatanEselon);
                $preview.$unitKerja.setValue(result.data.unitKerja);
                $preview.$instansi.setValue(result.data.instansi);
                $preview.$instansiLainnya.setValue(result.data.instansiLainnya);
                $preview.$pendidikanTerakhir.setValue(result.data.pendidikanTerakhir);
                $preview.$gender.setValue(result.data.jenisKelamin);
                $preview.$tanggalLahir.setValue(result.data.tanggalLahir);
                $preview.$tempatLahir.setValue(result.data.tempatLahir);
                $preview.$agama.setValue(result.data.agama);
                $preview.$alamat.setValue(result.data.alamat);
                $preview.$telpHp.setValue(result.data.telpHp);
                $preview.$registeredOn.setValue(result.data.registeredOn);
                $preview.$statusAkun.setValue(result.data.statusAkun);
                $preview.$activeOn.setValue(result.data.activeOn);
            }

            $loading.hide();
        });
    };

    this.detailPassword = function () {
        $loading.show();

        $change_password.clear();
        $change_password.$new_password.clear();
        $change_password.$retype_password.clear();

        var url = 'Anggota/detailData';
        var data = {
            id: $list.$table.getValue()
        };

        api.post(url, data, function (result) {

            if (result.status == 'success') {
                $change_password.$name.setValue(result.data.nama);
                $change_password.$email.setValue(result.data.email);
            }

            $loading.hide();
        });
    };

    this.serializeData = function () {
        var data = {
            nip: $form.$nip.getValue(),
            nama: $form.$nama.getValue(),
            nik: $form.$nik.getValue(),
            npwp: $form.$npwp.getValue(),
            foto: $form.$foto.getValue(),
            email: $form.$email.getValue(),
            statusAkunId: $form.$statusAkunId.getValue(),
            password: $form.$password.getValue(),
            retype_password: $form.$retype_password.getValue(),
            pangkatGolonganId: $form.$pangkatGolonganId.getValue(),
            jabatanEselon: $form.$jabatanEselon.getValue(),
            unitKerjaId: $form.$unitKerjaId.getValue(),
            instansiId: $form.$instansiId.getValue(),
            instansiLainnya: $form.$instansiLainnya.getValue(),
            pendidikanTerakhirId: $form.$pendidikanTerakhirId.getValue(),
            jenisKelaminId: $form.$jenisKelaminId.getValue(),
            tempatLahir: $form.$tempatLahir.getValue(),
            tanggalLahir: $form.$tanggalLahir.getValue(),
            agamaId: $form.$agamaId.getValue(),
            alamat: $form.$alamat.getValue(),
            telpHp: $form.$telpHp.getValue(),
        };

        if (saveMode == EDIT_MODE)
            data.id = $list.$table.getValue();

        return data;
    };

    this.clearData = function () {
        $form.clear();

        $form.$nip.clear();
        $form.$nama.clear();
        $form.$nik.clear();
        $form.$npwp.clear();
        $form.$foto.clear();
        $form.$email.clear();
        $form.$statusAkunId.clear();

        $form.$pangkatGolonganId.clear();
        $form.$jabatanEselon.clear();
        $form.$unitKerjaId.clear();
        $form.$instansiId.clear();
        $form.$instansiLainnya.clear();
        $form.$pendidikanTerakhirId.clear();
        $form.$jenisKelaminId.clear();
        $form.$tempatLahir.clear();
        $form.$tanggalLahir.clear();
        $form.$agamaId.clear();
        $form.$alamat.clear();
        $form.$telpHp.clear();
    };

    this.editData = function () {
        $loading.show();
        saveMode = EDIT_MODE;

        $form.$password.closest('.row').hide();
        $form.$retype_password.closest('.row').hide();
        this.clearData();

        var url = 'Anggota/detailData';
        var data = {
            id: $list.$table.getValue()
        };

        api.post(url, data, function (result) {

            if (result.status == 'success') {
                $form.$nip.setValue(result.data.nip);
                $form.$nama.setValue(result.data.nama);
                $form.$nik.setValue(result.data.nik);
                $form.$npwp.setValue(result.data.npwp);
                $form.$foto.setValue(result.data.foto);
                $form.$email.setValue(result.data.email);
                $form.$statusAkunId.setValue(result.data.statusAkunId);
                $form.$pangkatGolonganId.setValue(result.data.pangkatGolonganId);
                $form.$jabatanEselon.setValue(result.data.jabatanEselon);
                $form.$unitKerjaId.setValue(result.data.unitKerjaId);
                $form.$instansiId.setValue(result.data.instansiId);
                $form.$instansiLainnya.setValue(result.data.instansiLainnya);
                $form.$pendidikanTerakhirId.setValue(result.data.pendidikanTerakhirId);
                $form.$jenisKelaminId.setValue(result.data.jenisKelaminId);
                $form.$tempatLahir.setValue(result.data.tempatLahir);
                $form.$tanggalLahir.setValue(result.data._tanggalLahir);
                $form.$agamaId.setValue(result.data.agamaId);
                $form.$alamat.setValue(result.data.alamat);
                $form.$telpHp.setValue(result.data.telpHp);

                $form.$nip.focus();
            }

            $loading.hide();
        });
    };

    this.newData = function () {
        saveMode = NEW_MODE;

        $form.$password.closest('.row').show();
        $form.$retype_password.closest('.row').show();
        this.clearData();
        $form.$nip.focus();
    };

    this.saveData = function () {
        $form.$button.$save.loading();

        switch (saveMode) {
            case EDIT_MODE:
                var url = 'Anggota/updateData';
                break;
            case NEW_MODE:
                var url = 'Anggota/createData';
        }

        var data = this.serializeData();

        api.post(url, data, function (result) {

            switch (result.status) {
                case 'success':
                    $splash.show(result.message);

                    this.readData(function () {
                        switch (saveMode) {
                            case EDIT_MODE:
                                $tab.$list.trigger('click');
                                break;
                            case NEW_MODE:
                                this.clearData();
                                $form.$nip.focus();
                        }

                        $form.$button.$save.release();
                        window.scrollTo(0, 0);
                    }.bind(this));

                    break;
                case 'invalid':
                    $form.error(result);
                    $form.$button.$save.release();
            }

        }.bind(this));
    };

    this.savePassword = function () {
        $change_password.$button.$save.loading();

        var url = 'Anggota/updatePassword';
        var data = {
            id: $list.$table.getValue(),
            new_password: $change_password.$new_password.getValue(),
            retype_password: $change_password.$retype_password.getValue(),
        };

        api.post(url, data, function (result) {

            switch (result.status) {
                case 'success':
                    $splash.show(result.message);

                    this.readData(function () {

                        $change_password.$new_password.clear();
                        $change_password.$retype_password.clear();

                        $change_password.$button.$save.release();
                    }.bind(this));

                    break;
                case 'invalid':
                    $change_password.error(result);
                    $change_password.$new_password.focus();

                    $change_password.$button.$save.release();
            }

        }.bind(this));
    };


    this.exportData = function (type, url, id) {

        $loading.show();

        var data = {};
        if (id)
            data.id = id;

        api.post(url, data, function (result) {

            $loading.hide();

            switch (result.status) {
                case 'success':

                    if (type == 'pdf')
                        router.open(result.data);
                    else
                        router.redirect(result.data);

                    break;
                case 'failed':
                    $msgbox.alert(result.message);
            }
        });
    };

    this.exportListToPdf = function () {

        var type = 'pdf';
        var url = 'Anggota/createPDFList';

        this.exportData(type, url);
    };

    this.exportDetailToPdf = function () {
        var type = 'pdf';
        var url = 'Anggota/createPDFDetail';
        var id = $list.$table.getValue();

        this.exportData(type, url, id);
    };


};

dashboard.application.controllers.Anggota.prototype =
    Object.create(dashboard.application.core.Controller.prototype);

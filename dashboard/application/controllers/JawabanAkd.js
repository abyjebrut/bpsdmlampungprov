dashboard.application.controllers.JawabanAkd = function () {
    var $el = $page.getScope('JawabanAkd');
    dashboard.application.core.Controller.call(this, $el);

    var rs = [];
    var renderedRs = [];

    var rsRange = config.paging.range;

    var tableHeads = ['NIP_NAMA', 'JABATAN', 'INSTANSI'];
    var tableFields = ['nipNama', 'jabatan', 'instansi'];

    var saveMode;

    var rsItems = [];
    var cartHeads = ['RUMUSAN_KOMPETENSI', 'TINGKAT_KESANGGUPAN'];
    var cartFields = ['rumusanKompetensi', 'tingkatKesanggupan'];

    var $tab;
    var $list;
    var $preview;

    this.init = function () {

        // tab

        $tab = $el.find('#tab');
        $tab.$list = $tab.getRef('list');
        $tab.$preview = $tab.getRef('preview');

        // list

        $list = $el.find('#list');
        $list.$search = $list.getRef('search').uiTextBox({ icon: 'search', placeholder: 'Search...' });
        $list.$table = $list.getRef('table').uiTable({ headers: tableHeads });
        $list.$paging = $list.getRef('paging').uiPaging();

        $list.$button = $list.find('#button');

        $list.$button.$refresh = $list.$button.getRef('refresh').uiButton();

        // preview

        $preview = $el.find('#preview');

        $preview.$nipNama = $preview.getRef('nipNama').uiTextView();
        $preview.$jabatan = $preview.getRef('jabatan').uiTextView();
        $preview.$unitKerja = $preview.getRef('unitKerja').uiTextView();
        $preview.$instansi = $preview.getRef('instansi').uiTextView();
        $preview.$kedudukan = $preview.getRef('kedudukan').uiTextView();
        $preview.$links = $preview.getRef('links').uiTableView(cartHeads);

        $preview.$button = $preview.find('#button');
        $preview.$button.$pdf = $preview.$button.getRef('pdf').uiButton();
        $preview.$button.$cancel = $preview.$button.getRef('cancel').uiButton();

    };

    this.driveEvents = function () {
        // tab

        $tab.$preview.on('click', this.detailData.bind(this));

        // list

        $list.$search.on('keyup', this.searchData.bind(this));

        $list.$table.on('dblclick', function () {
            $tab.$preview.trigger('click');
        });

        $list.$paging.on('click', this.moveData.bind(this));

        $list.$button.$refresh.on('click', this.readData.bind(this));

        // preview

        $preview.$button.$pdf.on('click', this.exportDetailToPdf.bind(this));

        $preview.$button.$cancel.on('click', function () {
            $tab.$list.trigger('click');
        });

    };

    this.driveFieldSequence = function () {

    };

    this.reload = function () {
        $tab.$list.trigger('click');
    };


    this.readData = function (callback) {

        if (!callback)
            $loading.show();

        var url = 'SoalAkd/readDataJawaban';
        var data = {
            orderBy: 'id',
            reverse: 1,
        };

        api.post(url, data, function (result) {

            rs = result.data;
            var rsCount = rs.length;

            if (rsCount > 0) {
                this.dataShow();
            }
            else {
                this.dataHide();
            }

            renderedRs = this.renderTable($list.$table, rs, tableFields, rsRange);
            this.renderPaging($list.$paging, renderedRs, rsRange);

            if ($.isFunction(callback))
                callback();
            else
                $loading.hide();
        }.bind(this));
    };

    this.dataHide = function () {
        $list.$paging.hide();
        $tab.$preview.hide();
    };

    this.dataShow = function () {
        $list.$paging.show();
        $tab.$preview.show();
    };

    this.searchData = function () {

        var filteredRs = rs.filter(function (data) {
            var regex = new RegExp($list.$search.getValue(), 'gi');
            var found = false;

            tableFields.forEach(function (field) {
                if (data[field])
                    if (data[field].match(regex))
                        found = true;
            });

            return found;
        });

        renderedRs = this.renderTable($list.$table, filteredRs, tableFields, rsRange);
        if (renderedRs.length > 0) {
            this.renderPaging($list.$paging, renderedRs, rsRange);
            this.dataShow();
        } else {
            this.dataHide();
        }
    };

    this.moveData = function () {
        renderedRs = this.renderTable($list.$table, renderedRs, tableFields, rsRange, $list.$paging.getValue());
    };


    this.detailData = function () {
        $loading.show();

        var url = 'SoalAkd/detailData';
        var data = {
            id: $list.$table.getValue()
        };

        api.post(url, data, function (result) {

            if (result.status == 'success') {
                $preview.$nipNama.setValue(result.data.nipNama);
                $preview.$jabatan.setValue(result.data.jabatan);
                $preview.$unitKerja.setValue(result.data.unitKerja);
                $preview.$instansi.setValue(result.data.instansi);
                $preview.$kedudukan.setValue(result.data.kedudukan);

                rsItems = result.data.links;
                this.renderViewCart();

            }

            $loading.hide();
        }.bind(this));
    };


    this.renderViewCart = function () {
        this.renderTable($preview.$links, rsItems, cartFields, 100);
    };

    this.exportData = function (type, url, id) {

        $loading.show();

        var data = {};
        if (id)
            data.id = id;

        api.post(url, data, function (result) {

            $loading.hide();

            switch (result.status) {
                case 'success':

                    if (type == 'pdf')
                        router.open(result.data);
                    else
                        router.redirect(result.data);

                    break;
                case 'failed':
                    $msgbox.alert(result.message);
            }
        });
    };

    this.exportDetailToPdf = function () {
        var type = 'pdf';
        var url = 'SoalAkd/createPDFDetail';
        var id = $list.$table.getValue();

        this.exportData(type, url, id);
    };

};

dashboard.application.controllers.JawabanAkd.prototype =
    Object.create(dashboard.application.core.Controller.prototype);

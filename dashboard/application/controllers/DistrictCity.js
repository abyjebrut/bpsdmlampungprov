dashboard.application.controllers.DistrictCity = function () {
    var $el = $page.getScope('DistrictCity');
    dashboard.application.core.Controller.call(this, $el);

    var rs = [];
    var renderedRs = [];

    var rsRange = config.paging.range;

    var tableHeads = ['DISTRICT_CITY'];
    var tableFields = ['districtCity'];

    var saveMode;

    var $tab;
    var $list;
    var $preview;
    var $form;

    this.init = function () {

        // tab

        $tab = $el.find('#tab');
        $tab.$list = $tab.getRef('list');
        $tab.$preview = $tab.getRef('preview');
        $tab.$form = $tab.getRef('form');
        $tab.$new = $tab.getRef('new');

        // list

        $list = $el.find('#list');

        $list.$filter = $list.getRef('filter').uiComboBox({placeholder: 'Filter By Regional'});
        $list.$search = $list.getRef('search').uiTextBox({icon: 'search', placeholder: 'Search...'});
        $list.$table = $list.getRef('table').uiTable({headers: tableHeads});
        $list.$paging = $list.getRef('paging').uiPaging();

        $list.$button = $list.find('#button');
        $list.$button.$delete = $list.$button.getRef('delete').uiButton();
        $list.$button.$detail = $list.$button.getRef('detail').uiButton();

        $list.$button.$refresh = $list.$button.getRef('refresh').uiButton();
        $list.$button.$edit = $list.$button.getRef('edit').uiButton();
        $list.$button.$pdf = $list.$button.getRef('pdf').uiButton();
        $list.$button.$excel = $list.$button.getRef('excel').uiButton();

        // preview

        $preview = $el.find('#preview');

        $preview.$regional = $preview.getRef('regional').uiTextView();
        $preview.$districtCity = $preview.getRef('districtCity').uiTextView();

        $preview.$button = $preview.find('#button');
        $preview.$button.$edit = $preview.$button.getRef('edit').uiButton();
        $preview.$button.$cancel = $preview.$button.getRef('cancel').uiButton();
        $preview.$button.$pdf = $preview.$button.getRef('pdf').uiButton();
        $preview.$button.$excel = $preview.$button.getRef('excel').uiButton();

        // form

        $form = $el.find('#form').uiForm();

        $form.$regionalId = $form.getRef('regionalId').uiComboBox();
        $form.$districtCity = $form.getRef('districtCity').uiTextBox();

        $form.$button = $form.find('#button');
        $form.$button.$save = $form.$button.getRef('save').uiButton();
        $form.$button.$cancel = $form.$button.getRef('cancel').uiButton();

    };

    this.driveEvents = function () {
        // tab

        $tab.$preview.on('click', this.detailData.bind(this));
        $tab.$form.on('click', this.editData.bind(this));
        $tab.$new.on('click', this.newData.bind(this));

        // list

        $list.$filter.on('change', this.readData.bind(this));
        $list.$search.on('keyup', this.searchData.bind(this));

        $list.$table.on('dblclick', function () {
            $tab.$preview.trigger('click');
        });

        $list.$paging.on('click', this.moveData.bind(this));
        $list.$button.$delete.on('click', this.deleteData.bind(this));

        $list.$button.$detail.on('click', function () {
            $tab.$preview.trigger('click');
        });

        $list.$button.$edit.on('click', function () {
            $tab.$form.trigger('click');
        });

        $list.$button.$refresh.on('click', this.reload.bind(this));
        $list.$button.$pdf.on('click', this.exportListToPdf.bind(this));
        $list.$button.$excel.on('click', this.exportListToExcel.bind(this));
        
        // preview

        $preview.$button.$edit.on('click', function () {
            $tab.$form.trigger('click');
        });

        $preview.$button.$cancel.on('click', function () {
            $tab.$list.trigger('click');
        });

        $preview.$button.$pdf.on('click', this.exportDetailToPdf.bind(this));
        $preview.$button.$excel.on('click', this.exportDetailToExcel.bind(this));
        
        // form

        $form.$button.$save.on('click', this.saveData.bind(this));

        $form.$button.$cancel.on('click', function () {
            $tab.$list.trigger('click');
        });

    };

    this.driveFieldSequence = function () {

        $form.$districtCity.on('enter', function () {
            $form.$button.$save.trigger('click');
        });

    };

    this.reload = function () {
        $tab.$list.trigger('click');

        $list.$filter.clear();
        this.readData();

        $list.$filter.renderApi('Regional/readData', {text: 'regional'});
        $form.$regionalId.renderApi('Regional/readData', {text: 'regional'});
    };

    this.readData = function (callback) {

        if (!callback)
            $loading.show();

        var url = 'DistrictCity/readData';
        var data = {
            orderBy: 'districtCity',
            regionalId: $list.$filter.getValue(),
        };

        api.post(url, data, function (result) {

            rs = result.data;
            var rsCount = rs.length;

            if (rsCount > 0) {
                var showButtons = true;
                $tab.$preview.show();
                $tab.$form.show();
            }
            else  {
                var showButtons = false;
                $tab.$preview.hide();
                $tab.$form.hide();
            }

            $list.$button.$delete.setEnabled(showButtons);
            $list.$button.$detail.setEnabled(showButtons);
            $list.$button.$edit.setEnabled(showButtons);

            renderedRs = this.renderTable($list.$table, rs, tableFields, rsRange);
            this.renderPaging($list.$paging, renderedRs, rsRange);

            if ($.isFunction(callback))
                callback();
            else
                $loading.hide();
        }.bind(this));
    };

    this.searchData = function () {

        var filteredRs = rs.filter(function (data) {
            var regex = new RegExp($list.$search.getValue(), 'gi');
            var found = false;

            tableFields.forEach(function (field) {
                if (data[field])
                    if (data[field].match(regex))
                        found = true;
            });

            return found;
        });

        renderedRs = this.renderTable($list.$table, filteredRs, tableFields, rsRange);
        this.renderPaging($list.$paging, renderedRs, rsRange);
    };

    this.moveData = function () {
        renderedRs = this.renderTable($list.$table, renderedRs, tableFields, rsRange, $list.$paging.getValue());
    };

    this.deleteData = function () {
        var checkedId = $list.$table.getCheckedValues();

        if (checkedId.length == 0) {
            $msgbox.alert('NO_DATA_CHECKED');
            return;
        };

        var url = 'DistrictCity/multipleDeleteData';
        var data = {
            multipleId: checkedId,
        };

        $msgbox.confirm('DELETE_CHECKED_DATA', function () {
            api.post(url, data, function (result) {

                switch (result.status) {
                case 'success':
                    $splash.show(result.message);
                    this.readData();

                    break;
                case 'failed':
                    $msgbox.alert(result.message);
                }

            }.bind(this));
        }.bind(this));

    };

    this.detailData = function () {
        $loading.show();

        var url = 'DistrictCity/detailData';
        var data = {
            id: $list.$table.getValue()
        };

        api.post(url, data, function (result) {

            if (result.status == 'success') {
                $preview.$regional.setValue(result.data.regional);
                $preview.$districtCity.setValue(result.data.districtCity);
            }

            $loading.hide();
        });
    };

    this.serializeData = function () {
        var data = {
            regionalId: $form.$regionalId.getValue(),
            districtCity: $form.$districtCity.getValue(),
        };

        if (saveMode == EDIT_MODE)
            data.id = $list.$table.getValue();

        return data;
    };

    this.clearData = function () {
        $form.clear();

        $form.$districtCity.clear();
    };

    this.editData = function () {
        $loading.show();
        saveMode = EDIT_MODE;

        this.clearData();

        var url = 'DistrictCity/detailData';
        var data = {
            id: $list.$table.getValue()
        };

        api.post(url, data, function (result) {

            if (result.status == 'success') {
                $form.$regionalId.setValue(result.data.regionalId);
                $form.$districtCity.setValue(result.data.districtCity);

                $form.$districtCity.focus();
            }

            $loading.hide();
        });
    };

    this.newData = function () {
        saveMode = NEW_MODE;

        this.clearData();
        $form.$regionalId.setValue($list.$filter.getValue());

        $form.$districtCity.focus();
    };

    this.saveData = function () {
        $form.$button.$save.loading();

        switch (saveMode) {
        case EDIT_MODE:
            var url = 'DistrictCity/updateData';
            break;
        case NEW_MODE:
            var url = 'DistrictCity/createData';
        }

        var data = this.serializeData();

        api.post(url, data, function (result) {

            switch (result.status) {
            case 'success':
                $splash.show(result.message);

                this.readData(function () {
                    switch (saveMode) {
                    case EDIT_MODE:
                        $tab.$list.trigger('click');
                        break;
                    case NEW_MODE:
                        this.clearData();
                        $form.$districtCity.focus();
                    }

                    $form.$button.$save.release();
                    window.scrollTo(0, 0);
                }.bind(this));

                break;
            case 'invalid':
                $form.error(result);
                $form.$button.$save.release();
            }

        }.bind(this));
    };

    this.exportData = function (type, url, id) {

        $loading.show();

        var data = {};
        if (id)
            data.id = id;

        api.post(url, data, function (result) {

            $loading.hide();

            switch (result.status) {
            case 'success':

                if (type == 'pdf')
                    router.open(result.data);
                else
                    router.redirect(result.data);

                break;
            case 'failed':
                $msgbox.alert(result.message);
            }
        });
    };

    this.exportListToPdf = function () {

        var type = 'pdf';
        var url = 'DistrictCity/createPDFList';

        this.exportData(type, url);
    };

    this.exportListToExcel = function () {
        var type = 'xls';
        var url = 'DistrictCity/createExcelList';

        this.exportData(type, url);
    };
    
    this.exportDetailToPdf = function () {
        var type = 'pdf';
        var url = 'DistrictCity/createPDFDetail';
        var id = $list.$table.getValue();

        this.exportData(type, url, id);
    };
    
    this.exportDetailToExcel = function () {
        var type = 'xls';
        var url = 'DistrictCity/createExcelDetail';
        var id = $list.$table.getValue();

        this.exportData(type, url, id);
    };

};

dashboard.application.controllers.DistrictCity.prototype =
Object.create(dashboard.application.core.Controller.prototype);

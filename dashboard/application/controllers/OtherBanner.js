dashboard.application.controllers.OtherBanner = function () {
  var $el = $page.getScope('OtherBanner');
  dashboard.application.core.Controller.call(this, $el);

  var rs = [];
  var renderedRs = [];

  var rsRange = config.paging.range;

  var tableHeads = ['PAGE'];
  var tableFields = ['title'];

  var $tab;
  var $list;
  var $form;

  this.init = function () {

    // tab

    $tab = $el.find('#tab');
    $tab.$list = $tab.getRef('list');
    $tab.$form = $tab.getRef('form');

    // list

    $list = $el.find('#list');
    $list.$search = $list.getRef('search').uiTextBox({ icon: 'search', placeholder: 'Search...' });
    $list.$table = $list.getRef('table').uiTable({ headers: tableHeads });
    $list.$paging = $list.getRef('paging').uiPaging();

    $list.$button = $list.find('#button');

    $list.$button.$refresh = $list.$button.getRef('refresh').uiButton();
    $list.$button.$edit = $list.$button.getRef('edit').uiButton();

    // form

    $form = $el.find('#form').uiForm();

    $form.$slContainer = $form.getRef('slContainer');
    $form.$slContainer.hide();
    $form.$elContainer = $form.getRef('elContainer');
    $form.$elContainer.hide();
    $form.$labelSl = $form.getRef('labelSl');
    $form.$labelEl = $form.getRef('labelEl');

    $form.$pagesId = $form.getRef('pagesId').uiTextView();
    $form.$publishStatusId = $form.getRef('publishStatusId').uiRadioBox();
    $form.$languageCode = $form.getRef('languageCode').uiLangBox();
    $form.$title = $form.getRef('title').uiTextBox();
    $form.$titleSl = $form.getRef('titleSl').uiTextBox();
    $form.$titleEl = $form.getRef('titleEl').uiTextBox();
    $form.$banner = $form.getRef('banner').uiUploadImage();

    $form.$button = $form.find('#button');
    $form.$button.$save = $form.$button.getRef('save').uiButton();
    $form.$button.$cancel = $form.$button.getRef('cancel').uiButton();

  };

  this.driveEvents = function () {
    // tab

    $tab.$form.on('click', this.editData.bind(this));

    // list

    $list.$search.on('keyup', this.searchData.bind(this));

    $list.$table.on('dblclick', function () {
      $tab.$form.trigger('click');
    });

    $list.$paging.on('click', this.moveData.bind(this));

    $list.$button.$edit.on('click', function () {
      $tab.$form.trigger('click');
    });

    $list.$button.$refresh.on('click', this.loadData.bind(this));

    // form

    $form.$button.$save.on('click', this.saveData.bind(this));

    $form.$button.$cancel.on('click', function () {
      $tab.$list.trigger('click');
    });

    $form.$languageCode.on('click', this.showHideLangContainers.bind(this));

  };

  this.driveFieldSequence = function () {

  };

  this.reload = function () {
    $tab.$list.trigger('click');

  };

  this.loadData = function () {
    this.readData();

    $form.$publishStatusId.renderApi('PublishStatus/readData', { text: 'publishStatus' });
    $form.$languageCode.renderApi('Language/readData', { text: 'language', flag: 'flag' });

  };

  this.showHideLangContainers = function () {

    if ($form.$languageCode.getValue() == localStorage.primaryLang) {
      $form.$slContainer.hide();
      $form.$elContainer.hide();
    } else if ($form.$languageCode.getValue() == localStorage.secondaryLang) {
      $form.$slContainer.show();
      $form.$labelSl.html($form.$languageCode.getText());
      $form.$elContainer.hide();
    } else if ($form.$languageCode.getValue() == localStorage.extendedLang) {
      $form.$slContainer.hide();
      $form.$elContainer.show();
      $form.$labelEl.html($form.$languageCode.getText());
    }
  };

  this.readData = function (callback) {

    if (!callback)
      $loading.show();

    var url = 'OtherBanner/readData';

    api.post(url, function (result) {

      rs = result.data;
      var rsCount = rs.length;

      if (rsCount > 0) {
        var showButtons = true;
        $tab.$form.show();
      }
      else {
        var showButtons = false;
        $tab.$form.hide();
      }

      $list.$button.$edit.setEnabled(showButtons);

      renderedRs = this.renderTable($list.$table, rs, tableFields, rsRange);
      this.renderPaging($list.$paging, renderedRs, rsRange);

      if ($.isFunction(callback))
        callback();
      else
        $loading.hide();
    }.bind(this));
  };

  this.searchData = function () {

    var filteredRs = rs.filter(function (data) {
      var regex = new RegExp($list.$search.getValue(), 'gi');
      var found = false;

      tableFields.forEach(function (field) {
        if (data[field])
          if (data[field].match(regex))
            found = true;
      });

      return found;
    });

    renderedRs = this.renderTable($list.$table, filteredRs, tableFields, rsRange);
    this.renderPaging($list.$paging, renderedRs, rsRange);
  };

  this.moveData = function () {
    renderedRs = this.renderTable($list.$table, renderedRs, tableFields, rsRange, $list.$paging.getValue());
  };

  this.serializeData = function () {
    var data = {
      pagesId: $list.$table.getValue(),
      title: $form.$title.getValue(),
      titleSl: $form.$titleSl.getValue(),
      titleEl: $form.$titleEl.getValue(),
      publishStatusId: $form.$publishStatusId.getValue(),
      banner: $form.$banner.getValue(),
    };

    return data;
  };


  this.clearData = function () {
    $form.clear();

    $form.$pagesId.clear();
    $form.$publishStatusId.clear();
    $form.$languageCode.setValue(localStorage.primaryLang);
    this.showHideLangContainers();
    $form.$title.clear();
    $form.$titleSl.clear();
    $form.$titleEl.clear();
    $form.$banner.clear();

  };

  this.editData = function () {
    $loading.show();

    this.clearData();

    var url = 'OtherBanner/detailData';
    var data = {
      id: $list.$table.getValue()
    };

    api.post(url, data, function (result) {

      if (result.status == 'success') {
        $form.$pagesId.setValue(result.extra);
        $form.$title.setValue(result.data.title);
        $form.$titleSl.setValue(result.data.title_sl);
        $form.$titleEl.setValue(result.data.title_el);
        $form.$publishStatusId.setValue(result.data.publishStatusId);
        $form.$banner.setValue(result.data.banner);

        $form.$publishStatusId.focus();
      }

      $loading.hide();
    }.bind(this));
  };

  this.saveData = function () {
    $form.$button.$save.loading();

    var url = 'OtherBanner/saveData';
    var data = this.serializeData();

    api.post(url, data, function (result) {

      switch (result.status) {
        case 'success':
          $splash.show(result.message);
          $form.$button.$save.release();
          this.editData(function () {
            $tab.$form.trigger('click');

            window.scrollTo(0, 0);
          }.bind(this));
          break;
        case 'invalid':
          $form.error(result);
          $form.$button.$save.release();
      }

    }.bind(this));
  };

};

dashboard.application.controllers.OtherBanner.prototype =
  Object.create(dashboard.application.core.Controller.prototype);

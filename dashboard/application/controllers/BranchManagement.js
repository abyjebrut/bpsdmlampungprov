dashboard.application.controllers.BranchManagement = function () {
    var $el = $page.getScope('BranchManagement');
    dashboard.application.core.Controller.call(this, $el);

    var rs = [];
    var renderedRs = [];

    var rsRange = config.paging.range;

    var tableHeads = ['BRANCH_NAME','PHONE_NUMBER'];
    var tableFields = ['branchName','phoneNumber'];

    var saveMode;

    var $tab;
    var $list;
    var $preview;
    var $form;

    this.init = function () {

        // tab

        $tab = $el.find('#tab');
        $tab.$list = $tab.getRef('list');
        $tab.$preview = $tab.getRef('preview');
        $tab.$form = $tab.getRef('form');
        $tab.$new = $tab.getRef('new');

        // list

        $list = $el.find('#list');
        $list.$search = $list.getRef('search').uiTextBox({icon: 'search', placeholder: 'Search...'});
        $list.$table = $list.getRef('table').uiTable({headers: tableHeads});
        $list.$paging = $list.getRef('paging').uiPaging();

        $list.$button = $list.find('#button');
        $list.$button.$delete = $list.$button.getRef('delete').uiButton();
        $list.$button.$detail = $list.$button.getRef('detail').uiButton();

        $list.$button.$edit = $list.$button.getRef('edit').uiButton();

        $list.$button.$up = $list.$button.getRef('up').uiButton();
        $list.$button.$down = $list.$button.getRef('down').uiButton();
        $list.$button.$refresh = $list.$button.getRef('refresh').uiButton();

        // preview

        $preview = $el.find('#preview');
        $preview.$branchName = $preview.getRef('branchName').uiTextView();
        $preview.$regional = $preview.getRef('regional').uiTextView();
        $preview.$districtCity = $preview.getRef('districtCity').uiTextView();
        $preview.$address = $preview.getRef('address').uiTextAreaView();
        $preview.$mapLocation = $preview.getRef('mapLocation').uiMapView();
        $preview.$latitude = $preview.getRef('latitude').uiTextView();
        $preview.$longitude = $preview.getRef('longitude').uiTextView();
        $preview.$phoneNumber = $preview.getRef('phoneNumber').uiTextView();
        $preview.$faxNumber = $preview.getRef('faxNumber').uiTextView();
        $preview.$email = $preview.getRef('email').uiTextView();
        $preview.$facebookPage = $preview.getRef('facebookPage').uiTextView();
        $preview.$twitterPage = $preview.getRef('twitterPage').uiTextView();

        $preview.$button = $preview.find('#button');
        $preview.$button.$edit = $preview.$button.getRef('edit').uiButton();
        $preview.$button.$cancel = $preview.$button.getRef('cancel').uiButton();

        // form

        $form = $el.find('#form').uiForm();
        $form.$branchName = $form.getRef('branchName').uiTextBox();
        $form.$regionalId = $form.getRef('regionalId').uiComboBox();
        $form.$districtCityId = $form.getRef('districtCityId').uiComboBox();
        $form.$address = $form.getRef('address').uiTextArea();
        $form.$mapLocation = $form.getRef('mapLocation').uiMapPicker();
        $form.$latitude = $form.getRef('latitude').uiTextBox();
        $form.$longitude = $form.getRef('longitude').uiTextBox();
        $form.$phoneNumber = $form.getRef('phoneNumber').uiTextBox();
        $form.$faxNumber = $form.getRef('faxNumber').uiTextBox();
        $form.$email = $form.getRef('email').uiTextBox();
        $form.$facebookPage = $form.getRef('facebookPage').uiTextBox();
        $form.$twitterPage = $form.getRef('twitterPage').uiTextBox();

        $form.$mainBranchId = $form.getRef('mainBranchId').uiCheckBox();
        $form.$mainBranchId.render([{value: 1, text: 'Main Branch'}]);

        $form.$button = $form.find('#button');
        $form.$button.$save = $form.$button.getRef('save').uiButton();
        $form.$button.$cancel = $form.$button.getRef('cancel').uiButton();

    };

    this.driveEvents = function () {
        // tab

        $tab.$preview.on('click', this.detailData.bind(this));
        $tab.$form.on('click', this.editData.bind(this));
        $tab.$new.on('click', this.newData.bind(this));

        // list

        $list.$search.on('keyup', this.searchData.bind(this));

        $list.$table.on('dblclick', function () {
            $tab.$preview.trigger('click');
        });

        $list.$paging.on('click', this.moveData.bind(this));
        $list.$button.$delete.on('click', this.deleteData.bind(this));

        $list.$button.$detail.on('click', function () {
            $tab.$preview.trigger('click');
        });

        $list.$button.$edit.on('click', function () {
            $tab.$form.trigger('click');
        });

        $list.$button.$up.on('click', this.moveUp.bind(this));
        $list.$button.$down.on('click', this.moveDown.bind(this));
        $list.$button.$refresh.on('click', this.reload.bind(this));

        // preview

        $preview.$button.$edit.on('click', function () {
            $tab.$form.trigger('click');
        });

        $preview.$button.$cancel.on('click', function () {
            $tab.$list.trigger('click');
        });

        // form

        $form.$regionalId.on('change', this.filterDistrictCity.bind(this));
        $form.$button.$save.on('click', this.saveData.bind(this));

        $form.$mapLocation.on('change', function () {
          var location = $form.$mapLocation.getValue().split(',');
          $form.$latitude.setValue(location[0]);
          $form.$longitude.setValue(location[1]);
        });

        $form.$button.$cancel.on('click', function () {
            $tab.$list.trigger('click');
        });

    };

    this.driveFieldSequence = function () {

        $form.$branchName.on('enter', function () {
            $form.$button.$save.trigger('click');
        });

    };

    this.reload = function () {
        $tab.$list.trigger('click');
        this.readData();

        $form.$regionalId.renderApi('Regional/readData', {text: 'regional'});
        $form.$districtCityId.renderApi('DistrictCity/readData', {text: 'districtCity'});
    };

    this.filterDistrictCity = function () {
        var regionalId = $form.$regionalId.getValue();
        $form.$districtCityId.renderApi('DistrictCity/readData', {regionalId: regionalId}, {text: 'districtCity'});
    };

    this.readData = function (callback) {

        if (!callback)
            $loading.show();

        var url = 'Branch/readData';
        var data = {
            orderBy: 'position',
        };

        api.post(url, data, function (result) {

            rs = result.data;
            var rsCount = rs.length;

            if (rsCount > 0) {
                var showButtons = true;
                $tab.$preview.show();
                $tab.$form.show();
            }
            else  {
                var showButtons = false;
                $tab.$preview.hide();
                $tab.$form.hide();
            }

            $list.$button.$delete.setEnabled(showButtons);
            $list.$button.$detail.setEnabled(showButtons);
            $list.$button.$edit.setEnabled(showButtons);
//            $list.$button.$down.setEnabled(showButtons);
//            $list.$button.$up.setEnabled(showButtons);

            renderedRs = this.renderTable($list.$table, rs, tableFields, rsRange);
            this.renderPaging($list.$paging, renderedRs, rsRange);

            if ($.isFunction(callback))
                callback();
            else
                $loading.hide();
        }.bind(this));
    };

    this.searchData = function () {

        var filteredRs = rs.filter(function (data) {
            var regex = new RegExp($list.$search.getValue(), 'gi');
            var found = false;

            tableFields.forEach(function (field) {
                if (data[field])
                    if (data[field].match(regex))
                        found = true;
            });

            return found;
        });

        renderedRs = this.renderTable($list.$table, filteredRs, tableFields, rsRange);
        this.renderPaging($list.$paging, renderedRs, rsRange);
    };

    this.moveData = function () {
        renderedRs = this.renderTable($list.$table, renderedRs, tableFields, rsRange, $list.$paging.getValue());
    };

    this.moveUp = function () {

        var activeId = $list.$table.getValue();
        var prevId = $list.$table.find('tr.active').prev().find('input[type="checkbox"]').val()

        if(!prevId)
            return;

        $loading.show();

        var url = 'Branch/moveData';
        var data = {
            idFrom: activeId,
            idTo: prevId,
        };

        api.post(url, data, function (result) {

            if (result.status == 'success') {
                this.reload();
            }

            $loading.hide();
        }.bind(this));

    };

    this.moveDown = function () {

        var activeId = $list.$table.getValue();
        var prevId = $list.$table.find('tr.active').next().find('input[type="checkbox"]').val()

        if(!prevId)
            return;

        $loading.show();

        var url = 'Branch/moveData';
        var data = {
            idFrom: activeId,
            idTo: prevId,
        };

        api.post(url, data, function (result) {

            if (result.status == 'success') {
                this.reload();
            }

            $loading.hide();
        }.bind(this));
    };

    this.deleteData = function () {
        var checkedId = $list.$table.getCheckedValues();

        if (checkedId.length == 0) {
            $msgbox.alert('NO_DATA_CHECKED');
            return;
        };

        var url = 'Branch/multipleDeleteData';
        var data = {
            multipleId: checkedId,
        };

        $msgbox.confirm('DELETE_CHECKED_DATA', function () {
            api.post(url, data, function (result) {

                switch (result.status) {
                case 'success':
                    $splash.show(result.message);
                    this.readData();

                    break;
                case 'failed':
                    $msgbox.alert(result.message);
                }

            }.bind(this));
        }.bind(this));

    };

    this.detailData = function () {
        $loading.show();

        var url = 'Branch/detailData';
        var data = {
            id: $list.$table.getValue(),
        };

        api.post(url, data, function (result) {

            if (result.status == 'success') {

                $preview.$branchName.setValue(result.data.branchName);
                $preview.$regional.setValue(result.data.regional);
                $preview.$districtCity.setValue(result.data.districtCity);
                $preview.$address.setValue(result.data.address);
                $preview.$mapLocation.setValue(result.data.latitude, result.data.longitude);
                $preview.$latitude.setValue(result.data.latitude);
                $preview.$longitude.setValue(result.data.longitude);
                $preview.$phoneNumber.setValue(result.data.phoneNumber);
                $preview.$faxNumber.setValue(result.data.faxNumber);
                $preview.$email.setValue(result.data.email);
                $preview.$facebookPage.setValue(result.data.facebookPage);
                $preview.$twitterPage.setValue(result.data.twitterPage);

            }

            $loading.hide();
        });
    };

    this.serializeData = function () {

        if ($form.$mainBranchId.isChecked())
            var mainBranchId = 1;
        else
            var mainBranchId = 2;

        var data = {
            branchName: $form.$branchName.getValue(),
            districtCityId: $form.$districtCityId.getValue(),
            address: $form.$address.getValue(),
            latitude: $form.$latitude.getValue(),
            longitude: $form.$longitude.getValue(),
            phoneNumber: $form.$phoneNumber.getValue(),
            faxNumber: $form.$faxNumber.getValue(),
            email: $form.$email.getValue(),
            facebookPage: $form.$facebookPage.getValue(),
            twitterPage: $form.$twitterPage.getValue(),
            mainBranchId: mainBranchId,
        };

        if (saveMode == EDIT_MODE)
            data.id = $list.$table.getValue();

        return data;
    };

    this.clearData = function () {
        $form.clear();

        $form.$branchName.clear();
        $form.$regionalId.clear();
        $form.$districtCityId.clear();
        $form.$address.clear();

        $form.$mapLocation.setValue(config.ui.defaultLatitude, config.ui.defaultLongitude);

        $form.$latitude.clear();
        $form.$longitude.clear();
        $form.$phoneNumber.clear();
        $form.$faxNumber.clear();
        $form.$email.clear();
        $form.$facebookPage.clear();
        $form.$twitterPage.clear();
        $form.$mainBranchId.clear();
    };

    this.editData = function () {
        $loading.show();
        saveMode = EDIT_MODE;

        this.clearData();

        var url = 'Branch/detailData';
        var data = {
            id: $list.$table.getValue(),
        };

        api.post(url, data, function (result) {

            if (result.status == 'success') {

                $form.$branchName.setValue(result.data._branchName);
//                $form.$regionalId.setValue(result.data.regionalId);
                $form.$districtCityId.setValue(result.data.districtCityId);
                $form.$address.setValue(result.data.address);
                $form.$mapLocation.setValue(result.data.latitude, result.data.longitude);
                $form.$latitude.setValue(result.data.latitude);
                $form.$longitude.setValue(result.data.longitude);
                $form.$phoneNumber.setValue(result.data.phoneNumber);
                $form.$faxNumber.setValue(result.data.faxNumber);
                $form.$email.setValue(result.data.email);
                $form.$facebookPage.setValue(result.data.facebookPage);
                $form.$twitterPage.setValue(result.data.twitterPage);
                $form.$mainBranchId.setValue(result.data.mainBranchId);

                $form.$branchName.focus();
            }

            $loading.hide();
        });
    };

    this.newData = function () {
        saveMode = NEW_MODE;

        this.clearData();
        $form.$branchName.focus();
    };

    this.saveData = function () {
        $form.$button.$save.loading();

        switch (saveMode) {
        case EDIT_MODE:
            var url = 'Branch/updateData';
            break;
        case NEW_MODE:
            var url = 'Branch/createData';
        }

        var data = this.serializeData();

        api.post(url, data, function (result) {

            switch (result.status) {
            case 'success':
                $splash.show(result.message);

                this.readData(function () {
                    switch (saveMode) {
                    case EDIT_MODE:
                        $tab.$list.trigger('click');
                        break;
                    case NEW_MODE:
                        this.clearData();
                        $form.$branchName.focus();
                    }

                    $form.$button.$save.release();
                    window.scrollTo(0, 0);
                }.bind(this));

                break;
            case 'invalid':
                $form.error(result);
                $form.$button.$save.release();
            }

        }.bind(this));
    };



};

dashboard.application.controllers.BranchManagement.prototype =
Object.create(dashboard.application.core.Controller.prototype);

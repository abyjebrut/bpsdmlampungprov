dashboard.application.controllers.Diklat = function () {
    var $el = $page.getScope('Diklat');
    dashboard.application.core.Controller.call(this, $el);
    
    var rs = [];
    var rsBahan = [];
    var renderedRs = [];
    var renderedRsBahan = [];

    var rsRange = config.paging.range;

    var tableHeads = ['PUBLISH_STATUS', 'Nomor', 'Nama Diklat', 'Jenis', 'Angkatan', 'Tanggal Mulai', 'Sampai'];
    var tableFields = ['publishStatus', 'nomor', 'namaDiklat', 'jenisDiklat', 'angkatan', 'pelaksanaanMulai', 'pelaksanaanSelesai'];

    var viewHeadsBahan = ['No', 'Tanggal', 'Waktu', 'Mata Pelajaran', 'Nama Pengajar', 'Jumlah Jam'];
    var viewFieldsBahan = ['noUrut', 'allTanggal', 'allWaktu', 'mataPelajaran', 'allPengajar', 'allJam'];

    var tableHeadsBahan = ['No', 'Tanggal', 'Waktu', 'Mata Pelajaran', 'Nama Pengajar', 'Jumlah Jam'];
    var tableFieldsBahan = ['noUrut', 'allTanggal', 'allWaktu', 'mataPelajaran', 'allPengajar', 'allJam'];

    var rsPengajar = [];
    var cartHeads = ['Nama Pengajar'];
    var cartFields = ['namaPengajar'];
    var rsPengajarD = [];
    var rsIsiMapel = [];

    var saveMode;
    var saveModeBahan;
    var tipeWaktuId;
    var jenisDiklatIdPengajar;
    var angkatanIdPengajar;
    var tglSelesai;

    var idPengajarSelect = '', idMapelSelect = '', idTglSelect = ''
    var $tab;
    var $list;
    var $listBahan;
    var $preview;
    var $form;

    var monthRecords = [
        'Jan',
        'Feb',
        'Mar',
        'Apr',
        'Mei',
        'Jun',
        'Jul',
        'Ags',
        'Sept',
        'Okt',
        'Nov',
        'Dec',
    ]

    var hariRecords = [
        'Minggu',
        'Senin',
        'Selasa',
        'Rabu',
        'Kamis',
        'Jum`at',
        'Sabtu',
    ]

    this.init = function () {

        // tab

        $tab = $el.find('#tab');
        $tab.$list = $tab.getRef('list');
        $tab.$preview = $tab.getRef('preview');
        $tab.$form = $tab.getRef('form');
        $tab.$new = $tab.getRef('new');
        $tab.$listBahan = $tab.getRef('listBahan');

        // list

        $list = $el.find('#list');
        $list.$publishStatusId = $list.getRef('publishStatusId').uiRadioBox();
        $list.$tahun = $list.getRef('tahun').uiComboBox();
        $list.$jenisDiklatId = $list.getRef('jenisDiklatId').uiComboBox();
        $list.$diklatId = $list.getRef('diklatId').uiComboBox();
        $list.$search = $list.getRef('search').uiTextBox({ icon: 'search', placeholder: 'Search...' });
        $list.$table = $list.getRef('table').uiTable({ headers: tableHeads });
        $list.$paging = $list.getRef('paging').uiPaging();

        $list.$button = $list.find('#button');
        $list.$button.$delete = $list.$button.getRef('delete').uiButton();
        $list.$button.$detail = $list.$button.getRef('detail').uiButton();

        $list.$button.$edit = $list.$button.getRef('edit').uiButton();

        $list.$button.$refresh = $list.$button.getRef('refresh').uiButton();

        // list Bahan

        $listBahan = $el.find('#listBahan').uiForm();

        $listBahan.$pengajarDiklatId = $listBahan.getRef('pengajarDiklatId').uiComboBox();
        $listBahan.$mataPelajaranId = $listBahan.getRef('mataPelajaranId').uiComboBox();
        $listBahan.$lokasiId = $listBahan.getRef('lokasiId').uiComboBox();
        $listBahan.$tempatId = $listBahan.getRef('tempatId').uiComboBox();
        $listBahan.$tanggal = $listBahan.getRef('tanggal').uiDatePicker();
        $listBahan.$elWaktu = $listBahan.getRef('elWaktu');
        $listBahan.$elWaktu.hide();
        $listBahan.$elTanggal = $listBahan.getRef('elTanggal');
        $listBahan.$elTanggal.hide();
        $listBahan.$jamMulai = $listBahan.getRef('jamMulai').uiTextView();
        $listBahan.$jamSelesai = $listBahan.getRef('jamSelesai').uiTextView();
        $listBahan.$jumlahHari = $listBahan.getRef('jumlahHari').uiTextView();
        $listBahan.$tanggalSelesai_ = $listBahan.getRef('tanggalSelesai_').uiTextView();
        $listBahan.$listPengajar = $listBahan.getRef('listPengajar').uiTable(cartHeads);
        $listBahan.$add = $listBahan.getRef('add');
        $listBahan.$delete = $listBahan.getRef('delete');

        $listBahan.$search = $listBahan.getRef('search').uiTextBox({ icon: 'search', placeholder: 'Search...' });
        $listBahan.$table = $listBahan.getRef('table').uiTable({ headers: tableHeadsBahan });

        $listBahan.$button = $listBahan.find('#button');

        $listBahan.$button.$save = $listBahan.$button.getRef('save').uiButton();
        $listBahan.$button.$cancel = $listBahan.$button.getRef('cancel').uiButton();
        $listBahan.$button.$delete = $listBahan.$button.getRef('delete').uiButton();
        $listBahan.$button.$edit = $listBahan.$button.getRef('edit').uiButton();
        $listBahan.$button.$excel = $listBahan.$button.getRef('excel').uiButton();
        $listBahan.$button.$refresh = $listBahan.$button.getRef('refresh').uiButton();

        // preview

        $preview = $el.find('#preview');

        $preview.$publishStatus = $preview.getRef('publishStatus').uiTextView();
        $preview.$nomor = $preview.getRef('nomor').uiTextView();
        $preview.$pelaksanaanMulai = $preview.getRef('pelaksanaanMulai').uiTextView();
        $preview.$pelaksanaanSelesai = $preview.getRef('pelaksanaanSelesai').uiTextView();
        $preview.$jenisDiklat = $preview.getRef('jenisDiklat').uiTextView();
        $preview.$namaDiklat = $preview.getRef('namaDiklat').uiTextView();
        $preview.$angkatan = $preview.getRef('angkatan').uiTextView();
        $preview.$listBahan = $preview.getRef('listBahan').uiTableView({ headers: viewHeadsBahan });

        $preview.$button = $preview.find('#button');
        $preview.$button.$edit = $preview.$button.getRef('edit').uiButton();
        $preview.$button.$cancel = $preview.$button.getRef('cancel').uiButton();


        // form

        $form = $el.find('#form').uiForm();

        $form.$publishStatusId = $form.getRef('publishStatusId').uiRadioBox();
        $form.$nomor = $form.getRef('nomor').uiTextBox();
        $form.$tanggal = $form.getRef('tanggal').uiDatePicker();
        $form.$diklatId = $form.getRef('diklatId').uiComboBox();
        $form.$jenisDiklat = $form.getRef('jenisDiklat').uiTextView();
        $form.$pelaksanaanMulai = $form.getRef('pelaksanaanMulai').uiTextView();
        $form.$pelaksanaanSelesai = $form.getRef('pelaksanaanSelesai').uiTextView();
        $form.$angkatanId = $form.getRef('angkatanId').uiComboBox();

        $form.$button = $form.find('#button');
        $form.$button.$save = $form.$button.getRef('save').uiButton();
        $form.$button.$cancel = $form.$button.getRef('cancel').uiButton();

        // form

    };

    this.driveEvents = function () {
        // tab

        $tab.$list.on('click', this.readData.bind(this));
        $tab.$listBahan.on('click', this.readDataBahan.bind(this));

        $tab.$preview.on('click', this.detailData.bind(this));
        $tab.$form.on('click', this.editData.bind(this));
        $tab.$new.on('click', this.newData.bind(this));

        // list

        $list.$publishStatusId.on('click', this.readData.bind(this));
        $list.$tahun.on('change', this.readData.bind(this));
        $list.$jenisDiklatId.on('change', this.readData.bind(this));
        $list.$diklatId.on('change', this.readData.bind(this));
        $list.$search.on('keyup', this.searchData.bind(this));

        $list.$table.on('dblclick', function () {
            $tab.$preview.trigger('click');
        });

        $list.$paging.on('click', this.moveData.bind(this));
        $list.$button.$delete.on('click', this.deleteData.bind(this));

        $list.$button.$detail.on('click', function () {
            $tab.$preview.trigger('click');
        });

        $list.$button.$edit.on('click', function () {
            $tab.$form.trigger('click');
        });

        $list.$button.$refresh.on('click', this.reload.bind(this));

        // list Bahan

        $listBahan.$search.on('keyup', this.searchDataBahan.bind(this));

        $listBahan.$mataPelajaranId.on('change', this.showMaPel.bind(this));
        $listBahan.$tanggal.on('change', this.showTanggalSelesai.bind(this));

        $listBahan.$button.$delete.on('click', this.deleteDataBahan.bind(this));
        $listBahan.$button.$edit.on('click', this.editDataBahan.bind(this));
        $listBahan.$add.on('click', this.addToCart.bind(this));
        $listBahan.$delete.on('click', this.deleteFromCart.bind(this));

        $listBahan.$button.$excel.on('click', this.exportDetailToExcel.bind(this));
        $listBahan.$button.$refresh.on('click', this.readDataBahan.bind(this));

        // preview

        $preview.$button.$edit.on('click', function () {
            $tab.$form.trigger('click');
        });

        $preview.$button.$cancel.on('click', function () {
            $tab.$list.trigger('click');
        });

        // form
        $form.$diklatId.on('change', this.showDiklat.bind(this));
        $form.$button.$save.on('click', this.saveData.bind(this));

        $form.$button.$cancel.on('click', function () {
            $tab.$list.trigger('click');
        });

        // form Bahan
        $listBahan.$lokasiId.on('change', this.showTempat.bind(this));
        $listBahan.$button.$save.on('click', this.saveDataBahan.bind(this));
        $listBahan.$button.$cancel.on('click', this.clearDataBahan.bind(this));

    };

    this.showDiklat = function () {
        $loading.show();

        var url = 'Diklat/detailData';
        var data = {
            id: $form.$diklatId.getValue()
        };

        api.post(url, data, function (result) {

            if (result.status == 'success') {
                $form.$pelaksanaanMulai.setValue(result.data.pelaksanaanMulai);
                $form.$jenisDiklat.setValue(result.data.jenisDiklat);
                $form.$pelaksanaanSelesai.setValue(result.data.pelaksanaanSelesai);
            }

            $loading.hide();
        });
    };

    this.showMaPel = function () {
        $loading.show();

        var url = 'MataPelajaran/detailData';
        var data = {
            id: $listBahan.$mataPelajaranId.getValue()
        };

        api.post(url, data, function (result) {

            if (result.status == 'success') {
                $listBahan.$jamMulai.setValue(result.data.jamMulai);
                $listBahan.$jamSelesai.setValue(result.data.jamSelesai);
                $listBahan.$jumlahHari.setValue(result.data.jumlahHari);
                tipeWaktuId = result.data.tipeWaktuId
                $listBahan.$elWaktu.hide();
                $listBahan.$elTanggal.hide();
                var tgl1 = new Date($listBahan.$tanggal.getValue())
                tgl1.setDate(tgl1.getDate() + parseInt(result.data.jumlahHari))
                var tglSampai = tgl1.getFullYear() + '-' + (tgl1.getMonth() + 1) + '-' + tgl1.getDate()
                tglSelesai = tglSampai;

                if (tipeWaktuId == 1) {
                    $listBahan.$elWaktu.show();
                } else {
                    $listBahan.$elTanggal.show();
                    var tglSampai1 = tgl1.getDate() + '-' + (tgl1.getMonth() + 1) + '-' + tgl1.getFullYear()
                    $listBahan.$tanggalSelesai_.setValue(tglSampai1);
                }
            }

            $loading.hide();
        });
    };

    this.showTanggalSelesai = function () {
        var tgl1 = new Date($listBahan.$tanggal.getValue())
        tgl1.setDate(tgl1.getDate() + parseInt($listBahan.$jumlahHari.getValue()))
        var tglSampai = tgl1.getFullYear() + '-' + (tgl1.getMonth() + 1) + '-' + tgl1.getDate()
        tglSelesai = tglSampai;
        var tglSampai1 = tgl1.getDate() + '-' + (tgl1.getMonth() + 1) + '-' + tgl1.getFullYear()
        $listBahan.$tanggalSelesai_.setValue(tglSampai1);
    };


    this.showTempat = function () {
        var lokasiId = $listBahan.$lokasiId.getValue();
        $listBahan.$tempatId.renderApi('Tempat/readData', { lokasiId: lokasiId }, { text: 'tempat' });
    };

    this.driveFieldSequence = function () {
    };

    this.reload = function () {
        $tab.$list.trigger('click');
        this.readData();
        var publishStatusId = 2;
        $list.$jenisDiklatId.renderApi('JenisDiklat/readData', { orderBy: 'position' }, { text: 'jenisDiklat' });
        $list.$diklatId.renderApi('Diklat/readData', { publishStatusId: publishStatusId }, { text: 'namaDiklat' });
        $form.$diklatId.renderApi('Diklat/readDataFilter', { text: 'namaDiklat' });
        $list.$tahun.renderApi('Diklat/readDataTahun', { text: 'tahun' });
        $form.$angkatanId.renderApi('Angkatan/readData', { text: 'angkatan' });
        $listBahan.$tempatId.renderApi('Tempat/readData', { text: 'tempat' });
        $listBahan.$lokasiId.renderApi('Lokasi/readData', { text: 'lokasi' });
        $listBahan.$pengajarDiklatId.renderApi('PengajarDiklat/readDataDashboard', { text: 'nama' });
        $form.$publishStatusId.renderApi('PublishStatus/readData', { text: 'publishStatus' });
        $list.$publishStatusId.renderApi('PublishStatus/readData', { text: 'publishStatus' });
        $listBahan.$mataPelajaranId.renderApi('MataPelajaran/readData',  { text: 'comboMataPelajaran' });

    };

    this.readData = function (callback) {

        if (!callback)
            $loading.show();

        var url = 'Diklat/readData';
        var data = {
            orderBy: 'pelaksanaanMulai',
            reverse: 1,
            publishStatusId: $list.$publishStatusId.getValue(),
            tahun: $list.$tahun.getText(),
            jenisDiklatId: $list.$jenisDiklatId.getValue(),
            diklatId: $list.$diklatId.getValue(),
        };

        api.post(url, data, function (result) {

            rs = result.data;
            var rsCount = rs.length;

            if (rsCount > 0) {
                var showButtons = true;
                $tab.$preview.show();
                $tab.$form.show();
            }
            else {
                var showButtons = false;
                $tab.$preview.hide();
                $tab.$form.hide();
            }

            $tab.$new.show();
            $list.$button.$delete.setEnabled(showButtons);
            $list.$button.$detail.setEnabled(showButtons);
            $list.$button.$edit.setEnabled(showButtons);

            renderedRs = this.renderTable($list.$table, rs, tableFields, rsRange);
            this.renderPaging($list.$paging, renderedRs, rsRange);

            if ($.isFunction(callback))
                callback();
            else
                $loading.hide();
        }.bind(this));
    };

    this.readDataBahan = function (callback) {
        this.clearDataBahan();
        var idJadwal = $list.$table.getValue();
        if (!idJadwal) {
            $msgbox.alert('Data Kosong');
            $tab.$list.on('click', this.readData.bind(this));
            $tab.$list.trigger('click');
        }

        let tgl1 = new Date()
        let tgl = tgl1.getFullYear() + '-' + (tgl1.getMonth() + 1) + '-' + tgl1.getDate()

        $listBahan.$tanggal.setValue(tgl);

        var url = 'Diklat/detailData';
        var data = {
            id: $list.$table.getValue()
        };

        api.post(url, data, function (result) {
            if (result.status == 'success') {
                jenisDiklatIdPengajar = result.data.jenisDiklatId;
                angkatanIdPengajar = result.data.angkatanId;
                var rsG = [];
                result.data.listMapel.forEach(data => {
                    var i = -1;
                    var found = -1;

                    for (var row1 of result.data.listFilterMapel) {
                        i++;
                        if (row1.id == data.id) found = i;
                    }
                    if (found < 0) {
                        rsG.push({
                            value: data.id,
                            text: data.comboMataPelajaran,
                        });
                    }
                });

                $listBahan.$mataPelajaranId.render(rsG);
            }
        });


        if (!callback)
            $loading.show();


        var url = 'BahanDiklat/readData';
        var data = {
            jadwalPengajarId: $list.$table.getValue(),
        };

        api.post(url, data, function (result) {

            // rsBahan = result.data;
            rsBahan = new Array();
            result.data.forEach(function (data1) {
                var date1 = new Date(data1.tanggal);
                var bulan = monthRecords[date1.getMonth()]
                var hari = hariRecords[date1.getDay()]
                var date1Selesai = new Date(data1.tanggalSelesai);
                var bulanSelesai = monthRecords[date1Selesai.getMonth()]
                var hariSelesai = hariRecords[date1Selesai.getDay()]
                var allTanggal, allWaktu, allPengajar, allJam

                if (data1.tipeWaktuId == 1) {
                    allTanggal = hari + ', ' + date1.getDate() + ' ' + bulan + ' ' + date1.getFullYear()
                    allWaktu = data1.jamMulai + ' - ' + data1.jamSelesai
                    allPengajar = data1.namaPengajar
                    allJam = data1.jumlahJp
                } else {
                    allTanggal = hari + ', ' + date1.getDate() + ' ' + bulan + ' ' + date1.getFullYear() + '<br>' + ' s/d ' + '<br>' + hariSelesai + ', ' + date1Selesai.getDate() + ' ' + bulanSelesai + ' ' + date1Selesai.getFullYear()
                    allWaktu = data1.jumlahHari + ' hari'
                    // var a =JSON.parse(data1.listPengajar)
                    var htmlView = '';
                    var noi = 0
                    $.each(data1.listPengajar, function (key, value) {
                        noi++
                        htmlView += noi + '. ' + value.namaPengajar + '<br>';
                    });

                    allPengajar = htmlView
                    allJam = ''
                }

                rsBahan.push({
                    id: data1.id,
                    allWaktu: allWaktu,
                    allTanggal: allTanggal,
                    allPengajar: allPengajar,
                    allJam: allJam,
                    jadwalPengajarId: data1.jadwalPengajarId,
                    pengajarDiklatId: data1.pengajarDiklatId,
                    namaPengajar: data1.namaPengajar,
                    tempatId: data1.tempatId,
                    tempat: data1.tempat,
                    lokasiId: data1.lokasiId,
                    lokasi: data1.lokasi,
                    mataPelajaranId: data1.mataPelajaranId,
                    mataPelajaran: data1.mataPelajaran,
                    noUrut: data1.noUrut,
                    jumlahHari: data1.jumlahHari,
                    tipeWaktuId: data1.tipeWaktuId,
                    tipeWaktu: data1.tipeWaktu,
                    tipeMataPelajaranId: data1.tipeMataPelajaranId,
                    tipeMataPelajaran: data1.tipeMataPelajaran,
                    status: data1.status,
                    tanggal: data1.tanggal,
                    _tanggal: data1._tanggal,
                    tanggalSelesai: data1.tanggalSelesai,
                    _tanggalSelesai: data1._tanggalSelesai,
                    jamMulai: data1.jamMulai,
                    nilaiJamMulai: data1.nilaiJamMulai,
                    _jamMulai: data1._jamMulai,
                    jamMulaiMenit: data1.jamMulaiMenit,
                    jamSelesai: data1.jamSelesai,
                    nilaiJamSelesai: data1.nilaiJamSelesai,
                    _jamSelesai: data1._jamSelesai,
                    jamSelesaiMenit: data1.jamSelesaiMenit,
                    jumlahJam: data1.jumlahJam,
                });

            });

            var rsCount = rsBahan.length;

            if (rsCount > 0) {
                var showButtons = true;
                $listBahan.$button.$excel.show();
            }
            else {
                var showButtons = false;
                $listBahan.$button.$excel.hide();
            }
            saveModeBahan = NEW_MODE
            $tab.$new.hide();
            $tab.$preview.hide();
            $tab.$form.hide();

            $listBahan.$button.$delete.setEnabled(showButtons);



            // renderedRsBahan = this.renderTableNoPaging($listBahan.$table, rsBahan, tableFieldsBahan, rsRange);
            this.renderTableNoPaging($listBahan.$table, rsBahan, tableFieldsBahan, 10000);

            if ($.isFunction(callback))
                callback();
            else
                $loading.hide();
        }.bind(this));
    };

    this.searchData = function () {

        var filteredRs = rs.filter(function (data) {
            var regex = new RegExp($list.$search.getValue(), 'gi');
            var found = false;

            tableFields.forEach(function (field) {
                if (data[field])
                    if (data[field].match(regex))
                        found = true;
            });

            return found;
        });

        renderedRs = this.renderTable($list.$table, filteredRs, tableFields, rsRange);
        this.renderPaging($list.$paging, renderedRs, rsRange);
    };

    this.searchDataBahan = function () {

        var filteredRs = rsBahan.filter(function (data) {
            var regex = new RegExp($listBahan.$search.getValue(), 'gi');
            var found = false;

            tableFieldsBahan.forEach(function (field) {
                if (data[field])
                    if (data[field].match(regex))
                        found = true;
            });

            return found;
        });

        // renderedRsBahan = this.renderTable($listBahan.$table, filteredRs, tableFieldsBahan, rsRange);
        this.renderTableNoPaging($listBahan.$table, filteredRs, tableFieldsBahan, 10000);
    };

    this.moveData = function () {
        renderedRs = this.renderTable($list.$table, renderedRs, tableFields, rsRange, $list.$paging.getValue());
    };

    this.deleteData = function () {
        var checkedId = $list.$table.getCheckedValues();

        if (checkedId.length == 0) {
            $msgbox.alert('NO_DATA_CHECKED');
            return;
        };

        var url = 'Diklat/multipleDeleteData';
        var data = {
            multipleId: checkedId,
        };

        $msgbox.confirm('DELETE_CHECKED_DATA', function () {
            api.post(url, data, function (result) {

                switch (result.status) {
                    case 'success':
                        $splash.show(result.message);
                        this.readData();
                        $form.$diklatId.renderApi('Diklat/readDataFilter', { text: 'namaDiklat' });
                        break;
                    case 'failed':
                        $msgbox.alert(result.message);
                }

            }.bind(this));
        }.bind(this));

    };

    this.deleteDataBahan = function () {
        var checkedId = $listBahan.$table.getCheckedValues();

        if (checkedId.length == 0) {
            $msgbox.alert('NO_DATA_CHECKED');
            return;
        };

        var url = 'BahanDiklat/multipleDeleteData';
        var data = {
            multipleId: checkedId,
        };

        $msgbox.confirm('DELETE_CHECKED_DATA', function () {
            api.post(url, data, function (result) {

                switch (result.status) {
                    case 'success':
                        $splash.show(result.message);
                        this.readDataBahan();

                        break;
                    case 'failed':
                        $msgbox.alert(result.message);
                }

            }.bind(this));
        }.bind(this));

    };

    this.detailData = function () {
        $loading.show();

        var url = 'Diklat/detailData';
        var data = {
            id: $list.$table.getValue(),
        };

        api.post(url, data, function (result) {

            if (result.status == 'success') {
                $preview.$publishStatus.setValue(result.data.publishStatus);
                $preview.$nomor.setValue(result.data.nomor);
                $preview.$pelaksanaanMulai.setValue(result.data.pelaksanaanMulai);
                $preview.$pelaksanaanSelesai.setValue(result.data.pelaksanaanSelesai);
                $preview.$jenisDiklat.setValue(result.data.jenisDiklat);
                $preview.$namaDiklat.setValue(result.data.namaDiklat);
                $preview.$angkatan.setValue(result.data.angkatan);
                rsPengajarD = result.data.listPengajar
                result.data.listBahanJadwal.forEach(data1 => {
                    var date1 = new Date(data1.tanggal);
                    var bulan = monthRecords[date1.getMonth()]
                    var hari = hariRecords[date1.getDay()]
                    var date1Selesai = new Date(data1.tanggalSelesai);
                    var bulanSelesai = monthRecords[date1Selesai.getMonth()]
                    var hariSelesai = hariRecords[date1Selesai.getDay()]
                    var allTanggal, allWaktu, allPengajar, allJam

                    if (data1.tipeWaktuId == 1) {
                        allTanggal = hari + ', ' + date1.getDate() + ' ' + bulan + ' ' + date1.getFullYear()
                        allWaktu = data1.jamMulai + ' - ' + data1.jamSelesai
                        allPengajar = data1.namaPengajar
                        if (data1.jumlahJp == 0)
                            allJam = '-'
                        else
                            allJam = data1.jumlahJp
                    } else {
                        allTanggal = hari + ', ' + date1.getDate() + ' ' + bulan + ' ' + date1.getFullYear() + '<br>' + ' s/d ' + '<br>' + hariSelesai + ', ' + date1Selesai.getDate() + ' ' + bulanSelesai + ' ' + date1Selesai.getFullYear()
                        allWaktu = data1.jumlahHari + ' hari'
                        var htmlView = '';
                        var noi = 0
                        $.each(result.data.listPengajar, function (key, value) {
                            noi++
                            htmlView += noi + '. ' + value.namaPengajar + '<br>';
                        });

                        allPengajar = htmlView
                        allJam = ''
                    }

                    rsBahan.push({
                        id: data1.id,
                        allWaktu: allWaktu,
                        allTanggal: allTanggal,
                        allPengajar: allPengajar,
                        allJam: allJam,
                        jadwalPengajarId: data1.jadwalPengajarId,
                        pengajarDiklatId: data1.pengajarDiklatId,
                        namaPengajar: data1.namaPengajar,
                        tempatId: data1.tempatId,
                        tempat: data1.tempat,
                        lokasiId: data1.lokasiId,
                        lokasi: data1.lokasi,
                        mataPelajaranId: data1.mataPelajaranId,
                        mataPelajaran: data1.mataPelajaran,
                        noUrut: data1.noUrut,
                        jumlahHari: data1.jumlahHari,
                        tipeWaktuId: data1.tipeWaktuId,
                        tipeWaktu: data1.tipeWaktu,
                        tipeMataPelajaranId: data1.tipeMataPelajaranId,
                        tipeMataPelajaran: data1.tipeMataPelajaran,
                        status: data1.status,
                        tanggal: data1.tanggal,
                        _tanggal: data1._tanggal,
                        tanggalSelesai: data1.tanggalSelesai,
                        _tanggalSelesai: data1._tanggalSelesai,
                        jamMulai: data1.jamMulai,
                        nilaiJamMulai: data1.nilaiJamMulai,
                        _jamMulai: data1._jamMulai,
                        jamMulaiMenit: data1.jamMulaiMenit,
                        jamSelesai: data1.jamSelesai,
                        nilaiJamSelesai: data1.nilaiJamSelesai,
                        _jamSelesai: data1._jamSelesai,
                        jamSelesaiMenit: data1.jamSelesaiMenit,
                        jumlahJam: data1.jumlahJam,
                    });
                });
                this.renderViewCart();

            }

            $loading.hide();
        }.bind(this));
    };

    this.serializeData = function () {

        var data = {
            publishStatusId: $form.$publishStatusId.getValue(),
            nomor: $form.$nomor.getValue(),
            diklatId: $form.$diklatId.getValue(),
            angkatanId: $form.$angkatanId.getValue(),
        };

        if (saveMode == EDIT_MODE)
            data.id = $list.$table.getValue();

        return data;
    };

    this.serializeDataBahan = function () {

        var data = {
            jadwalPengajarId: $list.$table.getValue(),
            pengajarDiklatId: $listBahan.$pengajarDiklatId.getValue(),
            mataPelajaranId: $listBahan.$mataPelajaranId.getValue(),
            tempatId: $listBahan.$tempatId.getValue(),
            tanggal: $listBahan.$tanggal.getValue(),
            tanggalSelesai: tglSelesai,
            listPengajar: rsPengajar,
        };

        if (saveModeBahan == EDIT_MODE)
            data.id = $listBahan.$table.getValue();

        return data;
    };

    this.clearData = function () {
        $form.clear();

        $form.$publishStatusId.setValue(2);
        $form.$nomor.clear();
        $form.$pelaksanaanMulai.clear();
        $form.$jenisDiklat.clear();
        $form.$pelaksanaanSelesai.clear();
        $form.$diklatId.clear();
        $form.$angkatanId.clear();

    };

    this.clearDataBahan = function () {
        $listBahan.clear();
        $listBahan.$elWaktu.hide();
        $listBahan.$elTanggal.hide();
        let tgl1 = new Date()
        let tgl = tgl1.getFullYear() + '-' + (tgl1.getMonth() + 1) + '-' + tgl1.getDate()

        $listBahan.$tanggal.setValue(tgl);

        $listBahan.$mataPelajaranId.clear();
        $listBahan.$lokasiId.clear();
        $listBahan.$tempatId.clear();
        $listBahan.$pengajarDiklatId.clear();
        $listBahan.$jamMulai.clear();
        $listBahan.$jamSelesai.clear();
        saveMode = NEW_MODE;
    };

    this.editData = function () {
        $loading.show();

        this.clearData();
        saveMode = EDIT_MODE;

        var url = 'Diklat/detailData';
        var data = {
            id: $list.$table.getValue(),
        };

        api.post(url, data, function (result) {

            if (result.status == 'success') {

                $form.$publishStatusId.setValue(result.data.publishStatusId);
                $form.$nomor.setValue(result.data.nomor);
                $form.$pelaksanaanMulai.setValue(result.data.pelaksanaanMulai);
                $form.$pelaksanaanSelesai.setValue(result.data.pelaksanaanSelesai);
                $form.$jenisDiklat.setValue(result.data.jenisDiklat);
                $form.$diklatId.setValue(result.data.diklatId);
                $form.$angkatanId.setValue(result.data.angkatanId);

                $form.$nomor.focus();
            }

            $loading.hide();
        }.bind(this));
    };

    this.editDataBahan = function () {
        $loading.show();
        saveModeBahan = EDIT_MODE;

        this.clearData();

        var url = 'BahanDiklat/detailData';
        var data = {
            id: $listBahan.$table.getValue(),
        };

        api.post(url, data, function (result) {

            if (result.status == 'success') {

                $listBahan.$pengajarDiklatId.setValue(result.data.pengajarDiklatId);
                $listBahan.$lokasiId.setValue(result.data.lokasiId);
                $listBahan.$tempatId.setValue(result.data.tempatId);
                $listBahan.$tanggal.setValue(result.data.tanggal);
                $listBahan.$jamMulai.setValue(result.data._jamMulai);
                $listBahan.$jamSelesai.setValue(result.data._jamSelesai);

                var mataPelajaranId = result.data.mataPelajaranId;
                var rsG = [];
                result.data.listMapel.forEach(data => {
                    var i = -1;
                    var found = -1;

                    for (var row1 of result.data.listFilterMapel) {
                        i++;
                        if (row1.id == data.id || data.id == mataPelajaranId) found = i;

                    }

                    if (found < 0 || data.id == mataPelajaranId) {
                        rsG.push({
                            value: data.id,
                            text: data.comboMataPelajaran,
                        });
                    }

                });

                $listBahan.$mataPelajaranId.render(rsG);
                $listBahan.$mataPelajaranId.setValue(mataPelajaranId);

                var tipeWaktuId = result.data.tipeWaktuId
                $listBahan.$elWaktu.hide();
                $listBahan.$elTanggal.hide();

                if (tipeWaktuId == 1) {
                    $listBahan.$elWaktu.show();
                } else {
                    $listBahan.$elTanggal.show();
                }

                rsPengajar = new Array();
                result.data.listPengajar.forEach(function (data1) {

                    rsPengajar.push({
                        id: data1.id,
                        bahanDiklatId: data1.bahanDiklatId,
                        pengajarDiklatId: data1.pengajarDiklatId,
                        namaPengajar: data1.namaPengajar,
                    });

                });
                this.renderEditCart();

                $listBahan.$pengajarDiklatId.focus();
            }

            $loading.hide();
        }.bind(this));
    };


    this.newData = function () {
        saveMode = NEW_MODE;

        this.clearData();
        $form.$nomor.focus();
    };


    this.saveData = function () {
        $form.$button.$save.loading();

        switch (saveMode) {
            case EDIT_MODE:
                var url = 'Diklat/updateData';
                break;
            case NEW_MODE:
                var url = 'Diklat/createData';
        }

        var data = this.serializeData();

        api.post(url, data, function (result) {

            switch (result.status) {
                case 'success':
                    $splash.show(result.message);
                    $form.$diklatId.renderApi('Diklat/readDataFilter', { text: 'namaDiklat' });
                    this.readData(function () {
                        switch (saveMode) {
                            case EDIT_MODE:
                                $tab.$list.trigger('click');
                                break;
                            case NEW_MODE:
                                this.clearData();
                                $form.$nomor.focus();
                        }

                        $form.$button.$save.release();
                        window.scrollTo(0, 0);
                    }.bind(this));

                    break;
                case 'invalid':
                    $form.error(result);
                    $form.$button.$save.release();
            }

        }.bind(this));
    };

    this.saveDataBahan = function () {

        $listBahan.$button.$save.loading();

        switch (saveModeBahan) {
            case EDIT_MODE:
                var url = 'BahanDiklat/updateData';
                break;
            case NEW_MODE:
                var url = 'BahanDiklat/createData';
        }

        var data = this.serializeDataBahan();
        rsPengajar = new Array();
        this.renderEditCart();
        api.post(url, data, function (result) {

            switch (result.status) {
                case 'success':
                    $splash.show(result.message);

                    this.readDataBahan(function () {
                        switch (saveModeBahan) {
                            case EDIT_MODE:
                                $tab.$listBahan.trigger('click');
                                saveModeBahan = NEW_MODE
                                break;
                            case NEW_MODE:
                                // this.clearDataBahan();
                                saveModeBahan = NEW_MODE
                                $listBahan.$pengajarDiklatId.focus();
                        }
                        $listBahan.$button.$save.release();
                        window.scrollTo(0, 0);
                    }.bind(this));

                    break;
                case 'invalid':
                    $listBahan.error(result);
                    $listBahan.$button.$save.release();
            }

        }.bind(this));
    };

    this.addToCart = function () {

        if (!$listBahan.$pengajarDiklatId.getValue()) {
            $msgbox.alert('Pengajar Diklat diperlukan');
            return;
        }

        var found = false;
        rsPengajar.forEach(function (data) {

            if (data.pengajarDiklatId == $listBahan.$pengajarDiklatId.getValue())
                found = true;
        });

        if (found) {
            $msgbox.alert('Pengajar Diklat sudah terdaftar ');
            return;
        }

        rsPengajar.push({
            id: this.guid(),
            pengajarDiklatId: $listBahan.$pengajarDiklatId.getValue(),
            namaPengajar: $listBahan.$pengajarDiklatId.getText(),
        });
        ;

        $listBahan.$pengajarDiklatId.focus();
        this.renderEditCart();
    };

    this.renderDataTable = function ($table, dataSource, keyFields, range, pageActive) {

        if ($(window).height() > 900)
            range = 15;

        if (!pageActive)
            var pageActive = 1;

        var rowActive = (pageActive - 1) * range;
        var rows = [];

        for (var i = rowActive; i < dataSource.length; i++) {
            var row = [];
            var field = dataSource[i];

            if (field) {

                keyFields.forEach(function (keyField) {
                    row.push(field[keyField]);
                });

                rows.push({
                    id: field.id,
                    noUrut: field.noUrut,
                    tipeWaktuId: field.tipeWaktuId,
                    tipeMataPelajaranId: field.tipeMataPelajaranId,
                    allTanggal: field.allTanggal,
                    allWaktu: field.allWaktu,
                    pengajarId: '',
                    mataPelajaran: field.mataPelajaran,
                    allJam: field.allJam,
                });
            }
        }
        var tableAnak = `<table border=5 bordercolor=green>
        <tr>
          <td>
            First row of Inner Table
          </td>
        </tr>
        <tr>
          <td>
            Second row of Inner Table
          </td>
        </tr>
      </table>`;
        var tableHeadsHtml = '';
        var select1 = '';
        var ol1 = '';
        tableHeadsHtml += `<th style="display:none;" >Id</th>`;
        tableHeadsHtml += `<th style="display:none;" >TipeWaktu</th>`;
        tableHeadsHtml += `<th style="display:none;" >TipeMapel</th>`;
        for (tableHead of viewHeadsBahan) {
            tableHeadsHtml += `<th>${tableHead}</th>`;
        }

        var html = `
        <table id="example">
        <thead>
            <tr>
                ${tableHeadsHtml}
                <th>Aksi</th>
            </tr>
            
            
        
        </thead>
        <tbody>
        `;
        for (row of rows) {

            html += '<tr style="cursor: pointer">';
            html += `<td style="display:none; border-bottom: solid silver 1px">${row.id}</td>`;
            html += `<td style="display:none; border-bottom: solid silver 1px">${row.tipeWaktuId}</td>`;
            html += `<td style="display:none; border-bottom: solid silver 1px">${row.tipeMataPelajaranId}</td>`;
            html += `<td style="border-bottom: solid silver 1px">${row.noUrut}</td>`;
            html += `<td data-ref="tanggal" style="border-bottom: solid silver 1px"><input value="${row.tanggal}" class="datepicker input-width" placeholder="Tanggal" type="text" style="width:75px!important;" ></td>`;
            html += `<td style="border-bottom: solid silver 1px">${row.allWaktu}</td>`;
            html += `<td style="border-bottom: solid silver 1px">${row.mataPelajaran}</td>`;
            
            for (column of rsPengajarD) {
                select1 += `<option value="${column['id']}">
                 ${column['nama']}
            </option>
            `;
            }
            rsPengajarD = [];
            if(row.tipeWaktuId==2)
            ol1 +=  `<ol>
            <li>Coffee</li>
            <li>Tea</li>
            <li>Milk</li>
            </ol> `;
            html += `<td style="border-bottom: solid silver 1px">${row.pengajarId}<select class="cpengajar" data-ref="pengjarId">${select1}</select>
            ${ol1}
            </td>`;
            html += `<td style="border-bottom: solid silver 1px">${row.allJam}</td>`;

            html += `<td style="width: 140px">
                <button class="act-simpan" data-id="${row.id}" href="javascript:">
                    Simpan
                </button>
                
               
            </td>`;
            html += '</tr>';
        }

        html += '</tbody></table>'

        $table.html(html);
        $table.find('table').DataTable({
            searching: true, "paging": false,
            select: true,
            "ordering": true,
            "info": false,
            columnDefs: [{
                orderable: false,
                targets: [1, 2, 3, 4, 5]
            }]
        });
        $input = $table.find('input.datepicker');
        $input.datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd/mm/yy',
            yearRange: '-' + config.ui.pastYearRange + ':+' + config.ui.futureYearRange,

        });
        // $table.getRef('tanggal').uiDatePicker();

        var foundCari = false;
        var table = $('#example').DataTable();

        $('#example tbody').on('click', 'tr', function () {
            $(this).toggleClass('selected');
        });

        $('#example tbody').on('click', 'tr', function () {
            var rowData = table.row(this).data();
            idMapelSelect = rowData[0]
            foundCari == false
            rsIsiMapel.forEach(function (data) {
                if (data.id == idMapelSelect)
                    foundCari = true;
            });

            if (foundCari == false) {
                rsIsiMapel.push({
                    id: idMapelSelect,
                    tglMulai: '',
                    pengajarId: '',
                });
            }

            $table.find("select.cpengajar").change(function () {
                idPengajarSelect = $(this).children("option:selected").val();
                if (foundCari)
                    $.each(rsIsiMapel, function (i, row) {
                        if (row.id == idMapelSelect) {
                            rsIsiMapel[i].pengajarId = idPengajarSelect;
                        }
                    });
            });

            $table.find('input.datepicker').change(function () {
                idTglSelect = $(this).val();
                console.log(idTglSelect)
                if (foundCari)
                    $.each(rsIsiMapel, function (i, row) {
                        if (row.id == idMapelSelect) {
                            rsIsiMapel[i].tglMulai = idTglSelect;
                        }
                    });
            });

        });

        $table.find('.act-simpan').click(function () {
            var idData = $(event.target).data('id');
            console.log(rsIsiMapel)
            return false;
        });

        return dataSource;
    };

    this.cobaDong = function () {
        alert('test')
    };

    this.renderViewCart = function () {
        this.renderDataTable($preview.$listBahan, rsBahan, viewFieldsBahan, 1000);
        // this.renderTableNoPaging($preview.$listBahan, rsBahan, viewFieldsBahan, 1000);
    };

    this.renderEditCart = function () {
        this.renderTableNoPaging($listBahan.$listPengajar, rsPengajar, cartFields, 1000);
    };

    this.deleteFromCart = function () {
        var checkedId = $listBahan.$listPengajar.getCheckedValues();
        if (checkedId.length <= 0)
            return;

        $msgbox.confirm('Delete selected data', function () {

            checkedId.forEach(function (indexId) {
                var i = -1;

                rsPengajar.forEach(function (data) {
                    i++;

                    if (data.id == indexId)
                        rsPengajar.splice(i, 1);

                });
            });

            this.renderEditCart();
        }.bind(this));

    };

    this.exportData = function (type, url, id) {

        $loading.show();

        var data = {};
        if (id)
            data.id = id;

        api.post(url, data, function (result) {

            $loading.hide();

            switch (result.status) {
                case 'success':

                    if (type == 'pdf')
                        router.open(result.data);
                    else
                        router.redirect(result.data);

                    break;
                case 'failed':
                    $msgbox.alert(result.message);
            }
        });
    };

    this.exportDetailToExcel = function () {
        var type = 'excel';
        var url = 'Diklat/createExcelDetail';
        var id = $list.$table.getValue();

        this.exportData(type, url, id);
    };

};

dashboard.application.controllers.Diklat.prototype =
    Object.create(dashboard.application.core.Controller.prototype);

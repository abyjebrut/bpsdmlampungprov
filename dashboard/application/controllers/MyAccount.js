dashboard.application.controllers.MyAccount = function () {
    var $el = $page.getScope('MyAccount');
    dashboard.application.core.Controller.call(this, $el);


    var rsMapel = [];
    var cartHeads = ['Jenis Diklat', 'Mata Pelajaran'];
    var cartFields = ['namaSingkat', 'labelMapel'];

    var rsLampiran = [];
    var cartHeadsLampiran = ['Upload'];
    var cartFieldsLampiran = ['judul'];

    var $tab;
    var $preview;
    var $form;
    var $change_password;

    this.init = function () {

        // tab

        $tab = $el.find('#tab');
        $tab.$preview = $tab.getRef('preview');
        $tab.$form = $tab.getRef('form');
        $tab.$change_password = $tab.getRef('change_password');

        // preview

        $preview = $el.find('#preview');

        $preview.$elPengajar = $preview.getRef('elPengajar');
        $preview.$elPengajar.hide();
        $preview.$pangkatGolongan = $preview.getRef('pangkatGolongan').uiTextView();
        $preview.$jabatan = $preview.getRef('jabatan').uiTextView();
        $preview.$jenisPengajar = $preview.getRef('jenisPengajar').uiTextView();

        $preview.$gender = $preview.getRef('gender').uiTextView();
        $preview.$dateOfBirth = $preview.getRef('dateOfBirth').uiTextView();
        $preview.$name = $preview.getRef('name').uiTextView();
        $preview.$nip = $preview.getRef('nip').uiTextView();
        $preview.$photo = $preview.getRef('photo').uiImageView();
        $preview.$email = $preview.getRef('email').uiTextView();
        $preview.$mobileNumber = $preview.getRef('mobileNumber').uiTextView();
        $preview.$userPrivileges = $preview.getRef('userPrivileges').uiTextView();
        $preview.$address = $preview.getRef('address').uiTextAreaView();
        $preview.$aktivasi = $preview.getRef('aktivasi').uiTextView();
        $preview.$tipeUser = $preview.getRef('tipeUser').uiTextView();

        $preview.$listMapel = $preview.getRef('listMapel').uiTableView({ headers: cartHeads });
        $preview.$listLampiran = $preview.getRef('listLampiran').uiTableView(cartHeadsLampiran);

        $preview.$button = $preview.find('#button');
        $preview.$button.$edit = $preview.$button.getRef('edit').uiButton();
        $preview.$button.$cancel = $preview.$button.getRef('cancel').uiButton();
        $preview.$button.$pdf = $preview.$button.getRef('pdf').uiButton();
        $preview.$button.$excel = $preview.$button.getRef('excel').uiButton();

        // form

        $form = $el.find('#form').uiForm();

        $form.$name = $form.getRef('name').uiTextBox({ placeholder: 'Ditulis beserta gelar bila ada' });
        $form.$nip = $form.getRef('nip').uiTextBox();
        $form.$photo = $form.getRef('photo').uiUploadImage();
        $form.$email = $form.getRef('email').uiTextBox();
        $form.$mobileNumber = $form.getRef('mobileNumber').uiTextBox();
        $form.$genderId = $form.getRef('genderId').uiRadioBox();
        $form.$dateOfBirth = $form.getRef('dateOfBirth').uiDatePicker();
        $form.$elPengajar = $form.getRef('elPengajar');
        $form.$elPengajar.hide();
        $form.$elHide = $form.getRef('elHide');
        $form.$elHide.hide();
        $form.$pangkatGolonganId = $form.getRef('pangkatGolonganId').uiComboBox();
        $form.$jabatanId = $form.getRef('jabatanId').uiComboBox();
        $form.$jenisPengajarId = $form.getRef('jenisPengajarId').uiComboBox();

        $form.$address = $form.getRef('address').uiTextArea();

        $form.$jenisDiklatId = $form.getRef('jenisDiklatId').uiComboBox();
        $form.$labelMapelId = $form.getRef('labelMapelId').uiComboBox();
        $form.$listMapel = $form.getRef('listMapel').uiTable(cartHeads);
        $form.$add = $form.getRef('add');
        $form.$edit = $form.getRef('edit');
        $form.$delete = $form.getRef('delete');

        $form.$judul = $form.getRef('judul').uiTextBox();
        $form.$lampiran = $form.getRef('lampiran').uiUploadFilePdf();
        $form.$listLampiran = $form.getRef('listLampiran').uiTable(cartHeadsLampiran);
        $form.$addLampiran = $form.getRef('addLampiran');
        $form.$deleteLampiran = $form.getRef('deleteLampiran');
        $form.$deleteLampiran.hide();


        $form.$button = $form.find('#button');
        $form.$button.$save = $form.$button.getRef('save').uiButton();
        $form.$button.$cancel = $form.$button.getRef('cancel').uiButton();

        // change_password

        $change_password = $el.find('#change_password').uiForm();
        $change_password.$name = $change_password.getRef('name').uiTextView();
        $change_password.$email = $change_password.getRef('email').uiTextView();
        $change_password.$old_password = $change_password.getRef('old_password').uiTextBox({ type: TYPE_PASSWORD });
        $change_password.$new_password = $change_password.getRef('new_password').uiTextBox({ type: TYPE_PASSWORD });
        $change_password.$retype_password = $change_password.getRef('retype_password').uiTextBox({ type: TYPE_PASSWORD });

        $change_password.$button = $change_password.find('#button');
        $change_password.$button.$save = $change_password.$button.getRef('save').uiButton();
        $change_password.$button.$cancel = $change_password.$button.getRef('cancel').uiButton();

        // dialog

        $form.$editDialog = $form.getRef('editDialog');

        $form.$editDialog.$dialogJenisDiklatId = $form.$editDialog.getRef('dialogJenisDiklatId').uiComboBox();
        $form.$editDialog.$dialogLabelMapelId = $form.$editDialog.getRef('dialogLabelMapelId').uiComboBox();

        $form.$editDialog.$button = $form.$editDialog.find('.actions');

        $form.$editDialog.$button.$save = $form.$editDialog.$button.getRef('save').uiButton();
        $form.$editDialog.$button.$cancel = $form.$editDialog.$button.getRef('cancel').uiButton();

        // end dialog
    };

    this.driveEvents = function () {
        // tab

        $tab.$form.on('click', this.editData.bind(this));
        $tab.$change_password.on('click', this.detailPassword.bind(this));

        // preview

        $preview.$button.$edit.on('click', function () {
            $tab.$form.trigger('click');
        });

        $preview.$button.$pdf.on('click', this.exportDetailToPdf.bind(this));
        $preview.$button.$excel.on('click', this.exportDetailToExcel.bind(this));

        // form
        $form.$jenisDiklatId.on('change', this.showMapel.bind(this));
        $form.$add.on('click', this.addToCart.bind(this));
        $form.$edit.on('click', this.editCart.bind(this));
        $form.$delete.on('click', this.deleteFromCart.bind(this));
        $form.$editDialog.$button.$save.on('click', this.saveCart.bind(this));
        $form.$editDialog.$button.$cancel.on('click', this.clearEditDataMapel.bind(this));

        $form.$addLampiran.on('click', this.addToCartLampiran.bind(this));
        $form.$deleteLampiran.on('click', this.deleteFromCartLampiran.bind(this));

        $form.$button.$save.on('click', this.saveData.bind(this));
        $form.$button.$cancel.on('click', function () {
            $tab.$preview.trigger('click');
        });

        // change_password

        $change_password.$button.$save.on('click', this.savePassword.bind(this));

        $change_password.$button.$cancel.on('click', function () {
            $tab.$preview.trigger('click');
        });
    };
    this.driveFieldSequence = function () {

        $change_password.$old_password.on('enter', function () {
            $change_password.$new_password.focus();
        });

        $change_password.$new_password.on('enter', function () {
            $change_password.$retype_password.focus();
        });

        $change_password.$retype_password.on('enter', function () {
            $change_password.$button.$save.trigger('click');
        });
    };

    this.reload = function () {
        $tab.$preview.trigger('click');



    };

    this.showMapel = function () {
        // var jenisDiklatId =$form.$jenisDiklatId.getValue(); 
        // $form.$labelMapelId.renderApi('MataPelajaran/readFilterLabelMapel', { jenisDiklatId:jenisDiklatId  }, { value: 'labelMapelId', text: 'mataPelajaran' });
    };

    this.showDialogMapel = function () {
        // var jenisDiklatId =$form.$editDialog.$dialogJenisDiklatId.getValue(); 
        // $form.$editDialog.$dialogLabelMapelId.renderApi('MataPelajaran/readFilterLabelMapel', { jenisDiklatId:jenisDiklatId  }, { value: 'labelMapelId', text: 'mataPelajaran' });
    };


    this.detailData = function (callback) {
        $loading.show();
        // $form.$labelMapelId.renderApi('LabelMapel/readData', {text: 'labelMapel'});
        $form.$jenisDiklatId.renderApi('JenisDiklat/readData', { text: 'namaSingkat' });

        $form.$editDialog.$dialogJenisDiklatId.renderApi('JenisDiklat/readData', { text: 'namaSingkat' });
        // $form.$editDialog.$dialogLabelMapelId.renderApi('LabelMapel/readData', {text: 'labelMapel'});

        $form.$genderId.renderApi('Gender/readData', { text: 'gender' });
        $form.$pangkatGolonganId.renderApi('PangkatGolongan/readData', { text: 'pangkatGolongan' });
        $form.$jabatanId.renderApi('Jabatan/readData', { text: 'jabatan' });
        $form.$jenisPengajarId.renderApi('JenisPengajar/readData', { text: 'jenisPengajar' });
        $form.$labelMapelId.renderApi('LabelMapel/readData', { text: 'labelMapel' });
        $form.$editDialog.$dialogLabelMapelId.renderApi('LabelMapel/readData', { text: 'labelMapel' });

        var url = 'Account/detailData';

        api.post(url, function (result) {

            if (result.status == 'success') {
                $preview.$pangkatGolongan.setValue(result.data.pangkatGolongan);
                $preview.$jabatan.setValue(result.data.jabatan);
                $preview.$jenisPengajar.setValue(result.data.jenisPengajar);
                $preview.$name.setValue(result.data.name);
                $preview.$nip.setValue(result.data.nip);
                $preview.$photo.setValue(result.data.photo);
                $preview.$email.setValue(result.data.email);
                $preview.$gender.setValue(result.data.gender);
                $preview.$dateOfBirth.setValue(result.data.dateOfBirth);
                $preview.$mobileNumber.setValue(result.data.mobileNumber);
                $preview.$userPrivileges.setValue(result.data.privileges);
                $preview.$address.setValue(result.data.address);
                $preview.$aktivasi.setValue(result.data.aktivasi);
                $preview.$tipeUser.setValue(result.data.tipeUser);
                $preview.$elPengajar.hide();

                if (result.data.tipeUserId == 2) {
                    $preview.$elPengajar.show()
                    rsMapel = result.data.listMapel;
                    this.renderViewCart();

                    if (result.data.listLampiran.length > 0) {
                        rsLampiran = new Array();
                        result.data.listLampiran.forEach(function (data1) {
                            var lampiranUrl = '../asset/archive/' + data1.lampiran;
                            var htmlUrl = '<a href="' + lampiranUrl + '" target="_blank" download>' + data1.judul + '</a>';

                            rsLampiran.push({
                                id: data1.id,
                                sppdId: data1.sppdId,
                                judul: htmlUrl,
                                lampiran: data1.lampiran,
                            });

                        });
                    } else
                        rsLampiran = result.data.listLampiran;

                    this.renderViewCartLampiran();
                }
            }

            $loading.hide();
        }.bind(this));
    };

    this.serializeData = function () {
        var data = {
            name: $form.$name.getValue(),
            nip: $form.$nip.getValue(),
            photo: $form.$photo.getValue(),
            pangkatGolonganId: $form.$pangkatGolonganId.getValue(),
            jabatanId: $form.$jabatanId.getValue(),
            jenisPengajarId: $form.$jenisPengajarId.getValue(),
            email: $form.$email.getValue(),
            genderId: $form.$genderId.getValue(),
            dateOfBirth: $form.$dateOfBirth.getValue(),
            mobileNumber: $form.$mobileNumber.getValue(),
            address: $form.$address.getValue(),
            listMapel: rsMapel,
            listLampiran: rsLampiran,
        };

        return data;
    };

    this.clearData = function () {
        $form.clear();
        var date = new Date();
        var tgl = `${date.getFullYear()}-${date.getDate()}-${date.getMonth() + 1}`;

        $form.$dateOfBirth.setValue(tgl);
        $form.$name.clear();
        $form.$nip.clear();
        $form.$photo.clear();
        $form.$email.clear();
        $form.$genderId.setValue(1);
        $form.$pangkatGolonganId.clear();
        $form.$jabatanId.clear();
        $form.$jenisPengajarId.clear();
        $form.$mobileNumber.clear();
        $form.$address.clear();
        $form.$jenisDiklatId.clear();
        $form.$labelMapelId.clear();

        rsMapel = new Array();
        this.renderEditCart();

        rsLampiran = new Array();
        this.renderEditCartLampiran();

    };

    this.editData = function () {
        $loading.show();

        this.clearData();

        var url = 'Account/detailData';

        api.post(url, function (result) {

            if (result.status == 'success') {
                $form.$name.setValue(result.data.name);
                $form.$nip.setValue(result.data.nip);
                $form.$photo.setValue(result.data.photo);
                $form.$email.setValue(result.data.email);
                $form.$pangkatGolonganId.setValue(result.data.pangkatGolonganId);
                $form.$jabatanId.setValue(result.data.jabatanId);
                $form.$jenisPengajarId.setValue(result.data.jenisPengajarId);
                $form.$genderId.setValue(result.data.genderId);
                $form.$dateOfBirth.setValue(result.data._dateOfBirth);
                $form.$mobileNumber.setValue(result.data.mobileNumber);
                $form.$address.setValue(result.data.address);
                $form.$elPengajar.hide()
                $form.$deleteLampiran.hide();
                $form.$delete.hide();
                if (result.data.tipeUserId == 2) {
                    $form.$elPengajar.show()
                    rsMapel = result.data.listMapel;

                    if (rsMapel.length > 0) {
                        $form.$delete.show();
                        $form.$edit.show();
                    }
                    this.renderEditCart();
                    rsLampiran = result.data.listLampiran;
                    this.renderEditCartLampiran();

                    if (rsLampiran.length > 0)
                        $form.$deleteLampiran.show();
                }
                $form.$name.focus();
            }

            $loading.hide();
        }.bind(this));
    };

    this.saveData = function () {
        $form.$button.$save.loading();

        var url = 'Account/saveData';
        var data = this.serializeData();

        api.post(url, data, function (result) {

            switch (result.status) {
                case 'success':
                    $splash.show(result.message);

                    $tab.$preview.trigger('click');
                    $form.$button.$save.release();
                    // this.detailData(function () {

                    //     // window.scrollTo(0, 0);
                    // }.bind(this));

                    break;
                case 'invalid':
                    $form.error(result);
                    $form.$button.$save.release();
            }

        }.bind(this));
    };

    this.addToCart = function () {

        if (!$form.$jenisDiklatId.getValue()) {
            $msgbox.alert('Jenis Diklat belum diisi');
            return;
        }

        if (!$form.$labelMapelId.getValue()) {
            $msgbox.alert('Mata Pelajaran belum diisi');
            return;
        }


        var found = false;
        rsMapel.forEach(function (data) {

            if (data.labelMapelId == $form.$labelMapelId.getValue() && data.jenisDiklatId == $form.$jenisDiklatId.getValue())
                found = true;
        });

        if (found) {
            $msgbox.alert('Mata Pelajaran sudah ada');
            return;
        }

        rsMapel.push({
            id: this.guid(),
            labelMapelId: $form.$labelMapelId.getValue(),
            labelMapel: $form.$labelMapelId.getText(),
            jenisDiklatId: $form.$jenisDiklatId.getValue(),
            namaSingkat: $form.$jenisDiklatId.getText(),
        });

        $form.$jenisDiklatId.clear();
        $form.$labelMapelId.clear();
        $form.$jenisDiklatId.focus();

        $form.$delete.show();
        $form.$edit.show();
        this.renderEditCart();
    };

    this.clearEditDataMapel = function () {
        $form.$editDialog.$dialogJenisDiklatId.clear();
        $form.$editDialog.$dialogLabelMapelId.clear();

        $form.$editDialog.modal('hide');

    };

    this.editCart = function () {

        var id = $form.$listMapel.getValue();

        if (!id)
            return;


        $form.$editDialog.$dialogJenisDiklatId.clear();
        $form.$editDialog.$dialogLabelMapelId.clear();

        var rowMapel = [];

        $.each(rsMapel, function (i, row) {
            if (row.id == id)
                rowMapel = row;
        });



        $form.$editDialog.$dialogJenisDiklatId.renderApi('JenisDiklat/readData', {
            text: 'namaSingkat', done: function () {
                $form.$editDialog.$dialogJenisDiklatId.setValue(rowMapel.jenisDiklatId);
            }
        });
        var jenisDiklatId = rowMapel.jenisDiklatId;

        $form.$editDialog.$dialogLabelMapelId.renderApi('MataPelajaran/readFilterLabelMapel', { jenisDiklatId: jenisDiklatId }, {
            value: 'labelMapelId', text: 'mataPelajaran', done: function () {
                $form.$editDialog.$dialogLabelMapelId.setValue(rowMapel.labelMapelId);
            }
        });

        $form.$editDialog.modal('setting', 'closable', false).modal('show');
    };

    this.saveCart = function () {
        var id = $form.$listMapel.getValue();

        $.each(rsMapel, function (i, row) {
            if (row.id == id) {

                rsMapel[i].jenisDiklatId = $form.$editDialog.$dialogJenisDiklatId.getValue();
                rsMapel[i].namaSingkat = $form.$editDialog.$dialogJenisDiklatId.getText();
                rsMapel[i].labelMapelId = $form.$editDialog.$dialogLabelMapelId.getValue();
                rsMapel[i].labelMapel = $form.$editDialog.$dialogLabelMapelId.getText();

            }
        });

        this.renderEditCart();
        this.clearEditDataMapel();
    };

    this.deleteFromCart = function () {
        var checkedId = $form.$listMapel.getCheckedValues();
        if (checkedId.length <= 0)
            return;

        $msgbox.confirm('Delete selected data', function () {

            checkedId.forEach(function (indexId) {
                var i = -1;

                rsMapel.forEach(function (data) {
                    i++;

                    if (data.id == indexId)
                        rsMapel.splice(i, 1);

                });
            });

            if (rsMapel.length <= 0) {
                $form.$delete.hide();
                $form.$edit.hide();
            }
            this.renderEditCart();
        }.bind(this));

    };

    this.renderEditCart = function () {
        this.renderTableNoPaging($form.$listMapel, rsMapel, cartFields, 1000);
    };

    this.renderViewCart = function () {
        this.renderTableNoPaging($preview.$listMapel, rsMapel, cartFields, 1000);
    };

    this.renderViewCartLampiran = function () {
        this.renderTableNoPaging($preview.$listLampiran, rsLampiran, cartFieldsLampiran, 100);
    };

    this.renderEditCartLampiran = function () {
        this.renderTableNoPaging($form.$listLampiran, rsLampiran, cartFieldsLampiran, 1000);
    };

    this.addToCartLampiran = function () {

        if (!$form.$judul.getValue()) {
            $msgbox.alert('Judul diperlukan');
            return;
        }

        if (!$form.$lampiran.getValue()) {
            $msgbox.alert('Lampiran diperlukan');
            return;
        }

        rsLampiran.push({
            id: this.guid(),
            lampiran: $form.$lampiran.getValue(),
            judul: $form.$judul.getValue(),
        });

        $form.$judul.focus();
        $form.$judul.clear();
        $form.$lampiran.clear();
        $form.$deleteLampiran.show();

        this.renderEditCartLampiran();
    };

    this.deleteFromCartLampiran = function () {
        var checkedId = $form.$listLampiran.getCheckedValues();

        if (checkedId.length <= 0) {
            $msgbox.alert('Tidak ada yang dipilih ')
            return;
        }

        $msgbox.confirm('Delete selected data', function () {

            checkedId.forEach(function (indexId) {
                var i = -1;
                rsLampiran.forEach(function (data) {

                    i++;
                    if (data.id == indexId)
                        rsLampiran.splice(i, 1);

                    var url = 'User/deleteDataLampiran';
                    var data = {
                        lampiran: data.lampiran
                    };

                    api.post(url, data, function () { });

                });
            });

            if (rsLampiran.length <= 0)
                $form.$deleteLampiran.hide();

            this.renderEditCartLampiran();
        }.bind(this));

    };

    this.exportData = function (type, url, id) {

        $loading.show();

        api.post(url, function (result) {

            $loading.hide();

            switch (result.status) {
                case 'success':

                    if (type == 'pdf')
                        router.open(result.data);
                    else
                        router.redirect(result.data);

                    break;
                case 'failed':
                    $msgbox.alert(result.message);
            }
        });
    };

    this.exportDetailToPdf = function () {
        var type = 'pdf';
        var url = 'Account/createPDFDetail';

        this.exportData(type, url);
    };

    this.exportDetailToExcel = function () {
        var type = 'xls';
        var url = 'Account/createExcelDetail';

        this.exportData(type, url);
    };

    this.detailPassword = function () {
        $loading.show();

        $change_password.clear();
        $change_password.$old_password.clear();
        $change_password.$new_password.clear();
        $change_password.$retype_password.clear();

        var url = 'Account/detailData';

        api.post(url, function (result) {

            if (result.status == 'success') {
                $change_password.$name.setValue(result.data.name);
                $change_password.$email.setValue(result.data.email);
            }

            $loading.hide();
        });
    };

    this.savePassword = function () {
        $change_password.$button.$save.loading();

        var url = 'Account/updatePassword';
        var data = {
            old_password: $change_password.$old_password.getValue(),
            new_password: $change_password.$new_password.getValue(),
            retype_password: $change_password.$retype_password.getValue(),
        };

        api.post(url, data, function (result) {

            switch (result.status) {
                case 'success':
                    $splash.show(result.message);

                    $change_password.$old_password.clear();
                    $change_password.$new_password.clear();
                    $change_password.$retype_password.clear();
                    $change_password.$button.$save.release();
                    this.detailData.bind(this);

                    $tab.$preview.trigger('click');

                    break;
                case 'invalid':
                    $change_password.error(result);
                    $change_password.$old_password.focus();

                    $change_password.$button.$save.release();
            }

        }.bind(this));
    };

};

dashboard.application.controllers.MyAccount.prototype =
    Object.create(dashboard.application.core.Controller.prototype);

dashboard.application.controllers.JenisDiklat = function () {
    var $el = $page.getScope('JenisDiklat');
    dashboard.application.core.Controller.call(this, $el);

    var rs = [];
    var renderedRs = [];

    var rsRange = config.paging.range;

    var tableHeads = ['JENIS_DIKLAT', 'Nama Singkat'];
    var tableFields = ['jenisDiklat', 'namaSingkat'];

    var rsBerkasPendaftaran = [];
    var cartHeads = ['Berkas Pendaftaran'];
    var cartFields = ['berkasPendaftaran'];


    var saveMode;

    var $tab;
    var $list;
    var $preview;
    var $form;

    this.init = function () {

        // tab

        $tab = $el.find('#tab');
        $tab.$list = $tab.getRef('list');
        $tab.$preview = $tab.getRef('preview');
        $tab.$form = $tab.getRef('form');
        $tab.$new = $tab.getRef('new');

        // list

        $list = $el.find('#list');
        $list.$search = $list.getRef('search').uiTextBox({ icon: 'search', placeholder: 'Search...' });
        $list.$table = $list.getRef('table').uiTable({ headers: tableHeads });
        $list.$paging = $list.getRef('paging').uiPaging();

        $list.$button = $list.find('#button');
        $list.$button.$delete = $list.$button.getRef('delete').uiButton();
        $list.$button.$detail = $list.$button.getRef('detail').uiButton();
        $list.$button.$edit = $list.$button.getRef('edit').uiButton();

        $list.$button.$refresh = $list.$button.getRef('refresh').uiButton();


        // preview

        $preview = $el.find('#preview');

        $preview.$jenisDiklat = $preview.getRef('jenisDiklat').uiTextView();
        $preview.$namaSingkat = $preview.getRef('namaSingkat').uiTextView();

        $preview.$listBerkasPendaftaran = $preview.getRef('listBerkasPendaftaran').uiTableView(cartHeads);

        $preview.$button = $preview.find('#button');
        $preview.$button.$edit = $preview.$button.getRef('edit').uiButton();
        $preview.$button.$cancel = $preview.$button.getRef('cancel').uiButton();

        // form

        $form = $el.find('#form').uiForm();

        $form.$jenisDiklat = $form.getRef('jenisDiklat').uiTextBox();
        $form.$namaSingkat = $form.getRef('namaSingkat').uiTextBox();
        $form.$berkasPendaftaranId = $form.getRef('berkasPendaftaranId').uiComboBox();

        $form.$listBerkasPendaftaran = $form.getRef('listBerkasPendaftaran').uiTable(cartHeads);
        $form.$add = $form.getRef('add');
        $form.$delete = $form.getRef('delete');


        $form.$button = $form.find('#button');
        $form.$button.$save = $form.$button.getRef('save').uiButton();
        $form.$button.$cancel = $form.$button.getRef('cancel').uiButton();

    };

    this.driveEvents = function () {
        // tab

        $tab.$preview.on('click', this.detailData.bind(this));
        $tab.$form.on('click', this.editData.bind(this));
        $tab.$new.on('click', this.newData.bind(this));

        // list

        $list.$search.on('keyup', this.searchData.bind(this));

        $list.$table.on('dblclick', function () {
            if (!$list.$search.getValue()) {
                if (rs.length > 0)
                    $tab.$preview.trigger('click');
            } else {
                if (renderedRs.length > 0)
                    $tab.$preview.trigger('click');
            }
        });

        $list.$paging.on('click', this.moveData.bind(this));
        $list.$button.$delete.on('click', this.deleteData.bind(this));

        $list.$button.$detail.on('click', function () {
            $tab.$preview.trigger('click');
        });

        $list.$button.$edit.on('click', function () {
            $tab.$form.trigger('click');
        });

        $list.$button.$refresh.on('click', this.readData.bind(this));

        // preview

        $preview.$button.$edit.on('click', function () {
            $tab.$form.trigger('click');
        });

        $preview.$button.$cancel.on('click', function () {
            $tab.$list.trigger('click');
        });

        // form
        $form.$add.on('click', this.addToCart.bind(this));
        $form.$delete.on('click', this.deleteFromCart.bind(this));

        $form.$button.$save.on('click', this.saveData.bind(this));

        $form.$button.$cancel.on('click', function () {
            $tab.$list.trigger('click');
        });

    };

    this.driveFieldSequence = function () {


    };

    this.reload = function () {
        $tab.$list.trigger('click');



    };

    this.readData = function (callback) {

        if (!callback)
            $loading.show();
        $form.$berkasPendaftaranId.renderApi('BerkasPendaftaran/readData', { text: 'berkasPendaftaran' });
        var url = 'JenisDiklat/readData';
        var data = {
            orderBy: 'id',
        };

        api.post(url, data, function (result) {

            rs = result.data;
            var rsCount = rs.length;

            if (rsCount > 0) {
                this.dataShow();
            }
            else {
                this.dataHide();
            }

            renderedRs = this.renderTable($list.$table, rs, tableFields, rsRange);
            this.renderPaging($list.$paging, renderedRs, rsRange);

            if ($.isFunction(callback))
                callback();
            else
                $loading.hide();
        }.bind(this));
    };

    this.dataHide = function () {
        $list.$paging.hide();
        $tab.$form.hide();
        $tab.$preview.hide();
        $list.$button.$delete.hide();
        $list.$button.$detail.hide();
        $list.$button.$edit.hide();
    };

    this.dataShow = function () {
        $list.$paging.show();
        $tab.$form.show();
        $tab.$preview.show();
        $list.$button.$delete.show();
        $list.$button.$detail.show();
        $list.$button.$edit.show();
    };

    this.searchData = function () {

        var filteredRs = rs.filter(function (data) {
            var regex = new RegExp($list.$search.getValue(), 'gi');
            var found = false;

            tableFields.forEach(function (field) {
                if (data[field])
                    if (data[field].match(regex))
                        found = true;
            });

            return found;
        });

        renderedRs = this.renderTable($list.$table, filteredRs, tableFields, rsRange);
        if (renderedRs.length > 0) {
            this.renderPaging($list.$paging, renderedRs, rsRange);
            this.dataShow();
        } else {
            this.dataHide();
        }
    };

    this.moveData = function () {
        renderedRs = this.renderTable($list.$table, renderedRs, tableFields, rsRange, $list.$paging.getValue());
    };

    this.deleteData = function () {
        var checkedId = $list.$table.getCheckedValues();

        if (checkedId.length == 0) {
            $msgbox.alert('NO_DATA_CHECKED');
            return;
        };

        var url = 'JenisDiklat/multipleDeleteData';
        var data = {
            multipleId: checkedId,
        };

        $msgbox.confirm('DELETE_CHECKED_DATA', function () {
            api.post(url, data, function (result) {

                switch (result.status) {
                    case 'success':
                        $splash.show(result.message);
                        this.readData();

                        break;
                    case 'failed':
                        var htmlError = '';

                        $.each(result.data, function (key, value) {
                            htmlError += value + '<br>';
                        });
                        $msgbox.alert(htmlError);
                        this.readData();
                }

            }.bind(this));
        }.bind(this));

    };

    this.detailData = function () {
        $loading.show();

        var url = 'JenisDiklat/detailData';
        var data = {
            id: $list.$table.getValue()
        };

        api.post(url, data, function (result) {

            if (result.status == 'success') {
                $preview.$jenisDiklat.setValue(result.data.jenisDiklat);
                $preview.$namaSingkat.setValue(result.data.namaSingkat);
                rsBerkasPendaftaran = result.data.listBerkasPendaftaran;
                this.renderViewCart();
            }

            $loading.hide();
        }.bind(this));
    };

    this.serializeData = function () {
        var data = {
            jenisDiklat: $form.$jenisDiklat.getValue(),
            namaSingkat: $form.$namaSingkat.getValue(),
            listBerkasPendaftaran: rsBerkasPendaftaran,
        };

        if (saveMode == EDIT_MODE)
            data.id = $list.$table.getValue();

        return data;
    };

    this.clearData = function () {
        $form.clear();
        $form.$jenisDiklat.clear();
        $form.$namaSingkat.clear();
        $form.$berkasPendaftaranId.clear();

        rsBerkasPendaftaran = new Array();
        this.renderEditCart();
    };

    this.editData = function () {
        $loading.show();
        saveMode = EDIT_MODE;

        this.clearData();

        var url = 'JenisDiklat/detailData';
        var data = {
            id: $list.$table.getValue()
        };

        api.post(url, data, function (result) {

            if (result.status == 'success') {
                $form.$jenisDiklat.setValue(result.data.jenisDiklat);
                $form.$namaSingkat.setValue(result.data.namaSingkat);

                rsBerkasPendaftaran = result.data.listBerkasPendaftaran;
                this.renderEditCart();


                $form.$jenisDiklat.focus();
            }

            $loading.hide();
        }.bind(this));
    };

    this.newData = function () {
        saveMode = NEW_MODE;

        this.clearData();
        $form.$jenisDiklat.focus();
    };

    this.saveData = function () {
        $form.$button.$save.loading();

        switch (saveMode) {
            case EDIT_MODE:
                var url = 'JenisDiklat/updateData';
                break;
            case NEW_MODE:
                var url = 'JenisDiklat/createData';
        }

        var data = this.serializeData();

        api.post(url, data, function (result) {

            switch (result.status) {
                case 'success':
                    $splash.show(result.message);

                    this.readData(function () {
                        switch (saveMode) {
                            case EDIT_MODE:
                                $tab.$list.trigger('click');
                                break;
                            case NEW_MODE:
                                this.clearData();
                                $form.$jenisDiklat.focus();
                        }

                        $form.$button.$save.release();
                        window.scrollTo(0, 0);
                    }.bind(this));

                    break;
                case 'invalid':
                    $form.error(result);
                    $form.$button.$save.release();
            }

        }.bind(this));
    };

    this.addToCart = function () {

        if (!$form.$berkasPendaftaranId.getValue()) {
            $msgbox.alert('Berkas Pendaftaran diperlukan');
            return;
        }

        var found = false;
        rsBerkasPendaftaran.forEach(function (data) {
            if (data.berkasPendaftaranId == $form.$berkasPendaftaranId.getValue())
                found = true;
        });

        if (found) {
            $msgbox.alert('Berkas Pendaftaran sudah terdaftar');
            return;
        }

        rsBerkasPendaftaran.push({
            id: this.guid(),
            berkasPendaftaranId: $form.$berkasPendaftaranId.getValue(),
            berkasPendaftaran: $form.$berkasPendaftaranId.getText(),
        });

        $form.$berkasPendaftaranId.clear();
        $form.$berkasPendaftaranId.focus();
        this.renderEditCart();
    };

    this.deleteFromCart = function () {
        var checkedId = $form.$listBerkasPendaftaran.getCheckedValues();
        if (checkedId.length <= 0)
            return;

        $msgbox.confirm('Delete selected data', function () {

            checkedId.forEach(function (indexId) {
                var i = -1;

                rsBerkasPendaftaran.forEach(function (data) {
                    i++;

                    if (data.id == indexId)
                        rsBerkasPendaftaran.splice(i, 1);

                });
            });

            this.renderEditCart();
        }.bind(this));

    };

    this.renderViewCart = function () {
        this.renderTable($preview.$listBerkasPendaftaran, rsBerkasPendaftaran, cartFields, 100);
    };

    this.renderEditCart = function () {
        this.renderTable($form.$listBerkasPendaftaran, rsBerkasPendaftaran, cartFields, 100);
    };

};

dashboard.application.controllers.JenisDiklat.prototype =
    Object.create(dashboard.application.core.Controller.prototype);

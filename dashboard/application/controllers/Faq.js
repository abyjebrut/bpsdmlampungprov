dashboard.application.controllers.Faq = function () {
    var $el = $page.getScope('Faq');
    dashboard.application.core.Controller.call(this, $el);

    var rs = [];
    var renderedRs = [];

    var rsRange = config.paging.range;

    var tableHeads = ['PUBLISH_STATUS', 'PERTANYAAN', 'JAWABAN'];
    var tableFields = ['publishStatus', 'pertanyaan', 'jawaban'];

    var saveMode;

    var $tab;
    var $list;
    var $preview;
    var $form;

    this.init = function () {

        // tab

        $tab = $el.find('#tab');
        $tab.$list = $tab.getRef('list');
        $tab.$preview = $tab.getRef('preview');
        $tab.$form = $tab.getRef('form');
        $tab.$new = $tab.getRef('new');

        // list

        $list = $el.find('#list');
        $list.$search = $list.getRef('search').uiTextBox({ icon: 'search', placeholder: 'Search...' });
        $list.$publishStatusId = $list.getRef('publishStatusId').uiRadioBox();
        $list.$table = $list.getRef('table').uiTable({ headers: tableHeads });
        $list.$paging = $list.getRef('paging').uiPaging();

        $list.$button = $list.find('#button');
        $list.$button.$delete = $list.$button.getRef('delete').uiButton();
        $list.$button.$detail = $list.$button.getRef('detail').uiButton();
        $list.$button.$edit = $list.$button.getRef('edit').uiButton();

        $list.$button.$up = $list.$button.getRef('up').uiButton();
        $list.$button.$down = $list.$button.getRef('down').uiButton();
        $list.$button.$refresh = $list.$button.getRef('refresh').uiButton();


        // preview

        $preview = $el.find('#preview');

        $preview.$publishStatus = $preview.getRef('publishStatus').uiTextView();
        $preview.$pertanyaan = $preview.getRef('pertanyaan').uiTextAreaView();
        $preview.$jawaban = $preview.getRef('jawaban').uiTextAreaView();

        $preview.$button = $preview.find('#button');
        $preview.$button.$edit = $preview.$button.getRef('edit').uiButton();
        $preview.$button.$cancel = $preview.$button.getRef('cancel').uiButton();

        // form

        $form = $el.find('#form').uiForm();

        $form.$publishStatusId = $form.getRef('publishStatusId').uiRadioBox();
        $form.$pertanyaan = $form.getRef('pertanyaan').uiTextArea();
        $form.$jawaban = $form.getRef('jawaban').uiTextArea();

        $form.$button = $form.find('#button');
        $form.$button.$save = $form.$button.getRef('save').uiButton();
        $form.$button.$cancel = $form.$button.getRef('cancel').uiButton();

    };

    this.driveEvents = function () {
        // tab

        $tab.$preview.on('click', this.detailData.bind(this));
        $tab.$form.on('click', this.editData.bind(this));
        $tab.$new.on('click', this.newData.bind(this));

        // list

        $list.$search.on('keyup', this.searchData.bind(this));
        $list.$publishStatusId.on('click', this.readData.bind(this));

        $list.$table.on('dblclick', function () {
            $tab.$preview.trigger('click');
        });

        $list.$paging.on('click', this.moveData.bind(this));
        $list.$button.$delete.on('click', this.deleteData.bind(this));

        $list.$button.$detail.on('click', function () {
            $tab.$preview.trigger('click');
        });

        $list.$button.$edit.on('click', function () {
            $tab.$form.trigger('click');
        });

        $list.$button.$up.on('click', this.moveUp.bind(this));
        $list.$button.$down.on('click', this.moveDown.bind(this));
        $list.$button.$refresh.on('click', this.loadData.bind(this));

        // preview

        $preview.$button.$edit.on('click', function () {
            $tab.$form.trigger('click');
        });

        $preview.$button.$cancel.on('click', function () {
            $tab.$list.trigger('click');
        });

        // form

        $form.$button.$save.on('click', this.saveData.bind(this));

        $form.$button.$cancel.on('click', function () {
            $tab.$list.trigger('click');
        });

    };

    this.driveFieldSequence = function () {


    };

    this.reload = function () {
        $tab.$list.trigger('click');
    };

    this.loadData = function () {
        this.readData();
        $form.$publishStatusId.renderApi('PublishStatus/readData', { text: 'publishStatus' });
        $list.$publishStatusId.renderApi('PublishStatus/readData', { text: 'publishStatus' });
    };

    this.readData = function (callback) {

        if (!callback)
            $loading.show();

        var url = 'Faq/readData';
        var data = {
            orderBy: 'position',
            publishStatusId: $list.$publishStatusId.getValue(),
        };

        api.post(url, data, function (result) {

            rs = result.data;
            var rsCount = rs.length;

            if (rsCount > 0) {
                this.dataShow();
            }
            else {
                this.dataHide();
            }

            renderedRs = this.renderTable($list.$table, rs, tableFields, rsRange);
            this.renderPaging($list.$paging, renderedRs, rsRange);

            if ($.isFunction(callback))
                callback();
            else
                $loading.hide();
        }.bind(this));
    };

    this.dataHide = function () {
        $list.$paging.hide();
        $tab.$form.hide();
        $tab.$preview.hide();
        $list.$button.$delete.hide();
        $list.$button.$detail.hide();
        $list.$button.$edit.hide();
        $list.$button.$down.hide();
        $list.$button.$up.hide();
    };

    this.dataShow = function () {
        $list.$paging.show();
        $tab.$form.show();
        $tab.$preview.show();
        $list.$button.$delete.show();
        $list.$button.$detail.show();
        $list.$button.$edit.show();
        $list.$button.$down.show();
        $list.$button.$up.show();

    };

    this.searchData = function () {

        var filteredRs = rs.filter(function (data) {
            var regex = new RegExp($list.$search.getValue(), 'gi');
            var found = false;

            tableFields.forEach(function (field) {
                if (data[field])
                    if (data[field].match(regex))
                        found = true;
            });

            return found;
        });

        renderedRs = this.renderTable($list.$table, filteredRs, tableFields, rsRange);
        if (renderedRs.length > 0) {
            this.renderPaging($list.$paging, renderedRs, rsRange);
            this.dataShow();
        } else {
            this.dataHide();
        }
    };

    this.moveData = function () {
        renderedRs = this.renderTable($list.$table, renderedRs, tableFields, rsRange, $list.$paging.getValue());
    };

    this.moveUp = function () {

        var activeId = $list.$table.getValue();
        var prevId = $list.$table.find('tr.active').prev().find('input[type="checkbox"]').val()

        if (!prevId)
            return;

        $loading.show();

        var url = 'Faq/moveData';
        var data = {
            idFrom: activeId,
            idTo: prevId,
        };

        api.post(url, data, function (result) {

            if (result.status == 'success') {
                this.reload();
            }

            $loading.hide();
        }.bind(this));

    };

    this.moveDown = function () {

        var activeId = $list.$table.getValue();
        var prevId = $list.$table.find('tr.active').next().find('input[type="checkbox"]').val()

        if (!prevId)
            return;

        $loading.show();

        var url = 'Faq/moveData';
        var data = {
            idFrom: activeId,
            idTo: prevId,
        };

        api.post(url, data, function (result) {

            if (result.status == 'success') {
                this.reload();
            }

            $loading.hide();
        }.bind(this));
    };

    this.deleteData = function () {
        var checkedId = $list.$table.getCheckedValues();

        if (checkedId.length == 0) {
            $msgbox.alert('NO_DATA_CHECKED');
            return;
        };

        var url = 'Faq/multipleDeleteData';
        var data = {
            multipleId: checkedId,
        };

        $msgbox.confirm('DELETE_CHECKED_DATA', function () {
            api.post(url, data, function (result) {

                switch (result.status) {
                    case 'success':
                        $splash.show(result.message);
                        this.readData();

                        break;
                    case 'failed':
                        $msgbox.alert(result.message);
                }

            }.bind(this));
        }.bind(this));

    };

    this.detailData = function () {
        $loading.show();

        var url = 'Faq/detailData';
        var data = {
            id: $list.$table.getValue()
        };

        api.post(url, data, function (result) {

            if (result.status == 'success') {
                $preview.$publishStatus.setValue(result.data.publishStatus);
                $preview.$pertanyaan.setValue(result.data.pertanyaan);
                $preview.$jawaban.setValue(result.data.jawaban);
            }

            $loading.hide();
        });
    };

    this.serializeData = function () {
        var data = {
            publishStatusId: $form.$publishStatusId.getValue(),
            pertanyaan: $form.$pertanyaan.getValue(),
            jawaban: $form.$jawaban.getValue(),
        };

        if (saveMode == EDIT_MODE)
            data.id = $list.$table.getValue();

        return data;
    };

    this.clearData = function () {
        $form.clear();
        $form.$publishStatusId.clear();
        $form.$pertanyaan.clear();
        $form.$jawaban.clear();
    };

    this.editData = function () {
        $loading.show();
        saveMode = EDIT_MODE;

        this.clearData();

        var url = 'Faq/detailData';
        var data = {
            id: $list.$table.getValue()
        };

        api.post(url, data, function (result) {

            if (result.status == 'success') {
                $form.$publishStatusId.setValue(result.data.publishStatusId);
                $form.$pertanyaan.setValue(result.data.pertanyaan);
                $form.$jawaban.setValue(result.data.jawaban);

                $form.$publishStatusId.focus();
            }

            $loading.hide();
        });
    };

    this.newData = function () {
        saveMode = NEW_MODE;

        this.clearData();
        $form.$publishStatusId.focus();
    };

    this.saveData = function () {
        $form.$button.$save.loading();

        switch (saveMode) {
            case EDIT_MODE:
                var url = 'Faq/updateData';
                break;
            case NEW_MODE:
                var url = 'Faq/createData';
        }

        var data = this.serializeData();

        api.post(url, data, function (result) {

            switch (result.status) {
                case 'success':
                    $splash.show(result.message);

                    this.readData(function () {
                        switch (saveMode) {
                            case EDIT_MODE:
                                $tab.$list.trigger('click');
                                break;
                            case NEW_MODE:
                                this.clearData();
                                $form.$publishStatusId.focus();
                        }

                        $form.$button.$save.release();
                        window.scrollTo(0, 0);
                    }.bind(this));

                    break;
                case 'invalid':
                    $form.error(result);
                    $form.$button.$save.release();
            }

        }.bind(this));
    };

};

dashboard.application.controllers.Faq.prototype =
    Object.create(dashboard.application.core.Controller.prototype);

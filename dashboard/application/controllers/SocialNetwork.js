dashboard.application.controllers.SocialNetwork = function () {
    var $el = $page.getScope('SocialNetwork');
    dashboard.application.core.Controller.call(this, $el);

    var $tab;
    var $preview;
    var $form;

    this.init = function () {

        // tab

        $tab = $el.find('#tab');
        $tab.$preview = $tab.getRef('preview');
        $tab.$form = $tab.getRef('form');

        // preview

        $preview = $el.find('#preview');

        $preview.$facebookPage = $preview.getRef('facebookPage').uiTextView();
        $preview.$twitterPage = $preview.getRef('twitterPage').uiTextView();
        $preview.$googlePlusPage = $preview.getRef('googlePlusPage').uiTextView();
        $preview.$instagramPage = $preview.getRef('instagramPage').uiTextView();

        $preview.$button = $preview.find('#button');
        $preview.$button.$edit = $preview.$button.getRef('edit').uiButton();
        $preview.$button.$cancel = $preview.$button.getRef('cancel').uiButton();

        // form

        $form = $el.find('#form').uiForm();

        $form.$facebookPage = $form.getRef('facebookPage').uiTextBox();
        $form.$twitterPage = $form.getRef('twitterPage').uiTextBox();
        $form.$googlePlusPage = $form.getRef('googlePlusPage').uiTextBox();
        $form.$instagramPage = $form.getRef('instagramPage').uiTextBox();

        $form.$button = $form.find('#button');
        $form.$button.$save = $form.$button.getRef('save').uiButton();
        $form.$button.$cancel = $form.$button.getRef('cancel').uiButton();

    };

    this.driveEvents = function () {
        // tab

        $tab.$form.on('click', this.editData.bind(this));

        // preview

        $preview.$button.$edit.on('click', function () {
            $tab.$form.trigger('click');
        });

        // form

        $form.$button.$save.on('click', this.saveData.bind(this));

        $form.$button.$cancel.on('click', function () {
            $tab.$preview.trigger('click');
        });

    };

    this.driveFieldSequence = function () {

    };

    this.reload = function () {
        $tab.$preview.trigger('click');

    };

    this.detailData = function (callback) {
        $loading.show();

        var url = 'SocialNetwork/detailData';

        api.post(url, function (result) {

            if (result.status == 'success') {
                $preview.$facebookPage.setValue(result.data.facebookPage);
                $preview.$twitterPage.setValue(result.data.twitterPage);
                $preview.$googlePlusPage.setValue(result.data.googlePlusPage);
                $preview.$instagramPage.setValue(result.data.instagramPage);

                if ($.isFunction(callback))
                    callback();
            }

            $loading.hide();
        });
    };

    this.serializeData = function () {
        var data = {
            facebookPage: $form.$facebookPage.getValue(),
            twitterPage: $form.$twitterPage.getValue(),
            googlePlusPage: $form.$googlePlusPage.getValue(),
            instagramPage: $form.$instagramPage.getValue(),
        };

        return data;
    };

    this.clearData = function () {
        $form.clear();

        $form.$facebookPage.clear();
        $form.$twitterPage.clear();
        $form.$googlePlusPage.clear();
        $form.$instagramPage.clear();

    };

    this.editData = function () {
        $loading.show();

        this.clearData();

        var url = 'SocialNetwork/detailData';

        api.post(url, function (result) {

            if (result.status == 'success') {
                $form.$facebookPage.setValue(result.data.facebookPage);
                $form.$twitterPage.setValue(result.data.twitterPage);
                $form.$googlePlusPage.setValue(result.data.googlePlusPage);
                $form.$instagramPage.setValue(result.data.instagramPage);

                $form.$facebookPage.focus();
            }

            $loading.hide();
        });
    };

    this.saveData = function () {
        $form.$button.$save.loading();

        var url = 'SocialNetwork/saveData';
        var data = this.serializeData();

        api.post(url, data, function (result) {

            switch (result.status) {
                case 'success':
                    $splash.show(result.message);

                    this.detailData(function () {
                        $tab.$preview.trigger('click');

                        $form.$button.$save.release();
                        window.scrollTo(0, 0);
                    }.bind(this));

                    break;
                case 'invalid':
                    $form.error(result);
                    $form.$button.$save.release();
            }

        }.bind(this));
    };


};

dashboard.application.controllers.SocialNetwork.prototype =
    Object.create(dashboard.application.core.Controller.prototype);

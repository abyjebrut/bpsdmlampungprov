dashboard.application.controllers.LaporanRekapitulasi = function () {
    var $el = $page.getScope('LaporanRekapitulasi');
    dashboard.application.core.Controller.call(this, $el);

    var rs = [];
    var renderedRs = [];

    var rsRange = config.paging.range;

    var tableHeads = ['Jenis Diklat', 'Kode', 'Nama Diklat'];
    var tableFields = ['jenisDiklat', 'kode', 'namaDiklat'];

    var rsLaporan = [];
    var cartHeads = ['NAMA', 'OPD', 'JUDUL LAPORAN AKTUALISASI'];
    var cartFields = ['nama', 'unitKerja', 'judulLaporan'];

    var saveMode;

    var $tab;
    var $list;
    var $preview;

    this.init = function () {

        // tab

        $tab = $el.find('#tab');
        $tab.$list = $tab.getRef('list');
        $tab.$preview = $tab.getRef('preview');

        // list

        $list = $el.find('#list');
        $list.$jenisDiklatId = $list.getRef('jenisDiklatId').uiComboBox({ placeholder: 'Filter By Jenis Diklat' });
        $list.$publishStatusId = $list.getRef('publishStatusId').uiRadioBox();
        $list.$search = $list.getRef('search').uiTextBox({ icon: 'search', placeholder: 'Search...' });
        $list.$table = $list.getRef('table').uiTable({ headers: tableHeads });
        $list.$paging = $list.getRef('paging').uiPaging();

        $list.$button = $list.find('#button');
        $list.$button.$detail = $list.$button.getRef('detail').uiButton();

        $list.$button.$refresh = $list.$button.getRef('refresh').uiButton();

        // preview

        $preview = $el.find('#preview');

        $preview.$publishStatus = $preview.getRef('publishStatus').uiTextView();
        $preview.$jenisDiklat = $preview.getRef('jenisDiklat').uiTextView();
        $preview.$kode = $preview.getRef('kode').uiTextView();
        $preview.$namaDiklat = $preview.getRef('namaDiklat').uiTextAreaView();
        $preview.$tahunDiklat = $preview.getRef('tahunDiklat').uiTextView();
        $preview.$lokasi = $preview.getRef('lokasi').uiTextView();
        $preview.$angkatan = $preview.getRef('angkatan').uiTextView();
        $preview.$pelaksanaanMulai = $preview.getRef('pelaksanaanMulai').uiTextView();
        $preview.$pelaksanaanSelesai = $preview.getRef('pelaksanaanSelesai').uiTextView();
        $preview.$kuota = $preview.getRef('kuota').uiTextView();
        $preview.$statusDiklat = $preview.getRef('statusDiklat').uiTextView();
        $preview.$listLaporan = $preview.getRef('listLaporan').uiTableView(cartHeads);

        $preview.$button = $preview.find('#button');
        $preview.$button.$excel = $preview.$button.getRef('excel').uiButton();
        $preview.$button.$cancel = $preview.$button.getRef('cancel').uiButton();

    };

    this.driveEvents = function () {
        // tab

        $tab.$preview.on('click', this.detailData.bind(this));

        // list

        $list.$jenisDiklatId.on('change', this.readData.bind(this));
        $list.$search.on('keyup', this.searchData.bind(this));

        $list.$publishStatusId.on('click', this.readData.bind(this));
        $list.$table.on('dblclick', function () {
            if (!$list.$search.getValue()) {
                if (rs.length > 0)
                    $tab.$preview.trigger('click');
            } else {
                if (renderedRs.length > 0)
                    $tab.$preview.trigger('click');
            }
        });

        $list.$paging.on('click', this.moveData.bind(this));

        $list.$button.$detail.on('click', function () {
            $tab.$preview.trigger('click');
        });


        $list.$button.$refresh.on('click', this.loadData.bind(this));

        // preview

        $preview.$button.$excel.on('click', this.exportDetailToExcel.bind(this));
        $preview.$button.$cancel.on('click', function () {
            $tab.$list.trigger('click');
        });
    };

    this.driveFieldSequence = function () {

    };

    this.reload = function () {
        $tab.$list.trigger('click');
    };

    this.loadData = function () {
        $list.$jenisDiklatId.clear();
        $list.$publishStatusId.renderApi('PublishStatus/readData', {
            text: 'publishStatus',
            done: function () {
                $list.$publishStatusId.setValue(2);
                this.readData();
            }.bind(this),
        });

        $list.$jenisDiklatId.renderApi('JenisDiklat/readData', { orderBy: 'position' }, { text: 'jenisDiklat' });
    };

    this.readData = function (callback) {

        if (!callback)
            $loading.show();

        var url = 'LaporanRekapitulasi/readData';
        var data = {
            orderBy: 'id',
            publishStatusId: $list.$publishStatusId.getValue(),
            reverse: 1,
            jenisDiklatId: $list.$jenisDiklatId.getValue(),
        };

        api.post(url, data, function (result) {

            rs = result.data;
            var rsCount = rs.length;
            if (rsCount > 0) {
                this.dataShow();
            }
            else {
                this.dataHide();
            }


            renderedRs = this.renderTable($list.$table, rs, tableFields, rsRange);
            this.renderPaging($list.$paging, renderedRs, rsRange);

            if ($.isFunction(callback))
                callback();
            else
                $loading.hide();
        }.bind(this));
    };

    this.dataHide = function () {
        $list.$paging.hide();
        $tab.$preview.hide();
        $list.$button.$detail.hide();
    };

    this.dataShow = function () {
        $list.$paging.show();
        $list.$button.$detail.show();
        $tab.$preview.show();
    };

    this.searchData = function () {

        var filteredRs = rs.filter(function (data) {
            var regex = new RegExp($list.$search.getValue(), 'gi');
            var found = false;

            tableFields.forEach(function (field) {
                if (data[field])
                    if (data[field].match(regex))
                        found = true;
            });

            return found;
        });

        renderedRs = this.renderTable($list.$table, filteredRs, tableFields, rsRange);
        if (renderedRs.length > 0) {
            this.renderPaging($list.$paging, renderedRs, rsRange);
            this.dataShow();
        } else {
            this.dataHide();
        }
    };

    this.moveData = function () {
        renderedRs = this.renderTable($list.$table, renderedRs, tableFields, rsRange, $list.$paging.getValue());
    };

    this.detailData = function () {
        $loading.show();

        var url = 'LaporanRekapitulasi/detailData';
        var data = {
            id: $list.$table.getValue()
        };

        api.post(url, data, function (result) {

            if (result.status == 'success') {
                $preview.$publishStatus.setValue(result.data.publishStatus);
                $preview.$jenisDiklat.setValue(result.data.jenisDiklat);
                $preview.$kode.setValue(result.data.kode);
                $preview.$namaDiklat.setValue(result.data.namaDiklat);
                $preview.$tahunDiklat.setValue(result.data.tahunDiklat);
                $preview.$lokasi.setValue(result.data.lokasi);
                $preview.$angkatan.setValue(result.data.angkatan);
                $preview.$pelaksanaanMulai.setValue(result.data.pelaksanaanMulai);
                $preview.$pelaksanaanSelesai.setValue(result.data.pelaksanaanSelesai);
                $preview.$kuota.setValue(result.data.kuota);
                $preview.$statusDiklat.setValue(result.data.statusDiklat);
                rsLaporan = result.data.listLaporan;
                this.renderViewCart();
            }

            $loading.hide();
        }.bind(this));
    };

    this.renderViewCart = function () {
        this.renderTableNoPaging($preview.$listLaporan, rsLaporan, cartFields, 10000);
    };

    this.exportData = function (type, url, id) {

        $loading.show();

        var data = {};
        if (id)
            data.id = id;

        api.post(url, data, function (result) {

            $loading.hide();

            switch (result.status) {
                case 'success':

                    if (type == 'pdf')
                        router.open(result.data);
                    else
                        router.redirect(result.data);

                    break;
                case 'failed':
                    $msgbox.alert(result.message);
            }
        });
    };

    this.exportDetailToExcel = function () {
        var type = 'excel';
        var url = 'LaporanRekapitulasi/createExcelDetail';
        var id = $list.$table.getValue();

        this.exportData(type, url, id);
    };

};

dashboard.application.controllers.LaporanRekapitulasi.prototype =
    Object.create(dashboard.application.core.Controller.prototype);

dashboard.application.controllers.RejectPendaftaranDiklat = function () {
    var $el = $page.getScope('RejectPendaftaranDiklat');
    dashboard.application.core.Controller.call(this, $el);

    var rs = [];
    var renderedRs = [];

    var rsRange = config.paging.range;

    var tableHeads = ['NIP', 'NAMA', 'Instansi', 'NAMA_DIKLAT', 'Status Upload'];
    var tableFields = ['nip', 'nama', 'instansi', 'namaDiklat', 'statusUpload'];

    var saveMode;

    var $tab;
    var $list;
    var $preview;

    this.init = function () {

        // tab

        $tab = $el.find('#tab');
        $tab.$list = $tab.getRef('list');
        $tab.$preview = $tab.getRef('preview');

        // list

        $list = $el.find('#list');
        $list.$instansiId = $list.getRef('instansiId').uiComboBox({ placeholder: 'Filter By Instansi' });
        $list.$namaDiklat = $list.getRef('namaDiklat').uiComboBox({ placeholder: 'Filter By Nama Diklat' });
        $list.$search = $list.getRef('search').uiTextBox({ icon: 'search', placeholder: 'Search...' });
        $list.$table = $list.getRef('table').uiTable({ headers: tableHeads });
        $list.$paging = $list.getRef('paging').uiPaging();

        $list.$button = $list.find('#button');
        $list.$button.$delete = $list.$button.getRef('delete').uiButton();
        $list.$button.$detail = $list.$button.getRef('detail').uiButton();

        $list.$button.$refresh = $list.$button.getRef('refresh').uiButton();
        $list.$button.$excel = $list.$button.getRef('excel').uiButton();

        // preview

        $preview = $el.find('#preview');

        $preview.$jenisDiklat = $preview.getRef('jenisDiklat').uiTextView();
        $preview.$namaDiklat = $preview.getRef('namaDiklat').uiTextView();
        $preview.$nip = $preview.getRef('nip').uiTextView();
        $preview.$nama = $preview.getRef('nama').uiTextView();
        $preview.$foto = $preview.getRef('foto').uiImageView();
        $preview.$email = $preview.getRef('email').uiTextView();
        $preview.$pangkatGolongan = $preview.getRef('pangkatGolongan').uiTextView();
        $preview.$jabatanEselon = $preview.getRef('jabatanEselon').uiTextView();
        $preview.$unitKerja = $preview.getRef('unitKerja').uiTextView();
        $preview.$instansi = $preview.getRef('instansi').uiTextView();
        $preview.$instansiLainnya = $preview.getRef('instansiLainnya').uiTextView();
        $preview.$pendidikanTerakhir = $preview.getRef('pendidikanTerakhir').uiTextView();
        $preview.$gender = $preview.getRef('gender').uiTextView();
        $preview.$tempatLahir = $preview.getRef('tempatLahir').uiTextView();
        $preview.$tanggalLahir = $preview.getRef('tanggalLahir').uiTextView();
        $preview.$agama = $preview.getRef('agama').uiTextView();
        $preview.$alamat = $preview.getRef('alamat').uiTextAreaView();
        $preview.$telpHp = $preview.getRef('telpHp').uiTextView();

        $preview.$button = $preview.find('#button');
        $preview.$button.$edit = $preview.$button.getRef('edit').uiButton();
        $preview.$button.$cancel = $preview.$button.getRef('cancel').uiButton();
        $preview.$button.$pdf = $preview.$button.getRef('pdf').uiButton();

    };

    this.driveEvents = function () {
        // tab

        $tab.$preview.on('click', this.detailData.bind(this));

        // list

        $list.$instansiId.on('change', this.readData.bind(this));
        $list.$namaDiklat.on('change', this.readData.bind(this));
        $list.$search.on('keyup', this.searchData.bind(this));

        $list.$table.on('dblclick', function () {
            if (!$list.$search.getValue()) {
                if (rs.length > 0)
                    $tab.$preview.trigger('click');
            } else {
                if (renderedRs.length > 0)
                    $tab.$preview.trigger('click');
            }
        });

        $list.$paging.on('click', this.moveData.bind(this));

        $list.$button.$delete.on('click', this.deleteData.bind(this));

        $list.$button.$excel.on('click', this.exportListToExcel.bind(this));
        $list.$button.$detail.on('click', function () {
            $tab.$preview.trigger('click');
        });

        $list.$button.$refresh.on('click', this.loadData.bind(this));

        // preview

        $preview.$button.$edit.on('click', this.approveData.bind(this));

        $preview.$button.$cancel.on('click', function () {
            $tab.$list.trigger('click');
        });

        $preview.$button.$pdf.on('click', this.exportDetailToPdf.bind(this));

    };

    this.driveFieldSequence = function () {


    };

    this.reload = function () {
        $tab.$list.trigger('click');
    };

    this.loadData = function () {
        $list.$instansiId.clear();
        $list.$namaDiklat.clear();
        this.readData();
        $list.$instansiId.renderApi('Instansi/readData', { orderBy: 'instansi' }, { text: 'instansi' });
        //kosongin data
        $list.$namaDiklat.renderApi('Diklat/readData', { jenisDiklatId: 11, publishStatusId: 2 }, { text: 'namaDiklat' });
        // $list.$namaDiklat.renderApi('Diklat/readData', {text: 'namaDiklat'});
    };

    this.approveData = function () {

        $msgbox.confirm('Anda Mau Approve Data ' + $preview.$nama.getValue() + ' ??', function () {
            $loading.show();

            var url = 'PendaftaranDiklat/updateDataPending';
            var data = {
                id: $list.$table.getValue()
            };

            api.post(url, data, function (result) {

                if (result.status == 'success') {
                    $splash.show(result.message);
                    this.readData(function () {
                        $tab.$list.trigger('click');

                    }.bind(this));
                }

                $loading.hide();
            }.bind(this));
        }.bind(this));
    };


    this.readData = function (callback) {
        var jenisDiklatId = 11;

        if (!callback)
            $loading.show();

        var url = 'PendaftaranDiklat/readDataReject';
        var data = {
            orderBy: 'nip',
            reverse: 1,
            jenisDiklatId: jenisDiklatId,
            namaDiklat: $list.$namaDiklat.getText(),
            instansiId: $list.$instansiId.getValue(),
            diklatId: $list.$namaDiklat.getValue(),
        };

        api.post(url, data, function (result) {

            rs = result.data;
            var rsCount = rs.length;
            $list.$button.$excel.hide();
            if (rsCount > 0) {
                this.dataShow();
            }
            else {
                this.dataHide();
            }


            renderedRs = this.renderTable($list.$table, rs, tableFields, rsRange);
            this.renderPaging($list.$paging, renderedRs, rsRange);

            if ($.isFunction(callback))
                callback();
            else
                $loading.hide();
        }.bind(this));
    };

    this.dataHide = function () {
        $list.$paging.hide();
        $tab.$preview.hide();
        $list.$button.$detail.hide();
        $list.$button.$excel.hide();
        $list.$button.$delete.hide();
    };

    this.dataShow = function () {
        $list.$paging.show();
        $list.$button.$detail.show();
        $list.$button.$delete.show();
        $tab.$preview.show();
        if ($list.$namaDiklat.getValue())
            $list.$button.$excel.show();
    };

    this.searchData = function () {

        var filteredRs = rs.filter(function (data) {
            var regex = new RegExp($list.$search.getValue(), 'gi');
            var found = false;

            tableFields.forEach(function (field) {
                if (data[field])
                    if (data[field].match(regex))
                        found = true;
            });

            return found;
        });

        renderedRs = this.renderTable($list.$table, filteredRs, tableFields, rsRange);
        if (renderedRs.length > 0) {
            this.renderPaging($list.$paging, renderedRs, rsRange);
            this.dataShow();
        } else {
            this.dataHide();
        }
    };

    this.moveData = function () {
        renderedRs = this.renderTable($list.$table, renderedRs, tableFields, rsRange, $list.$paging.getValue());
    };

    this.detailData = function () {
        $loading.show();

        var url = 'PendaftaranDiklat/detailData';
        var data = {
            id: $list.$table.getValue()
        };

        api.post(url, data, function (result) {

            if (result.status == 'success') {
                $preview.$jenisDiklat.setValue(result.data.jenisDiklat);
                $preview.$namaDiklat.setValue(result.data.namaDiklat);
                $preview.$nip.setValue(result.data.nip);
                $preview.$nama.setValue(result.data.nama);
                $preview.$foto.setValue(result.data.foto);
                $preview.$email.setValue(result.data.email);
                $preview.$pangkatGolongan.setValue(result.data.pangkatGolongan);
                $preview.$jabatanEselon.setValue(result.data.jabatanEselon);
                $preview.$unitKerja.setValue(result.data.unitKerja);
                $preview.$instansi.setValue(result.data.instansi);
                $preview.$instansiLainnya.setValue(result.data.instansiLainnya);
                $preview.$pendidikanTerakhir.setValue(result.data.pendidikanTerakhir);
                $preview.$gender.setValue(result.data.jenisKelamin);
                $preview.$tanggalLahir.setValue(result.data.tanggalLahir);
                $preview.$tempatLahir.setValue(result.data.tempatLahir);
                $preview.$agama.setValue(result.data.agama);
                $preview.$alamat.setValue(result.data.alamat);
                $preview.$telpHp.setValue(result.data.telpHp);
            }

            $loading.hide();
        });
    };

    this.deleteData = function () {
        var checkedId = $list.$table.getCheckedValues();

        if (checkedId.length == 0) {
            $msgbox.alert('NO_DATA_CHECKED');
            return;
        };

        var url = 'PendaftaranDiklat/multipleDeleteData';
        var data = {
            multipleId: checkedId,
        };

        $msgbox.confirm('DELETE_CHECKED_DATA', function () {
            api.post(url, data, function (result) {

                switch (result.status) {
                    case 'success':
                        $splash.show(result.message);
                        this.readData();

                        break;
                    case 'failed':
                        $msgbox.alert(result.message);
                }

            }.bind(this));
        }.bind(this));

    };

    this.exportData = function (type, url, id) {

        $loading.show();

        var data = {};
        if (id)
            data.id = id;

        api.post(url, data, function (result) {

            $loading.hide();

            switch (result.status) {
                case 'success':

                    if (type == 'pdf')
                        router.open(result.data);
                    else
                        router.redirect(result.data);

                    break;
                case 'failed':
                    $msgbox.alert(result.message);
            }
        });
    };

    this.exportListToExcel = function () {
        var type = 'xls';
        var url = 'PendaftaranDiklat/createExcelList';
        var id = $list.$namaDiklat.getValue();

        this.exportData(type, url, id);
    };

    this.exportDetailToPdf = function () {
        var type = 'pdf';
        var url = 'PendaftaranDiklat/createPDFDetail';
        var id = $list.$table.getValue();

        this.exportData(type, url, id);
    };

};

dashboard.application.controllers.RejectPendaftaranDiklat.prototype =
    Object.create(dashboard.application.core.Controller.prototype);

dashboard.application.controllers.Database = function () {
    var $el = $page.getScope('Database');
    dashboard.application.core.Controller.call(this, $el);

    var $tab;
    var $preview;

    this.init = function () {

        // tab

        $tab = $el.find('#tab');
        $tab.$preview = $tab.getRef('preview');

        $preview = $el.find('#preview');

        $preview.$backup = $preview.getRef('backup').uiButton();

        $preview.$button = $preview.find('#button');



    };

    this.driveEvents = function () {
        // tab

        $tab.$preview.on('click', this.detailData.bind(this));


        // preview

        $preview.$backup.on('click', this.backupDb.bind(this));

    };

    this.driveFieldSequence = function () {


    };

    this.reload = function () {
        $tab.$preview.trigger('click');
        this.detailData();
    };

    this.backupDb = function () {
        $loading.show();
        var url = 'BackupDb/_backupDb';
        var data = {
            id: 1
        };

        api.post(url, data, function (result) {

            switch (result.status) {
                case 'success':
                    // var fileUrl = api.getTmp(result.data);
                    // window.location = result.data;
                    // console.log(result)
                    router.open(result.data);

                    break;
                case 'invalid':
            }
            $loading.hide();
        }.bind(this));

    };

    this.detailData = function (callback) {
        $loading.show();
        $loading.hide();

    };

};

dashboard.application.controllers.Database.prototype =
    Object.create(dashboard.application.core.Controller.prototype);

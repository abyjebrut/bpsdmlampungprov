dashboard.application.controllers.Kelulusan = function () {
    var $el = $page.getScope('Kelulusan');
    dashboard.application.core.Controller.call(this, $el);

    var rs = [];
    var renderedRs = [];

    var rsRange = config.paging.range;

    var tableHeads = ['NIP_NAMA', 'NO_REGISTRASI'];
    var tableFields = ['nipNama', 'noRegistrasi'];

    var saveMode;

    var $tab;
    var $list;
    var $preview;
    var $form;

    this.init = function () {

        // tab

        $tab = $el.find('#tab');
        $tab.$list = $tab.getRef('list');
        $tab.$preview = $tab.getRef('preview');
        $tab.$form = $tab.getRef('form');
        $tab.$new = $tab.getRef('new');

        // list

        $list = $el.find('#list');
        $list.$jenisDiklatId = $list.getRef('jenisDiklatId').uiComboBox({ placeholder: 'Filter By Jenis Diklat' });
        $list.$tahun = $list.getRef('tahun').uiComboBox({ placeholder: 'Filter By Tahun' });
        $list.$namaDiklat = $list.getRef('namaDiklat').uiComboBox({ placeholder: 'Filter By Nama Diklat' });
        $list.$search = $list.getRef('search').uiTextBox({ icon: 'search', placeholder: 'Search...' });
        $list.$table = $list.getRef('table').uiTable({ headers: tableHeads });
        $list.$paging = $list.getRef('paging').uiPaging();

        $list.$button = $list.find('#button');
        $list.$button.$delete = $list.$button.getRef('delete').uiButton();
        $list.$button.$detail = $list.$button.getRef('detail').uiButton();
        $list.$button.$edit = $list.$button.getRef('edit').uiButton();

        $list.$button.$pdf = $list.$button.getRef('pdf').uiButton();
        $list.$button.$refresh = $list.$button.getRef('refresh').uiButton();


        // preview

        $preview = $el.find('#preview');

        $preview.$jenisDiklat = $preview.getRef('jenisDiklat').uiTextView();
        $preview.$namaDiklat = $preview.getRef('namaDiklat').uiTextAreaView();
        $preview.$nipNama = $preview.getRef('nipNama').uiTextView();
        $preview.$noRegistrasi = $preview.getRef('noRegistrasi').uiTextView();

        $preview.$button = $preview.find('#button');
        $preview.$button.$edit = $preview.$button.getRef('edit').uiButton();
        $preview.$button.$cancel = $preview.$button.getRef('cancel').uiButton();

        // form

        $form = $el.find('#form').uiForm();

        $form.$jenisDiklatId = $form.getRef('jenisDiklatId').uiComboBox();
        $form.$tahun = $form.getRef('tahun').uiComboBox();
        //        $form.$tahun = $form.getRef('tahun').uiDatePicker({month: false, date: false});
        $form.$namaDiklat = $form.getRef('namaDiklat').uiComboBox();
        $form.$nipNama = $form.getRef('nipNama').uiComboBox();
        $form.$noRegistrasi = $form.getRef('noRegistrasi').uiTextBox();

        $form.$button = $form.find('#button');
        $form.$button.$save = $form.$button.getRef('save').uiButton();
        $form.$button.$cancel = $form.$button.getRef('cancel').uiButton();

    };

    this.driveEvents = function () {
        // tab

        $tab.$preview.on('click', this.detailData.bind(this));
        $tab.$form.on('click', this.editData.bind(this));
        $tab.$new.on('click', this.newData.bind(this));

        // list

        $list.$namaDiklat.on('change', this.readData.bind(this));
        //        $list.$jenisDiklatId.on('change', this.readData.bind(this));
        //        $list.$tahun.on('change', this.readData.bind(this));

        $list.$jenisDiklatId.on('change', this.listNamaDiklat.bind(this));
        $list.$tahun.on('change', this.listNamaDiklat.bind(this));

        $list.$search.on('keyup', this.searchData.bind(this));

        $list.$table.on('dblclick', function () {
            $tab.$preview.trigger('click');
        });

        $list.$paging.on('click', this.moveData.bind(this));
        $list.$button.$delete.on('click', this.deleteData.bind(this));

        $list.$button.$detail.on('click', function () {
            $tab.$preview.trigger('click');
        });

        $list.$button.$edit.on('click', function () {
            $tab.$form.trigger('click');
        });

        $list.$button.$pdf.on('click', this.exportListToPdf.bind(this));
        $list.$button.$refresh.on('click', this.loadData.bind(this));

        // preview

        $preview.$button.$edit.on('click', function () {
            $tab.$form.trigger('click');
        });

        $preview.$button.$cancel.on('click', function () {
            $tab.$list.trigger('click');
        });

        // form

        $form.$jenisDiklatId.on('change', this.filterNamaDiklat.bind(this));
        $form.$tahun.on('change', this.filterNamaDiklat.bind(this));
        $form.$namaDiklat.on('change', this.filterNipNama.bind(this));

        $form.$button.$save.on('click', this.saveData.bind(this));

        $form.$button.$cancel.on('click', function () {
            $tab.$list.trigger('click');
        });

    };

    this.driveFieldSequence = function () {

    };

    this.reload = function () {
        $tab.$list.trigger('click');

    };

    this.loadData = function () {
        this.readData();

        $list.$tahun.renderApi('Diklat/readDataTahun', { text: 'tahun' });
        $list.$jenisDiklatId.renderApi('JenisDiklat/readData', { orderBy: 'position' }, { text: 'jenisDiklat' });
        $list.$namaDiklat.renderApi('Diklat/readData', { text: 'namaDiklatKode' });
        $form.$tahun.renderApi('Diklat/readDataTahun', { value: 'tahun', text: 'tahun' });
        $form.$jenisDiklatId.renderApi('JenisDiklat/readData', { orderBy: 'position' }, { text: 'jenisDiklat' });
        //   $form.$nipNama.renderApi('Anggota/readData', {text: 'nipNama'});
    };

    this.filterNamaDiklat = function () {
        var jenisDiklatId = $form.$jenisDiklatId.getValue();
        var tahun = $form.$tahun.getValue();
        $form.$namaDiklat.renderApi('Diklat/readDataKelulusan', { jenisDiklatId: jenisDiklatId, tahun: tahun }, { text: 'namaDiklatKode' });
    };

    this.listNamaDiklat = function () {
        var jenisDiklatId = $list.$jenisDiklatId.getValue();
        var tahun = $list.$tahun.getText();
        $list.$namaDiklat.renderApi('Diklat/readDataKelulusan', { jenisDiklatId: jenisDiklatId, tahun: tahun }, { text: 'namaDiklatKode' });
    };

    //    this.listBulanTahunNamaDiklat = function () {
    //    	var jenisDiklatId = $list.$jenisDiklatId.getValue();
    //        var tahun = $list.$tahun.getValue();
    //        $list.$namaDiklat.renderApi('Diklat/readDataKelulusan', {jenisDiklatId: jenisDiklatId, tahun: tahun}, {text: 'namaDiklatKode'});
    //    };
    //    
    this.filterNipNama = function () {
        var diklatId = $form.$namaDiklat.getValue();
        $form.$nipNama.renderApi('PendaftaranDiklat/readData', { diklatId: diklatId }, { value: 'akunId', text: 'nipNama' });
    };

    this.readData = function (callback) {

        if (!callback)
            $loading.show();

        var url = 'Kelulusan/readData';
        var data = {
            orderBy: 'nip',
            reverse: 1,
            //                jenisDiklatId: $list.$jenisDiklatId.getValue(),
            namaDiklat: $list.$namaDiklat.getValue(),
            //                bulan: $list.$bulan.getValue(),
            //                tahun: $list.$tahun.getValue(),
        };

        api.post(url, data, function (result) {

            rs = result.data;
            var rsCount = rs.length;

            if (rsCount > 0) {
                var showButtons = true;
                $tab.$preview.show();
                $tab.$form.show();
            }
            else {
                var showButtons = false;
                $tab.$preview.hide();
                $tab.$form.hide();
            }

            $list.$button.$delete.setEnabled(showButtons);
            $list.$button.$detail.setEnabled(showButtons);
            $list.$button.$edit.setEnabled(showButtons);

            renderedRs = this.renderTable($list.$table, rs, tableFields, rsRange);
            this.renderPaging($list.$paging, renderedRs, rsRange);

            if ($.isFunction(callback))
                callback();
            else
                $loading.hide();
        }.bind(this));
    };

    this.searchData = function () {

        var filteredRs = rs.filter(function (data) {
            var regex = new RegExp($list.$search.getValue(), 'gi');
            var found = false;

            tableFields.forEach(function (field) {
                if (data[field])
                    if (data[field].match(regex))
                        found = true;
            });

            return found;
        });

        renderedRs = this.renderTable($list.$table, filteredRs, tableFields, rsRange);
        this.renderPaging($list.$paging, renderedRs, rsRange);
    };

    this.moveData = function () {
        renderedRs = this.renderTable($list.$table, renderedRs, tableFields, rsRange, $list.$paging.getValue());
    };


    this.deleteData = function () {
        var checkedId = $list.$table.getCheckedValues();

        if (checkedId.length == 0) {
            $msgbox.alert('NO_DATA_CHECKED');
            return;
        };

        var url = 'Kelulusan/multipleDeleteData';
        var data = {
            multipleId: checkedId,
        };

        $msgbox.confirm('DELETE_CHECKED_DATA', function () {
            api.post(url, data, function (result) {

                switch (result.status) {
                    case 'success':
                        $splash.show(result.message);
                        this.readData();

                        break;
                    case 'failed':
                        $msgbox.alert(result.message);
                }

            }.bind(this));
        }.bind(this));

    };

    this.detailData = function () {
        $loading.show();

        var url = 'Kelulusan/detailData';
        var data = {
            id: $list.$table.getValue()
        };

        api.post(url, data, function (result) {

            if (result.status == 'success') {
                $preview.$jenisDiklat.setValue(result.data.jenisDiklat);
                $preview.$namaDiklat.setValue(result.data.namaDiklat);
                $preview.$nipNama.setValue(result.data.nipNama);
                $preview.$noRegistrasi.setValue(result.data.noRegistrasi);
            }

            $loading.hide();
        });
    };

    this.serializeData = function () {
        var data = {
            diklatId: $form.$namaDiklat.getValue(),
            akunId: $form.$nipNama.getValue(),
            noRegistrasi: $form.$noRegistrasi.getValue(),
        };

        if (saveMode == EDIT_MODE)
            data.id = $list.$table.getValue();

        return data;
    };

    this.clearData = function () {
        $form.clear();
        $form.$jenisDiklatId.clear();
        $form.$namaDiklat.clear();
        $form.$tahun.clear();
        $form.$nipNama.clear();
        $form.$noRegistrasi.clear();
    };

    this.editData = function () {
        $loading.show();
        saveMode = EDIT_MODE;

        this.clearData();

        //        $form.$namaDiklat.renderApi('Diklat/readData', {text: 'namaDiklatKode'});

        var url = 'Kelulusan/detailData';
        var data = {
            id: $list.$table.getValue()
        };

        api.post(url, data, function (result) {

            if (result.status == 'success') {
                $form.$jenisDiklatId.setValue(result.data.jenisDiklatId);
                $form.$tahun.setValue(result.data.tahun);

                var jenisDiklatId = result.data.jenisDiklatId;
                var tahun = result.data.tahun;

                $form.$namaDiklat.renderApi('Diklat/readDataKelulusan',
                    { jenisDiklatId: jenisDiklatId, tahun: tahun },
                    {
                        text: 'namaDiklatKode',
                        done: function () {
                            //                		alert('id: ' + result.data.diklatId);
                            $form.$namaDiklat.setValue(result.data.diklatId);
                        },
                    });

                var diklatId = result.data.diklatId;
                $form.$nipNama.renderApi('PendaftaranDiklat/readData', { diklatId: diklatId },
                    {
                        value: 'akunId',
                        text: 'nipNama',
                        done: function () {
                            $form.$nipNama.setValue(result.data.akunId);
                        },
                    });

                $form.$noRegistrasi.setValue(result.data.noRegistrasi);

                $form.$jenisDiklatId.focus();
            }

            $loading.hide();
        });
    };

    this.newData = function () {
        saveMode = NEW_MODE;

        this.clearData();
        $form.$jenisDiklatId.focus();
    };

    this.saveData = function () {
        $form.$button.$save.loading();

        switch (saveMode) {
            case EDIT_MODE:
                var url = 'Kelulusan/updateData';
                break;
            case NEW_MODE:
                var url = 'Kelulusan/createData';
        }

        var data = this.serializeData();

        api.post(url, data, function (result) {

            switch (result.status) {
                case 'success':
                    $splash.show(result.message);

                    this.readData(function () {
                        switch (saveMode) {
                            case EDIT_MODE:
                                $tab.$list.trigger('click');
                                break;
                            case NEW_MODE:
                                this.clearData();
                                $form.$jenisDiklatId.focus();
                        }

                        $form.$button.$save.release();
                        window.scrollTo(0, 0);
                    }.bind(this));

                    break;
                case 'invalid':
                    $form.error(result);
                    $form.$button.$save.release();
            }

        }.bind(this));
    };

    this.exportData = function (type, url, id, jenisDiklatId, tahun, namaDiklatId) {

        $loading.show();

        var data = {};
        if (id)
            data.id = id;
        data.jenisDiklatId = jenisDiklatId;
        data.tahun = tahun;
        data.namaDiklatId = namaDiklatId;

        api.post(url, data, function (result) {

            $loading.hide();

            switch (result.status) {
                case 'success':

                    if (type == 'pdf')
                        router.open(result.data);
                    else
                        router.redirect(result.data);

                    break;
                case 'failed':
                    $msgbox.alert(result.message);
            }
        });
    };

    this.exportListToPdf = function () {

        var type = 'pdf';
        var url = 'Kelulusan/createPDFList';
        var id = $list.$search.getValue();
        var jenisDiklatId = $list.$jenisDiklatId.getValue();
        var tahun = $list.$tahun.getValue();
        var namaDiklatId = $list.$namaDiklat.getValue();

        this.exportData(type, url, id, jenisDiklatId, tahun, namaDiklatId);
    };

};

dashboard.application.controllers.Kelulusan.prototype =
    Object.create(dashboard.application.core.Controller.prototype);

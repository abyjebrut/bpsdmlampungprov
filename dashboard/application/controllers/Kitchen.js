dashboard.application.controllers.Kitchen = function () {
    var $el = $page.getScope('Kitchen');
    dashboard.application.core.Controller.call(this, $el);

    var $tab;
    var $preview;
    var $form;

    this.init = function () {

        // tab

        $tab = $el.find('#tab');
        $tab.$preview = $tab.getRef('preview');
        $tab.$form = $tab.getRef('form');

        // preview

        $preview = $el.find('#preview');

        $preview.$langBox = $preview.getRef('langBox').uiLangBox();
        $preview.$langBox.render([{
          text: 'Indonesia',
          value: 'id',
          flag: 'indonesia',
        }, {
          text: 'English',
          value: 'en',
          flag: 'english',
        }, {
          text: 'United Kingdom',
          value: 'uk',
          flag: 'united kingdom',
        }]);

        $preview.$button1 = $preview.getRef('button1').uiButton();
        $preview.$button2 = $preview.getRef('button2').uiButton();

        $preview.$checkBox = $preview.getRef('checkBox').uiCheckBox();

        $preview.$checkTree = $preview.getRef('checkTree').uiCheckTree();
        $preview.$checkTree.render(config.menu);

        $preview.$treeView = $preview.getRef('treeView').uiTreeView();
        $preview.$treeView.render(config.menu);

        $preview.$comboBox = $preview.getRef('comboBox').uiComboBox();

        $preview.$datePicker = $preview.getRef('datePicker').uiDatePicker();
        $preview.$datePicker1 = $preview.getRef('datePicker1').uiDatePicker({date: false, picker: false});

        $preview.$htmlView = $preview.getRef('htmlView').uiHTMLView();
        $preview.$imageView = $preview.getRef('imageView').uiImageView();
        $preview.$mapPicker = $preview.getRef('mapPicker').uiMapPicker();
        $preview.$moneyBox = $preview.getRef('moneyBox').uiMoneyBox();
        $preview.$multipleImageView = $preview.getRef('multipleImageView').uiMultipleImageView();
        $preview.$multipleUploadImage = $preview.getRef('multipleUploadImage').uiMultipleUploadImage();
        $preview.$numericBox = $preview.getRef('numericBox').uiNumericBox({maxlength: 3, text: '(Pcs)'});
        $preview.$paging = $preview.getRef('paging').uiPaging({pageCount: 8});
        $preview.$radioBox = $preview.getRef('radioBox').uiRadioBox();
        $preview.$richText = $preview.getRef('richText').uiRichText();
        $preview.$table = $preview.getRef('table').uiTable({headers: ['Name', 'Email', 'Address']});
        $preview.$tableView = $preview.getRef('tableView').uiTableView({headers: ['Name', 'Email', 'Address']});
        $preview.$textArea = $preview.getRef('textArea').uiTextArea();
        $preview.$textAreaView = $preview.getRef('textAreaView').uiTextAreaView();
        $preview.$textBox = $preview.getRef('textBox').uiTextBox();
        $preview.$textView = $preview.getRef('textView').uiTextView();
        $preview.$uploadFile = $preview.getRef('uploadFile').uiUploadFile();
        $preview.$uploadImage = $preview.getRef('uploadImage').uiUploadImage({resolutionY: 400});

        $preview.$button = $preview.find('#button');
        $preview.$button.$test = $preview.$button.getRef('test').uiButton();

        var $testModal = $el.getRef('testModal');
        var $name = $testModal.getRef('name').uiTextBox();

        // $preview.$button.$test.on('click', function () {
        //     $testModal.modal('show');
        // });
    };

    this.driveEvents = function () {

        $preview.$datePicker.on('change', function (event) {
            alert('masuk ' + $(event.target).val());
        });

        $preview.$button1.on('click', function () {

            if ($preview.$button1.getText() == 'Enabled')
                $preview.$button1.setText('Disabled');
            else
                $preview.$button1.setText('Enabled');

            var button2Enabled = $preview.$button2.getEnabled();
            $preview.$button2.setEnabled(!button2Enabled);
        });

        $preview.$button2.on('click', function () {
            alert('klik button 1 utk disabled');
        });

        $tab.$preview.on('click', this.detailData.bind(this));

        $preview.$button.$test.on('click', this.testUi.bind(this));

        $preview.$checkBox.on('change', function () {
//            alert('hi ' + $preview.$checkBox.isChecked());
        });

        $preview.$comboBox.on('change', function () {
            alert('text: ' + $preview.$comboBox.getText());
        });

        $preview.$radioBox.on('change', function () {
            alert('value: ' + $preview.$radioBox.getValue());
        });

        $preview.$textBox.on('keyup', function () {
            if (!$preview.$textBox.getValue()) {
                $preview.$textBox.showError('Text required');
            }
            else if ($preview.$textBox.getValue().length <= 8) {
                $preview.$textBox.showError('Text must more than 8 character');

                $preview.$numericBox.showError('Text must more than 8 character');
                $preview.$moneyBox.showError('Text must more than 8 character');

            }
            else {
                $preview.$textBox.hideError();
                $preview.$numericBox.hideError();
                $preview.$moneyBox.hideError();
            }
        });

    };

    this.reload = function () {

        $tab.$preview.trigger('click');

        $preview.$checkBox.render([
            {value: 1, text: 'Check 1'},
            {value: 2, text: 'Check 2'},
            {value: 3, text: 'Check 3'},
            {value: 4, text: 'Check 4'},
        ]);

        $preview.$comboBox.render([
            {value: 1, text: 'Data 1'},
            {value: 2, text: 'Data 2'},
            {value: 3, text: 'Data 3'},
            {value: 4, text: 'Data 4'},
        ]);

        $preview.$radioBox.render([
            {value: 1, text: 'Data 1'},
            {value: 2, text: 'Data 2'},
            {value: 3, text: 'Data 3'},
            {value: 4, text: 'Data 4'},
        ]);

        $preview.$table.render({
            rows: [{
                value: 1,
                text: ['Fadlun Anaturdasa', 'f.anaturdasa@gmail.com', 'Bandung'],
            }, {
                value: 2,
                text: ['Fadlun Anaturdasa', 'f.anaturdasa@glovory.com', 'Bandar Lampung'],
            }, {
                value: 3,
                text: ['Robocop', 'f.anaturdasa@glovory.com', 'Medan'],
            },]
        });

        $preview.$tableView.render({
            rows: [{
                value: 1,
                text: ['Fadlun Anaturdasa', 'f.anaturdasa@gmail.com', 'Bandung'],
            }, {
                value: 2,
                text: ['Fadlun Anaturdasa', 'f.anaturdasa@glovory.com', 'Bandar Lampung'],
            }, {
                value: 3,
                text: ['Robocop', 'f.anaturdasa@glovory.com', 'Medan'],
            },]
        });

        $preview.$htmlView.setValue('<h4>HTML View <i>test markup</i></h4>')
        $preview.$imageView.setValue('9E55105C810F229DD7A8438ECBD5B7763A2FBA00.jpeg');
        $preview.$multipleImageView.setValue(['9E55105C810F229DD7A8438ECBD5B7763A2FBA00.jpeg','58944B980A94CFCA6CC16C5EC2859F55F4D36072.jpeg']);

//        $preview.$moneyBox.setValue(12345678.12);
//        $preview.$moneyBox.setEnable(false);

//        $preview.$numericBox.setText('%');
        $preview.$paging.setValue(6);
        $preview.$textArea.setValue('Hello world');
        $preview.$textAreaView.setValue('Hello world');
        $preview.$textBox.setValue('Hello world');
        $preview.$textView.setValue('Hello world');

    };

    this.detailData = function (callback) {

    };

    this.testUi = function () {
      $preview.$langBox.setValue('en');
      // $preview.$langBox.clear();
      // alert($preview.$langBox.getValue());
//        console.log($preview.$checkBox.getText());
//        console.log($preview.$datePicker.getText());
//        console.log($preview.$moneyBox.getValue());
//        console.log($preview.$paging.getValue());
//        $preview.$richText.setValue('<b>test</b><i>html</i>');
//        console.log($preview.$richText.getValue());

//        $preview.$datePicker.clear();
//        $preview.$datePicker.setValue('2012-4-10');

        // console.log($preview.$datePicker.getMonth());
        // console.log($preview.$datePicker.getYear());
        //
        // $preview.$datePicker1.setMonth(12);
        // $preview.$datePicker1.setYear(2012);
    };
};

dashboard.application.controllers.Kitchen.prototype =
Object.create(dashboard.application.core.Controller.prototype);

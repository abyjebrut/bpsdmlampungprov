dashboard.application.controllers.FooterExcel = function () {
    var $el = $page.getScope('FooterExcel');
    dashboard.application.core.Controller.call(this, $el);

    var rs = [];
    var renderedRs = [];

    var rsRange = config.paging.range;

    var tableHeads = ['Nama', 'Nip'];
    var tableFields = ['name', 'nip'];

    var saveMode;

    var $tab;
    var $list;
    var $preview;
    var $form;

    this.init = function () {

        // tab

        $tab = $el.find('#tab');
        $tab.$list = $tab.getRef('list');
        $tab.$preview = $tab.getRef('preview');
        $tab.$form = $tab.getRef('form');

        // list

        $list = $el.find('#list');
        $list.$search = $list.getRef('search').uiTextBox({ icon: 'search', placeholder: 'Search...' });
        $list.$table = $list.getRef('table').uiTable({ headers: tableHeads });
        $list.$paging = $list.getRef('paging').uiPaging();

        $list.$button = $list.find('#button');
        $list.$button.$detail = $list.$button.getRef('detail').uiButton();
        $list.$button.$edit = $list.$button.getRef('edit').uiButton();

        $list.$button.$refresh = $list.$button.getRef('refresh').uiButton();


        // preview

        $preview = $el.find('#preview');

        $preview.$nama = $preview.getRef('nama').uiTextView();
        $preview.$nip = $preview.getRef('nip').uiTextView();

        $preview.$button = $preview.find('#button');
        $preview.$button.$edit = $preview.$button.getRef('edit').uiButton();
        $preview.$button.$cancel = $preview.$button.getRef('cancel').uiButton();

        // form

        $form = $el.find('#form').uiForm();

        $form.$pengajarDiklatId = $form.getRef('pengajarDiklatId').uiComboBox();

        $form.$button = $form.find('#button');
        $form.$button.$save = $form.$button.getRef('save').uiButton();
        $form.$button.$cancel = $form.$button.getRef('cancel').uiButton();

    };

    this.driveEvents = function () {
        // tab

        $tab.$preview.on('click', this.detailData.bind(this));
        $tab.$form.on('click', this.editData.bind(this));

        // list

        $list.$search.on('keyup', this.searchData.bind(this));

        $list.$table.on('dblclick', function () {
            if (!$list.$search.getValue()) {
                if (rs.length > 0)
                    $tab.$preview.trigger('click');
            } else {
                if (renderedRs.length > 0)
                    $tab.$preview.trigger('click');
            }
        });

        $list.$paging.on('click', this.moveData.bind(this));

        $list.$button.$detail.on('click', function () {
            $tab.$preview.trigger('click');
        });

        $list.$button.$edit.on('click', function () {
            $tab.$form.trigger('click');
        });

        $list.$button.$refresh.on('click', this.readData.bind(this));

        // preview

        $preview.$button.$edit.on('click', function () {
            $tab.$form.trigger('click');
        });

        $preview.$button.$cancel.on('click', function () {
            $tab.$list.trigger('click');
        });

        // form

        $form.$button.$save.on('click', this.saveData.bind(this));

        $form.$button.$cancel.on('click', function () {
            $tab.$list.trigger('click');
        });

    };

    this.driveFieldSequence = function () {


    };

    this.reload = function () {
        $tab.$list.trigger('click');

    };

    this.readData = function (callback) {

        if (!callback)
            $loading.show();

        $form.$pengajarDiklatId.renderApi('User/readData', { instansiId: localStorage.instansiId, tipeUserId: 2, orderBy: 'name' }, { text: 'name' });
        var url = 'FooterExcel/readData';
        var data = {
            orderBy: 'id',
        };

        api.post(url, data, function (result) {

            rs = result.data;
            var rsCount = rs.length;

            if (rsCount > 0) {
                this.dataShow();
            }
            else {
                this.dataHide();
            }

            renderedRs = this.renderTable($list.$table, rs, tableFields, rsRange);
            this.renderPaging($list.$paging, renderedRs, rsRange);

            if ($.isFunction(callback))
                callback();
            else
                $loading.hide();
        }.bind(this));
    };

    this.dataHide = function () {
        $list.$paging.hide();
        $tab.$form.hide();
        $tab.$preview.hide();
        $list.$button.$detail.hide();
        $list.$button.$edit.hide();
    };

    this.dataShow = function () {
        $list.$paging.show();
        $tab.$form.show();
        $tab.$preview.show();
        $list.$button.$detail.show();
        $list.$button.$edit.show();
    };

    this.searchData = function () {

        var filteredRs = rs.filter(function (data) {
            var regex = new RegExp($list.$search.getValue(), 'gi');
            var found = false;

            tableFields.forEach(function (field) {
                if (data[field])
                    if (data[field].match(regex))
                        found = true;
            });

            return found;
        });

        renderedRs = this.renderTable($list.$table, filteredRs, tableFields, rsRange);
        if (renderedRs.length > 0) {
            this.renderPaging($list.$paging, renderedRs, rsRange);
            this.dataShow();
        } else {
            this.dataHide();
        }
    };

    this.moveData = function () {
        renderedRs = this.renderTable($list.$table, renderedRs, tableFields, rsRange, $list.$paging.getValue());
    };


    this.detailData = function () {
        $loading.show();

        var url = 'FooterExcel/detailData';
        var data = {
            id: $list.$table.getValue()
        };

        api.post(url, data, function (result) {

            if (result.status == 'success') {
                $preview.$nama.setValue(result.data.name);
                $preview.$nip.setValue(result.data.nip);

            }

            $loading.hide();
        });
    };

    this.serializeData = function () {
        var data = {
            pengajarDiklatId: $form.$pengajarDiklatId.getValue(),
        };

        if (saveMode == EDIT_MODE)
            data.id = $list.$table.getValue();

        return data;
    };

    this.clearData = function () {
        $form.clear();
        $form.$pengajarDiklatId.clear();
    };

    this.editData = function () {
        $loading.show();
        saveMode = EDIT_MODE;

        this.clearData();

        var url = 'FooterExcel/detailData';
        var data = {
            id: $list.$table.getValue()
        };

        api.post(url, data, function (result) {

            if (result.status == 'success') {
                $form.$pengajarDiklatId.setValue(result.data.pengajarDiklatId);

                $form.$pengajarDiklatId.focus();
            }

            $loading.hide();
        });
    };


    this.saveData = function () {
        $form.$button.$save.loading();

        switch (saveMode) {
            case EDIT_MODE:
                var url = 'FooterExcel/updateData';
                break;
            case NEW_MODE:
                var url = 'FooterExcel/createData';
        }

        var data = this.serializeData();

        api.post(url, data, function (result) {

            switch (result.status) {
                case 'success':
                    $splash.show(result.message);

                    this.readData(function () {
                        switch (saveMode) {
                            case EDIT_MODE:
                                $tab.$list.trigger('click');
                                break;
                            case NEW_MODE:
                                this.clearData();
                                $form.$pengajarDiklatId.focus();
                        }

                        $form.$button.$save.release();
                        window.scrollTo(0, 0);
                    }.bind(this));

                    break;
                case 'invalid':
                    $form.error(result);
                    $form.$button.$save.release();
            }

        }.bind(this));
    };

};

dashboard.application.controllers.FooterExcel.prototype =
    Object.create(dashboard.application.core.Controller.prototype);

dashboard.application.controllers.Login = function () {
    var $el = $('#login');

    var $form;
    var $button;

    this.init = function () {
        $form = $el.find('#form').uiForm();
        $form.$email = $form.getRef('email').uiTextBox({ icon: 'mail' });
        $form.$password = $form.getRef('password').uiTextBox({ type: TYPE_PASSWORD, icon: 'lock' });

        $button = $el.find('#button');
        $button.$login = $button.getRef('login').uiButton();
    };

    this.driveEvents = function () {
        $button.$login.on('click', this.login.bind(this));

        $form.$email.on('enter', function () {
            $form.$password.focus();
        });

        $form.$password.on('enter', function () {
            $button.$login.trigger('click');
        });

    };

    this.login = function () {

        $button.$login.loading();

        $form.clear();

        var data = {
            email: $form.$email.getValue(),
            password: $form.$password.getValue(),
        };

        api.post('Account/login', data, function (result) {

            switch (result.status) {
                case 'success':

                    localStorage.namaUser = result.data.namaUser;
                    localStorage.userType = result.data.type;
                    localStorage.userId = result.data.userId;
                    localStorage.authKey = result.data.auth;
                    localStorage.menuAccess = result.data.menuAccess;
                    localStorage.panelAccess = result.data.panelAccess;
                    localStorage.instansiId = result.data.instansiId;

                    $form.$email.clear();
                    $form.$password.clear();

                    router.redirect('#/desktop');

                    $el.fadeOut();
                    break;
                case 'invalid':
                    $form.error(result);
            }

            $button.$login.release();
        }.bind(this));

    };
};

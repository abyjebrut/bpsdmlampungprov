dashboard.application.controllers.ArsipMateri = function () {
    var $el = $page.getScope('ArsipMateri');
    dashboard.application.core.Controller.call(this, $el);

    var rs = [];
    var renderedRs = [];

    var rsRange = config.paging.range;

    var tableHeads = ['MATA_DIKLAT'];
    var tableFields = ['mataDiklat'];

    var saveMode;

    var rsItems = [];
    var cartHeads = ['PENGAJAR'];
    var cartFields = ['pengajar'];

    var $tab;
    var $list;
    var $preview;
    var $form;

    this.init = function () {

        // tab

        $tab = $el.find('#tab');
        $tab.$list = $tab.getRef('list');
        $tab.$preview = $tab.getRef('preview');
        $tab.$form = $tab.getRef('form');
        $tab.$new = $tab.getRef('new');

        // list

        $list = $el.find('#list');
        $list.$jenisDiklatId = $list.getRef('jenisDiklatId').uiComboBox({ placeholder: 'Filter By Jenis Diklat' });
        $list.$namaDiklat = $list.getRef('namaDiklat').uiComboBox({ placeholder: 'Filter By Nama Diklat' });
        $list.$search = $list.getRef('search').uiTextBox({ icon: 'search', placeholder: 'Search...' });
        $list.$publishStatusId = $list.getRef('publishStatusId').uiRadioBox();
        $list.$table = $list.getRef('table').uiTable({ headers: tableHeads });
        $list.$paging = $list.getRef('paging').uiPaging();

        $list.$button = $list.find('#button');
        $list.$button.$delete = $list.$button.getRef('delete').uiButton();
        $list.$button.$detail = $list.$button.getRef('detail').uiButton();
        $list.$button.$edit = $list.$button.getRef('edit').uiButton();

        $list.$button.$refresh = $list.$button.getRef('refresh').uiButton();


        // preview

        $preview = $el.find('#preview');

        $preview.$publishStatus = $preview.getRef('publishStatus').uiTextView();
        $preview.$jenisDiklat = $preview.getRef('jenisDiklat').uiTextView();
        $preview.$namaDiklat = $preview.getRef('namaDiklat').uiTextView();
        $preview.$mataDiklat = $preview.getRef('mataDiklat').uiTextView();
        $preview.$fileWord = $preview.getRef('fileWord').uiTextView();
        $preview.$fileExcel = $preview.getRef('fileExcel').uiTextView();
        $preview.$filePowerPoint = $preview.getRef('filePowerPoint').uiTextView();
        $preview.$filePdf = $preview.getRef('filePdf').uiTextView();
        $preview.$links = $preview.getRef('links').uiTableView(cartHeads);

        $preview.$button = $preview.find('#button');
        $preview.$button.$edit = $preview.$button.getRef('edit').uiButton();
        $preview.$button.$cancel = $preview.$button.getRef('cancel').uiButton();

        // form

        $form = $el.find('#form').uiForm();

        $form.$publishStatusId = $form.getRef('publishStatusId').uiRadioBox();
        $form.$jenisDiklatId = $form.getRef('jenisDiklatId').uiRadioBox();
        $form.$diklatId = $form.getRef('diklatId').uiComboBox();
        $form.$mataDiklat = $form.getRef('mataDiklat').uiTextBox();
        $form.$fileWord = $form.getRef('fileWord').uiUploadFile();
        $form.$fileExcel = $form.getRef('fileExcel').uiUploadFile();
        $form.$filePowerPoint = $form.getRef('filePowerPoint').uiUploadFile();
        $form.$filePdf = $form.getRef('filePdf').uiUploadFile();

        $form.$pengajarDiklatId = $form.getRef('pengajarDiklatId').uiComboBox();
        $form.$links = $form.getRef('links').uiTable(cartHeads);
        $form.$add = $form.getRef('add');
        $form.$delete = $form.getRef('delete');

        $form.$button = $form.find('#button');
        $form.$button.$save = $form.$button.getRef('save').uiButton();
        $form.$button.$cancel = $form.$button.getRef('cancel').uiButton();

    };

    this.driveEvents = function () {
        // tab

        $tab.$preview.on('click', this.detailData.bind(this));
        $tab.$form.on('click', this.editData.bind(this));
        $tab.$new.on('click', this.newData.bind(this));

        // list

        //        $list.$jenisDiklatId.on('click', this.listNamaDiklat.bind(this));
        $list.$jenisDiklatId.on('change', this.readData.bind(this));
        $list.$namaDiklat.on('change', this.readData.bind(this));
        $list.$search.on('keyup', this.searchData.bind(this));
        $list.$publishStatusId.on('click', this.readData.bind(this));

        $list.$table.on('dblclick', function () {
            $tab.$preview.trigger('click');
        });

        $list.$paging.on('click', this.moveData.bind(this));
        $list.$button.$delete.on('click', this.deleteData.bind(this));

        $list.$button.$detail.on('click', function () {
            $tab.$preview.trigger('click');
        });

        $list.$button.$edit.on('click', function () {
            $tab.$form.trigger('click');
        });

        $list.$button.$refresh.on('click', this.loadData.bind(this));

        // preview

        $preview.$button.$edit.on('click', function () {
            $tab.$form.trigger('click');
        });

        $preview.$button.$cancel.on('click', function () {
            $tab.$list.trigger('click');
        });

        // form

        $form.$jenisDiklatId.on('change', this.filterNamaDiklat.bind(this));

        $form.$add.on('click', this.addToCart.bind(this));
        $form.$delete.on('click', this.deleteFromCart.bind(this));

        $form.$button.$save.on('click', this.saveData.bind(this));

        $form.$button.$cancel.on('click', function () {
            $tab.$list.trigger('click');
        });

    };

    this.driveFieldSequence = function () {

        //        $form.$nama.on('enter', function () {
        //            $form.$button.$save.trigger('click');
        //        });

    };

    this.reload = function () {
        $tab.$list.trigger('click');
    };

    this.loadData = function () {
        this.readData();

        $form.$diklatId.renderApi('Diklat/readData', { text: 'namaDiklat' });
        $form.$pengajarDiklatId.renderApi('PengajarDiklat/readData', { text: 'nama' });
        $form.$publishStatusId.renderApi('PublishStatus/readData', { text: 'publishStatus' });
        $list.$publishStatusId.renderApi('PublishStatus/readData', { text: 'publishStatus' });
        $list.$namaDiklat.renderApi('Diklat/readData', { text: 'namaDiklat' });

        $list.$jenisDiklatId.renderApi('JenisDiklat/readData', { orderBy: 'position' }, { text: 'jenisDiklat' });
        $form.$jenisDiklatId.renderApi('JenisDiklat/readData', { orderBy: 'position' }, { text: 'jenisDiklat' });
    };

    this.filterNamaDiklat = function () {
        var jenisDiklatId = $form.$jenisDiklatId.getValue();
        $form.$diklatId.renderApi('Diklat/readData', { jenisDiklatId: jenisDiklatId }, { text: 'namaDiklat' });
    };

    this.listNamaDiklat = function () {
        var jenisDiklatId = $list.$jenisDiklatId.getValue();
        $list.$namaDiklat.renderApi('Diklat/readData', { jenisDiklatId: jenisDiklatId }, { text: 'namaDiklat' });
    };

    this.readData = function (callback) {

        if (!callback)
            $loading.show();

        var url = 'ArsipMateri/readData';
        var data = {
            orderBy: 'mataDiklat',
            reverse: 1,
            publishStatusId: $list.$publishStatusId.getValue(),
            jenisDiklatId: $list.$jenisDiklatId.getValue(),
            namaDiklat: $list.$namaDiklat.getText(),
        };

        api.post(url, data, function (result) {

            rs = result.data;
            var rsCount = rs.length;

            if (rsCount > 0) {
                var showButtons = true;
                $tab.$preview.show();
                $tab.$form.show();
            }
            else {
                var showButtons = false;
                $tab.$preview.hide();
                $tab.$form.hide();
            }

            $list.$button.$delete.setEnabled(showButtons);
            $list.$button.$detail.setEnabled(showButtons);
            $list.$button.$edit.setEnabled(showButtons);

            renderedRs = this.renderTable($list.$table, rs, tableFields, rsRange);
            this.renderPaging($list.$paging, renderedRs, rsRange);

            if ($.isFunction(callback))
                callback();
            else
                $loading.hide();
        }.bind(this));
    };

    this.searchData = function () {

        var filteredRs = rs.filter(function (data) {
            var regex = new RegExp($list.$search.getValue(), 'gi');
            var found = false;

            tableFields.forEach(function (field) {
                if (data[field])
                    if (data[field].match(regex))
                        found = true;
            });

            return found;
        });

        renderedRs = this.renderTable($list.$table, filteredRs, tableFields, rsRange);
        this.renderPaging($list.$paging, renderedRs, rsRange);
    };

    this.moveData = function () {
        renderedRs = this.renderTable($list.$table, renderedRs, tableFields, rsRange, $list.$paging.getValue());
    };


    this.deleteData = function () {
        var checkedId = $list.$table.getCheckedValues();

        if (checkedId.length == 0) {
            $msgbox.alert('NO_DATA_CHECKED');
            return;
        };

        var url = 'ArsipMateri/multipleDeleteData';
        var data = {
            multipleId: checkedId,
        };

        $msgbox.confirm('DELETE_CHECKED_DATA', function () {
            api.post(url, data, function (result) {

                switch (result.status) {
                    case 'success':
                        $splash.show(result.message);
                        this.readData();

                        break;
                    case 'failed':
                        $msgbox.alert(result.message);
                }

            }.bind(this));
        }.bind(this));

    };

    this.detailData = function () {
        $loading.show();

        var url = 'ArsipMateri/detailData';
        var data = {
            id: $list.$table.getValue()
        };

        api.post(url, data, function (result) {

            if (result.status == 'success') {
                $preview.$publishStatus.setValue(result.data.publishStatus);
                $preview.$jenisDiklat.setValue(result.data.jenisDiklat);
                $preview.$namaDiklat.setValue(result.data.namaDiklat);
                $preview.$mataDiklat.setValue(result.data.mataDiklat);
                $preview.$fileWord.setValue(result.data.fileWord);
                $preview.$fileExcel.setValue(result.data.fileExcel);
                $preview.$filePowerPoint.setValue(result.data.filePowerPoint);
                $preview.$filePdf.setValue(result.data.filePdf);

                rsItems = result.data.links;
                this.renderViewCart();

            }

            $loading.hide();
        }.bind(this));
    };

    this.serializeData = function () {
        var data = {
            publishStatusId: $form.$publishStatusId.getValue(),
            diklatId: $form.$diklatId.getValue(),
            mataDiklat: $form.$mataDiklat.getValue(),
            fileWord: $form.$fileWord.getValue(),
            fileExcel: $form.$fileExcel.getValue(),
            filePowerPoint: $form.$filePowerPoint.getValue(),
            filePdf: $form.$filePdf.getValue(),
            links: rsItems,

        };

        if (saveMode == EDIT_MODE)
            data.id = $list.$table.getValue();

        return data;
    };

    this.clearData = function () {
        $form.clear();
        $form.$publishStatusId.clear();
        $form.$jenisDiklatId.clear();
        $form.$diklatId.clear();
        $form.$mataDiklat.clear();
        $form.$fileWord.clear();
        $form.$fileExcel.clear();
        $form.$filePowerPoint.clear();
        $form.$filePdf.clear();
        $form.$pengajarDiklatId.clear();

        rsItems = new Array();
        this.renderEditCart();

    };

    this.editData = function () {
        $loading.show();
        saveMode = EDIT_MODE;

        this.clearData();

        $form.$diklatId.renderApi('Diklat/readData', { text: 'namaDiklat' });

        var url = 'ArsipMateri/detailData';
        var data = {
            id: $list.$table.getValue()
        };

        api.post(url, data, function (result) {

            if (result.status == 'success') {
                $form.$publishStatusId.setValue(result.data.publishStatusId);
                $form.$jenisDiklatId.setValue(result.data.jenisDiklatId);
                $form.$diklatId.setValue(result.data.diklatId);
                $form.$mataDiklat.setValue(result.data.mataDiklat);
                $form.$fileWord.setValue(result.data.fileWord);
                $form.$fileExcel.setValue(result.data.fileExcel);
                $form.$filePowerPoint.setValue(result.data.filePowerPoint);
                $form.$filePdf.setValue(result.data.filePdf);

                rsItems = new Array();
                result.data.links.forEach(function (data1) {

                    rsItems.push({
                        id: data1.id,
                        pengajar: data1.pengajar,
                        pengajarDiklatId: data1.pengajarDiklatId,
                    });

                });
                this.renderEditCart();

                $form.$mataDiklat.focus();
            }

            $loading.hide();
        }.bind(this));
    };

    this.newData = function () {
        saveMode = NEW_MODE;

        this.clearData();
        $form.$mataDiklat.focus();
    };

    this.saveData = function () {
        $form.$button.$save.loading();

        switch (saveMode) {
            case EDIT_MODE:
                var url = 'ArsipMateri/updateData';
                break;
            case NEW_MODE:
                var url = 'ArsipMateri/createData';
        }

        var data = this.serializeData();

        api.post(url, data, function (result) {

            switch (result.status) {
                case 'success':
                    $splash.show(result.message);

                    this.readData(function () {
                        switch (saveMode) {
                            case EDIT_MODE:
                                $tab.$list.trigger('click');
                                break;
                            case NEW_MODE:
                                this.clearData();
                                $form.$mataDiklat.focus();
                        }

                        $form.$button.$save.release();
                        window.scrollTo(0, 0);
                    }.bind(this));

                    break;
                case 'invalid':
                    $form.error(result);
                    $form.$button.$save.release();
            }

        }.bind(this));
    };

    this.renderViewCart = function () {
        this.renderTable($preview.$links, rsItems, cartFields, 100);
    };

    this.renderEditCart = function () {
        this.renderTable($form.$links, rsItems, cartFields, 100);
    };

    this.addToCart = function () {

        if (!$form.$pengajarDiklatId.getValue()) {
            $msgbox.alert('PENGAJAR_REQUIRED');
            return;
        }

        var found = false;
        rsItems.forEach(function (data) {

            if (data.pengajarDiklatId == $form.$pengajarDiklatId.getValue())
                found = true;
        });

        if (found) {
            $msgbox.alert('PENGAJAR_IS_REG');
            return;
        }

        rsItems.push({
            id: this.guid(),
            pengajarDiklatId: $form.$pengajarDiklatId.getValue(),
            pengajar: $form.$pengajarDiklatId.getText(),
        });

        $form.$pengajarDiklatId.clear();
        $form.$pengajarDiklatId.focus();

        this.renderEditCart();
    };

    this.deleteFromCart = function () {
        var checkedId = $form.$links.getCheckedValues();
        if (checkedId.length <= 0)
            return;

        $msgbox.confirm('Delete selected data', function () {

            checkedId.forEach(function (indexId) {
                var i = -1;

                rsItems.forEach(function (data) {
                    i++;

                    if (data.id == indexId)
                        rsItems.splice(i, 1);

                });
            });

            this.renderEditCart();
        }.bind(this));

    };

};

dashboard.application.controllers.ArsipMateri.prototype =
    Object.create(dashboard.application.core.Controller.prototype);

dashboard.application.controllers.DiklatFungsional = function () {
    var $el = $page.getScope('DiklatFungsional');
    dashboard.application.core.Controller.call(this, $el);

    var rs = [];
    var rows = [];
    var rsBahan = [];
    var renderedRs = [];

    var rsRange = config.paging.range;

    var tableHeads = ['Jenis Diklat', 'Kode', 'Nama Diklat', 'Kuota', 'Status'];
    var tableFields = ['jenisDiklat', 'kode', 'namaDiklat', 'kuota', 'statusDiklat'];

    var rsBahanMapel = [];
    var viewHeadsMapel = ['No', 'Mata Pelajaran', 'Waktu', 'Jenis Diklat', 'Tipe', 'JML JP'];
    var viewFieldsMapel = ['noUrut', 'mataPelajaran', 'waktu', 'jenisDiklat', 'tipeMataPelajaran', 'jumlahJp_'];

    var viewHeadsBahan = ['No', 'Tanggal', 'Waktu', 'Mata Pelajaran', 'Nama Pengajar', 'Ruang', 'Jumlah Jam'];
    var viewFieldsBahan = ['noUrut', 'allTanggal', 'allWaktu', 'mataPelajaran', 'allPengajar', 'ruang', 'allJam'];

    var rsPengajar = [];
    var cartHeads = ['Nama Pengajar'];
    var cartFields = ['namaPengajar'];

    var rsLokasi = [];
    var cartHeadsLokasi = ['Lokasi Pelaksanaan', 'Angkatan'];
    var cartFieldsLokasi = ['lokasi', 'angkatan'];

    var rsPengajarD = [];
    var rsIsiMapel = [];

    var saveMode;
    var saveModeDialog;
    var detailMode;
    var tipeWaktuId;
    var tipeTanggalId;
    var jenisDiklatIdPengajar;
    var angkatanIdPengajar;
    var tglSelesai;

    var idMapelSelect = ''
    var idDiklat = ''
    var $tab;
    var $list;
    var $listBahan;
    var $preview;
    var $form;

    var monthRecords = [
        'Jan',
        'Feb',
        'Mar',
        'Apr',
        'Mei',
        'Jun',
        'Jul',
        'Ags',
        'Sept',
        'Okt',
        'Nov',
        'Dec',
    ]

    var hariRecords = [
        'Minggu',
        'Senin',
        'Selasa',
        'Rabu',
        'Kamis',
        'Jum`at',
        'Sabtu',
    ]

    this.init = function () {

        // tab

        $tab = $el.find('#tab');
        $tab.$list = $tab.getRef('list');
        $tab.$preview = $tab.getRef('preview');
        $tab.$form = $tab.getRef('form');
        $tab.$new = $tab.getRef('new');

        // list

        $list = $el.find('#list');
        $list.$filter = $list.getRef('filter').uiComboBox({ placeholder: 'Filter By Jenis Diklat' });
        $list.$publishStatusId = $list.getRef('publishStatusId').uiRadioBox();
        $list.$search = $list.getRef('search').uiTextBox({ icon: 'search', placeholder: 'Search...' });
        // $list.$table = $list.getRef('table').uiTable({ headers: tableHeads });
        $list.$table = $list.getRef('table');
        $list.$paging = $list.getRef('paging').uiPaging();

        $list.$button = $list.find('#button');
        $list.$button.$delete = $list.$button.getRef('delete').uiButton();
        $list.$button.$detail = $list.$button.getRef('detail').uiButton();
        $list.$button.$edit = $list.$button.getRef('edit').uiButton();
        $list.$button.$jadwal = $list.$button.getRef('jadwal').uiButton();

        $list.$button.$refresh = $list.$button.getRef('refresh').uiButton();

        // preview

        $preview = $el.find('#preview');

        $preview.$publishStatus = $preview.getRef('publishStatus').uiTextView();
        $preview.$jenisDiklat = $preview.getRef('jenisDiklat').uiTextView();
        $preview.$kode = $preview.getRef('kode').uiTextView();
        $preview.$elDetail = $preview.getRef('elDetail');
        $preview.$elDetail.hide();
        $preview.$listJadwalDetail = $preview.getRef('listJadwalDetail').uiTableViewClick(viewHeadsBahan);

        $preview.$elDetailInput = $preview.getRef('elDetailInput');
        $preview.$elDetailInput.hide();

        $preview.$listJadwalDetailInput = $preview.getRef('listJadwalDetailInput');

        $preview.$namaDiklat = $preview.getRef('namaDiklat').uiTextAreaView();
        $preview.$tahunDiklat = $preview.getRef('tahunDiklat').uiTextView();
        $preview.$userCreated = $preview.getRef('userCreated').uiTextView();
        $preview.$tglCreated = $preview.getRef('tglCreated').uiTextView();
        $preview.$userUpdated = $preview.getRef('userUpdated').uiTextView();
        $preview.$tglUpdated = $preview.getRef('tglUpdated').uiTextView();
        $preview.$lokasi = $preview.getRef('lokasi').uiTextView();
        $preview.$angkatan = $preview.getRef('angkatan').uiTextView();
        $preview.$pelaksanaanMulai = $preview.getRef('pelaksanaanMulai').uiTextView();
        $preview.$pelaksanaanSelesai = $preview.getRef('pelaksanaanSelesai').uiTextView();
        $preview.$kuota = $preview.getRef('kuota').uiTextView();
        $preview.$statusDiklat = $preview.getRef('statusDiklat').uiTextView();


        $preview.$button = $preview.find('#button');
        $preview.$button.$edit = $preview.$button.getRef('edit').uiButton();
        $preview.$button.$cancel = $preview.$button.getRef('cancel').uiButton();
        $preview.$button.$excel = $preview.$button.getRef('excel').uiButton();

        // dialog User

        $preview.$dialogUser = $preview.getRef('dialogUser');

        $preview.$dialogUser.$userCreated = $preview.$dialogUser.getRef('userCreated').uiTextView();
        $preview.$dialogUser.$tglCreated = $preview.$dialogUser.getRef('tglCreated').uiTextView();
        $preview.$dialogUser.$userUpdated = $preview.$dialogUser.getRef('userUpdated').uiTextView();
        $preview.$dialogUser.$tglUpdated = $preview.$dialogUser.getRef('tglUpdated').uiTextView();
        $preview.$dialogUser.$keteranganUser = $preview.$dialogUser.getRef('keteranganUser').uiTextView();
        $preview.$dialogUser.$noUrutUser = $preview.$dialogUser.getRef('noUrutUser').uiTextView();
        $preview.$dialogUser.$mataPelajaranUser = $preview.$dialogUser.getRef('mataPelajaranUser').uiTextView();
        $preview.$dialogUser.$button = $preview.$dialogUser.find('.actions');
        $preview.$dialogUser.$button.$cancel = $preview.$dialogUser.$button.getRef('cancel').uiButton();

        // end dialog User

        // dialog

        $preview.$editDialog = $preview.getRef('editDialog');

        $preview.$editDialog.$pengajarDiklatId = $preview.$editDialog.getRef('pengajarDiklatId').uiComboBox();
        $preview.$editDialog.$mataPelajaran = $preview.$editDialog.getRef('mataPelajaran').uiTextView();
        $preview.$editDialog.$ruang = $preview.$editDialog.getRef('ruang').uiTextBox();
        $preview.$editDialog.$keterangan = $preview.$editDialog.getRef('keterangan').uiTextBox();
        $preview.$editDialog.$tanggal = $preview.$editDialog.getRef('tanggal').uiDatePicker();
        $preview.$editDialog.$elWaktu = $preview.$editDialog.getRef('elWaktu');
        $preview.$editDialog.$elWaktu.hide();
        $preview.$editDialog.$elTanggal = $preview.$editDialog.getRef('elTanggal');
        $preview.$editDialog.$elTanggal.hide();
        $preview.$editDialog.$noUrut = $preview.$editDialog.getRef('noUrut').uiTextView();
        $preview.$editDialog.$jamMulai = $preview.$editDialog.getRef('jamMulai').uiTextView();
        $preview.$editDialog.$jamSelesai = $preview.$editDialog.getRef('jamSelesai').uiTextView();
        $preview.$editDialog.$jumlahHari = $preview.$editDialog.getRef('jumlahHari').uiTextView();
        $preview.$editDialog.$tanggalSelesai_ = $preview.$editDialog.getRef('tanggalSelesai_').uiTextView();
        $preview.$editDialog.$listPengajar = $preview.$editDialog.getRef('listPengajar').uiTable(cartHeads);

        $preview.$editDialog.$add = $preview.$editDialog.getRef('add');
        $preview.$editDialog.$delete = $preview.$editDialog.getRef('delete');
        $preview.$editDialog.$delete.hide();
        $preview.$editDialog.$button = $preview.$editDialog.find('.actions');

        $preview.$editDialog.$button.$save = $preview.$editDialog.$button.getRef('save').uiButton();
        $preview.$editDialog.$button.$cancel = $preview.$editDialog.$button.getRef('cancel').uiButton();

        // end dialog

        // form

        $form = $el.find('#form').uiForm();

        $form.$publishStatusId = $form.getRef('publishStatusId').uiRadioBox();
        $form.$jenisDiklatId = $form.getRef('jenisDiklatId').uiComboBox();
        $form.$kode = $form.getRef('kode').uiTextView();
        $form.$elKode = $form.getRef('elKode');
        $form.$elKode.hide();
        $form.$lNew = $form.getRef('lNew');
        $form.$lNew.hide();
        $form.$lEdit = $form.getRef('lEdit');
        $form.$lEdit.hide();
        $form.$jenisDiklat = $form.getRef('jenisDiklat').uiTextView();
        $form.$pelaksanaanMulaiEdit = $form.getRef('pelaksanaanMulaiEdit').uiTextView();
        $form.$pelaksanaanSelesaiEdit = $form.getRef('pelaksanaanSelesaiEdit').uiTextView();
        $form.$namaDiklat = $form.getRef('namaDiklat').uiTextArea();
        $form.$tahunDiklat = $form.getRef('tahunDiklat').uiComboBox();
        $form.$lokasiId = $form.getRef('lokasiId').uiComboBox();
        $form.$angkatanId = $form.getRef('angkatanId').uiComboBox();
        $form.$pelaksanaanMulai = $form.getRef('pelaksanaanMulai').uiDatePicker();
        $form.$pelaksanaanSelesai = $form.getRef('pelaksanaanSelesai').uiDatePicker();
        $form.$kuota = $form.getRef('kuota').uiNumericBox({ maxlength: 3 });
        $form.$statusDiklatId = $form.getRef('statusDiklatId').uiRadioBox();
        $form.$listMataPelajaran = $form.getRef('listMataPelajaran').uiTable(viewHeadsMapel);
        $form.$delete = $form.getRef('delete');
        $form.$delete.hide();

        $form.$button = $form.find('#button');
        $form.$button.$save = $form.$button.getRef('save').uiButton();
        $form.$button.$cancel = $form.$button.getRef('cancel').uiButton();

    };

    this.renderRsTable = function ($tableRs, dataSource, keyFields, range, pageActive) {

        if ($(window).height() > 900)
            range = 15;

        if (!pageActive)
            var pageActive = 1;

        var rowActive = (pageActive - 1) * range;
        rows = [];

        for (var i = rowActive; i < dataSource.length; i++) {
            var row = [];
            var field = dataSource[i];

            if (field) {

                keyFields.forEach(function (keyField) {
                    row.push(field[keyField]);
                });

                rows.push({
                    id: field.id,
                    jenisDiklatId: field.jenisDiklatId,
                    jenisDiklat: field.jenisDiklat,
                    kode: field.kode,
                    namaDiklat: field.namaDiklat,
                    kuota: field.kuota,
                    kuotaDiklat: field.kuotaDiklat,
                    statusDiklat: field.statusDiklat,
                });
            }
        }

        var tableHeadsHtml = '';

        tableHeadsHtml += `<th style="display:none;" >Id</th>`;
        for (tableHead of tableHeads) {
            tableHeadsHtml += `<th>${tableHead}</th>`;
        }

        var html = `
        <table id="faiz1" class="hover" style="width: 100%">
        <thead>
            <tr>
                ${tableHeadsHtml}
                <th>Aksi</th>
            </tr>
            
        </thead>
        <tbody>
        `;
        var button1 = '';
        for (row of rows) {
            html += '<tr style="cursor: pointer">';
            html += `<td style="display:none; border-bottom: solid silver 1px">${row.id}</td>`;
            html += `<td style="border-bottom: solid silver 1px">${row.jenisDiklat}</td>`;
            html += `<td style="border-bottom: solid silver 1px">${row.kode}</td>`;
            html += `<td style="border-bottom: solid silver 1px">${row.namaDiklat}</td>`;
            html += `<td style="border-bottom: solid silver 1px">${row.kuota}</td>`;
            html += `<td style="border-bottom: solid silver 1px">${row.statusDiklat}</td>`;
            button1 = ''
            if (row.kuotaDiklat == 0)
                button1 = `<button class="act-delete" data-id="${row.id}" href="javascript:">
                Hapus
                </button>
                <button class="act-detail" data-id="${row.id}" href="javascript:">
                Lihat
                </button>
                <button class="act-edit" data-id="${row.id}" href="javascript:">
                Ubah
                </button>
                <button class="act-jadwal" data-id="${row.id}" href="javascript:">
                Jadwal
                </button>
                `
            else
                button1 = `
                <button class="act-detail" data-id="${row.id}" href="javascript:">
                Lihat
                </button>
                `
            html += `<td style="width: 90px">${button1}
                    </td>`;

            html += '</tr>';
        }
        html += '</tbody></table>'

        $tableRs.html(html);
        $tableRs.find('table').DataTable({
            searching: false,
            "paging": false,
        });

        var table = $('#faiz1').DataTable();

        $('#faiz1 tbody').on('click', 'button', function () {
            var data = table.row($(this).parents('tr')).data();
        });


        $tableRs.find('.act-delete, .act-detail, .act-edit, .act-jadwal, .act-copy').off('click');
        $tableRs.find('.act-delete').on('click', this.deleteData.bind(this));
        $tableRs.find('.act-detail').on('click', this.detailData.bind(this));
        $tableRs.find('.act-jadwal').on('click', this.detailData.bind(this));
        $tableRs.find('.act-detail').on('click', () => {
            detailMode = 'Detail';
            saveModeDialog = 'Detail';
            $tab.$preview.trigger('click');
        });
        $tableRs.find('.act-edit').on('click', this.editData.bind(this));
        $tableRs.find('.act-edit').on('click', () => {
            $tab.$form.trigger('click');
        });
        $tableRs.find('.act-jadwal').on('click', () => {
            detailMode = 'Input';
            saveModeDialog = 'Detail';
            $tab.$preview.trigger('click');
        });
        return dataSource;
    };

    this.driveEvents = function () {
        // tab

        $tab.$preview.on('click', this.detailData.bind(this));

        $tab.$form.on('click', this.editData.bind(this));
        $tab.$new.on('click', this.newData.bind(this));

        // list

        $list.$filter.on('change', this.readData.bind(this));
        $list.$search.on('keyup', this.searchData.bind(this));
        $list.$publishStatusId.on('click', this.readData.bind(this));


        $list.$paging.on('click', this.moveData.bind(this));
        $list.$button.$delete.on('click', this.deleteData.bind(this));

        $list.$button.$detail.on('click', function () {
            detailMode = 'Detail'
            $tab.$preview.trigger('click');
        });
        $list.$button.$jadwal.on('click', function () {
            detailMode = 'Input'
            $tab.$preview.trigger('click');
        });

        $list.$button.$edit.on('click', function () {
            $tab.$form.trigger('click');
        });

        $list.$button.$refresh.on('click', this.reload.bind(this));

        // preview

        $preview.$button.$edit.on('click', function () {
            $tab.$form.trigger('click');
        });

        $preview.$button.$cancel.on('click', function () {
            $tab.$list.trigger('click');
        });


        $preview.$listJadwalDetail.on('dblclick', this.showDetailUserCreated.bind(this));

        $preview.$dialogUser.$button.$cancel.on('click', this.hideDetailUserCreated.bind(this));

        $preview.$button.$excel.on('click', this.exportDetailToExcel.bind(this));

        $preview.$editDialog.$add.on('click', this.addToCart.bind(this));
        $preview.$editDialog.$delete.on('click', this.deleteFromCart.bind(this));
        $preview.$editDialog.$button.$save.on('click', this.saveJadwal.bind(this));
        $preview.$editDialog.$button.$cancel.on('click', this.clearEditDataSubMenu.bind(this));

        $preview.$editDialog.$pengajarDiklatId.on('change', this.clearDataShowError.bind(this));
        // form

        $form.$delete.on('click', this.deleteBahanMapelCart.bind(this));
        $form.$lokasiId.on('change', this.showFormAngkatan.bind(this));
        $form.$jenisDiklatId.on('change', this.showFormLokasi.bind(this));
        $form.$tahunDiklat.on('change', this.showFormLokasi1.bind(this));
        $form.$button.$save.on('click', this.saveData.bind(this));

        $form.$button.$cancel.on('click', function () {
            $tab.$list.trigger('click');
        });

    };

    this.driveFieldSequence = function () {

    };


    this.showFormLokasi = function () {

        var jenisDiklatId = $form.$jenisDiklatId.getValue();
        var tahunDiklat = $form.$tahunDiklat.getValue();

        if (jenisDiklatId && tahunDiklat)
            $form.$lokasiId.renderApi('Diklat/readLokasiData', { id: idDiklat, jenisDiklatId: jenisDiklatId, tahunDiklat: tahunDiklat }, { text: 'lokasi' });

        var url = 'MataPelajaran/readData';
        var data = {
            orderBy: 'jenisDiklatId, noUrut, jamMulai',
            reverse: 2,
            jenisDiklatId: jenisDiklatId,
            publishStatusId: 2,
        };

        rsBahanMapel = [];
        api.post(url, data, function (result) {

            result.data.forEach(row => {
                var aWaktu = row.tipeWaktuId;
                if (aWaktu == 1) {
                    var waktu = row.jamMulai + ' s/d ' + row.jamSelesai;
                } else {
                    var waktu = row.jumlahHari + ' hari';
                }

                rsBahanMapel.push({
                    id: row.id,
                    publishStatusId: row.publishStatusId,
                    publishStatus: row.publishStatus,
                    noUrut: row.noUrut,
                    waktu: waktu,
                    labelMapelId: row.labelMapelId,
                    mataPelajaran: row.mataPelajaran,
                    jamMulai: row.jamMulai,
                    nilaiJamMulai: row.nilaiJamMulai,
                    _jamMulai: row._jamMulai,
                    jamMulaiMenit: row.jamMulaiMenit,
                    jamSelesai: row.jamSelesai,
                    nilaiJamSelesai: row.nilaiJamSelesai,
                    _jamSelesai: row._jamSelesai,
                    jamSelesaiMenit: row.jamSelesaiMenit,
                    jumlahHari: row.jumlahHari,
                    jenisDiklatId: row.jenisDiklatId,
                    jenisDiklat: row.jenisDiklat,
                    tipeMataPelajaranId: row.tipeMataPelajaranId,
                    tipeMataPelajaran: row.tipeMataPelajaran,
                    tipeWaktuId: row.tipeWaktuId,
                    tipeWaktu: row.tipeWaktu,
                    tipeTanggalId: row.tipeTanggalId,
                    tipeTanggal: row.tipeTanggal,
                    jumlahJp: row.jumlahJp,
                    jumlahJp_: row.jumlahJp_,
                });
            });

            $form.$delete.hide();
            if (rsBahanMapel.length > 0)
                $form.$delete.show();

            this.renderCartBahanMapel();
        }.bind(this));

    };

    this.renderCartBahanMapel = function () {
        this.renderTableNoPaging($form.$listMataPelajaran, rsBahanMapel, viewFieldsMapel, 1000);
    };

    this.deleteBahanMapelCart = function () {
        var checkedId = $form.$listMataPelajaran.getCheckedValues();
        if (checkedId.length <= 0)
            return;

        $msgbox.confirm('Delete selected data', function () {

            checkedId.forEach(function (indexId) {
                var i = -1;

                rsBahanMapel.forEach(function (data) {
                    i++;

                    if (data.id == indexId)
                        rsBahanMapel.splice(i, 1);

                });
                if (rsBahanMapel.length <= 0)
                    $form.$delete.hide();
            });

            this.renderCartBahanMapel();
        }.bind(this));

    };

    this.showFormLokasi1 = function () {

        var jenisDiklatId = $form.$jenisDiklatId.getValue();
        var tahunDiklat = $form.$tahunDiklat.getValue();

        if (jenisDiklatId && tahunDiklat)
            $form.$lokasiId.renderApi('Diklat/readLokasiData', { id: idDiklat, jenisDiklatId: jenisDiklatId, tahunDiklat: tahunDiklat }, { text: 'lokasi' });
    };

    this.showFormAngkatan = function () {

        var jenisDiklatId = $form.$jenisDiklatId.getValue();
        var tahunDiklat = $form.$tahunDiklat.getValue();
        var lokasiId = $form.$lokasiId.getValue();
        if (lokasiId)
            $form.$angkatanId.renderApi('Diklat/readAngkatanData', { id: idDiklat, jenisDiklatId: jenisDiklatId, tahunDiklat: tahunDiklat, lokasiId: lokasiId }, { text: 'angkatan' });

    };

    this.clearEditDataSubMenu = function () {
        $preview.$editDialog.modal('hide');
    };

    this.showDetailUserCreated = function () {
        var rowSubMenu = [];

        $.each(rsBahan, function (i, row) {
            if (row.id == $preview.$listJadwalDetail.getValue())
                rowSubMenu = row;
        });

        if (rowSubMenu.userIdUpdated == 0)
            var tglUpdate = '';
        else
            var tglUpdate = rowSubMenu._tanggalUpdated;
        $preview.$dialogUser.$userCreated.setValue(rowSubMenu.nameCreated);
        $preview.$dialogUser.$tglCreated.setValue(rowSubMenu._tanggalCreated);
        $preview.$dialogUser.$userUpdated.setValue(rowSubMenu.nameUpdated);
        $preview.$dialogUser.$tglUpdated.setValue(tglUpdate);
        $preview.$dialogUser.$keteranganUser.setValue(rowSubMenu.keterangan);
        $preview.$dialogUser.$noUrutUser.setValue(rowSubMenu.noUrut);
        $preview.$dialogUser.$mataPelajaranUser.setValue(rowSubMenu.mataPelajaran);

        $preview.$dialogUser.modal('setting', 'closable', false).modal('show');
    };

    this.hideDetailUserCreated = function () {
        $preview.$dialogUser.modal('hide');
    };

    this.reload = function () {

        $tab.$list.trigger('click');
        this.readData();
        // var i =2018
        var rsTahun = [];
        for (let i = 2018; i < 2040; i++) {
            rsTahun.push({
                value: i,
                text: i,
            });
        }

        $list.$publishStatusId.renderApi('PublishStatus/readData', {
            text: 'publishStatus',
            done: function () {
                $list.$publishStatusId.setValue(2);
                this.readData();
            }.bind(this),
        });

        $form.$tahunDiklat.render(rsTahun);
        $list.$filter.renderApi('JenisDiklat/readDataFungsional', { orderBy: 'position' }, { text: 'jenisDiklat' });
        $form.$jenisDiklatId.renderApi('JenisDiklat/readDataFungsional', { text: 'jenisDiklat' });
        $form.$statusDiklatId.renderApi('StatusDiklat/readData', { text: 'statusDiklat' });
        $form.$publishStatusId.renderApi('PublishStatus/readData', { text: 'publishStatus' });
        $form.$angkatanId.renderApi('Angkatan/readData', { text: 'angkatan' });
        $form.$lokasiId.renderApi('Lokasi/readData', { text: 'lokasi' });

    };
    this.readData = function (callback) {

        if (!callback)
            $loading.show();

        var url = 'Diklat/readDataFungsional';
        var data = {
            orderBy: '_pelaksanaanMulai',
            reverse: 1,
            jenisDiklatId: $list.$filter.getValue(),
            publishStatusId: $list.$publishStatusId.getValue(),
        };

        api.post(url, data, function (result) {

            rs = result.data;
            var rsCount = rs.length;

            if (rsCount > 0) {
                this.dataShow();
            }
            else {
                this.dataHide();
            }

            renderedRs = this.renderRsTable($list.$table, rs, tableFields, rsRange);
            this.renderPaging($list.$paging, renderedRs, rsRange);

            if ($.isFunction(callback))
                callback();
            else
                $loading.hide();
        }.bind(this));
    };

    this.dataHide = function () {
        $list.$paging.hide();
        // $tab.$form.hide();
        $list.$button.$delete.hide();
        $list.$button.$edit.hide();
        $list.$button.$detail.hide();
        $list.$button.$jadwal.hide();
    };

    this.dataShow = function () {
        $list.$paging.show();
        // $tab.$form.show();
        $list.$button.$delete.show();
        $list.$button.$edit.show();
        $list.$button.$detail.show();
        $list.$button.$jadwal.show();
    };

    this.searchData = function () {

        var filteredRs = rs.filter(function (data) {
            var regex = new RegExp($list.$search.getValue(), 'gi');
            var found = false;

            tableFields.forEach(function (field) {
                if (data[field])
                    if (data[field].match(regex))
                        found = true;
            });

            return found;
        });

        renderedRs = this.renderRsTable($list.$table, filteredRs, tableFields, rsRange);
        if (renderedRs.length > 0) {
            this.renderPaging($list.$paging, renderedRs, rsRange);
            this.dataShow();
        } else {
            this.dataHide();
        }
    };

    this.moveData = function () {
        renderedRs = this.renderTable($list.$table, renderedRs, tableFields, rsRange, $list.$paging.getValue());
    };

    this.deleteData = function () {
        // var checkedId = $list.$table.getCheckedValues();
        // var checkedId = [id];
        var id = $(event.target).data('id');
        if (!id)
            return;

        // if (!id) {
        //     $msgbox.alert('NO_DATA_CHECKED');
        //     return;
        // };

        var url = 'Diklat/deleteData';
        var data = {
            id: id,
        };

        var rowDik = [];
        $.each(rows, function (i, row) {
            if (row.id == id)
                rowDik = row;
        });

        $msgbox.confirm('Hapus Data Nama Diklat = ' + ' ' + rowDik.namaDiklat + ' ??', function () {
            api.post(url, data, function (result) {

                switch (result.status) {
                    case 'success':
                        $splash.show(result.message);
                        this.readData();
                        break;
                    case 'failed':
                        $msgbox.alert(result.message);
                }

            }.bind(this));
        }.bind(this));

    };

    this.detailData = function () {
        var id
        if (saveModeDialog != 'Modal') {
            id = $(event.target).data('id');
            idDiklat = id
        }
        if (!id)
            id = idDiklat;
        else if (!id)
            return;
        $loading.show();

        var url = 'Diklat/detailData';
        var data = {
            id: id
        };

        api.post(url, data, function (result) {

            if (result.status == 'success') {
                $preview.$publishStatus.setValue(result.data.publishStatus);
                $preview.$jenisDiklat.setValue(result.data.jenisDiklat);
                $preview.$kode.setValue(result.data.kode);
                $preview.$namaDiklat.setValue(result.data.namaDiklat);
                $preview.$tahunDiklat.setValue(result.data.tahunDiklat);
                $preview.$userCreated.setValue(result.data.nameCreated);
                if (result.data.userIdCreated == 0)
                    var tglC = ''
                else
                    var tglC = result.data._tanggalCreated

                $preview.$tglCreated.setValue(tglC);
                $preview.$userUpdated.setValue(result.data.nameUpdated);
                if (result.data.userIdUpdated == 0)
                    var tglU = ''
                else
                    var tglU = result.data._tanggalUpdated

                $preview.$tglUpdated.setValue(tglU);
                $preview.$lokasi.setValue(result.data.lokasi);
                $preview.$angkatan.setValue(result.data.angkatan);
                $preview.$pelaksanaanMulai.setValue(result.data.pelaksanaanMulai);
                $preview.$pelaksanaanSelesai.setValue(result.data.pelaksanaanSelesai);
                $preview.$kuota.setValue(result.data.kuota);
                $preview.$statusDiklat.setValue(result.data.statusDiklat);

                var filterReloadJadwal = result.data.filterReloadJadwal;

                $preview.$elDetail.hide();
                $preview.$elDetailInput.hide();
                $preview.$button.$edit.hide();;
                if (detailMode == 'Detail') {
                    $preview.$elDetail.show();
                    $preview.$button.$edit.show();
                } else {
                    $preview.$elDetailInput.show();

                }
                rsBahan = [];
                rsPengajarD = result.data.listPengajar
                idDiklat = result.data.id
                result.data.listJadwalDiklat.forEach(data1 => {

                    var date1 = new Date(data1.tanggal);
                    var bulan = monthRecords[date1.getMonth()]
                    var hari = hariRecords[date1.getDay()]
                    var date1Selesai = new Date(data1.tanggalSelesai);
                    var bulanSelesai = monthRecords[date1Selesai.getMonth()]
                    var hariSelesai = hariRecords[date1Selesai.getDay()]
                    var allTanggal, allWaktu, allPengajar, allJam
                    var statusSimpan = data1.statusSimpan
                    var pengajarDiklatId = data1.pengajarDiklatId;
                    var htmlView = '';
                    var noi = 0
                    rsPengajar = [];
                    result.data.listJadwalDiklatPengajar.forEach(row1 => {
                        if (data1.id == row1.bahanJadwalDiklatId) {
                            noi++
                            htmlView += noi + '. ' + row1.namaPengajar + '<br>';
                            rsPengajar.push({
                                id: row1.id,
                                bahanJadwalDiklatId: row1.id,
                                pengajarDiklatId: row1.pengajarDiklatId,
                                namaPengajar: row1.namaPengajar,
                            });
                        }

                    });

                    this.renderCart();

                    allPengajar = htmlView
                    if (data1.jumlahJp == 0)
                            allJam = '-'
                        else
                            allJam = data1.jumlahJp

                    if (data1.tipeWaktuId == 1) {

                        allTanggal = hari + '<br>' + date1.getDate() + ' ' + bulan + ' ' + date1.getFullYear()

                        allWaktu = data1.jamMulai + ' - ' + data1.jamSelesai
                        

                    } else {

                            allTanggal = hari + ', ' + date1.getDate() + ' ' + bulan + ' ' + date1.getFullYear() + '<br>' + ' s/d ' + '<br>' + hariSelesai + ', ' + date1Selesai.getDate() + ' ' + bulanSelesai + ' ' + date1Selesai.getFullYear()
                        allWaktu = data1.jumlahHari + ' hari ' + data1.tipeTanggal
                        
                    }
                    rsBahan.push({
                        id: data1.id,
                        allWaktu: allWaktu,
                        allTanggal: allTanggal,
                        allPengajar: allPengajar,
                        allJam: allJam,
                        diklatId: data1.diklatId,
                        jenisDiklatId: data1.jenisDiklatId,
                        listJadwalDiklatPengajar:result.data.listJadwalDiklatPengajar,
                        ruang: data1.ruang,
                        pengajarDiklatId: data1.pengajarDiklatId,
                        userIdCreated: data1.userIdCreated,
                        nameCreated: data1.nameCreated,
                        _tanggalCreated: data1._tanggalCreated,
                        userIdUpdated: data1.userIdUpdated,
                        nameUpdated: data1.nameUpdated,
                        _tanggalUpdated: data1._tanggalUpdated,
                        namaPengajar: data1.namaPengajar,
                        keterangan: data1.keterangan,
                        mataPelajaranId: data1.mataPelajaranId,
                        labelMapelId: data1.labelMapelId,
                        mataPelajaran: data1.mataPelajaran,
                        noUrut: data1.noUrut,
                        tipeTanggalId: data1.tipeTanggalId,
                        jumlahHari: data1.jumlahHari,
                        tipeWaktuId: data1.tipeWaktuId,
                        tipeWaktu: data1.tipeWaktu,
                        tipeMataPelajaranId: data1.tipeMataPelajaranId,
                        tipeMataPelajaran: data1.tipeMataPelajaran,
                        statusSimpan: statusSimpan,
                        tanggal: data1.tanggal,
                        _tanggal: data1._tanggal,
                        tanggalSelesai: data1.tanggalSelesai,
                        _tanggalSelesai: data1._tanggalSelesai,
                        jamMulai: data1.jamMulai,
                        nilaiJamMulai: data1.nilaiJamMulai,
                        _jamMulai: data1._jamMulai,
                        jamMulaiMenit: data1.jamMulaiMenit,
                        jamSelesai: data1.jamSelesai,
                        nilaiJamSelesai: data1.nilaiJamSelesai,
                        _jamSelesai: data1._jamSelesai,
                        jamSelesaiMenit: data1.jamSelesaiMenit,
                        jumlahJp: data1.jumlahJp,
                        jumlahJam: data1.jumlahJam,
                    });
                });

                if (detailMode == 'Detail') {
                    this.renderViewCart();
                } else {
                    this.renderEditCart();
                }


            }

            $loading.hide();
        }.bind(this));
    };


    this.serializeData = function () {
        var date1 = new Date();
        var tanggalI = date1.getFullYear() + '-' + (date1.getMonth() + 1) + '-' + date1.getDate() + ' ' + date1.getHours() + ':' + date1.getMinutes() + ':' + date1.getSeconds();
        var data = {
            userId: localStorage.userId,
            tanggalI: tanggalI,
            publishStatusId: $form.$publishStatusId.getValue(),
            jenisDiklatId: $form.$jenisDiklatId.getValue(),
            lokasiId: $form.$lokasiId.getValue(),
            tahunDiklat: $form.$tahunDiklat.getValue(),
            angkatanId: $form.$angkatanId.getValue(),
            namaDiklat: $form.$namaDiklat.getValue(),
            pelaksanaanMulai: $form.$pelaksanaanMulai.getValue(),
            pelaksanaanSelesai: $form.$pelaksanaanSelesai.getValue(),
            kuota: $form.$kuota.getValue(),
            statusDiklatId: $form.$statusDiklatId.getValue(),
            listMapel: rsBahanMapel,
        };

        if (saveMode == EDIT_MODE)
            data.id = idDiklat;

        return data;
    };


    this.clearData = function () {
        $form.clear();
        let tgl1 = new Date()
        let tgl = tgl1.getFullYear() + '-' + (tgl1.getMonth() + 1) + '-' + tgl1.getDate()

        $form.$publishStatusId.setValue(2);
        $form.$kode.clear();
        $form.$namaDiklat.clear();
        $form.$tahunDiklat.setValue(tgl1.getFullYear());
        $form.$angkatanId.clear();
        $form.$lokasiId.clear();
        $form.$pelaksanaanMulai.setValue(tgl);
        $form.$pelaksanaanSelesai.setValue(tgl);
        $form.$kuota.setValue(40);
        $form.$statusDiklatId.setValue(1);

        
        rsBahanMapel = new Array();
        this.renderCartBahanMapel();

    };

    this.editData = function () {
        var id = $(event.target).data('id');

        if (!id)
            return;
        idDiklat = id;
        $loading.show();

        this.clearData();
        $form.$lEdit.show();
        $form.$lNew.hide();
        saveMode = EDIT_MODE;
        $form.$elKode.show();
        var url = 'Diklat/detailData';
        var data = {
            id: id,
        };

        api.post(url, data, function (result) {

            if (result.status == 'success') {

                $form.$publishStatusId.setValue(result.data.publishStatusId);
                $form.$kode.setValue(result.data.kode);
                $form.$tahunDiklat.setValue(result.data.tahunDiklat);
                $form.$lokasiId.setValue(result.data.lokasiId);
                $form.$angkatanId.setValue(result.data.angkatanId);
                $form.$namaDiklat.setValue(result.data.namaDiklat);
                $form.$pelaksanaanMulai.setValue(result.data._pelaksanaanMulai);
                $form.$pelaksanaanSelesai.setValue(result.data._pelaksanaanSelesai);
                $form.$kuota.setValue(result.data._kuota);
                $form.$statusDiklatId.setValue(result.data.statusDiklatId);
                $form.$jenisDiklatId.setValue(result.data.jenisDiklatId);
                $form.$jenisDiklat.setValue(result.data.jenisDiklat);
                $form.$pelaksanaanMulaiEdit.setValue(result.data.pelaksanaanMulai);
                $form.$pelaksanaanSelesaiEdit.setValue(result.data.pelaksanaanSelesai);

                $form.$kode.focus();
            }

            $loading.hide();
        }.bind(this));
    };


    this.newData = function () {
        saveMode = NEW_MODE;

        $form.$jenisDiklatId.setValue($list.$filter.getValue());
        $form.$elKode.hide();
        $form.$lNew.show();
        $form.$lEdit.hide();
        this.clearData();
        $form.$jenisDiklatId.focus();
    };

    this.saveData = function () {

        if (!$form.$pelaksanaanMulai.getValue()) {
            $msgbox.alert('PELAKSANAAN_MULAI_REQUIRED');
            return;
        }

        $form.$button.$save.loading();

        switch (saveMode) {
            case EDIT_MODE:
                var url = 'Diklat/updateData';
                break;
            case NEW_MODE:
                var url = 'Diklat/createDataFungsional';
        }

        var data = this.serializeData();

        api.post(url, data, function (result) {

            switch (result.status) {
                case 'success':
                    $splash.show(result.message);

                    this.readData(function () {
                        switch (saveMode) {
                            case EDIT_MODE:
                                $tab.$list.trigger('click');
                                break;
                            case NEW_MODE:
                                $tab.$list.trigger('click');
                        }

                        $form.$button.$save.release();
                        window.scrollTo(0, 0);
                    }.bind(this));

                    break;
                case 'invalid':
                    $form.error(result);
                    $form.$button.$save.release();
            }

        }.bind(this));
    };
    this.renderDataTable = function ($table, dataSource, keyFields, range, pageActive) {

        if ($(window).height() > 900)
            range = 15;

        if (!pageActive)
            var pageActive = 1;

        var rowActive = (pageActive - 1) * range;
        rsIsiMapel = [];

        for (var i = rowActive; i < dataSource.length; i++) {
            var row = [];
            var field = dataSource[i];

            if (field) {

                keyFields.forEach(function (keyField) {
                    row.push(field[keyField]);
                });

                rsIsiMapel.push({
                    id: field.id,
                    noUrut: field.noUrut,
                    tipeTanggalId: field.tipeTanggalId,
                    tipeWaktuId: field.tipeWaktuId,
                    labelMapelId: field.labelMapelId,
                    tipeMataPelajaranId: field.tipeMataPelajaranId,
                    allTanggal: field.allTanggal,
                    allWaktu: field.allWaktu,
                    tanggal: field.tanggal,
                    pengajarDiklatId: field.pengajarDiklatId,
                    allPengajar: field.allPengajar,
                    mataPelajaran: field.mataPelajaran,
                    allJam: field.allJam,
                    ruang: field.ruang,
                    keterangan: field.keterangan,
                    statusSimpan: field.statusSimpan,
                });
            }


        }

        var tableHeadsHtml = '';
        tableHeadsHtml += `<th style="display:none;" >Id</th>`;
        for (tableHead of viewHeadsBahan) {
            tableHeadsHtml += `<th>${tableHead}</th>`;
        }

        var html = `
        <table id="zea" class="hover" style="width: 100%">
        <thead>
            <tr>
                ${tableHeadsHtml}
                <th>Aksi</th>
            </tr>
            
        </thead>
        <tbody>
        `;

        for (row of rsIsiMapel) {
            html += '<tr style="cursor: pointer">';
            html += `<td style="display:none; border-bottom: solid silver 1px">${row.id}</td>`;
            html += `<td style="border-bottom: solid silver 1px">${row.noUrut}</td>`;
            html += `<td style="border-bottom: solid silver 1px">${row.allTanggal}</td>`;
            html += `<td style="border-bottom: solid silver 1px">${row.allWaktu}</td>`;
            html += `<td style="border-bottom: solid silver 1px">${row.mataPelajaran}</td>`;
            html += `<td style="border-bottom: solid silver 1px">${row.allPengajar}</td>`;
            html += `<td style="border-bottom: solid silver 1px">${row.ruang}</td>`;
            html += `<td style="border-bottom: solid silver 1px">${row.allJam}</td>`;
            html += `<td style="border-bottom: solid silver 1px; width: 40px">
                    <button data-toggle="modal" data-backdrop="static" data-keyboard="false" class="act-simpan" data-id="${row.id}" href="javascript:">
                     Input
                    </button>
            </td>`;
            html += '</tr>';
        }
        html += '</tbody></table>'


        $table.html(html);
        $table.find('table').DataTable({
            searching: true,
            "paging": false,
        });

        var table = $('#zea').DataTable();

        $('#zea tbody').on('click', 'button', function () {
            var data = table.row($(this).parents('tr')).data();
            idMapelSelect = data[0];
        });

        var order = table.order();

        table
            .order([1, 'asc'], [3, 'asc'])
            .draw();


        $table.find('.act-simpan').off('click');
        $table.find('.act-simpan').on('click', this.addJadwal.bind(this));
        return dataSource;
    };

    this.clearDataShowError = function () {
        $preview.$editDialog.$pengajarDiklatId.hideError();
    };

    this.addJadwal = function () {

        $preview.$editDialog.$delete.hide();
        if (rsPengajar.length > 0)
            $preview.$editDialog.$delete.show();

        var id = $(event.target).data('id');
        if (!id)
            return;

        $preview.$editDialog.$noUrut.clear();
        $preview.$editDialog.$mataPelajaran.clear();
        $preview.$editDialog.$ruang.clear();
        $preview.$editDialog.$keterangan.clear();
        $preview.$editDialog.$jamMulai.clear();
        $preview.$editDialog.$jamSelesai.clear();
        $preview.$editDialog.$jumlahHari.clear();
        $preview.$editDialog.$tanggalSelesai_.clear();
        $preview.$editDialog.$pengajarDiklatId.clear();

        var rowSubMenu = [];

        $.each(rsBahan, function (i, row) {
            if (row.id == id)
                rowSubMenu = row;
        });

        $preview.$editDialog.$noUrut.setValue(rowSubMenu.noUrut);
        $preview.$editDialog.$ruang.setValue(rowSubMenu.ruang);
        $preview.$editDialog.$keterangan.setValue(rowSubMenu.keterangan);
        $preview.$editDialog.$mataPelajaran.setValue(rowSubMenu.mataPelajaran);
        $preview.$editDialog.$jamMulai.setValue(rowSubMenu.jamMulai);
        $preview.$editDialog.$jamSelesai.setValue(rowSubMenu.jamSelesai);
        $preview.$editDialog.$jumlahHari.setValue(rowSubMenu.jumlahHari);

        rsPengajar = [];
        rowSubMenu.listJadwalDiklatPengajar.forEach(row1 => {
            if (rowSubMenu.id == row1.bahanJadwalDiklatId) {
                rsPengajar.push({
                    id: row1.id,
                    pengajarDiklatId: row1.pengajarDiklatId,
                    namaPengajar: row1.namaPengajar,
                });
            }
        });


        this.renderCart();
        
        var tipeWaktuId = rowSubMenu.tipeWaktuId
        $preview.$editDialog.$elWaktu.hide();
        $preview.$editDialog.$elTanggal.hide();

        if (tipeWaktuId == 1) {
            $preview.$editDialog.$elWaktu.show();
        } else {
            $preview.$editDialog.$elTanggal.show();
        }

        $preview.$editDialog.$pengajarDiklatId.renderApi('User/readUserMapelData', { jenisDiklatId: rowSubMenu.jenisDiklatId, labelMapelId: rowSubMenu.labelMapelId }, {
            value: 'userId', text: 'namaPengajar', done: function () {
                // $preview.$editDialog.$pengajarDiklatId.setValue(rowSubMenu.pengajarDiklatId);
            }
        });

        $preview.$editDialog.modal('setting', 'closable', false).modal('show');
    };

    this.addToCart = function () {
        var id = idMapelSelect;
        if (!$preview.$editDialog.$pengajarDiklatId.getValue()) {
            $preview.$editDialog.$pengajarDiklatId.showError('Pengajar Diklat belum diisi');
            return;
        }
        var found = false;
                rsPengajar.forEach(function (data) {
                    if (data.pengajarDiklatId == $preview.$editDialog.$pengajarDiklatId.getValue())
                        found = true;
                });
        
                if (found) {
                    $preview.$editDialog.$pengajarDiklatId.showError('Pengajar Diklat sudah terdaftar');
                    return;
                }
        var url = 'Diklat/cariJadwalPengajarData';
        var data = {
            userId: localStorage.userId,
            id: id,
            pengajarDiklatId: $preview.$editDialog.$pengajarDiklatId.getValue(),
        };

        api.post(url, data, function (result) {

            if (result.status == 'success') {
                
                rsPengajar.push({
                    id: this.guid(),
                    pengajarDiklatId: $preview.$editDialog.$pengajarDiklatId.getValue(),
                    namaPengajar: $preview.$editDialog.$pengajarDiklatId.getText(),
                });
        
                $preview.$editDialog.$delete.show();
                $preview.$editDialog.$pengajarDiklatId.clear();
                $preview.$editDialog.$pengajarDiklatId.focus();
                this.renderCart();

            } else if (result.status == 'failed') {
                $preview.$editDialog.$pengajarDiklatId.showError(result.data[0]);
            }
        }.bind(this));

    };

    this.deleteFromCart = function () {
        var checkedId = $preview.$editDialog.$listPengajar.getCheckedValues();

        checkedId.forEach(function (indexId) {
            var i = -1;

            rsPengajar.forEach(function (data) {
                i++;

                if (data.id == indexId)
                    rsPengajar.splice(i, 1);

            });

            if (rsPengajar.length <= 0)
                $preview.$editDialog.$delete.hide();

        });

        this.renderCart();

    };

    this.renderCart = function () {
        this.renderTableNoPaging($preview.$editDialog.$listPengajar, rsPengajar, cartFields, 1000);
    };

    this.saveJadwal = function () {
        var id = idMapelSelect;

        saveModeDialog = 'Modal';
        $preview.$editDialog.$button.$save.loading();

        var date1 = new Date();
        var tanggalI = date1.getFullYear() + '-' + (date1.getMonth() + 1) + '-' + date1.getDate() + ' ' + date1.getHours() + ':' + date1.getMinutes() + ':' + date1.getSeconds();

        var url = 'Diklat/simpanJadwalData';
        var data = {
            userId: localStorage.userId,
            tanggalI: tanggalI,
            id: id,
            pengajarDiklatId: $preview.$editDialog.$pengajarDiklatId.getValue(),
            ruang: $preview.$editDialog.$ruang.getValue(),
            keterangan: $preview.$editDialog.$keterangan.getValue(),
            listPengajar: rsPengajar,
        };


        api.post(url, data, function (result) {

            if (result.status == 'success') {
                detailMode = 'Input'
                $tab.$preview.trigger('click');
                $preview.$editDialog.modal('hide');
            } else if (result.status == 'failed') {
                $preview.$editDialog.$pengajarDiklatId.showError(result.data[0]);
            }
        });
        $preview.$editDialog.$button.$save.release();
    };


    this.renderViewCart = function () {
        this.renderTableNoPaging($preview.$listJadwalDetail, rsBahan, viewFieldsBahan, 1000);
    };

    this.renderEditCart = function () {
        this.renderDataTable($preview.$listJadwalDetailInput, rsBahan, viewFieldsBahan, 1000);
    };

    this.exportData = function (type, url, id) {

        $loading.show();

        var data = {};
        if (id)
            data.id = id;

        api.post(url, data, function (result) {

            $loading.hide();

            switch (result.status) {
                case 'success':

                    if (type == 'pdf')
                        router.open(result.data);
                    else
                        router.redirect(result.data);

                    break;
                case 'failed':
                    $msgbox.alert(result.message);
            }
        });
    };

    this.exportDetailToExcel = function () {
        var type = 'xls';
        var url = 'Diklat/createExcelDetail';
        var id = idDiklat;

        this.exportData(type, url, id);
    };

};

dashboard.application.controllers.DiklatFungsional.prototype =
    Object.create(dashboard.application.core.Controller.prototype);

dashboard.application.controllers.PengajarDiklat = function () {
    var $el = $page.getScope('PengajarDiklat');
    dashboard.application.core.Controller.call(this, $el);

    var rs = [];
    var renderedRs = [];

    var rsRange = config.paging.range;

    var tableHeads = ['Nama Lengkap', 'EMAIL', 'PHONE_NUMBER'];
    var tableFields = ['name', 'email', 'mobileNumber'];

    var rsMapel = [];
    var cartHeads = ['Jenis Diklat', 'Mata Pelajaran'];
    var cartFields = ['namaSingkat', 'labelMapel'];

    var rsLampiran = [];
    var cartHeadsLampiran = ['Upload'];
    var cartFieldsLampiran = ['judul'];


    var saveMode;
    var lockShowPrivileges = false;

    var $tab;
    var $list;
    var $preview;
    var $form;


    this.init = function () {

        // tab

        $tab = $el.find('#tab');
        $tab.$list = $tab.getRef('list');
        $tab.$preview = $tab.getRef('preview');
        $tab.$form = $tab.getRef('form');
        $tab.$new = $tab.getRef('new');

        // list

        $list = $el.find('#list');
        $list.$search = $list.getRef('search').uiTextBox({ icon: 'search', placeholder: 'Search...' });

        $list.$table = $list.getRef('table').uiTable({ headers: tableHeads });
        $list.$paging = $list.getRef('paging').uiPaging();

        $list.$button = $list.find('#button');
        $list.$button.$delete = $list.$button.getRef('delete').uiButton();
        $list.$button.$detail = $list.$button.getRef('detail').uiButton();

        $list.$button.$refresh = $list.$button.getRef('refresh').uiButton();

        $list.$button.$excel = $list.$button.getRef('excel').uiButton();
        $list.$button.$edit = $list.$button.getRef('edit').uiButton();

        // preview

        $preview = $el.find('#preview');

        $preview.$elPengajar = $preview.getRef('elPengajar');
        $preview.$elPengajar.hide();
        $preview.$pangkatGolongan = $preview.getRef('pangkatGolongan').uiTextView();
        $preview.$jabatan = $preview.getRef('jabatan').uiTextView();
        $preview.$jenisPengajar = $preview.getRef('jenisPengajar').uiTextView();

        $preview.$gender = $preview.getRef('gender').uiTextView();
        $preview.$dateOfBirth = $preview.getRef('dateOfBirth').uiTextView();
        $preview.$name = $preview.getRef('name').uiTextView();
        $preview.$nip = $preview.getRef('nip').uiTextView();
        $preview.$photo = $preview.getRef('photo').uiImageView();
        $preview.$email = $preview.getRef('email').uiTextView();
        $preview.$mobileNumber = $preview.getRef('mobileNumber').uiTextView();
        $preview.$address = $preview.getRef('address').uiTextAreaView();
        $preview.$aktivasi = $preview.getRef('aktivasi').uiTextView();
        $preview.$listMapel = $preview.getRef('listMapel').uiTableView({ headers: cartHeads });
        $preview.$listLampiran = $preview.getRef('listLampiran').uiTableView(cartHeadsLampiran);

        $preview.$button = $preview.find('#button');
        $preview.$button.$edit = $preview.$button.getRef('edit').uiButton();
        $preview.$button.$cancel = $preview.$button.getRef('cancel').uiButton();

        // form
        $form = $el.find('#form').uiForm();

        $form.$name = $form.getRef('name').uiTextBox({ placeholder: 'Ditulis beserta gelar bila ada' });
        $form.$nip = $form.getRef('nip').uiTextBox();
        $form.$password = $form.getRef('password').uiTextBox({ type: TYPE_PASSWORD });
        $form.$retype_password = $form.getRef('retype_password').uiTextBox({ type: TYPE_PASSWORD });
        $form.$photo = $form.getRef('photo').uiUploadImage();
        $form.$email = $form.getRef('email').uiTextBox();
        $form.$mobileNumber = $form.getRef('mobileNumber').uiTextBox();
        $form.$genderId = $form.getRef('genderId').uiRadioBox();
        $form.$dateOfBirth = $form.getRef('dateOfBirth').uiDatePicker();
        $form.$pangkatGolonganId = $form.getRef('pangkatGolonganId').uiComboBox();
        $form.$jabatanId = $form.getRef('jabatanId').uiComboBox();
        $form.$jenisPengajarId = $form.getRef('jenisPengajarId').uiComboBox();


        $form.$address = $form.getRef('address').uiTextArea();

        $form.$jenisDiklatId = $form.getRef('jenisDiklatId').uiComboBox();
        $form.$labelMapelId = $form.getRef('labelMapelId').uiComboBox();
        $form.$listMapel = $form.getRef('listMapel').uiTable(cartHeads);
        $form.$add = $form.getRef('add');
        $form.$edit = $form.getRef('edit');
        $form.$edit.hide();
        $form.$delete = $form.getRef('delete');
        $form.$delete.hide();

        $form.$judul = $form.getRef('judul').uiTextBox();
        $form.$lampiran = $form.getRef('lampiran').uiUploadFilePdf();
        $form.$listLampiran = $form.getRef('listLampiran').uiTable(cartHeadsLampiran);
        $form.$addLampiran = $form.getRef('addLampiran');
        $form.$deleteLampiran = $form.getRef('deleteLampiran');
        $form.$deleteLampiran.hide();

        $form.$button = $form.find('#button');
        $form.$button.$save = $form.$button.getRef('save').uiButton();
        $form.$button.$cancel = $form.$button.getRef('cancel').uiButton();

        // dialog

        $form.$editDialog = $form.getRef('editDialog');

        $form.$editDialog.$dialogJenisDiklatId = $form.$editDialog.getRef('dialogJenisDiklatId').uiComboBox();
        $form.$editDialog.$dialogLabelMapelId = $form.$editDialog.getRef('dialogLabelMapelId').uiComboBox();

        $form.$editDialog.$button = $form.$editDialog.find('.actions');

        $form.$editDialog.$button.$save = $form.$editDialog.$button.getRef('save').uiButton();
        $form.$editDialog.$button.$cancel = $form.$editDialog.$button.getRef('cancel').uiButton();

        // end dialog

    };

    this.driveEvents = function () {
        // tab

        $tab.$preview.on('click', this.detailData.bind(this));
        $tab.$form.on('click', this.editData.bind(this));
        $tab.$new.on('click', this.newData.bind(this));

        // list
        $list.$search.on('keyup', this.searchData.bind(this));

        $list.$table.on('dblclick', function () {
            if (!$list.$search.getValue()) {
                if (rs.length > 0)
                    $tab.$preview.trigger('click');
            } else {
                if (renderedRs.length > 0)
                    $tab.$preview.trigger('click');
            }
        });

        $list.$paging.on('click', this.moveData.bind(this));
        $list.$button.$delete.on('click', this.deleteData.bind(this));

        $list.$button.$excel.on('click', this.exportListToExcel.bind(this));
        $list.$button.$detail.on('click', function () {
            $tab.$preview.trigger('click');
        });

        $list.$button.$edit.on('click', function () {
            $tab.$form.trigger('click');
        });

        $list.$button.$refresh.on('click', this.readData.bind(this));

        // preview

        $preview.$button.$edit.on('click', function () {
            $tab.$form.trigger('click');
        });

        $preview.$button.$cancel.on('click', function () {
            $tab.$list.trigger('click');
        });


        // form
        $form.$jenisDiklatId.on('change', this.showMapel.bind(this));
        $form.$add.on('click', this.addToCart.bind(this));
        $form.$edit.on('click', this.editCart.bind(this));
        $form.$delete.on('click', this.deleteFromCart.bind(this));
        $form.$editDialog.$button.$save.on('click', this.saveCart.bind(this));
        $form.$editDialog.$button.$cancel.on('click', this.clearEditDataMapel.bind(this));

        $form.$addLampiran.on('click', this.addToCartLampiran.bind(this));
        $form.$deleteLampiran.on('click', this.deleteFromCartLampiran.bind(this));


        $form.$button.$save.on('click', this.saveData.bind(this));
        $form.$button.$cancel.on('click', function () {
            $tab.$list.trigger('click');
        });

    };

    this.driveFieldSequence = function () {

        $form.$name.on('enter', function () {
            $form.$password.focus();
        });

        $form.$password.on('enter', function () {
            $form.$retype_password.focus();
        });

    };


    this.reload = function () {
        $tab.$list.trigger('click');

    };

    this.showMapel = function () {
        // var jenisDiklatId =$form.$jenisDiklatId.getValue(); 
        // $form.$labelMapelId.renderApi('MataPelajaran/readFilterLabelMapel', { jenisDiklatId:jenisDiklatId  }, { value: 'labelMapelId', text: 'mataPelajaran' });
    };

    this.showDialogMapel = function () {
        // var jenisDiklatId =$form.$editDialog.$dialogJenisDiklatId.getValue(); 
        // $form.$editDialog.$dialogLabelMapelId.renderApi('MataPelajaran/readFilterLabelMapel', { jenisDiklatId:jenisDiklatId  }, { value: 'labelMapelId', text: 'mataPelajaran' });
    };

    this.readData = function (callback) {

        if (!callback)
            $loading.show();
        $list.$search.clear();

        $form.$jenisDiklatId.renderApi('JenisDiklat/readData', { text: 'namaSingkat' });
        $form.$editDialog.$dialogJenisDiklatId.renderApi('JenisDiklat/readData', { text: 'namaSingkat' });

        $form.$genderId.renderApi('Gender/readData', { text: 'gender' });
        $form.$pangkatGolonganId.renderApi('PangkatGolongan/readData', { text: 'pangkatGolongan' });
        $form.$jabatanId.renderApi('Jabatan/readData', { text: 'jabatan' });
        $form.$jenisPengajarId.renderApi('JenisPengajar/readData', { text: 'jenisPengajar' });
        $form.$labelMapelId.renderApi('LabelMapel/readData', { text: 'labelMapel' });
        $form.$editDialog.$dialogLabelMapelId.renderApi('LabelMapel/readData', { text: 'labelMapel' });
        var url = 'User/readData';
        var data = {
            instansiId: localStorage.instansiId,
            tipeUserId: 2,
            orderBy: 'name',
        };

        api.post(url, data, function (result) {

            rs = result.data;
            var rsCount = rs.length;

            if (rsCount > 0) {
                this.dataShow();
            }
            else {
                this.dataHide();
            }

            renderedRs = this.renderTable($list.$table, rs, tableFields, rsRange);
            this.renderPaging($list.$paging, renderedRs, rsRange);

            if ($.isFunction(callback))
                callback();
            else
                $loading.hide();
        }.bind(this));
    };

    this.dataHide = function () {
        $list.$button.$delete.hide();
        $list.$button.$detail.hide();
        $list.$button.$edit.hide();
        $list.$paging.hide();
    };

    this.dataShow = function () {
        $list.$paging.show();
        $list.$button.$delete.show();
        $list.$button.$detail.show();
        $list.$button.$edit.show();
    };

    this.searchData = function () {
        var filteredRs = rs.filter(function (data) {
            var regex = new RegExp($list.$search.getValue(), 'gi');
            var foundSearch = false;
            tableFields.forEach(function (field) {
                if (data[field])
                    if (data[field].match(regex))
                        foundSearch = true;
            });
            return foundSearch;
        });

        renderedRs = this.renderTable($list.$table, filteredRs, tableFields, rsRange);
        if (renderedRs.length > 0) {
            this.renderPaging($list.$paging, renderedRs, rsRange);
            this.dataShow();
        } else {
            this.dataHide();
        }

    };

    this.moveData = function () {
        renderedRs = this.renderTable($list.$table, renderedRs, tableFields, rsRange, $list.$paging.getValue());
    };

    this.deleteData = function () {
        var checkedId = $list.$table.getCheckedValues();

        if (checkedId.length == 0) {
            $msgbox.alert('NO_DATA_CHECKED');
            return;
        };

        var url = 'User/multipleDeleteData';
        var data = {
            multipleId: checkedId,
        };

        $msgbox.confirm('DELETE_CHECKED_DATA', function () {
            api.post(url, data, function (result) {

                switch (result.status) {
                    case 'success':
                        $splash.show(result.message);
                        this.readData();

                        break;
                    case 'failed':
                        var htmlError = '';

                        $.each(result.data, function (key, value) {
                            htmlError += value + '<br>';
                        });
                        $msgbox.alert(htmlError);
                        this.readData();
                }

            }.bind(this));
        }.bind(this));

    };

    this.detailData = function () {
        $loading.show();

        var url = 'User/detailData';
        var data = {
            id: $list.$table.getValue()
        };
        api.post(url, data, function (result) {

            if (result.status == 'success') {
                $preview.$pangkatGolongan.setValue(result.data.pangkatGolongan);
                $preview.$jabatan.setValue(result.data.jabatan);
                $preview.$jenisPengajar.setValue(result.data.jenisPengajar);
                $preview.$name.setValue(result.data.name);
                $preview.$nip.setValue(result.data.nip);
                $preview.$photo.setValue(result.data.photo);
                $preview.$email.setValue(result.data.email);
                $preview.$gender.setValue(result.data.gender);
                $preview.$dateOfBirth.setValue(result.data.dateOfBirth);
                $preview.$mobileNumber.setValue(result.data.mobileNumber);
                $preview.$address.setValue(result.data.address);
                $preview.$aktivasi.setValue(result.data.aktivasi);
                rsMapel = result.data.listMapel;
                this.renderViewCart();

                if (result.data.listLampiran.length > 0) {
                    rsLampiran = new Array();
                    result.data.listLampiran.forEach(function (data1) {
                        var lampiranUrl = '../asset/archive/' + data1.lampiran;
                        var htmlUrl = '<a href="' + lampiranUrl + '" target="_blank" download>' + data1.judul + '</a>';

                        rsLampiran.push({
                            id: data1.id,
                            sppdId: data1.sppdId,
                            judul: htmlUrl,
                            lampiran: data1.lampiran,
                        });

                    });
                } else
                    rsLampiran = result.data.listLampiran;

                this.renderViewCartLampiran();
            }

            $loading.hide();
        }.bind(this));
    };

    this.serializeData = function () {
        var data = {
            instansiId: localStorage.instansiId,
            name: $form.$name.getValue(),
            nip: $form.$nip.getValue(),
            password: $form.$password.getValue(),
            retype_password: $form.$retype_password.getValue(),
            photo: $form.$photo.getValue(),
            pangkatGolonganId: $form.$pangkatGolonganId.getValue(),
            jabatanId: $form.$jabatanId.getValue(),
            jenisPengajarId: $form.$jenisPengajarId.getValue(),
            email: $form.$email.getValue(),
            genderId: $form.$genderId.getValue(),
            dateOfBirth: $form.$dateOfBirth.getValue(),
            mobileNumber: $form.$mobileNumber.getValue(),
            privilegesId: 14,
            address: $form.$address.getValue(),
            listLampiran: rsLampiran,
            tipeUserId: 2,
            listMapel: rsMapel,
        };

        if (saveMode == EDIT_MODE)
            data.id = $list.$table.getValue();

        return data;
    };

    this.clearData = function () {
        $form.clear();
        var tgl1 = new Date();
        let tgl = tgl1.getFullYear() + '-' + (tgl1.getMonth() + 1) + '-' + tgl1.getDate()

        $form.$dateOfBirth.setValue(tgl);
        $form.$name.clear();
        $form.$nip.clear();
        $form.$password.clear();
        $form.$retype_password.clear();
        $form.$photo.clear();
        $form.$email.clear();
        $form.$genderId.setValue(1);
        $form.$pangkatGolonganId.clear();
        $form.$jabatanId.clear();
        $form.$jenisPengajarId.clear();
        $form.$mobileNumber.clear();
        $form.$address.clear();

        $form.$jenisDiklatId.clear();
        $form.$labelMapelId.clear();

        rsMapel = new Array();
        this.renderEditCart();

        rsLampiran = new Array();
        this.renderEditCartLampiran();

    };

    this.editData = function () {
        $loading.show();
        saveMode = EDIT_MODE;

        $form.$password.closest('.row').hide();
        $form.$retype_password.closest('.row').hide();

        this.clearData();

        var url = 'User/detailData';
        var data = {
            id: $list.$table.getValue()
        };

        api.post(url, data, function (result) {

            if (result.status == 'success') {
                $form.$name.setValue(result.data.name);
                $form.$nip.setValue(result.data.nip);
                $form.$photo.setValue(result.data.photo);
                $form.$email.setValue(result.data.email);
                $form.$pangkatGolonganId.setValue(result.data.pangkatGolonganId);
                $form.$jabatanId.setValue(result.data.jabatanId);
                $form.$jenisPengajarId.setValue(result.data.jenisPengajarId);
                $form.$genderId.setValue(result.data.genderId);
                $form.$dateOfBirth.setValue(result.data._dateOfBirth);
                $form.$mobileNumber.setValue(result.data.mobileNumber);
                $form.$address.setValue(result.data.address);

                rsMapel = result.data.listMapel;
                $form.$delete.hide();
                $form.$edit.hide();
                if (rsMapel.length > 0) {
                    $form.$delete.show();
                    $form.$edit.show();
                }
                this.renderEditCart();
                rsLampiran = result.data.listLampiran;
                $form.$deleteLampiran.hide();
                if (rsLampiran.length > 0)
                    $form.$deleteLampiran.show();
                this.renderEditCartLampiran();
                $form.$name.focus();
            }

            $loading.hide();
        }.bind(this));
    };

    this.newData = function () {
        saveMode = NEW_MODE;

        $form.$password.closest('.row').show();
        $form.$retype_password.closest('.row').show();

        this.clearData();
        $form.$name.focus();
    };

    this.saveData = function () {
        $form.$button.$save.loading();

        switch (saveMode) {
            case EDIT_MODE:
                var url = 'User/updateData';
                break;
            case NEW_MODE:
                var url = 'User/createData';
        }

        var data = this.serializeData();

        api.post(url, data, function (result) {

            switch (result.status) {
                case 'success':
                    $splash.show(result.message);

                    this.readData(function () {
                        switch (saveMode) {
                            case EDIT_MODE:
                                this.readData();
                                $tab.$list.trigger('click');
                                break;
                            case NEW_MODE:
                                this.clearData();
                                $form.$name.focus();
                        }

                        $form.$button.$save.release();
                        window.scrollTo(0, 0);
                    }.bind(this));

                    break;
                case 'invalid':
                    $form.error(result);
                    $form.$button.$save.release();
            }

        }.bind(this));
    };

    this.addToCart = function () {

        if (!$form.$jenisDiklatId.getValue()) {
            $msgbox.alert('Jenis Diklat belum diisi');
            return;
        }

        if (!$form.$labelMapelId.getValue()) {
            $msgbox.alert('Mata Pelajaran belum diisi');
            return;
        }


        var found = false;
        rsMapel.forEach(function (data) {

            if (data.labelMapelId == $form.$labelMapelId.getValue() && data.jenisDiklatId == $form.$jenisDiklatId.getValue())
                found = true;
        });

        if (found) {
            $msgbox.alert('Mata Pelajaran sudah ada');
            return;
        }

        rsMapel.push({
            id: this.guid(),
            labelMapelId: $form.$labelMapelId.getValue(),
            labelMapel: $form.$labelMapelId.getText(),
            jenisDiklatId: $form.$jenisDiklatId.getValue(),
            namaSingkat: $form.$jenisDiklatId.getText(),
        });

        $form.$jenisDiklatId.clear();
        $form.$labelMapelId.clear();
        $form.$jenisDiklatId.focus();

        $form.$delete.show();
        $form.$edit.show();
        this.renderEditCart();
    };

    this.clearEditDataMapel = function () {
        $form.$editDialog.$dialogJenisDiklatId.clear();
        $form.$editDialog.$dialogLabelMapelId.clear();

        $form.$editDialog.modal('hide');

    };

    this.editCart = function () {

        var id = $form.$listMapel.getValue();

        if (!id)
            return;


        $form.$editDialog.$dialogJenisDiklatId.clear();
        $form.$editDialog.$dialogLabelMapelId.clear();

        var rowMapel = [];

        $.each(rsMapel, function (i, row) {
            if (row.id == id)
                rowMapel = row;
        });



        $form.$editDialog.$dialogJenisDiklatId.renderApi('JenisDiklat/readData', {
            text: 'namaSingkat', done: function () {
                $form.$editDialog.$dialogJenisDiklatId.setValue(rowMapel.jenisDiklatId);
            }
        });
        var jenisDiklatId = rowMapel.jenisDiklatId;

        $form.$editDialog.$dialogLabelMapelId.renderApi('MataPelajaran/readFilterLabelMapel', { jenisDiklatId: jenisDiklatId }, {
            value: 'labelMapelId', text: 'mataPelajaran', done: function () {
                $form.$editDialog.$dialogLabelMapelId.setValue(rowMapel.labelMapelId);
            }
        });

        $form.$editDialog.modal('setting', 'closable', false).modal('show');
    };

    this.saveCart = function () {
        var id = $form.$listMapel.getValue();

        $.each(rsMapel, function (i, row) {
            if (row.id == id) {

                rsMapel[i].jenisDiklatId = $form.$editDialog.$dialogJenisDiklatId.getValue();
                rsMapel[i].namaSingkat = $form.$editDialog.$dialogJenisDiklatId.getText();
                rsMapel[i].labelMapelId = $form.$editDialog.$dialogLabelMapelId.getValue();
                rsMapel[i].labelMapel = $form.$editDialog.$dialogLabelMapelId.getText();

            }
        });

        this.renderEditCart();
        this.clearEditDataMapel();
    };

    this.deleteFromCart = function () {
        var checkedId = $form.$listMapel.getCheckedValues();
        if (checkedId.length <= 0)
            return;

        $msgbox.confirm('Delete selected data', function () {

            checkedId.forEach(function (indexId) {
                var i = -1;

                rsMapel.forEach(function (data) {
                    i++;

                    if (data.id == indexId)
                        rsMapel.splice(i, 1);

                });
            });

            if (rsMapel.length <= 0) {
                $form.$delete.hide();
                $form.$edit.hide();
            }
            this.renderEditCart();
        }.bind(this));

    };

    this.renderEditCart = function () {
        this.renderTableNoPaging($form.$listMapel, rsMapel, cartFields, 1000);
    };

    this.renderViewCart = function () {
        this.renderTableNoPaging($preview.$listMapel, rsMapel, cartFields, 1000);
    };

    this.renderViewCartLampiran = function () {
        this.renderTableNoPaging($preview.$listLampiran, rsLampiran, cartFieldsLampiran, 100);
    };

    this.renderEditCartLampiran = function () {
        this.renderTableNoPaging($form.$listLampiran, rsLampiran, cartFieldsLampiran, 1000);
    };

    this.addToCartLampiran = function () {

        if (!$form.$judul.getValue()) {
            $msgbox.alert('Judul diperlukan');
            return;
        }

        if (!$form.$lampiran.getValue()) {
            $msgbox.alert('Lampiran diperlukan');
            return;
        }

        rsLampiran.push({
            id: this.guid(),
            lampiran: $form.$lampiran.getValue(),
            judul: $form.$judul.getValue(),
        });

        $form.$judul.focus();
        $form.$judul.clear();
        $form.$lampiran.clear();
        $form.$deleteLampiran.show();

        this.renderEditCartLampiran();
    };

    this.deleteFromCartLampiran = function () {
        var checkedId = $form.$listLampiran.getCheckedValues();

        if (checkedId.length <= 0) {
            $msgbox.alert('Tidak ada yang dipilih ')
            return;
        }

        $msgbox.confirm('Delete selected data', function () {

            checkedId.forEach(function (indexId) {
                var i = -1;
                rsLampiran.forEach(function (data) {

                    i++;
                    if (data.id == indexId)
                        rsLampiran.splice(i, 1);

                    var url = 'User/deleteDataLampiran';
                    var data = {
                        lampiran: data.lampiran
                    };

                    api.post(url, data, function () { });

                });
            });

            if (rsLampiran.length <= 0)
                $form.$deleteLampiran.hide();

            this.renderEditCartLampiran();
        }.bind(this));

    };

    this.exportData = function (type, url, id) {

        $loading.show();

        var data = {};
        if (id)
            data.id = id;

        api.post(url, data, function (result) {

            $loading.hide();

            switch (result.status) {
                case 'success':

                    if (type == 'pdf')
                        router.open(result.data);
                    else
                        router.redirect(result.data);

                    break;
                case 'failed':
                    $msgbox.alert(result.message);
            }
        });
    };

    this.exportListToExcel = function () {
        var type = 'xls';
        var url = 'User/createExcelListPengajar';

        this.exportData(type, url);
    };

};

dashboard.application.controllers.PengajarDiklat.prototype =
    Object.create(dashboard.application.core.Controller.prototype);

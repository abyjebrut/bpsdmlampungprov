dashboard.application.controllers.SoalAkd = function () {
    var $el = $page.getScope('SoalAkd');
    dashboard.application.core.Controller.call(this, $el);

    var rs = [];
    var renderedRs = [];

    var rsRange = config.paging.range;

    var tableHeads = ['NIP_NAMA', 'JABATAN', 'INSTANSI'];
    var tableFields = ['nipNama', 'jabatan', 'instansi'];

    var saveMode;

    var rsItems = [];
    var cartHeads = ['RUMUSAN_KOMPETENSI'];
    var cartFields = ['rumusanKompetensi'];

    var $tab;
    var $list;
    var $preview;
    var $form;

    this.init = function () {

        // tab

        $tab = $el.find('#tab');
        $tab.$list = $tab.getRef('list');
        $tab.$preview = $tab.getRef('preview');
        $tab.$form = $tab.getRef('form');
        $tab.$new = $tab.getRef('new');

        // list

        $list = $el.find('#list');
        $list.$search = $list.getRef('search').uiTextBox({ icon: 'search', placeholder: 'Search...' });
        $list.$table = $list.getRef('table').uiTable({ headers: tableHeads });
        $list.$paging = $list.getRef('paging').uiPaging();

        $list.$button = $list.find('#button');
        $list.$button.$delete = $list.$button.getRef('delete').uiButton();
        $list.$button.$detail = $list.$button.getRef('detail').uiButton();
        $list.$button.$edit = $list.$button.getRef('edit').uiButton();

        $list.$button.$refresh = $list.$button.getRef('refresh').uiButton();
        $list.$button.$sendEmail = $list.$button.getRef('sendEmail').uiButton();

        // preview

        $preview = $el.find('#preview');

        $preview.$jabatan = $preview.getRef('jabatan').uiTextView();
        $preview.$unitKerja = $preview.getRef('unitKerja').uiTextView();
        $preview.$instansi = $preview.getRef('instansi').uiTextView();
        $preview.$nipNama = $preview.getRef('nipNama').uiTextView();
        $preview.$kedudukan = $preview.getRef('kedudukan').uiTextView();
        $preview.$links = $preview.getRef('links').uiTableView(cartHeads);

        $preview.$button = $preview.find('#button');
        $preview.$button.$edit = $preview.$button.getRef('edit').uiButton();
        $preview.$button.$cancel = $preview.$button.getRef('cancel').uiButton();

        // form

        $form = $el.find('#form').uiForm();

        $form.$jabatan = $form.getRef('jabatan').uiTextBox();
        $form.$unitKerja = $form.getRef('unitKerja').uiTextBox();
        $form.$instansi = $form.getRef('instansi').uiTextBox();
        $form.$akunId = $form.getRef('akunId').uiComboBox();
        $form.$kedudukanId = $form.getRef('kedudukanId').uiRadioBox();


        $form.$rumusanKompetensi = $form.getRef('rumusanKompetensi').uiTextArea();
        $form.$links = $form.getRef('links').uiTable(cartHeads);
        $form.$add = $form.getRef('add');
        $form.$delete = $form.getRef('delete');

        $form.$button = $form.find('#button');
        $form.$button.$save = $form.$button.getRef('save').uiButton();
        $form.$button.$cancel = $form.$button.getRef('cancel').uiButton();

    };

    this.driveEvents = function () {
        // tab

        $tab.$preview.on('click', this.detailData.bind(this));
        $tab.$form.on('click', this.editData.bind(this));
        $tab.$new.on('click', this.newData.bind(this));

        // list

        $list.$search.on('keyup', this.searchData.bind(this));

        $list.$table.on('dblclick', function () {
            $tab.$preview.trigger('click');
        });

        $list.$paging.on('click', this.moveData.bind(this));
        $list.$button.$delete.on('click', this.deleteData.bind(this));

        $list.$button.$detail.on('click', function () {
            $tab.$preview.trigger('click');
        });

        $list.$button.$edit.on('click', function () {
            $tab.$form.trigger('click');
        });

        $list.$button.$refresh.on('click', this.readData.bind(this));
        $list.$button.$sendEmail.on('click', this.sendEmail.bind(this));

        // preview

        $preview.$button.$edit.on('click', function () {
            $tab.$form.trigger('click');
        });

        $preview.$button.$cancel.on('click', function () {
            $tab.$list.trigger('click');
        });

        // form

        $form.$add.on('click', this.addToCart.bind(this));
        $form.$delete.on('click', this.deleteFromCart.bind(this));

        $form.$button.$save.on('click', this.saveData.bind(this));

        $form.$button.$cancel.on('click', function () {
            $tab.$list.trigger('click');
        });

    };

    this.driveFieldSequence = function () {

    };

    this.reload = function () {
        $tab.$list.trigger('click');


    };


    this.readData = function (callback) {

        if (!callback)
            $loading.show();

        $form.$akunId.renderApi('AnggotaSoalAkd/readData', { text: 'nipNama' });
        $form.$kedudukanId.renderApi('Kedudukan/readData', { text: 'kedudukan' });
        var url = 'SoalAkd/readData';
        var data = {
            orderBy: 'id',
            reverse: 1,
        };

        api.post(url, data, function (result) {

            rs = result.data;
            var rsCount = rs.length;

            if (rsCount > 0) {
                this.dataShow();
            }
            else {
                this.dataHide();
            }

            renderedRs = this.renderTable($list.$table, rs, tableFields, rsRange);
            this.renderPaging($list.$paging, renderedRs, rsRange);

            if ($.isFunction(callback))
                callback();
            else
                $loading.hide();
        }.bind(this));
    };

    this.dataHide = function () {
        $list.$paging.hide();
        $tab.$form.hide();
        $tab.$preview.hide();
        $list.$button.$delete.hide();
        $list.$button.$detail.hide();
        $list.$button.$edit.hide();
        $list.$button.$sendEmail.hide();
    };

    this.dataShow = function () {
        $list.$paging.show();
        $tab.$form.show();
        $tab.$preview.show();
        $list.$button.$delete.show();
        $list.$button.$detail.show();
        $list.$button.$edit.show();
        $list.$button.$sendEmail.show();
    };

    this.searchData = function () {

        var filteredRs = rs.filter(function (data) {
            var regex = new RegExp($list.$search.getValue(), 'gi');
            var found = false;

            tableFields.forEach(function (field) {
                if (data[field])
                    if (data[field].match(regex))
                        found = true;
            });

            return found;
        });

        renderedRs = this.renderTable($list.$table, filteredRs, tableFields, rsRange);
        if (renderedRs.length > 0) {
            this.renderPaging($list.$paging, renderedRs, rsRange);
            this.dataShow();
        } else {
            this.dataHide();
        }
    };

    this.moveData = function () {
        renderedRs = this.renderTable($list.$table, renderedRs, tableFields, rsRange, $list.$paging.getValue());
    };


    this.deleteData = function () {
        var checkedId = $list.$table.getCheckedValues();

        if (checkedId.length == 0) {
            $msgbox.alert('NO_DATA_CHECKED');
            return;
        };

        var url = 'SoalAkd/multipleDeleteData';
        var data = {
            multipleId: checkedId,
        };

        $msgbox.confirm('DELETE_CHECKED_DATA', function () {
            api.post(url, data, function (result) {

                switch (result.status) {
                    case 'success':
                        $splash.show(result.message);
                        this.readData();

                        break;
                    case 'failed':
                        $msgbox.alert(result.message);
                }

            }.bind(this));
        }.bind(this));

    };

    this.detailData = function () {
        $loading.show();

        var url = 'SoalAkd/detailData';
        var data = {
            id: $list.$table.getValue()
        };

        api.post(url, data, function (result) {

            if (result.status == 'success') {
                $preview.$jabatan.setValue(result.data.jabatan);
                $preview.$unitKerja.setValue(result.data.unitKerja);
                $preview.$instansi.setValue(result.data.instansi);
                $preview.$nipNama.setValue(result.data.nipNama);
                $preview.$kedudukan.setValue(result.data.kedudukan);

                rsItems = result.data.links;
                this.renderViewCart();

            }

            $loading.hide();
        }.bind(this));
    };

    this.sendEmail = function () {
        var checkedId = $list.$table.getCheckedValues();

        if (checkedId.length > 1) {
            $msgbox.alert('CANNOT_PROCESS_MULTIPLE_SELECTION');
            return;
        };

        $msgbox.confirm('CONFRIM_NOTIF_EMAIL', function () {
            $loading.show();

            var url = 'SoalAkd/sendEmailAkd';
            var data = {
                id: $list.$table.getValue()
            };

            api.post(url, data, function (result) {

                if (result.status == 'success') {
                    $splash.show(result.message);
                }

                $loading.hide();
            });
        }.bind(this));
    };
    this.serializeData = function () {
        var data = {
            jabatan: $form.$jabatan.getValue(),
            unitKerja: $form.$unitKerja.getValue(),
            instansi: $form.$instansi.getValue(),
            akunId: $form.$akunId.getValue(),
            kedudukanId: $form.$kedudukanId.getValue(),
            links: rsItems,
        };

        if (saveMode == EDIT_MODE)
            data.id = $list.$table.getValue();

        return data;
    };

    this.clearData = function () {
        $form.$akunId.renderApi('AnggotaSoalAkd/readData', { text: 'nipNama' });

        $form.clear();
        $form.$jabatan.clear();
        $form.$unitKerja.clear();
        $form.$instansi.clear();
        $form.$akunId.clear();
        $form.$kedudukanId.clear();

        $form.$rumusanKompetensi.clear();

        rsItems = new Array();
        this.renderEditCart();

    };

    this.editData = function () {
        $loading.show();
        saveMode = EDIT_MODE;

        this.clearData();

        var url = 'SoalAkd/detailData';
        var data = {
            id: $list.$table.getValue()
        };

        api.post(url, data, function (result) {

            if (result.status == 'success') {
                $form.$jabatan.setValue(result.data.jabatan);
                $form.$unitKerja.setValue(result.data.unitKerja);
                $form.$instansi.setValue(result.data.instansi);
                $form.$akunId.setValue(result.data.akunId);
                $form.$kedudukanId.setValue(result.data.kedudukanId);

                rsItems = new Array();
                result.data.links.forEach(function (data1) {

                    rsItems.push({
                        id: data1.id,
                        rumusanKompetensi: data1.rumusanKompetensi,
                    });

                });
                this.renderEditCart();

                $form.$akunId.focus();
            }

            $loading.hide();
        }.bind(this));
    };

    this.newData = function () {
        saveMode = NEW_MODE;

        this.clearData();
        $form.$akunId.focus();
    };

    this.saveData = function () {
        $form.$button.$save.loading();

        switch (saveMode) {
            case EDIT_MODE:
                var url = 'SoalAkd/updateData';
                break;
            case NEW_MODE:
                var url = 'SoalAkd/createData';
        }

        var data = this.serializeData();

        api.post(url, data, function (result) {

            switch (result.status) {
                case 'success':
                    $splash.show(result.message);

                    this.readData(function () {
                        switch (saveMode) {
                            case EDIT_MODE:
                                $tab.$list.trigger('click');
                                break;
                            case NEW_MODE:
                                this.clearData();
                                $form.$akunId.focus();
                        }

                        $form.$button.$save.release();
                        window.scrollTo(0, 0);
                    }.bind(this));

                    break;
                case 'invalid':
                    $form.error(result);
                    $form.$button.$save.release();
            }

        }.bind(this));
    };

    this.renderViewCart = function () {
        this.renderTable($preview.$links, rsItems, cartFields, 100);
    };

    this.renderEditCart = function () {
        this.renderTable($form.$links, rsItems, cartFields, 100);
    };

    this.addToCart = function () {

        if (!$form.$rumusanKompetensi.getValue()) {
            $msgbox.alert('RUMUSAN_KOMPETENSI_REQUIRED');
            return;
        }

        var found = false;
        rsItems.forEach(function (data) {

            if (data.rumusanKompetensi == $form.$rumusanKompetensi.getValue())
                found = true;
        });

        if (found) {
            $msgbox.alert('RUMUSAN_KOMPETENSI_IS_REG');
            return;
        }

        rsItems.push({
            id: this.guid(),
            rumusanKompetensi: $form.$rumusanKompetensi.getValue(),
        });

        $form.$rumusanKompetensi.clear();
        $form.$rumusanKompetensi.focus();

        this.renderEditCart();
    };

    this.deleteFromCart = function () {
        var checkedId = $form.$links.getCheckedValues();
        if (checkedId.length <= 0)
            return;

        $msgbox.confirm('Delete selected data', function () {

            checkedId.forEach(function (indexId) {
                var i = -1;

                rsItems.forEach(function (data) {
                    i++;

                    if (data.id == indexId)
                        rsItems.splice(i, 1);

                });
            });

            this.renderEditCart();
        }.bind(this));

    };

};

dashboard.application.controllers.SoalAkd.prototype =
    Object.create(dashboard.application.core.Controller.prototype);

dashboard.application.controllers.Menu = function () {
    var $el = $page.getScope('Menu');
    dashboard.application.core.Controller.call(this, $el);

    var rs = [];
    var renderedRs = [];

    var rsRange = config.paging.range;

    var tableHeads = ['MENU_CAPTION', 'MENU_BEHAVIOR', 'PUBLISH_STATUS'];
    var tableFields = ['menuCaption', 'behavior', 'publishStatus'];

    var rsSubMenu = [];
    var cartHeads = ['SUB_MENU_CAPTION', 'MENU_BEHAVIOR', 'DESTINATION'];
    var cartFields = ['subMenuCaption', 'behavior', 'destination'];

    var saveMode;
    var languageS;
    var languageE;

    var $tab;
    var $list;
    var $preview;
    var $form;

    this.init = function () {

        // tab

        $tab = $el.find('#tab');
        $tab.$list = $tab.getRef('list');
        $tab.$preview = $tab.getRef('preview');
        $tab.$form = $tab.getRef('form');
        $tab.$new = $tab.getRef('new');

        // list

        $list = $el.find('#list');
        $list.$publishStatusId = $list.getRef('publishStatusId').uiRadioBox();
        $list.$search = $list.getRef('search').uiTextBox({ icon: 'search', placeholder: 'Search...' });
        $list.$table = $list.getRef('table').uiTable({ headers: tableHeads });
        $list.$paging = $list.getRef('paging').uiPaging();

        $list.$button = $list.find('#button');
        $list.$button.$delete = $list.$button.getRef('delete').uiButton();
        $list.$button.$detail = $list.$button.getRef('detail').uiButton();

        $list.$button.$edit = $list.$button.getRef('edit').uiButton();

        $list.$button.$up = $list.$button.getRef('up').uiButton();
        $list.$button.$down = $list.$button.getRef('down').uiButton();
        $list.$button.$refresh = $list.$button.getRef('refresh').uiButton();

        // preview

        $preview = $el.find('#preview');
        $preview.$pageContainer = $preview.getRef('pageContainer');
        $preview.$pageContainer.hide();
        $preview.$slPageContainer = $preview.getRef('slPageContainer');
        $preview.$slPageContainer.hide();
        $preview.$elPageContainer = $preview.getRef('elPageContainer');
        $preview.$elPageContainer.hide();
        $preview.$urlContainer = $preview.getRef('urlContainer');
        $preview.$urlContainer.hide();
        $preview.$subMenuContainer = $preview.getRef('subMenuContainer');
        $preview.$subMenuContainer.hide();

        $preview.$slContainer = $preview.getRef('slContainer');
        $preview.$slContainer.hide();
        $preview.$elContainer = $preview.getRef('elContainer');
        $preview.$elContainer.hide();
        $preview.$labelSl = $preview.getRef('labelSl');
        $preview.$labelEl = $preview.getRef('labelEl');

        $preview.$menuCaption = $preview.getRef('menuCaption').uiTextView();
        $preview.$menuCaptionSl = $preview.getRef('menuCaptionSl').uiTextView();
        $preview.$menuCaptionEl = $preview.getRef('menuCaptionEl').uiTextView();
        $preview.$menuBehavior = $preview.getRef('menuBehavior').uiTextView();
        $preview.$page = $preview.getRef('page').uiTextView();
        $preview.$pageSl = $preview.getRef('pageSl').uiTextView();
        $preview.$pageEl = $preview.getRef('pageEl').uiTextView();
        $preview.$url = $preview.getRef('url').uiTextView();
        $preview.$openURLOnNewTab = $preview.getRef('openURLOnNewTab').uiTextView();
        $preview.$subMenu = $preview.getRef('subMenu').uiTableView(cartHeads);
        $preview.$publishStatus = $preview.getRef('publishStatus').uiTextView();

        $preview.$button = $preview.find('#button');
        $preview.$button.$edit = $preview.$button.getRef('edit').uiButton();
        $preview.$button.$cancel = $preview.$button.getRef('cancel').uiButton();

        // form

        $form = $el.find('#form').uiForm();

        $form.$pageContainer = $form.getRef('pageContainer');
        $form.$urlContainer = $form.getRef('urlContainer');
        $form.$subMenuContainer = $form.getRef('subMenuContainer');
        $form.$subMenuPageContainer = $form.getRef('subMenuPageContainer');
        $form.$subMenuUrlContainer = $form.getRef('subMenuUrlContainer');

        $form.$slContainer = $form.getRef('slContainer');
        $form.$slContainer.hide();
        $form.$elContainer = $form.getRef('elContainer');
        $form.$elContainer.hide();
        $form.$labelSl = $form.getRef('labelSl');
        $form.$labelEl = $form.getRef('labelEl');

        $form.$slPageContainer = $form.getRef('slPageContainer');
        $form.$slPageContainer.hide();
        $form.$elPageContainer = $form.getRef('elPageContainer');
        $form.$elPageContainer.hide();

        $form.$languageCode = $form.getRef('languageCode').uiLangBox();
        $form.$languageCodeSubMenu = $form.getRef('languageCodeSubMenu').uiLangBox();
        $form.$menuCaption = $form.getRef('menuCaption').uiTextBox();
        $form.$menuCaptionSl = $form.getRef('menuCaptionSl').uiTextBox();
        $form.$menuCaptionEl = $form.getRef('menuCaptionEl').uiTextBox();
        $form.$menuBehaviorId = $form.getRef('menuBehaviorId').uiRadioBox();
        $form.$pagesId = $form.getRef('pagesId').uiComboBox();
        $form.$pagesSl = $form.getRef('pagesSl').uiTextView();
        $form.$pagesEl = $form.getRef('pagesEl').uiTextView();
        $form.$url = $form.getRef('url').uiTextBox();
        $form.$openURLOnNewTabId = $form.getRef('openURLOnNewTabId').uiCheckBox();
        $form.$openURLOnNewTabId.render([{ value: 1, text: 'Open URL On New Tab' }]);

        $form.$slSubMenuContainer = $form.getRef('slSubMenuContainer');
        $form.$slSubMenuContainer.hide();
        $form.$elSubMenuContainer = $form.getRef('elSubMenuContainer');
        $form.$elSubMenuContainer.hide();

        $form.$slSubMenuPageContainer = $form.getRef('slSubMenuPageContainer');
        $form.$slSubMenuPageContainer.hide();
        $form.$elSubMenuPageContainer = $form.getRef('elSubMenuPageContainer');
        $form.$elSubMenuPageContainer.hide();

        $form.$subMenuCaption = $form.getRef('subMenuCaption').uiTextBox();
        $form.$subMenuCaptionSl = $form.getRef('subMenuCaptionSl').uiTextBox();
        $form.$subMenuCaptionEl = $form.getRef('subMenuCaptionEl').uiTextBox();
        $form.$subMenuBehaviorId = $form.getRef('subMenuBehaviorId').uiRadioBox();
        $form.$subMenuPagesId = $form.getRef('subMenuPagesId').uiComboBox();
        $form.$subMenuPagesSl = $form.getRef('subMenuPagesSl').uiTextView();
        $form.$subMenuPagesEl = $form.getRef('subMenuPagesEl').uiTextView();
        $form.$subMenuUrl = $form.getRef('subMenuUrl').uiTextBox();
        $form.$subMenuOpenURLOnNewTabId = $form.getRef('subMenuOpenURLOnNewTabId').uiCheckBox();
        $form.$subMenuOpenURLOnNewTabId.render([{ value: 1, text: 'Open URL On New Tab' }]);

        $form.$subMenu = $form.getRef('subMenu').uiTable(cartHeads);
        $form.$add = $form.getRef('add');
        $form.$delete = $form.getRef('delete');
        $form.$edit = $form.getRef('edit');
        $form.$publishStatusId = $form.getRef('publishStatusId').uiRadioBox();

        $form.$button = $form.find('#button');
        $form.$button.$save = $form.$button.getRef('save').uiButton();
        $form.$button.$cancel = $form.$button.getRef('cancel').uiButton();

        // dialog

        $form.$editDialog = $form.getRef('editDialog');

        $form.$editDialog.$slSubContainer = $form.$editDialog.getRef('slSubContainer');
        $form.$editDialog.$slSubContainer.hide();
        $form.$editDialog.$elSubContainer = $form.$editDialog.getRef('elSubContainer');
        $form.$editDialog.$elSubContainer.hide();

        $form.$editDialog.$slSubPageContainer = $form.$editDialog.getRef('slSubPageContainer');
        $form.$editDialog.$slSubPageContainer.hide();
        $form.$editDialog.$elSubPageContainer = $form.$editDialog.getRef('elSubPageContainer');
        $form.$editDialog.$elSubPageContainer.hide();

        $form.$editDialog.$languageCodeSub = $form.$editDialog.getRef('languageCodeSub').uiLangBox();
        $form.$editDialog.$subMenuCaption = $form.$editDialog.getRef('subMenuCaption').uiTextBox();
        $form.$editDialog.$subMenuCaptionSl = $form.$editDialog.getRef('subMenuCaptionSl').uiTextBox();
        $form.$editDialog.$subMenuCaptionEl = $form.$editDialog.getRef('subMenuCaptionEl').uiTextBox();
        $form.$editDialog.$subMenuBehaviorId = $form.$editDialog.getRef('subMenuBehaviorId').uiRadioBox();
        $form.$editDialog.$subMenuPagesId = $form.$editDialog.getRef('subMenuPagesId').uiComboBox();
        $form.$editDialog.$subMenuPagesSl = $form.$editDialog.getRef('subMenuPagesSl').uiTextView();
        $form.$editDialog.$subMenuPagesEl = $form.$editDialog.getRef('subMenuPagesEl').uiTextView();
        $form.$editDialog.$subMenuUrl = $form.$editDialog.getRef('subMenuUrl').uiTextBox();
        $form.$editDialog.$subMenuOpenURLOnNewTabId = $form.$editDialog.getRef('subMenuOpenURLOnNewTabId').uiCheckBox();
        $form.$editDialog.$subMenuOpenURLOnNewTabId.render([{ value: 1, text: 'Open URL On New Tab' }]);

        $form.$editDialog.$button = $form.$editDialog.find('.actions');

        $form.$editDialog.$button.$save = $form.$editDialog.$button.getRef('save').uiButton();
        $form.$editDialog.$button.$cancel = $form.$editDialog.$button.getRef('cancel').uiButton();

        // end dialog

    };

    this.driveEvents = function () {
        // tab

        $tab.$preview.on('click', this.detailData.bind(this));
        $tab.$form.on('click', this.editData.bind(this));
        $tab.$new.on('click', this.newData.bind(this));

        // list

        $list.$publishStatusId.on('click', this.readData.bind(this));
        $list.$search.on('keyup', this.searchData.bind(this));

        $list.$table.on('dblclick', function () {
            $tab.$preview.trigger('click');
        });

        $list.$paging.on('click', this.moveData.bind(this));
        $list.$button.$delete.on('click', this.deleteData.bind(this));

        $list.$button.$detail.on('click', function () {
            $tab.$preview.trigger('click');
        });

        $list.$button.$edit.on('click', function () {
            $tab.$form.trigger('click');
        });

        $list.$button.$up.on('click', this.moveUp.bind(this));
        $list.$button.$down.on('click', this.moveDown.bind(this));
        $list.$button.$refresh.on('click', this.loadData.bind(this));

        // preview

        $preview.$button.$edit.on('click', function () {
            $tab.$form.trigger('click');
        });

        $preview.$button.$cancel.on('click', function () {
            $tab.$list.trigger('click');
        });

        // form

        $form.$menuBehaviorId.on('change', this.showHideFormContainers.bind(this));
        $form.$menuBehaviorId.trigger('change');

        $form.$subMenuBehaviorId.on('change', this.showHideFormSubContainers.bind(this));
        $form.$subMenuBehaviorId.trigger('change');

        $form.$button.$save.on('click', this.saveData.bind(this));

        $form.$add.on('click', this.addToCart.bind(this));
        $form.$delete.on('click', this.deleteFromCart.bind(this));
        $form.$edit.on('click', this.editCart.bind(this));
        $form.$subMenu.on('dblclick', this.editCart.bind(this));
        $form.$editDialog.$button.$save.on('click', this.saveCart.bind(this));
        $form.$editDialog.$button.$cancel.on('click', this.clearEditDataSubMenu.bind(this));
        $form.$editDialog.$subMenuPagesId.on('change', this.showEditSubMenuPages.bind(this));
        $form.$editDialog.$languageCodeSub.on('click', this.showHideEditLangSubContainers.bind(this));

        $form.$button.$cancel.on('click', function () {
            $tab.$list.trigger('click');
        });

        $form.$languageCode.on('click', this.showHideLangContainers.bind(this));
        $form.$pagesId.on('change', this.showPages.bind(this));

        $form.$languageCodeSubMenu.on('click', this.showHideLangSubMenuContainers.bind(this));
        $form.$subMenuPagesId.on('change', this.showSubMenuPages.bind(this));

    };

    this.driveFieldSequence = function () {
    };

    this.reload = function () {
        $tab.$list.trigger('click');

    };

    this.loadData = function () {
        this.readData();
        $form.$menuBehaviorId.renderApi('MenuBehavior/readData', { text: 'behavior' });
        $form.$pagesId.renderApi('OrderedPages/readData', { text: 'title' });
        $form.$subMenuBehaviorId.renderApi('SubMenuBehavior/readData', { text: 'behavior' });
        $form.$subMenuPagesId.renderApi('OrderedPages/readData', { text: 'title' });
        $form.$publishStatusId.renderApi('PublishStatus/readData', { text: 'publishStatus' });
        $form.$languageCode.renderApi('Language/readData', { text: 'language', flag: 'flag' });
        $form.$languageCodeSubMenu.renderApi('Language/readData', { text: 'language', flag: 'flag' });

        $list.$publishStatusId.renderApi('PublishStatus/readData', { text: 'publishStatus' });

        $form.$editDialog.$subMenuBehaviorId.renderApi('SubMenuBehavior/readData', { text: 'behavior' });
        $form.$editDialog.$subMenuPagesId.renderApi('OrderedPages/readData', { text: 'title' });
        $form.$editDialog.$languageCodeSub.renderApi('Language/readData', { text: 'language', flag: 'flag' });
    };

    this.showHideFormContainers = function () {
        $form.$pageContainer.hide();
        $form.$urlContainer.hide();
        $form.$subMenuContainer.hide();

        switch (parseInt($form.$menuBehaviorId.getValue())) {
            case 1:
                $form.$pageContainer.show();
                if ($form.$languageCode.getValue() == localStorage.primaryLang) {
                    $form.$elPageContainer.hide();
                    $form.$slPageContainer.hide();
                } else if ($form.$languageCode.getValue() == localStorage.secondaryLang) {
                    $form.$slPageContainer.show();
                    $form.$elPageContainer.hide();
                } else if ($form.$languageCode.getValue() == localStorage.extendedLang) {
                    $form.$elPageContainer.show();
                    $form.$slPageContainer.hide();
                }
                break;
            case 2:
                $form.$pageContainer.hide();
                $form.$elPageContainer.hide();
                $form.$slPageContainer.hide();
                $form.$urlContainer.show();
                break;
            case 3:
                $form.$pageContainer.hide();
                $form.$elPageContainer.hide();
                $form.$slPageContainer.hide();
                $form.$subMenuContainer.show();
        }
    };

    this.showPages = function () {
        $loading.show();

        var url = 'OrderedPages/detailData';
        var data = {
            id: $form.$pagesId.getValue()
        };

        api.post(url, data, function (result) {

            if (result.status == 'success') {
                $form.$pagesSl.setValue(result.data.title_sl);
                $form.$pagesEl.setValue(result.data.title_el);
            }

            $loading.hide();
        });
    };

    this.showSubMenuPages = function () {
        $loading.show();

        var url = 'OrderedPages/detailData';
        var data = {
            id: $form.$subMenuPagesId.getValue()
        };

        api.post(url, data, function (result) {

            if (result.status == 'success') {
                $form.$subMenuPagesSl.setValue(result.data.title_sl);
                $form.$subMenuPagesEl.setValue(result.data.title_el);
            }

            $loading.hide();
        });
    };

    this.showEditSubMenuPages = function () {
        $loading.show();

        $form.$editDialog.$subMenuPagesSl.clear();
        $form.$editDialog.$subMenuPagesEl.clear();

        var url = 'OrderedPages/detailData';
        var data = {
            id: $form.$editDialog.$subMenuPagesId.getValue()
        };

        api.post(url, data, function (result) {

            if (result.status == 'success') {
                $form.$editDialog.$subMenuPagesSl.setValue(result.data.title_sl);
                $form.$editDialog.$subMenuPagesEl.setValue(result.data.title_el);
            }

            $loading.hide();
        });
    };

    this.showHideLangContainers = function () {

        if ($form.$languageCode.getValue() == localStorage.primaryLang) {
            $form.$slContainer.hide();
            $form.$elContainer.hide();
            $form.$elPageContainer.hide();
            $form.$slPageContainer.hide();
        } else if ($form.$languageCode.getValue() == localStorage.secondaryLang) {
            $form.$slContainer.show();
            $form.$elContainer.hide();
            if (parseInt($form.$menuBehaviorId.getValue()) == 1) {
                $form.$slPageContainer.show();
                $form.$elPageContainer.hide();
            }
        } else if ($form.$languageCode.getValue() == localStorage.extendedLang) {
            $form.$slContainer.hide();
            $form.$elContainer.show();
            if (parseInt($form.$menuBehaviorId.getValue()) == 1) {
                $form.$elPageContainer.show();
                $form.$slPageContainer.hide();
            }
        }
    };

    this.showHideLangSubMenuContainers = function () {

        if ($form.$languageCodeSubMenu.getValue() == localStorage.primaryLang) {
            $form.$slSubMenuContainer.hide();
            $form.$elSubMenuContainer.hide();
            $form.$elSubMenuPageContainer.hide();
            $form.$slSubMenuPageContainer.hide();
        } else if ($form.$languageCodeSubMenu.getValue() == localStorage.secondaryLang) {
            $form.$slSubMenuContainer.show();
            $form.$elSubMenuContainer.hide();
            if (parseInt($form.$subMenuBehaviorId.getValue()) == 1) {
                $form.$slSubMenuPageContainer.show();
                $form.$elSubMenuPageContainer.hide();
            }
        } else if ($form.$languageCodeSubMenu.getValue() == localStorage.extendedLang) {
            $form.$slSubMenuContainer.hide();
            $form.$elSubMenuContainer.show();
            if (parseInt($form.$subMenuBehaviorId.getValue()) == 1) {
                $form.$elSubMenuPageContainer.show();
                $form.$slSubMenuPageContainer.hide();
            }
        }
    };

    this.showHideEditLangSubContainers = function () {

        if ($form.$editDialog.$languageCodeSub.getValue() == localStorage.primaryLang) {
            $form.$editDialog.$slSubContainer.hide();
            $form.$editDialog.$elSubContainer.hide();
            $form.$editDialog.$elSubPageContainer.hide();
            $form.$editDialog.$slSubPageContainer.hide();
        } else if ($form.$editDialog.$languageCodeSub.getValue() == localStorage.secondaryLang) {
            $form.$editDialog.$slSubContainer.show();
            $form.$editDialog.$elSubContainer.hide();
            if (parseInt($form.$editDialog.$subMenuBehaviorId.getValue()) == 1) {
                $form.$editDialog.$slSubPageContainer.show();
                $form.$editDialog.$elSubPageContainer.hide();
            }
        } else if ($form.$editDialog.$languageCodeSub.getValue() == localStorage.extendedLang) {
            $form.$editDialog.$slSubContainer.hide();
            $form.$editDialog.$elSubContainer.show();
            if (parseInt($form.$editDialog.$subMenuBehaviorId.getValue()) == 1) {
                $form.$editDialog.$elSubPageContainer.show();
                $form.$editDialog.$slSubPageContainer.hide();
            }
        }
    };

    this.showHideFormSubContainers = function () {
        $form.$subMenuPageContainer.hide();
        $form.$subMenuUrlContainer.hide();

        switch (parseInt($form.$subMenuBehaviorId.getValue())) {
            case 1:
                $form.$subMenuPageContainer.show();
                if ($form.$languageCodeSubMenu.getValue() == localStorage.primaryLang) {
                    $form.$elSubMenuPageContainer.hide();
                    $form.$slSubMenuPageContainer.hide();
                } else if ($form.$languageCodeSubMenu.getValue() == localStorage.secondaryLang) {
                    $form.$slSubMenuPageContainer.show();
                    $form.$elSubMenuPageContainer.hide();
                } else if ($form.$languageCodeSubMenu.getValue() == localStorage.extendedLang) {
                    $form.$elSubMenuPageContainer.show();
                    $form.$slSubMenuPageContainer.hide();
                }
                break;
            case 2:
                $form.$elSubMenuPageContainer.hide();
                $form.$slSubMenuPageContainer.hide();
                $form.$subMenuUrlContainer.show();
        }
    };

    this.readData = function (callback) {

        if (!callback)
            $loading.show();

        var url = 'Menu/readData';
        var data = {
            orderBy: 'position',
            publishStatusId: $list.$publishStatusId.getValue(),
        };

        api.post(url, data, function (result) {

            rs = result.data;
            var rsCount = rs.length;

            if (rsCount > 0) {
                this.dataShow();
            }
            else {
                this.dataHide();
            }

            // renderedRs = this.renderTable($list.$table, rs, tableFields, 100);
            this.renderTableNoPaging($list.$table, rs, tableFields, 100);

            if ($.isFunction(callback))
                callback();
            else
                $loading.hide();
        }.bind(this));
    };

    this.dataHide = function () {
        $list.$paging.hide();
        $tab.$form.hide();
        $tab.$preview.hide();
        $list.$button.$delete.hide();
        $list.$button.$detail.hide();
        $list.$button.$edit.hide();
        $list.$button.$down.hide();
        $list.$button.$up.hide();
    };

    this.dataShow = function () {
        $list.$paging.show();
        $tab.$form.show();
        $tab.$preview.show();
        $list.$button.$delete.show();
        $list.$button.$detail.show();
        $list.$button.$edit.show();
        $list.$button.$down.show();
        $list.$button.$up.show();

    };

    this.searchData = function () {
        var filteredRs = rs.filter(function (data) {
            var regex = new RegExp($list.$search.getValue(), 'gi');
            var found = false;

            tableFields.forEach(function (field) {
                if (data[field])
                    if (data[field].match(regex))
                        found = true;
            });

            return found;
        });

        renderedRs = this.renderTable($list.$table, filteredRs, tableFields, rsRange);
        if (renderedRs.length > 0) {
            this.renderPaging($list.$paging, renderedRs, rsRange);
            this.dataShow();
        } else {
            this.dataHide();
        }
    };

    this.moveData = function () {
        renderedRs = this.renderTable($list.$table, renderedRs, tableFields, rsRange, $list.$paging.getValue());
    };

    this.moveUp = function () {

        var activeId = $list.$table.getValue();
        var prevId = $list.$table.find('tr.active').prev().find('input[type="checkbox"]').val()

        if (!prevId)
            return;

        $loading.show();

        var url = 'Menu/moveData';
        var data = {
            idFrom: activeId,
            idTo: prevId,
        };

        api.post(url, data, function (result) {

            if (result.status == 'success') {
                this.reload();
            }

            $loading.hide();
        }.bind(this));

    };

    this.moveDown = function () {

        var activeId = $list.$table.getValue();
        var prevId = $list.$table.find('tr.active').next().find('input[type="checkbox"]').val()

        if (!prevId)
            return;

        $loading.show();

        var url = 'Menu/moveData';
        var data = {
            idFrom: activeId,
            idTo: prevId,
        };

        api.post(url, data, function (result) {

            if (result.status == 'success') {
                this.reload();
            }

            $loading.hide();
        }.bind(this));
    };

    this.deleteData = function () {
        var checkedId = $list.$table.getCheckedValues();

        if (checkedId.length == 0) {
            $msgbox.alert('NO_DATA_CHECKED');
            return;
        };

        var url = 'Menu/multipleDeleteData';
        var data = {
            multipleId: checkedId,
        };

        $msgbox.confirm('DELETE_CHECKED_DATA', function () {
            api.post(url, data, function (result) {

                switch (result.status) {
                    case 'success':
                        $splash.show(result.message);
                        this.readData();

                        break;
                    case 'failed':
                        $msgbox.alert(result.message);
                }

            }.bind(this));
        }.bind(this));

    };

    this.detailData = function () {
        $loading.show();

        var url = 'Language/detailData';

        api.post(url, function (result) {

            if (result.status == 'success') {
                languageS = result.data.secondaryEnabledId;
                languageE = result.data.extendedEnabledId;

                if (languageS == 1)
                    $preview.$slContainer.show();
                else
                    $preview.$slContainer.hide();

                if (languageE == 1)
                    $preview.$elContainer.show();
                else
                    $preview.$elContainer.hide();

                $preview.$labelSl.html(result.data.secondaryLanguage);
                $preview.$labelEl.html(result.data.extendedLanguage);
            }
        });

        var url = 'Menu/detailData';
        var data = {
            id: $list.$table.getValue(),
        };

        api.post(url, data, function (result) {

            if (result.status == 'success') {

                $preview.$menuCaption.setValue(result.data.menuCaption);
                $preview.$menuCaptionSl.setValue(result.data.menuCaption_sl);
                $preview.$menuCaptionEl.setValue(result.data.menuCaption_el);
                $preview.$menuBehavior.setValue(result.data.behavior);
                $preview.$page.setValue(result.data.page);
                $preview.$pageSl.setValue(result.data.page_sl);
                $preview.$pageEl.setValue(result.data.page_el);
                $preview.$url.setValue(result.data.url);
                $preview.$openURLOnNewTab.setValue(result.data.openURLOnNewTab);
                $preview.$publishStatus.setValue(result.data.publishStatus);

                var menuBehaviorId = result.data.menuBehaviorId;
                if (menuBehaviorId == 1) {
                    $preview.$pageContainer.show();
                    if (languageS == 1)
                        $preview.$slPageContainer.show();
                    else
                        $preview.$slPageContainer.hide();

                    if (languageE == 1)
                        $preview.$elPageContainer.show();
                    else
                        $preview.$elPageContainer.hide();

                    $preview.$urlContainer.hide();
                    $preview.$subMenuContainer.hide();
                } else if (menuBehaviorId == 2) {
                    $preview.$pageContainer.hide();
                    $preview.$slPageContainer.hide();
                    $preview.$elPageContainer.hide();
                    $preview.$urlContainer.show();
                    $preview.$subMenuContainer.hide();
                } else if (menuBehaviorId == 3) {
                    $preview.$pageContainer.hide();
                    $preview.$slPageContainer.hide();
                    $preview.$elPageContainer.hide();
                    $preview.$urlContainer.hide();
                    $preview.$subMenuContainer.show();
                }

                rsSubMenu = result.data.subMenu;
                this.renderViewCart();
            }

            $loading.hide();
        }.bind(this));
    };

    this.serializeData = function () {

        if ($form.$openURLOnNewTabId.isChecked())
            var openURLOnNewTabId = 1;
        else
            var openURLOnNewTabId = 2;

        var data = {
            menuCaption: $form.$menuCaption.getValue(),
            menuCaptionSl: $form.$menuCaptionSl.getValue(),
            menuCaptionEl: $form.$menuCaptionEl.getValue(),
            menuBehaviorId: $form.$menuBehaviorId.getValue(),
            publishStatusId: $form.$publishStatusId.getValue(),
            pagesId: $form.$pagesId.getValue(),
            url: $form.$url.getValue(),
            openURLOnNewTabId: openURLOnNewTabId,
            subMenu: rsSubMenu,
        };

        if (saveMode == EDIT_MODE)
            data.id = $list.$table.getValue();

        return data;
    };

    this.clearData = function () {
        $form.clear();

        var url = 'Language/detailData';

        api.post(url, function (result) {

            if (result.status == 'success') {
                $form.$labelSl.html(result.data.secondaryLanguage);
                $form.$labelEl.html(result.data.extendedLanguage);

            }
        });

        $form.$languageCode.setValue(localStorage.primaryLang);
        this.showHideLangContainers();

        $form.$menuCaption.clear();
        $form.$menuCaptionSl.clear();
        $form.$menuCaptionEl.clear();
        $form.$menuBehaviorId.clear();
        $form.$pagesId.clear();
        $form.$pagesSl.clear();
        $form.$pagesEl.clear();
        $form.$url.clear();
        $form.$openURLOnNewTabId.clear();
        $form.$publishStatusId.clear();

        this.clearDataSubMenu();

        rsSubMenu = new Array();
        this.renderEditCart();

    };

    this.clearDataSubMenu = function () {
        $form.$subMenuCaption.clear();
        $form.$subMenuCaptionSl.clear();
        $form.$subMenuCaptionEl.clear();
        $form.$subMenuBehaviorId.clear();
        $form.$subMenuPagesId.clear();
        $form.$subMenuPagesSl.clear();
        $form.$subMenuPagesEl.clear();
        $form.$subMenuUrl.clear();
        $form.$subMenuOpenURLOnNewTabId.clear();
        $form.$editDialog.modal('hide');

        $form.$languageCodeSubMenu.setValue(localStorage.primaryLang);
        this.showHideLangSubMenuContainers();
    };

    this.clearEditDataSubMenu = function () {
        $form.$editDialog.$subMenuCaption.clear();
        $form.$editDialog.$subMenuCaptionSl.clear();
        $form.$editDialog.$subMenuCaptionEl.clear();
        $form.$editDialog.$subMenuBehaviorId.clear();
        $form.$editDialog.$subMenuPagesId.clear();
        $form.$editDialog.$subMenuPagesSl.clear();
        $form.$editDialog.$subMenuPagesEl.clear();
        $form.$editDialog.$subMenuUrl.clear();
        $form.$editDialog.$subMenuOpenURLOnNewTabId.clear();
        $form.$editDialog.modal('hide');

        $form.$editDialog.$languageCodeSub.setValue(localStorage.primaryLang);
        this.showHideEditLangSubContainers();
    };

    this.editData = function () {
        $loading.show();
        saveMode = EDIT_MODE;

        this.clearData();

        var url = 'Menu/detailData';
        var data = {
            id: $list.$table.getValue(),
        };

        api.post(url, data, function (result) {

            if (result.status == 'success') {

                $form.$menuCaption.setValue(result.data.menuCaption);
                $form.$menuCaptionSl.setValue(result.data.menuCaption_sl);
                $form.$menuCaptionEl.setValue(result.data.menuCaption_el);
                $form.$menuBehaviorId.setValue(result.data.menuBehaviorId);
                $form.$pagesId.setValue(result.data.pagesId);
                $form.$pagesSl.setValue(result.data.page_sl);
                $form.$pagesEl.setValue(result.data.page_el);
                $form.$url.setValue(result.data.url);
                $form.$openURLOnNewTabId.setValue(result.data.openURLOnNewTabId);
                $form.$publishStatusId.setValue(result.data.publishStatusId);

                rsSubMenu = new Array();
                result.data.subMenu.forEach(function (data1) {

                    rsSubMenu.push({
                        id: data1.id,
                        subMenuCaption: data1.subMenuCaption,
                        subMenuCaption_sl: data1.subMenuCaption_sl,
                        subMenuCaption_el: data1.subMenuCaption_el,
                        subMenuBehaviorId: data1.subMenuBehaviorId,
                        behavior: data1.behavior,
                        pagesId: data1.pagesId,
                        page: data1.page,
                        destination: data1.destination,
                        uri: data1.uri,
                        url: data1.url,
                        openUrlOnNewTabId: data1.openUrlOnNewTabId,
                    });

                });
                this.renderEditCart();

                $form.$menuCaption.focus();
            }

            $loading.hide();
        }.bind(this));
    };

    this.newData = function () {
        saveMode = NEW_MODE;

        this.clearData();
        $form.$menuCaption.focus();
    };

    this.saveData = function () {
        $form.$button.$save.loading();

        switch (saveMode) {
            case EDIT_MODE:
                var url = 'Menu/updateData';
                break;
            case NEW_MODE:
                var url = 'Menu/createData';
        }

        var data = this.serializeData();

        api.post(url, data, function (result) {

            switch (result.status) {
                case 'success':
                    $splash.show(result.message);

                    this.readData(function () {
                        switch (saveMode) {
                            case EDIT_MODE:
                                $tab.$list.trigger('click');
                                break;
                            case NEW_MODE:
                                this.clearData();
                                $form.$menuCaption.focus();
                        }

                        $form.$button.$save.release();
                        window.scrollTo(0, 0);
                    }.bind(this));

                    break;
                case 'invalid':
                    $form.error(result);
                    $form.$button.$save.release();
            }

        }.bind(this));
    };

    this.renderViewCart = function () {
        this.renderTable($preview.$subMenu, rsSubMenu, cartFields, 100);
    };

    this.renderEditCart = function () {
        this.renderTable($form.$subMenu, rsSubMenu, cartFields, 100);
    };

    this.addToCart = function () {

        if (!$form.$subMenuCaption.getValue()) {
            $msgbox.alert('SUB_MENU_CAPTION_REQUIRED');
            return;
        }

        var found = false;
        rsSubMenu.forEach(function (data) {

            if (data.subMenuCaption == $form.$subMenuCaption.getValue())
                found = true;
        });

        if (found) {
            $msgbox.alert('SUB_MENU_CAPTION_IS_REG');
            return;
        }

        if ($form.$subMenuOpenURLOnNewTabId.isChecked())
            var subMenuOpenURLOnNewTabId = 1;
        else
            var subMenuOpenURLOnNewTabId = 2;

        if ($form.$subMenuBehaviorId.getValue() == 1)
            var destination = $form.$subMenuPagesId.getText();
        else
            var destination = $form.$subMenuUrl.getValue();

        rsSubMenu.push({
            id: this.guid(),
            subMenuCaption: $form.$subMenuCaption.getValue(),
            subMenuCaption_sl: $form.$subMenuCaptionSl.getValue(),
            subMenuCaption_el: $form.$subMenuCaptionEl.getValue(),
            subMenuBehaviorId: $form.$subMenuBehaviorId.getValue(),
            behavior: $form.$subMenuBehaviorId.getText(),
            pagesId: $form.$subMenuPagesId.getValue(),
            url: $form.$subMenuUrl.getValue(),
            destination: destination,
            openUrlOnNewTabId: subMenuOpenURLOnNewTabId,
        });

        this.clearDataSubMenu();

        $form.$subMenuCaption.focus();
        this.renderEditCart();
    };

    this.editCart = function () {

        var id = $form.$subMenu.getValue();

        if (!id)
            return;

        $form.$editDialog.$languageCodeSub.setValue(localStorage.primaryLang);

        $form.$editDialog.$subMenuPagesSl.clear();
        $form.$editDialog.$subMenuPagesEl.clear();

        var rowSubMenu = [];

        $.each(rsSubMenu, function (i, row) {
            if (row.id == id)
                rowSubMenu = row;
        });

        if (rowSubMenu.openUrlOnNewTabId == 1)
            $form.$editDialog.$subMenuOpenURLOnNewTabId.setValue([1]);
        else
            $form.$editDialog.$subMenuOpenURLOnNewTabId.setValue();


        $form.$editDialog.$subMenuCaption.setValue(rowSubMenu.subMenuCaption);
        $form.$editDialog.$subMenuCaptionSl.setValue(rowSubMenu.subMenuCaption_sl);
        $form.$editDialog.$subMenuCaptionEl.setValue(rowSubMenu.subMenuCaption_el);
        $form.$editDialog.$subMenuBehaviorId.setValue(rowSubMenu.subMenuBehaviorId);

        var lastId = rowSubMenu.pagesId;

        $form.$editDialog.$subMenuPagesId.renderApi('OrderedPages/readData', {
            text: 'title', done: function () {
                $form.$editDialog.$subMenuPagesId.setValue(lastId);
            }
        });

        $form.$editDialog.$subMenuUrl.setValue(rowSubMenu.url);

        var url = 'OrderedPages/detailData';
        var data = {
            id: lastId
        };

        api.post(url, data, function (result) {

            if (result.status == 'success') {
                $form.$editDialog.$subMenuPagesSl.setValue(result.data.title_sl);
                $form.$editDialog.$subMenuPagesEl.setValue(result.data.title_el);
            }
        });


        $form.$editDialog.modal('show');
    };

    this.saveCart = function () {
        var id = $form.$subMenu.getValue();

        if ($form.$editDialog.$subMenuOpenURLOnNewTabId.isChecked())
            var subMenuOpenURLOnNewTabId = 1;
        else
            var subMenuOpenURLOnNewTabId = 2;

        if ($form.$editDialog.$subMenuBehaviorId.getValue() == 1)
            var destination = $form.$editDialog.$subMenuPagesId.getText();
        else
            var destination = $form.$editDialog.$subMenuUrl.getValue();

        $.each(rsSubMenu, function (i, row) {
            if (row.id == id) {

                rsSubMenu[i].subMenuCaption = $form.$editDialog.$subMenuCaption.getValue();
                rsSubMenu[i].subMenuCaption_sl = $form.$editDialog.$subMenuCaptionSl.getValue();
                rsSubMenu[i].subMenuCaption_el = $form.$editDialog.$subMenuCaptionEl.getValue();
                rsSubMenu[i].subMenuBehaviorId = $form.$editDialog.$subMenuBehaviorId.getValue();
                rsSubMenu[i].behavior = $form.$editDialog.$subMenuBehaviorId.getText();
                rsSubMenu[i].pagesId = $form.$editDialog.$subMenuPagesId.getValue();
                rsSubMenu[i].destination = destination;
                rsSubMenu[i].url = $form.$editDialog.$subMenuUrl.getValue();
                rsSubMenu[i].openUrlOnNewTabId = subMenuOpenURLOnNewTabId;

            }
        });

        this.renderEditCart();
        this.clearEditDataSubMenu();
    };

    this.deleteFromCart = function () {
        var checkedId = $form.$subMenu.getCheckedValues();
        if (checkedId.length <= 0)
            return;

        $msgbox.confirm('Delete selected data', function () {

            checkedId.forEach(function (indexId) {
                var i = -1;

                rsSubMenu.forEach(function (data) {
                    i++;

                    if (data.id == indexId)
                        rsSubMenu.splice(i, 1);

                });
            });

            this.renderEditCart();
        }.bind(this));

    };

};

dashboard.application.controllers.Menu.prototype =
    Object.create(dashboard.application.core.Controller.prototype);

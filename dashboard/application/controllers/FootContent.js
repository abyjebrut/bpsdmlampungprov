dashboard.application.controllers.FootContent = function () {
    var $el = $page.getScope('FootContent');
    dashboard.application.core.Controller.call(this, $el);

    var rsItems = [];
    var cartHeads = ['LINK_CAPTION', 'LINK_BEHAVIOR', 'DESTINATION'];
    var cartFields = ['linkCaption', 'behavior', 'destination'];

    var $tab;
    var $preview;
    var $form;

    this.init = function () {

        // tab

        $tab = $el.find('#tab');
        $tab.$preview = $tab.getRef('preview');
        $tab.$form = $tab.getRef('form');

        // preview

        $preview = $el.find('#preview');

        $preview.$slContainer = $preview.getRef('slContainer');
        $preview.$slContainer.hide();
        $preview.$elContainer = $preview.getRef('elContainer');
        $preview.$elContainer.hide();
        $preview.$labelSl = $preview.getRef('labelSl');
        $preview.$labelEl = $preview.getRef('labelEl');

        $preview.$description = $preview.getRef('description').uiHTMLView();
        $preview.$descriptionSl = $preview.getRef('descriptionSl').uiHTMLView();
        $preview.$descriptionEl = $preview.getRef('descriptionEl').uiHTMLView();
        $preview.$links = $preview.getRef('links').uiTableView(cartHeads);

        $preview.$button = $preview.find('#button');
        $preview.$button.$edit = $preview.$button.getRef('edit').uiButton();
        $preview.$button.$cancel = $preview.$button.getRef('cancel').uiButton();

        // form

        $form = $el.find('#form').uiForm();
        $form.$pageContainer = $form.getRef('pageContainer');
        $form.$urlContainer = $form.getRef('urlContainer');
        $form.$slPageContainer = $form.getRef('slPageContainer');
        $form.$elPageContainer = $form.getRef('elPageContainer');

        $form.$slContainer = $form.getRef('slContainer');
        $form.$slContainer.hide();
        $form.$elContainer = $form.getRef('elContainer');
        $form.$elContainer.hide();
        $form.$labelSl = $form.getRef('labelSl');
        $form.$labelEl = $form.getRef('labelEl');

        $form.$slContainerLink = $form.getRef('slContainerLink');
        $form.$slContainerLink.hide();
        $form.$elContainerLink = $form.getRef('elContainerLink');
        $form.$elContainerLink.hide();

        $form.$languageCode = $form.getRef('languageCode').uiLangBox();
        $form.$languageCodeLink = $form.getRef('languageCodeLink').uiLangBox();

        $form.$description = $form.getRef('description').uiRichText();
        $form.$descriptionSl = $form.getRef('descriptionSl').uiRichText();
        $form.$descriptionEl = $form.getRef('descriptionEl').uiRichText();
        $form.$linkCaption = $form.getRef('linkCaption').uiTextBox();
        $form.$linkCaptionSl = $form.getRef('linkCaptionSl').uiTextBox();
        $form.$linkCaptionEl = $form.getRef('linkCaptionEl').uiTextBox();
        $form.$linkBehaviorId = $form.getRef('linkBehaviorId').uiRadioBox();
        $form.$pagesId = $form.getRef('pagesId').uiComboBox();
        $form.$pagesSl = $form.getRef('pagesSl').uiTextView();
        $form.$pagesEl = $form.getRef('pagesEl').uiTextView();
        $form.$url = $form.getRef('url').uiTextBox();
        $form.$openURLOnNewTabId = $form.getRef('openURLOnNewTabId').uiCheckBox();
        $form.$openURLOnNewTabId.render([{ value: 1, text: 'Open URL On New Tab' }]);
        $form.$links = $form.getRef('links').uiTable(cartHeads);
        $form.$add = $form.getRef('add');
        $form.$delete = $form.getRef('delete');
        $form.$edit = $form.getRef('edit');

        $form.$button = $form.find('#button');
        $form.$button.$save = $form.$button.getRef('save').uiButton();
        $form.$button.$cancel = $form.$button.getRef('cancel').uiButton();

        // dialog

        $form.$editDialog = $form.getRef('editDialog');

        $form.$editDialog.$slPageContainer = $form.getRef('slPageContainer');
        $form.$editDialog.$elPageContainer = $form.getRef('elPageContainer');

        $form.$editDialog.$slContainerLink = $form.getRef('slContainerLink');
        $form.$editDialog.$slContainerLink.hide();
        $form.$editDialog.$elContainerLink = $form.getRef('elContainerLink');
        $form.$editDialog.$elContainerLink.hide();

        $form.$editDialog.$languageCodeLink = $form.getRef('languageCodeLink').uiLangBox();
        $form.$editDialog.$linkCaption = $form.$editDialog.getRef('linkCaption').uiTextBox();
        $form.$editDialog.$linkCaptionSl = $form.$editDialog.getRef('linkCaptionSl').uiTextBox();
        $form.$editDialog.$linkCaptionEl = $form.$editDialog.getRef('linkCaptionEl').uiTextBox();
        $form.$editDialog.$linkBehaviorId = $form.$editDialog.getRef('linkBehaviorId').uiRadioBox();
        $form.$editDialog.$pagesId = $form.$editDialog.getRef('pagesId').uiComboBox();
        $form.$editDialog.$pagesSl = $form.$editDialog.getRef('pagesSl').uiTextView();
        $form.$editDialog.$pagesEl = $form.$editDialog.getRef('pagesEl').uiTextView();
        $form.$editDialog.$url = $form.$editDialog.getRef('url').uiTextBox();
        $form.$editDialog.$openURLOnNewTabId = $form.$editDialog.getRef('openURLOnNewTabId').uiCheckBox();
        $form.$editDialog.$openURLOnNewTabId.render([{ value: 1, text: 'Open URL On New Tab' }]);

        $form.$editDialog.$button = $form.$editDialog.find('.actions');

        $form.$editDialog.$button.$save = $form.$editDialog.$button.getRef('save').uiButton();
        $form.$editDialog.$button.$cancel = $form.$editDialog.$button.getRef('cancel').uiButton();

        // end dialog

    };

    this.driveEvents = function () {
        // tab

        $tab.$form.on('click', this.editData.bind(this));

        // preview

        $preview.$button.$edit.on('click', function () {
            $tab.$form.trigger('click');
        });


        // form

        $form.$linkBehaviorId.on('change', this.showHideFormContainers.bind(this));
        $form.$linkBehaviorId.trigger('change');

        $form.$add.on('click', this.addToCart.bind(this));
        $form.$delete.on('click', this.deleteFromCart.bind(this));
        $form.$edit.on('click', this.editCart.bind(this));
        $form.$links.on('dblclick', this.editCart.bind(this));
        $form.$editDialog.$button.$save.on('click', this.saveCart.bind(this));
        $form.$editDialog.$button.$cancel.on('click', this.clearDataLinks.bind(this));

        $form.$button.$save.on('click', this.saveData.bind(this));

        $form.$button.$cancel.on('click', function () {
            $tab.$preview.trigger('click');
        });

        $form.$languageCode.on('click', this.showHideLangContainers.bind(this));
        $form.$languageCodeLink.on('click', this.showHideLangContainersLink.bind(this));
        $form.$pagesId.on('change', this.showPages.bind(this));
        $form.$editDialog.$pagesId.on('change', this.showEditPages.bind(this));

    };

    this.driveFieldSequence = function () {

    };

    this.reload = function () {
        $tab.$preview.trigger('click');



    };

    this.showHideFormContainers = function () {
        $form.$pageContainer.hide();
        $form.$urlContainer.hide();

        switch (parseInt($form.$linkBehaviorId.getValue())) {
            case 1:
                $form.$pageContainer.show();
                if ($form.$languageCodeLink.getValue() == localStorage.primaryLang) {
                    $form.$elPageContainer.hide();
                    $form.$slPageContainer.hide();
                } else if ($form.$languageCodeLink.getValue() == localStorage.secondaryLang) {

                    $form.$slPageContainer.show();
                    $form.$elPageContainer.hide();
                } else if ($form.$languageCodeLink.getValue() == localStorage.extendedLang) {
                    $form.$elPageContainer.show();
                    $form.$slPageContainer.hide();
                }
                break;
            case 2:
                $form.$elPageContainer.hide();
                $form.$slPageContainer.hide();
                $form.$urlContainer.show();
        }
    };

    this.showPages = function () {
        $loading.show();

        var url = 'OrderedPages/detailData';
        var data = {
            id: $form.$pagesId.getValue()
        };

        api.post(url, data, function (result) {

            if (result.status == 'success') {
                $form.$pagesSl.setValue(result.data.title_sl);
                $form.$pagesEl.setValue(result.data.title_el);
            }

            $loading.hide();
        });
    };

    this.showEditPages = function () {
        $loading.show();

        $form.$editDialog.$pagesSl.clear();
        $form.$editDialog.$pagesEl.clear();

        var url = 'OrderedPages/detailData';
        var data = {
            id: $form.$editDialog.$pagesId.getValue()
        };

        api.post(url, data, function (result) {

            if (result.status == 'success') {
                $form.$editDialog.$pagesSl.setValue(result.data.title_sl);
                $form.$editDialog.$pagesEl.setValue(result.data.title_el);
            }

            $loading.hide();
        });
    };

    this.showHideLangContainers = function () {

        if ($form.$languageCode.getValue() == localStorage.primaryLang) {
            $form.$slContainer.hide();
            $form.$elContainer.hide();
        } else if ($form.$languageCode.getValue() == localStorage.secondaryLang) {
            $form.$slContainer.show();
            $form.$elContainer.hide();
        } else if ($form.$languageCode.getValue() == localStorage.extendedLang) {
            $form.$slContainer.hide();
            $form.$elContainer.show();
        }
    };

    this.showHideLangContainersLink = function () {

        if ($form.$languageCodeLink.getValue() == localStorage.primaryLang) {
            $form.$slContainerLink.hide();
            $form.$elContainerLink.hide();
            $form.$elPageContainer.hide();
            $form.$slPageContainer.hide();
        } else if ($form.$languageCodeLink.getValue() == localStorage.secondaryLang) {
            $form.$slContainerLink.show();
            $form.$elContainerLink.hide();

            if (parseInt($form.$linkBehaviorId.getValue()) == 1) {
                $form.$slPageContainer.show();
                $form.$elPageContainer.hide();
            }
        } else if ($form.$languageCodeLink.getValue() == localStorage.extendedLang) {
            $form.$slContainerLink.hide();
            $form.$elContainerLink.show();
            if (parseInt($form.$linkBehaviorId.getValue()) == 1) {
                $form.$elPageContainer.show();
                $form.$slPageContainer.hide();
            }
        }
    };

    this.detailData = function (callback) {
        $loading.show();
        $form.$linkBehaviorId.renderApi('LinkBehavior/readData', { text: 'behavior' });
        $form.$pagesId.renderApi('OrderedPages/readData', { text: 'title' });
        $form.$languageCode.renderApi('Language/readData', { text: 'language', flag: 'flag' });
        $form.$languageCodeLink.renderApi('Language/readData', { text: 'language', flag: 'flag' });

        $form.$editDialog.$linkBehaviorId.renderApi('LinkBehavior/readData', { text: 'behavior' });
        $form.$editDialog.$pagesId.renderApi('OrderedPages/readData', { text: 'title' });
        $form.$editDialog.$languageCodeLink.renderApi('Language/readData', { text: 'language', flag: 'flag' });
        var url = 'Language/detailData';

        api.post(url, function (result) {

            if (result.status == 'success') {
                $languageS = result.data.secondaryEnabledId;
                $languageE = result.data.extendedEnabledId;

                if ($languageS == 1)
                    $preview.$slContainer.show();
                else
                    $preview.$slContainer.hide();

                if ($languageE == 1)
                    $preview.$elContainer.show();
                else
                    $preview.$elContainer.hide();
                $preview.$labelSl.html(result.data.secondaryLanguage);
                $preview.$labelEl.html(result.data.extendedLanguage);
            }
        });

        var url = 'FootContent/detailData';

        api.post(url, function (result) {

            if (result.status == 'success') {

                $preview.$description.setValue(result.data.description);
                $preview.$descriptionSl.setValue(result.data.description_sl);
                $preview.$descriptionEl.setValue(result.data.description_el);

                rsItems = result.data.links;
                this.renderViewCart();

                if ($.isFunction(callback))
                    callback();
            }

            $loading.hide();
        }.bind(this));
    };

    this.serializeData = function () {
        var data = {
            description: $form.$description.getValue(),
            descriptionSl: $form.$descriptionSl.getValue(),
            descriptionEl: $form.$descriptionEl.getValue(),
            links: rsItems,
        };

        return data;
    };

    this.clearData = function () {

        var url = 'Language/detailData';

        api.post(url, function (result) {

            if (result.status == 'success') {
                $form.$labelSl.html(result.data.secondaryLanguage);
                $form.$labelEl.html(result.data.extendedLanguage);

            }
        });

        $form.clear();

        $form.$languageCode.setValue(localStorage.primaryLang);
        this.showHideLangContainers();

        $form.$description.clear();
        $form.$descriptionSl.clear();
        $form.$descriptionEl.clear();
        $form.$linkBehaviorId.clear();

        this.clearDataLinks();

        rsItems = new Array();
        this.renderEditCart();

    };

    this.clearDataLinks = function () {

        $form.$languageCodeLink.setValue(localStorage.primaryLang);
        this.showHideLangContainersLink();

        $form.$linkCaption.clear();
        $form.$linkCaptionSl.clear();
        $form.$linkCaptionEl.clear();
        $form.$linkBehaviorId.clear();
        $form.$pagesId.clear();
        $form.$pagesSl.clear();
        $form.$pagesEl.clear();
        $form.$url.clear();
        $form.$openURLOnNewTabId.clear();
        $form.$editDialog.modal('hide');

    };

    this.editData = function () {
        $loading.show();

        this.clearData();

        var url = 'FootContent/detailData';

        api.post(url, function (result) {

            if (result.status == 'success') {
                $form.$description.setValue(result.data.description);
                $form.$descriptionSl.setValue(result.data.description_sl);
                $form.$descriptionEl.setValue(result.data.description_el);

                rsItems = new Array();
                result.data.links.forEach(function (data1) {

                    rsItems.push({
                        id: data1.id,
                        linkCaption: data1.linkCaption,
                        linkCaption_sl: data1.linkCaption_sl,
                        linkCaption_el: data1.linkCaption_el,
                        linkBehaviorId: data1.linkBehaviorId,
                        behavior: data1.behavior,
                        pagesId: data1.pagesId,
                        page: data1.page,
                        url: data1.url,
                        openUrlOnNewTabId: data1.openUrlOnNewTabId,
                        destination: data1.destination,
                    });

                });

                this.renderEditCart();

                $form.$description.focus();
            }

            $loading.hide();
        }.bind(this));
    };

    this.saveData = function () {
        $form.$button.$save.loading();

        var url = 'FootContent/saveData';
        var data = this.serializeData();

        api.post(url, data, function (result) {

            switch (result.status) {
                case 'success':
                    $splash.show(result.message);

                    this.detailData(function () {
                        $tab.$preview.trigger('click');

                        $form.$button.$save.release();
                        window.scrollTo(0, 0);
                    }.bind(this));

                    break;
                case 'invalid':
                    $form.error(result);
                    $form.$button.$save.release();
            }

        }.bind(this));
    };

    this.renderViewCart = function () {
        this.renderTable($preview.$links, rsItems, cartFields, 100);
    };

    this.renderEditCart = function () {
        this.renderTable($form.$links, rsItems, cartFields, 100);
    };

    this.addToCart = function () {

        if (!$form.$linkCaption.getValue()) {
            $msgbox.alert('LINK_CAPTION_REQUIRED');
            return;
        }

        if (!$form.$linkBehaviorId.getValue()) {
            $msgbox.alert('LINK_BEHAVIOR_REQUIRED');
            return;
        }

        //        if(!$form.$url.getValue()) {
        //        	$msgbox.alert('URL_REQUIRED');
        //            return;
        //        }

        var found = false;
        rsItems.forEach(function (data) {

            if (data.linkCaption == $form.$linkCaption.getValue())
                found = true;
        });

        if (found) {
            $msgbox.alert('LINK_CAPTION_IS_REG');
            return;
        }

        if ($form.$openURLOnNewTabId.isChecked())
            var openURLOnNewTabId = 1;
        else
            var openURLOnNewTabId = 2;

        if ($form.$linkBehaviorId.getValue() == 1)
            var destination = $form.$pagesId.getText();
        else
            var destination = $form.$url.getValue();

        rsItems.push({
            id: this.guid(),
            linkCaption: $form.$linkCaption.getValue(),
            linkCaption_sl: $form.$linkCaptionSl.getValue(),
            linkCaption_el: $form.$linkCaptionEl.getValue(),
            linkBehaviorId: $form.$linkBehaviorId.getValue(),
            behavior: $form.$linkBehaviorId.getText(),
            destination: destination,
            pagesId: $form.$pagesId.getValue(),
            url: $form.$url.getValue(),
            openUrlOnNewTabId: openURLOnNewTabId,
        });

        this.clearDataLinks();
        $form.$linkCaption.focus();

        this.renderEditCart();
    };

    this.editCart = function () {

        $form.$editDialog.$languageCodeLink.setValue(localStorage.primaryLang);
        $form.$editDialog.$pagesSl.clear();
        $form.$editDialog.$pagesEl.clear();

        var id = $form.$links.getValue();

        if (!id)
            return;

        var rowLinks = [];

        $.each(rsItems, function (i, row) {
            if (row.id == id)
                rowLinks = row;
        });

        if (rowLinks.openUrlOnNewTabId == 1)
            $form.$editDialog.$openURLOnNewTabId.setValue([1]);
        else
            $form.$editDialog.$openURLOnNewTabId.setValue();

        $form.$editDialog.$linkCaption.setValue(rowLinks.linkCaption);
        $form.$editDialog.$linkCaptionSl.setValue(rowLinks.linkCaption_sl);
        $form.$editDialog.$linkCaptionEl.setValue(rowLinks.linkCaption_el);
        $form.$editDialog.$linkBehaviorId.setValue(rowLinks.linkBehaviorId);

        var lastId = rowLinks.pagesId;

        $form.$editDialog.$pagesId.renderApi('OrderedPages/readData', {
            text: 'title', done: function () {
                $form.$editDialog.$pagesId.setValue(lastId);
            }
        });

        var url = 'OrderedPages/detailData';
        var data = {
            id: rowLinks.pagesId
        };

        api.post(url, data, function (result) {

            if (result.status == 'success') {
                $form.$editDialog.$pagesSl.setValue(result.data.title_sl);
                $form.$editDialog.$pagesEl.setValue(result.data.title_el);
            }
        });


        $form.$editDialog.$url.setValue(rowLinks.url);

        $form.$editDialog.modal('show');
    };

    this.saveCart = function () {

        if ($form.$editDialog.$openURLOnNewTabId.isChecked())
            var openURLOnNewTabId = 1;
        else
            var openURLOnNewTabId = 2;

        if ($form.$editDialog.$linkBehaviorId.getValue() == 1)
            var destination = $form.$editDialog.$pagesId.getText();
        else
            var destination = $form.$editDialog.$url.getValue();

        var id = $form.$links.getValue();

        $.each(rsItems, function (i, row) {
            if (row.id == id) {

                rsItems[i].linkCaption = $form.$editDialog.$linkCaption.getValue();
                rsItems[i].linkCaption_sl = $form.$editDialog.$linkCaptionSl.getValue();
                rsItems[i].linkCaption_el = $form.$editDialog.$linkCaptionEl.getValue();
                rsItems[i].linkBehaviorId = $form.$editDialog.$linkBehaviorId.getValue();
                rsItems[i].behavior = $form.$editDialog.$linkBehaviorId.getText();
                rsItems[i].pagesId = $form.$editDialog.$pagesId.getValue();
                rsItems[i].page = $form.$editDialog.$pagesId.getText();
                rsItems[i].url = $form.$editDialog.$url.getValue();
                rsItems[i].openUrlOnNewTabId = openURLOnNewTabId;
                rsItems[i].destination = destination;

            }
        });

        this.renderEditCart();
        this.clearDataLinks();
    };

    this.deleteFromCart = function () {
        var checkedId = $form.$links.getCheckedValues();
        if (checkedId.length <= 0)
            return;

        $msgbox.confirm('Delete selected data', function () {

            checkedId.forEach(function (indexId) {
                var i = -1;

                rsItems.forEach(function (data) {
                    i++;

                    if (data.id == indexId)
                        rsItems.splice(i, 1);

                });
            });

            this.renderEditCart();
        }.bind(this));

    };

};

dashboard.application.controllers.FootContent.prototype =
    Object.create(dashboard.application.core.Controller.prototype);

dashboard.application.controllers.KegiatanDiklat = function () {
    var $el = $page.getScope('KegiatanDiklat');
    dashboard.application.core.Controller.call(this, $el);

    var $tab;
    var $preview;
    var $form;

    this.init = function () {

        // tab

        $tab = $el.find('#tab');
        $tab.$preview = $tab.getRef('preview');
        $tab.$form = $tab.getRef('form');

        // preview

        $preview = $el.find('#preview');

        $preview.$judul = $preview.getRef('judul').uiTextView();
        $preview.$pengantar = $preview.getRef('pengantar').uiHTMLView();

        $preview.$button = $preview.find('#button');
        $preview.$button.$edit = $preview.$button.getRef('edit').uiButton();
        $preview.$button.$cancel = $preview.$button.getRef('cancel').uiButton();

        // form

        $form = $el.find('#form').uiForm();

        $form.$judul = $form.getRef('judul').uiTextBox();
        $form.$pengantar = $form.getRef('pengantar').uiRichText();

        $form.$button = $form.find('#button');
        $form.$button.$save = $form.$button.getRef('save').uiButton();
        $form.$button.$cancel = $form.$button.getRef('cancel').uiButton();

    };

    this.driveEvents = function () {
        // tab

        $tab.$form.on('click', this.editData.bind(this));

        // preview

        $preview.$button.$edit.on('click', function () {
            $tab.$form.trigger('click');
        });


        // form

        $form.$button.$save.on('click', this.saveData.bind(this));

        $form.$button.$cancel.on('click', function () {
            $tab.$preview.trigger('click');
        });

    };

    this.driveFieldSequence = function () {

    };

    this.reload = function () {
        $tab.$preview.trigger('click');
    };

    this.detailData = function (callback) {
        $loading.show();

        var url = 'KegiatanDiklat/detailData';

        api.post(url, function (result) {

            if (result.status == 'success') {

                $preview.$judul.setValue(result.data.judul);
                $preview.$pengantar.setValue(result.data.pengantar);

                if ($.isFunction(callback))
                    callback();
            }

            $loading.hide();
        }.bind(this));
    };

    this.serializeData = function () {
        var data = {
            judul: $form.$judul.getValue(),
            pengantar: $form.$pengantar.getValue(),
        };

        return data;
    };

    this.clearData = function () {

        $form.clear();

        $form.$judul.clear();
        $form.$pengantar.clear();
    };

    this.editData = function () {
        $loading.show();

        this.clearData();

        var url = 'KegiatanDiklat/detailData';

        api.post(url, function (result) {

            if (result.status == 'success') {
                $form.$judul.setValue(result.data.judul);
                $form.$pengantar.setValue(result.data.pengantar);

                $form.$judul.focus();
            }

            $loading.hide();
        }.bind(this));
    };

    this.saveData = function () {
        $form.$button.$save.loading();

        var url = 'KegiatanDiklat/saveData';
        var data = this.serializeData();

        api.post(url, data, function (result) {

            switch (result.status) {
                case 'success':
                    $splash.show(result.message);

                    this.detailData(function () {
                        $tab.$preview.trigger('click');

                        $form.$button.$save.release();
                        window.scrollTo(0, 0);
                    }.bind(this));

                    break;
                case 'invalid':
                    $form.error(result);
                    $form.$button.$save.release();
            }

        }.bind(this));
    };

};

dashboard.application.controllers.KegiatanDiklat.prototype =
    Object.create(dashboard.application.core.Controller.prototype);

dashboard.application.config.Routes = function () {

    this.getConfig = function () {
        return {

            '#/settings/branches': 'BranchManagement',
            '#/settings/users': 'Users',
            '#/settings/users/users_privileges': 'UsersPrivileges',
            '#/settings/users/users_aktivasi': 'UsersAktivasi',
            '#/settings/users/users_log': 'UsersLog',

            '#/contents/kegiatan_diklat': 'KegiatanDiklat',
            '#/contents/widyaiswara': 'Widyaiswara',
            '#/contents/bahan_ajar': 'BahanAjar',
            '#/contents/sttpl_alumni': 'SttplAlumni',
            '#/contents/foot_content': 'FootContent',

            '#/pengajar_diklat': 'PengajarDiklat',
            '#/diklat': 'Diklat',
            '#/rekap_ajar_wi': 'RekapAjarWi',
            '#/arsip_materi': 'ArsipMateri',
            '#/diklat_pkti': 'DiklatPkti',
            '#/pertanyaan': 'Pertanyaan',
            '#/kelulusan': 'Kelulusan',
            '#/soal_akd': 'SoalAkd',
            '#/jawaban_akd': 'JawabanAkd',
            '#/master_data/lokasi/tempat': 'Tempat',
            '#/master_data/label_mapel': 'LabelMapel',
            '#/master_data/mata_pelajaran': 'MataPelajaran',
            '#/master_data/mata_pelajaran_fungsional': 'MataPelajaranFungsional',
            '#/master_data/berkas_pendaftaran': 'BerkasPendaftaran',
            '#/master_data/footer_excel': 'FooterExcel',
            '#/laporan_rekapitulasi': 'LaporanRekapitulasi',

            // Articles
            '#/posts/blog/categories': 'BlogCategories',
            '#/posts/news/categories': 'NewsCategories',
            '#/posts/news/headline': 'NewsHeadline',

            '#/jadwal_diklat/pengajar': 'JadwalPengajar',

            // Gallery
            '#/product/categories': 'ProductCategories',
            '#/gallery/categories': 'GalleryCategories',

            '#/guest_book': 'GuestBook',

        };
    };

};
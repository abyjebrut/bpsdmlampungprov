dashboard.application.config.Panel = function () {

    this.getConfig = function () {
        return {

            // Settings: {
            //   access: ACCESS_USER,
            //   caption: 'SETTINGS',
            //   url: '#/settings',
            //   icon: 'configure',
            // },

            // Regional: {
            //     access: ACCESS_USER,
            //     caption: 'REGIONAL',
            //     url: '#/settings/regional',
            //     icon: 'map',
            // },

            // Branches: {
            //   access: ACCESS_USER,
            //   caption: 'BRANCHES',
            //   url: '#/settings/branches',
            //   icon: 'linkify',
            //   divider: true,
            // },

            // Users: {
            //     access: ACCESS_USER,
            //     caption: 'USERS',
            //     url: '#/settings/users',
            //     icon: 'user',
            // },

            // MyAccount: {
            //     access: ACCESS_USER_ONLY,
            //     caption: 'MY_ACCOUNT',
            //     url: '#/settings/my_account',
            //     icon: 'browser',
            //     divider: true,
            // },


            PreviewWebsite: {
                access: ACCESS_USER,
                target: 'blank',
                caption: 'PREVIEW_WEBSITE',
                url: localStorage.urlSite,
                icon: 'desktop',
                divider: true,
            },

            Logout: {
                access: ACCESS_FREE,
                caption: 'LOGOUT',
                url: 'javascript:$dashboard.logout()',
                icon: 'lock',
            },

        }
    };

};
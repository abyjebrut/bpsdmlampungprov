dashboard.application.config.Config = function () {

  this.app = {
    title: 'BPSDM Lampung Provinsi',
    directory: 'dashboard',
    biLingual: false,
  };

  this.url = {
    autoSearch: true,
    site: 'http://localhost:8081/bpsdmlampungprov',
    api: 'http://localhost:8081/bpsdmlampungprov/api',
    // autoSearch: false,
    //     site: 'https://bpsdmlampung.xyz/',
    //     api: 'https://bpsdmlampung.xyz/api',
  };

  this.lang = {
    primary: {
      // code: 'en',
      // description: 'English',
      // icon: 'united kingdom flag',
      code: 'id',
      description: 'Indonesia',
      icon: 'indonesia flag',
    },
    secondary: {
      code: 'id',
      description: 'Indonesia',
      icon: 'indonesia flag',
    },
  };

  this.paging = {
    range: 6,
  };

  this.ui = {
    native: false,
    loading: true,

    // mapView & mapPicker

    googleApi: false,
    defaultLatitude: '-6.196400',
    defaultLongitude: '106.813132',
    mapZoom: 10,

    // datePicker

    pastYearRange: 100,
    futureYearRange: 20,
  };

  var configNs = dashboard.application.config;

  this.autoLoad = new configNs.AutoLoad().getConfig();

  this.uiComponents = this.autoLoad.uiComponents;
  this.forms = this.autoLoad.forms;

  this.routes = new configNs.Routes().getConfig();
  this.strings = new configNs.Strings().getConfig();

  this.menuInstance = new configNs.Menu();
  this.panelInstance = new configNs.Panel();

  this.load = function (callback) {

    if (this.url.autoSearch) {
      var siteUrl =
        window.location.href.substr(
          0,
          window.location.href.length - (('/' + this.app.directory).length + location.hash.length)
        );
      //            var apiUrl = siteUrl + '?api=';
      var apiUrl = siteUrl + 'api/';

      this.url.site = siteUrl;
      this.url.api = apiUrl;
    }
    else {
      // Code Igniter adjusment
      this.url.api = this.url.api + '/';
    }

    var url = this.url.api + 'System/readConfig';

    $.post(url, function (result) {
      if (result.status == 'success') {

        // language

        // if (!localStorage.lang)
        // localStorage.lang = result.data.lang;
        localStorage.lang = 'id';

        localStorage.primaryLang = result.data.primaryLang;
        localStorage.secondaryLang = result.data.secondaryLang;
        localStorage.extendedLang = result.data.extendedLang;

        if (localStorage.lang != localStorage.primaryLang && localStorage.lang != localStorage.secondaryLang)
          localStorage.lang = localStorage.primaryLang;

        if (!localStorage.urlSite)
          localStorage.urlSite = this.url.site;

        // url

        localStorage.urlApi = this.url.api;
        localStorage.urlTmp = result.data.urlTmp;
        localStorage.urlArchive = result.data.urlArchive;

        localStorage.apiKey = result.data.apiKey;

        // auth

        if (!localStorage.authKey)
          localStorage.authKey = '';

        if (!localStorage.userType)
          localStorage.userType = 'free';

        if (!localStorage.menuAccess)
          localStorage.menuAccess = '';

        if (!localStorage.panelAccess)
          localStorage.panelAccess = '';

        this.menu = this.menuInstance.getConfig();
        this.panel = this.panelInstance.getConfig();

        callback();
      }
    }.bind(this));

  };

};

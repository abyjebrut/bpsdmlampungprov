dashboard.application.config.Strings = function () {

  this.getConfig = function () {
    return {
      en: this.getEnStrings(),
      id: this.getIdStrings(),
    };
  };

  this.getEnStrings = function () {
    return {

      // Char A

      ANGGOTA: 'Account',
      ARSIP_MATERI: 'Archive',
      ADDRESS: 'Address',
      ANNOUNCEMENT: 'Announcement',
      API_ADDRESS: 'API Address',
      APPLY: 'Apply',
      APPLY_SETTINGS: 'Apply Settings?',
      APPLICATION_NAME: 'Application Name',
      ARTICLES: 'Articles',
      AUTHOR: 'Author',

      // Char B

      BLOG: 'Blog',
      BLOG_CATEGORY: 'Blog Category',
      BANNER: 'Banner',
      BRANCH: 'Branch',
      BRANCHES: 'Branches',
      BRANCH_NAME: 'Branch Name',
      BRANCH_MANAGEMENT: 'Branch Management',
      BAHAN_AJAR: 'Bahan Ajar',

      // Char C

      CANCEL: 'Cancel',
      CATEGORY: 'Category',
      CATEGORIES: 'Categories',
      CAPTION: 'Caption',
      CITY: 'City',
      CLOSE: 'Close',
      CONTACT: 'Contact',
      CONTENTS: 'Contents',
      CONTENT_LAYOUT: 'Content Layout',
      CONTACT_PROFILE: 'Contact/Profile',
      CONTENT_MANAGEMENT: 'Content Management',
      COVER_PHOTO: 'Cover Photo',
      COVER_IMAGE: 'Cover Image',
      COMMENTS: 'Comments',
      COMMENT: 'Comment',
      COMMENTS_AT: 'Comments At',
      COMPANY_NAME: 'Company Name',
      CONFRIM_RESET_PASSWORD: 'Confrim reset password',
      CONFRIM_NOTIF_EMAIL: 'Confrim notif email',
      CANNOT_PROCESS_MULTIPLE_SELECTION: 'Cannot process multiple selection',


      // Char D

      DASHBOARD: 'Dashboard',
      DATE_OF_BIRTH: 'Date Of Birth',
      DELETE: 'Delete',
      DELETE_CHECKED_DATA: 'Delete checked data?',
      DETAIL: 'Detail',
      DESKTOP: 'Desktop',
      DESCRIPTION: 'Description',
      DISTRICT: 'District',
      DISTRICT_CITY: 'District/City',
      DROP_SUB_MENU: 'Drop Sub Menu',
      DELETE_FROM_HEADLINE: 'Delete From Headline',
      DESTINATION: 'Destination',

      // Char E

      EMAIL: 'Email',
      EMAIL_REQUIRED: 'Email required',
      EDIT: 'Edit',
      EVENTS: 'Events',
      EVENTS_DATE: 'Events Date',
      EVENTS_TIME: 'Events Time',
      EMAIL_NOTIF: 'Email Notif',


      // Char F

      FAX_NUMBER: 'Fax Number',
      FACE_CONTENT: 'Face Content',
      FACEBOOK: 'Facebook',
      FACEBOOK_ACCOUNT: 'Facebook Account',
      FACEBOOK_PAGE: 'Facebook Page',
      FROM: 'From',
      FIRST_NAME: 'First Name',
      FOOTER: 'Footer',
      FOOT_CONTENT: 'Foot Content',
      FAQ: 'FAQ',
      FILE_TYPE_DENIED: 'File Type denied',

      // Char G

      GALLERY: 'Gallery',
      GENDER: 'Gender',
      GENERAL_SETTINGS: 'General Settings',
      GUEST_BOOK: 'Guest Book',
      GO_TO_URL: 'Go To URL',
      GOOGLE_PLUS: 'Google Plus',

      // Char H

      HEAD_CONTENT: 'Head Content',
      HEADER_FOOTER: 'Header/Footer',
      HEADLINE: 'Headline',

      // Char I

      INSTANSI: 'Instansi',
      INBOX: 'Inbox',
      IMAGE: 'Image',
      IMAGE_FULL_WIDTH: 'Image Full Width',
      IMAGE_ON_LEFT: 'Image On Left',
      IMAGE_ON_RIGHT: 'Image On Right',
      INSTAGRAM: 'Instagram',
      IN_RESPONSE_TO: 'In Response To',

      // Char J

      JENIS_DIKLAT: 'Jenis Diklat',
      JABATAN: 'Jabatan',

      // Char K

      KEYWORDS: 'Keywords',
      KEYWORDS_REQUIRED: 'Keywords required',
      KITCHEN: 'Kitchen',
      KEGIATAN_DIKLAT: 'Kegiatan Diklat',
      KEDUDUKAN: 'Kedudukan',


      // Char L

      LAST_NAME: 'Last Name',
      LATITUDE: 'Latitude',
      LOCATION: 'Location',
      LONGITUDE: 'Longitude',
      LINKS: 'Links',
      LINK_CAPTION: 'Link Caption',
      LINK_CAPTION_REQUIRED: 'Link Caption required',
      LINK_BEHAVIOR: 'Link Behavior',
      LINK_BEHAVIOR_REQUIRED: 'Link Behavior required',
      LOG: 'Log',
      LOGIN: 'Login',
      LOGOUT: 'Logout',
      LOGOUT_APPLICATION: 'Logout from this Application?',

      // Char M

      MAP_LOCATION: 'Map Location',
      MESSAGE: 'Message',
      MENU: 'Menu',
      MENU_BEHAVIOR: 'Menu Behavior',
      MENU_CAPTION: 'Menu Caption',
      MENU_PAGES: 'Menu & Pages',
      MENU_MANAGEMENT: 'Menu Management',
      MEDIA_TYPE: 'Media Type',
      MULTI_PAGES: 'Multi Pages',
      MOBILE_NUMBER: 'Mobile Number',
      MOBILE_NUMBER_REQUIRED: 'Mobile Number required',
      MORE_DESCRIPTION: 'More Description',
      MAIN_BRANCH: 'Main Branch',
      MASTER_DATA: 'Master Data',
      MENU_ACCESS: 'Menu Access',
      MY_ACCOUNT: 'My Account',

      // Char N

      NAME: 'Name',
      NEWS: 'News',
      NEWS_CATEGORY: 'News Category',
      NEWS_EVENTS: 'News & Events',
      NETWORK: 'Network',
      NEW_PASSWORD: 'New Password',
      NO: 'No',
      NO_DATA_CHECKED: 'No data checked',
      NO_MEDIA: 'No Media',

      // Char O

      OLD_PASSWORD: 'Old Password',
      OPEN_PAGE: 'Open Page',
      OPEN_URL_ON_NEW_TAB: 'Open URL On New Tab',
      OTHER_NUMBER: 'Other Number',
      OTHER_PAGES: 'Other Pages',

      // Char P

      PANGKAT_GOLONGAN: 'Pangkat/Golongan',
      PENDIDIKAN_TERAKHIR: 'Pendidikan Terakhir',
      PAGE: 'Page',
      PAGES: 'Pages',
      PASSWORD: 'Password',
      PAYMENT: 'Payment',
      PANEL_ACCESS: 'Panel Access',
      PUBLISH_STATUS: 'Publish Status',
      PHOTO: 'Photo',
      PHOTO_CATEGORY: 'Photo Category',
      PHONE_NUMBER: 'Phone Number',
      POSTED_ON: 'Posted On',
      PREVIEW_WEBSITE: 'Preview Website',
      PRIVILEGES: 'Privileges',
      PROFILE: 'Profile',
      PROFILE_CONTACT: 'Profile/Contact',
      PRODUCT: 'Product',
      PRODUCT_NAME: 'Product Name',
      PRODUCT_CATEGORY: 'Product Category',
      PRICE: 'Price',
      POSTS: 'Posts',
      PENDAFTARAN_DIKLAT: 'Pendaftaran Diklat',

      // Char R

      READ: 'Read',
      READ_MORE_URL: 'Read More URL',
      REGIONAL: 'Regional',
      REPORT: 'Report',
      RESET: 'Reset',
      RESET_SETTINGS: 'Reset Settings?',
      REGULAR_PAGES: 'Regular Pages',
      RETYPE_PASSWORD: 'Retype Password',
      RUNNING_TEXT: 'Running Text',
      RESET_EMAIL: 'Reset Email',
      RESET_PASSWORD: 'Reset Password',


      // Char S

      SEND_EMAIL: 'Reset Password',
      SAVE: 'Save',
      STTPL_ALUMNI: 'STTPL/Alumni',
      SALES_TRANSACTION: 'Sales Transaction',
      SALES_REPORT: 'Sales Report',
      SEO: 'SEO',
      SET_AS_HEADLINE: 'Set As Headline',
      SET_AS_HEADLINE_CHECKED_DATA: 'Set checked data as Headline?',
      SETTINGS: 'Settings',
      SUB_PAGES: 'Sub Pages',
      SUB_TITLE: 'Sub Title',
      SUBJECT: 'Subject',
      SUBSCRIBER: 'Subscriber',
      SUB_MENU_CAPTION: 'Sub Menu Caption',
      SUB_MENU_CAPTION_REQUIRED: 'Sub Menu Caption required',
      SUB_MENU_CAPTION_IS_REG: 'Sub Menu Caption already registered',
      SOCIAL_MEDIA: 'Social Media',
      SOCIAL_NETWORK: 'Social Network',
      SHOW_PRICE: 'Show Price',
      STILL_INSIDE: 'Still Inside',
      SHOW_READ_MORE_LINK: 'Show Read More Link',
      SOAL: 'SOAL',

      // Char T

      TITLE: 'Title',
      TRANSACTION: 'Transaction',
      TWITTER: 'Twitter',
      TWITTER_ACCOUNT: 'Twitter Account',
      TWITTER_PAGE: 'Twitter Page',
      TINGKAT_KESANGGUPAN: 'TINGKAT_KESANGGUPAN',

      // Char U

      URL: 'URL',
      URL_REQUIRED: 'URL required',
      USER: 'User',
      USERS: 'Users',
      USER_LOG: 'User Log',
      USER_MANAGEMENT: 'User Management',
      USER_PRIVILEGES: 'User Privileges',
      USER_NAME: 'Username',

      // Char V

      VIDEO: 'Video',
      VIDEO_EMBED: 'Video (Embed)',

      // Char W

      WIDYAISWARA: 'Widyaiswara',
      WEBSITE_ADDRESS: 'Website Address',

      // Char Y

      YES: 'Yes',

    };
  };

  this.getIdStrings = function () {
    return {
      // Char A

      ACTIVE_ON: 'Active On',
      AGAMA: 'Agama',
      ANGGOTA: 'Anggota',
      ARSIP_MATERI: 'Arsip/Materi',
      ADDRESS: 'Alamat',
      ANNOUNCEMENT: 'Pengumuman',
      API_ADDRESS: 'Alamat API',
      APPLY: 'Terap..',
      APPLY_SETTINGS: 'Terapkan Pengaturan?',
      APPLICATION_NAME: 'Nama Aplikasi',
      ARTICLES: 'Artikel',
      AUTHOR: 'Penulis',

      // Char B

      BLOG: 'Blog',
      BLOG_CATEGORY: 'Kategori Blog',
      BANNER: 'Banner',
      BRANCH: 'Cabang',
      BRANCHES: 'Cabang',
      BRANCH_NAME: 'Nama Cabang',
      BRANCH_MANAGEMENT: 'Manajemen Cabang',
      BAHAN_AJAR: 'Bahan Ajar',

      // Char C

      CANCEL: 'Batal',
      CATEGORY: 'Kategori',
      CATEGORIES: 'Categories',
      CAPTION: 'Judul',
      CITY: 'Kota',
      CLOSE: 'Tutup',
      CONTACT: 'Kontak',
      CONTENTS: 'Konten',
      CONTENT_LAYOUT: 'Layout Konten',
      CONTACT_PROFILE: 'Kontak/Profil',
      CONTENT_MANAGEMENT: 'Manajemen Konten',
      COVER_PHOTO: 'Foto Cover',
      COVER_IMAGE: 'Gambar Cover',
      COMMENTS: 'Komentar',
      COMMENT: 'Komentar',
      COMMENTS_AT: 'Komentar Pada',
      COMPANY_NAME: 'Nama Perusahaan',
      CONFRIM_RESET_PASSWORD: 'Reset password anggota yang terseleksi',
      CONFRIM_NOTIF_EMAIL: 'Pemberitahuan email ke anggota yang terseleksi',
      CANNOT_PROCESS_MULTIPLE_SELECTION: 'Tidak dapat memproses seleksi lebih dari satu',


      // Char D

      DASHBOARD: 'Dashboard',
      DATE_OF_BIRTH: 'Tanggal Lahir',
      DELETE: 'Hapus',
      DELETE_CHECKED_DATA: 'Hapus data yg ditandai?',
      DETAIL: 'Lihat',
      DESKTOP: 'Halaman Depan',
      DESCRIPTION: 'Deskripsi',
      DESKRIPSI: 'Deskripsi',
      DISTRICT: 'Kabupaten',
      DIKLAT: 'Diklat',
      DISTRICT_CITY: 'Kabupaten/Kota',
      DROP_SUB_MENU: 'Menampilkan Sub Menu',
      DELETE_FROM_HEADLINE: 'Hapus Dari Headline',
      DESTINATION: 'Tujuan',

      // Char E

      EMAIL: 'Email',
      EMAIL_REQUIRED: 'Email belum diisi',
      EDIT: 'Ubah',
      EVENTS: 'Acara',
      EVENTS_DATE: 'Tanggal Acara',
      EVENTS_TIME: 'Jam Acara',
      EMAIL_NOTIF: 'Email Pemberitahuan',

      // Char F

      FAX_NUMBER: 'No. Fax',
      FACE_CONTENT: 'Konten Muka',
      FACEBOOK: 'Facebook',
      FACEBOOK_ACCOUNT: 'Akun Facebook',
      FACEBOOK_PAGE: 'Halaman Facebook',
      FROM: 'Dari',
      FIRST_NAME: 'Nama Depan',
      FILE_WORD: 'File Word',
      FILE_EXCEL: 'File Excel',
      FILE_POWER_POINT: 'File Power Point',
      FILE_PDF: 'File Pdf',
      FOTO: 'Foto',
      FOOTER: 'Kop Kaki',
      FOOT_CONTENT: 'Konten Kaki',
      FAQ: 'FAQ',
      FILE_TYPE_DENIED: 'Tipe File tidak diizinkan',

      // Char G

      GALLERY: 'Galeri',
      GENDER: 'Jenis Kelamin',
      GENERAL_SETTINGS: 'Pengaturan Umum',
      GUEST_BOOK: 'Buku Tamu',
      GO_TO_URL: 'Menuju ke URL',
      GOOGLE_PLUS: 'Google Plus',

      // Char H

      HEAD_CONTENT: 'Konten Kepala',
      HEADER_FOOTER: 'Kop Kepala/Kaki',
      HEADLINE: 'Headline',

      // Char I

      INSTANSI: 'Instansi',
      INSTANSI_LAINNYA: 'Instansi Lainnya',
      INBOX: 'Kotak Pesan',
      GUESBOOK: 'Kotak Pesan',
      IMAGE: 'Gambar',
      IMAGE_FULL_WIDTH: 'Gambar Dengan Lebar Penuh',
      IMAGE_ON_LEFT: 'Gambar di Kiri',
      IMAGE_ON_RIGHT: 'Gambar di Kanan',
      INSTAGRAM: 'Instagram',
      IN_RESPONSE_TO: 'Respon Dari',

      // Char J

      JABATAN: 'Jabatan',
      JABATAN_ESELON: 'Jabatan Eselon',
      JAWABAN: 'Jawaban',
      JENIS_DIKLAT: 'Jenis Diklat',
      JUDUL: 'Judul',

      // Char K

      KEYWORDS: 'Kata Kunci',
      KELULUSAN: 'Kelulusan',
      KEYWORDS_REQUIRED: 'Kata Kunci belum diisi',
      KITCHEN: 'Dapur',
      KEGIATAN_DIKLAT: 'Kegiatan Diklat',
      KUOTA: 'Kuota',
      KODE: 'Kode',
      KEDUDUKAN: 'Kedudukan',

      // Char L

      LAST_NAME: 'Nama Belakang',
      LATITUDE: 'Latitude',
      LOCATION: 'Lokasi',
      LONGITUDE: 'Longitude',
      LINKS: 'Tautan',
      LINK_CAPTION: 'Label Tautan',
      LINK_CAPTION_REQUIRED: 'Label Tautan belum diisi',
      LINK_BEHAVIOR: 'Behavior Tautan',
      LINK_BEHAVIOR_REQUIRED: 'Prilaku Tautan belum diisi',
      LOG: 'Jejak',
      LOGIN: 'Masuk',
      LOGOUT: 'Keluar',
      LOGOUT_APPLICATION: 'Keluar dari Aplikasi ini?',

      // Char M

      MATA_DIKLAT: 'Mata Diklat',
      MAP_LOCATION: 'Peta Lokasi',
      MESSAGE: 'Pesan',
      MENU: 'Menu',
      MENU_BEHAVIOR: 'Kegunaan Menu',
      MENU_CAPTION: 'Label Menu',
      MENU_PAGES: 'Menu & Halaman',
      MENU_MANAGEMENT: 'Manajemen Menu',
      MEDIA_TYPE: 'Tipe Media',
      MULTI_PAGES: 'Multi Halaman',
      MOBILE_NUMBER: 'No. HP',
      MOBILE_NUMBER_REQUIRED: 'No. HP belum diisi',
      MORE_DESCRIPTION: 'Deskripsi Lengkap',
      MAIN_BRANCH: 'Cabang Utama',
      MASTER_DATA: 'Data Master',
      MENU_ACCESS: 'Akses Menu',
      MY_ACCOUNT: 'Akun Saya',

      // Char N

      NAME: 'Nama',
      NAMA: 'Nama',
      NIP_NAMA: 'Nip/Nama',
      NAMA_DIKLAT: 'Nama Diklat',
      NEWS: 'Berita',
      NEWS_CATEGORY: 'Kategori Berita',
      NEWS_EVENTS: 'Berita & Acara',
      NETWORK: 'Jaringan',
      NEW_PASSWORD: 'Kata Sandi Baru',
      NO: 'Tidak',
      NO_DATA_CHECKED: 'Tidak ada data yg ditandai',
      NO_MEDIA: 'Tidak ada Media',
      NO_REGISTRASI: 'No Registrasi',

      // Char O

      OLD_PASSWORD: 'Kata Sandi Lama',
      OPEN_PAGE: 'Buka Halaman',
      OPEN_URL_ON_NEW_TAB: 'Buka URL di Tab Baru',
      OTHER_NUMBER: 'Nomor Lain',
      OTHER_PAGES: 'Page Lain',

      // Char P

      PANGKAT_GOLONGAN: 'Pangkat/Golongan',
      PENDIDIKAN_TERAKHIR: 'Pendidikan Terakhir',
      PAGE: 'Halaman',
      PAGES: 'Halaman',
      PASSWORD: 'Kata Sandi',
      PAYMENT: 'Pembayaran',
      PANEL_ACCESS: 'Akses Panel',
      PELAKSANAAN_SALAH: 'Pelaksanaan Mulai Melebihi Pelaksanaan Selesai',
      PELAKSANAAN_MULAI: 'Pelaksanaan Mulai',
      PELAKSANAAN_MULAI_REQUIRED: 'Pelaksanaan Mulai belum diisi',
      PELAKSANAAN_SELESAI: 'Pelaksanaan Selesai',
      PELAKSANAAN_SELESAI_REQUIRED: 'Pelaksanaan Selesai belum diisi',
      PENGANTAR: 'Pengantar',
      PERTANYAAN: 'Pertanyaan',
      PENGAJAR_DIKLAT: 'Pengajar Diklat',
      PENGAJAR: 'Pengajar',
      PENGAJAR_REQUIRED: 'Pengajar belum diisi',
      PENGAJAR_IS_REG: 'Pengajar sudah terdaftar sebelumnya',
      PUBLISH_STATUS: 'Status Publikasi',
      PHOTO: 'Foto',
      PHOTO_CATEGORY: 'Kategori Foto',
      PHONE_NUMBER: 'No. Telp',
      POSTED_ON: 'Dikirim Pada',
      PREVIEW_WEBSITE: 'Lihat Situs',
      PRIVILEGES: 'Hak Akses',
      PROFILE: 'Profil',
      PROFILE_CONTACT: 'Profil/Kontak',
      PRODUCT: 'Produk',
      PRODUCT_NAME: 'Nama Produk',
      PRODUCT_CATEGORY: 'Kategori Produk',
      PRICE: 'Harga',
      POSTS: 'Artikel',
      PENDAFTARAN_DIKLAT: 'Pendaftaran Diklat',

      // Char R

      READ: 'Baca',
      REGISTRASI_ON: 'Pendaftaran Pada',
      REGIONAL: 'Wilayah',
      REPORT: 'Laporan',
      RESET: 'Semula',
      RESET_SETTINGS: 'Kembalikan ke Pengaturan semula?',
      REGULAR_PAGES: 'Halaman Reguler',
      RETYPE_PASSWORD: 'Ulangi Kata Sandi',
      RUNNING_TEXT: 'Teks Berjalan',
      RUMUSAN_KOMPETENSI: 'Rumusan Kompetensi',
      RUMUSAN_KOMPETENSI_REQUIRED: 'Rumusan Kompetensi belum diisi',
      RUMUSAN_KOMPETENSI_IS_REG: 'Rumusan Kompetensi sudah terdaftar sebelumnya',
      RESET_PASSWORD: 'Reset Password',

      // Char S

      SEND_EMAIL: 'Reset Kata Sandi',
      STTPL_ALUMNI: 'STTPL/Alumni',
      STATUS_DIKLAT: 'Status Diklat',
      STATUS_AKUN: 'Status Akun',
      SAVE: 'Simpan',
      SALES_TRANSACTION: 'Transaksi Penjualan',
      SALES_REPORT: 'Laporan Penjualan',
      SEO: 'SEO',
      SET_AS_HEADLINE: 'Jadikan Headline',
      SET_AS_HEADLINE_CHECKED_DATA: 'Jadikan data yg ditandai sebagai Headline?',
      SETTINGS: 'Pengaturan',
      SUB_PAGES: 'Sub Halaman',
      SUB_TITLE: 'Sub Judul',
      SUBJECT: 'Subyek',
      SUBSCRIBER: 'Langganan',
      SUB_MENU_CAPTION: 'Label Sub Menu',
      SUB_MENU_CAPTION_REQUIRED: 'Label Sub Menu belum diisi',
      SUB_MENU_CAPTION_IS_REG: 'Label Sub Menu sudah terdaftar sebelumnya',
      SOCIAL_MEDIA: 'Media Sosial',
      SOCIAL_NETWORK: 'Jejaring Sosial',
      SHOW_PRICE: 'Tampilkan Harga',
      STILL_INSIDE: 'Masih Di Dalam',
      SHOW_READ_MORE_LINK: 'Tampilkan Tautan Baca Selengkapnya',
      SOAL: 'Soal',

      // Char T

      TAHUN: 'Tahun',
      TEMPAT_LAHIR: 'Tempat Lahir',
      TELP_HP: 'Telp HP',
      TITLE: 'Judul',
      TRANSACTION: 'Transaksi',
      TWITTER: 'Twitter',
      TWITTER_ACCOUNT: 'Akun Twitter',
      TWITTER_PAGE: 'Halaman Twitter',
      TINGKAT_KESANGGUPAN: 'Tingkat Kesanggupan',

      // Char U

      UNIT_KERJA: 'Unit Kerja',
      URL: 'URL',
      URL_REQUIRED: 'URL belum diisi',
      USER: 'Pengguna',
      USERS: 'Pengguna',
      USER_LOG: 'Jejak Pengguna',
      USER_MANAGEMENT: 'Manajemen Pengguna',
      USER_PRIVILEGES: 'Hak Akses Pengguna',
      USER_NAME: 'ID Pengguna',

      // Char V

      VIDEO: 'Video',
      VIDEO_EMBED: 'Video (Embed)',

      // Char W

      WEBSITE_ADDRESS: 'Alamat Website',
      WIDYAISWARA: 'Widyaiswara',


      // Char Y

      YES: 'Ya',
    };
  };
};

dashboard.application.config.Menu = function () {

    this.getConfig = function () {
        return {

            Desktop: {
                caption: 'DASHBOARD',
                url: '#/desktop',
                icon: 'home',
            },
            GuestBook: {
                caption: 'GUESBOOK',
                icon: 'mail',
                url: '#/guest_book',

            },
            Pengguna: {
                caption: 'USERS',
                icon: 'users',

                subStructure: {

                    Users: {
                        caption: 'Users',
                        url: '#/settings/users',
                    },
                    UsersAktivasi: {
                        caption: 'Tidak Aktif',
                        url: '#/settings/users/users_aktivasi',
                    },
                    UsersPrivileges: {
                        caption: 'PRIVILEGES',
                        url: '#/settings/users/users_privileges',
                    },
                    UsersLog: {
                        caption: 'LOG',
                        url: '#/settings/users/users_log',
                    },
                    Database: {
                        caption: 'Database',
                        url: '#/settings/database',
                    },
                },
            },

            MyAccount: {
                caption: 'MY_ACCOUNT',
                url: '#/settings/my_account',
                icon: 'browser',
            },

            Contents: {
                caption: 'CONTENTS',
                icon: 'write square',

                subStructure: {

                    KegiatanDiklat: {
                        caption: 'KEGIATAN_DIKLAT',
                        url: '#/contents/kegiatan_diklat',
                    },

                    Widyaiswara: {
                        caption: 'WIDYAISWARA',
                        url: '#/contents/widyaiswara',
                    },

                    BahanAjar: {
                        caption: 'BAHAN_AJAR',
                        url: '#/contents/bahan_ajar',
                    },

                    SttplAlumni: {
                        caption: 'STTPL_ALUMNI',
                        url: '#/contents/sttpl_alumni',
                    },



                    Pages: {
                        caption: 'PAGES',
                        url: '#/contents/pages',
                    },

                    Menu: {
                        caption: 'MENU',
                        url: '#/contents/menu',
                    },

                    OtherBanner: {
                        caption: 'Other Banner',
                        url: '#/contents/other_banner',
                    },

                    FootContent: {
                        caption: 'Foot Content',
                        url: '#/contents/foot_content',
                    },

                },
            },

            MasterData: {
                caption: 'MASTER_DATA',
                icon: 'cube',

                subStructure: {

                    Instansi: {
                        caption: 'INSTANSI',
                        url: '#/master_data/instansi',
                    },

                    UnitKerja: {
                        caption: 'UNIT_KERJA',
                        url: '#/master_data/unit_kerja',
                    },

                    PangkatGolongan: {
                        caption: 'PANGKAT_GOLONGAN',
                        url: '#/master_data/pangkat_golongan',
                    },

                    Jabatan: {
                        caption: 'Jabatan',
                        url: '#/master_data/jabatan',
                    },

                    PendidikanTerakhir: {
                        caption: 'PENDIDIKAN_TERAKHIR',
                        url: '#/master_data/pendidikan_terakhir',
                    },

                    JenisPengajar: {
                        caption: 'Jenis Pengajar',
                        url: '#/master_data/jenis_pengajar',
                    },

                    JenisDiklat: {
                        caption: 'JENIS_DIKLAT',
                        url: '#/master_data/jenis_diklat',
                    },

                    BerkasPendaftaran: {
                        caption: 'Berkas Pendaftaran',
                        url: '#/master_data/berkas_pendaftaran',
                    },

                    Lokasi: {
                        caption: 'Lokasi',
                        url: '#/master_data/lokasi',
                    },

                    Angkatan: {
                        caption: 'Angkatan',
                        url: '#/master_data/angkatan',
                    },

                    TanggalMerah: {
                        caption: 'Tanggal Merah',
                        url: '#/master_data/tanggal_merah',
                    },

                    FooterExcel: {
                        caption: 'Footer Excel',
                        url: '#/master_data/footer_excel',
                    },

                    LabelMapel: {
                        caption: 'Judul Mata Pelajaran',
                        url: '#/master_data/label_mapel',
                    },

                    MataPelajaran: {
                        caption: 'Mata Pelajaran',
                        url: '#/master_data/mata_pelajaran',
                    },

                    MataPelajaranFungsional: {
                        caption: 'Mata Pelajaran Fungsional',
                        url: '#/master_data/mata_pelajaran_fungsional',
                    },

                }
            },

            RekapAjar: {
                caption: 'Rekap Ajar',
                url: '#/rekap_ajar',
                icon: 'info',
            },

            RekapAjarWi: {
                caption: 'Rekap Ajar WI',
                url: '#/rekap_ajar_wi',
                icon: 'info',
            },

            Anggota: {
                caption: 'ANGGOTA',
                url: '#/anggota',
                icon: 'student',
            },

            ApproveAnggota: {
                caption: 'Approve Anggota',
                url: '#/approve_anggota',
                icon: 'student',
            },

            PengajarDiklat: {
                caption: 'Pengajar Diklat',
                url: '#/pengajar_diklat',
                icon: 'users',
            },

            Diklat: {
                caption: 'DIKLAT',
                url: '#/diklat',
                icon: 'calendar',
            },

            DiklatPktuf: {
                caption: 'Diklat PKTUF',
                url: '#/diklat_pktuf',
                icon: 'calendar',
            },

            DiklatPkti: {
                caption: 'Diklat PKTI',
                url: '#/diklat_pkti',
                icon: 'calendar',
            },

            ArsipMateri: {
                caption: 'ARSIP_MATERI',
                url: '#/arsip_materi',
                icon: 'file text outline',
            },

            Kelulusan: {
                caption: 'KELULUSAN',
                url: '#/kelulusan',
                icon: 'child',
            },


            Faqs: {
                caption: 'FAQ',
                icon: 'comments',

                subStructure: {

                    Faq: {
                        caption: 'FAQ',
                        url: '#/faq',
                    },
                    Pertanyaan: {
                        caption: 'Pertanyaan',
                        url: '#/pertanyaan',
                    },
                },
            },

            PendaftaranDiklat: {
                caption: 'PENDAFTARAN_DIKLAT',
                url: '#/pendaftaran_diklat',
                icon: 'sign in',
            },

            ApprovePendaftaranDiklat: {
                caption: 'Approve Pendaftaran Diklat',
                url: '#/approve_pendaftaran_diklat',
                icon: 'sign in',
            },

            RejectPendaftaranDiklat: {
                caption: 'Reject Pendaftaran Diklat',
                url: '#/reject_pendaftaran_diklat',
                icon: 'sign in',
            },


            Akd: {
                caption: 'AKD',
                icon: 'student',

                subStructure: {

                    AkdAnggota: {
                        caption: 'AKD',
                        url: '#/akd_anggota',
                    },
                    SoalAkd: {
                        caption: 'Soal',
                        url: '#/soal_akd',
                    },
                    JawabanAkd: {
                        caption: 'Jawaban',
                        url: '#/jawaban_akd',
                    },
                },
            },

            ContactProfile: {
                caption: 'CONTACT_PROFILE',
                url: '#/contact_profile',
                icon: 'phone',
            },

            SocialNetwork: {
                caption: 'SOCIAL_NETWORK',
                url: '#/social_network',
                icon: 'browser',
            },

            Seo: {
                caption: 'SEO',
                url: '#/seo',
                icon: 'line chart',
            },

            Laporan: {
                caption: 'Laporan',
                icon: 'info',

                subStructure: {
                    LaporanRekapitulasi: {
                        caption: 'Rekapitulasi',
                        url: '#/laporan_rekapitulasi',
                    },

                }
            },

        }
    };

};
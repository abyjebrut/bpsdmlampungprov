dashboard.application.config.AutoLoad = function () {

  this.getConfig = function () {
    return {
      uiComponents: this.getUiComponents(),
      forms: this.getForms(),
    };
  };

  // Set false to make some UI not loaded at first time
  this.getUiComponents = function () {
    return {
      uiLangBox: true,
      uiForm: true,

      uiTextBox: true,
      uiMoneyBox: true,
      uiNumericBox: true,
      uiTextArea: true,
      uiRichText: true,

      uiComboBox: true,
      uiRadioBox: true,
      uiCheckBox: true,

      uiButton: true,
      uiDatePicker: true,

      uiTable: true,
      uiExpandTable: true,
      uiPaging: true,

      uiUploadImage: true,
      uiMultipleUploadImage: true,
      uiUploadFile: true,
      uiUploadFilePdf: true,

      uiTextView: true,
      uiTextAreaView: true,
      uiTreeView: true,

      uiImageView: true,
      uiMultipleImageView: true,

      uiHTMLView: true,

      uiTableView: true,
      uiTableViewClick: true,
      uiCheckTree: true,

      uiMapView: true,
      uiMapPicker: true,
    };
  };

  // Add Controller Class Name on forms, will auto load controllers and views with same name
  this.getForms = function () {
    var rsPage = []
    localStorage.dataPage = ''
    var data = [{ "nama": "Desktop", "status": "" }, { "nama": "GuestBook", "status": "readData" }, { "nama": "KegiatanDiklat", "status": "detailData" }, { "nama": "Widyaiswara", "status": "detailData" }
      , { "nama": "BahanAjar", "status": "detailData" }, { "nama": "Database", "status": "detailData" }, { "nama": "MyAccount", "status": "detailData" }, { "nama": "SttplAlumni", "status": "detailData" }, { "nama": "Pages", "status": "loadData" }
      , { "nama": "Menu", "status": "loadData" }, { "nama": "OtherBanner", "status": "loadData" }, { "nama": "FootContent", "status": "detailData" }, { "nama": "Instansi", "status": "readData" }
      , { "nama": "UnitKerja", "status": "readData" }, { "nama": "PangkatGolongan", "status": "readData" }, { "nama": "Jabatan", "status": "readData" }, { "nama": "PendidikanTerakhir", "status": "readData" }
      , { "nama": "JenisPengajar", "status": "readData" }, { "nama": "JenisDiklat", "status": "readData" }, { "nama": "BerkasPendaftaran", "status": "readData" }, { "nama": "Lokasi", "status": "readData" }
      , { "nama": "Angkatan", "status": "readData" }, { "nama": "TanggalMerah", "status": "loadData" }, { "nama": "FooterExcel", "status": "readData" }, { "nama": "LabelMapel", "status": "readData" }, { "nama": "MataPelajaran", "status": "loadData" }
      , { "nama": "MataPelajaranFungsional", "status": "loadData" }, { "nama": "RekapAjar", "status": "loadData" }, { "nama": "RekapAjarWi", "status": "loadData" }, { "nama": "Anggota", "status": "loadData" }
      , { "nama": "ApproveAnggota", "status": "loadData" }, { "nama": "PengajarDiklat", "status": "readData" }, { "nama": "Diklat", "status": "loadData" }, { "nama": "DiklatPkti", "status": "loadData" }, { "nama": "DiklatPktuf", "status": "loadData" }, { "nama": "ArsipMateri", "status": "loadData" }
      , { "nama": "Kelulusan", "status": "loadData" }, { "nama": "Faq", "status": "loadData" }, { "nama": "Pertanyaan", "status": "readData" }, { "nama": "PendaftaranDiklat", "status": "loadData" }, { "nama": "ApprovePendaftaranDiklat", "status": "loadData" }, { "nama": "RejectPendaftaranDiklat", "status": "loadData" }
      , { "nama": "AkdAnggota", "status": "readData" }, { "nama": "SoalAkd", "status": "readData" }, { "nama": "JawabanAkd", "status": "readData" }, { "nama": "ContactProfile", "status": "detailData" }, { "nama": "SocialNetwork", "status": "detailData" }
      , { "nama": "Seo", "status": "readData" }, { "nama": "LaporanRekapitulasi", "status": "loadData" }
      , { "nama": "Users", "status": "loadData" }, { "nama": "UsersAktivasi", "status": "readData" }
      , { "nama": "UsersPrivileges", "status": "readData" }, { "nama": "UsersLog", "status": "readData" }];

    data.forEach(row1 => {
      rsPage.push({
        nama: row1.nama,
        status: row1.status,
        nilai: 0,
      });

    });

    localStorage.dataPage = JSON.stringify(rsPage)

    return [
      // 'MyAccount',

      // 'BranchManagement',
      // 'Regional',
      // 'DistrictCity',
      // 'SocialNetwork',
    ];
  };
};

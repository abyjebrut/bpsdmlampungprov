// Namespace

var dashboard = {
  system: {
    core: {},
  },
  application: {
    config: {},
    controllers: {},
    core: {},
  },
};

// Constants

var EDIT_MODE = 1;
var NEW_MODE = 2;

var TYPE_VALUE = 1;
var TYPE_TEXT = 2;
var TYPE_PASSWORD = 3;

var LANG_EN = 'en';
var LANG_ID = 'id';
var LANG_AR = 'ar';

var ACCESS_ADMIN = 1;
var ACCESS_USER = 2;
var ACCESS_USER_ONLY = 3;
var ACCESS_FREE = 4;

// Tools

var config;

var api;
var loader;
var router;

// Widgets

var $overlay;
var $loading;
var $msgbox;
var $splash;
var $splashScreen;

var $headerWrapper;
var $header;
var $logo;
var $infoBar;
var $breadCrumb;

var $menu;
var $panel;

var $page;
var $tabWrapper;
var $buttonWrapper;

var $dashboard;
var $login;

$.getScript('system/core/Loader.js', function () {
    loader = new dashboard.system.core.Loader();
    loader.load();
});

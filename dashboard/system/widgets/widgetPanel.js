$.fn.widgetPanel = function () {
    var keys = [];

    this.init = function () {
        this.render();
    };

    this.getAllKeys = function () {
        return keys;
    };

    this.setVisible = function(key, visible) {
        var $menuItem = this.find('[data-key="' + key + '"]');
        if(visible)
            $menuItem.show();
        else
            $menuItem.hide();
    };

    this.hideAll = function () {
        $.each(keys, function (i, key) {
            this.setVisible(key, false);
        }.bind(this));

    };

    this.showAll = function () {
        $.each(keys, function (i, key) {
            this.setVisible(key, true);
        }.bind(this));
    };

    this.render = function () {

        keys = [];
        var htmlMenu = '';

        $.each(config.panel, function (key, item) {
            keys.push(key);

            if(item.caption) {
                var caption = item.caption;
                var otherCaption = config.strings[localStorage.lang][caption];

                if (otherCaption)
                    caption = otherCaption;
            }
            else
                var caption = '';

            if(item.url)
                var url = item.url;
            else
                var url = '';

            if(item.icon)
                var icon = item.icon;
            else
                var icon = '';

            if(item.divider)
                var className = 'divider';
            else
                var className = '';

            if(item.target)
                var target = item.target;
            else
                var target = '';

            htmlMenu +=
                '<li data-key="' + key + '" class="' + className + '">' +
                '<a target="' + target + '" href="' + url + '"><i class="' + icon + ' icon"></i> ' + caption + '</a>' +
                '</li>';

        }.bind(this));

        this.html('<ul>' + htmlMenu + '</ul>');
    };

    this.init();
    return this;
};

$.fn.widgetPage = function () {
    var namaJs = '';
    var statusJs = '';
    this.open = function (url) {
        this.find('>div').hide();

        var cleanHash = url.substr(2);
        var splitHash = cleanHash.split('/');

        var objectHash = splitHash[splitHash.length - 1];
        var splitObjectHash = objectHash.split('_');
        var camelObjectHash = '';
        var scopeObjectHash = '';
        var masterMenu = '';


        splitObjectHash
            .forEach(function (data) {
                camelObjectHash += data.substr(0, 1).toUpperCase() + data.substr(1);
            });
        masterMenu = camelObjectHash;
        namaJs = masterMenu
        camelObjectHash = camelObjectHash.substr(0, 1).toLowerCase() + camelObjectHash.substr(1);
        scopeObjectHash = camelObjectHash.substr(0, 1).toUpperCase() + camelObjectHash.substr(1);

        $.each(config.routes, function (hash, controllerClassName) {
            if (url == hash) {
                objectHash = '';
                camelObjectHash = controllerClassName.substr(0, 1).toLowerCase() + controllerClassName.substr(1);
                scopeObjectHash = controllerClassName;
            }
        });

        if (camelObjectHash != 'login') {

            var rsPage = JSON.parse(localStorage.dataPage);
            var nilai = '';

            rsPage.forEach(row1 => {
                if (masterMenu == row1.nama) {
                    nilai = row1.nilai;
                    statusJs = row1.status;
                }
            });

            if (dashboard.application.controllers[camelObjectHash]) {
                if (dashboard.application.controllers[camelObjectHash].reload)
                    dashboard.application.controllers[camelObjectHash].reload();
            } else {
                if (nilai == 0) {
                    $.when(
                        this.loadView(masterMenu, '#page')
                    ).done(function () {
                        this.renderStrings();
                        this.initOne();
                        this.loadControllers(masterMenu);

                    }.bind(this));

                    $.each(rsPage, function (i, row) {
                        if (masterMenu == row.nama) {
                            rsPage[i].nilai = 1;
                        }
                    });
                    localStorage.dataPage = JSON.stringify(rsPage)
                }

            }
            this.getScope(scopeObjectHash).show();
        } else {
            $menu.netralize();
            $login.show();
        }

    };

    this.renderStrings = function () {
        var $spans = $('span');

        $.each($spans, function (i, label) {
            var $label = $(label);
            var label = $label.html().trim();
            var string = config.strings[localStorage.lang][label];

            if (string)
                $label.html(string);
        });

        var $labels = $('label');

        $.each($labels, function (i, label) {
            var $label = $(label);
            var label = $label.html().trim();
            var string = config.strings[localStorage.lang][label];

            if (string)
                $label.html(string);
        });

        var $buttons = $('button');

        $.each($buttons, function (i, button) {
            var $button = $(button);
            var label = $button.html().trim();
            var string = config.strings[localStorage.lang][label];

            if (string)
                $button.html(string);
        });

    };

    this.initOne = function () {
        router = new dashboard.system.core.Router();
        router.route();
    };

    this.loadControllers = function (className) {
        var objectName = className.substr(0, 1).toLowerCase() + className.substr(1);
        $.getScript('application/controllers/' + className + '.js', function () {
            this.initPage(className, objectName);
        }.bind(this));

    };

    this.initPage = function (className, objectName) {
        dashboard.application.controllers[objectName] =
            new dashboard.application.controllers[className]();

        var methods = [
            'init',
            'driveEvents',
            'driveFieldSequence',
        ];

        $.each(methods, function (i, method) {

            if (dashboard.application.controllers[objectName][method])
                dashboard.application.controllers[objectName][method]();
        });

        var camelObjectHash = router.getHashInfo().camelObjectHash;

        $.each(config.routes, function (hash, controllerClassName) {
            if (location.hash == hash)
                camelObjectHash = controllerClassName.substr(0, 1).toLowerCase() + controllerClassName.substr(1);
        });

        if (objectName == camelObjectHash) {
            if (dashboard.application.controllers[camelObjectHash].reload)
                dashboard.application.controllers[camelObjectHash].reload();
        }
        this.readData();

    };

    this.readData = function () {

        if (namaJs != '') {
            var masterMenu1 = namaJs.substr(0, 1).toLowerCase() + namaJs.substr(1);

            if (statusJs == 'loadData') {
                if (dashboard.application.controllers[masterMenu1].reload)
                    dashboard.application.controllers[masterMenu1].loadData();

            } else if (statusJs == 'readData') {

                if (dashboard.application.controllers[masterMenu1].reload)
                    dashboard.application.controllers[masterMenu1].readData();
            } else if (statusJs == 'detailData') {

                if (dashboard.application.controllers[masterMenu1].reload)
                    dashboard.application.controllers[masterMenu1].detailData();
            }
        }

    };

    this.loadView = function (viewName, placeOnSelector) {
        var viewUrl = 'application/views/' + viewName + '.html';

        return $.ajax({
            url: viewUrl,
            cache: false,
            success: function (result) {
                $(placeOnSelector).append(result);
            },
        });
    };

    return this;
};

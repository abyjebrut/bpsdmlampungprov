$.fn.widgetSplash = function () {

    this.init = function () {
        this.render();
        this.driveEvents();
    };

    this.driveEvents = function () {
        this.find('button').on('click', this.hide.bind(this));
    };

    this.show = function (message) {
        
        var string = config.strings[localStorage.lang][message];
        if (string)
            message = string;
        
        this.find('button').html(message);

        this.transition('fade down', function () {
            setTimeout(this.hide.bind(this), 1000);
        }.bind(this));
    };

    this.hide = function () {
        this.transition('drop');
    };

    this.render = function () {
        this.html(
            '<button type="button" class="ui positive button"></button>'
        );
    };

    this.init();
    return this;
};

$.fn.widgetMsgbox = function () {

    var $headerWrapper;
    var $content;
    var $actions;

    this.init = function () {

        this.render();

        $headerWrapper = this.find('.header');
        $content = this.find('.content');
        $actions = this.find('.actions');
    };

    this.alert = function (message, callback) {
        var $btnOK = $actions.getRef('btnOK');
        $actions.getRef('btnCancel').hide();

        $btnOK.off('click');
        $btnOK.on('click', function () {
            this.modal('hide');
        }.bind(this));

        $headerWrapper.html('Alert');

        var string = config.strings[localStorage.lang][message];
        if (string)
            message = string;

        $content.html(message);

        if (callback) {
            $btnOK.on('click', function () {
                callback();
            }.bind(this));
        }

        this.modal('show');

    };

    this.confirm = function (message, callback) {
        var $btnOK = $actions.getRef('btnOK');
        $actions.getRef('btnCancel').show();

        $btnOK.off('click');
        $btnOK.on('click', function () {
            this.modal('hide');
        }.bind(this));

        $headerWrapper.html('Confirm');

        var string = config.strings[localStorage.lang][message];
        if (string)
            message = string;

        $content.html(message);

        if (callback) {
            $btnOK.on('click', function () {
                callback();
            }.bind(this));
        }

        this.modal('show');

    };

    this.render = function () {
        this.html(
            '<i class="close icon"></i>' +
            '<div class="header"></div>' +
            '<div class="content"></div>' +
            '<div class="actions">' +
                '<button data-ref="btnOK" type="button" class="ui primary button">OK</button> ' +
                '<button data-ref="btnCancel" type="button" class="ui primary button">Cancel</button>' +
            '</div>'
        );

        this.addClass('ui');
        this.addClass('small');
        this.addClass('modal');

        var $btnCancel = this.getRef('btnCancel');
        $btnCancel.on('click', function () {
            this.modal('hide');
        }.bind(this));
    };

    this.init();
    return this;
};

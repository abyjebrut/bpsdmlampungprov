$.fn.widgetLoading = function () {
    var $loading;

    this.init = function () {
        this.render();
        $loading = $(this.selector);
    };

    this.show = function () {
        if(config.ui.loading)
            $loading.show();
    };

    this.hide = function () {
        if(config.ui.loading)
            $loading.fadeOut();
    };

    this.render = function () {
        this.html(
            '<div class="ui active inverted dimmer">' +
                '<div class="ui text loader">Loading...</div>' +
            '</div>'
        );
    };

    this.init();
    return this;
};

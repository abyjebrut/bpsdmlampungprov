$.fn.widgetInfoBar = function () {

    this.init = function () {
        this.readUserActive();

        if (config.app.biLingual)
            this.render();

        this.driveEvents();
    };

    this.driveEvents = function () {
        if (config.app.biLingual)
            this.find('a').on('click', this.changeLang.bind(this));
        else
            $(window).on('hashchange', this.readUserActive.bind(this));
    };

    this.readUserActive = function () {
        api.post('Account/detailData', function (result) {
            var name = result.data.name;

            if (localStorage.userType == 'admin')
                var url = 'javascript:';
            else
                var url = '#/settings/my_account';

            if (!config.app.biLingual)
                this.render({
                    'url': url,
                    'name': name,
                });
        }.bind(this));
    };

    this.changeLang = function (event) {
        var lang = $(event.currentTarget).data('lang');
        localStorage.lang = lang;
        this.render();

        window.location.reload();
    };

    this.render = function (state) {

        if (config.app.biLingual) {

            var primaryCode = config.lang.primary.code;
            var primaryDescription = config.lang.primary.description;
            var primaryIcon = config.lang.primary.icon;

            var secondaryCode = config.lang.secondary.code;
            var secondaryDescription = config.lang.secondary.description;
            var secondaryIcon = config.lang.secondary.icon;

            switch (localStorage.lang) {
                case primaryCode:
                    var primaryActive = 'active';
                    var secondaryActive = '';

                    break;
                case secondaryCode:
                    var primaryActive = '';
                    var secondaryActive = 'active';
            }

            var htmlStructure =
                '<a data-lang="' + primaryCode +
                '" title="' + primaryDescription + '" class="lang item ' + primaryActive + '" href="javascript:">' +
                '&nbsp; <i class="' + primaryIcon + '"></i>' +
                '</a>' +
                '<a data-lang="' + secondaryCode +
                '" title="' + secondaryDescription + '" class="lang item ' + secondaryActive + '" href="javascript:">' +
                '&nbsp; <i class="' + secondaryIcon + '"></i>' +
                '</a>'
                ;
        }
        else {

            var htmlStructure =
                '<a data-ref="link" class="item hidden-xs" href="' + state.url + '">' +
                '<i class="user icon"></i> <span>' + state.name + '</span>' +
                '</a>';
        }

        this.html(htmlStructure);
    };

    this.init();
    return this;
};

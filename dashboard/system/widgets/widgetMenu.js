$.fn.widgetMenu = function () {

    var keys = [];
    var namaJs = '';
    var statusJs = '';

    this.init = function () {
        this.render();
        this.driveEvents();
    };

    this.driveEvents = function () {
        this.find('a').on('click', this.tree.bind(this));
    };

    this.select = function (url) {

        this.find('a').removeClass('active');

        var $item = this.find('a[href="' + url + '"]');
        $item.addClass('active');

        var $parentLv1 = $item.parent().parent();
        var $parentLv2 = $parentLv1.parent().parent();

        if ($parentLv1.parent().prop('id') != 'menu') {
            $parentLv1.show();

            var $parentLv1Item = $parentLv1.siblings('a');
            var $parentLv1Caret = $parentLv1Item.find('.caret');

            $parentLv1Item.addClass('expand');
            $parentLv1Caret.css('transform', 'rotate(90deg)');

            if ($parentLv2.prop('tagName') == 'UL') {
                $parentLv2.show();

                var $parentLv2Item = $parentLv2.siblings('a');
                var $parentLv2Caret = $parentLv2Item.find('.caret');

                $parentLv2Item.addClass('expand');
                $parentLv2Caret.css('transform', 'rotate(90deg)');
            }
        }

        if ($overlay.css('display') != 'none')
            $dashboard.toggleMenu();
    };

    this.tree = function (event) {

        var $item = $(event.target);
        if ($item.prop('tagName') != 'A')
            $item = $item.parent();

        if (!$item.hasClass('expand') && $item.parent().parent().parent().prop('id') == 'menu')
            this.netralize();

        var $caret = $item.find('.caret');
        var $childTree = $item.siblings('ul');

        if ($childTree.html()) {
            if ($childTree.css('display') == 'none')
                this.rotateCaret($caret, true);
            else
                this.rotateCaret($caret, false);

            $item.toggleClass('expand');
            $childTree.slideToggle();
        }

        var m = $item.context.href;
        if (m != "javascript:") {
            var masterMenu = $item.context.parentNode.dataset.key
            var rsPage = JSON.parse(localStorage.dataPage);
            var nilai = '';
            namaJs = masterMenu
            rsPage.forEach(row1 => {
                if (masterMenu == row1.nama) {
                    nilai = row1.nilai;
                    statusJs = row1.status;
                }
            });
            if (nilai == 0) {
                $.when(
                    this.loadView(masterMenu, '#page')
                ).done(function () {

                    this.renderStrings();
                    this.initOne();
                    this.loadControllers(masterMenu);
                }.bind(this));
                $.each(rsPage, function (i, row) {
                    if (masterMenu == row.nama) {
                        rsPage[i].nilai = 1;
                    }
                });
                localStorage.dataPage = JSON.stringify(rsPage)

            } else {
                this.readData();
            }


        }

    };
    this.renderStrings = function () {
        var $spans = $('span');

        $.each($spans, function (i, label) {
            var $label = $(label);
            var label = $label.html().trim();
            var string = config.strings[localStorage.lang][label];

            if (string)
                $label.html(string);
        });

        var $labels = $('label');

        $.each($labels, function (i, label) {
            var $label = $(label);
            var label = $label.html().trim();
            var string = config.strings[localStorage.lang][label];

            if (string)
                $label.html(string);
        });

        var $buttons = $('button');

        $.each($buttons, function (i, button) {
            var $button = $(button);
            var label = $button.html().trim();
            var string = config.strings[localStorage.lang][label];

            if (string)
                $button.html(string);
        });

    };

    this.initOne = function () {
        router = new dashboard.system.core.Router();
        router.route();
    };

    this.loadControllers = function (className) {
        var objectName = className.substr(0, 1).toLowerCase() + className.substr(1);
        $.getScript('application/controllers/' + className + '.js', function () {
            this.initPage(className, objectName);
        }.bind(this));

    };

    this.initPage = function (className, objectName) {
        dashboard.application.controllers[objectName] =
            new dashboard.application.controllers[className]();

        var methods = [
            'init',
            'driveEvents',
            'driveFieldSequence',
        ];

        $.each(methods, function (i, method) {

            if (dashboard.application.controllers[objectName][method])
                dashboard.application.controllers[objectName][method]();
        });

        var camelObjectHash = router.getHashInfo().camelObjectHash;

        $.each(config.routes, function (hash, controllerClassName) {
            if (location.hash == hash)
                camelObjectHash = controllerClassName.substr(0, 1).toLowerCase() + controllerClassName.substr(1);
        });

        if (objectName == camelObjectHash) {
            if (dashboard.application.controllers[camelObjectHash].reload)
                dashboard.application.controllers[camelObjectHash].reload();
        }

        this.readData();
    };

    this.loadView = function (viewName, placeOnSelector) {
        var viewUrl = 'application/views/' + viewName + '.html';

        return $.ajax({
            url: viewUrl,
            cache: false,
            success: function (result) {
                $(placeOnSelector).append(result);
            },
        });
    };

    this.netralize = function () {

        var $childTree = this.find('>ul>li>ul');

        var $item = $childTree.siblings('a.expand');
        var $caret = $childTree.siblings('a.expand').find('.caret');

        $item.removeClass('expand');
        this.rotateCaret($caret, false);

        $childTree.slideUp();
    };

    this.rotateCaret = function ($caret, rotateDown) {
        if (rotateDown) {
            var degStart = 0;
            var degFinish = 90;
        } else {
            var degStart = 90;
            var degFinish = 0;
        }

        $({
            deg: degStart
        }).animate({
            deg: degFinish
        }, {
            duration: 500,
            step: function (now) {
                $caret.css('transform', 'rotate(' + now + 'deg)');
            }
        });
    };

    this.getAllKeys = function () {
        return keys;
    };

    this.setVisible = function (key, visible) {
        var $menuItem = this.find('[data-key="' + key + '"]');
        if (visible) {
            $menuItem.show();

            var $menuStep1 = $menuItem.parent().parent();
            var isMenu = $menuStep1.data('key');

            if (isMenu)
                $menuStep1.show();

            var $menuStep2 = $menuItem.parent().parent().parent().parent();
            var isMenu = $menuStep2.data('key');

            if (isMenu)
                $menuStep2.show();

        }
        else
            $menuItem.hide();
    };

    this.hideAll = function () {
        $.each(keys, function (i, key) {
            this.setVisible(key, false);
        }.bind(this));
    };

    this.showAll = function () {
        $.each(keys, function (i, key) {
            this.setVisible(key, true);
        }.bind(this));
    };

    this.renderMenuItem = function (item) {

        if (item.caption) {
            var caption = item.caption;
            var otherCaption = config.strings[localStorage.lang][caption];

            if (otherCaption)
                caption = otherCaption;
        }
        else {
            var caption = 'No Caption Defined';
        }

        if (item.url)
            var url = item.url;
        else
            var url = 'javascript:';

        if (localStorage.lang == localStorage.secondaryLang && item.secondaryUrl)
            url = item.secondaryUrl;

        if (item.icon) {
            var htmlIcon = '<i class="' + item.icon + ' icon"></i>';
            caption = htmlIcon + ' ' + caption;
        }

        if (item.subStructure) {
            var caret = '<i class="caret right icon"></i>';
            caption += ' ' + caret;
        }

        var htmlMenu =
            '<a href="' + url + '" class="active">' + caption + '</a>';

        return htmlMenu;
    };

    this.render = function () {

        keys = [];
        var htmlMenu = '';

        $.each(config.menu, function (keyLv1, item) {
            keys.push(keyLv1);
            if (!item.subStructure)
                htmlMenu += '<li data-key="' + keyLv1 + '">' + this.renderMenuItem(item) + '</li>';
            else {

                htmlMenu += '<li data-key="' + keyLv1 + '">' + this.renderMenuItem(item) + '<ul>';

                $.each(item.subStructure, function (keyLv2, item2) {
                    keys.push(keyLv2);

                    //                    menu level 2 begin
                    if (!item2.subStructure)
                        htmlMenu += '<li data-key="' + keyLv2 + '">' + this.renderMenuItem(item2) + '</li>';
                    else {

                        htmlMenu += '<li data-key="' + keyLv2 + '">' + this.renderMenuItem(item2) + '<ul>';

                        $.each(item2.subStructure, function (keyLv3, item3) {
                            keys.push(keyLv3);
                            htmlMenu += '<li data-key="' + keyLv3 + '">' + this.renderMenuItem(item3) + '</li>';

                        }.bind(this));

                        htmlMenu += '</ul></li>';
                    }

                    //                    menu level 2 end

                }.bind(this));

                htmlMenu += '</ul></li>';
            }

        }.bind(this));

        this.html('<ul>' + htmlMenu + '</ul>');
    };

    this.readData = function () {
        if (namaJs != '') {
            var masterMenu1 = namaJs.substr(0, 1).toLowerCase() + namaJs.substr(1);

            if (statusJs == 'loadData') {
                if (dashboard.application.controllers[masterMenu1].reload)
                    dashboard.application.controllers[masterMenu1].loadData();

            } else if (statusJs == 'readData') {

                if (dashboard.application.controllers[masterMenu1].reload)
                    dashboard.application.controllers[masterMenu1].readData();
            } else if (statusJs == 'detailData') {

                if (dashboard.application.controllers[masterMenu1].reload)
                    dashboard.application.controllers[masterMenu1].detailData();
            }
        }

    };


    this.init();
    return this;
};

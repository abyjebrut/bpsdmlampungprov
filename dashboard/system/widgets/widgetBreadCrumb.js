$.fn.widgetBreadCrumb = function () {

    this.render = function (url) {

        url = url.substr(2);
        var splitUrl = url.split('/');

        var htmlClue = '';

        splitUrl.forEach(function (clue) {

            var splitClue = clue.split('_');
            var capitalClue = '';

//            var hashCode = clue;

            splitClue.forEach(function (hashCode) {
                var splitHashCode = hashCode.split('=>');
                var word = splitHashCode[0];

                if(splitHashCode.length <= 1)
                    capitalClue += word.substr(0, 1).toUpperCase() + word.substr(1) + ' ';
                else {
                    var url = splitHashCode[1].replace(/\|/g, '/');
                    var linkLabel = word;

                    url = url.replace(/-/g, '_');
                    linkLabel = linkLabel.replace('-', ' ');

                    capitalClue +=
                        '<a href="' + url + '">' +
                        linkLabel + '</a> ';
                }
            });

            capitalClue = capitalClue.trim();

            htmlClue +=
                '<i class="right chevron icon divider"></i><div class="section">' + capitalClue + '</div>';
        });

        this.html(
            '<div class="ui breadcrumb small">' +
                '<a class="section" href="#/desktop"><i class="home icon"></i></a>' +
                htmlClue +
            '</div>'
        );

    };

    return this;
};

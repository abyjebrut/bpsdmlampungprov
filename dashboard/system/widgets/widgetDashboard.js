$.fn.widgetDashboard = function () {

    var menuVisible;

    this.init = function () {
        this.driveEvents();
    };

    this.driveEvents = function () {

        $(window).on('resize', this.adjustLayout.bind(this));

        $('[data-open="menu"]').on('click', this.toggleMenu.bind(this));
        $overlay.on('click', this.toggleMenu.bind(this));

        $('[data-open="panel"]').on('click', this.dropDownPanel.bind(this));
        $(window).on('click', this.dropUpPanel.bind(this));
    };

    this.adjustLayout = function () {
        $overlay.hide();
        $('body').css('overflow-y', 'auto');

        $headerWrapper.css('left', '0px');
        $tabWrapper.css('left', '0px');
        $page.css('left', '0px');
        $buttonWrapper.css('left', '0px');

        if ($(window).width() <= 800) {
            $menu.css('left', '-' + $menu.outerWidth() + 'px');
            $headerWrapper.css('padding-left', '0px');
            $tabWrapper.css('padding-left', '0px');
            $page.css('padding-left', '0px');
            $buttonWrapper.css('padding-left', '0px');

        } else {
            $menu.css('left', '0px');
            $headerWrapper.css('padding-left', $menu.outerWidth() + 'px');
            $tabWrapper.css('padding-left', $menu.outerWidth() + 'px');
            $page.css('padding-left', $menu.outerWidth() + 'px');
            $buttonWrapper.css('padding-left', $menu.outerWidth() + 'px');
        }
    };

    this.toggleMenu = function () {
        if ($menu.css('left') == '0px')
            menuVisible = true;
        else
            menuVisible = false;

        if ($(window).width() <= 800)
            $overlay.toggle();

        if ($overlay.css('display') == 'none')
            $('body').css('overflow-y', 'auto');
        else
            $('body').css('overflow-y', 'hidden');

        if (!menuVisible) {
            $menu.animate({
                left: '0px'
            });

            var pushedCSS = {};

            if ($(window).width() > 800)
                pushedCSS['padding-left'] = $menu.outerWidth() + 'px';
            else
                pushedCSS['left'] = $menu.outerWidth() + 'px';

            $headerWrapper.animate(pushedCSS);
            $page.animate(pushedCSS);
            $tabWrapper.animate(pushedCSS);
            $buttonWrapper.animate(pushedCSS);
        } else {
            $menu.animate({
                left: '-' + $menu.outerWidth() + 'px'
            });

            var pushedCSS = {};

            if ($(window).width() > 800)
                pushedCSS['padding-left'] = '0px';
            else
                pushedCSS['left'] = '0px';

            $headerWrapper.animate(pushedCSS);
            $page.animate(pushedCSS);
            $tabWrapper.animate(pushedCSS);
            $buttonWrapper.animate(pushedCSS);
        }

    };

    this.dropDownPanel = function (event) {

        if ($panel.css('display') == 'none') {
            event.stopPropagation();
            $panel.slideDown();
        }
    };

    this.dropUpPanel = function (event) {
        $panel.slideUp();
    };

    this.logout = function () {
        $msgbox.confirm('LOGOUT_APPLICATION', function () {

            api.post('Account/logout', function (result) {

                switch (result.status) {
                case 'success':
                    localStorage.userType = '';
                    localStorage.authKey = '';

                    router.redirect('#/login');
                }

            }.bind(this));
        }.bind(this));
    };

    this.init();
    return this;
};

dashboard.system.core.Controller = function ($el) {

  this.init = function () {
    this.driveEvents();
  };

  this.driveEvents = function () {
    $el.find('#tab .item').on('click', this.driveTab.bind(this));
  };

  this.setTitle = function (title) {
    document.title = config.app.title + ' | ' + title;
  };

  this.driveTab = function (event) {

    var $selectedTab = $(event.currentTarget);
    var href = $selectedTab.data('href');

    if (href)
      router.redirect(href);
    else {
      window.scrollTo(0, 0);

      $el.find('#tab .item').removeClass('active');
      $selectedTab.addClass('active');

      var section = $selectedTab.data('section');
      var $pageActive = $selectedTab.closest('.page');

      $pageActive.find('.section').hide();
      $pageActive.find('#' + section).show();
    }
  };

  this.createUniqNumber = function () {
    return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
  };

  this.createUniqId = function () {
    var uniqId =
    'id-' +
    this.createUniqNumber() + this.createUniqNumber() + '-' +
    this.createUniqNumber() + '-' +
    this.createUniqNumber() + '-' +
    this.createUniqNumber() + '-' +
    this.createUniqNumber() + this.createUniqNumber() + this.createUniqNumber();

    return uniqId;
  };

  this.guid = function () {
    return this.createUniqId();
  };

  this.init();
};

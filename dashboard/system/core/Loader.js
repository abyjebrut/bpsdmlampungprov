dashboard.system.core.Loader = function () {

  this.enhanceJQuery = function () {

    $.fn.field = function (name) {
      return this.find('[name="' + name + '"]');
    };

    $.fn.getScope = function (scope) {
      return this.find('[data-scope="' + scope + '"]');
    };

    $.fn.getRef = function (ref) {
      return this.find('[data-ref="' + ref + '"]');
    };

  };

  this.load = function () {

    this.enhanceJQuery();
    var basePath = 'application/config/';

    $.when(
      $.getScript(basePath + 'AutoLoad.js'),
      $.getScript(basePath + 'Menu.js'),
      $.getScript(basePath + 'Panel.js'),
      $.getScript(basePath + 'Routes.js'),
      $.getScript(basePath + 'Strings.js')
    ).done(function () {

      $.getScript("application/config/Config.js", function () {
        config = new dashboard.application.config.Config();
        config.load(function () {

          this.loadModules();

        }.bind(this));
      }.bind(this));
    }.bind(this));

  };

  this.loadModules = function () {

    $.when(
      this.loadView('Login', '#main')
    ).done(function () {
      var viewsDef = [];

      config.forms.forEach(function (view) {
        viewsDef.push(this.loadView(view, '#page'));
      }.bind(this));
      $.when.apply(
        this, viewsDef
      ).done(function () {
        this.renderStrings();

        $.when(
          // Load Core components

          $.getScript("system/core/Api.js"),
          $.getScript("system/core/Router.js")
        ).done(function () {

          $.when(
            // Load Widgets

            $.getScript("system/widgets/widgetLoading.js"),
            $.getScript("system/widgets/widgetMsgbox.js"),
            $.getScript("system/widgets/widgetSplash.js"),
            $.getScript("system/widgets/widgetBreadCrumb.js"),
            $.getScript("system/widgets/widgetMenu.js"),
            $.getScript("system/widgets/widgetPanel.js"),
            $.getScript("system/widgets/widgetInfoBar.js"),
            $.getScript("system/widgets/widgetPage.js"),
            $.getScript("system/widgets/widgetDashboard.js")
          ).done(function () {

            var uiGetScripts = [];

            $.each(config.uiComponents, function (key, condition) {
              if (condition == true)
                uiGetScripts.push(
                  $.getScript('system/ui/' + key + '.js')
                );
            });

            $.when.apply(
              this, uiGetScripts
            ).done(function () {

              $.getScript("system/core/Controller.js", function () {
                $.getScript("application/core/Controller.js", function () {
                  $.getScript("application/controllers/Login.js", function () {

                    this.init();
                    this.loadControllers();
                  }.bind(this));
                }.bind(this));
              }.bind(this));

            }.bind(this));

          }.bind(this));
        }.bind(this));


      }.bind(this));
    }.bind(this));

  };

  this.loadControllers = function () {
    config.forms.forEach(function (data) {
      var className = data;
      var objectName = data.substr(0, 1).toLowerCase() + data.substr(1);

      $.getScript('application/controllers/' + className + '.js', function () {
        this.initPage(className, objectName);
      }.bind(this));

    }.bind(this));
  };

  this.loadView = function (viewName, placeOnSelector) {
    var viewUrl = 'application/views/' + viewName + '.html';

    return $.ajax({
      url: viewUrl,
      cache: false,
      success: function (result) {
        $(placeOnSelector).append(result);
      },
    });
  };

  this.initPage = function (className, objectName) {
    dashboard.application.controllers[objectName] =
      new dashboard.application.controllers[className]();
    var methods = [
      'init',
      'driveEvents',
      'driveFieldSequence',
    ];

    $.each(methods, function (i, method) {

      if (dashboard.application.controllers[objectName][method])
        dashboard.application.controllers[objectName][method]();
    });

    var camelObjectHash =
      router.getHashInfo().camelObjectHash;

    $.each(config.routes, function (hash, controllerClassName) {
      if (location.hash == hash)
        camelObjectHash = controllerClassName.substr(0, 1).toLowerCase() + controllerClassName.substr(1);
    });

    if (objectName == camelObjectHash) {
      if (dashboard.application.controllers[camelObjectHash].reload)
        dashboard.application.controllers[camelObjectHash].reload();
    }
  };

  this.init = function () {

    // Load core components

    api = new dashboard.system.core.Api();
    router = new dashboard.system.core.Router();

    // Put widgets to object variable

    $overlay = $('#overlay');
    $loading = $('#loading').widgetLoading();
    $msgbox = $('#msgbox').widgetMsgbox();
    $splash = $('#splash').widgetSplash();
    $splashScreen = $('#splash-screen');

    $headerWrapper = $('#header-wrapper');
    $header = $('#header');
    $logo = $('#logo');
    $infoBar = $('#info-bar').widgetInfoBar();
    $breadCrumb = $('#bread-crumb').widgetBreadCrumb();

    $menu = $('#menu').widgetMenu();
    $panel = $('#panel').widgetPanel();

    $page = $('#page').widgetPage();
    $tabWrapper = $('.tab-wrapper');
    $buttonWrapper = $('.button-wrapper');

    $dashboard = $('#dashboard').widgetDashboard();
    $login = $('#login');

    // Set Title
    $logo.html(config.app.title);

    // Init page Login
    this.initPage('Login', 'login');

    // Hide Loading Splash Screen after 3 minutes

    setTimeout(function () {
      $splashScreen.transition('fade out');
    }, 3000);
    router.route();
  };

  this.renderStrings = function () {
    var $spans = $('span');

    $.each($spans, function (i, label) {
      var $label = $(label);
      var label = $label.html().trim();
      var string = config.strings[localStorage.lang][label];

      if (string)
        $label.html(string);
    });

    var $labels = $('label');

    $.each($labels, function (i, label) {
      var $label = $(label);
      var label = $label.html().trim();
      var string = config.strings[localStorage.lang][label];

      if (string)
        $label.html(string);
    });

    var $buttons = $('button');

    $.each($buttons, function (i, button) {
      var $button = $(button);
      var label = $button.html().trim();
      var string = config.strings[localStorage.lang][label];

      if (string)
        $button.html(string);
    });

  };

};

dashboard.system.core.Router = function () {

  this.route = function () {
    this.driveEvents();
  };

  this.driveEvents = function () {
    this.driveRouter();
    $(window).on('hashchange', this.driveRouter.bind(this));
  };

  this.driveRouter = function () {
    var locationHash = location.hash;

    if (!localStorage.authKey) {
      this.redirect('#/login');
    }
    else if (!locationHash) {
      this.redirect('#/desktop');
    }

    $breadCrumb.render(locationHash);
    $menu.select(locationHash);
    $page.open(locationHash);

    var hashInfo = this.getHashInfo();
    document.title = config.app.title + ' | ' + hashInfo.titleObjectHash;
  };

  this.getParam = function(index) {
    var params = location.hash.split('/');
    return params[index];
  };

  this.getHashInfo = function () {
    var completeHash = location.hash;
    var cleanHash = location.hash.substr(2);
    var splitHash = cleanHash.split('/');
    var objectHash = splitHash[splitHash.length - 1];

    var splitObjectHash = objectHash.split('_');
    var camelObjectHash = '';
    var scopeObjectHash = '';
    var titleObjectHash = '';

    splitObjectHash.forEach(function (data) {
      camelObjectHash += data.substr(0, 1).toUpperCase() + data.substr(1);
    });

    camelObjectHash = camelObjectHash.substr(0, 1).toLowerCase() + camelObjectHash.substr(1);
    scopeObjectHash = camelObjectHash.substr(0, 1).toUpperCase() + camelObjectHash.substr(1);

    splitObjectHash.forEach(function (data) {
      titleObjectHash += data.substr(0, 1).toUpperCase() + data.substr(1) + ' ';
    });

    titleObjectHash = titleObjectHash.trim();

    var hashInfo = {
      'completeHash': completeHash,
      'cleanHash': cleanHash,
      'splitHash': splitHash,
      'objectHash': objectHash,
      'splitObjectHash': splitObjectHash,
      'camelObjectHash': camelObjectHash,
      'scopeObjectHash': scopeObjectHash,
      'titleObjectHash': titleObjectHash,
    };

    return hashInfo;
  };

  this.redirect = function (url) {
    window.location = url;
  };

  this.open = function (url) {
    window.open(url);
  };

};

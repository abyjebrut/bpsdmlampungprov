dashboard.system.core.Api = function () {

  this.getArchive = function (archive) {
    if (typeof archive == 'undefined')
      archive = '';
    else
      archive = '/' + archive;

    var urlArchive = localStorage.urlArchive + archive;
    return urlArchive;
  };

  this.getTmp = function (archive) {
    if (typeof archive == 'undefined')
      archive = '';
    else
      archive = '/' + archive;

    var urlArchive = localStorage.urlTmp + archive;
    return urlArchive;
  };

  this.post = async function (url, data, callback) {
    url = config.url.api + url;

    if (callback) {
      var request = data;
      request.key = localStorage.apiKey;
      request.auth = localStorage.authKey;

    } else {
      var request = {
        key: localStorage.apiKey,
        auth: localStorage.authKey,
      };

      callback = data;
    }

    request.lang = localStorage.lang;
    await $.post(url, request, function (result) {
      callback(result);

      switch (result.status) {
        case 'denied':
          localStorage.userType = '';
          localStorage.authKey = '';

          config.load(function () {
            router.redirect('#/login');
          });

          break;
        case 'failed':
          console.log('API call failed: ' + result.message);
      }

    });
  };

  this.post = async function (url, data, callback) {
    url = config.url.api + url;

    if (callback) {
      var request = data;
      request.key = localStorage.apiKey;
      request.auth = localStorage.authKey;

    } else {
      var request = {
        key: localStorage.apiKey,
        auth: localStorage.authKey,
      };

      callback = data;
    }

    request.lang = localStorage.lang;
    await $.post(url, request, function (result) {
      callback(result);

      switch (result.status) {
        case 'denied':
          localStorage.userType = '';
          localStorage.authKey = '';

          config.load(function () {
            router.redirect('#/login');
          });

          break;
        case 'failed':
          console.log('API call failed: ' + result.message);
      }

    });
  };
};

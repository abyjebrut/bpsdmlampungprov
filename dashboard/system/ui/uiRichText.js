$.fn.uiRichText = function (props) {

    var id;

    this.init = function () {

        if(!props)
            props = {};

        this.render();
    };

    this.val = function (value) {
        if (typeof value == 'undefined')
            return this.getValue();
        else
            this.setValue(value);
    };

    this.setValue = function (value) {
        if(value == null)
            value = '';

        tinyMCE.get(id).setContent(value);
    };

    this.getValue = function () {
        return tinyMCE.get(id).getContent();
    };

    this.clear = function () {
        this.setValue('');
    };

    this.createUniqNumber = function () {
        return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
    };

    this.createUniqId = function () {
        var uniqId =
            'id-' +
            this.createUniqNumber() + this.createUniqNumber() + '-' +
            this.createUniqNumber() + '-' +
            this.createUniqNumber() + '-' +
            this.createUniqNumber() + '-' +
            this.createUniqNumber() + this.createUniqNumber() + this.createUniqNumber();

        return uniqId;
    };

    this.render = function () {
        id = this.createUniqId();

        var htmlTemplate =
            '<div class="ui form">' +
                '<textarea id="' + id + '" style="width:100%"></textarea>' +
            '</div>';

        this.html(htmlTemplate);

        var settings = {
            selector: this.selector + ' textarea',
            menubar: false,
            statusbar: false,
            toolbar_items_size: 'small',
            height: 200,
            plugins: [
                 "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
                 "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                 "save table contextmenu directionality emoticons template paste textcolor"
           ],
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons | ltr rtl",
        };

        if (localStorage.lang == LANG_AR)
            settings.directionality = 'rtl';

        tinymce.init(settings);
    };

    this.init();
    return this;
};

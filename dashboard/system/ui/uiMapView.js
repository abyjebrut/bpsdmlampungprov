$.fn.uiMapView = function (props) {
    var $map;
    var map;
    var marker;
    var infoWindow;

    this.init = function () {

        if(!props)
            props = {};

        this.render();

        $map = this.find('.map');

        var latitude = 0;
        var longitude = 0;

        if (config.ui.googleApi)
            this.initMap(latitude, longitude);
    };

    this.initMap = function (latitude, longitude) {

        if (config.ui.googleApi) {

            var mapPosition = new google.maps.LatLng(latitude, longitude);

            var mapProp = {
                center: mapPosition,
                zoom: 10,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };

            var mapDOM = $map.get(0);
            map = new google.maps.Map(mapDOM, mapProp);

            marker = new google.maps.Marker({
                position: mapPosition,
            });

            marker.setMap(map);

            infoWindow = new google.maps.InfoWindow({
                content: 'Latitude: ' + latitude + '<br>Longitude: ' + longitude
            });

            infoWindow.open(map, marker);

            google.maps.event.trigger(map, 'resize');
        }
    };

    this.val = function (latitude, longitude) {
        this.initMap(latitude, longitude);
    };

    this.setValue = function (latitude, longitude) {
        this.initMap(latitude, longitude);
    };

    this.getValue = function () {

    };

    this.clear = function () {
        this.setValue('');
    };

    this.render = function () {
        var htmlTemplate =
            '<div class="map" style="width: 100%;height: 380px; background: silver"></div>';

        this.html(htmlTemplate);
    };

    this.init();
    return this;
};

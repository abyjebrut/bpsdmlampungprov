$.fn.uiRadioBox = function (props) {

    var $input;

    this.init = function () {
        if(!props)
            props = {};

        this.render();
    };

    this.val = function (value) {
        if (typeof value == 'undefined')
            return this.getValue();
        else
            this.setValue(value);
    };

    this.setValue = function (value) {
        this.find('.ui.radio').checkbox('uncheck');

        var $radio = this.find('input[value="' + value + '"]').parent();
        $radio.checkbox('check');
    };

    this.getData = function (type) {
        var value = '';

        $.each(this.find('.ui.radio'), function () {
            if ($(this).checkbox('is checked')) {
                switch(type) {
                    case TYPE_VALUE:
                        value = $(this).find('input').val();
                        break;

                    case TYPE_TEXT:
                        value = $(this).find('label').text();
                }
            }
        });

        return value;
    };

    this.getValue = function () {
        return this.getData(TYPE_VALUE);
    };

    this.getText = function () {
        return this.getData(TYPE_TEXT);
    };

    this.clear = function () {
        this.setValue('');
    };

    this.render = function (data) {
        var htmlTemplate =
            '<div class="ui form">' +
                '<div class="inline fields">';

        if (data) {
            $.each(data, function (i, row) {
                var uiRef = '';

                if (this.attr('data-ref'))
                    uiRef = this.attr('data-ref');
                else
                    uiRef = this.attr('name');

                var text = row.text;
                var otherText = config.strings[localStorage.lang][text];

                if (otherText)
                    text = otherText;

                htmlTemplate +=
                    '<div class="field">' +
                        '<div class="ui radio checkbox">' +
                            '<input value="' + row.value + '" name="' + uiRef + '" type="radio">' +
                            '<label>' + text + '</label>' +
                        '</div>' +
                    '</div>';

            }.bind(this));
        }

        htmlTemplate += '</div></div>';

        this.html(htmlTemplate);

        this.find('.ui.radio').checkbox();
        $input = this.find('input');
    };

    this.renderApi = function (sourceUrl, request, params) {
        $loading.show();

        if (!params) {
            params = request;
            request = {};
        }

        api.post(sourceUrl, request, function (result) {
            var items = [];

            if (result.data) {
                result.data.forEach(function (data) {

                    if (!params.value)
                        var valueKey = 'id';
                    else
                        var valueKey = params.value;

                    items.push({value: data[valueKey], text: data[params.text]});
                });
            }

            this.render(items);
            $loading.hide();

            if ($.isFunction(params.done))
                params.done();
        }.bind(this));
    };

    return this;
};

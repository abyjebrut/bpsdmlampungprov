$.fn.uiMoneyBox = function (props) {
    var $input;
    var $error;

    this.init = function () {

        if(!props)
            props = {};

        if (!props.lang)
            props.lang = localStorage.lang;

        this.render();
        this.driveEvents();
    };

    this.driveEvents = function () {
        $input.on('focus', this.light.bind(this));
        $input.on('blur', this.unLight.bind(this));
        $input.on('keydown', this.activateEnterEvent.bind(this));
    };

    this.activateEnterEvent = function (event) {
        if (event.which == 13)
            this.trigger('enter');
    };

    this.light = function () {
        $input.css('background-color', '#F0F5FA');
    };

    this.unLight = function () {
        $input.css('background-color', '#FFFFFF');
    };

    this.focus = function () {
        $input.focus();
    };

    this.showError = function (message) {
//        this.addClass('field');
//        this.addClass('error');
        $error.find('p').html(message);
        $error.slideDown();

    };

    this.hideError = function () {
//        this.removeClass('field');
//        this.removeClass('error');

        $error.slideUp(function () {
            $error.find('p').html('');
        });

    };

    this.val = function (value) {
        if (typeof value == 'undefined')
            return this.getValue();
        else
            this.setValue(value);
    };

    this.setValue = function (value) {
        $input.val(value);
        $input.trigger('keypress');
    };

    this.getValue = function () {
        var value = $input.val();

        switch (props.lang) {
            case LANG_EN:
                value = value.replace(/,/g, '');

                break;
            case LANG_ID:
                value = value.replace(/\./g, '');
                value = value.replace(/,/g, '.');
        }

        return value;
    };

    this.clear = function () {
        this.setValue('');
    };

    this.setEnable = function (enable) {
        $input.prop('disabled', !enable);

        if(enable)
            $input.css('background', 'white');
        else
            $input.css('background', '#F0F0F0');

    };

    this.getEnable = function () {
        return !$input.prop('disabled');
    };

    this.render = function () {

        if (!props.maxlength)
            props.maxlength = 50;

        this.addClass('fluid');

        var htmlTemplate =
            '<input type="text" maxlength="' + props.maxlength + '">' +
            '<div class="ui error message" style="margin-top: 4px; display: none"><p></p></div>';

        if (props.icon) {
            this.addClass('ui');
            this.addClass('input');
            this.addClass('left');
            this.addClass('icon');

            htmlTemplate = '<i class="' + props.icon + ' icon"></i>' + htmlTemplate;
        }

        this.html(htmlTemplate);

        $input = this.find('input');
        $error = this.find('.ui.error.message');

        switch (props.lang) {
            case LANG_EN:
                $input.autoNumeric({
                    aSep: ",",
                    aDec: ".",
                    vMax: '999999999999.99'
                });

                break;
            case LANG_ID:
                $input.autoNumeric({
                    aSep: ".",
                    aDec: ",",
                    vMax: '999999999999.99'
                });
        }

    };

    this.init();
    return this;
};

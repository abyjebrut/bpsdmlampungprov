$.fn.uiHTMLView = function (props) {
    var $span;

    this.init = function () {

        if(!props)
            props = {};

        this.render();
    };

    this.val = function (value) {
        if (typeof value == 'undefined')
            return this.getValue();
        else
            this.setValue(value);
    };

    this.setValue = function (value) {
        $span.html(value);
    };

    this.getValue = function () {
        return $span.html();
    };

    this.clear = function () {
        this.setValue('');
    };

    this.render = function () {
        var htmlTemplate =
            '<div class="ui form" style="min-height: 240px;padding: 16px;border: solid #D9D9D9 1px; border-radius: 4px">' +
                '<span></span>' +
            '</div>';

        this.html(htmlTemplate);

        $span = this.find('span');
    };

    this.init();
    return this;
};

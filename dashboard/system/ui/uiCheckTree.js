$.fn.uiCheckTree = function (props) {
    var keys = [];

    this.init = function () {
        if(!props)
            props = {};

        if(props.structure)
            this.render(props.structure);
        else
            this.render();
    };

    this.val = function (value) {
        if(typeof value == 'undefined')
            return this.getValue();
        else
            this.setValue(value);
    };

    this.setValue = function (value) {
        this.uncheckAll();

        if(typeof value == 'undefined' || value == null)
            return;

        if(!$.isArray(value))
            value = value.split(',');

        if(value) {
            value.forEach(function (iValue) {
                this.check(iValue);
            }.bind(this));
        }

    };

    this.getValue = function () {
        return this.jstree("get_checked").toString();
    };

    this.getArrayValue = function () {
        return this.jstree("get_checked");
    };

    this.clear = function () {
        this.setValue('');
    };

    this.check = function (key) {
        this.jstree('check_node', key);
    };

    this.uncheck = function (key) {
        this.jstree('uncheck_node', key);
    };

    this.checkAll = function () {
        $.each(keys, function (i, key) {
            this.check(key);
        }.bind(this));
    };

    this.uncheckAll = function () {
        $.each(keys, function (i, key) {
            this.uncheck(key);
        }.bind(this));
    };

    this.render = function (data, callback) {
        if(!data)
            return;

        keys = [];
        var treeData = [];

        $.each(data, function (keyLv1, dataLv1) {

            keys.push(keyLv1);
            var treeDataLv2 = [];

            if(dataLv1.subStructure) {
                $.each(dataLv1.subStructure, function (keyLv2, dataLv2) {

                    keys.push(keyLv2);
                    var treeDataLv3 = [];

                    if(dataLv2.subStructure) {
                        $.each(dataLv2.subStructure, function (keyLv3, dataLv3) {

                            keys.push(keyLv3);

                            var caption = dataLv3.caption;
                            var otherCaption = config.strings[localStorage.lang][caption];

                            if (otherCaption)
                                caption = otherCaption;

                            treeDataLv3.push({
                                id: keyLv3,
                                text: caption
                            });
                        });
                    }

                    var caption = dataLv2.caption;
                    var otherCaption = config.strings[localStorage.lang][caption];

                    if (otherCaption)
                        caption = otherCaption;

                    treeDataLv2.push({
                        id: keyLv2,
                        text: caption,
                        children: treeDataLv3,
                    });
                });
            }

            var caption = dataLv1.caption;
            var otherCaption = config.strings[localStorage.lang][caption];

            if (otherCaption)
                caption = otherCaption;

            treeData.push({
                id: keyLv1,
                text: caption,
                children: treeDataLv2,
            });
        });

        this.jstree({
            checkbox: {
                keep_selected_style: false,
            },
            plugins: ['checkbox'],
            core: {
                themes: {
                    icons: false,
                },
                data: treeData,
            },
        }).on('loaded.jstree', function() {
            this.jstree('open_all');

            if($.isFunction(callback))
                callback();
        }.bind(this));
    };

    this.init();
    return this;
};

$.fn.uiTextAreaView = function (props) {
    var $textarea;

    this.init = function () {
        if(!props)
            props = {};

        this.render();
    };

    this.val = function (value) {
        if (typeof value == 'undefined')
            return this.getValue();
        else
            this.setValue(value);
    };

    this.setValue = function (value) {
        $textarea.val(value);
    };

    this.getValue = function () {
        return $textarea.val();
    };

    this.clear = function () {
        this.setValue('');
    };

    this.render = function () {
        var htmlTemplate =
            '<div class="ui form">' +
                '<textarea style="height: 240px" disabled></textarea>' +
            '</div>';

        this.html(htmlTemplate);
        $textarea = this.find('textarea');
    };

    this.init();
    return this;
};

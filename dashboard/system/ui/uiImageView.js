$.fn.uiImageView = function (props) {
    var $img;
    var fileImage;
    var urlImage;
    var urlBlankImage = 'asset/img/image.png';

    this.init = function () {

        if(!props)
            props = {};

        this.render();
    };

    this.val = function (value) {
        if (typeof value == 'undefined')
            return this.getValue();
        else
            this.setValue(value);
    };

    this.setValue = function (value) {
        if (value) {

            fileImage = value;
            urlImage = api.getArchive(fileImage);

            $img.prop('src', urlImage);
        } else {
            fileImage = '';
            urlImage = '';

            $img.prop('src', urlBlankImage);
        }
    };

    this.getValue = function () {
        return fileImage;
    };

    this.clear = function () {
        this.setValue('');
    };

    this.render = function (data) {

        if (data)
            var htmlTemplate =
                '<img src="' + data + '" style="max-width: 100%">';
        else
            var htmlTemplate =
                '<img src="' + urlBlankImage + '" style="max-width: 100%">';

        this.html(htmlTemplate);
        $img = this.find('img');
    };

    this.init();
    return this;
};

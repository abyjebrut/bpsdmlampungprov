$.fn.uiNumericBox = function (props) {
    var $input;
    var $text;
    var $error;

    this.init = function () {

        if(!props)
            props = {};

        if(!props.text)
            props.text = '';

        this.render();
        this.driveEvents();
    };

    this.driveEvents = function () {
        $input.on('focus', this.light.bind(this));
        $input.on('blur', this.unLight.bind(this));
        $input.on('keydown', this.activateEnterEvent.bind(this));
        $input.on('keydown', this.onlyNumeric.bind(this));
    };

    this.activateEnterEvent = function (event) {

        if (event.which == 13)
            this.trigger('enter');
    };

    this.onlyNumeric = function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) ||
            // Allow: Ctrl+C
            (e.keyCode == 67 && e.ctrlKey === true) ||
            // Allow: Ctrl+X
            (e.keyCode == 88 && e.ctrlKey === true) ||
            // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    };

    this.light = function () {
        $input.css('background-color', '#F0F5FA');
    };

    this.unLight = function () {
        $input.css('background-color', '#FFFFFF');
    };

    this.focus = function () {
        $input.focus();
    };

    this.showError = function (message) {
//        this.addClass('field');
//        this.addClass('error');

        $error.find('p').html(message);
        $error.slideDown();

    };

    this.hideError = function () {
//        this.removeClass('field');
//        this.removeClass('error');

        $error.slideUp(function () {
            $error.find('p').html('');
        });

    };

    this.val = function (value) {
        if (typeof value == 'undefined')
            return this.getValue();
        else
            this.setValue(value);
    };

    this.setValue = function (value) {
        $input.val(value);
    };

    this.getValue = function () {
        return $input.val();
    };

    this.clear = function () {
        this.setValue('');
    };

    this.setEnable = function (enable) {
        $input.prop('disabled', !enable);

        if(enable)
            $input.css('background', 'white');
        else
            $input.css('background', '#F0F0F0');

    };

    this.getEnable = function () {
        return !$input.prop('disabled');
    };

    this.setText = function (text) {
        props.text = text;
        $text.html(props.text);
    };

    this.getText = function () {
        return props.text;
    };

    this.render = function () {

        if (!props.maxlength)
            props.maxlength = 50;

        if (!props.placeholder)
            props.placeholder = '';

        this.addClass('fluid');

        var htmlTemplate =
            '<input type="text" maxlength="' + props.maxlength + '" placeholder="' + props.placeholder + '">' +
            '<div class="ui error message" style="margin-top: 4px; display: none"><p></p></div>';

        this.css('position', 'relative');

        htmlTemplate +=
            '<b class="ui-text" style="position: absolute; top: 8px; right: 8px; text-align: right">' +
                props.text
            '</b>';

        if (props.icon) {
            this.addClass('ui');
            this.addClass('input');
            this.addClass('left');
            this.addClass('icon');

            htmlTemplate = '<i class="' + props.icon + ' icon"></i>' + htmlTemplate;
        }

        this.html(htmlTemplate);

        $input = this.find('input');
        $text = this.find('.ui-text');
        $error = this.find('.ui.error.message');
    }

    this.init();
    return this;
};

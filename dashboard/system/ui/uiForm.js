$.fn.uiForm = function (props) {
    var $errorBlock;

    this.init = function () {
        if(!props)
            props = {};

    };

    this.clear = function () {
        $errorBlock = this.find('.error-block');

        if ($errorBlock.remove)
            $errorBlock.remove();

        this.find('.ui.input').removeClass('field error');

        this.find('input, textarea').prop('title', '');
        this.find('input, textarea').tooltip();

    };

    this.error = function (errorResult) {

        this.clear();
        var htmlErrorList = '';

        $.each(errorResult.data, function (data) {

            var errorMessage = errorResult.data[data];
            
            if(config.strings[localStorage.lang][errorMessage])
                errorMessage = config.strings[localStorage.lang][errorMessage];
            
            var $errorField = this.getRef(data);

            if ($errorField == 0)
                $errorField = this.field(data);

            $errorField.parent().addClass('field error');
            $errorField.find('input, textarea').prop('title', errorMessage);
            
            htmlErrorList += '<li>' + errorMessage + '</li>';
        }.bind(this));
        
        if(config.strings[localStorage.lang][errorResult.message])
            var message = config.strings[localStorage.lang][errorResult.message];
        else
            var message = errorResult.message;
        
        this.prepend(
            '<div class="error-block" style="display: none">' +
                '<div class="ui error message">' +
                    '<div class="header">' + message + '</div>' +
                    '<ul>' + htmlErrorList + '</ul>' +
                '</div>' +
            '</div>'
        );

        $errorBlock = this.find('.error-block');
        $errorBlock.fadeIn();

        this.find('input, textarea').tooltip();
        window.scrollTo(0, 0);

        this.find('.ui.segment:visible').transition('shake');
    };

    this.init();
    return this;
};

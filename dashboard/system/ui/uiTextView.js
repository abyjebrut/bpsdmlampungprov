$.fn.uiTextView = function (props) {
    var $input;

    this.init = function () {

        if(!props)
            props = {};

        this.render();
    };

    this.val = function (value) {
        if (typeof value == 'undefined')
            return this.getValue();
        else
            this.setValue(value);
    };

    this.setValue = function (value) {
        var caption = value;
        var otherCaption = config.strings[localStorage.lang][caption];

        if (otherCaption)
            caption = otherCaption;
        
        $input.val(caption);
    };

    this.getValue = function () {
        return $input.val();
    };

    this.clear = function () {
        this.setValue('');
    };

    this.render = function () {
        var htmlTemplate =
            '<input type="text" style="color: black" disabled>';

        this.html(htmlTemplate);
        $input = this.find('input');
    };

    this.init();
    return this;
};

$.fn.uiTableView = function (props) {

    this.init = function () {
        if(!props)
            props = {};

        if($.isArray(props)) {
            var headers = props;
            props = {};
            props.headers = headers;
        }

        if(!props.headers)
            props.headers = [];

        if(!props.rows)
            props.rows = [];

        this.render({
                headers: props.headers,
                rows: props.rows,
            });
    };

    this.render = function (data) {

        if(!data)
            return;

        if(!data.headers)
            data.headers = props.headers;

        var htmlHeaders = '';

        $.each(data.headers, function (i, header) {
            var caption = header;
            var otherCaption = config.strings[localStorage.lang][caption];

            if (otherCaption)
                caption = otherCaption;
            
            htmlHeaders += '<th>' + caption + '</th>';
        });

        var htmlRows = '';

        if (data.rows) {

            $.each(data.rows, function (iLv1, row) {
                var htmlCells = '';

                $.each(row.text, function (iLv2, _text) {
                    if(_text == null)
                        _text = '';
                    
                    var caption = _text;
                    var otherCaption = config.strings[localStorage.lang][caption];

                    if (otherCaption)
                        caption = otherCaption;

                    htmlCells += '<td>' + caption + '</td>';
                });

                htmlRows +=
                    '<tr>' +
                    htmlCells +
                    '</tr>';
            });
        }

        var htmlTemplate =
            '<table class="ui celled table">' +
                '<thead>' +
                    '<tr>' +
                        htmlHeaders +
                    '</tr>' +
                '</thead>' +
                '<tbody>' + htmlRows + '</tbody>' +
            '</table>';

        this.html(htmlTemplate);
    };

    this.init();
    return this;
};

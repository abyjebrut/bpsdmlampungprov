$.fn.uiUploadFile = function (props) {

    var $a;
    var $span;
    var $file;
    var $hidden;

    this.init = function () {

        if(!props)
            props = {};

        this.render();
        this.driveEvents();
    };

    this.driveEvents = function () {
        $a.on('click', this.deleteFile.bind(this));
        $file.on('change', this.uploadFile.bind(this));
    };

    this.deleteFile = function () {
        $msgbox.confirm('Delete current File?', function () {
            $loading.show();

            var url = 'System/deleteFile';
            var data = {
                file: this.val(),
            };

            api.post(url, data, function (result) {
                this.val('');
                $loading.hide();
            }.bind(this));

        }.bind(this));
    };

    this.uploadFile = function (event) {
        $loading.show();

        var formData = new FormData();

        $.each(event.target.files, function (key, value) {
            formData.append(key, value);
        });

        $.ajax({
            url: config.url.api + 'System/uploadFile',
            type: 'POST',
            data: formData,
            cache: false,
            dataType: 'json',
            processData: false,
            contentType: false,
            success: function(result, textStatus, jqXHR) {

                if(result.status == 'success') {
                    $a.show();
                    $span.html(result.data);
                    $hidden.val(result.data);
                }
                else
                    $msgbox.alert(result.message);

                $loading.hide();
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus);
                $loading.hide();
            },
        });
    };

    this.val = function (value) {
        if (typeof value == 'undefined')
            return this.getValue();
        else
            this.setValue(value);
    };

    this.setValue = function (value) {
        if (value) {
            $a.show();

            var urlFile = api.getArchive(value);

            $span.html(urlFile);
            $hidden.val(value);
        }
        else {
            $a.hide();
            $span.html('');
            $hidden.val('');
            $file.val('');
        }
    };

    this.getValue = function () {
        var splitHiddenVal = $hidden.val().split('/');
        var hiddenVal = splitHiddenVal[splitHiddenVal.length - 1];
        return hiddenVal;
    };

    this.clear = function () {
        this.setValue('');
    };

    this.render = function () {
        var htmlTemplate =
            '<span style="font-size: 10px;word-wrap: break-word"></span>' +
            '<a style="color: #DC6868; display: none" href="javascript:">' +
                '<i style="float: right" class="remove icon"></i> ' +
            '</a>' +
            '<input type="file"><input type="hidden">';

        this.html(htmlTemplate);

        $a = this.find('a');
        $span = this.find('span');
        $file = this.find('input[type="file"]');
        $hidden = this.find('input[type="hidden"]');
    };

    this.init();
    return this;
};

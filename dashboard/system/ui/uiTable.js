$.fn.uiTable = function(props) {
    var $table;

    var $tHead;
    var $tBody;

    var $chkHead;
    var $chkBody;

    this.init = function() {
        if (!props)
            props = {};

        if ($.isArray(props)) {
            var headers = props;
            props = {};
            props.headers = headers;
        }

        if (!props.headers)
            props.headers = [];

        if (!props.rows)
            props.rows = [];

        this.render({
            headers: props.headers,
            rows: props.rows,
        });
    };

    this.driveEvents = function() {
        $tBody.on('click', this.selectRow.bind(this));
        $chkHead.on('click', this.toggleCheckbox.bind(this));
    };

    this.toggleCheckbox = function() {

        if ($chkHead.checkbox('is checked'))
            $chkBody.checkbox('check');
        else
            $chkBody.checkbox('uncheck');
    };

    this.selectRow = function(event) {
        $tBody.find('tr').removeClass('active');

        var $row = $(event.target).closest('tr');
        $row.addClass('active');
    };

    this.val = function(value) {
        if (typeof value == 'undefined')
            return this.getValue();
        else
            this.setValue(value);
    };

    this.setValue = function(value) {

        $tBody.find('tr').removeClass('active');
        $tBody.find('input[value="' + value + '"]').closest('tr').addClass('active');
    };

    this.getValue = function() {
        return $tBody.find('tr.active input').val();
    };

    this.clear = function() {
        this.render({ rows: [] });
    };

    this.getCheckedValues = function() {
        var checkedValues = [];

        $tBody.find('tr').each(function(i, row) {
            var $chkSemantic = $(row).find('.ui.checkbox');
            var $chkNative = $(row).find('input');

            if ($chkSemantic.checkbox('is checked'))
                checkedValues.push($chkNative.val());
        });

        return checkedValues;
    };

    this.checked = function() {
        return this.getCheckedValues();
    };

    this.getAllValues = function() {
        var values = [];

        $tBody.find('tr').each(function(i, row) {
            var $chkNative = $(row).find('input');
            values.push($chkNative.val());
        });

        return values;
    };

    this.render = function(data) {

        //        data = {
        //            headers: ['ID', 'Nama', 'Email'],
        //            subHeaders: ['No.', 'Tunjangan 1', 'Tunjangan 2'],
        //            rows: [
        //                {
        //                    value: 14,
        //                    text: [1, 'Roni', 'roni@yahoo.com'],
        //                    subTable: {
        //                        rows: [{
        //                            value: 1,
        //                            text: [1, 'Data 1', 'Data 2'],
        //                        }, {
        //                            value: 2,
        //                            text: [2, 'Data 3', 'Data 4'],
        //                        }],
        //                    },
        //                },
        //                {
        //                    value: 21,
        //                    text: [2, 'Roni 2', 'roni2@yahoo.com'],
        //                },
        //            ],
        //        };

        if (!data)
            return;

        if (!data.headers)
            data.headers = props.headers;

        var htmlHeaders = '';

        $.each(data.headers, function(i, header) {
            var caption = header;
            var otherCaption = config.strings[localStorage.lang][caption];

            if (otherCaption)
                caption = otherCaption;

            htmlHeaders += '<th>' + caption + '</th>';
        });

        var htmlRows = '';

        if (data.rows) {

            $.each(data.rows, function(iLv1, row) {
                var htmlCells = '';

                $.each(row.text, function(iLv2, _text) {
                    if (_text == null)
                        _text = '';

                    var caption = _text;
                    var otherCaption = config.strings[localStorage.lang][caption];

                    if (otherCaption)
                        caption = otherCaption;

                    htmlCells += '<td>' + caption + '</td>';
                });

                htmlRows +=
                    '<tr>' +
                    '<td><div class="ui fitted checkbox"><input type="checkbox" value="' +
                    row.value + '"><label></label></div></td>' +
                    htmlCells +
                    '</tr>';
            });
        }

        var htmlTemplate =
            '<table class="ui celled table">' +
            '<thead>' +
            '<tr>' +
            '<th style="width: 24px">' +
            '<div class="ui fitted checkbox">' +
            '<input type="checkbox">' +
            '<label></label>' +
            '</div>' +
            '</th>' +
            htmlHeaders +
            '</tr>' +
            '</thead>' +
            '<tbody>' + htmlRows + '</tbody>' +
            '</table>';

        this.html(htmlTemplate);

        $table = this.find('table');
        $tHead = this.find('thead');
        $tBody = this.find('tbody');
        $chkHead = $tHead.find('.ui.checkbox');
        $chkBody = $tBody.find('.ui.checkbox');

        $table.addClass('hover-table selectable single line unstackable');

        $chkHead.checkbox();
        $chkBody.checkbox();

        this.driveEvents();
        $tBody.find('tr:first-child').trigger('click');
    };

    this.init();
    return this;
};
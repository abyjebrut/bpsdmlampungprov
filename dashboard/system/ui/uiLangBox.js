$.fn.uiLangBox = function (props) {

  var $a;

  this.init = function () {
    if(!props)
      props = {};

    // this.render([{
    //   value: 'en',
    //   text: 'English',
    //   flag: 'united kingdom',
    // },{
    //   value: 'id',
    //   text: 'Indonesia',
    //   flag: 'indonesia',
    // }]);
  };

  this.val = function (value) {
    if (typeof value == 'undefined')
      return this.getValue();
    else
      this.setValue(value);
  };

  this.setValue = function (value) {
    $a.removeClass('active');

    if (value)
      this.find('[data-value="' + value + '"]').addClass('active');
  };

  this.getValue = function () {
    var value = this.find('.active').data('value');
    return value;
  };

  this.getText = function () {
    var text = this.find('.active').attr('title');
    return text;
  };

  this.clear = function () {
    this.setValue('');
  };

  this.render = function (data) {

    var htmlTemplate =
      '<div class="ui small menu" style="float: right">';

    if (data) {
      $.each(data, function (i, row) {

        htmlTemplate +=
          '<a data-value="' + row.value + '" title="' + row.text + '" class="lang item" href="javascript:">' +
            '<i class="' + row.flag + ' flag"></i>' +
          '</a>';

      }.bind(this));
    }

    htmlTemplate += '</div>';
    this.html(htmlTemplate);

    $a = this.find('a');

    $a.off('click');
    $a.on('click', this.selectFlag.bind(this));
  };

  this.selectFlag = function (event) {
    $a.removeClass('active');
    $(event.currentTarget).addClass('active');
  };

  this.renderApi = function (sourceUrl, request, params) {
    $loading.show();

    if (!params) {
      params = request;
      request = {};
    }

    api.post(sourceUrl, request, function (result) {
      var items = [];

      if (result.data) {
        result.data.forEach(function (data) {

          if (!params.value)
            var valueKey = 'id';
          else
            var valueKey = params.value;

          items.push({value: data[valueKey], text: data[params.text], flag: data[params.flag]});
        });
      }

      this.render(items);
      $loading.hide();

      if ($.isFunction(params.done))
        params.done();
    }.bind(this));
  };

  this.init();
  return this;
};

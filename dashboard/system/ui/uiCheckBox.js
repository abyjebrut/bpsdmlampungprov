$.fn.uiCheckBox = function (props) {

    var $input;

    this.init = function () {
        if(!props)
            props = {};

        this.render();
    };

    this.checkAll = function () {
        var values = [];
        $.each(this.find('.ui.checkbox'), function () {
            values.push($(this).find('input').val());
        });

        this.setValue(values);
    };

    this.val = function (value) {
        if (typeof value == 'undefined')
            return this.getValue();
        else
            this.setValue(value);
    };

    this.setValue = function (value) {
        if (value == null)
            value = '';

        this.find('.ui.checkbox').checkbox('uncheck');

        if(!$.isArray(value))
            value = value.split(',');

        value.forEach(function (data) {
            var check = this.find('input[value="' + data + '"]').parent();
            check.checkbox('check');
        }.bind(this));
    };

    this.getData = function (type) {

        var checkedValue = '';
        var value = [];

        $.each(this.find('.ui.checkbox'), function () {
            if ($(this).checkbox('is checked')) {

                switch (type) {
                    case TYPE_VALUE:
                        checkedValue = $(this).find('input').val();
                        break;

                    case TYPE_TEXT:
                        checkedValue = $(this).find('input').data('text');
                }

                value.push(checkedValue);
            }
        });

        return value.toString();
    };

    this.getValue = function () {
        return this.getData(TYPE_VALUE);
    };

    this.getArrayValue = function () {
        return this.getValue().split(',');
    };

    this.getText = function () {
        return this.getData(TYPE_TEXT);
    };

    this.clear = function () {
        this.setValue('');
    };

    this.isChecked = function () {
        var vCount = this.getValue().length;

        if(vCount > 0)
            return true;
        else
            return false;
    };

    this.render = function (data) {
        var htmlTemplate =
            '<div class="ui form">' +
                '<div class="inline fields">';

        if (data) {
            $.each(data, function (i, row) {

                var uiRef = '';

                if (this.attr('data-ref'))
                    uiRef = this.attr('data-ref');
                else
                    uiRef = this.attr('name');

                htmlTemplate +=
                    '<div class="field">' +
                        '<div class="ui checkbox">' +
        '<input value="' + row.value + '" data-text="' + row.text + '" name="' + uiRef + '" type="checkbox">' +
                            '<label>' + row.text + '</label>' +
                        '</div>' +
                    '</div>';

            }.bind(this));
        }

        htmlTemplate += '</div></div>';

        this.html(htmlTemplate);
        $input = this.find('input');

//        $input.on('change', function () {
//            this.trigger('change');
//        }.bind(this));

        this.find('.ui.checkbox').checkbox();
    };

    this.renderApi = function (sourceUrl, request, params) {
        $loading.show();

        if (!params) {
            params = request;
            request = {};
        }

        api.post(sourceUrl, request, function (result) {
            var items = [];

            if (result.data) {
                result.data.forEach(function (data) {

                    if (!params.value)
                        var valueKey = 'id';
                    else
                        var valueKey = params.value;

                    items.push({value: data[valueKey], text: data[params.text]});
                });
            }

            this.render(items);
            $loading.hide();

            if ($.isFunction(params.done))
                params.done();
        }.bind(this));
    };

    return this;
};

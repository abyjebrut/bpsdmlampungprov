$.fn.uiHijriPicker = function (props) {
    var $input;
    var $error;

    this.init = function () {

        if(!props)
            props = {};

        if(!props.type)
            props.type = TYPE_TEXT;

        this.render();
        this.driveEvents();
    };

    this.driveEvents = function () {
        $input.on('focus', this.light.bind(this));
        $input.on('blur', this.unLight.bind(this));
        $input.on('keydown', this.activateEnterEvent.bind(this));
    };

    this.activateEnterEvent = function (event) {
        if (event.which == 13)
            this.trigger('enter');
    };

    this.light = function () {
        $input.css('background-color', '#F0F5FA');
    };

    this.unLight = function () {
        $input.css('background-color', '#FFFFFF');
    };

    this.focus = function () {
        $input.focus();
    };

    this.showError = function (message) {
//        this.addClass('field');
//        this.addClass('error');

        $error.find('p').html(message);
        $error.slideDown();

    };

    this.hideError = function () {
//        this.removeClass('field');
//        this.removeClass('error');

        $error.slideUp(function () {
            $error.find('p').html('');
        });

    };

    this.val = function (value) {
        if (typeof value == 'undefined')
            return this.getValue();
        else
            this.setValue(value);
    };

    this.getValue = function () {
        return $input.val();
    };

    this.setValue = function (value) {
        $input.val(value);
    };

    this.clear = function () {
        this.setValue('');
    };

    this.setEnable = function (enable) {
        $input.prop('disabled', !enable);

        if(enable)
            $input.css('background', 'white');
        else
            $input.css('background', '#F0F0F0');

    };

    this.getEnable = function () {
        return !$input.prop('disabled');
    };

    this.render = function () {

        if(!props.maxlength)
            props.maxlength = 50;

        if(!props.placeholder)
            props.placeholder = '';

        this.addClass('fluid');

        switch(props.type) {
            case TYPE_TEXT:
                var htmlTemplate =
                    '<input type="text" maxlength="' + props.maxlength + '" placeholder="' + props.placeholder + '">';

                break;
            case TYPE_PASSWORD:
                var htmlTemplate =
                    '<input type="password" autocomplete="off" maxlength="' + props.maxlength + '" placeholder="' + props.placeholder + '">';

        }

        if(props.icon) {
            this.addClass('ui');
            this.addClass('input');
            this.addClass('left');
            this.addClass('icon');

            htmlTemplate = '<i class="' + props.icon + ' icon"></i>' + htmlTemplate;
        }

        htmlTemplate +=
            '<div class="ui error message" style="margin-top: 4px; display: none"><p></p></div>';

        this.html(htmlTemplate);

        $input = this.find('input');
        $error = this.find('.ui.error.message');
        
        $input.calendarsPicker({
            calendar: $.calendars.instance('islamic'),
            width: 30,
//            showTrigger: '<img src="asset/img/calendar.gif" alt="Popup" class="trigger">',
        });
    };

    this.init();
    return this;
};

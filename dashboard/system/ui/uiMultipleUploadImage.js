$.fn.uiMultipleUploadImage = function (props) {

    var $a;

    var $container;
    var $file;

    var values = [];

    this.init = function () {

        if(!props)
            props = {};

        this.render();
        this.driveEvents();
    };

    this.driveEvents = function () {
        $file.on('change', this.uploadImage.bind(this));
    };

    this.reloadDeleteEvent = function () {
        $a = this.find('a');
        $a.off('click');
        $a.on('click', this.deleteImage.bind(this));
    };

    this.deleteImage = function (event) {
        var $currentTarget = $(event.currentTarget);

        $msgbox.confirm('Delete current Image?', function () {
            $loading.show();
            var fileName = $currentTarget.data('file');

            var url = 'System/deleteFile';
            var data = {
                file: fileName,
            };

            api.post(url, data, function (result) {
                $currentTarget.parent().remove();

                function removeArray(arr) {
                    var what, a = arguments, L = a.length, ax;
                    while (L > 1 && arr.length) {
                        what = a[--L];
                        while ((ax= arr.indexOf(what)) !== -1) {
                            arr.splice(ax, 1);
                        }
                    }
                    return arr;
                }

                removeArray(values, fileName);
                $loading.hide();
            }.bind(this));

        }.bind(this));
    };

    this.uploadImage = function (event) {
        $loading.show();

        var formData = new FormData();

        $.each(event.target.files, function (key, value) {
            formData.append(key, value);
        });

        var self = this;

        $.ajax({
            url: config.url.api + 'System/uploadImage',
            type: 'POST',
            data: formData,
            cache: false,
            dataType: 'json',
            processData: false,
            contentType: false,
            success: function(result, textStatus, jqXHR) {

                if(result.status == 'success') {

                    var splitValue = result.data.split('/');
                    var fileName = splitValue[splitValue.length - 1];

                    $container.append(
                        '<div class="four wide column" style="position: relative">' +
                        '<img style="width: 100%" src="' + result.data + '">' +
                        '<a data-file="' + fileName + '" style="background: #DC6868;color: white; padding: 2px;position: absolute; left: 4px; top: 0px" href="javascript:">' +
                            '&nbsp; Delete <i class="remove icon"></i>' +
                        '</a>' +
                        '</div>'
                    );

                    self.reloadDeleteEvent();

                    var splitValue = result.data.split('/');
                    var fileName = splitValue[splitValue.length - 1];

                    values.push(fileName);
                }
                else
                    $msgbox.alert(result.message);

                $loading.hide();
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus);
                $loading.hide();
            },
        });
    };

    this.val = function (value) {
        if (typeof value == 'undefined')
            return this.getValue();
        else
            this.setValue(value);
    };

    this.setValue = function (value) {
        if (value) {

            if(!$.isArray(value))
                value = value.split(',');

            values = value;
            values.forEach(function (data) {
                var imgUrl = localStorage.urlArchive + '/' + data;

                $container.append(
                    '<div class="four wide column" style="position: relative">' +
                    '<img style="width: 100%" src="' + imgUrl + '">' +
                    '<a data-file="' + data + '" style="background: #DC6868;color: white; padding: 2px;position: absolute; left: 4px; top: 0px" href="javascript:">' +
                        '&nbsp; Delete <i class="remove icon"></i>' +
                    '</a>' +
                    '</div>'
                );
            });

            this.reloadDeleteEvent();

        } else {
            values = [];
            $container.html('');
        }
    };

    this.getValue = function () {
        return values;
    };

    this.clear = function () {
        this.setValue('');
    };

    this.render = function () {
        var htmlTemplate =
            '<div class="ui stackable grid" ' +
            'style="padding: 16px; border: solid #D9D9D9 1px; border-radius: 4px; min-height: 240px;"></div>' +
            '<input type="file" style="margin-top: 8px">';

        this.html(htmlTemplate);

        $container = this.find('div');
        $file = this.find('input[type="file"]');
    };

    this.init();
    return this;
};

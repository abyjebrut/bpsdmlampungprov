$.fn.uiExpandTable = function (heads, childHeads, hideCheckbox) {
    var $table;

    var $tHead;
    var $tBody;

    var $chkHead;
    var $chkBody;

    var htmlList;

    this.dataSource = [];

    this.init = function () {
        var htmlHeads = '';

        heads.forEach(function (field) {
            htmlHeads += '<th>' + field + '</th>';
        });

        var htmlTemplate =
            '<table class="ui celled table">' +
                '<thead>' +
                    '<tr>' +
                        '<th style="width: 24px">' +
                            '<div class="ui fitted checkbox">' +
                                '<input type="checkbox">' +
                                '<label></label>' +
                            '</div>' +
                        '</th>' +
                        htmlHeads +
                    '</tr>' +
                '</thead>' +
                '<tbody></tbody>' +
            '</table>';

        this.html(htmlTemplate);

        $table = this.find('table');

        $tHead = this.find('thead');
        $tBody = this.find('> table > tbody');

        $table.addClass('hover-table');

        $chkHead = $tHead.find('.ui.checkbox');
        $chkHead.checkbox();

        $chkBody = $tBody.find('.ui.checkbox');

        if(hideCheckbox) {
//            $chkHead.hide();
            $chkBody.hide();
        }

        this.driveEvents();
    };

    this.driveEvents = function () {
        $tBody.on('click', this.select.bind(this));
        $chkHead.on('click', this.toggleCheckbox.bind(this));

    };

    this.expandRow = function (event) {

        if($(event.target).html() == '[-]') {
            $(event.target).html('[+]');
            $(event.target).closest('tr').next().slideUp();
        }
        else {
            $(event.target).html('[-]');
            $(event.target).closest('tr').next().slideDown();
        }

    };

    this.toggleCheckbox = function () {

        if ($chkHead.checkbox('is checked'))
            $chkBody.checkbox('check');
        else
            $chkBody.checkbox('uncheck');

    };

    this.select = function (event) {
        $tBody.find('tr').removeClass('active');

        var $row = $(event.target).closest('tr');
        $row.addClass('active');
    };

    this.clear = function () {
        htmlList = '';
    };

    this.push = function (id, arrData, child) {
        var htmlCell = '';
        var columnCount = 1;
        var rowLength = arrData.length;

        arrData.forEach(function (data) {
            columnCount++;

            if(data == null)
                data = '';

            if(columnCount > rowLength) {
                if(child)
                    data += '<a class="expand-caret" style="float: right" href="javascript:">[+]</a>';
            }

            htmlCell += '<td>' + data + '</td>';
        });

//        htmlCell+= '<td>[+]</td>';

        var htmlRow =
            '<tr>' +
            '<td><div class="ui fitted checkbox"><input type="checkbox" value="' + id + '"><label></label></div></td>' +
            htmlCell +
            '</tr>';

        var htmlChild = '';

        if(child) {
            htmlChild =
                '<table class="ui celled table">' +
                '<thead><tr>'
                ;

            childHeads.forEach(function (head) {
                htmlChild +=
                    '<th>' + head + '</th>';
            });

            htmlChild +=
                '</tr></thead><tbody>';

            child.forEach(function (row) {
                htmlChild += '<tr>';

                row.forEach(function (column) {
                    htmlChild += '<td style="vertical-align: top">' + column + '</td>';
                });

                htmlChild += '</tr>';
            });

            htmlChild +=
                '</tbody>' +
                '</table>'
                ;
        }

        htmlList += htmlRow;

        if(htmlChild != '')
            htmlList += '<tr style="display: none" ><td colspan="' + columnCount + '">' + htmlChild + '</td></tr>';
    };

    this.val = function (value) {
        if (typeof value == 'undefined')
            return this.getValue();
        else
            this.setValue(value);
    };

    this.setValue = function (value) {

        if(value) {
            $tBody.find('tr').removeClass('active');
            $tBody.find('input[value="' + value + '"]').closest('tr').addClass('active');
        }
        else
            htmlList = '';
    };

    this.getValue = function () {
        var value = $tBody.find('tr.active input').val();

        if(value == undefined) {
            value = $tBody.find('tr.active').parent().parent().parent().parent().prev().find('input').val();
        }

        return value;
    };

//    this.clear = function () {
//        this.setValue('');
//    };

    this.checked = function () {
        var multiVal = new Array();

        $tBody.find('tr').each(function (i, row) {
            var $chkSemantic = $(row).find('.ui.checkbox');
            var $chkNative = $(row).find('input');

            if ($chkSemantic.checkbox('is checked'))
                multiVal.push($chkNative.val());
        });

        return multiVal;
    };

    this.data = function (field) {
        var searchData;

        this.dataSource.forEach(function (data) {
            if(data.id == this.val())
                searchData = data[field];
        }.bind(this));

        return searchData;
    };

    this.render = function () {

        $tBody.html(htmlList);
        $tBody.find('> tr:first-child').trigger('click');

        $chkBody = $tBody.find('.ui.checkbox');
        $chkBody.checkbox();

        if(hideCheckbox) {
//            $chkHead.hide();
            $chkBody.hide();
        }

        this.find('.expand-caret').on('click', this.expandRow.bind(this));
    };

    this.init();
    return this;
};

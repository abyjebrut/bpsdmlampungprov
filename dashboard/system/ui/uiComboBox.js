$.fn.uiComboBox = function (props) {
    var $select;
    var changeAction;
    var $error;

    this.init = function () {
        if (!props)
            props = {};

        if (!props.placeholder)
            props.placeholder = '';

        if (typeof props.native == 'undefined')
            props.native = config.ui.native;

        this.render();
    };

    this.on = function (drive, action) {
        if (drive == 'change')
            changeAction = action;
    };

    this.val = function (value) {
        if (typeof value == 'undefined')
            return this.getValue();
        else
            this.setValue(value);
    };

    this.showError = function (message) {
        $error.find('p').html(message);
        $error.slideDown();
    };

    this.hideError = function () {
        $error.slideUp(function () {
            $error.find('p').html('');
        });

    };

    this.setValue = function (value) {

        $select.val('');
        $select.siblings('.text').html('');

        $.each($select.find('option'), function (i, option) {
            var realValue = $(option).val();
            var splitValue = realValue.split('^');

            if (splitValue.length == 1)
                var id = '';
            else
                var id = splitValue[0].trim();

            if (value == id) {

                if (props.native == false) {
                    $select.dropdown('set selected', realValue);
                    $select.dropdown('set value', realValue);
                } else
                    $select.val(realValue);
            }

        }.bind(this));
    };

    this.getData = function (type) {
        var realValue = $select.val();

        if (realValue) {

            switch (type) {
                case TYPE_VALUE:
                    var index = 0;
                    break;
                case TYPE_TEXT:
                    var index = 1;
            }

            var splitValue = realValue.split('^');
            var value = splitValue[index].trim();

            return value;
        } else
            return '';
    };

    this.getValue = function () {
        return this.getData(TYPE_VALUE);
    };

    this.getText = function () {
        return this.getData(TYPE_TEXT);
    };

    this.clear = function () {
        this.setValue('');
    };

    this.render = function (data) {

        var htmlTemplate =
            '<select class="ui search dropdown fluid"></select>';
            htmlTemplate +=
            '<div class="ui error message" style="margin-top: 4px; display: none"><p></p></div>';
        this.html(htmlTemplate);
        
        $error = this.find('.ui.error.message');
        
        $select = this.find('select');

        var htmlList = '<option value="">' + props.placeholder + '</option>';

        if (data) {
            $.each(data, function (i, row) {
                htmlList +=
                    '<option value="' + row.value + ' ^ ' + row.text + '">' + row.text + '</option>';
            });
        }
        

        $select.html(htmlList);

        if (props.native == false) {
            $select.siblings('.menu').remove();

            if ($.isFunction(changeAction))
                $select.dropdown({
                    onChange: function (value, text) {
                        $select.dropdown('set selected', value);
                        $select.dropdown('set value', value);

                        changeAction();
                    },
                });
            else
                $select.dropdown();

            //            $select.parent().find('input').on('keydown', function (event) {
            //
            //                if(event.keyCode == 13)
            //                    event.stopPropagation();
            //            });

        } else {
            $select.removeClass('ui');
            $select.removeClass('search');
            $select.removeClass('dropdown');
            $select.removeClass('fluid');

            $select.parent().addClass('ui');
            $select.parent().addClass('form');

            if ($.isFunction(changeAction))
                $select.on('change', changeAction);
        }

    };

    this.renderApi = function (sourceUrl, request, params) {
        $loading.show();

        if (!params) {
            params = request;
            request = {};
        }

        api.post(sourceUrl, request, function (result) {
            var items = [];

            if (result.data) {
                result.data.forEach(function (data) {

                    if (!params.value)
                        var valueKey = 'id';
                    else
                        var valueKey = params.value;

                    items.push({ value: data[valueKey], text: data[params.text] });
                });
            }

            this.render(items);
            $loading.hide();

            if ($.isFunction(params.done))
                params.done();
        }.bind(this));
    };

    this.init();
    return this;
};

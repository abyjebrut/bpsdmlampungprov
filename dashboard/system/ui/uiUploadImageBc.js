$.fn.uiUploadImage = function (props) {

    var $a;
    var $img;
    var $file;
    var $hidden;
    var $progress;

    this.init = function () {

        if(!props)
            props = {};

        if(props.resolution) {
            props.resolutionX = props.resolution[0];
            props.resolutionY = props.resolution[1];
        }

        this.render();
        this.driveEvents();
    };

    this.driveEvents = function () {
        $a.on('click', this.deleteImage.bind(this));
        $file.on('change', this.uploadImage.bind(this));
    };

    this.deleteImage = function () {
        $msgbox.confirm('Delete current Image?', function () {
            $loading.show();

            var url = 'System/deleteFile';
            var data = {
                file: this.val(),
            };

            api.post(url, data, function (result) {
                this.val('');
                $loading.hide();
            }.bind(this));

        }.bind(this));
    };

    this.uploadImage = function (event) {
        var formData = new FormData();

        $.each(event.target.files, function (key, value) {
            formData.append(key, value);
        });

        $progress.fadeIn();
        $progress.progress({
            percent: 0,
        });

        $file.prop('disabled', true);

        $.ajax({
            url: config.url.api + 'System/uploadImage',
            type: 'POST',
            data: formData,
            cache: false,
            dataType: 'json',
            processData: false,
            contentType: false,
            success: function(result, textStatus, jqXHR) {

                if(result.status == 'success') {
                    $a.show();

                    $progress.progress({
                        percent: 50,
                    });

                    $img.load(function() {
                        $hidden.val(result.data);

                        $progress.progress({
                            percent: 100,
                        });
                        $progress.fadeOut('slow');
                        $file.prop('disabled', false);

                    }).prop('src', result.data);
                }
                else {
                    $progress.fadeOut();
                    $file.prop('disabled', false);
                    $file.val('');
                    
                    $msgbox.alert(result.message);
                }

            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus);
                $loading.hide();
                $file.prop('disabled', false);
            },
        });
    };

    this.val = function (value) {
        if (typeof value == 'undefined')
            return this.getValue();
        else
            this.setValue(value);
    };

    this.setValue = function (value) {
        if(value) {
            var urlImage = api.getArchive(value);

            $img.load(function(result) {
                $a.show();
            }).prop('src', urlImage);

            $hidden.val(value);
        } else {
            $a.hide();
            $img.removeAttr('src');
            $hidden.val('');
            $file.val('');

            $progress.progress({
                percent: 0,
            });
        }
    };

    this.getValue = function () {
        var splitHiddenVal = $hidden.val().split('/');
        var hiddenVal = splitHiddenVal[splitHiddenVal.length - 1];
        return hiddenVal
    };

    this.clear = function () {
        this.setValue('');
    };

    this.render = function () {
        var htmlWidth = '';
        var htmlHeight = '';

        if (props.resolutionX)
            htmlWidth = props.resolutionX + 'px';
        else
            htmlWidth = '-';

        if (props.resolutionY)
            htmlHeight = props.resolutionY + 'px';
        else
            htmlHeight = '-';

        var htmlResolution =
            '<div style="color: red; font-size: 12px">' +
            'Best Fit On: (Width: ' + htmlWidth + ' / Height: ' + htmlHeight + ')</div>';

        if (!props.resolutionX && !props.resolutionY)
            htmlResolution = '';

        var htmlTemplate =
            '<div style="position: relative">' +
            '<img style="max-width: 100%">' +
            '<a style="display: none;background: #DC6868;color: white; padding: 2px;position: absolute; left: 0px; top: 0px" href="javascript:">' +
                '&nbsp; Delete <i class="remove icon"></i>' +
            '</a>' +
            '<input type="file"><input type="hidden">' +
            '<div class="ui progress" style="position: absolute; right: 10px; bottom: -14px; width: 50%; display: none"><div class="bar"></div></div>' +
            htmlResolution +
            '</div>';

        this.html(htmlTemplate);

        $a = this.find('a');
        $img = this.find('img');
        $file = this.find('input[type="file"]');
        $hidden = this.find('input[type="hidden"]');
        $progress = this.find('.ui.progress');
    };

    this.init();
    return this;
};

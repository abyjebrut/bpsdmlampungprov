$.fn.uiMultipleImageView = function (props) {
    var values = [];
    var $container;

    this.init = function () {

        if(!props)
            props = {};

        this.render();
    };

    this.val = function (value) {
        if (typeof value == 'undefined')
            return this.getValue();
        else
            this.setValue(value);
    };

    this.setValue = function (value) {
        if(value) {
            if(!$.isArray(value))
                value = value.split(',');

            values = value;
            var htmlImages = '';

            $.each(value, function (i, data) {
                var urlImage = api.getArchive(data);
                htmlImages +=
                    '<div class="four wide column"><img style="width: 100%" src="' + urlImage + '"></div>';
            });

            $container.html(htmlImages);
        } else {
            values = [];
            $container.html('');
        }
    };

    this.getValue = function () {
        return values;
    };

    this.clear = function () {
        this.setValue('');
    };

    this.render = function () {
        var htmlTemplate =
            '<div class="container ui stackable grid" style="min-height: 240px;padding: 16px;border: solid #D9D9D9 1px; border-radius: 4px"></div>';

        this.html(htmlTemplate);
        $container = this.find('.container');

    };

    this.init();
    return this;
};


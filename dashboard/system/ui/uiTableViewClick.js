$.fn.uiTableViewClick = function (props) {
    var $table;

    var $tHead;
    var $tBody;


    this.init = function () {
        if (!props)
            props = {};

        if ($.isArray(props)) {
            var headers = props;
            props = {};
            props.headers = headers;
        }

        if (!props.headers)
            props.headers = [];

        if (!props.rows)
            props.rows = [];

        this.render({
            headers: props.headers,
            rows: props.rows,
        });
    };

    this.driveEvents = function () {
        $tBody.on('click', this.selectRow.bind(this));
    };

    this.selectRow = function (event) {
        $tBody.find('tr').removeClass('active');

        var $row = $(event.target).closest('tr');
        $row.addClass('active');
    };

    this.val = function (value) {
        if (typeof value == 'undefined')
            return this.getValue();
        else
            this.setValue(value);
    };

    this.setValue = function (value) {

        $tBody.find('tr').removeClass('active');
        $tBody.find('input[value="' + value + '"]').closest('tr').addClass('active');
    };

    this.getValue = function () {
        return $tBody.find('tr.active input').val();
    };

    this.clear = function () {
        this.render({ rows: [] });
    };


    this.getAllValues = function () {
        var values = [];

        $tBody.find('tr').each(function (i, row) {
            var $chkNative = $(row).find('input');
            values.push($chkNative.val());
        });

        return values;
    };

    this.render = function (data) {

        if (!data)
            return;

        if (!data.headers)
            data.headers = props.headers;

        var htmlHeaders = '';

        $.each(data.headers, function (i, header) {
            var caption = header;
            var otherCaption = config.strings[localStorage.lang][caption];

            if (otherCaption)
                caption = otherCaption;

            htmlHeaders += '<th>' + caption + '</th>';
        });

        var htmlRows = '';

        if (data.rows) {

            $.each(data.rows, function (iLv1, row) {
                var htmlCells = '';

                $.each(row.text, function (iLv2, _text) {
                    if (_text == null)
                        _text = '';

                    var caption = _text;
                    var otherCaption = config.strings[localStorage.lang][caption];

                    if (otherCaption)
                        caption = otherCaption;

                    htmlCells += '<td>' + caption + '</td>';
                });

                htmlRows +=
                '<tr>' +
                '<td style="display:none" ><div class="ui fitted checkbox"><input type="checkbox" value="' +
                row.value + '"><label></label></div></td>' +
                htmlCells +
                '</tr>';

                // htmlRows +=
                //     '<tr>' +
                //     htmlCells +
                //     '</tr>';
            });
        }

        var htmlTemplate =
            '<table class="ui celled table">' +
            '<thead>' +
            '<tr>' +
            htmlHeaders +
            '</tr>' +
            '</thead>' +
            '<tbody>' + htmlRows + '</tbody>' +
            '</table>';

        this.html(htmlTemplate);

        $table = this.find('table');
        $tHead = this.find('thead');
        $tBody = this.find('tbody');
        
        $table.addClass('hover-table');

        
        this.driveEvents();
        $tBody.find('tr:first-child').trigger('click');
    };

    this.init();
    return this;
};

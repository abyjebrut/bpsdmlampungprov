$.fn.uiButton = function (props) {
    var enabled = true;
    var lastClass = '';

    this.init = function () {
        if(!props)
            props = {};
    };

    this.setText = function (text) {
        this.html(text);
    };

    this.getText = function () {
        return this.html();
    };

    this.setEnabled = function (_enabled) {
        enabled = _enabled;

        if (enabled) {

            if (lastClass)
                this.addClass(lastClass);

            this.prop('disabled', false);
        }
        else {

            if (this.hasClass('negative')) {
                this.removeClass('negative');
                lastClass = 'negative';
            }

            if (this.hasClass('primary')) {
                this.removeClass('primary');
                lastClass = 'primary';
            }

            this.prop('disabled', true);
        }
    };

    this.getEnabled = function () {
        return enabled;
    };

    this.loading = function () {
        this.addClass('loading');
        this.prop('disabled', true);
    };

    this.release = function () {
        this.removeClass('loading');
        this.prop('disabled', false);
    };

    this.init();
    return this;
};

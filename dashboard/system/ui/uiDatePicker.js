$.fn.uiDatePicker = function (props) {
    var $input;
    var $img;

    var $year;
    var $month;
    var $date;

    this.init = function () {
        if(!props)
            props = {};

        if(typeof props.year == 'undefined')
            props.year = true;

        if(typeof props.month == 'undefined')
            props.month = true;

        if(typeof props.date == 'undefined')
            props.date = true;

        if(typeof props.picker == 'undefined')
            props.picker = true;

        if(!props.lang)
            props.lang = localStorage.lang;

        this.render();
        this.driveEvents();
    };

    this.driveEvents = function () {
        $input.on('focus', this.light.bind(this));
        $input.on('blur', this.unLight.bind(this));
        $input.on('change', this.selectDate.bind(this));

        $year.on('change', this.inputDate.bind(this));
        $month.on('change', this.inputDate.bind(this));
        $date.on('change', this.inputDate.bind(this));
    };

    this.light = function () {
        $input.css('background-color', '#F0F5FA');
    };

    this.unLight = function () {
        $input.css('background-color', '#FFFFFF');
    };

    this.selectDate = function () {
        $year.val(this.getYear());
        $month.val(this.getMonth());
        $date.val(this.getDate());
    };

    this.inputDate = function () {

        var yearValue = $year.val();
        var monthValue = $month.val();
        var dateValue = $date.val();

        if (!yearValue)
            yearValue = 2015;

        if (!monthValue)
            monthValue = 1;

        if (!dateValue)
            dateValue = 1;

        var stringDate = yearValue + '-' + monthValue + '-' + dateValue;
        this.setValue(stringDate);
    };

    this.val = function (value) {
        if(typeof value == 'undefined')
            return this.getValue();
        else
            this.setValue(value);
    };

    this.setValue = function (value) {
        if(!value) {
            $input.datepicker('setDate', null);
            $year.val('');
            $month.val('');
            $date.val('');
        }
        else {
            var splitValue = value.split('-');

            var dateYear = splitValue[0];
            var dateMonth = splitValue[1];
            var dateDay = splitValue[2];

            var dateValue = dateDay + '/' + dateMonth + '/' + dateYear;

            if(dateValue == '00/00/0000')
                dateValue = null;

            $input.datepicker('setDate', dateValue);
            $input.trigger('change');
        }
    };

    this.setYear = function (value) {
        $year.val(value);
        $year.trigger('change');
    };

    this.setMonth = function (value) {
        $month.val(value);
        $month.trigger('change');
    };

    this.setDate = function (value) {
        $date.val(value);
        $date.trigger('change');
    };

    this.getValue = function () {
        var realDate = $input.datepicker('getDate');

        if(!realDate)
            return '';
        else {
            var dateValue = this.getYear() + '-' + this.getMonth() + '-' + this.getDate();
            return dateValue;
        }
    };

    this.getYear = function () {
        var realDate = $input.datepicker('getDate');

        if(!realDate)
            return '';
        else
            return realDate.getFullYear();
    };

    this.getMonth = function () {
        var realDate = $input.datepicker('getDate');

        if(!realDate)
            return '';
        else
            return realDate.getMonth() + 1;
    };

    this.getDate = function () {
        var realDate = $input.datepicker('getDate');

        if(!realDate)
            return '';
        else
            return realDate.getDate();
    };

    this.getText = function () {
        return $input.val();
    };

    this.clear = function () {
        this.setValue('');
    };

    this.render = function () {

        var htmlDays = '<option></option>';

        for (var iDays = 1; iDays <= 31; iDays++) {
            htmlDays += '<option>' + iDays + '</option>';
        }

        htmlDays =
            '<div class="ui form" style="display: inline-block; padding-right: 2px;">' +
                '<select class="select-date" style="height: 38px">' + htmlDays + '</select>' +
            '</div>';

        var htmlMonths = '<option></option>';

        switch (props.lang) {
            case LANG_EN:
                var months = [
                    'January',
                    'February',
                    'March',
                    'April',
                    'May',
                    'June',
                    'July',
                    'August',
                    'September',
                    'October',
                    'November',
                    'December'
                ];

                break;
            case LANG_ID:
                var months = [
                    'Januari',
                    'Februari',
                    'Maret',
                    'April',
                    'Mei',
                    'Juni',
                    'Juli',
                    'Agustus',
                    'September',
                    'Oktober',
                    'Nopember',
                    'Desember'
                ];
        }

        for (var iMonths = 1; iMonths <= 12; iMonths++) {
            htmlMonths += '<option value="' + iMonths + '">' + months[iMonths - 1] + '</option>';
        }

        htmlMonths =
            '<div class="ui form" style="display: inline-block; padding-right: 2px;">' +
                '<select class="select-month" style="height: 38px">' + htmlMonths + '</select>' +
            '</div>';

        var htmlYears = '<option></option>';

        var date = new Date();
        var thisYear = date.getFullYear();
        var pastYear = thisYear - config.ui.pastYearRange;
        var futureYear = thisYear + config.ui.futureYearRange;

        for (var iYears = pastYear; iYears <= futureYear; iYears++) {
            htmlYears += '<option>' + iYears + '</option>';
        }

        htmlYears =
            '<div class="ui form" style="display: inline-block; padding-right: 2px;">' +
                '<select class="select-year" style="height: 38px">' + htmlYears + '</select>' +
            '</div>';


        switch (props.lang) {
            case LANG_EN:
                var htmlTemplate =
                    '<div class="">' +
                    htmlYears +
                    htmlMonths +
                    htmlDays +
                    '<input type="text"></div>';
                break;
            case LANG_ID:
                var htmlTemplate =
                    '<div class="">' +
                    htmlDays +
                    htmlMonths +
                    htmlYears +
                    '<input type="text"></div>';
        }


        this.html(htmlTemplate);

        $input = this.find('input');
        $input.datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd/mm/yy',
            yearRange: '-' + config.ui.pastYearRange + ':+' + config.ui.futureYearRange,

            showOn: "button",
            buttonImage: "asset/img/calendar.gif",
            buttonImageOnly: true,
            buttonText: "Select date",
        });

        $input.css({
            height: '38px',
            width: '4px',
            padding: '0px',
            visibility: 'hidden',
        });

        $img = this.find('img');
        $img.css('cursor', 'pointer');

        $year = this.find('.select-year');
        $month = this.find('.select-month');
        $date = this.find('.select-date');

        if (!props.year)
            $year.parent().hide();

        if (!props.month)
            $month.parent().hide();

        if (!props.date)
            $date.parent().hide();

        if (!props.picker) {
            $input.hide();
            $img.hide();
        }

    };

    this.init();
    return this;
};

$.fn.uiMapPicker = function (props) {
    var $map;
    var $latitude;
    var $longitude;

    var map;
    var marker;
    var infoWindow;

    var latitude;
    var longitude;

    this.init = function () {
        if(!props)
            props = {};

//        if(typeof props.showTextBox == 'undefined')
//            props.showTextBox = false;

        this.render();
    };

    this.driveEvents = function () {

        if (config.ui.googleApi) {
            google.maps.event.addListener(map, 'click', function (event) {
                this.placeMarker(event.latLng);
            }.bind(this));
        }

//        if (props.showTextBox) {
//            $latitude.on('change', function () {
//                this.setValue($latitude.val(), $longitude.val());
//            }.bind(this));
//
//            $longitude.on('change', function () {
//                this.setValue($latitude.val(), $longitude.val());
//            }.bind(this));
//        }
    };

    this.placeMarker = function (location) {

        marker.setPosition(location);
        infoWindow.setContent('Latitude: ' + location.lat() + '<br>Longitude: ' + location.lng());
        infoWindow.open(map, marker);

        latitude = location.lat();
        longitude = location.lng();

        this.trigger('change');

//        if ($latitude)
//            $latitude.val(location.lat());
//
//        if ($longitude)
//            $longitude.val(location.lng());
    };

    this.refresh = function () {
        google.maps.event.trigger(map, 'resize');
    };

    this.val = function (value) {
        if(typeof value == 'undefined')
            return this.getValue();
        else
            this.setValue(value);
    };

    this.setValue = function (value1, value2) {

        if (value1) {
            if (!value2) {

                if($.isArray(value1)) {
                    latitude = value1[0];
                    longitude = value1[1];
                }
                else {
                    var splitValue1 = value1.split(',');
                    latitude = splitValue1[0];
                    longitude = splitValue1[1];
                }
            }
            else {
                latitude = value1;
                longitude = value2;
            }

        }
        else {
            latitude = config.ui.defaultLatitude;
            longitude = config.ui.defaultLongitude;
        }

        this.createMap();
    };

    this.getValue = function () {
        var value = latitude + ',' + longitude;
        return value;
    };

    this.getArrayValue = function () {
        var value = [latitude, longitude];
        return value;
    };

    this.clear = function () {
        this.setValue('');
    };

    this.createMap = function () {

        if (config.ui.googleApi) {

            var mapPosition = new google.maps.LatLng(latitude, longitude);

            var mapProp = {
                center: mapPosition,
                zoom: config.ui.mapZoom,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
            };

            var mapDOM = $map.get(0);
            map = new google.maps.Map(mapDOM, mapProp);

            marker = new google.maps.Marker({
                position: mapPosition,
            });

            marker.setMap(map);

            infoWindow = new google.maps.InfoWindow({
                content: 'Latitude: ' + latitude + '<br>Longitude: ' + longitude
            });

            infoWindow.open(map, marker);

//            if ($latitude)
//                $latitude.val(latitude);
//
//            if ($longitude)
//                $longitude.val(longitude);

            google.maps.event.trigger(map, 'resize');

            this.driveEvents();
        }
    };

    this.render = function () {

//        if (props.showTextBox)
//            var htmlTextBox =
//                '<div class="ui stackable grid">' +
//                '<div class="eight wide column"><input class="_latitude" type="text" placeholder="Latitude"></div>' +
//                '<div class="eight wide column"><input class="_longitude" type="text" placeholder="Longitude"></div>' +
//                '</div>';
//        else
//            var htmlTextBox = '';

        var htmlTemplate =
            '<div class="map" style="width: 100%;height: 380px; background: silver"></div>'
//            htmlTextBox
            ;

        this.html(htmlTemplate);

        $map = this.find('.map');

//        if (props.showTextBox) {
//            $latitude = this.find('._latitude');
//            $longitude = this.find('._longitude');
//        }

        latitude = config.ui.defaultLatitude;
        longitude = config.ui.defaultLongitude;

        this.createMap(latitude, longitude);
    };

    this.init();
    return this;
};

$.fn.uiPaging = function (props) {
    var pageCount;
    var pageActive;
    var pageRange = 10;
    var rangeActive = 0;
    var lastLimit = 0;

    var $btnPrev;
    var $btnNext;

    this.init = function () {

        if(!props)
            props = {};

        if(props.pageCount)
            this.render(props.pageCount);
        else
            this.render();

        this.driveEvents();
    };

    this.driveEvents = function () {
        this.on('click', this.move.bind(this));
    };

    this.move = function (event) {
        if ($(event.target).prop('tagName') == 'I')
            var $pagingButton = $(event.target).parent();
        else
            var $pagingButton = $(event.target);

        var selectedPage = $pagingButton.data('page');

        switch (selectedPage) {
        case -1:

            if (pageActive > 1)
                pageActive--;
            break;
        case 0:
            if (pageActive < pageCount)
                pageActive++;
            break;
        default:
            pageActive = selectedPage;
        }

        this.find('a').removeClass('active');
        this.find('a[data-page="' + pageActive + '"]').addClass('active');
    };

    this.val = function (value) {
        if (typeof value == 'undefined')
            return this.getValue();
        else
            this.setValue(value);
    };

    this.setValue = function (value) {
        pageActive = value;

        this.find('a').removeClass('active');
        this.find('a[data-page="' + pageActive + '"]').addClass('active');
    };

    this.getValue = function () {
        return pageActive;
    };

    this.clear = function () {
        this.setValue('');
    };

    this.prevRange = function () {
        rangeActive -= (pageRange + lastLimit);
        this.render(pageCount, true);
    };

    this.nextRange = function () {
        this.render(pageCount, true);
    };

    this.render = function (data, localUpdate) {

        if (!localUpdate)
          rangeActive = 0;

        var htmlTemplate =
            '<div class="ui pagination menu">' +
                // '<a data-page="-1" href="javascript:" class="item" style="width: 43.65px">' +
                //     '<i class="left arrow icon"></i>' +
                // '</a>' +
                '<span class="no-hidden-xs"></span>' +
                // '<a data-page="0" href="javascript:" class="item" style="width: 43.65px">' +
                //     '<i class="right arrow icon"></i>' +
                // '</a>' +
            '</div>';

        if (!data)
          return;

        this.html(htmlTemplate);

        pageCount = data;
        var htmlButtons = '';

        if (rangeActive > 1)
          htmlButtons += '<a data-page="' + ((rangeActive - pageRange) + 1) + '" id="btn-prev" href="javascript:" class="item" style="width: 43.65px">...</a>';

        var i = 0;
        while (rangeActive < pageCount) {
          rangeActive++;
          i++;

          htmlButtons += '<a data-page="' + rangeActive + '" href="javascript:" class="item" style="width: 43.65px">' + rangeActive + '</a>';

          if (i >= pageRange) {
            htmlButtons += '<a data-page="' + (rangeActive + 1) + '" id="btn-next" href="javascript:" class="item" style="width: 43.65px">...</a>';
            break;
          }
        }

        lastLimit = i;

        this.find('.no-hidden-xs').html(htmlButtons);
        this.find('.no-hidden-xs a:first-child').addClass('active');

        pageActive = 1;

        $btnPrev = this.find('#btn-prev');
        $btnNext = this.find('#btn-next');

        $btnPrev.off('click');
        $btnPrev.on('click', this.prevRange.bind(this));

        $btnNext.off('click');
        $btnNext.on('click', this.nextRange.bind(this));
    };

    this.init();
    return this;
};
